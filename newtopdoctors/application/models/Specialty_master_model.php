<?php

class Specialty_master_model extends MY_Model {

	protected $primary_key = 'specialties_id';

	function arabic_speciality() {
		return $this->db->select('specialties_id, name AS SPEC, mipmap AS Mipmap')
						->from($this->_table)
						->where('status', "1")
						->order_by('name', 'ASC')
						->get()
						->result();
	}

	function english_speciality() {
		return $this->db->select('specialties_id, name_en AS SPEC, mipmap AS Mipmap')
						->from($this->_table)
						->where('status', "1")
						->order_by('name_en', 'ASC')
						->get()
						->result();
	}

	function specialityDetails($name,$lang) {

		$this->db->select('specialties_id');
		$this->db->from($this->_table);
		if($lang=='ar'){
			$this->db->where('name', $name);
		}else{
			$this->db->where('name_en', $name);
		}	
		$this->db->where('status', "1");
		return $this->db->get()->row();
	}
}