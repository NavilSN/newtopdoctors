<?php

class Work_master_model extends MY_Model {

	protected $primary_key = 'work_id';

	function getAllHospitals() {
		return $this->db->select()
						->from($this->_table)
						->where(array(
							'work_type' => 'Hospital',
							'status'	=> '1'
						))
						->order_by('name_en')
						->get()->result();
	}

	function getAllClinics() {
		return $this->db->select() 
						->from($this->_table)
						->where(array(
							'work_type' => 'Clinic',
							'status'	=> '1'
						))
						->order_by('name_en')
						->get()->result();
	}

	function getAllWorkMater() {
	return 	$this->db->select('work_master.*,COUNT(work_rating.user_id) AS TotalReview,work_rating.average_score as RatingAvg')
			->from('work_master')
			->join('work_rating','work_rating.work_id=work_master.work_id','left')
			->where('work_master.status','1')
			->group_by('work_master.work_id')
			->order_by('work_master.name_en')
			->get()->result();				
	}

	function getAllWorkMaterResult($lang,$userId,$page,$perPage) {
            $page = ($page - 1);
              $page = ($page * $perPage);
		if($lang=='ar'){
			$this->db->select("work_master.work_id AS WorkId, work_master.name AS Name, work_master.address AS Address, work_master.phone AS Phone, work_master.photo AS Photo, 'null' AS DoctorId, work_master.work_type AS WorkType, 'null' AS Latitude, 'null' AS Longitude, 'null' AS Zoom, wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus,work_rating.average_score as RatingAvg, COUNT(work_rating.work_id) AS TotalReview");
		}else{
			$this->db->select("work_master.work_id AS WorkId, work_master.name_en AS Name, work_master.address_en AS Address, work_master.phone AS Phone, work_master.photo AS Photo, 'null' AS DoctorId, work_master.work_type AS WorkType, 'null' AS Latitude, 'null' AS Longitude, 'null' AS Zoom, wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus,work_rating.average_score as RatingAvg, COUNT(work_rating.work_id) AS TotalReview");
		}
		$this->db->from('work_master');

		$this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd 
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = work_master.work_id) AS Mipmap');

		$this->db->select('(SELECT GROUP_CONCAT(sm.name_en) FROM `doctor_details` dd 
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = work_master.work_id) AS Speciality');

		$this->db->join("worktype_bookmark_master wbm", "wbm.work_id = work_master.work_id AND wbm.user_id=".$userId, "left");
		$this->db->join("work_rating", "work_rating.work_id = work_master.work_id", "left");
		//$this->db->order_by('work_master.name_en');
		$this->db->where('work_master.status','1');
		$this->db->group_by("work_master.work_id");
                $this->db->order_by('FIELD(work_type, "Hospital", "Clinic", "Lab")');
                if ($perPage == '' ||  $page == '' || $page <= 0) {
                    $perPage = 10;
                    $page = 0;
                }  
                $this->db->limit($perPage,$page);
		return $this->db->get()->result();
	}

	function searchDoctors($location, $speciality, $gender) {
		return $this->db->select()
						->from($this->_table)
						->get()
						->result();
	}

	function searchLab($gender, $speciality) {
		
		$where['work_type'] = 'Lab';

		if(isset($gender) && $gender != '') {
			
			if($gender == 1){
				$data = 1;
			}else{
				$data = 2;
			}
			$where['gender'] = $data;
		}

		if(isset($speciality) && $speciality != '') {
			$where['specialty_master.name_en'] = $speciality;
		}

		return $this->db->select("$this->_table.*, specialty_master.name_en AS SpecialityName, doctor_details.email, doctor_details.phone_number")
						->from($this->_table)
						->join('doctor_master_en', "$this->_table.doctor_id = doctor_master_en.doctor_id", 'left')
						->join('doctor_details', 'doctor_details.doctor_id = doctor_master_en.doctor_id', 'left')
						->join('specialty_master', 'specialty_master.specialties_id = doctor_details.speciality_id', 'left')
						->where($where)
						->get()
						->result();
	}

	function checkEmail($email){

		return $this->db->select()
				->where('email',$email)
				->get('work_master')
				->row();
	}
        
        /**
         * get all work master result
         * @param char $lang
         * @param int $userId
         * @return mixed array
         */
        function getAllWorkMaterResultNumRows($lang,$userId) {
            
            
		if($lang=='ar'){
			$this->db->select("work_master.work_id AS WorkId, work_master.name AS Name, work_master.address AS Address, work_master.phone AS Phone, work_master.photo AS Photo, 'null' AS DoctorId, work_master.work_type AS WorkType, 'null' AS Latitude, 'null' AS Longitude, 'null' AS Zoom, wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus,work_rating.average_score as RatingAvg, COUNT(work_rating.work_id) AS TotalReview");
		}else{
			$this->db->select("work_master.work_id AS WorkId, work_master.name_en AS Name, work_master.address_en AS Address, work_master.phone AS Phone, work_master.photo AS Photo, 'null' AS DoctorId, work_master.work_type AS WorkType, 'null' AS Latitude, 'null' AS Longitude, 'null' AS Zoom, wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus,work_rating.average_score as RatingAvg, COUNT(work_rating.work_id) AS TotalReview");
		}
		$this->db->from('work_master');

		$this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd 
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = work_master.work_id) AS Mipmap');

		$this->db->select('(SELECT GROUP_CONCAT(sm.name_en) FROM `doctor_details` dd 
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = work_master.work_id) AS Speciality');

		$this->db->join("worktype_bookmark_master wbm", "wbm.work_id = work_master.work_id AND wbm.user_id=".$userId, "left");
		$this->db->join("work_rating", "work_rating.work_id = work_master.work_id", "left");
		//$this->db->order_by('work_master.name_en');
		$this->db->where('work_master.status','1');
		$this->db->group_by("work_master.work_id");  
                $this->db->order_by('FIELD(work_type, "Hospital", "Clinic", "Lab")');
		return $this->db->get()->num_rows();
	}

	function all_work_master($lang = 'en') {
		if ($lang == 'ar') {
			$sql = "SELECT work_id AS id, COALESCE(NULLIF(name, ''), name_en) AS name FROM work_master";
			$sql .= " ORDER BY name_en ASC";
			$query = $this->db->query($sql);

			return $query->result_array();
			/*return $this->db->select('work_id AS id, name')
				->from('work_master')
				->order_by('name', 'ASC')
				->get()
				->result();*/
		} else {
			return $this->db->select('work_id AS id, name_en AS name')
				->from('work_master')
				->order_by('name', 'ASC')
				->get()
				->result();
		}
	}

	/**
	* Get Work details using url key
	* @param string $key
	* @return object 
	**/
	function get_name($key) {
		
		return $this->db->select("work_id, name_en AS Name")
			->from('work_master')
		    ->order_by('name', 'ASC')
			->where('work_master.url_key', $key)
			->get()
			->row();
	}
}