<?php

class Enquiry_master_model extends MY_Model {

	protected $_table = 'enquiry_master';

	protected $primary_key = 'enquiry_id';

	public $before_create = array('timestamps');

	public $before_update = array('update_timestamps');

	/**
	 * Set timestamps when creating new enquiry
	 * @param  array $enquiry_master 
	 * @return array
	 */
	protected function timestamps($enquiry_master) {
		$enquiry_master['created_at'] = $enquiry_master['updated_at'] = date('Y-m-d H:i:s');

		return $enquiry_master;
	}

	/**
	 * Set update timestamps when updating enquiry
	 * @param  array $enquiry_master 
	 * @return array
	 */
	protected function update_timestamps($enquiry_master) {
		$enquiry_master['updated_at'] = date('Y-m-d H:i:s');

		return $enquiry_master;
	}

	/**
	 * Get all enquiry list
	 * @return array
	 */
	public function get_all_enquiry() {
		$this->db->select()
			->from($this->_table)
			->order_by('created_at', 'DESC')
			->get()
			->result();
	}
}