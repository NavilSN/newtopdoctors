<?php

class Work_rating_model extends MY_Model {

	protected $primary_key = 'work_rating_id';

	function totalDoctorReview($id) {
		return $this->db->select()
			->from($this->_table)
			->where($this->primary_key, $id)
			->get()
			->num_rows();
	}

	function reviewRating($workId){

		return $this->db->select('AVG(reputation) AS Reputation, 
		 	SUM(IF(reputation > 0, 1, 0)) AS TotalReputation,AVG(clinic) as clinic,
		 	SUM(IF(clinic > 0, 1, 0)) AS totalClinic, AVG(availability) as availability,
		 	SUM(IF(availability > 0, 1, 0)) AS totalAvaibility, avg(approachability) as approachability , 
		 	SUM(IF(approachability > 0, 1, 0)) AS totalApproachability, avg(technology) as technology,
		 	SUM(IF(technology > 0, 1, 0)) AS totalTechnology,date_created as dateCreate')
		->where('work_id',$workId)
		->group_by('work_id')
		->get('work_rating')
		->row();

		 /*return $this->db->select('AVG(reputation) AS Reputation, COUNT(reputation) AS TotalReputation ,AVG(clinic) as clinic,COUNT(clinic) as totalClinic,AVG(availability) as availability,COUNT(availability) as totalAvaibility,avg(approachability) as approachability , count(approachability) as totalApproachability,avg(technology) as technology,COUNT(technology) as totalTechnology,date_created as dateCreate')
		->where('work_id',$workId)
		->group_by('work_id')
		->get('work_rating')
		->row();*/
	}

	function checkDuplicate($workId,$userId){
		return $this->db->select('*')
						->where('work_id',$workId)
						->where('user_id',$userId)
						->get('work_rating')
						->row();

	}

	function userDetail($workId){
            $base_url=base_url().'uploads/user_image/';
		$data = $this->db->select('work_rating.work_rating_id,work_rating.user_id,user_master.first_name,user_master.last_name,work_rating.date_created,user_master.address as address,work_rating.average_score,work_rating.comment,work_rating.date_created as dateCreate,work_rating.visibility AS visible,if(user_master.photo is null or user_master.photo = "", "'.$base_url.'user_profile_default_image.png", concat("'.$base_url.'",user_master.photo)) AS Photo,work_rating.reviewIcon')
						->join('user_master','user_master.user_id=work_rating.user_id', 'left')
						->where('work_id',$workId)
						->get('work_rating')
						->result();
	foreach ($data as $row) {
		if (trim($row->first_name) == '' || trim($row->last_name)) {
			$row->first_name = 'Anonymous';
			$row->last_name = '';
		}
	}
	return $data;
	}

	public function topTenDoctor($lang){
		if($lang=='ar'){
			$this->db->select('doctor_details.doctor_id as doctorId,doctor_master.first_name as firstname ,doctor_master.last_name as lastname, doctor_details.phone_number as phone,
			SUM(doctors_rating.average_score)/COUNT(doctors_rating.doctor_id) as ratingAvg, doctor_details.photo as photo,specialty_master.name as specialityName, specialty_master.specialties_id as specialityId ,specialty_master.mipmap as Mipmap,count(doctors_rating.doctor_id) as totalReview,doctor_master.address as address,bookmark_master.status as BookmarkStatus,bookmark_master.user_id AS BookmarkUserId');
		}else{
			$this->db->select('doctor_details.doctor_id as doctorId,doctor_master_en.first_name as firstname ,doctor_master_en.last_name as lastname, doctor_details.phone_number as phone,
			SUM(work_rating.average_score)/COUNT(work_rating.doctor_id) as ratingAvg, doctor_details.photo as photo,specialty_master.name_en as specialityName, specialty_master.specialties_id as specialityId ,specialty_master.mipmap as Mipmap,count(work_rating.doctor_id) as totalReview,doctor_master_en.address as address,worktype_bookmark_master.status as BookmarkStatus,worktype_bookmark_master.user_id AS BookmarkUserId');
		}
		$this->db->join('doctor_master_en','doctor_master_en.doctor_id=doctor_details.doctor_id','left');
		$this->db->join('work_rating','work_rating.work_id=doctor_details.work_id','both');
		$this->db->join('doctor_master','doctor_master.doctor_id=doctor_details.doctor_id','left');
		$this->db->join('specialty_master','specialty_master.specialties_id=doctor_details.speciality_id','left');
		$this->db->join('worktype_bookmark_master', 'worktype_bookmark_master.work_id = doctor_details.work_id', 'left');
		$this->db->limit(10);
		$this->db->group_by('doctors_rating.work_id');
		$this->db->order_by('ratingAvg', 'desc');
		return $this->db->get('doctor_details')->result();
		
	}

	function GetReview($userId,$workId){
		return $this->db->select('reputation,clinic,availability,approachability,technology,average_score as AvgScore,user_id as userId,work_id as workId')
				->where('user_id',$userId)
				->where('work_id',$workId)
				->get('work_rating')->row();
				
	}

	function UserByWorkReviews($userId,$lang){
		if($lang=='ar'){
			$this->db->select('wr.work_id as Id,CONCAT(wm.name) AS Name,wr.average_score as avgReview,wr.comment as comment,wr.date_created as dateCreate,wm.work_type,wr.reviewIcon');
		}else{
			$this->db->select('wr.work_id as Id,CONCAT(wm.name_en) AS Name,wr.average_score as avgReview,wr.comment as comment,wr.date_created as dateCreate,wm.work_type,wr.reviewIcon');
		}
		$this->db->join('work_master wm','wm.work_id=wr.work_id','left');
		$this->db->where('wr.user_id',$userId);
		return $this->db->get('work_rating wr')->result();

	}
        
        /**         
         * get rating details
         * @param int $reviewId
         * @return mixed array
         */
        function getRatingById($reviewId,$lang)
        {
           if($lang=="ar") 
           {
            $this->db->select('wr.reputation,wr.clinic,wr.availability,wr.approachability,wr.technology,wr.average_score as AvgScore,wr.comment,wr.visibility,CONCAT(u.first_name ," " , u.last_name) AS Name,wm.name AS WorkName');
           }
           else{
            $this->db->select('wr.reputation,wr.clinic,wr.availability,wr.approachability,wr.technology,wr.average_score as AvgScore,wr.comment,wr.visibility,CONCAT(u.first_name ," " , u.last_name) AS Name,wm.name_en AS WorkName');    
           }
            $this->db->from('work_rating wr');
            $this->db->join('user_master u','u.user_id=wr.user_id','left');
            $this->db->join('work_master wm','wm.work_id=wr.work_id','left');            
            $this->db->where('wr.work_rating_id',$reviewId);
            return $this->db->get()->result();
        }
	
}
