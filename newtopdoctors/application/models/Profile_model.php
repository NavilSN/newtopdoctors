<?php

class Profile_model extends CI_Model
{

    public function doctorProfile($id, $lang, $userId)
    {
        $id = (int) $id;
        if ($lang == 'ar') {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(dm.first_name, " ", dm.last_name) AS Name, dm.address AS Address, dd.email AS Email, IFNULL(dd.phone_number,"") AS Phone, dd.mobile_number AS Mobile, dd.photo AS Photo, dd.gender AS Gender, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude,dd.google_map_zoom AS Zoom,SUM(dr.average_score)/COUNT(dr.user_id) as RatingAvg, COUNT(dr.doctor_id) AS TotalReview, sm.mipmap as Mipmap');
        } else {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(dme.first_name, " ", dme.last_name) AS Name, dme.address AS Address, dd.email AS Email, IFNULL(dd.phone_number,"") AS Phone, dd.mobile_number AS Mobile, dd.photo AS Photo, dd.gender AS Gender, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude,dd.google_map_zoom AS Zoom,SUM(dr.average_score)/COUNT(dr.user_id) as RatingAvg, COUNT(dr.doctor_id) AS TotalReview, sm.mipmap as Mipmap');
        }
        $this->db->from('doctor_details dd');
        $this->db->join('doctor_master dm', 'dm.doctor_id = dd.doctor_id', 'left');
        $this->db->join('doctor_master_en dme', 'dme.doctor_id = dd.doctor_id', 'left');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left'); //
        $this->db->where('dd.doctor_id', $id);
        $this->db->limit(1);
        $this->db->group_by('dd.doctor_id');
        return $this->db->get()->result();
        //echo $this->db->last_query();die();
    }

    public function doctorProfileBookmark($id, $userId)
    {
        $id = (int) $id;
        $userId = (int) $userId;
        $this->db->select('um.user_id AS BookmarkUserId, bm.status AS BookmarkStatus');
        $this->db->from('doctor_details dd');
        $this->db->join('bookmark_master bm', 'bm.doctor_id = dd.doctor_id', 'left');
        $this->db->join('user_master um', 'um.user_id = bm.user_id', 'left');
        $this->db->where('dd.doctor_id', $id);
        $this->db->where('um.user_id', $userId);
        $this->db->group_by('dd.doctor_id');
        return $this->db->get()->row();
        //echo $this->db->last_query();die();
    }

    /**
     * Hospital profile details in Ar
     * @param  string $id
     * @return array
     */
    public function hospitalProfileAr($id = '')
    {
        $id = (int) $id;
        $this->db->select('wm.work_id AS HospitalId, wm.name AS Name, wm.address AS Address, wm.phone AS Phone, dd.working_hours AS WorkingHours, dd.photo AS Photo, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, lm.name AS Location,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg,COUNT(wr.work_id) AS TotalReview');
        $this->db->from('work_master wm');
        $this->db->join('doctor_details dd', 'dd.doctor_id = wm.doctor_id', 'left');
        $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
        $this->db->join('location_master lm', 'lm.location_id = wm.location_id', 'left');
        $this->db->where('wm.work_type', 'Hospital');
        $this->db->where('wm.status', '1');
        $this->db->where('wm.work_id', $id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }

    /**
     * Hospital profile detail in En
     * @param  string $id
     * @return array
     */
    public function hospitalProfileEn($id = '')
    {
        $id = (int) $id;
        $this->db->select('wm.work_id AS HospitalId, wm.name_en AS Name, wm.address_en AS Address, wm.phone AS Phone, dd.working_hours AS WorkingHours, dd.photo AS Photo, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, lm.name_en AS Location,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg,COUNT(wr.work_id) AS TotalReview');
        $this->db->from('work_master wm');
        $this->db->join('doctor_details dd', 'dd.doctor_id = wm.doctor_id', 'left');
        $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
        $this->db->join('location_master lm', 'lm.location_id = wm.location_id', 'left');
        $this->db->where('wm.work_type', 'Hospital');
        $this->db->where('wm.status', '1');
        $this->db->where('wm.work_id', $id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }

    /**
     * Hospital profile details
     * @param  string $id
     * @param  string $lang
     * @return array
     */
    public function hospitalProfile($id, $lang = 'en')
    {
        $result = array();

        if ($lang == 'ar') {
            $result = $this->hospitalProfileAr($id);
        } else {
            $result = $this->hospitalProfileEn($id);
        }

        return $result;
    }

    public function hospitalProfileBookmark($id, $userId)
    {
        $id = (int) $id;
        $userId = (int) $userId;
        $this->db->select('um.user_id AS BookmarkUserId, wbm.status AS BookmarkStatus');
        $this->db->from('work_master wm');
        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id', 'left');
        $this->db->join('user_master um', 'um.user_id = wbm.user_id', 'left');
        $this->db->where('wm.work_id', $id);
        $this->db->where('um.user_id', $userId);
        $this->db->where('wm.work_type', 'Hospital');
        $this->db->group_by('wm.work_id');
        return $this->db->get()->row();
    }

    public function clinicProfile($id, $lang)
    {
        $id = (int) $id;
        if ($lang == 'ar') {
            $this->db->select('wm.work_id AS ClinicId, wm.name AS Name, wm.address AS Address, wm.phone AS Phone, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom,sm.mipmap AS Mipmap,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg,COUNT(wr.work_id) AS TotalReview');
        } else {
            $this->db->select('wm.work_id AS ClinicId, wm.name_en AS Name, wm.address_en AS Address, wm.phone AS Phone, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom,sm.mipmap AS Mipmap,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg,COUNT(wr.work_id) AS TotalReview');
        }
        $this->db->from('work_master wm');
        $this->db->join('doctor_details dd', 'dd.work_id = wm.work_id', 'left');
        $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
        $this->db->where(array(
            'wm.work_type' => 'Clinic',
            'wm.status' => '1',
            'wm.work_id' => $id,
        ));
        $this->db->limit(1);
        return $this->db->get()->result();
        //echo $this->db->last_query(); die();
    }
    public function clinicProfileBookmark($id, $userId)
    {
        $id = (int) $id;
        $userId = (int) $userId;
        $this->db->select('um.user_id AS BookmarkUserId, wbm.status AS BookmarkStatus');
        $this->db->from('work_master wm');
        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id', 'left');
        $this->db->join('user_master um', 'um.user_id = wbm.user_id', 'left');
        $this->db->where('wm.work_id', $id);
        $this->db->where('um.user_id', $userId);
        $this->db->where('wm.work_type', 'Clinic');
        $this->db->group_by('wm.work_id');
        return $this->db->get()->row();
    }

    public function labProfile($id, $lang)
    {
        $id = (int) $id;
        if ($lang == 'ar') {
            $this->db->select('wm.work_id AS LabId, wm.name AS Name, wm.address AS Address, wm.phone AS Phone, dd.photo AS Photo, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom,sm.mipmap AS Mipmap,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg,COUNT(wr.work_id) AS TotalReview');
        } else {

            $this->db->select('wm.work_id AS LabId, wm.name_en AS Name, wm.address_en AS Address, wm.phone AS Phone, dd.photo AS Photo, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom,sm.mipmap AS Mipmap,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg,COUNT(wr.work_id) AS TotalReview');
        }
        $this->db->from('work_master wm');
        $this->db->join('doctor_details dd', 'dd.doctor_id = wm.doctor_id', 'left');
        $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
        $this->db->where(array(
            'wm.work_id' => $id,
            'wm.status' => '1',
        ));
        $this->db->limit(1);
        //'wm.work_type'    => 'Lab',
        return $this->db->get()->result();
    }

    public function labProfileBookmark($id, $userId)
    {
        $id = (int) $id;
        $userId = (int) $userId;
        $this->db->select('um.user_id AS BookmarkUserId, wbm.status AS BookmarkStatus');
        $this->db->from('work_master wm');
        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id', 'left');
        $this->db->join('user_master um', 'um.user_id = wbm.user_id', 'left');
        $this->db->where('wm.work_id', $id);
        $this->db->where('um.user_id', $userId);
        //$this->db->where('wm.work_type', 'Lab');
        $this->db->group_by('wm.work_id');
        return $this->db->get()->row();
    }

    public function countDoctors($workId)
    {
        $workId = (int) $workId;
        return $this->db->select()
            ->from('doctor_details')
            ->where('doctor_details.work_id', $workId)
            ->get()
            ->num_rows();
    }

    public function hospitalSpecialities($workId, $lang)
    {
        $workId = (int) $workId;
        if ($lang == 'ar') {
            $this->db->select('sm.name AS SpecialityName,sm.mipmap as Mipmap');
        } else {
            $this->db->select('sm.name_en AS SpecialityName,sm.mipmap as Mipmap');
        }
        $this->db->from('specialty_master sm');
        $this->db->join('doctor_details dd', 'dd.speciality_id = sm.specialties_id', 'left');
        $this->db->join('work_master wm', 'wm.work_id = dd.work_id', 'left');
        $this->db->where('wm.work_id', $workId);
        $this->db->group_by('sm.specialties_id');
        return $this->db->get()->result();
    }

    public function totalDoctorReview()
    {
        $this->load->model('Doctors_rating_model');
        return $this->Doctors_rating_model->totalDoctorReview();
    }

    public function totalClinicReview()
    {}

    public function totalLabReview()
    {}

    /**
     * Hospital doctor list
     *
     * @param $hospitalId int
     */
    public function hospitalDoctorList($hospitalId, $lang, $userId)
    {
        $hospitalId = (int) $hospitalId;
        $userId = (int) $userId;
        if ($lang == 'ar') {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(d.first_name, " ", d.last_name) AS Name, d.address AS Address, dd.phone_number AS Phone, dd.mobile_number AS Mobile, dd.working_hours AS WorkingHours, dd.gender AS Gender, dd.photo AS Photo, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, sm.name AS Speciality, cm.country_name AS Location,dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,sm.mipmap as Mipmap, bm.status AS BookmarkStatus, bm.user_id AS BookmarkUserId');
        } else {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(dm.first_name, " ", dm.last_name) AS Name, dm.address AS Address, dd.phone_number AS Phone, dd.mobile_number AS Mobile, dd.working_hours AS WorkingHours, dd.gender AS Gender, dd.photo AS Photo, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, sm.name_en AS Speciality, cm.country_name AS Location,dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,sm.mipmap as Mipmap, bm.status AS BookmarkStatus, bm.user_id AS BookmarkUserId');
        }
        $this->db->from('doctor_master_en dm');
        $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
        $this->db->join('doctor_master d', 'd.doctor_id = dm.doctor_id', 'left');
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
        $this->db->join('country_master cm', 'cm.country_id = dd.location_id', 'left');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
        $this->db->join('bookmark_master bm', 'bm.doctor_id = dd.doctor_id AND bm.user_id = ' . $userId, 'left');
        $this->db->where(array(
            'dd.work_id' => $hospitalId,
        ));
        $this->db->group_by('dd.doctor_id');
        return $this->db->get()->result();
    }

    public function workTypeTotalDoctorList($workId, $lang, $userId, $page, $limit = 10)
    {
        $workId = (int) $workId;
        $userId = (int) $userId;
        $offset = ($page - 1) * 10;
        if ($lang == 'ar') {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(d.first_name, " ", d.last_name) AS Name, d.address AS Address, dd.phone_number AS Phone, dd.mobile_number AS Mobile, dd.working_hours AS WorkingHours, dd.gender AS Gender, dd.photo AS Photo, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, sm.name AS Speciality, cm.country_name AS Location,dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,sm.mipmap as Mipmap, bm.status AS BookmarkStatus, bm.user_id AS BookmarkUserId');
        } else {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(dm.first_name, " ", dm.last_name) AS Name, dm.address AS Address, dd.phone_number AS Phone, dd.mobile_number AS Mobile, dd.working_hours AS WorkingHours, dd.gender AS Gender, dd.photo AS Photo, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, sm.name_en AS Speciality, cm.country_name AS Location,dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,sm.mipmap as Mipmap, bm.status AS BookmarkStatus, bm.user_id AS BookmarkUserId');
        }
        $this->db->from('doctor_master_en dm');
        $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
        $this->db->join('doctor_master d', 'd.doctor_id = dm.doctor_id', 'left');
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
        $this->db->join('country_master cm', 'cm.country_id = dd.location_id', 'left');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
        $this->db->join('bookmark_master bm', 'bm.doctor_id = dd.doctor_id AND bm.user_id = ' . $userId, 'left');
        $this->db->where(array(
            'dd.work_id' => $workId,
        ));
        $this->db->limit($limit, $offset);
        $this->db->group_by('dd.doctor_id');

        return $this->db->get()->result();
    }

    public function totalWorkTypeDoctorCount($workId)
    {
        return $this->countDoctors($workId);
    }

    public function userProfile($userId)
    {
        $userId = (int) $userId;
        $base_url = base_url() . 'uploads/user_image/';
        return $this->db->select('user_master.user_id as userId,CONCAT(user_master.first_name, " ", user_master.last_name) AS Name,user_master.first_name AS firstName,user_master.last_name AS lastName, user_master.email AS email,user_master.mobile_number as phone,IF(country_master.country_name IS NULL or country_master.country_name = "0", "", country_master.country_name) AS location, if(user_master.photo is null or user_master.photo = "", "' . $base_url . 'user_profile_default_image.png", concat("' . $base_url . '",user_master.photo)) AS Photo')

            ->select('(SELECT count(*) FROM `work_rating` WHERE work_rating.user_id = ' . $userId . ') as WorkReview')
            ->select('(SELECT count(*) FROM `doctors_rating` WHERE doctors_rating.user_id = ' . $userId . ') as DoctorReview')
            ->join('country_master', 'country_master.country_id=user_master.country_id', 'left')
            ->join('work_rating', 'work_rating.user_id=user_master.user_id', 'left')
            ->join('doctors_rating', 'doctors_rating.user_id=user_master.user_id', 'left')
            ->where('user_master.user_id', $userId)
            ->group_by('doctors_rating.user_id')
            ->get('user_master')
            ->result();

        //echo $this->db->last_query(); die();
    }

    public function workTypeDoctorList($workId, $lang, $userId)
    {
        $workId = (int) $workId;
        $userId = (int) $userId;
        if ($lang == 'ar') {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(d.first_name, " ", d.last_name) AS Name, d.address AS Address, dd.phone_number AS Phone, dd.mobile_number AS Mobile, dd.working_hours AS WorkingHours, dd.gender AS Gender, dd.photo AS Photo, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, sm.name AS Speciality, cm.country_name AS Location,dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,sm.mipmap as Mipmap, bm.status AS BookmarkStatus, bm.user_id AS BookmarkUserId');
        } else {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(dm.first_name, " ", dm.last_name) AS Name, dm.address AS Address, dd.phone_number AS Phone, dd.mobile_number AS Mobile, dd.working_hours AS WorkingHours, dd.gender AS Gender, dd.photo AS Photo, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, sm.name_en AS Speciality, cm.country_name AS Location,dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,sm.mipmap as Mipmap, bm.status AS BookmarkStatus, bm.user_id AS BookmarkUserId');
        }
        $this->db->from('doctor_master_en dm');
        $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
        $this->db->join('doctor_master d', 'd.doctor_id = dm.doctor_id', 'left');
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
        $this->db->join('country_master cm', 'cm.country_id = dd.location_id', 'left');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
        $this->db->join('bookmark_master bm', 'bm.doctor_id = dd.doctor_id AND bm.user_id = ' . $userId, 'left');
        $this->db->where(array(
            'dd.work_id' => $workId,
        ));
        $this->db->group_by('dd.doctor_id');
        return $this->db->get()->result();
    }

    public function workTypeSpeciality($workId)
    {
        $workId = (int) $workId;
        $speciality = $this->db->select('sp.mipmap')
            ->from('specialty_master sp')
            ->join('doctor_details dd', 'dd.speciality_id = sp.specialties_id', 'left')
            ->join('work_master w', 'w.work_id = dd.work_id', 'left')
            ->where('w.work_id', $workId)
            ->group_by('sp.specialties_id')
            ->get()
            ->result();
        $mipmap = array();
        foreach ($speciality as $row) {
            array_push($mipmap, $row->mipmap);
        }

        return $mipmap;
    }
}
