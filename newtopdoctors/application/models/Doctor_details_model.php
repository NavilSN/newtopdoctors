<?php

class Doctor_details_model extends My_model {

	private $table_name = 'doctor_details';

	function save($data) {
		$this->db->insert($this->table_name, $data);
	}

	/**
	 * Get doctor details
	 * @param  string $id 
	 * @return object
	 */
	function doctor_details($id) {
		return $this->db->select()
			->from('doctor_details')
			->join('doctor_master_en', 'doctor_master_en.doctor_id = doctor_details.doctor_id')
			->where('doctor_details.doctor_id', $id)
			->get()
			->row();
	}

	/**
	* Get Doctor details using url key
	* @param string $key
	* @return object 
	**/
	function get_name($key) {
		
		return $this->db->select("CONCAT(doctor_master_en.first_name, ' ', doctor_master_en.last_name) AS Name,doctor_master_en.doctor_id")
			->from('doctor_details')
			->join('doctor_master_en', 'doctor_master_en.doctor_id = doctor_details.doctor_id')
			->where('doctor_details.url_key', $key)
			->get()
			->row();
	}
}