<?php

class Doctors_rating_model extends MY_Model
{

    protected $primary_key = 'id';

    public function totalDoctorReview($id)
    {
        return $this->db->select()
            ->from($this->_table)
            ->where($this->primary_key, $id)
            ->get()
            ->num_rows();
    }

    public function reviewRating($doctorId)
    {

        return $this->db->select('AVG(reputation) AS Reputation,
		 	SUM(IF(reputation > 0, 1, 0)) AS TotalReputation,AVG(clinic) as clinic,
		 	SUM(IF(clinic > 0, 1, 0)) AS totalClinic, AVG(availability) as availability,
		 	SUM(IF(availability > 0, 1, 0)) AS totalAvaibility, avg(approachability) as approachability ,
		 	SUM(IF(approachability > 0, 1, 0)) AS totalApproachability, avg(technology) as technology,
		 	SUM(IF(technology > 0, 1, 0)) AS totalTechnology,date_created as dateCreate')
            ->where('doctor_id', $doctorId)
            ->group_by('doctor_id')
            ->get('doctors_rating')
            ->row();

        /*return $this->db->select('AVG(reputation) AS Reputation,
    SUM(IF(clinic = 1, 1, 0)) AS TotalReputation,
    , COUNT(reputation) AS TotalReputation ,AVG(clinic) as clinic,COUNT(clinic) as totalClinic,AVG(availability) as availability,COUNT(availability) as totalAvaibility,avg(approachability) as approachability , count(approachability) as totalApproachability,avg(technology) as technology,COUNT(technology) as totalTechnology,date_created as dateCreate')
    ->where('doctor_id',$doctorId)
    ->group_by('doctor_id')
    ->get('doctors_rating')
    ->row();*/
    }

    public function checkDuplicate($doctorId, $userId)
    {
        return $this->db->select('*')
            ->where('doctor_id', $doctorId)
            ->where('user_id', $userId)
            ->get('doctors_rating')
            ->row();

    }

    public function userDetail($doctorId)
    {
        $base_url = base_url() . 'uploads/user_image/';

        $user = $this->db->select('doctors_rating.rating_id,doctors_rating.user_id,user_master.first_name,user_master.last_name,doctors_rating.date_created,user_master.address as address,doctors_rating.average_score,doctors_rating.comment,doctors_rating.date_created as dateCreate,doctors_rating.visibility AS visible,if(user_master.photo is null or user_master.photo = "", "' . $base_url . 'user_profile_default_image.png", concat("' . $base_url . '",user_master.photo)) AS Photo,doctors_rating.reviewIcon')
            ->join('user_master', 'user_master.user_id=doctors_rating.user_id', 'left')
            ->where('doctor_id', $doctorId)->order_by('dateCreate', 'DESC')
            ->get('doctors_rating')
            ->result();
        foreach ($user as $row) {
            if (trim($row->first_name) == '') {
                $row->first_name = 'Anonymous';
            }
        }
        return $user;
    }

    public function topTenDoctor($lang, $user_id = 0)
    {
        if ($lang == 'ar') {
            $this->db->select('doctor_details.doctor_id as doctorId,doctor_master.first_name as firstname ,doctor_master.last_name as lastname, doctor_details.phone_number as phone, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE doctor_master.doctor_id=doctors_rating.doctor_id) as ratingAvg, doctor_details.photo as photo,specialty_master.name as specialityName, specialty_master.specialties_id as specialityId ,specialty_master.mipmap as Mipmap,count(doctors_rating.doctor_id) as totalReview,doctor_master.address as address,bookmark_master.status as BookmarkStatus,bookmark_master.user_id AS BookmarkUserId');
            $this->db->join('doctor_master', 'doctor_master.doctor_id=doctor_details.doctor_id', 'left');
        } else {
            $this->db->select('doctor_details.doctor_id as doctorId,doctor_master_en.first_name as firstname ,doctor_master_en.last_name as lastname, doctor_details.phone_number as phone, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE doctor_master_en.doctor_id=doctors_rating.doctor_id) as ratingAvg, doctor_details.photo as photo,specialty_master.name_en as specialityName, specialty_master.specialties_id as specialityId ,specialty_master.mipmap as Mipmap,count(doctors_rating.doctor_id) as totalReview,doctor_master_en.address as address,bookmark_master.status as BookmarkStatus,bookmark_master.user_id AS BookmarkUserId');
            $this->db->join('doctor_master_en', 'doctor_master_en.doctor_id=doctor_details.doctor_id', 'left');
        }

        $this->db->join('doctors_rating', 'doctors_rating.doctor_id=doctor_details.doctor_id', 'both');
        $this->db->join('specialty_master', 'specialty_master.specialties_id=doctor_details.speciality_id', 'left');
        $this->db->join('bookmark_master', 'bookmark_master.doctor_id = doctor_details.doctor_id AND bookmark_master.user_id = ' . $user_id, 'left');
        $this->db->order_by('ratingAvg DESC');
        $this->db->limit(10);
        $this->db->group_by('doctors_rating.doctor_id');
        $this->db->having('totalReview >', 50);

        return $this->db->get('doctor_details')->result();

    }

    public function GetReview($userId, $doctorId)
    {
        return $this->db->select('reputation,clinic,availability,approachability,technology,average_score as AvgScore,user_id as userId,doctor_id as doctorId')
            ->where('user_id', $userId)
            ->where('doctor_id', $doctorId)
            ->get('doctors_rating')->row();

    }

    public function UserByDoctorReviews($userId, $lang)
    {
        if ($lang == 'ar') {
            $this->db->select('dr.doctor_id as Id,CONCAT(dme.first_name, " ", dme.last_name) AS Name,dr.average_score as avgReview,dr.comment as comment,dr.date_created as dateCreate,dr.reviewIcon');
        } else {
            $this->db->select('dr.doctor_id as Id,CONCAT(dme.first_name, " ", dme.last_name) AS Name,dr.average_score as avgReview,dr.comment as comment,dr.date_created as dateCreate,dr.reviewIcon');
        }
        $this->db->join('doctor_master_en dme', 'dme.doctor_id=dr.doctor_id', 'left');
        $this->db->where('user_id', $userId);
        return $this->db->get('doctors_rating dr')->result();

    }

    /**
     * get rating details
     * @param int $reviewId
     * @return mixed array
     */
    public function getRatingById($reviewId, $lang)
    {
        $this->db->select('dr.reputation,dr.clinic,dr.availability,dr.approachability,dr.technology,dr.average_score as AvgScore,dr.comment,dr.visibility,CONCAT(u.first_name ," " , u.last_name) AS Name,CONCAT(dm.first_name ," " , dm.last_name) AS DoctorName');
        if ($lang == "ar") {
            $this->db->join('doctor_master dm', 'dm.doctor_id=dr.doctor_id', 'left');
        } else {
            $this->db->join('doctor_master_en dm', 'dm.doctor_id=dr.doctor_id', 'left');
            // $this->db->select('dr.reputation,dr.clinic,dr.availability,dr.approachability,dr.technology,dr.average_score as AvgScore,dr.comment,dr.visibility,CONCAT(u.first_name ," " , u.last_name) AS Name,CONCAT(dm.first_name ," " , dm.last_name) AS DoctorName');
        }
        $this->db->from('doctors_rating dr');
        $this->db->join('user_master u', 'u.user_id=dr.user_id', 'left');

        $this->db->where('dr.rating_id', $reviewId);
        return $this->db->get()->result();
    }

}
