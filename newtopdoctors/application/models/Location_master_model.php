<?php

class Location_master_model extends MY_Model {

	protected $primary_key = 'location_id';

	function locationDetails($name,$lang) {
		$this->db->select('location_id');
		$this->db->from($this->_table);
		if($lang=='ar'){
			$this->db->where('name', $name);
			$this->db->where('status', '1');
		}else{
			$this->db->where('name_en', $name);
			$this->db->where('status', '1');
		}
		return $this->db->get()->row();
	}

	function get_locationEN(){

		return $this->db->select('location_id AS locationId,name_en AS name')
						->from($this->_table)
						->where('status', '1')
						->order_by('name_en','ASC')						
						->get()
						->result();
	}

	function get_locationAR(){

		return $this->db->select('location_id AS locationId,name AS name')
						->from($this->_table)
						->where('status', '1')
						->order_by('name_en','ASC')						
						->get()
						->result();
	}
}