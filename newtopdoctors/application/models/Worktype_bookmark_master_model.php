<?php

class Worktype_bookmark_master_model extends MY_Model {
	protected $primary_key = 'worktype_bookmark_id';

	public function checkMain($workId,$userId){
			   return $this->db->select('wbm.worktype_bookmark_id,wbm.status,wm.name_en')
						->where(array(
							'wbm.user_id'	=> $userId,
							'wbm.work_id'	=> $workId
						))
						->join('work_master wm','wbm.work_id=wm.work_id')
						->get('worktype_bookmark_master wbm')->row();
	}

	public function bookmarkList($userId){
		return $this->db->select('work_master.work_id as Id,work_master.name_en as Name,work_master.phone as phone,work_master.photo as photo,work_master.address_en as address,work_rating.average_score as ratingAvg,work_master.work_type AS worktype,COUNT(work_rating.work_id) AS TotalReview')
			->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd 
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = work_master.work_id) AS Mipmap')

					->join('work_rating','work_rating.work_id=worktype_bookmark_master.work_id','left')
					->join('work_master','work_master.work_id=worktype_bookmark_master.work_id','left')
					->where('worktype_bookmark_master.user_id',$userId)
					->where('worktype_bookmark_master.status','1')
					->order_by('ratingAvg', 'DESC')
					->group_by('worktype_bookmark_master.work_id')
					->get('worktype_bookmark_master')
					->result();
	}

}
