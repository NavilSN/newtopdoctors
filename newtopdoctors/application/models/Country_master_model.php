<?php

class Country_master_model extends MY_Model {

	protected $primary_key = 'country_id';

	function countryDetails($name) {
		return $this->db->select('country_id')
						->from($this->_table)
						->where('country_name', $name)
						->get()->row();
	}
}