<?php

class Doctor_master_en_model extends CI_Model {

	private $table_name = "doctor_master_en";

	private $details_table = "doctor_details";

	function searchDoctors($location, $speciality, $gender) {

		//check for locatio is empty
		//if not add it in $where array
		if(isset($location) && $location != '') 
			$where['country_name'] = $location;

		//check for speciality
		if(isset($speciality) && $speciality != '')
			$where['name_en'] = $speciality;

		//check for gender
		if(isset($gender) && $gender != '') {
			$data = '1';
			if($gender == 'Male') {
				$data = '1';
			} elseif($gender == 'Female') {
				$data = '2';
			}
			$where['gender'] = $data;
		}

		return $this->db->select("dm.doctor_id AS DoctorId, CONCAT(dm.first_name, ' ', dm.last_name) AS Name, dm.address AS Address, dd.phone_number AS Phone")
			->from('doctor_master_en dm')
			->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left')
			->join('specialty_master sp', 'sp.specialties_id = dd.speciality_id', 'left')
			->join('country_master cm', 'cm.country_id = dd.location_id', 'left')
			->where($where)
			->get()
			->result();
	}

	function save($data) {
		$this->db->insert($this->table_name, $data);

		return $this->db->insert_id();
	}
        
        function getNearByDoctor($filter)
        {
              $filter['page'] = ($filter['page'] - 1);
                $page = ($filter['page'] * $filter['perPage']);
               $radius_in_kms = '2';
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(TRIM(dm.first_name), " ", TRIM(dm.last_name)) AS Name, dm.address AS Address, dd.phone_number AS Phone,sm.mipmap as Mipmap, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE dd.doctor_id=doctors_rating.doctor_id ) as RatingAvg, COUNT(dr.rating_id) AS TotalReview,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS 
                Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus');

            if($filter['lat']!="" || $filter['long']!="" )
            {
                $this->db->select('SQRT(POW(69.1 * (dd.google_map_latitude - ' . $filter["lat"] . '), 2) + POW(69.1 * ('. $filter["long"] .' - dd.google_map_longtude) * COS(dd.google_map_latitude / 57.3), 2)) AS distance');
                //$this->db->order_by('distance', 'ASC');
            }

            $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
            $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
            $this->db->join('doctors_rating dr', 'dr.doctor_id = dm.doctor_id', 'left');
            $this->db->join('bookmark_master bm', 'bm.doctor_id = dd.doctor_id AND bm.user_id ='.$filter['userId'], 'left');
            if($filter['lang']=="ar")
            {
                $this->db->from('doctor_master dm');
                $this->db->where('dm.first_name != ', '');
                $this->db->or_where('dm.last_name != ', '');
            }
            else{
                $this->db->from('doctor_master_en dm');
                $this->db->where('dm.first_name != ', '');
                $this->db->or_where('dm.last_name != ', '');
            }
            if($filter['topratestfirst']=="1")
            {
                $this->db->order_by('RatingAvg','DESC');
            }
            
            if($filter['lat']!="" || $filter['long']!="" )
            {
                $this->db->order_by('distance', 'ASC');
                 $this->db->having('distance <= ', 10);
                //$this->db->order_by('distance');
                //$this->db->limit(10);
                /*$this->db->where("(6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filter['lat'] . " - dd.google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filter['lat'] . " * PI() / 180)
                                    * COS(dd.google_map_latitude * PI() / 180) * POWER(SIN((" . $filter['long'] . " - dd.google_map_longtude) * PI() / 180 / 2), 2))) <= '10')");*/
            } 
            if ($filter['perPage'] == '' || $page == '' || $page <= 0) {
                                    $filter['perPage'] = 10;
                                    $page = 0;
                                }
            $this->db->limit($filter['perPage'],$page);
            $this->db->group_by('dd.doctor_id');

            
           return $this->db->get()->result();
            
            //echo $this->db->last_query();

            
            /*return $this->db->query("SELECT * FROM doctor_details WHERE (6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filter['lat'] . " - google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filter['lat'] . " * PI() / 180)
                                    * COS(google_map_latitude * PI() / 180) * POWER(SIN((" . $filter['long'] . " - google_map_longtude) * PI() / 180 / 2), 2))) <= '5')")->result();*/
            
          
        }
        
        function getNearByDoctorNumRows($filter)
        {
            
            $radius_in_kms = '2';
            
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(dm.first_name, " ", dm.last_name) AS Name, dm.address AS Address, dd.phone_number AS Phone,sm.mipmap as Mipmap,dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus');
            
            $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
            $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
            $this->db->join('doctors_rating dr', 'dr.doctor_id = dm.doctor_id', 'left');
            $this->db->join('bookmark_master bm', 'bm.doctor_id = dd.doctor_id AND bm.user_id ='.$filter['userId'], 'left');
            if($filter['lang']=="ar")
            {
                $this->db->from('doctor_master dm');
            }
            else{
                $this->db->from('doctor_master_en dm');
            }
            if($filter['lat']!="" || $filter['long']!="")
            {
            $this->db->where("(6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filter['lat'] . " - dd.google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filter['lat'] . " * PI() / 180)
                                    * COS(dd.google_map_latitude * PI() / 180) * POWER(SIN((" . $filter['long'] . " - dd.google_map_longtude) * PI() / 180 / 2), 2))) <= '10')");
            }
            
            
            $this->db->group_by('dd.doctor_id');
           return $this->db->get()->num_rows();
        }
        
}
