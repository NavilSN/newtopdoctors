<?php

class Doctor_master_model extends MY_Model {

	private $table_name = 'doctor_master';

	function save($data) {
		$this->db->insert($this->table_name, $data);

		return $this->db->insert_id();
	}
}