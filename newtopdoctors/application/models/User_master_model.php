<?php

class User_master_model extends MY_Model {
	protected $primary_key = 'user_id';

	function login_details($username, $password) {
		return $this->db->select()
						->from($this->_table)
						->join('country_master', "country_master.country_id = $this->_table.country_id", "left")
						->where(array(
							'email'	=> $username,
							'password'	=> $password
						))
						->get()
						->row();
	}

	function checkUserEmail($email){
		return $this->db->select()
						->where('email', $email)
						->get('user_master')->row();
	}
	function checkUserPrivateEmail($userId,$email){
		 return $this->db->select()
						->where('email', $email)
						->where('user_id !=', $userId)
						->get('user_master')->row();

						//echo $this->db->last_query(); die();
	}
        
        function getRecentView($userId,$lang,$recentlyViewed)
        {
           $result = array();
           
            $recentlyViewed = explode(',', $recentlyViewed);
          
            foreach($recentlyViewed as $data):
                if (strpos($data, 'D') !== false) {
                     $doctor = explode("D",$data);
                     $doctorId = $doctor[1];
                     $this->db->select('CONCAT(dm.first_name ," " , dm.last_name) AS Name, SUM(dr.average_score)/COUNT(dr.doctor_id) as ratingAvg,COUNT(dr.doctor_id)  AS totalreview, dd.google_map_latitude AS lat, dd.google_map_longtude As long,dm.address As Address, dd.phone_number AS phone,dd.photo AS Photo, bm.user_id AS BookmarkUserId,bm.status AS BookmarkStatus, "Doctor" AS worktype, dm.doctor_id AS ID, sp.mipmap AS Mipmap');
                     if($lang=="ar")
                     {
                     $this->db->from('doctor_master dm');
                     }
                     else 
                     {
                     $this->db->from('doctor_master_en dm');    
                     }
                     
                     $this->db->join('doctors_rating dr','dr.doctor_id=dm.doctor_id','left');
                     $this->db->join('doctor_details dd','dd.doctor_id=dm.doctor_id','left');
                     $this->db->join('bookmark_master bm', 'bm.doctor_id = dd.doctor_id AND bm.user_id = '.$userId, 'left');
                     $this->db->join('specialty_master sp', 'sp.specialties_id = dd.speciality_id', 'left');                      
                     $this->db->where('dm.doctor_id',$doctorId);
                     $this->db->where('dm.first_name != ', '');
                     $this->db->where('CONCAT(dm.first_name ," ", dm.last_name) IS NOT NULL', NULL, FALSE);
                     //$this->db->or_where('dm.last_name != ', '');                 
                     $recentlyViewedData = $this->db->get()->result();  
                }
                if (strpos($data, 'W') !== false) {
                     $work = explode("W",$data);
                     $workId = $work[1];
                     if($lang=="ar") {
                     $this->db->select('wm.name AS Name, SUM(wr.average_score)/COUNT(wr.work_id) as ratingAvg,COUNT(wr.work_id) AS totalreview, "null" AS lat, "null"  As long,wm.address As Address, wm.phone AS phone,wm.photo AS Photo, wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus, wm.work_type AS worktype, wm.work_id AS ID, GROUP_CONCAT(DISTINCT sp.mipmap SEPARATOR ",") AS Mipmap');                     
                     } else{
                         $this->db->select('wm.name_en AS Name, SUM(wr.average_score)/COUNT(wr.work_id) as ratingAvg,COUNT(wr.work_id) AS totalreview, "null" AS lat, "null"  As long,wm.address_en As Address, wm.phone AS phone,wm.photo AS Photo, wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus, wm.work_type AS worktype, wm.work_id AS ID, GROUP_CONCAT(DISTINCT sp.mipmap SEPARATOR ",") AS Mipmap');                     
                     }
                     $this->db->from('work_master wm');
                     $this->db->join('work_rating wr','wr.work_id=wm.work_id','left');      
                     $this->db->join('doctor_details dd', 'dd.work_id = wm.work_id', 'left');
                     $this->db->join('specialty_master sp', 'sp.specialties_id = dd.speciality_id', 'left');
                     $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id AND wbm.user_id = '.$userId, 'left');
                     $this->db->where('wm.work_id',$workId);
                     if($lang=="ar") {
                        $this->db->where('wm.name IS NOT NULL', NULL, FALSE);  
                        $this->db->where('wm.name != ', '');  
                     } else {
                        $this->db->where('wm.name_en IS NOT NULL', NULL, FALSE);  
                        $this->db->where('wm.name_en != ', '');  
                     }
                            
                     $this->db->group_by('wm.work_id');      
                     $recentlyViewedData = $this->db->get()->result();  
                 
                }
                  
              $result = array_merge($result,$recentlyViewedData);
               
            endforeach;
            return $result;
            //$this->db->get('');
        }
}