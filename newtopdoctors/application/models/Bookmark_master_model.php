<?php

class Bookmark_master_model extends MY_Model {
	protected $primary_key = 'bookmark_id';

	public function checkMain($doctorId,$userId){
			return $this->db->select()
						->where(array(
							'bm.user_id'	=> $userId,
							'bm.doctor_id'	=> $doctorId
						))
						->join('doctor_master_en dm','dm.doctor_id=bm.doctor_id')
						->get('bookmark_master bm')->row();
	}

	public function bookmarkList($userId){
		return $this->db->select('bookmark_master.doctor_id as Id,CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS Name, doctor_details.phone_number as phone,doctor_details.photo as photo,doctor_master_en.address as address,doctors_rating.average_score as ratingAvg, group_concat(sm.mipmap) AS MipMap,"Doctor" AS worktype,COUNT(doctors_rating.doctor_id) AS TotalReview')
					->join('user_master','user_master.user_id=bookmark_master.user_id','LEFT')
					->join('doctors_rating','doctors_rating.doctor_id=bookmark_master.doctor_id','LEFT')
					->join('doctor_master_en','doctor_master_en.doctor_id=bookmark_master.doctor_id','LEFT')
					->join('doctor_details','doctor_details.doctor_id=bookmark_master.doctor_id','LEFT')
					->join('specialty_master sm','sm.specialties_id=doctor_details.speciality_id','LEFT')
					->where('bookmark_master.user_id',$userId)
					->where('bookmark_master.status','1')
					->order_by('ratingAvg', 'DESC')
					->group_by('bookmark_master.doctor_id')
					->get('bookmark_master')
					->result();

	}

}
