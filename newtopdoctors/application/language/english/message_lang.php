<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*  Start Header*/
$lang['title'] = 'Top Doctors';
$lang['errorLogin'] = 'Invalid email or password';
$lang['registerSuccess'] = 'Registration Successully';
$lang['forgotpasswordsuccess'] = 'Please check email to new password';
$lang['forgotEmailNotFound'] = 'Email is not registered in the system.';
$lang['menuHome'] = 'Home';
$lang['menuDoctors'] = 'DOCTORS';
$lang['menuContactUs'] = 'CONTACT US';
$lang['menuSignIn'] = 'Sign In';
$lang['menuOR'] = 'OR';
$lang['menuRegister'] = 'REGISTER';
$lang['menuSignIn'] = 'Sign In';
$lang['menuMenu'] = 'Menu';

/*  End Header*/

/*  Start Footer*/


$lang['footerContent'] = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ';
$lang['footerAddress'] = '123 Eccles Old Road, New Salford Road, East';
$lang['footerEmail'] = 'info@domain.com';
$lang['footerPhone'] = '+44 123 456 788-9';
$lang['footerContentB'] = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ';
$lang['footerAddressB'] = '123 Eccles Old Road, New Salford Road, East';
$lang['footerEmailB'] = 'info@domain.com';
$lang['footerPhoneB'] = '+44 123 456 788-9';
$lang['footerUseFullLink'] = 'Useful Links';
$lang['footerMenuAboutUs'] = 'About Us';
$lang['footerMenuLatestBlog'] = 'Latest Blog';
$lang['footerMenuTermCondition'] = 'Terms and Conditions';
$lang['footerMenuPrivacyPolicy'] = 'Privacy Policy';
$lang['footerMenuContactUS'] = 'Contact Us';
$lang['footerLine'] = date('Y').' All Rights Reserved';
$lang['footerCopyRight'] = 'DocDirect';
$lang['Login'] = 'Login';
$lang['Register'] = 'Register';
$lang['loginEmail'] = 'E-mail';
$lang['loginPassword'] = 'Password';
$lang['loginForgotPass'] = 'Forgot Password ?';
$lang['loginSubmitBTN'] = 'Login';
$lang['loginFacebook'] = 'Login with Facebook';
$lang['loginGuest'] = 'Enter as a  guest';
$lang['regFirstName'] = 'FirstName';
$lang['regLastName'] = 'LastName';
$lang['regEmail'] = 'E-mail';
$lang['regMobileNumber'] = 'Mobile Number';
$lang['regPassword'] = 'Password';
$lang['regCPassword'] = 'Confirm Password';
$lang['regAddress'] = 'Address';
$lang['regCountry'] = 'Country';
$lang['regNewLatter'] = 'NewsLetter';
$lang['regTermCondition'] = 'Terms and conditions';
$lang['regBTN'] = 'Register';
$lang['regFacebook'] = 'Login with Facebook';
$lang['regForgotPass'] = 'Forgot Password';
$lang['regSignIn'] = 'Sign In';
$lang['regSignUp'] = 'Sign Up';
$lang['ForgotPassword'] = 'Forgot Password';
$lang['forgotPassEmail'] = 'E-mail';
$lang['forgotPassSubmitBTN'] = 'Submit';
$lang['validRegFirstname'] = 'Enter First name';
$lang['validRegfirstValid'] = 'Enter valid name';
$lang['validRegLastName'] = 'Enter last name';
$lang['validRegLastvalid'] = 'Enter valid name';
$lang['validRegEmail'] = 'Enter email';
$lang['validRegEmailvalid'] = 'Enter valid email';
$lang['validRegEmailexist'] = 'Email already exist.';
$lang['validRegMobile'] = 'Enter Mobile No';
$lang['validRegMobilemax'] = 'Enter maximum 13 digit number';
$lang['validRegMobilevalid'] = 'Enter valid mobile number';
$lang['validRegMobileMin'] = 'Enter minimum 10 digit number';
$lang['validRegPassword'] = 'Enter Password';
$lang['validRegPasswordMin'] = 'password should consist of min. 6 characters';
$lang['validRegCPassword'] = 'Enter Confirm Password';
$lang['validRegPasswordEqual'] = 'Password and Confirm password not match';
$lang['validRegAddress'] = 'Enter Address';
$lang['validRegCountry'] = 'Select Country';
$lang['validRegTermCondition'] = 'Accept terms and conditions';
$lang['success'] = 'Success !';
$lang['validRegPasswordwp'] = 'Enter Strong password';
$lang['validRegPasswordMinLength'] = 'password should consist of min. 6 characters';


/*  End Footer*/

/*  Start Home */

$lang['homeSearchDoctors'] = 'Doctors';
$lang['homeSearchHostpitals'] = 'Hospitals';
$lang['homeSearchClinic'] = 'Clinic';
$lang['homeSearchLab'] = 'Lab';
$lang['homeSearchlocation'] = 'Location';
$lang['homeSearchSpeciality'] = 'Speciality';
$lang['homeSearchGender'] = 'Gender';
$lang['homeSearchGenderMale'] = 'Male';
$lang['homeSearchGenderFemale'] = 'Female';
$lang['homeSearchName'] = 'Name';
$lang['homeSearchBTN'] = 'Search';
$lang['homeAddList'] = 'ADD TO OUR LIST';
$lang['homeAddDoctor'] = 'ADD A DOCTOR';
$lang['homeAddHospital'] = 'ADD A HOSPITAL';
$lang['homeAddClinic'] = 'ADD A CLINIC';
$lang['homeAddLab'] = 'ADD A LAB';
$lang['homeRUDoctor'] = 'ARE YOU A DOCTOR?';
$lang['homeRUDoctor2'] = 'REGISTER NOW AND REACH THOUSANDS OF PATIENTS';
$lang['homeRUDoctor3'] = 'Topdoctors.me is a regulated healthcare platform in Egypt that has 3 main objectives: medical directory, doctor rating, and social media management for doctors. Nowadays everyone is encouraged to speak their mind, whether its posting picture on instagram or sharing your state of mind on facebook and twitter somehow your thoughts spread. We are aiming to create a channel of communication for doctors and patient to connect and more importantly for patients to share their experiences with other patients.';
$lang['homeSignUP'] = 'Sign Up';
$lang['homeContent1'] = 'TOPDOCTORS ON THE GO';
$lang['homeContent2'] = 'GET THE APPLICATION';
$lang['homeTopReviews'] = 'Top Reviews';

/*  End Home*/

/*  Start Search */

$lang['searchPlaceHolder'] = 'Speciality';
$lang['searchbtn'] = 'Search';
$lang['filterTitle'] = 'Filters';
$lang['filterLocation'] = 'Location';
$lang['filterSpeciality'] = 'Speciality';
$lang['filterGender'] = 'Gender';
$lang['filterMale'] = 'Male';
$lang['filterFemale'] = 'Female';
$lang['filterNamePlace'] = 'Name';
$lang['filterSearchBtn'] = 'Search';
$lang['searchSortBy'] = 'Sort By';
$lang['searchNearByLocation'] = 'Near By Location';
$lang['searchRatingReview'] = 'Rating & Reviews';
$lang['searchBookMark'] = 'Book Mark';
$lang['searchFeaturedDoctor'] = 'Featured Doctor';
$lang['searchNoRecords'] = 'No Records';
$lang['searchGetDirection'] = 'GET DIRECTION';

/*  End Search */

/*  Start Doctor Profile */

$lang['DPSearchBTN'] = 'Search';
$lang['DPOnMap'] = 'ON MAP';
$lang['DPReview'] = 'Reviews';
$lang['DPReputation'] = 'Reputation';
$lang['DPClinic'] = 'Clinic Accessibility';
$lang['DPAvailability'] = 'Availability in Emergencies';
$lang['DPApproachability'] = 'Approachability';
$lang['DPTechnology'] = 'Technology & Equipment';
$lang['DPWriteReview'] = 'WRITE YOUR REVIEW';
$lang['DPFRMName'] = 'Name';
$lang['DPFRMSummary'] = 'Summary';
$lang['DPSubmitReview'] = 'Submit';

/*  End Doctor Profile */

/* Start Add Doctor Pop up */

$lang['AddDoctorPTitle'] = 'Add doctor';
$lang['AddDoctorPFirstname'] = 'First Name';
$lang['AddDoctorPLastname'] = 'Last Name';
$lang['AddDoctorPAddress'] = 'Address';
$lang['AddDoctorPEmail'] = 'E-mail';
$lang['AddDoctorPPhone'] = 'Mobile Number';
$lang['AddDoctorPLocation'] = 'Location';
$lang['AddDoctorPSpeciality'] = 'Speciality';
$lang['AddDoctorPWork'] = 'Work';
$lang['AddDoctorPGender'] = 'Select Gender';
$lang['AddHospitalPTitle'] = 'Add Hospital';
$lang['AddClinicPTitle'] = 'Add Clinic';
$lang['AddLabPTitle'] = 'Add Lab';
$lang['AddWorkPTitleName'] = 'Name';
$lang['AddWorkPTitleEmail'] = 'Email';
$lang['AddWorkPTitleLocation'] = 'Location';
$lang['AddWorkPMobile'] = 'Mobile';
$lang['AddWorkPAddress'] = 'Address';
$lang['AddPopGeneralSubmit'] = 'Submit';
$lang['AddPopIsprivate'] = 'Is private';
$lang['AddWorkPTitleContact'] = 'Contact';
$lang['AddDoctorMale'] = 'Male';
$lang['AddDoctorFemale'] = 'Female';
$lang['AddDoctorSpecialityLabel'] = 'Speciality';
$lang['SearchSeeMore'] = 'See More...';
$lang['SearchBack'] = 'Back';
$lang['changePassword'] = 'Change Password';
$lang['EditProfile'] = 'Edit Profile';
$lang['BookMarkList'] = 'Bookmark List';
$lang['Logout'] = 'Logout';
$lang['Hardtoreach'] = "Hard to reach";
$lang['Amazing'] = "Amazing";
$lang['MakeHomeVisits'] = "Make Home Visits";
$lang['GreatHelp'] = "Great Help";
$lang['AngryDoctor'] = "Angry Doctor";
$lang['Reachable'] = "Reachable";
$lang['OldDoctor'] = "Old Doctor";
$lang['Like'] = "Like";
$lang['Clean'] = "Clean";
$lang['Dirty'] = "Dirty";
$lang['LongWaiting'] = "Long Waiting";
$lang['Hospitalerrormsg'] = "To add Hospital, please make sure you are already logged in";
$lang['Clinicerrormsg'] = "To add Clinic, please make sure you are already logged in";
$lang['Laberrormsg'] = "To add Lab, please make sure you are already logged in";
$lang['Doctorerrormsg'] = "To add Doctors, please make sure you are already logged in";
$lang['dayago'] = " Day ago";
$lang['daysago'] = " Days ago";
$lang['hourago'] = " Hour ago";
$lang['hoursago'] = " Hours ago";
$lang['minuteago'] = " Minute ago";
$lang['minutesago'] = " Minutes ago";
$lang['secondago'] = " A few seconds ago";
$lang['Hospital'] = "Hospital";
$lang['Lab'] = "Lab";
$lang['Clinic'] = "Clinic";
$lang['Major'] = "Major";
$lang['Biography'] = "Biography";
$lang['Biography'] = "Biography";
$lang['chooseicon'] = "Choose an icon for your doctor";
$lang['choosewicon'] = "Choose an icon";

$lang['radiologylab'] = "Radiology Lab";
$lang['medicallab'] = "Medical Lab";
$lang['LabType'] = "Lab Type";
$lang['reviewlogin'] = "Please login to write a review";
$lang['getintouch'] = "Get in Touch";
$lang['editProfile'] = "Edit Profile";
$lang['changepassword'] = "Change Password";

$lang['confirmpassword'] = "Confirm Password";
$lang['newpassword'] = "New Password";
$lang['currentpassword'] = "Current Password";
$lang['Anonymous'] = "Post anonymous";
$lang['AnonymousName'] = "Anonymous";
$lang['contactdetails'] = "Contact Details";
$lang['topdoctorsLine'] = " Top Doctors";
$lang['cairoLang'] = "Cairo";
$lang['FullName'] = "Full Name";
$lang['emailaddress'] = "Email Address";
$lang['YourMsg'] = "Your Message";
$lang['message'] = "Send";
$lang['egyptline'] = "Egypt";
$lang['getintouch'] = "Get in Touch";
$lang['tophome'] = "5xxx Top Home . 105";
$lang['EnquiryTitle'] = "Enter Your Enquiry";
$lang['EnquiryName'] = "Name";
$lang['NameHolder'] = "Enter your name";
$lang['EmailHolder'] = "Enter your email";
$lang['Email'] = "Email";
$lang['PhoneNumber'] = "Phone Number";
$lang['Enquiry'] = "Enquiry";
$lang['SEND'] = "Send";
$lang['enquire'] = "Enquire";
$lang['valPhone'] = "Enter phone";
$lang['validNumbers'] = "Please enter only number";
$lang['validphoneNumbers'] = "Please enter only 15 number";
$lang['ValidWork'] = "Select work";
$lang['ValidLocation'] ="Select Location";
$lang['ValidSpeciality'] = "Select Specialties";
$lang['ValidGender'] = "Select Gender";
$lang['ValidName'] = "Enter name";
$lang['ValidAlphabet'] = "Please enter only aphabetics";
$lang['HospitalRemote'] = "This hospital already exists";
$lang['ClinicRemote'] = "This clinic already exists";
$lang['LabRemote'] = "This lab already exists";
$lang['ValidLabType'] ="Select Lab Type";
$lang['ValidContactNo'] = "Please enter a valid contact no.";
$lang['ValidCharacter'] = "Please enter a valid character.";
$lang['footerSearchText'] = "How To Search";
$lang['filterHomeSearchTitle'] = "DOCTORS FILTERS";
$lang['filterSearchDoctorTitle'] = 'DOCTORS FILTERS';
$lang['filterSearchHospitalTitle'] = 'Hospitals Filters';
$lang['filterSearchClinicTitle'] ='Clinic Filters';
$lang['filterSearchLabTitle'] = 'Lab Filters';


/*11-05-2017*/
$lang['AddDoctorPMobile'] = 'Mobile Number';
$lang['AddDoctorPPhones'] = 'Phone Number';
$lang['AddDoctorBiography'] = 'Biography';
$lang['AddDoctorWorkHours'] = 'Work Hours';
$lang['AddDoctorSpeciality'] = 'Speciality';