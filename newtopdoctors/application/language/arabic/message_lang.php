<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*  Start Header*/
$lang['title'] = 'أفضل الأطباء';
$lang['errorLogin'] = 'البريد الإلكتروني أو كلمة السر خاطئة';
$lang['registerSuccess'] = 'التسجيل بنجاح';
$lang['forgotpasswordsuccess'] = 'يرجى التحقق من البريد الإلكتروني إلى كلمة مرور جديدة';
$lang['forgotEmailNotFound'] = 'لم يتم انشاء حساب البريد الإلكتروني في النظام';
$lang['menuHome'] = 'الصفحة الرئيسية';
$lang['menuDoctors'] = 'الأطباء';
$lang['menuContactUs'] = 'اتصل بنا';
$lang['menuSignIn'] = 'تسجيل الدخول';
$lang['menuOR'] = 'أو';
$lang['menuRegister'] = 'انشاء حساب';
$lang['menuMenu'] = 'القائمة';

/*  End Header*/

/*  Start Footer*/

$lang['footerContent'] = 'أبجد هوز دولور الجلوس امات، إيليت دولور. ماسا';
$lang['footerAddress'] = '123 اكليس الطريق القديم، طريق سالفورد الجديد، الشرقي';
$lang['footerEmail'] = ' info@domain.com';
$lang['footerPhone'] = '+44 123 456 788-9';
$lang['footerContentB'] = 'أبجد هوز دولور الجلوس امات،إيليت. دولور. ماسا ';
$lang['footerAddressB'] = '123 اكليس الطريق القديم، طريق سالفورد الجديد، الشرقي';
$lang['footerEmailB'] = ' info@domain.com';
$lang['footerPhoneB'] = '+44 123 456 788-9';
$lang['footerUseFullLink'] = 'روابط مفيدة';
$lang['footerMenuAboutUs'] = 'معلومات عنا';
$lang['footerMenuLatestBlog'] = 'أحدث بلوق';
$lang['footerMenuTermCondition'] = 'الأحكام والشروط';
$lang['footerMenuPrivacyPolicy'] = ' سياسة الخصوصية';
$lang['footerMenuContactUS'] = 'اتصل بنا';
$lang['footerLine'] = date('Y').' جميع الحقوق محفوظة';
$lang['footerCopyRight'] = 'وثيقة المباشر';
$lang['Login'] = 'تسجيل الدخول';
$lang['Register'] = 'تسجيل';
$lang['loginEmail'] = 'البريد الإلكتروني';
$lang['loginPassword'] = 'كلمه السر';
$lang['loginForgotPass'] = 'هل نسيت كلمة المرور؟ ';
$lang['loginSubmitBTN'] = 'تسجيل الدخول';
$lang['loginFacebook'] = 'تسجيل الدخول باستخدام الفيسبوك ';
$lang['loginGuest'] = 'أدخل كضيف';
$lang['regFirstName'] = 'الاسم الاول';
$lang['regLastName'] = 'الاسم الثانى'; // 1-6-2017
$lang['regEmail'] = 'البريد الإلكتروني';
$lang['regMobileNumber'] = 'رقم الهاتف المحمول';
$lang['regPassword'] = 'كلمه السر';
$lang['regCPassword'] =  'تأكيد كلمة السر';//'تأكيد كلمة المرور هذا'; 1-6-2017
$lang['regAddress'] = 'عنوان';
$lang['regCountry'] = 'بلد';
$lang['regNewLatter'] = 'المجلة الأسبوعية'; // 'النشرة الإخبارية'; 1-6-2017
$lang['regTermCondition'] = 'الموافقة على بنود و شروط الموقع'; // 'الأحكام والشروط'; 1-6-2017
$lang['regBTN'] = 'تسجيل';
$lang['regFacebook'] = 'تسجيل الدخول باستخدام الفيسبوك';
$lang['regForgotPass'] = 'هل نسيت كلمة المرور';
$lang['regSignIn'] = 'تسجيل الدخول';
$lang['regSignUp'] = 'هل نسيت كلمة المرور';
$lang['ForgotPassword'] = 'هل نسيت كلمة المرور';
$lang['forgotPassEmail'] = 'البريد الإلكتروني';
$lang['forgotPassSubmitBTN'] = 'تسجيل';
$lang['validRegFirstname'] = 'أدخل الإسم الأول';
$lang['validRegfirstValid'] = 'إدخال اسم صالح';
$lang['validRegLastName'] = 'إدخال اسم آخر';
$lang['validRegLastvalid'] = 'إدخال اسم صالح';
$lang['validRegEmail'] = 'إدخال اسم صالح';
$lang['validRegEmailvalid'] = 'أدخل بريد إلكتروني صالح';
$lang['validRegEmailexist'] = 'البريد الالكتروني موجود بالفعل';
$lang['validRegMobile'] = 'إدخال أي المحمول';
$lang['validRegMobilemax'] = 'أدخل أقصى 13 أرقام';
$lang['validRegMobilevalid'] = 'ادخل رقم هاتف خلوي ساري المفعول';
$lang['validRegMobileMin'] = 'إدخال الحد الأدنى لعدد 10 أرقام';
$lang['validRegPassword'] = 'أدخل كلمة المرور';
$lang['validRegPasswordMin'] = 'يجب أن تتكون كلمة المرور من دقيقة. 6 أحرف';
$lang['validRegCPassword'] = 'يدخل تأكيد كلمة المرور';
$lang['validRegPasswordEqual'] = 'كلمة المرور و تأكيد كلمة المرور لا تتطابق';
$lang['validRegAddress'] = 'أدخل العنوان';
$lang['validRegCountry'] = 'أدخل العنوان';
$lang['validRegTermCondition'] = 'أوافق على الشروط و الأحكام';
$lang['success'] = 'نجاح';
$lang['validRegPasswordMinLength'] = 'يجب أن تتكون كلمة المرور من دقيقة. 6 أحرف';


/*  End Footer*/

/*  Start Home */

$lang['homeSearchDoctors'] = 'الأطباء';
$lang['homeSearchHostpitals'] = 'المستشفيات';
$lang['homeSearchClinic'] = 'عيادات';
$lang['homeSearchLab'] = 'المعامل';
$lang['homeSearchlocation'] = 'المنطقة';
$lang['homeSearchSpeciality'] = 'التخصص';
$lang['homeSearchGender'] = 'دكتور أو دكتورة';
$lang['homeSearchGenderMale'] = 'ذكر';
$lang['homeSearchGenderFemale'] = 'أنثى';
$lang['homeSearchName'] = 'الاسم';
$lang['homeSearchBTN'] = 'بحث';
$lang['homeAddList'] = 'إضافة إلى قائمتنا';
$lang['homeAddDoctor'] = 'إضافة طبيب';
$lang['homeAddHospital'] = 'إضافة مستشفى';
$lang['homeAddClinic'] = 'إضافة عيادة';
$lang['homeAddLab'] = 'إضافة مختبر';
$lang['homeRUDoctor'] = 'هل انت دكتور؟';
$lang['homeRUDoctor2'] = 'سجل الآن والوصول إلى آلاف المرضى';
$lang['homeRUDoctor3'] = 'موقع أفضل الأطباء له ثلاثة محاور رئيسية :
الدليل الطبى ، وتقييم الأطباء وادارة وسائل التواصل الاجتماعي للأطباء.
موقعنا يهدف إلى خلق قناة للتواصل بين المريض والطبيب وإلى السماح
للمرضى بمشاركة خبراتهم مع الآخرين.';

$lang['homeSignUP'] = 'سجل الآن';
$lang['homeContent1'] = 'أفضل الاطباء معك في كل مكان';
$lang['homeContent2'] = 'أحصل على التطبيق';
$lang['homeTopReviews'] = 'أفضل التقييمات';


/*  End Home*/

/*  Start Search */

$lang['searchPlaceHolder'] = 'تخصص';
$lang['searchbtn'] = 'بحث';
$lang['filterTitle'] = 'مرشحات';
$lang['filterLocation'] = 'المنطقة';
$lang['filterSpeciality'] = 'تخصص';
$lang['filterGender'] = 'النوع';
$lang['filterMale'] = 'ذكر';
$lang['filterFemale'] = 'أنثى';
$lang['filterNamePlace'] = 'اسم';
$lang['filterSearchBtn'] = 'بحث';
$lang['searchSortBy'] = 'ترتيب حسب';
$lang['searchNearByLocation'] = 'مكان قريب';
$lang['searchRatingReview'] = 'تقييم ونقد';
$lang['searchBookMark'] = 'المرجعية';
$lang['searchFeaturedDoctor'] = 'طبيب متميز';
$lang['searchNoRecords'] = 'لا تسجيلات';
$lang['searchGetDirection'] = 'احصل على اتجاه';

/*  End Search */

/*  Start Doctor Profile */

$lang['DPSearchBTN'] = 'بحث';
$lang['DPOnMap'] = 'على الخريطة';
$lang['DPReview'] = 'اضف تقييمك'; //'تقييمات'; 1-6-2017
$lang['DPReputation'] = 'سمعة الطبيب';//'سمعة'; 1-6-2017
$lang['DPClinic'] = 'سهولة الوصول للعيادة'; //'عيادة الوصول'; 1-6-2017
$lang['DPAvailability'] = 'التواجد فى حالات الطوارئ';//'توفر في حالات الطوارئ'; 1-6-2017
$lang['DPApproachability'] = 'سهولة التواصل مع الطبيب'; //'الوصول'; 1-6-2017
$lang['DPTechnology'] = 'الاجهزة المستخدمة'; //'التكنولوجيا والمعدات'; 1-6-2017
$lang['DPWriteReview'] = 'إرسال رأيك';
$lang['DPFRMName'] = 'اسم';
$lang['DPFRMSummary'] = 'ملخص';
$lang['DPSubmitReview'] = 'تسجيل';



/*  End Doctor Profile */

/* Start Add Doctor Pop up */

$lang['AddDoctorPTitle'] = 'إضافة الطبيب';
$lang['AddDoctorPFirstname'] = 'الاسم الاول';
$lang['AddDoctorPLastname'] = 'الاسم الثانى';
$lang['AddDoctorPAddress'] = 'العنوان';
$lang['AddDoctorPEmail'] = 'البريد الالكترونى';
$lang['AddDoctorPPhone'] = 'رقم الهاتف المحمول';
$lang['AddDoctorPLocation'] = 'المنطقة';
$lang['AddDoctorPSpeciality'] = 'تخصص';
$lang['AddDoctorPWork'] = 'عمل';
$lang['AddDoctorPGender'] = 'النوع';
$lang['AddHospitalPTitle'] = 'إضافة المستشفى';
$lang['AddClinicPTitle'] = 'إضافة العيادة';
$lang['AddLabPTitle'] = 'إضافة المعامل';
$lang['AddWorkPTitleName'] = 'اسم';
$lang['AddWorkPTitleEmail'] = 'البريد الإلكتروني';
$lang['AddWorkPTitleLocation'] = 'موقع';
$lang['AddWorkPMobile'] = 'التليفون المحمول';
$lang['AddWorkPAddress'] = 'عنوان';
$lang['AddPopGeneralSubmit'] = 'تسجيل';
$lang['AddPopIsprivate'] = 'عيادة خاصة';
$lang['AddWorkPTitleContact'] = 'اتصال';
$lang['AddDoctorMale'] = 'ذكر';
$lang['AddDoctorFemale'] = 'أنثى';
$lang['AddDoctorSpecialityLabel'] = 'تخصص';
$lang['SearchSeeMore'] = '...شاهد المزيد';
$lang['SearchBack'] = 'الى الخلف';
$lang['changePassword'] = 'تغيير كلمة السر';
$lang['EditProfile'] = 'تعديل الملف الشخصي';
$lang['BookMarkList'] = 'قائمتى لافضل الاطباء'; //'المرجعية القائمة'; 1-6-2017
$lang['Logout'] = 'خروج';
$lang['Chooseanicon'] = "Choose an icon";
$lang['Hardtoreach'] = "لا يمكن الوصول اليه";
$lang['Amazing'] = "رائع";
$lang['MakeHomeVisits'] = "ممكن زيارة المنزل";
$lang['GreatHelp'] = "مساعدة رائعة";
$lang['AngryDoctor'] = "دكتور غضبان";
$lang['Reachable'] = "يمكن الوصول اليه";
$lang['OldDoctor'] = "دكتور كبير في السن";
$lang['Like'] = "اعجبني";
$lang['Clean'] = "نظيفة";
$lang['Dirty'] = "غير نظيفة";
$lang['LongWaiting'] = "انتظار طويل";
$lang['Hospitalerrormsg'] = "لإضافة المستشفى، يرجى التأكد من أنك قمت بتسجيل الدخول بالفعل في";
$lang['Clinicerrormsg'] = "لإضافة عيادة، يرجى التأكد من أنك قمت بتسجيل الدخول بالفعل في";
$lang['Laberrormsg'] = "لإضافة مختبر، يرجى التأكد من أنك قمت بتسجيل الدخول بالفعل في";
$lang['Doctorerrormsg'] = "لإضافة الأطباء، من فضلك تأكد من أنك قمت بتسجيل الدخول بالفعل في";
$lang['dayago'] = " منذ يوم";
$lang['daysago'] = " أيام مضت";
$lang['hourago'] = " منذ ساعة";
$lang['hoursago'] = " منذ ساعات";
$lang['minuteago'] = " منذ دقيقة";
$lang['minutesago'] = " دقائق مضت";
$lang['secondago'] = " قبل بضع ثوان";
$lang['Hospital'] = "مستشفى";
$lang['Lab'] = "مختبر";
$lang['Clinic'] = "عيادة";
$lang['Major'] = "التخصص";
$lang['Biography'] = "السيرة الذاتية";
$lang['chooseicon'] = "إختيار أيقونة طبيبك";
$lang['choosewicon'] = "اختر رمزا";
$lang['radiologylab'] = "معمل أشعة"; //معمل أشعة    //معمل أشعة
$lang['medicallab'] = "معمل تحاليل";  //    //مختبر طبي
$lang['LabType'] = "نوع المعمل"; //      //نوع المعامل
$lang['reviewlogin'] = "يرجى تسجيل الدخول لتكتب تقييم";
$lang['getintouch'] = "يحصل علفيلمسى";
$lang['editProfile'] = "تعديل الملف الشخصي";
$lang['changepassword'] = "تغيير كلمة السر";
$lang['confirmpassword'] = "تأكيد كلمة المرور";
$lang['newpassword'] = "كلمة السر الجديدة";
$lang['currentpassword'] = "كلمة السر الحالية";
$lang['Anonymous'] = "آخر مجهول";
$lang['AnonymousName'] = "مجهول";
$lang['validRegPasswordwp'] = 'إدخال كلمة مرور قوية';
$lang['contactdetails'] = "للاتصال بنا";
$lang['topdoctorsLine'] = "أفضل الأطباء";
$lang['cairoLang'] = "القاهرة";

$lang['FullName'] = "الاسم الثلاثي";
$lang['emailaddress'] = "البريد الالكتروني";
$lang['YourMsg'] = "الرسالة";
$lang['message'] = "ارسل";
$lang['egyptline'] = "مصر";
$lang['contactaddress'] = "5xxx أعلىمنزل. 105";
$lang['getintouch'] = "يحصل على في لمس";
$lang['tophome'] = " أعلى الصفحة الرئيسية. 5xxx 105";
$lang['EnquiryTitle'] = "أدخل استفسارك";
$lang['EnquiryName'] = "اسم";
$lang['NameHolder'] = "أدخل أسمك";
$lang['EmailHolder'] = "ادخل بريدك الإلكتروني";
$lang['Email'] = "البريد الإلكتروني";
$lang['PhoneNumber'] = "رقم الهاتف";
$lang['Enquiry'] = "تحقيق";
$lang['SEND'] = "إرسال";
$lang['enquire'] = "استعلم";
$lang['valPhone'] = "إدخال الهاتف";
$lang['validNumbers'] = "يرجى إدخال الرقم الوحيد";
$lang['validphoneNumbers'] = "يرجى إدخال رقم 15 فقط";
$lang['ValidWork'] = "اختار عمل";
$lang['ValidLocation'] = "اختار موقع";
$lang['ValidSpeciality'] = "التخصصات";
$lang['ValidGender'] = "اختار النوع";
$lang['ValidName'] = "أدخل الاسم";
$lang['ValidAlphabet'] = "الرجاء إدخال الحروف الهجائية فقط";
$lang['HospitalRemote'] = "هذا المستشفى موجود بالفعل";
$lang['ClinicRemote'] = "هذه العيادة موجود بالفعل";
$lang['LabRemote'] = "هذا المعامل موجود بالفعل";
$lang['ValidLabType'] ="حدد نوع المعامل";
$lang['ValidContactNo'] = "يرجى الاتصال إدخال صالحة بغض النظر";
$lang['ValidCharacter'] = "الرجاء إدخال حرف صالح";
$lang['footerSearchText'] = "طريقة البحث";
$lang['filterHomeSearchTitle'] = "أفضل الأطباء";
$lang['filterSearchDoctorTitle'] = 'الأطباء المقترحون';
$lang['filterSearchHospitalTitle'] = 'المستشفيات المقترحة';
$lang['filterSearchClinicTitle'] ='العيادات المقترحة';
$lang['filterSearchLabTitle'] = 'المعامل المقترحة';


/* 11-05-2017 */
$lang['AddDoctorPMobile'] = 'رقم الهاتف المحمول';
$lang['AddDoctorPPhones'] = 'التليفون الارضي';
$lang['AddDoctorBiography'] = 'السيرة الذاتية';
$lang['AddDoctorWorkHours'] = 'مواعيد العمل';
$lang['AddDoctorSpeciality'] = 'التخصص';



