<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Work class
 */
class Work extends REST_Controller
{

    private $imagepath = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Work_master_model');
        $this->load->model('Specialty_master_model');
        $this->load->model('Country_master_model');
        $this->load->model('Location_master_model');

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=utf-8");
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        //trim all data
        foreach ($_POST as $key => $val) {
            $_POST[$key] = trim($val);
        }

        foreach ($_GET as $key => $val) {
            $_GET[$key] = trim($val);
        }
    }

    public function hospital_get()
    {
        if ($this->get('lang') != '') {
            $this->data['lang'] = $this->get('lang');
        }
        $hospital = $this->Work_master_model->getAllHospitals();
        if (!empty($hospital)) {
            $this->response(array(
                'status' => true,
                'data' => $hospital,
                'message' => count($hospital) . ' data found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data available!',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function hospital_post()
    {
        $hospital = $this->Work_master_model->getAllHospitals();
        if (!empty($hospital)) {
            $this->response(array(
                'status' => true,
                'data' => $hospital,
                'message' => count($hospital) . ' data found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data available!',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function clinic_get()
    {
        $clinic = $this->Work_master_model->getAllClinics();
        if (!empty($clinic)) {
            $this->response(array(
                'status' => true,
                'data' => $clinic,
                'message' => count($clinic) . ' data found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data available!',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function clinic_post()
    {
        $clinic = $this->Work_master_model->getAllClinics();
        if (!empty($clinic)) {
            $this->response(array(
                'status' => true,
                'data' => $clinic,
                'message' => count($clinic) . ' data found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data available!',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function searchLab_get()
    {
        $gender = $this->get('gender');
        $speciality = $this->get('speciality');

        $data = $this->Work_master_model->searchLab($gender, $speciality);

        if (!empty($data)) {
            $this->response(array(
                'status' => true,
                'data' => $data,
                'message' => count($data) . ' data are found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => '',
                'message' => 'There are no results that match your search.',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function searchLab_post()
    {
        $gender = $this->post('gender');
        $speciality = $this->post('speciality');

        $data = $this->Work_master_model->searchLab($gender, $speciality);

        if (!empty($data)) {
            $this->response(array(
                'status' => true,
                'data' => $data,
                'message' => count($data) . ' data are found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => '',
                'message' => 'There are no results that match your search.',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function allWorkMaster_post()
    {
        $lang = $this->post('lang');
        $userId = $this->post('userId');
        $page = $this->post('page');
        $perPage = "10";

        $workMaster = $this->Work_master_model->getAllWorkMaterResult($lang, $userId, $page, $perPage);
        $total_rows = $this->Work_master_model->getAllWorkMaterResultNumRows($lang, $userId);

        if (!empty($workMaster)) {
            $this->response(array(
                'status' => true,
                'data' => $workMaster,
                'message' => count($workMaster) . ' data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => $workMaster,
                'message' => 'No data available!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function allWorkMaster_get()
    {
        $lang = $this->get('lang');
        $userId = $this->get('userId');
        $page = $this->get('page');
        $perPage = "10";
        $workMaster = $this->Work_master_model->getAllWorkMaterResult($lang, $userId, $page, $perPage);
        $total_rows = $this->Work_master_model->getAllWorkMaterResultNumRows($lang, $userId);
        if (!empty($workMaster)) {
            $this->response(array(
                'status' => true,
                'data' => $workMaster,
                'message' => count($workMaster) . ' data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => $workMaster,
                'message' => 'No data available!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function nearby_get()
    {
        $nearby = $this->Work_master_model->getAllWorkMater();
        if (!empty($nearby)) {
            $this->response(array(
                'status' => true,
                'data' => $nearby,
                'message' => count($nearby) . ' data found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => $nearby,
                'message' => 'No data available!',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function nearby_post()
    {

        $nearby = $this->Work_master_model->getAllWorkMater();
        if (!empty($nearby)) {
            $this->response(array(
                'status' => true,
                'data' => $nearby,
                'message' => count($nearby) . ' data found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => $nearby,
                'message' => 'No data available!',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function labs_filter_get()
    {
        //get the gender and speciality
        $gender = $this->get('gender');
        $speciality = $this->get('speciality');

        if (!empty($gender) || !empty($speciality)) {
            $this->response(array(
                'status' => true,
                'gender' => $gender,
                'speciality' => $speciality,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function labs_filter_post()
    {}

    public function searchDoctors_post()
    {
        $this->load->model("Doctor_master_en_model");
        $gender = $this->post("gender");
        $speciality = $this->post("speciality");
        $location = $this->post("location");
        $data = $this->Doctor_master_en_model->searchDoctors($location, $speciality, $gender);

        if (!empty($data)) {
            $this->response(array(
                'status' => true,
                'data' => $data,
                'message' => count($data) . ' are found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => '',
                'message' => 'There are no results that match your search.',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function searchDoctors_get()
    {
        $this->load->model("Doctor_master_en_model");
        $gender = $this->get("gender");
        $speciality = $this->get("speciality");
        $location = $this->get("location");
        $data = $this->Doctor_master_en_model->searchDoctors($location, $speciality, $gender);

        if (!empty($data)) {
            $this->response(array(
                'status' => true,
                'data' => $data,
                'message' => count($data) . ' are found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => '',
                'message' => 'There are no results that match your search.',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function workAdd_get()
    {
        $lang = $this->get('lang');
        $location = $this->Location_master_model->locationDetails($this->get('location'), $lang);
        $doctorId = explode(',', $this->get('doctor'));
        $checkEmail = $this->Work_master_model->checkEmail($this->get('email'));

        if (count($checkEmail) > 0) {
            $this->response(array(
                'status' => false,
                'message' => 'Email aleardy exist',
            ), REST_Controller::HTTP_OK);
        } else {
            if ($this->post('lang') == 'ar') {
                $work_id = $this->Work_master_model->insert(array(
                    'name' => $this->get('name'),
                    'address' => $this->get('address'),
                    'phone' => $this->get('phone'),
                    'email' => $this->get('email'),
                    'location_id' => $location->location_id,
                    'doctor_id' => $this->get('doctor'),
                    'work_type' => $this->get('workType'),
                    'is_private' => $this->get('isPrivate'),
                    'photo' => $this->imagepath,
                    'status' => '0',
                ));

                foreach ($doctorId as $doctorValue) {
                    $this->db->where('doctor_id', $doctorValue);
                    $this->db->update('doctor_details', array('work_id' => $work_id, 'speciality_id' => $speciality->specialties_id));
                }

            } else {
                //insert work master
                $work_id = $this->Work_master_model->insert(array(
                    'name_en' => $this->get('name'),
                    'address_en' => $this->get('address'),
                    'phone' => $this->get('phone'),
                    'email' => $this->get('email'),
                    'location_id' => $location->location_id,
                    'speciality' => 1,
                    'work_type' => $this->get('workType'),
                    'is_private' => $this->get('isPrivate'),
                    'photo' => $this->imagepath,
                    'status' => '0',
                ));

                foreach ($doctorId as $doctorValue) {
                    $this->db->where('doctor_id', $doctorValue);
                    $this->db->update('doctor_details', array('work_id' => $work_id, 'speciality_id' => $speciality->specialties_id));
                }
            }
            $work_id = $this->db->insert_id();
            $photoPath = $this->base64_to_png($_GET['image'], $work_id, $work_id);
            $this->response(array(
                'status' => true,
                'message' => 'Data added successfully',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function workAdd_post()
    {
        $lang = $this->post('lang');
        $location = $this->Location_master_model->locationDetails($this->post('location'), $lang);
        //$checkEmail = 0;
        if ($this->post('email') != '') {
            $checkEmail = $this->Work_master_model->checkEmail($this->post('email'));
        }

        $doctorId = explode(',', $this->post('doctor'));

        if ((isset($checkEmail)) && (count($checkEmail) > 0)) {
            $this->response(array(
                'status' => false,
                'message' => 'Email aleardy exist',
            ), REST_Controller::HTTP_OK);
        } else {
            if ($this->post('lang') == 'ar') {
                $this->Work_master_model->insert(array(
                    'name' => $this->post('name') . ' ' . $this->post('lastName'),
                    'address' => $this->post('address'),
                    'mobileno' => $this->post('phone'),
                    'email' => $this->post('email'),
                    'location_id' => $location->location_id,
                    'work_type' => $this->post('workType'),
                    'is_private' => $this->post('isPrivate'),
                    'photo' => $this->imagepath,
                    'status' => '0',
                    'work_hours' => $this->post('workingHour'),
                    'phone' => $this->post('phoneNumber'),
                    'work_biography' => $this->post('biography'),
                    'firstname_ar' => $this->post('name'),
                    'lastname_ar' => $this->post('lastName'),
                ));
                foreach ($doctorId as $doctorValue) {
                    $this->db->where('doctor_id', $doctorValue);
                    $this->db->update('doctor_details', array('work_id' => $work_id, 'speciality_id' => $speciality->specialties_id));
                }
            } else {
                //insert work master
                $this->Work_master_model->insert(array(
                    'name_en' => $this->post('name') . ' ' . $this->post('lastName'),
                    'address_en' => $this->post('address'),
                    'mobileno' => $this->post('phone'),
                    'email' => $this->post('email'),
                    'location_id' => $location->location_id,
                    'speciality' => $speciality->specialties_id,
                    'work_type' => $this->post('workType'),
                    'is_private' => $this->post('isPrivate'),
                    'photo' => $this->imagepath,
                    'status' => '0',
                    'work_hours_en' => $this->post('workingHour'),
                    'phone' => $this->post('phoneNumber'),
                    'work_biography_en' => $this->post('biography'),
                    'firstname_en' => $this->post('name'),
                    'lastname_en' => $this->post('lastName'),
                ));
                foreach ($doctorId as $doctorValue) {
                    $this->db->where('doctor_id', $doctorValue);
                    $this->db->update('doctor_details', array('work_id' => $work_id, 'speciality_id' => $speciality->specialties_id));
                }
            }

            //send an email
            $this->send_email($_POST['name'], $_POST['address'], $_POST['phone'], $_POST['email'],
                $_POST['location'], $_POST['speciality'], $_POST['workType']);

            $work_id = $this->db->insert_id();
            $photoPath = $this->base64_to_png($_POST['image'], $work_id, $work_id);
            $this->response(array(
                'status' => true,
                'message' => 'Your request is successfully sent.',
            ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * Convert base64 to png
     */
    public function base64_to_png($base64, $imageName, $work_id)
    {
        define('UPLOAD_DIR', FCPATH . 'uploads/work/');
        $img = $base64;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = UPLOAD_DIR . $imageName . '.png';
        $success = file_put_contents($file, $data);

        $imageName .= '.png';

        //update photo
        $this->db->where('work_id', $work_id);
        $this->db->update('work_master', array(
            'photo' => $imageName,
        ));

        return $success ? $file : '';
    }

    public function all_work_type_get()
    {
        $lang = $this->get('lang');
        $work_master = $this->Work_master_model->all_work_master($lang);
        if (!empty($work_master)) {
            $this->response(array(
                'status' => true,
                'data' => $work_master,
                'message' => count($work_master) . ' data found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => $work_master,
                'message' => 'No data available!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function send_email($name, $address, $phone, $email, $location, $speciality,
        $work_type) {

        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->email->from(ADMIN_EMAIL); // change it to yours
        $this->email->to(ADMIN_EMAIL); // change it to yours
        $this->email->cc('mehul.raninga@searchnative.com');
        $this->email->subject('Top Doctors - New ' . $work_type . ' Registration');

        $message = "Hello Top Doctors,";
        $message .= "<br/><br/>A new " . $work_type . " information has been reqested for Top Doctors.";
        //$message .= "<br/><br/><a href='www.google.com'>Click here</a> ";
        //$message .= "following link to approve this request for Top Doctors.";
        $message .= "<br/><br/><b>Hospital name: </b>" . $name;
        //$message .= "<br/><b>Clinc name: </b>" . $clinic;
        $message .= "<br/><b>Email: </b>" . $email;
        $message .= "<br/><b>Phone: </b>" . $phone;
        $message .= "<br/><b>Location: </b>" . $location;
        $message .= "<br/><b>Address: </b>" . $address;
        //$message .= "<br/><b>Speciality: </b>" . $speciality;
        if ($work_type == 'Radiology Lab' || $work_type == 'Medical Lab' ||
            $work_type == 'Lab') {
            $message .= "<br/><b>Lab Type: </b>" . $work_type;
        } else {
            $message .= "<br/><b>Work: </b>" . $work_type;
        }

        $this->email->message($message);
        if (!$this->email->send()) {
            $this->response(array(
                'status' => false,
                'message' => 'Error occured while sending an email',
            ), REST_Controller::HTTP_OK);
        }
    }
}
