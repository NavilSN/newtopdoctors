<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Country extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Country_master_model');
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=utf-8");
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function index_get()
    {
        $this->db->cache_on();
        $country = $this->Country_master_model->get_all();
        $this->db->cache_off();
        if (!empty($country)) {
            $this->response(array(
                'status' => true,
                'data' => $country,
                'message' => count($country) . ' data found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Data is not available!',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function index_post()
    {
        $this->db->cache_on();
        $country = $this->Country_master_model->get_all();
        $this->db->cache_off();
        if (!empty($country)) {
            $this->response(array(
                'status' => true,
                'data' => $country,
                'message' => count($country) . ' data found!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Data is not available!',
            ), REST_Controller::HTTP_OK);
        }
    }
}
