<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Work_review extends REST_Controller {

	/**
	* Constructor
	*
	*  @return void
	*/
	function __construct() {
		parent::__construct();
		$this->load->model('Work_rating_model');
		$this->load->model('Doctors_rating_model');
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        //trim all data
		foreach ($_POST as $key => $val) {
			$_POST[$key] = trim($val);
		}

		foreach ($_GET as $key => $val) {
			$_GET[$key] = trim($val);
		}
	}

	function Add_get(){
		
		$workId=$this->get('workId');
		$userId=$this->get('userId');
		$reputation=$this->get('reputation');
		$clinic=$this->get('clinic');
		$availability=$this->get('availability');
		$approachability=$this->get('approachability');
		$technology=$this->get('technology');
		$comment=$this->get('comment');
		$userIp=$this->get('userIp');
		$visible=$this->get('visible');
                $reviewIcon=$this->get('reviewIcon');

		$checkDuplicate=$this->Work_rating_model->checkDuplicate($workId,$userId);
		$msg=$this->db->get_where('work_master',array('work_id'=>$workId))->row();
		if(count($checkDuplicate)>0){
			$this->response(array(
				'status'	=> FALSE,
				'message' => 'You have already rated this '.$msg->work_type,
			), REST_Controller::HTTP_OK);
		}else{
			$average=$reputation+$clinic+$availability+$approachability + $technology;

			$this->Work_rating_model->insert(array(
					'work_id'				=> $workId,
					'reputation'			=> $reputation,
					'clinic'				=> $clinic,
					'availability'			=> $availability,
					'approachability'		=> $approachability,
					'technology'			=> $technology,
					'average_score'			=> $average/5,
					'comment'				=> $comment,
					'user_id'				=> $userId,
					'user_ip'				=> $userIp,
					'visibility'			=> $visible,
					'summary'				=> $this->get('summary'),
                    'reviewIcon'                    =>$reviewIcon,
                    'date_created'			=> date('Y-m-d H:i:s'),
                    'review_from'			=>2
				));

				$this->response(array(
						'status'	=> TRUE,
						'message'	=> 'Review added Successfully',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	function Add_post(){
		$workId=$this->post('workId');
		$userId=$this->post('userId');
		$reputation=$this->post('reputation');
		$clinic=$this->post('clinic');
		$availability=$this->post('availability');
		$approachability=$this->post('approachability');
		$technology=$this->post('technology');
		$comment=$this->post('comment');
		$userIp=$this->post('userIp');
		$visible=$this->post('visible');
                $reviewIcon=$this->post('reviewIcon');
		$checkDuplicate=$this->Work_rating_model->checkDuplicate($workId,$userId);
		$msg=$this->db->get_where('work_master',array('work_id'=>$workId))->row();
		if(count($checkDuplicate)>0){
			$this->response(array(
				'status'	=> FALSE,
				'message' => 'You have already rated this '.$msg->work_type,
			), REST_Controller::HTTP_OK);
		}else{
			$average=$reputation+$clinic+$availability+$approachability + $technology;

			$this->Work_rating_model->insert(array(
					'work_id'				=> $workId,
					'reputation'			=> $reputation,
					'clinic'				=> $clinic,
					'availability'			=> $availability,
					'approachability'		=> $approachability,
					'technology'			=> $technology,
					'average_score'			=> $average/5,
					'comment'				=> $comment,
					'user_id'				=> $userId,
					'user_ip'				=> $userIp,
					'visibility'			=> $visible,
					'summary'				=> $this->post('summary'),
                    'reviewIcon'            => $reviewIcon,
                    'date_created'			=> date('Y-m-d H:i:s'),
                    'review_from'			=>2
				));

				$this->response(array(
						'status'	=> TRUE,
						'message'	=> 'Review added Successfully',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}
	
	function reviewRating_get(){

		$allReviewRate=$this->Work_rating_model->reviewRating($this->get('workId'));

		if(empty($allReviewRate)){
			$data=array('reputation'=>array('avg'=>0,'count'=>0),'clinic'=>array('avg'=>0,'count'=>0),'availability'=>array('avg'=>0,'count'=>0),'approachability'=>array('avg'=>0,'count'=>0),
			'technology'=>array('avg'=>0,'count'=>0)
			);
			$timeAge='';
		}else{
		$data=array('reputation'=>array('avg'=>$allReviewRate->Reputation,'count'=>$allReviewRate->TotalReputation),'clinic'=>array('avg'=>$allReviewRate->clinic,'count'=>$allReviewRate->totalClinic),'availability'=>array('avg'=>$allReviewRate->availability,'count'=>$allReviewRate->totalAvaibility),'approachability'=>array('avg'=>$allReviewRate->approachability,'count'=>$allReviewRate->totalApproachability),
			'technology'=>array('avg'=>$allReviewRate->technology,'count'=>$allReviewRate->totalTechnology)
			);
			$timeAge=$this->timeAgo(strtotime($allReviewRate->dateCreate));
		}
		

		$this->response(array(
					'status'	=> TRUE,
					'data'      =>$data,
					'commentTime'=>$timeAge,
					'message'	=> 'Data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		
		
	}

	function reviewRating_post(){

		$allReviewRate=$this->Work_rating_model->reviewRating($this->post('workId'));

		if(empty($allReviewRate)){
			$data=array('reputation'=>array('avg'=>0,'count'=>0),'clinic'=>array('avg'=>0,'count'=>0),'availability'=>array('avg'=>0,'count'=>0),'approachability'=>array('avg'=>0,'count'=>0),
			'technology'=>array('avg'=>0,'count'=>0)
			);
			$timeAge='';
		}else{
		$data=array('reputation'=>array('avg'=>$allReviewRate->Reputation,'count'=>$allReviewRate->TotalReputation),'clinic'=>array('avg'=>$allReviewRate->clinic,'count'=>$allReviewRate->totalClinic),'availability'=>array('avg'=>$allReviewRate->availability,'count'=>$allReviewRate->totalAvaibility),'approachability'=>array('avg'=>$allReviewRate->approachability,'count'=>$allReviewRate->totalApproachability),
			'technology'=>array('avg'=>$allReviewRate->technology,'count'=>$allReviewRate->totalTechnology)
			);
			$timeAge=$this->timeAgo(strtotime($allReviewRate->dateCreate));
		}
		

		$this->response(array(
					'status'	=> TRUE,
					'data'      =>$data,
					'commentTime'=>$timeAge,
					'message'	=> 'Data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		
	}

	function ratingUserDetail_get(){
		$workId=$this->get('workId');

		$data=$this->Work_rating_model->userDetail($workId);
		//print_r($data); 
		if(empty($data) || empty($workId)){
			$this->response(array(
								'status'	=> FALSE,
								'message'	=> 'No review found',
							), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			
			foreach ($data as $dataRow) {
			$dataArray[]=array('userId'=>$dataRow->user_id,
							'photo'=>$dataRow->Photo,
							'name'=>$dataRow->first_name.' '.$dataRow->last_name,
							'address'=>$dataRow->address,
							'dateTime'=>$dataRow->date_created,
							'avgRating'=>$dataRow->average_score,
							'comment'=>$dataRow->comment,
							'review'=>'',
                                                        'work_review_id'=>$dataRow->work_rating_id,
							'commentTime'=>$this->timeAgo(strtotime($dataRow->dateCreate)),
							'visible'=>$dataRow->visible,
                                                        'reviewIcon'=>$dataRow->reviewIcon
							);
			}	
			$this->response(array(
						'status'	=> TRUE,
						'data'      =>$dataArray,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	
	}

	function ratingUserDetail_post(){
		$workId=$this->post('workId');

		$data=$this->Work_rating_model->userDetail($workId);
		//print_r($data);
		if(empty($data) || empty($workId)){
			$this->response(array(
								'status'	=> FALSE,
								'message'	=> 'No review found',
							), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			foreach ($data as $dataRow) {
				
			$dataArray[]=array('userId'=>$dataRow->user_id,
							'photo'=>$dataRow->Photo,
							'name'=>$dataRow->first_name.' '.$dataRow->last_name,
							'dateTime'=>$dataRow->date_created,
							'avgRating'=>$dataRow->average_score,
							'comment'=>$dataRow->comment,
							'review'=>'',
                                                        'work_review_id'=>$dataRow->work_rating_id,
							'commentTime'=>$this->timeAgo(strtotime($dataRow->dateCreate)),
							'visible'=>$dataRow->visible,
                                                        'reviewIcon'=>$dataRow->reviewIcon
							);
							
			}	
			$this->response(array(
						'status'	=> TRUE,
						'data'      =>$dataArray,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	
	}
		
	function topTenDoctor_get(){
		$lang=$this->get('lang');	
 		$data=$this->Doctors_rating_model->topTenDoctor($lang);
		//print_r($data); die();
		if(empty($data)){
			$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			$this->response(array(
						'status'	=> TRUE,
						'result'	=>$data,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	function topTenDoctor_post(){
		$lang=$this->post('lang');	
 		$data=$this->Doctors_rating_model->topTenDoctor($lang);
		//print_r($data); die();
		if(empty($data)){
			$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			$this->response(array(
						'status'	=> TRUE,
						'result'	=>$data,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	function GetReview_get(){

		$userId=$this->get('userId');
		$workId=$this->get('workId');
		$GetReview=$this->Work_rating_model->GetReview($userId,$workId);

		if(count($GetReview)>0){

			$this->response(array(
				'status'	=> TRUE,
				'result'	=> $GetReview,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}else{

			$this->response(array(
				'status'	=> FALSE,
				'result'	=> array(),
				'message'	=> 'Data not found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}


	}

	function GetReview_post(){

		$userId=$this->post('userId');
		$workId=$this->post('workId');
		$GetReview=$this->Work_rating_model->GetReview($userId,$workId);

		if(count($GetReview)>0){

			$this->response(array(
				'status'	=> TRUE,
				'result'	=> $GetReview,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}else{

			$this->response(array(
				'status'	=> FALSE,
				'result'	=> array(),
				'message'	=> 'Data not found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}
	}

	function UserByWorkReviews_get(){
		$userId=$this->get('userId');
		$lang=$this->get('lang');
		
		$UserByWorkReviews=$this->Work_rating_model->UserByWorkReviews($userId,$lang);
		$UserByDoctorReviews=$this->Doctors_rating_model->UserByDoctorReviews($userId,$lang);
		$checkUserAvailable=$this->db->get_where('user_master',array('user_id'=>$userId))->row();

		$data1	= array();
		$data2	= array();

		if(count($checkUserAvailable)>0){

			if(count($UserByWorkReviews)>0 || count($UserByDoctorReviews)>0){
				foreach ($UserByWorkReviews as $key => $value) {
					$data1[]=array('workId'=>$value->Id,
								'workName'=>$value->Name,
								'avgReview'=>$value->avgReview,
								'comment'=>$value->comment,
								'commentTime'=>$this->timeAgo(strtotime($value->dateCreate)),
								'work_type'=>$value->work_type,
								);
                }
                foreach ($UserByDoctorReviews as $key1 => $value1) {
					$data2[]=array('workId'=>$value1->Id,
								'workName'=>$value1->Name,
								'avgReview'=>$value1->avgReview,
								'comment'=>$value1->comment,
								'commentTime'=>$this->timeAgo(strtotime($value1->dateCreate)),
								'work_type'=>'Doctor',
								);
                }

                $data=array_merge($data1,$data2);

			$this->response(array(
				'status'	=> TRUE,
				'result'	=> $data,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

			}else{

				$this->response(array(
					'status'	=> FALSE,
					'result'	=> array(),
					'message'	=> 'Data not found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code

			}
		}

	}

	function UserByWorkReviews_post(){
		$userId=$this->post('userId');
		$lang=$this->post('lang');

		$UserByWorkReviews=$this->Work_rating_model->UserByWorkReviews($userId,$lang);
		$UserByDoctorReviews=$this->Doctors_rating_model->UserByDoctorReviews($userId,$lang);
		$checkUserAvailable=$this->db->get_where('user_master',array('user_id'=>$userId))->row();

		$data1	= array();
		$data2	= array();

		if(count($checkUserAvailable)>0){

			if(count($UserByWorkReviews)>0 || count($UserByDoctorReviews)>0){
				foreach ($UserByWorkReviews as $key => $value) {
					$data1[]=array('workId'=>$value->Id,
								'workName'=>$value->Name,
								'avgReview'=>$value->avgReview,
								'comment'=>$value->comment,
								'commentTime'=>$this->timeAgo(strtotime($value->dateCreate)),
								'work_type'=>$value->work_type,
								);
                }
                foreach ($UserByDoctorReviews as $key1 => $value1) {
					$data2[]=array('workId'=>$value1->Id,
								'workName'=>$value1->Name,
								'avgReview'=>$value1->avgReview,
								'comment'=>$value1->comment,
								'commentTime'=>$this->timeAgo(strtotime($value1->dateCreate)),
								'work_type'=>'Doctor',
								);
                }

                $data=array_merge($data1,$data2);

			$this->response(array(
				'status'	=> TRUE,
				'result'	=> $data,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

			}else{

				$this->response(array(
					'status'	=> FALSE,
					'result'	=> array(),
					'message'	=> 'Data not found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code

			}
		}

	}
        
        /**
         * Rating details
         */
        function workReviewDetails_post()
        {
            $reviewId = $this->post('reviewId');
            $lang = $this->post('lang');
            $UserByWorkReviews=$this->Work_rating_model->getRatingById($reviewId,$lang);
            //echo $this->db->last_query();
            
            if(count($UserByWorkReviews)>0){

			$this->response(array(
				'status'	=> TRUE,
				'result'	=> $UserByWorkReviews,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}else{

			$this->response(array(
				'status'	=> FALSE,
				'result'	=> array(),
				'message'	=> 'Data not found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}
        }
        /**
         * Rating details
         */
        function workReviewDetails_get()
        {
            $reviewId = $this->get('reviewId');
            $lang = $this->get('lang');
            $UserByWorkReviews=$this->Work_rating_model->getRatingById($reviewId,$lang);
            //echo $this->db->last_query();
            
            if(count($UserByWorkReviews)>0){

			$this->response(array(
				'status'	=> TRUE,
				'result'	=> $UserByWorkReviews,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}else{

			$this->response(array(
				'status'	=> FALSE,
				'result'	=> array(),
				'message'	=> 'Data not found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}
        }

	function timeAgo($time_ago){
	  $cur_time   = time();
	  $time_elapsed   = $cur_time - $time_ago;
	  $seconds  = $time_elapsed ;
	  $minutes  = round($time_elapsed / 60 );
	  $hours    = round($time_elapsed / 3600);
	  $days     = round($time_elapsed / 86400 );
	  $weeks    = round($time_elapsed / 604800);
	  $months   = round($time_elapsed / 2600640 );
	  $years    = round($time_elapsed / 31207680 );
	  if($seconds <= 60){$time= "$seconds seconds ago";}
	  else if($minutes <=60){
	    if($minutes==1){$time= "1 minute ago";}else{$time= "$minutes minutes ago";}
	  }else if($hours <=24){
	    if($hours==1){$time= "an hour ago";}else{$time= "$hours hours ago";}
	  }else if($days <= 7){
	    if($days==1){$time= "$days day ago";}else{$time= "$days days ago";}
	  }else if($weeks <= 4.3){
	    if($weeks==1){$time= "1 week ago";}else{$time= "$weeks weeks ago";}
	  }else if($months <=12){
	    if($months==1){$time= "1 month ago";}else{$time= "$months months ago";}
	  }else{
	    if($years==1){$time= "1 year ago";}else{$time= "$years years ago";}
	  }

	  return $time;
	}
}
