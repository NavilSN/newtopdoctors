<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Worktype_bookmark extends REST_Controller {

	public $data = array();

	function __construct() {
		parent::__construct();
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=utf-8");
		//load model
		$this->load->model('Worktype_bookmark_master_model');
		$this->load->model('Bookmark_master_model');
		//set the default lang
		$this->data['lang'] = 'en_EN';
		//basic config
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
	}

	function Add_get(){
		$workId=$this->get('workId');
		$userId=$this->get('userId');
		$checkdata=$this->Worktype_bookmark_master_model->checkMain($workId,$userId);
		$getWorkName=$this->db->get_where('work_master',array('work_id'=>$workId))->row();
		//echo $checkdata->name_en; die();
		if(count($checkdata)>0){
			if($checkdata->status=='0'){
				$this->Worktype_bookmark_master_model->update($checkdata->worktype_bookmark_id,array(
					'user_id'		=> $workId,
					'work_id'		=> $userId,
					'status'		=> '1',
				));

				$this->response(array(
					'result'	=> 'Bookmarked',
					'status'	=> TRUE,
					'message'	=> 'Bookmarked',
					'name'		=> $checkdata->name_en,
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
			}else{
				
				$this->Worktype_bookmark_master_model->update($checkdata->worktype_bookmark_id, array(
					'user_id'		=> $userId,
					'work_id'		=> $workId,
					'status'		=> '0',
				));

				$this->response(array(
					'result'	=> 'Unbookmarked',
					'status'	=> TRUE,
					'message'	=> 'Unbookmarked',
					'name'		=> $checkdata->name_en,
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
			}	
			
		}else{
			$this->Worktype_bookmark_master_model->insert(array(
				'user_id'		=> $userId,
				'work_id'		=> $workId,
				'status'		=> '1',
			));

			$this->response(array(
					'result'	=> 'Bookmark is added',
					'status'	=> TRUE,
					'message'	=> 'Bookmark is added',
					'name'		=> $getWorkName->name_en,
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}

	}

	function Add_post(){
		$workId=$this->post('workId');
		$userId=$this->post('userId');
		$checkdata=$this->Worktype_bookmark_master_model->checkMain($workId,$userId);
		$getWorkName=$this->db->get_where('work_master',array('work_id'=>$workId))->row();
		//echo count($checkdata);die();
		if(count($checkdata)>0){
			if($checkdata->status=='0'){
				//echo $checkdata->status; die();
				$this->Worktype_bookmark_master_model->update($checkdata->worktype_bookmark_id, array(
					'user_id'		=> $userId,
					'work_id'		=> $workId,
					'status'		=> '1',
				));

				$this->response(array(
					'result'	=> 'Bookmarked',
					'status'	=> TRUE,
					'message'	=> 'Bookmarked',
					'name'		=> $checkdata->name_en,					
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
			}else{
			
				$this->Worktype_bookmark_master_model->update($checkdata->worktype_bookmark_id, array(
					'user_id'		=> $userId,
					'work_id'		=> $workId,
					'status'		=> '0',
				));
				$this->response(array(
					'result'	=> 'Unbookmarked',
					'status'	=> TRUE,
					'message'	=> 'Unbookmarked',					
					'name'		=> $checkdata->name_en,
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
			}	

		}else{
		$this->Worktype_bookmark_master_model->insert(array(
				'user_id'		=> $userId,
				'work_id'		=> $workId,
				'status'		=> '1',
			));

			$this->response(array(
					'result'	=> 'Bookmark is added',
					'status'	=> TRUE,
					'message'	=> 'Bookmark is added',					
					'name'		=> $getWorkName->name_en,
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
		
	}

	function bookmarkList_get(){
		
 		$userId=$this->get('userId');

		$data1=$this->Worktype_bookmark_master_model->bookmarkList($userId);
		$data2=$this->Bookmark_master_model->bookmarkList($userId);
		$data=array_merge($data1,$data2);
		if(empty($data) || empty($userId)){
			$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			$this->response(array(
						'status'	=> TRUE,
						'result'	=>$data,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	function bookmarkList_post(){
		
 		$userId=$this->post('userId');

		$data1=$this->Worktype_bookmark_master_model->bookmarkList($userId);
		$data2=$this->Bookmark_master_model->bookmarkList($userId);
		$data=array_merge($data1,$data2);
		
		if(empty($data) || empty($userId)){
			$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			$this->response(array(
						'status'	=> TRUE,
						'result'	=>$data,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

}