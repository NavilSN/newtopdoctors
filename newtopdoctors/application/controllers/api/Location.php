<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Location extends REST_Controller {

	/**
	* Constructor
	*
	*  @return void
	*/
	function __construct() {
		parent::__construct();
		$this->load->model('Location_master_model');
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
	}

	function getLocation_get(){
		$locationEN=$this->Location_master_model->get_locationEN();	
		$locationAR=$this->Location_master_model->get_locationAR();	
		if($this->get('lang')=='ar'){
			if(!empty($locationAR)) {
				$this->response(array(
					'status'	=> TRUE,
					'message'	=> 'Location details found',
					'data'		=> $locationAR
				), REST_Controller::HTTP_OK);
			} else {
				$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found!',
					'data'		=> array()
				), REST_Controller::HTTP_OK);
			}
		}else{	

			if(!empty($locationEN)) {
				$this->response(array(
					'status'	=> TRUE,
					'message'	=> 'Location details found',
					'data'		=> $locationEN
				), REST_Controller::HTTP_OK);
			} else {
				$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found!',
					'data'		=> array()
				), REST_Controller::HTTP_OK);
			}

		}
		
	}

	function getLocation_post(){
		$locationEN=$this->Location_master_model->get_locationEN();	
		$locationAR=$this->Location_master_model->get_locationAR();	
		if($this->post('lang')=='ar'){
			if(!empty($locationAR)) {
				$this->response(array(
					'status'	=> TRUE,
					'message'	=> 'Location details found',
					'data'		=> $locationAR
				), REST_Controller::HTTP_OK);
			} else {
				$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found!',
					'data'		=> array()
				), REST_Controller::HTTP_OK);
			}
		}else{	

			if(!empty($locationEN)) {
				$this->response(array(
					'status'	=> TRUE,
					'message'	=> 'Location details found',
					'data'		=> $locationEN
				), REST_Controller::HTTP_OK);
			} else {
				$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found!',
					'data'		=> array()
				), REST_Controller::HTTP_OK);
			}

		}
	}

	function getAllLocation($lang = 'en') {
		if ($lang == 'ar') {
			$this->db->select('location_id AS id, name AS name, parent_id AS parent');
		} else {
			$this->db->select('location_id AS id, name_en AS name, parent_id AS parent');
		}
		$this->db->from('location_master');
		$this->db->where('parent_id', 0);
		$this->db->where('status', '1');
		$result = $this->db->get();

		return $result->result();
	}

	function getChildLocation($id, $lang = 'en') {
		if ($lang == 'ar') {
			$this->db->select('location_id AS id, name AS name, parent_id AS parent');
		} else {
			$this->db->select('location_id AS id, name_en AS name, parent_id AS parent');
		}
		$this->db->from('location_master');
		$this->db->where('parent_id', $id);
		$this->db->where('status', '1');
		$result = $this->db->get();

		return $result->result();
	}

	function getParentChildLocation_get() {
		//$this->output->enable_profiler(TRUE);
		//$this->db->cache_on();
		$parents = $this->getAllLocation($this->get('lang'));
		$finalresult = array();
		$counter = 0;

		if ($this->get('lang') == 'ar') {
			foreach ($parents as $parent) {
				$finalresult[$counter]['id'] = $parent->id;
				$finalresult[$counter]['name'] = $parent->name;
				
				$childs = $this->getChildLocation($parent->id, $this->get('lang'));
				$finalresult[$counter]['childs'] = $childs;
				$counter++;
			}
		} else {
			foreach ($parents as $parent) {
				$finalresult[$counter]['id'] = $parent->id;
				$finalresult[$counter]['name'] = $parent->name;
				
				$childs = $this->getChildLocation($parent->id, $this->get('lang'));
				$finalresult[$counter]['childs'] = $childs;
				$counter++;
			}
		}

		$this->response(array(
			'status'	=> TRUE,
			'message'	=> count($finalresult) . ' locations found',
			'data'		=> $finalresult
		), REST_Controller::HTTP_OK);
	}
}