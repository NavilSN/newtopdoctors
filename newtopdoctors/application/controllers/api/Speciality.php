<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Speciality extends REST_Controller {

	public $data = array();

	function __construct() {
		parent::__construct();
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=utf-8");
		//load model
		$this->load->model('Specialty_master_model');
		//set the default lang
		$this->data['lang'] = 'en_EN';
		//basic config
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
	}

	function index_get() {		
	
		if($this->get('lang') != '') {
			$this->data['lang'] = $this->get('lang');
		}

		if($this->data['lang'] == 'ar') {
			$specialities = $this->Specialty_master_model->arabic_speciality();
			$this->response(array(
				'status'	=> TRUE,
				'data'	=> $specialities,
				'message'	=> count($specialities) . ' result found!',
			), REST_Controller::HTTP_OK);
		} else {
			$specialities = $this->Specialty_master_model->english_speciality();
			$this->response(array(
				'status'	=> TRUE,
				'data'	=> $specialities,
				'message'	=> count($specialities) . ' result found!'
			), REST_Controller::HTTP_OK);
		}		
	}

	function index_post() {
		if($this->post('lang') != '') {
			$this->data['lang'] = $this->post('lang');
		}

		if($this->data['lang'] == 'ar') {
			$specialities = $this->Specialty_master_model->arabic_speciality();
			$this->response(array(
				'status'	=> TRUE,
				'data'	=> $specialities,
				'message'	=> count($specialities) . ' result found!'
			), REST_Controller::HTTP_OK);
		} else {
			$specialities = $this->Specialty_master_model->english_speciality();
			$this->response(array(
				'status'	=> TRUE,
				'data'	=> $specialities,
				'message'	=> count($specialities) . ' result found!'
			), REST_Controller::HTTP_OK);
		}		
	}
}