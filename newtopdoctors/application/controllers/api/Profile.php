<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends REST_Controller
{

    private $imagepath = '';

    public function __construct()
    {
        parent::__construct();
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=utf-8");
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->model('Profile_model');
        $this->load->model('Country_master_model');
        $this->load->model('User_master_model');

        //trim all data
        foreach ($_POST as $key => $val) {
            $_POST[$key] = trim($val);
        }
    }

    public function doctorProfile_post()
    {
        $id = $this->post('doctorId');
        $userId = $this->post('userId');
        $lang = $this->post('lang');
        $result = $this->Profile_model->doctorProfile($id, $lang, $userId);
        $result2 = $this->Profile_model->doctorProfileBookmark($id, $userId);
        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => 'Doctor details found',
                'data' => $result,
                'BookmarkUserId' => $result2->BookmarkUserId,
                'BookmarkStatus' => $result2->BookmarkStatus,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found!',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    public function doctorProfile_get()
    {
        $id = $this->get('doctorId');
        $userId = $this->get('userId');
        $lang = $this->get('lang');
        $result = $this->Profile_model->doctorProfile($id, $lang, $userId);
        $result2 = $this->Profile_model->doctorProfileBookmark($id, $userId);
        //print_r($result2);
        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => 'Doctor details found',
                'data' => $result,
                'BookmarkUserId' => $result2->BookmarkUserId,
                'BookmarkStatus' => $result2->BookmarkStatus,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found!',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * Hospital profile get method
     */
    public function hospitalProfile_get()
    {
        $hospitalid = $this->get('hospitalId');
        $userId = $this->get('userId');
        $lang = $this->get('lang');
        $result = $this->Profile_model->hospitalProfile($hospitalid, $lang);
        $result2 = $this->Profile_model->hospitalProfileBookmark($hospitalid, $userId);
        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => 'hospital details are found',
                'data' => $result,
                'totalDcotors' => $this->Profile_model->countDoctors($hospitalid),
                'hospitalSpecialities' => $this->Profile_model->hospitalSpecialities($hospitalid, $lang),
                'BookmarkUserId' => $result2->BookmarkUserId,
                'BookmarkStatus' => $result2->BookmarkStatus,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found!',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    public function hospitalProfile_post()
    {
        $hospitalid = $this->post('hospitalId');
        $userId = $this->post('userId');
        $lang = $this->post('lang');
        $result = $this->Profile_model->hospitalProfile($hospitalid, $lang);
        $result2 = $this->Profile_model->hospitalProfileBookmark($hospitalid, $userId);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => 'hospital details are found',
                'data' => $result,
                'totalDcotors' => $this->Profile_model->countDoctors($hospitalid),
                'hospitalSpecialities' => $this->Profile_model->hospitalSpecialities($hospitalid, $lang),
                'BookmarkUserId' => $result2->BookmarkUserId,
                'BookmarkStatus' => $result2->BookmarkStatus,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found!',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    public function clinicProfile_get()
    {
        $clinicId = $this->get('clinicId');
        $lang = $this->get('lang');
        $userId = $this->get('userId');
        $result = $this->Profile_model->clinicProfile($clinicId, $lang);
        $result2 = $this->Profile_model->clinicProfileBookmark($clinicId, $userId);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'BookmarkUserId' => $result2->BookmarkUserId,
                'BookmarkStatus' => $result2->BookmarkStatus,
                'totalDcotors' => $this->Profile_model->countDoctors($clinicId),
                'message' => 'clinic details found',
                'mipmap' => $this->Profile_model->workTypeSpeciality($clinicId),
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }

    }

    public function clinicProfile_post()
    {
        $clinicId = $this->post('clinicId');
        $lang = $this->post('lang');
        $userId = $this->post('userId');
        $result = $this->Profile_model->clinicProfile($clinicId, $lang);
        $result2 = $this->Profile_model->clinicProfileBookmark($clinicId, $userId);
        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'BookmarkUserId' => $result2->BookmarkUserId,
                'BookmarkStatus' => $result2->BookmarkStatus,
                'totalDcotors' => $this->Profile_model->countDoctors($clinicId),
                'message' => 'clinic details found',
                'mipmap' => $this->Profile_model->workTypeSpeciality($clinicId),
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }

    }

    public function labProfile_get()
    {
        $labId = $this->get('labId');
        $lang = $this->get('lang');
        $userId = $this->get('userId');

        $result = $this->Profile_model->labProfile($labId, $lang);
        $result2 = $this->Profile_model->labProfileBookmark($labId, $userId);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'BookmarkUserId' => $result2->BookmarkUserId,
                'BookmarkStatus' => $result2->BookmarkStatus,
                'totalDcotors' => $this->Profile_model->countDoctors($labId),
                'message' => 'lab details found',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found!',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    public function labProfile_post()
    {
        $labId = $this->post('labId');
        $lang = $this->post('lang');
        $userId = $this->post('userId');

        $result = $this->Profile_model->labProfile($labId, $lang);
        $result2 = $this->Profile_model->labProfileBookmark($labId, $userId);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'BookmarkUserId' => $result2->BookmarkUserId,
                'BookmarkStatus' => $result2->BookmarkStatus,
                'totalDcotors' => $this->Profile_model->countDoctors($labId),
                'message' => 'lab details found',
                'mipmap' => $this->Profile_model->workTypeSpeciality($labId),
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found!',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    public function hospitalDoctorList_get()
    {
        $hospitalid = $this->get('hospitalId');
        $userId = $this->get('userId');
        $lang = $this->get('lang');
        $result = $this->Profile_model->hospitalDoctorList($hospitalid, $lang, $userId);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' doctors found',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * Doctor list for particular work type
     */
    public function workTypeDoctorList_get()
    {
        $workId = $this->get('workId');
        $userId = $this->get('userId');
        $lang = $this->get('lang');
        $result = $this->Profile_model->workTypeDoctorList($workId, $lang, $userId);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' doctors found',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    public function workTypeTotalDoctorsList_get()
    {
        $workId = $this->get('workId');
        $userId = $this->get('userId');
        $lang = $this->get('lang');
        $page = $this->get('page');
        $result = $this->Profile_model->workTypeTotalDoctorList($workId, $lang, $userId, $page);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' doctors found',
                'total_rows' => $this->Profile_model->totalWorkTypeDoctorCount($workId),
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * [workTypeTotalDoctorsList_post description]
     * @return [type] [description]
     */
    public function workTypeTotalDoctorsList_post()
    {
        $workId = $this->post('workId');
        $userId = $this->post('userId');
        $lang = $this->post('lang');
        $page = $this->post('page');
        $result = $this->Profile_model->workTypeTotalDoctorList($workId, $lang, $userId, $page);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' doctors found',
                'total_rows' => $this->Profile_model->totalWorkTypeDoctorCount($workId),
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    public function hospitalDoctorList_post()
    {
        $hospitalid = $this->post('hospitalId');
        $userId = $this->post('userId');
        $lang = $this->post('lang');

        $result = $this->Profile_model->hospitalDoctorList($hospitalid, $lang, $userId);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' doctors found',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'no data found',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    public function userProfile_get()
    {
        $id = $this->get('userId');
        $result = $this->Profile_model->userProfile($id);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => 'User details found',
                'data' => $result,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data found!',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    public function userProfile_post()
    {
        $id = $this->post('userId');
        $result = $this->Profile_model->userProfile($id);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => 'User details found',
                'data' => $result,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data found!',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    public function userProfileUpdateName_get()
    {
        $id = $this->get('userId');
        $name = $this->get('name');
        $data = explode(' ', $name);
        $this->db->where('user_id', $id);
        $this->db->update('user_master', array(
            'first_name' => $data[0],
            'last_name' => $data[1],
        ));
        $this->response(array(
            'status' => true,
            'message' => 'Your profile is updated successfully',
        ), REST_Controller::HTTP_OK);
    }

    public function userProfileUpdateName_post()
    {
        $id = $this->post('userId');
        $name = $this->post('name');
        $data = explode(' ', $name);
        $this->db->where('user_id', $id);
        $this->db->update('user_master', array(
            'first_name' => $data[0],
            'last_name' => $data[1],
        ));
        $this->response(array(
            'status' => true,
            'message' => 'Your profile is updated successfully',
        ), REST_Controller::HTTP_OK);
    }

    public function userProfileUpdateFullName_get()
    {
        $id = $this->get('userId');
        $firstName = $this->get('firstName');
        $lastName = $this->get('lastName');

        $checkUserAvailable = $this->db->get_where('user_master', array('user_id' => $id))->row();
        if (count($checkUserAvailable) > 0) {
            $this->db->where('user_id', $id);
            $this->db->update('user_master', array(
                'first_name' => $firstName,
                'last_name' => $lastName,
            ));
            $this->response(array(
                'status' => true,
                'message' => 'Your profile is updated successfully',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'User not found',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function userProfileUpdateFullName_post()
    {
        $id = $this->post('userId');
        $firstName = $this->post('firstName');
        $lastName = $this->post('lastName');

        $checkUserAvailable = $this->db->get_where('user_master', array('user_id' => $id))->row();
        if (count($checkUserAvailable) > 0) {
            $this->db->where('user_id', $id);
            $this->db->update('user_master', array(
                'first_name' => $firstName,
                'last_name' => $lastName,
            ));
            $this->response(array(
                'status' => true,
                'message' => 'Your profile is updated successfully',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'User not found',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function userProfileUpdateMobile_get()
    {
        $id = $this->get('userId');
        $mobile = $this->get('mobile');

        $checkUserAvailable = $this->db->get_where('user_master', array('user_id' => $id))->row();

        if (count($checkUserAvailable) > 0) {

            $this->db->where('user_id', $id);
            $this->db->update('user_master', array(
                'mobile_number' => $mobile,
            ));

            $this->response(array(
                'status' => true,
                'message' => 'Your mobile number is updated successfully',
            ), REST_Controller::HTTP_OK);
        } else {

            $this->response(array(
                'status' => true,
                'message' => 'User not found',
            ), REST_Controller::HTTP_OK);

        }
    }

    public function userProfileUpdateMobile_post()
    {
        $id = $this->post('userId');
        $mobile = $this->post('mobile');

        $checkUserAvailable = $this->db->get_where('user_master', array('user_id' => $id))->row();

        if (count($checkUserAvailable) > 0) {

            $this->db->where('user_id', $id);
            $this->db->update('user_master', array(
                'mobile_number' => $mobile,
            ));

            $this->response(array(
                'status' => true,
                'message' => 'Your mobile number is updated successfully',
            ), REST_Controller::HTTP_OK);
        } else {

            $this->response(array(
                'status' => true,
                'message' => 'User not found',
            ), REST_Controller::HTTP_OK);

        }
    }

    public function userProfileUpdateLocation_get()
    {
        $id = $this->get('userId');
        $location = $this->get('location');

        $country = $this->Country_master_model->countryDetails($location);
        $checkUserAvailable = $this->db->get_where('user_master', array('user_id' => $id))->row();

        if (count($checkUserAvailable) > 0) {

            $this->db->where('user_id', $id);
            $this->db->update('user_master', array('country_id' => $country->country_id));
            $this->response(array(
                'status' => true,
                'message' => 'Your location is updated successfully',
            ), REST_Controller::HTTP_OK);

        } else {

            $this->response(array(
                'status' => true,
                'message' => 'User not found',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function userProfileUpdateLocation_post()
    {
        $id = $this->post('userId');
        $location = $this->post('location');

        $country = $this->Country_master_model->countryDetails($location);
        $checkUserAvailable = $this->db->get_where('user_master', array('user_id' => $id))->row();

        if (count($checkUserAvailable) > 0) {

            $this->db->where('user_id', $id);
            $this->db->update('user_master', array('country_id' => $country->country_id));
            $this->response(array(
                'status' => true,
                'message' => 'Your location is updated successfully',
            ), REST_Controller::HTTP_OK);

        } else {

            $this->response(array(
                'status' => true,
                'message' => 'User not found',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function userProfileUpdateEmail_get()
    {
        $id = $this->get('userId');
        $email = $this->get('email');

        $checkUserPrivateEmail = $this->User_master_model->checkUserPrivateEmail($id, $email);
        $checkUserAvailable = $this->db->get_where('user_master', array('user_id' => $id))->row();

        if (count($checkUserAvailable) > 0) {

            if (count($checkUserPrivateEmail) > 0) {

                $this->response(array(
                    'status' => false,
                    'message' => 'Email is already exist',
                    'isSocial' => $checkUserAvailable->is_social,
                ), REST_Controller::HTTP_OK);

            } else {

                $this->db->where('user_id', $id);
                $this->db->update('user_master', array(
                    'email' => $email,
                ));

                $this->response(array(
                    'status' => true,
                    'message' => 'Your email is updated successfully',
                ), REST_Controller::HTTP_OK);
            }
        } else {

            $this->response(array(
                'status' => false,
                'message' => 'User not found',
            ), REST_Controller::HTTP_OK);

        }
    }

    public function userProfileUpdateEmail_post()
    {
        $id = $this->post('userId');
        $email = $this->post('email');

        $checkUserPrivateEmail = $this->User_master_model->checkUserPrivateEmail($id, $email);
        $checkUserAvailable = $this->db->get_where('user_master', array('user_id' => $id))->row();

        if (count($checkUserAvailable) > 0) {

            if (count($checkUserPrivateEmail) > 0) {

                $this->response(array(
                    'status' => false,
                    'message' => 'Email is already exist',
                    'isSocial' => $checkUserAvailable->is_social,
                ), REST_Controller::HTTP_OK);

            } else {

                $this->db->where('user_id', $id);
                $this->db->update('user_master', array(
                    'email' => $email,
                ));

                $this->response(array(
                    'status' => true,
                    'message' => 'Your email is updated successfully',
                ), REST_Controller::HTTP_OK);
            }
        } else {

            $this->response(array(
                'status' => false,
                'message' => 'User not found',
            ), REST_Controller::HTTP_OK);

        }

    }

    public function userProfileUpdatePic_post()
    {
        $id = $this->post('userId');
        $image = $this->post('image');
        $photoPath = '';
        $this->db->where('user_id', $id);
        $this->db->update('user_master', array(
            'photo' => $this->imagepath,
        ));

        ///upload image
        $photoPath = $this->base64_to_png($image, $id, $id);
    }

    public function userProfileUpdatePic_get()
    {
        $id = $this->get('userId');
        $image = $this->get('image');
        $photoPath = '';
        $this->db->where('user_id', $id);
        $this->db->update('user_master', array(
            'photo' => $this->imagepath,
        ));

        ///upload image
        $photoPath = $this->base64_to_png($image, $id, $id);
    }

    public function base64_to_png($base64, $imageName, $user_id)
    {
        define('UPLOAD_DIR', FCPATH . 'uploads/user_image/');
        $img = $base64;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = UPLOAD_DIR . $imageName . '.png';
        $success = file_put_contents($file, $data);
        //echo $file."<br>";
        //echo $data; die();
        $imageName .= '.png';

        //update photo
        $this->db->where('user_id', $user_id);
        $this->db->update('user_master', array(
            'photo' => $imageName,
        ));

        $this->response(array(
            'status' => true,
            'data' => base_url() . 'uploads/user_image/' . $imageName,
            'message' => 'User profile has been successfully updated',
        ), REST_Controller::HTTP_OK); // OK (200) Reponse code

        //return  $success ? $file : '';
    }

}
