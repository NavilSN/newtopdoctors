<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Doctor extends REST_Controller
{

    private $imagepath = '';

    public function __construct()
    {
        parent::__construct();
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=utf-8");
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        //load models
        $this->load->model('Doctor_master_model');
        $this->load->model('Doctor_master_en_model');
        $this->load->model('Doctor_details_model');
        $this->load->model('Specialty_master_model');
        $this->load->model('Country_master_model');
        $this->load->model('Work_master_model');
        $this->load->model('Location_master_model');

        //trim all data
        foreach ($_POST as $key => $val) {
            $_POST[$key] = trim($val);
        }
    }

    /**
     * Add doctor details
     */
    public function add_post()
    {
        $photoPath = '';

        $is_avail = $this->Doctor_details_model->get_by(array(
            'email' => $this->post('email'),
        ));
        if ($is_avail) {
            $this->response(array(
                'status' => false,
                'message' => 'This email address is already exist.',
            ), REST_Controller::HTTP_OK);
        }
        // insert data
        $doctor_id = null;
        if ($this->post('lang') == 'ar') {
            $doctor_id = $this->insertDoctorAR($_POST);
        } else {
            $doctor_id = $this->insertDoctorEN($_POST);
        }

        //upload image
        $photoPath = $this->base64ToPng($_POST['image'], $doctor_id . '_' . str_replace(' ', '', $_POST['name']), $doctor_id);

    }

    /**
     * Insert doctor in Arabic
     */
    public function insertDoctorAR($data)
    {
        $insert_id = null;

        $speciality = $this->Specialty_master_model->specialityDetails($this->post('speciality'), $this->post('lang'));
        $location = $this->Location_master_model->locationDetails($this->post('location'), $this->post('lang'));

        //Arabic data
        $insert_id = $this->Doctor_master_model->save(array(
            'first_name' => $this->post('name'),
            'last_name' => $this->post('last_name'),
            'address' => $this->post('address'),
            'biography' => $this->post('biography'),
        ));

        //EN doctor master
        $this->Doctor_master_en_model->save(array(
            'doctor_id' => $insert_id,
            'first_name' => '',
            'last_name' => '',
            'address' => '',
            'biography' => '',
        ));

        //insert work master
        $work_id = $this->Work_master_model->insert(array(
            'name' => $this->post('name'),
            'address' => $this->post('address'),
            'phone' => $this->post('mobile'),
            'email' => $this->post('email'),
            'doctor_id' => $insert_id,
            'work_type' => 'Clinic',
            'status' => '0',
        ));

        $this->Doctor_details_model->save(array(
            'doctor_id' => $insert_id,
            'email' => $this->post('email'),
            'phone_number' => $this->post('mobile'),
            'photo' => $this->imagepath,
            'work_id' => $this->post('work_type'),
            'speciality_id' => $speciality->specialties_id,
            'location_id' => $location->location_id,
            'gender' => ($this->post('gender') == 'Male' ? 1 : 2),
            'visibility' => '0',
        ));

        //send an email
        $doctor_work_type = "";
        $work_detail = $this->db->select()
            ->from('work_master')
            ->where('work_id', $_POST['work_type'])
            ->get()
            ->row();
        @$doctor_work_type = $work_detail->name;
        $this->send_email($_POST['name'], $_POST['clinic_name'], $_POST['email'], $_POST['mobile'],
            $_POST['location'], $_POST['address'], $_POST['speciality'], $_POST['gender'],
            'en', $doctor_work_type);

        return $insert_id;
    }

    /**
     * Insert doctor in EN by default
     */
    public function insertDoctorEN($data)
    {
        $insert_id = null;

        $speciality = $this->Specialty_master_model->specialityDetails($this->post('speciality'), $this->post('lang'));
        $location = $this->Location_master_model->locationDetails($this->post('location'), $this->post('lang'));

        //Arabic data
        $insert_id = $this->Doctor_master_model->save(array(
            'first_name' => '',
            'last_name' => '',
            'address' => '',
            'biography' => '',
        ));

        //EN doctor master
        $this->Doctor_master_en_model->save(array(
            'doctor_id' => $insert_id,
            'first_name' => $this->post('name'),
            'last_name' => $this->post('last_name'),
            'address' => $this->post('address'),
            'biography' => $this->post('biography'),
        ));

        //insert work master
        $work_id = $this->Work_master_model->insert(array(
            'name_en' => $this->post('clinic_name'),
            'address_en' => $this->post('address'),
            'phone' => $this->post('mobile'),
            'email' => $this->post('email'),
            'doctor_id' => $insert_id,
            'work_type' => 'Clinic',
            'status' => '0',
        ));

        $this->Doctor_details_model->save(array(
            'doctor_id' => $insert_id,
            'email' => $this->post('email'),
            'photo' => $this->imagepath,
            'work_id' => $this->post('work_type'),
            'speciality_id' => $speciality->specialties_id,
            'location_id' => $location->location_id,
            'phone_number' => $this->post('mobile'),
            'gender' => ($this->post('gender') == 'Male' ? 1 : 2),
            'visibility' => '0',
        ));

        //send an email
        $doctor_work_type = "";
        $work_detail = $this->db->select()
            ->from('work_master')
            ->where('work_id', $_POST['work_type'])
            ->get()
            ->row();
        @$doctor_work_type = $work_detail->name_en;
        $this->send_email($_POST['name'], $_POST['clinic_name'], $_POST['email'], $_POST['mobile'],
            $_POST['location'], $_POST['address'], $_POST['speciality'], $_POST['gender'],
            'en', $doctor_work_type);

        return $insert_id;
    }

    /**
     * Convert base64 to png
     */
    public function base64ToPng($base64, $imageName, $doctor_id)
    {
        define('UPLOAD_DIR', FCPATH . 'uploads/doctor_certificate/');
        $img = $base64;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = UPLOAD_DIR . $imageName . '.png';

        $success = file_put_contents($file, $data);

        $imageName .= '.png';

        //update photo
        $this->db->where('doctor_id', $doctor_id);
        $this->db->update('doctor_details', array(
            'photo' => $imageName,
        ));

        $this->response(array(
            'status' => true,
            'data' => array(),
            'message' => 'Your request is successfully sent.',
        ), REST_Controller::HTTP_OK); // OK (200) Reponse code

        //return  $success ? $file : '';
    }

    public function doctorlistGet()
    {

        $data = $this->db->select('doctor_details.doctor_id as id,CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS name')
            ->join('doctor_master', 'doctor_master.doctor_id=doctor_details.doctor_id')
            ->join('doctor_master_en', 'doctor_master_en.doctor_id=doctor_details.doctor_id')
            ->order_by('doctor_master_en.first_name', 'ASC')
            ->get('doctor_details')->result();

        foreach ($data as $dataRow) {
            $dataArray[] = $dataRow;
        }
        $this->response(array(
            'status' => true,
            'data' => $dataArray,
            'message' => 'Doctor List found',
        ), REST_Controller::HTTP_OK); // OK (200) Reponse code
    }

    public function doctorlistPost()
    {

        $data = $this->db->select('doctor_details.doctor_id as id,CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS name')
            ->join('doctor_master', 'doctor_master.doctor_id=doctor_details.doctor_id')
            ->join('doctor_master_en', 'doctor_master_en.doctor_id=doctor_details.doctor_id')
            ->order_by('doctor_master_en.first_name', 'ASC')
            ->get('doctor_details')->result();

        foreach ($data as $dataRow) {
            $dataArray[] = $dataRow;
        }
        $this->response(array(
            'status' => true,
            'data' => $dataArray,
            'message' => 'Doctor List found',
        ), REST_Controller::HTTP_OK); // OK (200) Reponse code
    }

    public function nearbydoctors_post()
    {
        $filter = array();

        $filter['lat'] = $this->post('lat');
        $filter['long'] = $this->post('long');
        $filter['userId'] = $this->post('userId');
        $filter['lang'] = $this->post('lang');
        $filter['isnearby'] = $this->post('isnearby');
        $filter['topratestfirst'] = $this->post('topratestfirst');
        $filter['page'] = $this->post('page');
        $filter['perPage'] = "10";

        $result = $this->Doctor_master_en_model->getNearByDoctor($filter);
        $total_rows = $this->Doctor_master_en_model->getNearByDoctorNumRows($filter);

        if (count($result) > 0) {

            $this->response(array(
                'status' => true,
                'result' => $result,
                'message' => 'Data found',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code

        } else {

            $this->response(array(
                'status' => false,
                'result' => array(),
                'message' => 'Data not found',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code

        }
    }

    public function nearbydoctors_get()
    {
        $filter = array();

        $filter['lat'] = $this->get('lat');
        $filter['long'] = $this->get('long');
        $filter['userId'] = $this->get('userId');
        $filter['lang'] = $this->get('lang');
        $filter['isnearby'] = $this->get('isnearby');
        $filter['topratestfirst'] = $this->get('topratestfirst');
        $filter['page'] = $this->get('page');
        $filter['perPage'] = "10";

        $result = $this->Doctor_master_en_model->getNearByDoctor($filter);
        //$total_rows = $this->Doctor_master_en_model->getNearByDoctorNumRows($filter);

        if (count($result) > 0) {

            $this->response(array(
                'status' => true,
                'result' => $result,
                'message' => 'Data found',
                'total_rows' => 10, //$total_rows
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code

        } else {

            $this->response(array(
                'status' => false,
                'result' => array(),
                'message' => 'Data not found',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code

        }
    }

    public function send_email($name, $clinic, $email, $mobile, $location, $address,
        $speciality, $gender, $lang, $work_type) {

        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->email->from(ADMIN_EMAIL); // change it to yours
        $this->email->to(ADMIN_EMAIL); // change it to yours
        $this->email->cc('mehul.raninga@searchnative.com');
        $this->email->subject('Top Doctors - New Doctor Registration');

        $message = "Hello Top Doctors,";
        $message .= "<br/><br/>A new doctor information has been reqested for Top Doctors.";
        //$message .= "<br/><br/><a href='www.google.com'>Click here</a> ";
        //$message .= "following link to approve this request for Top Doctors.";
        $message .= "<br/><br/><b>Doctor name: </b>" . $name;
        //$message .= "<br/><b>Clinc name: </b>" . $clinic;
        $message .= "<br/><b>Email: </b>" . $email;
        $message .= "<br/><b>Mobile: </b>" . $mobile;
        $message .= "<br/><b>Location: </b>" . $location;
        $message .= "<br/><b>Address: </b>" . $address;
        $message .= "<br/><b>Gender: </b>" . ($gender == '1' ? 'Male' : 'Female');
        $message .= "<br/><b>Speciality: </b>" . $speciality;
        $message .= "<br/><b>Work: </b>" . $work_type;

        $this->email->message($message);
        if (!$this->email->send()) {
            $this->response(array(
                'status' => false,
                'message' => 'Error occured while sending an email',
            ), REST_Controller::HTTP_OK);
        }
    }
}
