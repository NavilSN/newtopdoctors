<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller
{

    /**
     * Constructor
     *
     *  @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_master_model');
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        //trim all data
        foreach ($_POST as $key => $val) {
            $_POST[$key] = trim($val);
        }

        foreach ($_GET as $key => $val) {
            $_GET[$key] = trim($val);
        }
    }

    /**
     * User register GET method
     */
    public function register_get()
    {
        if (count($_GET)) {
            $is_avail = $this->User_master_model->get_by(array(
                'email' => $this->get('email'),
            ));
            if ($is_avail) {
                $this->response(array(
                    'status' => false,
                    'message' => 'Your email address is already registered with Top Doctors.',
                ), REST_Controller::HTTP_OK);
            }

            $userId = $this->User_master_model->insert(array(
                'first_name' => $this->get('first_name'),
                'last_name' => $this->get('last_name'),
                'email' => $this->get('email'),
                'mobile_number' => $this->get('mobile'),
                'password' => hash('md5', $this->get('password') . config_item('encryption_key')),
                'country_id' => $this->country_code_by_country_get(),
                'address' => $this->get('address'),
                'register_from' => 2,
                'registration_date' => date('Y-m-d'),
            ));

            $this->response(array(
                'status' => true,
                'message' => 'You have successfully registered and logged in.',
                'loginId' => $userId,
                'first_name' => $this->get('first_name'),
                'last_name' => $this->get('last_name'),
                'country' => $this->get('country_name'),
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Invalid method name',
            ), REST_Controller::HTTP_OK);
        }

    }

    /**
     * Register POST action
     */
    public function register_post()
    {
        if (count($_POST)) {
            //check for email is avail
            $is_avail = $this->User_master_model->get_by(array(
                'email' => $this->post('email'),
            ));
            if ($is_avail) {
                $this->response(array(
                    'status' => false,
                    'message' => 'Your email address is already registered with Top Doctors.',
                ), REST_Controller::HTTP_OK);
            }
            //insert
            $userId = $this->User_master_model->insert(array(
                'first_name' => $this->post('first_name'),
                'last_name' => $this->post('last_name'),
                'email' => $this->post('email'),
                'mobile_number' => $this->post('mobile'),
                'password' => hash('md5', $this->post('password') . config_item('encryption_key')),
                'country_id' => $this->country_code_by_country_post(),
                'address' => $this->post('address'),
                'register_from' => 2,
                'registration_date' => date('Y-m-d'),
            ));

            $this->response(array(
                'status' => true,
                'message' => 'You have successfully registered and logged in.',
                'loginId' => $userId,
                'first_name' => $this->post('first_name'),
                'last_name' => $this->post('last_name'),
                'country' => $this->post('country_name'),
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Inavalid method name',
            ), REST_Controller::HTTP_OK);
        }

    }

    /**
     * Login GET action
     */
    public function login_get()
    {
        $email = $this->get('email');
        $password = $this->get('password');

        if ($email === null || $password === null) {
            //Invalid parameters
            $this->response(array(
                'status' => false,
                'message' => 'Invalid credentials',
            ), REST_Controller::HTTP_OK);
        }

        $password = hash('md5', $password . config_item('encryption_key'));

        $user = $this->User_master_model->login_details($email, $password);
        if (empty($user->photo)) {
            $photo = 'user_profile_default_image.png';
        } else {
            $photo = $user->photo;
        }
        if (!empty($user)) {
            //user is avail
            $this->response(array(
                'status' => true,
                'message' => 'You have successfully logged in.',
                'email' => $user->email,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'id' => $user->user_id,
                'country' => $user->country_name,
                'photo' => base_url() . 'uploads/user_image/' . $photo,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Invalid username or password',
            ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * Login POST action
     */
    public function login_post()
    {
        $email = $this->post('email');
        $password = $this->post('password');

        if ($email === null || $password === null) {
            //Invalid parameters
            $this->response(array(
                'status' => false,
                'message' => 'Invalid credentials',
            ), REST_Controller::HTTP_OK);
        }

        $password = hash('md5', $password . config_item('encryption_key'));

        $user = $this->User_master_model->login_details($email, $password);

        if (empty($user->photo)) {
            $photo = 'user_profile_default_image.png';
        } else {
            $photo = $user->photo;
        }

        if (!empty($user)) {
            //user is avail
            $this->response(array(
                'status' => true,
                'message' => 'You have successfully logged in.',
                'email' => $user->email,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'id' => $user->user_id,
                'country' => $user->country_name,
                'photo' => base_url() . 'uploads/user_image/' . $photo,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Invalid username or password',
            ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * Forgot password GET action
     */
    public function forgot_password_get()
    {
        $email = trim($this->get('email'));

        if ($email === null) {
            $this->response(array(
                'status' => false,
                'message' => 'Email address is not valid!',
            ), REST_Controller::HTTP_OK);
        }

        //find user
        $user = $this->User_master_model->get_by(array(
            'email' => $email,
        ));

        if (!empty($user)) {
            //send email
            $is_sent = $this->sent_email($email);

            if ($is_sent) {
                $this->response(array(
                    'status' => true,
                    'message' => 'Forgot password email is successfully sent!',
                ), REST_Controller::HTTP_OK);
            } else {
                $this->response(array(
                    'status' => true,
                    'message' => 'Error occured while sending mail',
                ), REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Email is not registered with us.',
            ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * Forgot password POST action
     */
    public function forgot_password_post()
    {
        $email = trim($this->post('email'));

        if ($email === null) {
            $this->response(array(
                'status' => false,
                'message' => 'Email address is not valid!',
            ), REST_Controller::HTTP_OK);
        }

        //find user
        $user = $this->User_master_model->get_by(array(
            'email' => $email,
        ));

        if (!empty($user)) {
            //send email
            $is_sent = $this->sent_email($email);

            if ($is_sent) {
                $this->response(array(
                    'status' => true,
                    'message' => 'Forgot password email is successfully sent!',
                ), REST_Controller::HTTP_OK);
            } else {
                $this->response(array(
                    'status' => true,
                    'message' => 'Error occured while sending mail',
                ), REST_Controller::HTTP_OK);
            }

        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Email is not registered with us.',
            ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * Send Email for forgot password
     */
    public function sent_email($to)
    {

        // New Token Update
        $this->db->where('email', $to);
        $this->db->update('user_master', array('token' => bin2hex(openssl_random_pseudo_bytes(16))));

        $checkEmail = $this->db->get_where('user_master', array('email' => $to))->row();
        $url = base_url() . $checkEmail->token;

        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->email->clear(true);
        $this->email->from('tejas.patel@searchnative.in', 'Searchnative Pvt Ltd');
        $this->email->to($to);
        $this->email->subject('Reset password link');
        $message = 'This is reset password link message from Top Doctors <br/>';
        $message .= $url;
        $this->email->message($message);

        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
    //Set New password
    public function setNewPassword_get()
    {
        $token = $this->get('token');
        $newPassword = $this->get('newPassword');
        $confirmPassword = $this->get('confirmPassword');

        $checkToken = $this->db->get_where('user_master', array('token' => $token))->row();

        if (count($checkToken) > 0) {

            $this->db->where('token', $token);
            $this->db->update('user_master', array('password' => hash('md5', $confirmPassword . config_item('encryption_key'))));

            $this->db->where('token', $token);
            $this->db->update('user_master', array('token' => ' '));
            $this->response(array(
                'status' => true,
                'message' => 'Password updated successfully',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Token mismatch',
            ), REST_Controller::HTTP_OK);
        }
    }
    //Set New password
    public function setNewPassword_post()
    {
        $token = $this->post('token');
        $newPassword = $this->post('newPassword');
        $confirmPassword = $this->post('confirmPassword');
        $checkToken = $this->db->get_where('user_master', array('token' => $token))->row();

        if (count($checkToken) > 0) {

            $this->db->where('token', $token);
            $this->db->update('user_master', array('password' => hash('md5', $confirmPassword . config_item('encryption_key'))));

            $this->db->where('token', $token);
            $this->db->update('user_master', array('token' => ' '));
            $this->response(array(
                'status' => true,
                'message' => 'Password updated successfully',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Token mismatch',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function country_code_by_country_get()
    {
        $country_name = $this->get('country_name');
        $this->load->model('Country_master_model');
        $country = $this->Country_master_model->get_by(array(
            'country_name' => $country_name,
        ));

        if (count($country)) {
            return $country->country_id;
        }

        return '';
    }

    public function country_code_by_country_post()
    {
        $country_name = $this->post('country_name');
        $this->load->model('Country_master_model');
        $country = $this->Country_master_model->get_by(array(
            'country_name' => $country_name,
        ));

        if (count($country)) {
            return $country->country_id;
        }

        return '';
    }

    public function facebookLogin_get()
    {
        $nameNew = explode(" ", $this->get('name'));
        $name = $this->get('name');
        $token = $this->get('token');
        $email = $this->get('email');

        if (empty($name) || empty($token) || empty($email)) {
            $this->response(array(
                'status' => true,
                'message' => 'Please fill all the parameter',
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code
        } else {
            $checkfbtoken = $this->db->get_where('user_master', array('email' => $email))->row();
            if (count($checkfbtoken) == 0) {
                $userId = $this->User_master_model->insert(array(
                    'first_name' => $nameNew[0],
                    'last_name' => $nameNew[1],
                    'email' => $email,
                    'is_social' => '1',
                    'fbtoken' => $token,
                    'register_from' => 2,
                    'registration_date' => date('Y-m-d'),
                ));
            }
            $userId = $this->db->get_where('user_master', array('email' => $email))->row();
            $this->response(array(
                'status' => true,
                'message' => 'You have successfully logged in.',
                'name' => $name,
                'loginId' => $userId->user_id,
                'fbToken' => $token,
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code
        }
    }

    public function facebookLogin_post()
    {
        $nameNew = array();
        $nameNew = explode(" ", $this->post('name'));
        $name = $this->post('name');
        $token = $this->post('token');
        $email = $this->post('email');
        if (empty($name) || empty($token) || empty($email)) {
            $this->response(array(
                'status' => true,
                'message' => 'Please fill all the parameter',
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code
        } else {
            $checkfbtoken = $this->db->get_where('user_master', array('email' => $email))->row();
            if (count($checkfbtoken) == 0) {
                $userId = $this->User_master_model->insert(array(
                    'first_name' => $nameNew[0],
                    'last_name' => $nameNew[1],
                    'email' => $email,
                    'is_social' => '1',
                    'fbtoken' => $token,
                    'register_from' => 2,
                    'registration_date' => date('Y-m-d'),
                ));
            }
            $userId = $this->db->get_where('user_master', array('email' => $email))->row();
            $this->response(array(
                'status' => true,
                'message' => 'You have successfully logged in.',
                'name' => $name,
                'loginId' => $userId->user_id,
                'fbToken' => $token,
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code
        }
    }

    public function recentViewData_post()
    {
        $userId = $this->post('userId');
        $lang = $this->post('lang');
        $recentlyViewed = $this->post('recentlyViewed');

        //$page = $this->post('page');
        //$perPage = "10";
        $userReview = $this->User_master_model->getRecentView($userId, $lang, $recentlyViewed);

        if (count($userReview) > 0) {

            $this->response(array(
                'status' => true,
                'result' => $userReview,
                'message' => 'Data found',
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code

        } else {

            $this->response(array(
                'status' => false,
                'result' => array(),
                'message' => 'Data not found',
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code

        }

    }
    public function recentViewData_get()
    {
        $userId = $this->get('userId');
        $lang = $this->get('lang');
        $recentlyViewed = $this->get('recentlyViewed');

        //$page = $this->post('page');
        //$perPage = "10";
        $userReview = $this->User_master_model->getRecentView($userId, $lang, $recentlyViewed);
        if (count($userReview) > 0) {

            $this->response(array(
                'status' => true,
                'result' => $userReview,
                'message' => 'Data found',
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code

        } else {

            $this->response(array(
                'status' => false,
                'result' => array(),
                'message' => 'Data not found',
            ), REST_Controller::HTTP_OK); // OK (200) Reponse code

        }

    }

}
