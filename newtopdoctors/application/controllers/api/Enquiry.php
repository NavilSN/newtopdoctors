<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Enquiry extends REST_Controller {

	/**
	 * Constructor
	 *
	 * @return  void 
	 */
	function __construct() {
		parent::__construct();
		$this->load->model('Enquiry_master_model');

		//trim all data
		foreach ($_POST as $key => $val) {
			$_POST[$key] = trim($val);
		}
	}

	/**
	 * Add new user enquiry
	 */
	function add_post() {
		$user_id = $this->input->post('user_id');
		$work_id = $this->input->post('work_id');
		$work_type = $this->input->post('work_type');
		$description = $this->input->post('description');
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');

		//insert enquiry
		$insert_id = $this->Enquiry_master_model->insert(array(
			'user_id'		=> $user_id,
			'work_id'		=> $work_id,
			'work_type'		=> $work_type,
			'description'	=> $description,
			'name'			=> $name,
			'email'			=> $email,
			'phone'			=> $phone
		));

		if ($insert_id) {
			//send email to user and doctor
			$email_data['description'] = $description;
			$email_data['to_email'] = $email;
			$email_data['user_name'] = $name;
			$email_data['user_email'] = $email;
			$email_data['user_mobile'] = $phone;

			$this->get_work_type_details($work_id, $user_id, $work_type, $email_data);
			$this->response(array(
				'status'	=> TRUE,
				'message'	=> 'Thank you for your enquiry. Your message has been sent successfully.'
			), REST_Controller::HTTP_OK);
		} else {
			$this->response(array(
				'status'	=> FALSE,
				'message'	=> 'Error occured while inserting record'
			), REST_Controller::HTTP_OK);
		}
	}

	function view_get() {
		$enquiry = $this->Enquiry_master_model->get_all_enquiry();

		$this->response(array(
			'status'	=> TRUE,
			'data'		=> $enquiry,
			'message'	=> count($enquiry) . ' are found!'
		), REST_Controller::HTTP_OK);
	}

	/**
	 * Email to user
	 * @param  array  $data 
	 */
	function email_to_user($data = array()) {
		$data['subject'] = 'Top Dotcor - Enquiry Confirmation';
		$message = "Hi " . $data['user_name'] . ",";
		$message .= "<br/><br/>Thanks for your enquiry.";
		$message .= "<br/><br/>We've mention doctor information which we hope you'll find useful.";
		$message .= "<br/><br/><b>Your enquiry: </b>" . $data['description'];
		$message .= "<br/><br/><b>Doctor Name: </b>" . $data['name'];
		$message .= "<br/><b>Doctor Email: </b>" . $data['email'];
		$message .= "<br/><b>Doctor Phone: </b>" . $data['phone'];
		$message .= "<br/><b>Doctor Mobile: </b>" . $data['mobile'];
		$message .= "<br/><br/>We looking forward to hearing from you.";
		$message .= "<br/><br/>King regards,";
		$message .= "<br/>Top Doctors team";
		$data['message'] = $message;
		$this->sendMail($data);
	}

	/**
	 * Email to doctor
	 * @param  array
	 */
	function email_to_doctor($data = array()) {
		$data['subject'] = 'Top Dotcor - Enquiry Confirmation';
		$data['to_email'] = $data['email'];
		$message = "Hi " . $data['name'] . ",";
		$message .= "<br/><br/><b>User enquiry: <b/>" . $data['description'];
		$message .= "<br/><br/>We've mention doctor information which we hope you'll find useful.";
		$message .= "<br/><br/><b>User Name: </b>" . $data['user_name'];
		$message .= "<br/><b>User Mobile: </b>" . $data['user_mobile'];
		$message .= "<br/><b>User Email: </b>" . $data['user_email'];
		$data['message'] = $message;
		$this->sendMail($data);
	}

	/**
	 * Get work type or doctor details
	 * @param  string $id   
	 * @param  string $type 
	 * @param  array $email_data
	 */
	function get_work_type_details($id, $user_id, $type, $email_data) {

		if ( isset($id) && isset($type)) {
			$this->load->model('User_master_model');
			$user = $this->User_master_model->get($user_id);
			if ($user) {
				/*$email_data['to_email'] = $user->email;
				$email_data['user_name'] = $user->first_name . ' ' . $user->last_name;
				$email_data['user_email'] = $user->email;
				$email_data['user_mobile'] = $user->mobile_number;*/
			}			
			
			switch ($type) {
				case 'Doctor':
					# code...
					$this->load->model('Doctor_details_model');
					$doctor = $this->Doctor_details_model->doctor_details($id);
					$email_data['name'] = $doctor->first_name . ' ' . $doctor->last_name;
					$email_data['email'] = $doctor->email;
					$email_data['phone'] = $doctor->phone_number;
					$email_data['mobile'] = $doctor->mobile_number;				
					$this->email_to_user($email_data);
					$email_data['to_email'] = $doctor->email;
					$this->email_to_doctor($email_data);
					break;
				case 'Clinic':
				case 'Medical Lab':
				case 'Radiology Lab':
				case 'Lab':
					# code...
					$this->load->model('Work_master_model');
					$clinic = $this->Work_master_model->get($id);
					$email_data['name'] = $clinic->name_en;
					$email_data['email'] = $clinic->email;
					$email_data['phone'] = $clinic->phone;
					$email_data['mobile'] = '';		
					$this->email_to_user($email_data);
					$email_data['to_email'] = $clinic->email;						
					$this->email_to_doctor($email_data);	
					break;
				default:
					$email_data = array();
			}

		}
	}

	/**
	 * Send email to doctor and user
	 * @param  array $data
	 */
	function sendMail($data) {
		$this->load->library('email');
	    $this->email->set_newline("\r\n");
	    $this->email->from('mayur.ghadiya@searchnative.in'); // change it to yours
	    $this->email->to($data['to_email']);// change it to yours
	    $this->email->subject($data['subject']);
	    $this->email->message($data['message']);
	    if(!$this->email->send()) {
	    	$this->response(array(
				'status'	=> FALSE,
				'message'	=> 'Error occured while sending an email'
			), REST_Controller::HTTP_OK);
	    }
	}
}
