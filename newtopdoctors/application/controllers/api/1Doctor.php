<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends REST_Controller {

	private $imagepath = '';

	function __construct() {
		parent::__construct();
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=utf-8");
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        //load models
        $this->load->model('Doctor_master_model');
		$this->load->model('Doctor_master_en_model');
		$this->load->model('Doctor_details_model');
		$this->load->model('Specialty_master_model');
		$this->load->model('Country_master_model');
		$this->load->model('Work_master_model');
		$this->load->model('Location_master_model');
	}

	/**
	* Add doctor details
	*/
	function add_post() {
		$photoPath = '';
		
		$is_avail = $this->Doctor_details_model->get_by(array(
				'email'	=> $this->post('email'),
			));
			if($is_avail) {
				$this->response([
					'status'	=> FALSE,
					'message'	=> 'This email address is already exist.'
				], REST_Controller::HTTP_OK);
			}

		// insert data
		$doctor_id = NULL;
		if($this->post('lang') == 'ar') {
			$doctor_id = $this->insertDoctorAR($_POST);
		} else {
			$doctor_id = $this->insertDoctorEN($_POST);
		}

		//upload image
		$photoPath = $this->base64_to_png($_POST['image'], $doctor_id . '_' . str_replace(' ', '', $_POST['name']), $doctor_id);


	}

	/**
	* Insert doctor in Arabic
	*/
	function insertDoctorAR($data) {
		$insert_id = NULL;

		$speciality = $this->Specialty_master_model->specialityDetails($this->post('speciality'),$this->post('lang'));
		$location = $this->Location_master_model->locationDetails($this->post('location'),$this->post('lang'));

		//Arabic data
		$insert_id  = $this->Doctor_master_model->save([
			'first_name'	=> $this->post('name'),
			'last_name'		=> '',
			'address'		=> '',
		]);

		//EN doctor master
		$this->Doctor_master_en_model->save([
			'doctor_id'		=> $insert_id,
			'first_name'	=> '',
			'last_name'		=> '',
			'address'		=> '',
		]);

		//insert work master
		$work_id = $this->Work_master_model->insert([
			'name'			=> $this->post('name'),
			'address'		=> $this->post('address'),
			'phone'			=> $this->post('mobile'),
			'email'			=> $this->post('email'),
			'doctor_id'		=> $insert_id,
			'work_type'		=> 'Clinic',
			'status'		=> '1'
		]);

		$this->Doctor_details_model->save([
			'doctor_id'		=> $insert_id,
			'email'			=> $this->post('email'),
			'phone_number'	=> $this->post('mobile'),
			'photo'			=> $this->imagepath,
			'work_id'       => $work_id,
			'speciality_id'	=> $speciality->specialties_id,
			'location_id'	=> $location->location_id,
			'gender'		=> ($this->post('gender') == 'Male' ? 1 : 2)
		]);
		return $insert_id;
	}

	/**
	* Insert doctor in EN by default
	*/
	function insertDoctorEN($data) {
		$insert_id = NULL;

		$speciality = $this->Specialty_master_model->specialityDetails($this->post('speciality'),$this->post('lang'));
		$location = $this->Location_master_model->locationDetails($this->post('location'),$this->post('lang'));

		//Arabic data
		$insert_id  = $this->Doctor_master_model->save([
			'first_name'	=> '',
			'last_name'		=> '',
			'address'		=> '',
		]);

		//EN doctor master
		$this->Doctor_master_en_model->save([
			'doctor_id'		=> $insert_id,
			'first_name'	=> $this->post('name'),
			'last_name'		=> '',
			'address'		=> $this->post('address'),
		]);

		//insert work master
		$work_id=$this->Work_master_model->insert([
			'name_en'		=> $this->post('name'),
			'address_en'	=> $this->post('address'),
			'phone'			=> $this->post('mobile'),
			'email'			=> $this->post('email'),
			'doctor_id'		=> $insert_id,
			'work_type'		=> 'Clinic',
			'status'		=> '1'
		]);

		$this->Doctor_details_model->save([
			'doctor_id'		=> $insert_id,
			'email'			=> $this->post('email'),
			'photo'			=> $this->imagepath,
			'work_id'       => $work_id,
			'speciality_id'	=> $speciality->specialties_id,
			'location_id'	=> $location->location_id,
			'phone_number'	=> $this->post('mobile'),
			'gender'		=> ($this->post('gender') == 'Male' ? 1 : 2)
		]);
		return $insert_id;
	}

	/**
	* Convert base64 to png
	*/
	function base64_to_png($base64, $imageName, $doctor_id) {
		define('UPLOAD_DIR', FCPATH . 'uploads/doctor_certificate/');
		$img = $base64;
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file = UPLOAD_DIR . $imageName . '.png';
		
		$success = file_put_contents($file, $data);

		$imageName .= '.png';

		//update photo
		$this->db->where('doctor_id', $doctor_id);
		$this->db->update('doctor_details', [
			'photo'	=> $imageName
		]);
		
		$this->response([
			'status'	=> TRUE,
			'data'      => [],
			'message'	=> 'Doctor is successfully added!',
		], REST_Controller::HTTP_OK); // OK (200) Reponse code

		//return  $success ? $file : '';
	}

	function doctorList_get(){

		$data=$this->db->select('doctor_details.doctor_id as id,CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS name')
		->join('doctor_master','doctor_master.doctor_id=doctor_details.doctor_id')
		->join('doctor_master_en','doctor_master_en.doctor_id=doctor_details.doctor_id')
		->order_by('doctor_master_en.first_name','ASC')
		->get('doctor_details')->result();

		foreach ($data as $dataRow) {
			$dataArray[]=$dataRow;
		}	
			$this->response([
						'status'	=>  TRUE,
						'data'      =>  $dataArray,
						'message'	=> 'Doctor List found',
					], REST_Controller::HTTP_OK); // OK (200) Reponse code
	}

	function doctorList_post(){

		$data=$this->db->select('doctor_details.doctor_id as id,CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS name')
		->join('doctor_master','doctor_master.doctor_id=doctor_details.doctor_id')
		->join('doctor_master_en','doctor_master_en.doctor_id=doctor_details.doctor_id')
		->order_by('doctor_master_en.first_name','ASC')
		->get('doctor_details')->result();

		foreach ($data as $dataRow) {
			$dataArray[]=$dataRow;
		}	
			$this->response([
						'status'	=> TRUE,
						'data'      =>$dataArray,
						'message'	=> 'Doctor List found',
					], REST_Controller::HTTP_OK); // OK (200) Reponse code
	}

}