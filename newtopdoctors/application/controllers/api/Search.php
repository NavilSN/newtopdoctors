<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Search extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=utf-8");
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        //load model
        $this->load->model('Search_model');
        $this->load->model('Profile_model');

        //trim all data
        foreach ($_POST as $key => $val) {
            $_POST[$key] = trim($val);
        }

        foreach ($_GET as $key => $val) {
            $_GET[$key] = trim($val);
        }
    }

    public function filter_post()
    {
        $filters = array(
            'location' => $this->post('location'),
            'speciality' => $this->post('speciality'),
            'gender' => $this->post('gender'),
            'name' => $this->post('name'),
            'searchdata' => $this->post('searchdata'),
            'searchType' => $this->post('searchType'),
            'userId' => $this->post('userId'),
            'lang' => $this->post('lang'),
            'page' => $this->post('page'),
            'lat' => $this->post('lat'),
            'long' => $this->post('long'),
            'isnearby' => $this->post('isnearby'),
            'topratestfirst' => $this->post('topratestfirst'),
            'perPage' => 10,
        );
        //check for blank filters
        switch ($filters['searchType']) {
            case 'hospital':
                # code...
                if ($filters['name'] == '') {
                    $filters['name'] = null;
                }
                if ($filters['location'] == '') {
                    $filters['location'] = null;
                }
                if ($filters['speciality'] == '') {
                    $filters['speciality'] = null;
                }
                if ($filters['topratestfirst'] == '' || $filters['topratestfirst'] == "0") {
                    $filters['topratestfirst'] = null;
                }
                if (is_null($filters['location']) && is_null($filters['speciality']) &&
                    is_null($filters['name']) && is_null($filters['topratestfirst'])) {
                    $result = $this->Search_model->hospitalSearch($filters['lang'], $filters['userId'], $filters['page'], 10);
                    $total_rows = $this->Search_model->hospitalSearchNumRows($filters['lang'], $filters['userId']);
                } else {
                    //var_dump($filters);exit;
                    if ($filters['location']) {
                        $childLocations = array();
                        $this->load->helper('location');
                        $parentLocation = getParentIdFromName($filters['location'], $filters['lang']);
                        $childs = getChildInfo($parentLocation->location_id);

                        foreach ($childs as $child) {
                            if ($filters['lang'] == 'ar') {
                                array_push($childLocations, $child->name);
                            } else {
                                array_push($childLocations, $child->name_en);
                            }
                        }
                        $filters['childs'] = $childLocations;
                    }
                    $result = $this->Search_model->filterSearch($filters);
                    $total_rows = $this->Search_model->filterSearchNumRows($filters);
                }
                break;

            case 'clinic':
                if ($filters['name'] == '') {
                    $filters['name'] = null;
                }
                if ($filters['location'] == '') {
                    $filters['location'] = null;
                }
                if ($filters['speciality'] == '') {
                    $filters['speciality'] = null;
                }
                if ($filters['topratestfirst'] == '' || $filters['topratestfirst'] == "0") {
                    $filters['topratestfirst'] = null;
                }
                if (is_null($filters['location']) && is_null($filters['speciality']) &&
                    is_null($filters['name']) && is_null($filters['topratestfirst'])) {
                    $result = $this->Search_model->clinicSearch($filters['lang'], $filters['userId'], $filters['page'], 10);
                    $total_rows = $this->Search_model->clinicSearchNumRows($filters['lang'], $filters['userId']);
                } else {
                    if ($filters['location']) {
                        $childLocations = array();
                        $this->load->helper('location');
                        $parentLocation = getParentIdFromName($filters['location'], $filters['lang']);
                        $childs = getChildInfo($parentLocation->location_id);

                        foreach ($childs as $child) {
                            if ($filters['lang'] == 'ar') {
                                array_push($childLocations, $child->name);
                            } else {
                                array_push($childLocations, $child->name_en);
                            }
                        }
                        $filters['childs'] = $childLocations;
                    }
                    $result = $this->Search_model->filterSearch($filters);
                    $total_rows = $this->Search_model->filterSearchNumRows($filters);
                }
                break;

            case 'Radiology Lab':
            case 'Medical Lab':
            case 'Lab':
                if ($filters['searchType'] == 'Radiology Lab') {
                    $filters['speciality'] = 'Radiology Lab';
                } else if ($filters['searchType'] == 'Medical Lab') {
                    $filters['speciality'] = 'Medical Lab';
                } else {
                    $filters['speciality'] = 'Radiology Lab';
                }
                if ($filters['name'] == '') {
                    $filters['name'] = null;
                }
                if ($filters['location'] == '') {
                    $filters['location'] = null;
                }
                if ($filters['speciality'] == '') {
                    $filters['speciality'] = null;
                }
                if ($filters['topratestfirst'] == '' || $filters['topratestfirst'] == "0") {
                    $filters['topratestfirst'] = null;
                }

                if (is_null($filters['location']) &&
                    is_null($filters['name']) && is_null($filters['topratestfirst'])) {
                    $result = $this->Search_model->labSearch($filters);
                    $total_rows = $this->Search_model->labSearchNumRows($filters);
                } else {
                    if ($filters['location']) {
                        $childLocations = array();
                        $this->load->helper('location');
                        $parentLocation = getParentIdFromName($filters['location'], $filters['lang']);
                        $childs = getChildInfo($parentLocation->location_id);

                        foreach ($childs as $child) {
                            if ($filters['lang'] == 'ar') {
                                array_push($childLocations, $child->name);
                            } else {
                                array_push($childLocations, $child->name_en);
                            }
                        }
                        $filters['childs'] = $childLocations;
                    }
                    $result = $this->Search_model->filterSearch($filters);
                    $total_rows = $this->Search_model->filterSearchNumRows($filters);
                }
                break;

            default:
                if ($filters['location']) {
                    $childLocations = array();
                    $this->load->helper('location');
                    $parentLocation = getParentIdFromName($filters['location'], $filters['lang']);
                    $childs = getChildInfo($parentLocation->location_id);

                    foreach ($childs as $child) {
                        if ($filters['lang'] == 'ar') {
                            array_push($childLocations, $child->name);
                        } else {
                            array_push($childLocations, $child->name_en);
                        }
                    }
                    $filters['childs'] = $childLocations;
                }
                $result = $this->Search_model->filterSearch($filters);
                $total_rows = $this->Search_model->filterSearchNumRows($filters);
                break;
        }

        //return $result;
        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => array(),
                'message' => 'No data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function testfilter_get()
    {
        echo "dakho";die;
    }

    public function filter_get()
    {

        $filers['location'] = $this->get('location');
        $filers['speciality'] = $this->get('speciality');
        $filers['gender'] = $this->get('gender');
        $filers['name'] = $this->get('name');
        $filers['searchdata'] = $this->get('searchdata');
        $filers['searchType'] = $this->get('searchType');
        $filers['userId'] = $this->get('userId');
        $filers['lang'] = $this->get('lang');
        $filers['page'] = $this->get('page');
        $filers['lat'] = $this->get('lat');
        $filers['long'] = $this->get('long');
        $filers['isnearby'] = $this->get('isnearby');
        $filers['topratestfirst'] = $this->get('topratestfirst');
        $filers['page'] = $this->get('page');
        $filers['perPage'] = '10';
        if ($filers['searchdata'] != "") {
            $filers['name'] = $filers['searchdata'];
            $filers['location'] = $filers['searchdata'];
            $filers['speciality'] = $filers['searchdata'];

        }

        $result = $this->Search_model->filterSearch($filers);
        $total_rows = $this->Search_model->filterSearchNumRows($filers);

        //return $result;
        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => array(),
                'message' => 'No data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }

    }

    /**
     * Filter search
     * English US
     */
    public function filterSearch_post()
    {
        $location = $this->post('location');
        $speciality = $this->post('speciality');
        $gender = $this->post('gender');
        $name = $this->post('name');
        $searchKeyword = $this->post('searchdata');
        $searchType = $this->post("searchType");
        $userId = $this->post("userId");
        $isnearby = $this->post('isnearby');
        $topratestfirst = $this->post('topratestfirst');
        $lang = $this->post('lang');
        $lat = $this->post('lat');
        $long = $this->post('long');
        $page = $this->post('page');
        $perPage = 10;
        $filters = array(
            'location' => $location,
            'speciality' => $speciality,
            'gender' => $gender,
            'name' => $name,
            'search' => $searchKeyword,
            'searchType' => $searchType,
            'userId' => $userId,
            'lang' => $lang,
            'page' => $page,
            'perPage' => $perPage,
            'topratestfirst' => $topratestfirst,
            'isnearby' => $isnearby,
            'lat' => $lat,
            'long' => $long,

        );

        $result = $this->Search_model->search($filters);
        $total_rows = $this->Search_model->searchNumRows($filters);
        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => array(),
                'message' => 'No data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * Filter search get
     * English US
     */
    public function filterSearch_get()
    {
        $location = $this->get('location');
        $speciality = $this->get('speciality');
        $gender = $this->get('gender');
        $name = $this->get('name');
        $searchKeyword = $this->get('searchdata');
        $searchType = $this->get("searchType");
        $userId = $this->get("userId");
        $lang = $this->get('lang');
        $lat = $this->post('lat');
        $long = $this->post('long');
        $isnearby = $this->get('isnearby');
        $topratestfirst = $this->get('topratestfirst');
        $page = $this->post('page');
        $perPage = 10;
        $filters = array(
            'location' => $location,
            'speciality' => $speciality,
            'gender' => $gender,
            'name' => $name,
            'search' => $searchKeyword,
            'searchType' => $searchType,
            'userId' => $userId,
            'lang' => $lang,
            'page' => $page,
            'perPage' => $perPage,
            'topratestfirst' => $topratestfirst,
            'isnearby' => $isnearby,
            'lat' => $lat,
            'long' => $long,
        );

        $result = $this->Search_model->search($filters);
        $total_rows = $this->Search_model->searchNumRows($filters);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => array(),
                'message' => 'No data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function globalSearch_post()
    {
        $search = trim($this->post('search'));
        //$search = substitute_word($search);
        $userId = (int) $this->post('userId');
        $lang = trim($this->post('lang'));
        $page = (int) $this->post('page');
        $perPage = 10;

        if ($page < 1) {
            $page = 1;
        }
        $limit = ($page - 1) * $perPage;

        //global seatch modification 10/08/2017
        if ($search == '') {
            $total_rows_doctor = $this->db->count_all_results('doctor_master_en');
            $total_rows_work = $this->db->count_all_results('work_master');
            $total_rows = $total_rows_doctor + $total_rows_work;
            if ($lang == 'ar') {
                $total_doctor_result = $this->Search_model->all_doctor_result_ar($limit, $perPage, $userId);
                $total_work_result = $this->Search_model->all_works_result_ar($limit, $perPage, $userId);
            } else {
                $total_doctor_result = $this->Search_model->all_doctor_result($limit, $perPage, $userId);
                $total_work_result = $this->Search_model->all_works_result($limit, $perPage, $userId);
            }

            $result = array_merge($total_doctor_result, $total_work_result);
        } else {
            $result = $this->Search_model->global_search_new($search, $lang, $userId, $limit, $perPage);
            //echo $this->db->last_query();exit;

            $total_rows_work = $this->Search_model->work_global_search_result_count($search, $lang, $userId);
            $total_rows_doctor = $this->Search_model->doctor_global_search_result_count($search, $lang, $userId);
            $total_rows = $total_rows_work + $total_rows_doctor;
        }

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' data found!',
                'total_rows' => $total_rows,
                'total_doctor' => $total_rows_doctor,
                'page' => $page,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data found!',
                'data' => array(),
            ), REST_Controller::HTTP_OK);
        }
    }

    public function globalSearch_get()
    {
        $search = $this->get('search');
        $userId = $this->get('userId');
        $lang = $this->get('lang');
        $page = $this->get('page');
        $perPage = 10;
        $result = $this->Search_model->globalSearch($search, $lang, $userId, $page, $perPage); // 2 sec
        $total_rows_work = $this->Search_model->workMasterGlobalSearchNumRows($search, $lang, $userId);
        $total_rows_doctor = $this->Search_model->doctorMasterGlobalSearchNumRows($search, $lang, $userId);
        $total_rows = ($total_rows_work + $total_rows_doctor);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' data found!',
                'total_rows' => $total_rows,
                'page' => $page,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => true,
                'message' => 'No data found!',
                'data' => array(),
                'total_rows' => $total_rows,
                'page' => $page,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function hospitalSearch_post()
    {
        $lang = $this->post('lang');
        $userId = $this->post('userId');
        $page = $this->post('page');
        $perPage = 10;
        $result = $this->Search_model->hospitalSearch($lang, $userId, $page, $perPage);
        $total_rows = $this->Search_model->hospitalSearchNumRows($lang, $userId);
        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => count($result) . ' data found!',
                'data' => $result,
                'total_rows' => $total_rows,
            ));
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data found!',
                'data' => array(),
                'total_rows' => $total_rows,
            ));
        }
    }

    public function hospitalSearch_get()
    {
        $lang = $this->get('lang');
        $userId = $this->get('userId');
        $page = $this->get('page');
        $perPage = 10;

        $result = $this->Search_model->hospitalSearch($lang, $userId, $page, $perPage);
        $total_rows = $this->Search_model->hospitalSearchNumRows($lang, $userId);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => count($result) . ' data found!',
                'data' => $result,
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data found!',
                'data' => array(),
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function clinicSearch_post()
    {
        $lang = $this->post('lang');
        $userId = $this->post('userId');
        $page = $this->post('page');
        $perPage = 10;
        $result = $this->Search_model->clinicSearch($lang, $userId, $page, $perPage);
        $total_rows = $this->Search_model->clinicSearchNumRows($lang, $userId);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => array(),
                'message' => 'No data found',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function clinicSearch_get()
    {
        $lang = $this->get('lang');
        $userId = $this->get('userId');
        $page = $this->get('page');
        $perPage = 10;
        $result = $this->Search_model->clinicSearch($lang, $userId, $page, $perPage);
        $total_rows = $this->Search_model->clinicSearchNumRows($lang, $userId);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'data' => $result,
                'message' => count($result) . ' data found!',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'data' => array(),
                'message' => 'No data found',
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function labSearch_post()
    {
        $filters = array(
            'gender' => $this->post('gender'),
            'speciality' => $this->post('speciality'),
            'lang' => $this->post('lang'),
            'userId' => $this->post('userId'),
            'page' => $this->post('page'),
            'perPage' => 10,
        );

        $result = $this->Search_model->labSearch($filters);
        $total_rows = $this->Search_model->labSearchNumRows($filters);
        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => count($result) . ' data found!',
                'data' => $result,
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data found!',
                'data' => array(),
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function labSearch_get()
    {
        $filters = array(
            'gender' => $this->get('gender'),
            'speciality' => $this->get('speciality'),
            'lang' => $this->get('lang'),
            'userId' => $this->get('userId'),
            'page' => $this->get('page'),
            'perPage' => 10,
        );

        $result = $this->Search_model->labSearch($filters);
        $total_rows = $this->Search_model->labSearchNumRows($filters);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => count($result) . ' data found!',
                'data' => $result,
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data found!',
                'data' => array(),
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function doctorSearch_post()
    {
        $filters = array(
            'location' => $this->post('location'),
            'speciality' => $this->post('speciality'),
            'gender' => $this->post('gender'),
            'userId' => $this->post('userId'),
            'lang' => $this->post('lang'),
            'page' => $this->post('page'),
            'perPage' => 10,
        );

        if ($filters['location']) {
            $childLocations = array();
            $this->load->helper('location');
            $parentLocation = getParentIdFromName($filters['location'], $filters['lang']);
            $childs = getChildInfo($parentLocation->location_id);

            foreach ($childs as $child) {
                if ($filters['lang'] == 'ar') {
                    array_push($childLocations, $child->name);
                } else {
                    array_push($childLocations, $child->name_en);
                }
            }
            $filters['childs'] = $childLocations;
        }

        $result = $this->Search_model->doctorSearch($filters);
        $total_rows = $this->Search_model->doctorSearchNumRows($filters);

        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => count($result) . ' data found!',
                'data' => $result,
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data found!',
                'data' => array(),
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }

    public function doctorSearch_get()
    {
        $filters = array(
            'location' => $this->get('location'),
            'speciality' => $this->get('speciality'),
            'gender' => $this->get('gender'),
            'userId' => $this->get('userId'),
            'lang' => $this->get('lang'),
            'page' => $this->get('page'),
            'perPage' => 10,
        );

        $result = $this->Search_model->doctorSearch($filters);
        $total_rows = $this->Search_model->doctorSearchNumRows($filters);
        $result2 = $this->Search_model->bookmarkStatus();

        //$result=array_merge($result1,$result2);
        if (!empty($result)) {
            $this->response(array(
                'status' => true,
                'message' => count($result) . ' data found!',
                'bookmark' => $result2,
                'data' => $result,
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'No data found!',
                'data' => array(),
                'total_rows' => $total_rows,
            ), REST_Controller::HTTP_OK);
        }
    }
}
