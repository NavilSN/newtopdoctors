<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bookmark extends REST_Controller {

	public $data = array();

	function __construct() {
		parent::__construct();
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=utf-8");
		//load model
		$this->load->model('Bookmark_master_model');
		$this->load->model('Worktype_bookmark_master_model');
		//set the default lang
		$this->data['lang'] = 'en_EN';
		//basic config
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        //trim all data
		foreach ($_POST as $key => $val) {
			$_POST[$key] = trim($val);
		}

		foreach ($_GET as $key => $val) {
			$_GET[$key] = trim($val);
		}
	}

	function Add_get(){
		$checkdata=$this->Bookmark_master_model->checkMain($this->get('doctorId'),$this->get('userId'));
		$getWorkName=$this->db->get_where('doctor_master_en',array('doctor_id'=>$this->get('doctorId')))->row();

		//echo count($checkdata);die();
		if(count($checkdata)>0){
			if($checkdata->status=='0'){
				$this->Bookmark_master_model->update($checkdata->bookmark_id, array(
					'user_id'		=> $this->get('userId'),
					'doctor_id'		=> $this->get('doctorId'),
					'status'		=> '1',
				));

				$this->response(array(
					'result'	=> 'Bookmarked',
					'status'	=> TRUE,
					'message'	=> 'Bookmarked',
					'name'		=> $checkdata->first_name.' '.$checkdata->last_name,
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
			}else{
				$this->Bookmark_master_model->update($checkdata->bookmark_id, array(
					'user_id'		=> $this->get('userId'),
					'doctor_id'		=> $this->get('doctorId'),
					'status'		=> '0',
				));

				$this->response(array(
					'result'	=> 'Unbookmarked',
					'status'	=> TRUE,
					'message'	=> 'Unbookmarked',
					'name'		=> $checkdata->first_name.' '.$checkdata->last_name,
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
			}	
			
		}else{
			$this->Bookmark_master_model->insert(array(
				'user_id'		=> $this->get('userId'),
				'doctor_id'		=> $this->get('doctorId'),
				'status'		=> '1',
			));

			$this->response(array(
					'result'	=> 'Bookmark is added',
					'status'	=> TRUE,
					'message'	=> 'Bookmark is added',
					'name'		=> $getWorkName->first_name.' '.$getWorkName->last_name,
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}

	}

	function Add_post(){
		
		$checkdata=$this->Bookmark_master_model->checkMain($this->post('doctorId'),$this->post('userId'));
		$getWorkName=$this->db->get_where('doctor_master_en',array('doctor_id'=>$this->post('doctorId')))->row();
		//echo count($checkdata);die();
		if(count($checkdata)>0){
			if($checkdata->status=='0'){
				$this->Bookmark_master_model->update($checkdata->bookmark_id, array(
					'user_id'		=> $this->post('userId'),
					'doctor_id'		=> $this->post('doctorId'),
					'status'		=> '1',
				));

				$this->response(array(
					'result'	=> 'Bookmarked',
					'status'	=> TRUE,
					'message'	=> 'Bookmarked',
					'name'		=> $checkdata->first_name.' '.$checkdata->last_name,					
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
			}else{
				$this->Bookmark_master_model->update($checkdata->bookmark_id, array(
					'user_id'		=> $this->post('userId'),
					'doctor_id'		=> $this->post('doctorId'),
					'status'		=> '0',
				));
				$this->response(array(
					'result'	=> 'Unbookmarked',
					'status'	=> TRUE,
					'message'	=> 'Unbookmarked',					
					'name'		=> $checkdata->first_name.' '.$checkdata->last_name,
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
			}	

		}else{
		$this->Bookmark_master_model->insert(array(
				'user_id'		=> $this->post('userId'),
				'doctor_id'		=> $this->post('doctorId'),
				'status'		=> '1',
			));

			$this->response(array(
					'result'	=> 'Bookmark is added',
					'status'	=> TRUE,
					'message'	=> 'Bookmark is added',					
					'name'		=> $getWorkName->first_name.' '.$getWorkName->last_name,
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
		
	}

	function bookmarkList_get(){
		
 		$userId=$this->get('userId');

		$data1=$this->Bookmark_master_model->bookmarkList($userId);
		$data2=$this->Worktype_bookmark_master_model->bookmarkList($userId);
		$data=array_merge($data1,$data2);
		if(empty($data) || empty($userId)){
			$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			$this->response(array(
						'status'	=> TRUE,
						'result'	=>$data,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	function bookmarkList_post(){
		
 		$userId=$this->post('userId');

		$data1=$this->Bookmark_master_model->bookmarkList($userId);
		$data2=$this->Worktype_bookmark_master_model->bookmarkList($userId);
		$data=array_merge($data1,$data2);
		
		if(empty($data) || empty($userId)){
			$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			$this->response(array(
						'status'	=> TRUE,
						'result'	=>$data,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

}
