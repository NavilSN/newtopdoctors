<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Review extends REST_Controller {

	/**
	* Constructor
	*
	*  @return void
	*/
	function __construct() {
		parent::__construct();
		$this->load->model('Doctors_rating_model');
		$this->load->model('Work_rating_model');
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        //trim all data
		foreach ($_POST as $key => $val) {
			$_POST[$key] = trim($val);
		}

		foreach ($_GET as $key => $val) {
			$_GET[$key] = trim($val);
		}
	}

	function Add_get(){
		
		$doctorId=$this->get('doctorId');

		$checkDuplicate=$this->Doctors_rating_model->checkDuplicate($this->get('doctorId'),$this->get('userId'));
		if(count($checkDuplicate)>0){
			$this->response(array(
				'status'	=> FALSE,
				'message' => 'You have already rated this Doctor'
			), REST_Controller::HTTP_OK);
		}else{
			$average=$this->get('reputation')+$this->get('clinic')+$this->get('availability')+$this->get('approachability') + $this->get('technology');

			$this->Doctors_rating_model->insert(array(
					'doctor_id'				=> $this->get('doctorId'),
					'reputation'			=> $this->get('reputation'),
					'clinic'				=> $this->get('clinic'),
					'availability'			=> $this->get('availability'),
					'approachability'		=> $this->get('approachability'),
					'technology'			=> $this->get('technology'),
					'average_score'			=> $average/5,
					'comment'				=> $this->get('comment'),
					'user_id'				=> $this->get('userId'),
					'user_ip'				=> $this->get('userIp'),
					'visibility'			=> $this->get('visible'),
					'summary'				=> $this->get('summary'),
                    'reviewIcon'            => $this->get('reviewIcon'),
                    'date_created'			=> date('Y-m-d H:i:s'),
                    'review_from'			=>2
				));

				$this->response(array(
						'status'	=> TRUE,
						'message'	=> 'Data added Successfully',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	function Add_post(){
		$doctorId=$this->post('doctorId');

		$checkDuplicate=$this->Doctors_rating_model->checkDuplicate($this->post('doctorId'),$this->post('userId'));
		if(count($checkDuplicate)>0){
			$this->response(array(
				'status'	=> FALSE,
				'message' => 'You have already rated this Doctor'
			), REST_Controller::HTTP_OK);
		}else{
			$average=$this->post('reputation')+$this->post('clinic')+$this->post('availability')+$this->post('approachability') + $this->post('technology');

			$this->Doctors_rating_model->insert(array(
					'doctor_id'				=> $this->post('doctorId'),
					'reputation'			=> $this->post('reputation'),
					'clinic'				=> $this->post('clinic'),
					'availability'			=> $this->post('availability'),
					'approachability'		=> $this->post('approachability'),
					'technology'			=> $this->post('technology'),
					'average_score'			=> $average/5,
					'comment'				=> $this->post('comment'),
					'user_id'				=> $this->post('userId'),
					'user_ip'				=> $this->post('userIp'),
					'visibility'			=> $this->post('visible'),
					'summary'				=> $this->post('summary'),
                    'reviewIcon'                    =>$this->post('reviewIcon'),
                    'date_created'			=> date('Y-m-d H:i:s'),
                    'review_from'			=>2
				));

				$this->response(array(
						'status'	=> TRUE,
						'message'	=> 'Data added Successfully',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}
	
	function reviewRating_get(){

		$allReviewRate=$this->Doctors_rating_model->reviewRating($this->get('doctorId'));

		if(empty($allReviewRate)){
			$data=array('reputation'=>array('avg'=>0,'count'=>0),'clinic'=>array('avg'=>0,'count'=>0),'availability'=>array('avg'=>0,'count'=>0),'approachability'=>array('avg'=>0,'count'=>0),
			'technology'=>array('avg'=>0,'count'=>0)
			);
			$timeAge='';
		}else{
		$data=array('reputation'=>array('avg'=>$allReviewRate->Reputation,'count'=>$allReviewRate->TotalReputation),'clinic'=>array('avg'=>$allReviewRate->clinic,'count'=>$allReviewRate->totalClinic),'availability'=>array('avg'=>$allReviewRate->availability,'count'=>$allReviewRate->totalAvaibility),'approachability'=>array('avg'=>$allReviewRate->approachability,'count'=>$allReviewRate->totalApproachability),
			'technology'=>array('avg'=>$allReviewRate->technology,'count'=>$allReviewRate->totalTechnology)
			);
			$timeAge=$this->timeAgo(strtotime($allReviewRate->dateCreate));
		}
		

		$this->response(array(
					'status'	=> TRUE,
					'data'      =>$data,
					'commentTime'=>$timeAge,
					'message'	=> 'Data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		
		
	}

	function reviewRating_post(){

		$allReviewRate=$this->Doctors_rating_model->reviewRating($this->post('doctorId'));

		if(empty($allReviewRate)){
			$data=array('reputation'=>array('avg'=>0,'count'=>0),'clinic'=>array('avg'=>0,'count'=>0),'availability'=>array('avg'=>0,'count'=>0),'approachability'=>array('avg'=>0,'count'=>0),
			'technology'=>array('avg'=>0,'count'=>0)
			);
			$timeAge='';
		}else{
		$data=array('reputation'=>array('avg'=>$allReviewRate->Reputation,'count'=>$allReviewRate->TotalReputation),'clinic'=>array('avg'=>$allReviewRate->clinic,'count'=>$allReviewRate->totalClinic),'availability'=>array('avg'=>$allReviewRate->availability,'count'=>$allReviewRate->totalAvaibility),'approachability'=>array('avg'=>$allReviewRate->approachability,'count'=>$allReviewRate->totalApproachability),
			'technology'=>array('avg'=>$allReviewRate->technology,'count'=>$allReviewRate->totalTechnology)
			);
			$timeAge=$this->timeAgo(strtotime($allReviewRate->dateCreate));
		}
		

		$this->response(array(
					'status'	=> TRUE,
					'data'      =>$data,
					'commentTime'=>$timeAge,
					'message'	=> 'Data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		
	}

	function ratingUserDetail_get(){
		$doctorId=$this->get('doctorId');

		$data=$this->Doctors_rating_model->userDetail($doctorId);
		//print_r($data);
		if(empty($data) || empty($doctorId)){
			$this->response(array(
								'status'	=> FALSE,
								'message'	=> 'No review found',
							), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			
			foreach ($data as $dataRow) {
                          
			$dataArray[]=array('userId'=>$dataRow->user_id,
							'photo'=>$dataRow->Photo,
							'name'=>$dataRow->first_name.' '.$dataRow->last_name,
							'address'=>$dataRow->address,
							'dateTime'=>$dataRow->date_created,
							'avgRating'=>$dataRow->average_score,
							'comment'=>$dataRow->comment,
							'review'=>'',
                                                         'review_id'=>$dataRow->rating_id,
							'commentTime'=>$this->timeAgo(strtotime($dataRow->dateCreate)),
							'visible'=>$dataRow->visible,
                                                        'reviewIcon'=>$dataRow->reviewIcon
							);
			}	
			$this->response(array(
						'status'	=> TRUE,
						'data'      =>$dataArray,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	
	}

	function ratingUserDetail_post(){
		$doctorId=$this->post('doctorId');

		$data=$this->Doctors_rating_model->userDetail($doctorId);
		//print_r($data);
		if(empty($data) || empty($doctorId)){
			$this->response(array(
								'status'	=> FALSE,
								'message'	=> 'No review found',
							), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			foreach ($data as $dataRow) {
				
			$dataArray[]=array('userId'=>$dataRow->user_id,
							'photo'=>$dataRow->Photo,
							'name'=>$dataRow->first_name.' '.$dataRow->last_name,
							'dateTime'=>$dataRow->date_created,
							'avgRating'=>$dataRow->average_score,
							'comment'=>$dataRow->comment,
							'review'=>'',
                                                        'review_id'=>$dataRow->rating_id,
							'commentTime'=>$this->timeAgo(strtotime($dataRow->dateCreate)),
							'visible'=>$dataRow->visible,
                                                        'reviewIcon'=>$dataRow->reviewIcon
							);
			}	
			$this->response(array(
						'status'	=> TRUE,
						'data'      =>$dataArray,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	
	}
		
	function topTenDoctor_get(){
		$lang=$this->get('lang');	
 		$data=$this->Doctors_rating_model->topTenDoctor($lang);
		//print_r($data); die();
		if(empty($data)){
			$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			$this->response(array(
						'status'	=> TRUE,
						'result'	=>$data,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	function topTenDoctor_post(){
		$lang=$this->post('lang');	
 		$data=$this->Doctors_rating_model->topTenDoctor($lang);
		//print_r($data); die();
		if(empty($data)){
			$this->response(array(
					'status'	=> FALSE,
					'message'	=> 'No data found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}else{
			$this->response(array(
						'status'	=> TRUE,
						'result'	=>$data,
						'message'	=> 'Data found',
					), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	function GetReview_get(){

		$userId=$this->get('userId');
		$doctorId=$this->get('doctorId');
		$GetReview=$this->Doctors_rating_model->GetReview($userId,$doctorId);

		if(count($GetReview)>0){

			$this->response(array(
				'status'	=> TRUE,
				'result'	=> $GetReview,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}else{

			$this->response(array(
				'status'	=> FALSE,
				'result'	=> array(),
				'message'	=> 'Data not found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}


	}

	function GetReview_post(){

		$userId=$this->post('userId');
		$doctorId=$this->post('doctorId');
		$GetReview=$this->Doctors_rating_model->GetReview($userId,$doctorId);

		if(count($GetReview)>0){

			$this->response(array(
				'status'	=> TRUE,
				'result'	=> $GetReview,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}else{

			$this->response(array(
				'status'	=> FALSE,
				'result'	=> array(),
				'message'	=> 'Data not found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}
	}

	function UserByDoctorReviews_get(){
		$userId=$this->get('userId');
		$lang=$this->get('lang');
		
		$UserByDoctorReviews=$this->Doctors_rating_model->UserByDoctorReviews($userId,$lang);
		$UserByWorkReviews=$this->Work_rating_model->UserByWorkReviews($userId,$lang);
		$checkUserAvailable=$this->db->get_where('user_master',array('user_id'=>$userId))->row();
		$data1	= array();
		$data2	= array();
		if(count($checkUserAvailable)>0){
			//echo count($UserByWorkReviews);
			//print_r($UserByWorkReviews);
			if(count($UserByDoctorReviews)>0 || count($UserByWorkReviews)>0) {
				foreach ($UserByDoctorReviews as $key => $value) {
					$data1[]=array('doctorId'=>$value->Id,
								'doctorName'=>$value->Name,
								'avgReview'=>$value->avgReview,
								'comment'=>$value->comment,
								'commentTime'=>$this->timeAgo(strtotime($value->dateCreate)),
								'workType'=>'Doctor',
                                'reviewIcon'=>$value->reviewIcon
								);
                }
                foreach ($UserByWorkReviews as $key1 => $value1) {
					$data2[]=array('doctorId'=>$value1->Id,
								'doctorName'=>$value1->Name,
								'avgReview'=>$value1->avgReview,
								'comment'=>$value1->comment,
								'commentTime'=>$this->timeAgo(strtotime($value1->dateCreate)),
								'workType'=>$value1->work_type,
                                'reviewIcon'=>$value->reviewIcon
								);
                }
                
            	$data=array_merge($data2,$data1);
            $this->response(array(
				'status'	=> TRUE,
				'result'	=> $data,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

			}else{

				$this->response(array(
					'status'	=> FALSE,
					'result'	=> array(),
					'message'	=> 'Data not found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code

			}
		}else{

			$this->response(array(
					'status'	=> FALSE,
					'result'	=> array(),
					'message'	=> 'User not found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}

	}

	function UserByDoctorReviews_post(){
		$userId=$this->post('userId');
		$lang=$this->post('lang');

		$UserByDoctorReviews=$this->Doctors_rating_model->UserByDoctorReviews($userId,$lang);
		$UserByWorkReviews=$this->Work_rating_model->UserByWorkReviews($userId,$lang);
		$checkUserAvailable=$this->db->get_where('user_master',array('user_id'=>$userId))->row();
		$data1	= array();
		$data2	= array();
		if(count($checkUserAvailable)>0){
			//echo count($UserByWorkReviews);
			//print_r($UserByWorkReviews);
			if(count($UserByDoctorReviews)>0 || count($UserByWorkReviews)>0) {
				foreach ($UserByDoctorReviews as $key => $value) {
					$data1[]=array('doctorId'=>$value->Id,
								'doctorName'=>$value->Name,
								'avgReview'=>$value->avgReview,
								'comment'=>$value->comment,
								'commentTime'=>$this->timeAgo(strtotime($value->dateCreate)),
								'workType'=>'Doctor',
                                'reviewIcon'=>$value->reviewIcon
								);
                }
                foreach ($UserByWorkReviews as $key1 => $value1) {
					$data2[]=array('doctorId'=>$value1->Id,
								'doctorName'=>$value1->Name,
								'avgReview'=>$value1->avgReview,
								'comment'=>$value1->comment,
								'commentTime'=>$this->timeAgo(strtotime($value1->dateCreate)),
								'workType'=>$value1->work_type,
                                'reviewIcon'=>$value->reviewIcon
								);
                }
                
            	$data=array_merge($data2,$data1);
            $this->response(array(
				'status'	=> TRUE,
				'result'	=> $data,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

			}else{

				$this->response(array(
					'status'	=> FALSE,
					'result'	=> array(),
					'message'	=> 'Data not found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code

			}
		}else{

			$this->response(array(
					'status'	=> FALSE,
					'result'	=> array(),
					'message'	=> 'User not found',
				), REST_Controller::HTTP_OK); // OK (200) Reponse code
		}

	}

	function timeAgo($time_ago){
	  $cur_time   = time();
	  $time_elapsed   = $cur_time - $time_ago;
	  $seconds  = $time_elapsed ;
	  $minutes  = round($time_elapsed / 60 );
	  $hours    = round($time_elapsed / 3600);
	  $days     = round($time_elapsed / 86400 );
	  $weeks    = round($time_elapsed / 604800);
	  $months   = round($time_elapsed / 2600640 );
	  $years    = round($time_elapsed / 31207680 );
	  if($seconds <= 60){$time= "$seconds seconds ago";}
	  else if($minutes <=60){
	    if($minutes==1){$time= "1 minute ago";}else{$time= "$minutes minutes ago";}
	  }else if($hours <=24){
	    if($hours==1){$time= "an hour ago";}else{$time= "$hours hours ago";}
	  }else if($days <= 7){
	    if($days==1){$time= "$days day ago";}else{$time= "$days days ago";}
	  }else if($weeks <= 4.3){
	    if($weeks==1){$time= "1 week ago";}else{$time= "$weeks weeks ago";}
	  }else if($months <=12){
	    if($months==1){$time= "1 month ago";}else{$time= "$months months ago";}
	  }else{
	    if($years==1){$time= "1 year ago";}else{$time= "$years years ago";}
	  }

	  return $time;
	}
        
         /**
         * Rating details
         */
        function doctorReviewDetails_post()
        {
            $reviewId = $this->post('reviewId');
            $lang = $this->post('lang');
            $UserByWorkReviews=$this->Doctors_rating_model->getRatingById($reviewId,$lang);
            //echo $this->db->last_query();
            
            if(count($UserByWorkReviews)>0){

			$this->response(array(
				'status'	=> TRUE,
				'result'	=> $UserByWorkReviews,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}else{

			$this->response(array(
				'status'	=> FALSE,
				'result'	=> array(),
				'message'	=> 'Data not found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}
        }
        /**
         * Rating details
         */
        function doctorReviewDetails_get()
        {
            $reviewId = $this->get('reviewId');
            $lang = $this->get('lang');
            $UserByWorkReviews=$this->Doctors_rating_model->getRatingById($reviewId,$lang);
            //echo $this->db->last_query();
            
            if(count($UserByWorkReviews)>0){

			$this->response(array(
				'status'	=> TRUE,
				'result'	=> $UserByWorkReviews,
				'message'	=> 'Data found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}else{

			$this->response(array(
				'status'	=> FALSE,
				'result'	=> array(),
				'message'	=> 'Data not found',
			), REST_Controller::HTTP_OK); // OK (200) Reponse code

		}
        }

}
