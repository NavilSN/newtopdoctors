<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LanguageSwitcher extends MY_Controller
{
    public function __construct() {
        parent::__construct();     
    }
 
    function switchLang($language = "") {
        
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
        
       $a = $_SERVER['HTTP_REFERER'];

		if (strpos($a, 'typeofsearch') !== false) {
       		redirect(base_url());	
       	}
       	else{
       	redirect($_SERVER['HTTP_REFERER']);	
       	}
        
    }
}
