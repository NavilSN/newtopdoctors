<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bookmark_Result_model extends MY_Model {

    
    function result_count()
    {
        $siteLang=$this->session->userdata('site_lang');
        $userId=$this->session->userdata('user_id');
        $bookmarkResult1=array();
        $bookmarkResult2=array();
        $data['title']='TopDoctors';
       
        if($siteLang=="arabic")
        {
            $select = "CONCAT(doctor_master.first_name, ' ', doctor_master.last_name) AS Name,doctor_master.address as doctorAddress";
            $workselect = "`work_master`.`name` as `Name`, `work_master`.`address` as `doctorAddress`";
        }
        else{
            $select = "CONCAT(doctor_master_en.first_name, ' ', doctor_master_en.last_name) AS Name, `doctor_master_en`.`address` as `doctorAddress`";
            $workselect = "`work_master`.`name_en` as `Name`, `work_master`.`address_en` as `doctorAddress`";
        }

        return $this->db->query("(SELECT `doctor_details`.`photo`, `doctor_details`.`phone_number` AS `phone`, `doctor_details`.`doctor_id` as ID, 'doctor' AS work_type, " . $select . ", IFNULL(AVG(doctors_rating.average_score),0) as averageReview FROM `bookmark_master` LEFT JOIN `doctor_details` ON `doctor_details`.`doctor_id`=`bookmark_master`.`doctor_id` LEFT JOIN `doctor_master_en` ON `doctor_master_en`.`doctor_id`=`doctor_details`.`doctor_id` LEFT JOIN `doctor_master` ON `doctor_master`.`doctor_id`=`doctor_details`.`doctor_id` LEFT JOIN `doctors_rating` ON `doctors_rating`.`doctor_id`=`doctor_details`.`doctor_id` WHERE `bookmark_master`.`user_id` = '" . $userId . "' AND `bookmark_master`.`status` = '1' GROUP BY `doctor_details`.`doctor_id`) UNION (SELECT `work_master`.`photo`, `work_master`.`phone` AS `phone`, `work_master`.`work_id` as ID, `work_master`.`work_type`, " . $workselect . ", IFNULL(AVG(work_rating.average_score),0) as averageReview FROM `worktype_bookmark_master` LEFT JOIN `work_master` ON `work_master`.`work_id`=`worktype_bookmark_master`.`work_id` LEFT JOIN work_rating ON work_rating.work_id=work_master.work_id WHERE `worktype_bookmark_master`.`user_id` = '".$userId."' AND `worktype_bookmark_master`.`status` = '1' GROUP BY work_master.work_id) ORDER BY averageReview DESC")->num_rows();
         
    }
    
   
            
    function fetch_bookmark($perpage,$page)
    {   
        $siteLang=$this->session->userdata('site_lang');
        $userId=$this->session->userdata('user_id');
        $bookmarkResult1=array();
        $bookmarkResult2=array();
        $data['title']='TopDoctors';
      
          if($siteLang=="arabic")
        {
            $select = "CONCAT(doctor_master.first_name, ' ', doctor_master.last_name) AS Name,doctor_master.address as doctorAddress";
            $workselect = "`work_master`.`name` as `Name`, `work_master`.`address` as `doctorAddress`";
        }
        else{
            $select = "CONCAT(doctor_master_en.first_name, ' ', doctor_master_en.last_name) AS Name, `doctor_master_en`.`address` as `doctorAddress`";
            $workselect = "`work_master`.`name_en` as `Name`, `work_master`.`address_en` as `doctorAddress`";
        }

         return $this->db->query("(SELECT `doctor_details`.`photo`, `doctor_details`.`phone_number` AS `phone`, `doctor_details`.`doctor_id` as ID, 'doctor' AS work_type, " . $select . ", IFNULL(AVG(doctors_rating.average_score),0) as averageReview FROM `bookmark_master` LEFT JOIN `doctor_details` ON `doctor_details`.`doctor_id`=`bookmark_master`.`doctor_id` LEFT JOIN `doctor_master_en` ON `doctor_master_en`.`doctor_id`=`doctor_details`.`doctor_id` LEFT JOIN `doctor_master` ON `doctor_master`.`doctor_id`=`doctor_details`.`doctor_id` LEFT JOIN `doctors_rating` ON `doctors_rating`.`doctor_id`=`doctor_details`.`doctor_id` WHERE `bookmark_master`.`user_id` = '" . $userId . "' AND `bookmark_master`.`status` = '1' GROUP BY `doctor_details`.`doctor_id`) UNION (SELECT `work_master`.`photo`, `work_master`.`phone` AS `phone`, `work_master`.`work_id` as ID, `work_master`.`work_type`, " . $workselect . ", IFNULL(AVG(work_rating.average_score),0) as averageReview FROM `worktype_bookmark_master` LEFT JOIN `work_master` ON `work_master`.`work_id`=`worktype_bookmark_master`.`work_id` LEFT JOIN work_rating ON work_rating.work_id=work_master.work_id WHERE `worktype_bookmark_master`.`user_id` = '".$userId."' AND `worktype_bookmark_master`.`status` = '1' GROUP BY work_master.work_id)  ORDER BY averageReview DESC LIMIT $page,$perpage")->result();


        
    }
}
