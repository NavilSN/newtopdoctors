<?php 
$siteLang=$this->session->userdata('site_lang');
$this->load->model('home/Location_master_model');
$this->load->model('home/Specialty_master_model');
?>
<div class="search-full-box">
  <section class="text-center clearfix">
  <!-- <img src="images/doctors-banner.jpg" class="img-responsive"> -->
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-12 text-left">
               <!-- <form class="form-inline" action="<?php echo base_url();?>search" method="post">
                  <div class="form-group">
                    <span class="flip search-icon"> <i class="fa fa-search"></i></span>
                    <input type="text" name="searchSpecialityValue" class="form-control" id="" 
                    placeholder="<?php echo $this->lang->line('searchPlaceHolder'); ?>" value="">
                  </div>
                  <div class="form-control-btn-box">
                    <button type="submit" name="specialitySearch" class="btn btn-default search-btn"><?php echo $this->lang->line('DPSearchBTN'); ?></button>                   
                  </div>
                </form>-->
                <h2><?php echo $this->lang->line('BookMarkList'); ?></h2>
            </div>
          </div>          
      </div>
  </section>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/star-rating-svg.css">
<div class="filters-box m50t">
  <section class="text-center clearfix">
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-9 text-left">
            <div class="border-divider fullwidth"></div>
             <div class="filter-show-data clearfix">
                  <form role="form" class="form-horizontal">   
                    <div class="form-group">
                      <?php if(empty($bookmarkResult)) { ?>
                       <!--<div><h1 style="color:#dedede;"><?php //echo $this->lang->line('searchNoRecords'); ?><img src="<?php echo base_url(); ?>images/no-record-found-new.jpg"></h1></div> -->
                          <div><h1 style="color:#dedede;"><?php echo $this->lang->line('searchNoRecords'); ?></h1></div> 
                      <?php } else{
                      $i=1;
                      $a=1;
                      foreach ($bookmarkResult as $locationArrayRow) {
                        if($locationArrayRow->ID!="")
                        {
                        if($locationArrayRow->work_type=="doctor")
                        {
                      $this->db->select('AVG(reputation + clinic + availability + approachability + technology)/5 as average_score');
                      $this->db->where('dstatus','1');
                      $averageScoreRow=$this->db->get_where('doctors_rating',array('doctor_id'=>$locationArrayRow->ID))->row();
                    }
                     if($locationArrayRow->work_type=="Hospital" || $locationArrayRow->work_type=="Clinic"  || $locationArrayRow->work_type=="Radiology Lab"  || $locationArrayRow->work_type=="Medical Lab" )
                     {
                      $this->db->select('AVG(reputation + clinic + availability + approachability + technology)/5 as average_score');
                      $this->db->where('wstatus','1');
                      $averageScoreRowwork=$this->db->get_where('work_rating',array('work_id'=>$locationArrayRow->ID))->row();
                    }
                      ?>
                      <div class="col-sm-6 pull-left col-xs-12">
                        <?php 
                        if($locationArrayRow->work_type=="doctor")
                        {
                        $bookmarkedRow2= $this->db->get_where('bookmark_master',array('doctor_id'=>$locationArrayRow->ID,'user_id'=>$this->session->userdata("user_id")))->row();
                      }
                      if($locationArrayRow->work_type=="Hospital" || $locationArrayRow->work_type=="Clinic"  || $locationArrayRow->work_type=="Radiology Lab"  || $locationArrayRow->work_type=="Medical Lab" )
                     {
                        $bookmarkedRowWork2= $this->db->get_where('worktype_bookmark_master',array('work_id'=>$locationArrayRow->ID,'user_id'=>$this->session->userdata("user_id")))->row();
                      }
                         ?>
                         <div class="doctor-details <?php if($bookmarkedRow2->status==1){echo'bookmarks';} ?>">
                            <div class="top-box">
                              <?php if($locationArrayRow->work_type=='Hospital'){?>
                               <a href="<?php echo base_url(); ?>workProfile/index/<?php echo $locationArrayRow->work_id; ?>">
                                <img src="<?php echo base_url(); ?>images/Hospitals.png" alt="" height="150" width="150">
                              </a>
                              <?php }else if($locationArrayRow->work_type=='Clinic'){ ?>
                              <a href="<?php echo base_url(); ?>workProfile/index/<?php echo $locationArrayRow->work_id; ?>">
                                <img src="<?php echo base_url(); ?>images/Clinics.png" alt="" height="150" width="150">
                              </a>  
                              <?php }else if($locationArrayRow->work_type=='Radiology Lab' || $locationArrayRow->work_type=="Medical Lab"){ ?>
                               <a href="<?php echo base_url(); ?>workProfile/index/<?php echo $locationArrayRow->work_id; ?>">
                                <img src="<?php echo base_url(); ?>images/Labs.png" alt="" height="150" width="150">
                              </a>
                              <?php } if($locationArrayRow->work_type=='doctor'){ ?>
                              <a href="<?php echo base_url(); ?>doctorProfile/index/<?php echo $locationArrayRow->doctor_id; ?>">
                                <img src="<?php echo base_url(); ?>images/Doctors.png" alt="" height="150" width="150">
                              </a>
                              <?php } ?>
                                <div class="bookmarks-select">
                                  <img src="<?php echo base_url(); ?>images/bookmarks-left.png" alt="">
                                </div>
                                
                            </div>
                            <div class="bottom-box">
                                <div class="bottom-box-det">
                              <h2 class="flip text-left clearfix"><?php
                              if (strlen($locationArrayRow->Name) > 20) {
                                $locationArrayRow->Name = substr($locationArrayRow->Name, 0, 20);
                                echo substr($locationArrayRow->Name, 0, strrpos($locationArrayRow->Name, ' '));
                              }else{
                                echo $locationArrayRow->Name;
                              }
                              ?></h2>
                              <div class="border-divider"></div> 
                               <div class="review-det">
                                    <?php 
                                    $avgscore = '0';
                                    if($averageScoreRowwork->average_score!="" && $averageScoreRowwork->average_score!="0") {
                                        $avgscore = $averageScoreRowwork->average_score;
                                    }
                                    if($averageScoreRow->average_score!="" && $averageScoreRow->average_score!="0"){
                                        $avgscore = $averageScoreRow->average_score;
                                    }?>
                                   <input type="hidden" class="ratevalFilter<?php echo $i ?>" value="<?php if(empty($avgscore)){echo '0';}else{echo floatval($avgscore);} ?>" id="frateViewVal<?php echo $i; ?>"> 
                                      <div class="my-rating-<?php echo $i; ?>"></div>
                                    <script>
$(function() {


  $(".my-rating-<?php echo $i; ?>").starRating({
    totalStars: 5,
    emptyColor: 'lightgray',    
    activeColor: '#03878A',
    initialRating: $("#frateViewVal<?php echo $i; ?>").val(),
    strokeWidth: 0,
    useGradient: false,    
    starSize:'20',
      readOnly: true,
    callback: function(currentRating, $el){
    //  alert('rated ' +  currentRating);
      //console.log('DOM Element ', $el);
    }
  });


});
</script>
                                    <div class="flip text-right">
                                    <span class="title">
                                    <?php                                      
                                    
                                        $this->db->select('count(*) as doctorReview'); 
                                        $this->db->where('dstatus','1');
                                        $test=$this->db->get_where('doctors_rating',array('doctor_id'=>$locationArrayRow->doctor_id))->row();
                                        $this->db->select('count(*) as doctorReview'); 
                                        $this->db->where('wstatus','1');
                                        $test2=$this->db->get_where('work_rating',array('work_id'=>$locationArrayRow->work_id))->row();
                                        //echo $this->db->last_query();
                                        if($test->doctorReview > 0 || $test2->doctorReview > 0)
                                        {
                                            if($test->doctorReview > 0)
                                            {
                                            echo $test->doctorReview; 
                                            }elseif($test2->doctorReview > 0){
                                              echo  $test2->doctorReview;
                                            }
                                          
                                            
                                        }
                                        else{
                                            if($test->doctorReview =="0" && $test2->doctorReview=="0")
                                            {
                                                echo "0";
                                            }
                                        }
                                         echo " ".$this->lang->line('DPReview');
                                      ?>
                                    </span>
                                    </div>
                                 </div> 

                            

                                 <div class="address-det">
                                     <?php if($locationArrayRow->doctorAddress!=""){ ?>
                                    <div class="w100p clearfix">
                                          <div class="first-one">
                                          <img src="<?php echo base_url(); ?>images/location-icon.png" alt="">
                                      </div>
                                      <div class="first-one">
                                          <span class="address">
                                            <?php 
                                             if(strlen($locationArrayRow->doctorAddress) > 40){
                                              $locationArrayRow->doctorAddress = substr($locationArrayRow->doctorAddress, 0, 40);
                                               echo substr($locationArrayRow->doctorAddress, 0, strrpos($locationArrayRow->doctorAddress, ' '));
                                              }else{
                                                echo $locationArrayRow->doctorAddress;
                                              }
                                            ?>
                                          </span>
                                      </div>  
                                    </div>
                                     <?php } ?>
                                     <?php if($locationArrayRow->phone!=""){ ?>
                                    <div class="w100p clearfix">                                    
                                      <div class="second-one">
                                          <img src="<?php echo base_url(); ?>images/mobile-icon-s.png" alt="">
                                      </div>
                                      <div class=" second-one">
                                          <span class="phone">
                                            <?php echo $locationArrayRow->phone;?> 
                                          </span>
                                      </div>
                                    </div>
                                     <?php } ?>
                                    <div class="w100p clearfix">                                    
                                      <div class="third-one">
                                        <!-- <img src="images/teeth-icon.png" alt=""> -->
                                      </div>                                    
                                    </div> 

                                    
                                 </div>  
                              
                               </div>
                               <div class="w100p clearfix btn-box"> 
                                        <div class="col-sm-12 pull-left col-xs-12"> 
                                          <div class="flip text-right">   
                                              <input type="hidden" name="userId" class="userId<?php echo $a; ?>" value="<?php echo $this->session->userdata("user_id"); ?>">
                                              <input type="hidden" name="doctorId" class="doctorId<?php echo $a; ?>" value="<?php echo $locationArrayRow->ID; ?>">
                                              <input type="hidden" name="workId" class="workId<?php echo $a; ?>" value="<?php echo $locationArrayRow->ID; ?>">

                                              <?php if($this->session->userdata('user_login')==1) {  
                                                if($locationArrayRow->work_type=="Hospital" || $locationArrayRow->work_type=="Clinic"  || $locationArrayRow->work_type=="Radiology Lab"  || $locationArrayRow->work_type=="Medical Lab" )
                     { ?>
                                                <button type="button" class="bookmarkwork_type<?php echo $a; ?> <?php if($bookmarkedRowWork2->status==0){ echo 'bookmarks-plus-icon';}else{echo 'bookmarks-gray-plus-icon';}?>" name="bookmarkwork_type" value="<?php echo $bookmarkedRowWork2->worktype_bookmark_id;  ?>"></button>
                                              <?php } else{ ?>
                                              <button type="button" class="bookmark<?php echo $a; ?> <?php if($bookmarkedRow2->status==0){ echo 'bookmarks-plus-icon';}else{echo 'bookmarks-gray-plus-icon';} ?>" name="bookmark" value="<?php echo $bookmarkedRow2->bookmark_id;  ?>"></button>
                                             
                                              <?php }  } ?>
                                              <?php  if($locationArrayRow->work_type=="Hospital" || $locationArrayRow->work_type=="Clinic"  || $locationArrayRow->work_type=="Radiology Lab"  || $locationArrayRow->work_type=="Medical Lab" )
                     {  ?>
                                                <a href="<?php echo base_url(); ?>workProfile/index/<?php echo 
                                              $locationArrayRow->ID; ?>" class="btn btn-default search-btn"><?php echo $this->lang->line('SearchSeeMore'); ?></a>
                                              <?php }else{?>
                                                <a href="<?php echo base_url(); ?>doctorProfile/index/<?php echo 
                                              $locationArrayRow->ID; ?>" class="btn btn-default search-btn"><?php echo $this->lang->line('SearchSeeMore'); ?></a>
                                              <?php } ?>
                                            </div> 
                                        </div>
                                    </div>
                            </div>
                         </div>
                      </div>
                       <script>
                        $(".bookmarkwork_type<?php echo $a; ?>").click(function() {
                          $.ajax({
                            type: 'POST', 
                            url: "<?php echo base_url();?>search/bookmarkCreatework_type",
                            data: { userId: $('.userId<?php echo $a; ?>').val(),workId:$('.workId<?php echo $a; ?>').val()},
                            success: function(response){
                              location.reload();
                            }
                          });
                        });
                      </script>

                      <script>
                        $(".bookmark<?php echo $a; ?>").click(function() {
                          $.ajax({
                            type: 'POST', 
                            url: "<?php echo base_url();?>search/bookmarkCreate",
                            data: { userId: $('.userId<?php echo $a; ?>').val(),doctorId:$('.doctorId<?php echo $a; ?>').val()},
                            success: function(response){
                              location.reload();
                            }
                          });
                        });
                      </script>
                      <?php $i++; $a++; } }} ?>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                          <ul class = "pagination"><?php echo $links; ?></ul>
                      </div>
                  </form>
             </div>
            </div>
            <div class="clearfix"></div>
            </div>
          </div>          
      </div>
  </section>
</div>

<script src="<?php echo base_url(); ?>/js/jquery.star-rating-svg.js"></script>