<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bookmark extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('home/User_master_model');
        $this->load->model('home/Bookmark_master_model');
        $this->load->model('Bookmark_Result_model');
        $this->load->library('pagination');
        $this->load->helper("url");
	$this->data['slug_class'] = "bookmark";
    }

    function index() {
        $siteLang=$this->session->userdata('site_lang');
        $total_rows = $this->Bookmark_Result_model->result_count();
        $config = array();
        $config["base_url"] = base_url() . "bookmark/index";
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["bookmarkResult"] = $this->Bookmark_Result_model->fetch_bookmark($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $this->__templateFront('bookmark/index', $data);
    }

    /*function bookmark()
    {
        $total_rows = $this->Bookmark_Result_model->result_count();
        $config = array();
        $config["base_url"] = base_url() . "bookmark/index";
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["bookmarkResult"] = $this->Bookmark_Result_model->fetch_bookmark($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $this->__templateFront('bookmark/index', $data);
    }*/

   
}
