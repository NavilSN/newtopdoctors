<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Africa/Cairo');
        $this->load->model('home/User_master_model');
        $this->load->model('home/Doctor_details_model');
        $this->load->model('home/Doctor_master_en_model');
        $this->load->model('home/Doctor_master_model');
        $this->load->model('home/Work_master_model');
        $this->data['active'] = "home";
        $this->data['slug_class'] = "home";
        $this->load->library('email');

    }

    public function index()
    {

        $this->data['title'] = 'TopDoctors';
        $this->__templateFront('home/index', $this->data);
    }

    public function login_process()
    {

        $email = $_POST['loginemail'];
        $password = $_POST['loginpassword'];

        $get_row = $this->db->get_where('user_master', array('email' => $email, 'password' => hash('md5', $password . config_item('encryption_key')), 'status' => '1'));
        if ($get_row->num_rows() > 0) {
            $row = $get_row->row();
            $this->session->set_userdata("user_login", "1");
            $this->session->set_userdata("user_id", $row->user_id);
            $this->session->set_userdata("user_firstname", $row->first_name);
            $this->session->set_userdata("user_lastname", $row->last_name);
            //redirect($_SERVER['HTTP_REFERER']);
        } else {
            if ($this->session->userdata('site_lang') == 'arabic') {
                $errorLogin = $this->lang->line('errorLogin');
            } else {
                $errorLogin = $this->lang->line('errorLogin');
            }
            $this->session->set_flashdata('errorLogin', $errorLogin);
            //redirect(base_url());
            //redirect($_SERVER['HTTP_REFERER']);
        }
        if (isset($_POST['redirect']) && $_POST['redirect'] != "") {
            redirect($_POST['redirect']);
        } else {
            redirect(base_url());
        }
    }

    public function signup_process()
    {
        if ($_POST) {
            $this->User_master_model->insert(array(
                'first_name' => $_POST['firstname'],
                'last_name' => $_POST['lastname'],
                'email' => $_POST['email'],
                'mobile_number' => $_POST['mobile_no'],
                'password' => hash('md5', $_POST['c_password'] . config_item('encryption_key')),
                'country_id' => $_POST['country_id'],
                'address' => $_POST['address'],
                'registration_date' => date('Y-m-d'),
                'register_from' => 1,
            ));

            $insert_id = $this->db->insert_id();
            if (isset($_POST['c1'])) {
                $this->User_master_model->update($insert_id, array('newslatter' => $_POST['c1']));
            }
            $this->session->set_userdata("user_login", "1");
            $this->session->set_userdata("user_id", $insert_id);
            $this->session->set_userdata("user_firstname", $_POST['firstname']);
            $this->session->set_userdata("user_lastname", $_POST['lastname']);

            if ($insert_id) {
                $this->email->set_newline("\r\n");
                $this->email->from('topdoctors123@gmail.com', 'Top doctor');
                $this->email->to($_POST['email']);
                $this->email->subject('Top doctor registration success');
                $data['firstname'] = $_POST['firstname'];
                $data['email'] = $_POST['email'];
                $data['password'] = $_POST['password'];
                $msg = $this->load->view('template/email_template', $data, true);
                $this->email->message($msg);
                $this->email->send();

                /*Email Send for Newsletter*/
                if ($_POST['c1'] == '1') {
                    $this->email->set_newline("\r\n");
                    $this->email->from('topdoctors123@gmail.com', 'Top doctor');
                    $this->email->to($_POST['email']);
                    $this->email->subject('Newsletter subscribe for top doctor');
                    $dataln['firstname'] = $_POST['firstname'];
                    $msgnew = $this->load->view('template/newslatter_template', $dataln, true);
                    $this->email->message($msgnew);
                    $this->email->send();
                }
            }

            if ($this->session->userdata('site_lang') == 'arabic') {
                $registerSuccess = $this->lang->line('registerSuccess');
            } else {
                $registerSuccess = $this->lang->line('registerSuccess');
            }
            $this->flash_notification($registerSuccess);
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url('home'));
    }

    public function fblogin()
    {
        $base_url = $this->config->item('base_url'); //Read the baseurl from the config.php file
        //get the Facebook appId and app secret from facebook.php which located in config directory for the creating the object for Facebook class
        $facebook = new Facebook(array(
            'appId' => $this->config->item('appID'),
            'secret' => $this->config->item('appSecret'),
        ));

        $user = $facebook->getUser(); // Get the facebook user id
        //echo "<pre>";
        // print_r($user);
        // die;
        error_reporting(-1);
        if ($user) {

            try {
                $user_profile = $facebook->api('/me?fields=id,first_name,last_name,email'); //Get the facebook user profile data

                $user_profile['email'];
                $this->db->where('email', $user_profile['email']);
                $user_info = $this->db->get('user_master')->row();
                if (count($user_info) > 0) {
                    $first_name = $user_info->first_name;
                    $user_id = $user_info->user_id;
                    $last_name = $user_info->last_name;

                } else {

                    $this->db->insert('user_master', array("first_name" => $user_profile['first_name'], "last_name" => $user_profile['last_name'], "email" => $user_profile['email'], 'is_social' => '1', 'register_from' => 1, 'registration_date' => date('Y-m-d'), 'fbtoken' => $user_profile['id']));

                    $user_id = $this->db->insert_id();
                    $first_name = $user_profile['first_name'];
                    $last_name = $user_profile['last_name'];

                }

                $this->session->set_userdata("user_login", "1");
                $this->session->set_userdata("fb_login", "1");
                $this->session->set_userdata("user_id", $user_id);
                $this->session->set_userdata("user_firstname", $first_name);
                $this->session->set_userdata("user_lastname", $last_name);
                redirect(base_url('home'));
                $params = array('next' => $base_url . 'fbci/logout');

                $ses_user = array('User' => $user_profile,
                    'logout' => $facebook->getLogoutUrl($params), //generating the logout url for facebook
                );
                $this->session->set_userdata($ses_user);
                header('Location: ' . $base_url);
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = null;
            }
            redirect($_SERVER['HTTP_REFERER']);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function check_user_email($param = '')
    {

        $email = $_POST['email'];
        if ($param == '') {
            $data = $this->User_master_model->get_by(array(
                'email' => $email,
            ));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    public function forgot_password()
    {
        $email = $_POST['forgotemail'];
        if ($_POST) {
            $record = $this->db->get_where('user_master', array('email' => $email))->row();

            if ($record) {
                $user_id = $record->user_id;
                $random_string = $this->random_string_generate();
                //update token
                $this->db->update('user_master', array(
                    'token' => $random_string,
                ), array('email' => $record->email));
                //generate url
                $url = base_url() . 'home/resetPassword/' . $random_string;

                /*$this->update_forgot_password_key('', $user_id, hash('md5',$random_string.config_item('encryption_key')));*/
                $this->email->set_newline("\r\n");
                $this->email->from('topdoctors123@gmail.com', 'Top doctor');
                $this->email->to($record->email);
                $this->email->subject('Reset your top doctor password');
                $dataforgot['password'] = $random_string;
                $dataforgot['reset_link'] = $url;
                $msgforgotpass = $this->load->view('template/fotgotpass_template', $dataforgot, true);

                //echo $msgforgotpass;exit;

                $this->email->message($msgforgotpass);
                $this->email->send();

                if ($this->session->userdata('site_lang') == 'arabic') {
                    $registerSuccess = $this->lang->line('forgotpasswordsuccess');
                } else {
                    $registerSuccess = $this->lang->line('forgotpasswordsuccess');
                }
                $this->flash_notification($registerSuccess);

                redirect(base_url('home'));
            } else {
                if ($this->session->userdata('site_lang') == 'arabic') {
                    $forgotEmailNotFound = $this->lang->line('forgotEmailNotFound');
                } else {
                    $forgotEmailNotFound = $this->lang->line('forgotEmailNotFound');
                }
                $this->session->set_flashdata('forgotEmailNotFound', $forgotEmailNotFound);
                redirect(base_url('home'));
            }
        } else {
            redirect(base_url('home'));
        }
    }

    public function resetPassword($token)
    {
        if ($_POST) {
            if ($token == $_POST['_token']) {
                //update password and reset token and redirect
                if ($_POST['new_password'] == $_POST['confirm_password']) {
                    $this->db->update('user_master',
                        array(
                            'password' => hash('md5', $_POST['new_password'] . config_item('encryption_key')),
                            'token' => '',
                        ),
                        array('token' => $_POST['_token']));
                    $this->flash_notification('Password has been successfully updated.');
                    redirect(base_url());
                } else {
                    $this->session->set_flashdata("error", "Password is mismatched.");
                    redirect(base_url() . 'home/resetPassword/' . $token);
                }
            }
        }

        //check entry for token
        $user = $this->db->select()->from('user_master')
            ->where('token', $token)->get()->result();

        if ($user) {
            $this->data['token'] = $token;
            //$this->__templateFront('home/reset_password', $data);
            $this->data['title'] = 'Reset Password';
            $this->__templateFront('home/reset_password', $this->data, TRUE90);
            //$this->__template('home/reset_password',$this->data);
            //$this->load->view('home/reset_password.php');
            //show reset password form

        } else {
            $this->session->set_flashdata("error", "Link is Expired");
            redirect(base_url());
            //   show_404();
        }
    }

    /**
    Check Duplicate newsletter email
    return boolean
     */
    public function check_newsletter_email($param = '')
    {
        $email = $_POST['email_id'];

        if ($param == '') {
            $data = $this->User_master_model->check_newsletter_email($email);
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    public function unsubscribe($email = '')
    {
        if ($email != '') {
            $this->User_master_model->unsubscribe_user($email);
            $this->flash_notification('You are successfully unsubscribed');
        }
        redirect(base_url());
    }

/**
Add email to newsletter
param email
 */
    public function addtonewsletter()
    {
        $email = $this->input->post('newsletter');
        $this->User_master_model->add_email_to_newsletter($email);
        $this->flash_notification("You are successfully subscribed to newsletter");
        redirect(base_url());

    }

    /*function reset_password() {
    $email = $this->post('email');
    //check for data
    $user = $this->db->select()->from()->where()->get()->result();
    if (count($user)) {
    $token = $this->random_string_generate();
    //update token for user

    //generate url
    $url = base_url() . 'reset-password/' . ;
    }
    }*/

    public function random_string_generate()
    {
        $this->load->helper('string');
        return random_string('alnum', 16);
    }

    public function update_forgot_password_key($user_type, $user_id, $key)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('user_master', array(
            'password' => $key,
        ));
    }

    public function send_news()
    {
        $getSubscribe = $this->db->get_where('user_master', array('newslatter' => 1))->result();
        foreach ($getSubscribe as $getSubscribeRow) {
            $this->email->set_newline("\r\n");
            $this->email->to($getSubscribeRow->email);
            $this->email->from('topdoctors123@gmail.com');
            $this->email->subject('@ Top doctor - Newsletter Alert');
            $this->email->message('Hi Here is the info you requested.');
            $this->email->send();
        }
    }

    public function AddDoctor()
    {
        if ($_POST) {

            $this->load->model('admin/Doctor_details_model');
            if ($_POST['email'] != "") {
                $email_id = $this->Doctor_details_model->get_by(array('email' => $_POST['email']));
            }
            if ($_POST['location'] == "") {
                $location = "0";
            } else {
                $location = $_POST['location'];
            }
            if (count($email_id) > 0) {
                // $this->flash_notification('Email already exists!');
                $this->session->set_flashdata('error', "Email already exists!");
                redirect(base_url('home'));
            }
            /* $this->Doctor_master_model->insert(array('first_name'    => '',));
            $this->Doctor_master_en_model->insert(array(
            'first_name'    => trim($_POST['firstname']),
            'last_name'     => trim($_POST['lastname']),
            'address'       => $_POST['address'],
            ));
            $this->Doctor_details_model->insert(array(
            'email'                 => $_POST['email'],
            'work_id'               => $_POST['work'],
            'mobile_number'         => $_POST['phone'],
            'location_id'           => $location,
            'speciality_id'         => $_POST['speciality'],
            'gender'                => $_POST['gender'],
            'google_map_latitude'   => $_POST['latitude'],
            'google_map_longtude'   => $_POST['longtitude'],
            'visibility'            => '0',
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
            ));*/
            $this->userdoctorSendmail($_POST);
            $this->flash_notification('Your request has been send. Doctor will be shown once the moderator will approve the request');
            redirect(base_url('home'));
        }
    }

    public function latlong()
    {

        $zipcode = $_POST['address'];
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $zipcode . "&sensor=fam_close(fam)";
        $details = file_get_contents($url);
        $result = json_decode($details, true);
        $lat = $result['results'][0]['geometry']['location']['lat'];
        $lng = $result['results'][0]['geometry']['location']['lng'];

        echo $latlong = json_encode(array('lat' => $lat, 'lng' => $lng));
    }

    public function AddHospital()
    {

        if ($_POST) {
            if ($_POST['locationwork'] == "") {
                $location = "0";
            } else {
                $location = $_POST['locationwork'];
            }
            /*  $this->Work_master_model->insert(array(
            'name_en'       => trim($_POST['name_en']),
            'address_en'    => $_POST['addresswork'],
            'location_id'   => $location,
            'email'         => $_POST['emailHospital'],
            'phone'         => $_POST['phoneHospital'],
            'work_type'     => 'Hospital',
            'status'        => '0',
            ));*/
            $this->userhospitalSendmail($_POST);
            $this->flash_notification('Your request has been send. Hospital will be shown once the moderator will approve the request');
            redirect(base_url('home'));
        }
    }

    public function AddClinic()
    {

        if ($_POST) {
            if ($_POST['locationClinic'] == "") {
                $location = "0";
            } else {
                $location = $_POST['locationClinic'];
            }
            /*   $this->Work_master_model->insert(array(
            'name_en'       => trim($_POST['nameClinic']),
            'address_en'    => $_POST['addressClinic'],
            'location_id'   => $location,
            'email'         => $_POST['emailClinic'],
            'phone'         => $_POST['phoneClinic'],
            'work_type'     => 'Clinic',
            'is_private'    => (isset($_POST['isPrivate'])) ? '1' : '0',
            'status'        => '0',
            ));*/
            $this->userClinicSendmail($_POST);
            $this->flash_notification('Your request has been send. Clinic will be shown once the moderator will approve the request');
            redirect(base_url('home'));
        }
    }

    public function AddLab()
    {

        if ($_POST) {
            if ($_POST['locationLab'] == "") {
                $location = "0";
            } else {
                $location = $_POST['locationLab'];
            }

            /*$this->Work_master_model->insert(array(
            'name_en'       => trim($_POST['nameLab']),
            'address_en'    => $_POST['addressLab'],
            'location_id'   => $location,
            'email'         => $_POST['emailLab'],
            'phone'         => $_POST['phoneLab'],
            'work_type'     => $_POST['labtype'],
            'status'        => '0',
            ));*/
            $this->userLabSendmail($_POST);
            $this->flash_notification('Your request has been send. Lab will be shown once the moderator will approve the request');
            redirect(base_url('home'));
        }
    }

    public function checkDuplicateWork($param = '')
    {
        $workName = trim($this->input->post('workName'));
        if (!empty($workName)) {
            $data = $this->Work_master_model->get_by(array('name_en' => $workName));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        } else {
            echo "true";
        }
    }

    public function change_password($id)
    {
        if ($_POST) {
            $user_id = $this->User_master_model->update($id, array(
                'password' => hash('md5', $_POST['cpconfpassword'] . config_item('encryption_key'))));

        }
        $this->flash_notification('Password is successfully change.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function check_password($param = '')
    {

        $password = $this->input->post('cppassword');
        $data = $this->User_master_model->get_by(array(
            'user_id =' => $this->input->post('userid'),
            'password' => hash('md5', $password . config_item('encryption_key')),
        ));
        if ($data) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function updateProfile()
    {

        //$path = dirname(FCPATH.'uploads/user_image');

        $user_id = $this->session->userdata('user_id');
        $array = array("first_name" => $this->input->post('firstname'),
            "last_name" => $this->input->post('lastname'),
            "address" => $this->input->post('address'),
            "country_id" => $this->input->post('country_id'));
        $this->session->set_userdata('user_firstname', $this->input->post('firstname'));
        $this->session->set_userdata('user_lastname', $this->input->post('lastname'));

        if (!empty($_FILES['userprofile']['name'])) {
            $file = $_FILES['userprofile']['name'];
            $file_ext = explode('.', $file);
            $ext = strtolower(end($file_ext));
            $extension = array("jpg", 'png', 'jpeg');
            if (in_array($ext, $extension)) {
                /*if(file_exists($path.'/'.$user_id.'.jpg')) {
                unlink($filename);
                } */
                move_uploaded_file($_FILES['userprofile']['tmp_name'], FCPATH . 'uploads/user_image/' . $user_id . '.jpg');
                $this->User_master_model->update($user_id, array('photo' => $user_id . '.jpg'));
            } else {
                $this->session->set_flashdata('error', 'Please upload valid image');
                redirect(base_url());
            }
        }

        $this->User_master_model->update($user_id, $array);
        $this->flash_notification('Profile update successfully');
        redirect(base_url());

    }

    public function guestLogin()
    {

        $this->session->set_userdata("user_login", "2");
        $this->session->set_userdata("fullname", "Guest");
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Send Mail to admin for new doctor information
     * @param mixed array $data
     */
    public function userdoctorSendmail($data)
    {
        if ($data['location'] == "") {
            $location = "";
        } else {
            $this->load->model('admin/Location_master_model');
            $location_id = $data['location'];
            $location_row = $this->Location_master_model->get($location_id);
            $location = $location_row->name_en;
        }
        if ($data['speciality'] == "") {
            $speciality = "";
        } else {
            $speciality_id = $data['speciality'];
            $this->load->model('admin/Specialty_master_model');
            $speciality_row = $this->Specialty_master_model->get($speciality_id);
            $speciality = $speciality_row->name_en;
        }

        if ($data['gender'] == "1") {
            $gender = 'Male';
        } else {
            $gender = 'Female';
        }
        /*if ($data['work'] == "") {
            $work = "";
        } else {
            $this->load->model('Work_master_model');
            $work_id = $data['work'];
            $work_row = $this->Work_master_model->get($work_id);
            $work = $work_row->name_en;
        }*/
        $this->load->model('admin/User_master_model');
        if ($this->session->userdata('user_id')) {
            $user_id = $this->session->userdata('user_id');
            $user = $this->User_master_model->get($user_id);
            $username = $user->first_name . " " . $user->last_name;
            $useremail = $user->email;
        }
          $data['full_path']  ='';
        $this->load->library('upload');

                if($_FILES['doctorProfile']['size'] > 0) { // upload is the name of the file field in the form

                $aConfig['upload_path']      = './uploads/img_mail';
                $aConfig['allowed_types']    = 'jpg|png|jpeg|gif';                

                $this->upload->initialize($aConfig);

                  if($this->upload->do_upload('doctorProfile'))
                  {
                    $ret = $this->upload->data();
                    //echo ;die;
                    
                    $data['full_path'] = $ret['full_path'];                   

                  } else {
                     $this->session->set_flashdata('error', "Upload valid image!");
                    redirect(base_url('home'));
                    $pathToUploadedFile = '';
                   
                  }
                 
                }

                $address = $data['address'];
                if($address!=""){
                // Get JSON results from this request
                $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');

                // Convert the JSON to an array
                $geo = json_decode($geo, true);

                if ($geo['status'] == 'OK') {
                  // Get Lat & Long
                  $data['latitude'] = $geo['results'][0]['geometry']['location']['lat'];
                  $data['longtitude'] = $geo['results'][0]['geometry']['location']['lng'];
                }
                }

       $data['subject'] = 'Top Dotcor - New Doctor Information';
        $message = "Hello Top Doctors,";
        $message .= "<br/><br/>A new doctor information has been reqested for Top Doctors.";
        $message .= "<br/><br/><b>First Name  : </b>" . $data['firstname'];
        $message .= "<br/><b>Last Name : </b>" . $data['lastname'];
        $message .= "<br/><b>Address : </b>" . $data['address'];
        $message .= "<br/><b>Email : </b>" . $data['email'];
        $message .= "<br/><b>Phone Number : </b>" . $data['phone'];
        $message .= "<br/><b>Mobile Number : </b>" . $data['mobile'];
        $message .= "<br/><b>Biography : </b>" . $data['biography'];
        $message .= "<br/><b>Work Hours : </b>" . $data['work_hours'];        
        /*$message .= "<br/><b>Work : </b>" . $work;*/
        $message .= "<br/><b>Location : </b>" . $location;
        $message .= "<br/><b>Speciality : </b>" . $speciality;
        $message .= "<br/><b>Gender : </b>" . $gender;
        $message .= "<br/><b>Latitude : </b>" . $data['latitude'];
        $message .= "<br/><b>Longitude : </b>" . $data['longtitude'];        
        $message .= "<br/><br/> User Information";
        $message .= "<br/><b> Name : </b>" . $username;
        $message .= "<br/><b> Email : </b>" . $useremail;

        $message .= "<br/><br/>Kindly regards,";
        $message .= "<br/>Top Doctors team";
        $data['message'] = $message;
        $this->sendMail($data);
    }

    /**
     * Send Mail to admin for new hospital information
     * @param mixed array $data
     */
    public function userhospitalSendmail($data)
    {
          if ($data['locationwork'] == "") {
            $location = "";
        } else {
            $this->load->model('admin/Location_master_model');
            $location_id = $data['locationwork'];
            $location_row = $this->Location_master_model->get($location_id);
            $location = $location_row->name_en;
        }

        $this->load->model('admin/User_master_model');
        if ($this->session->userdata('user_id')) {
            $user_id = $this->session->userdata('user_id');
            $user = $this->User_master_model->get($user_id);
            $username = $user->first_name . " " . $user->last_name;
            $useremail = $user->email;
        }
        $pathToUploadedFile = '';
        $data['full_path'] = '';
        $this->load->library('upload');
 $data['full_path'] = '';
                if($_FILES['workProfile']['size'] > 0) { // upload is the name of the file field in the form

                    $aConfig['upload_path']      = './uploads/img_mail';
                    $aConfig['allowed_types']    = 'jpg|png|jpeg|gif';                

                    $this->upload->initialize($aConfig);

                    if($this->upload->do_upload('workProfile'))
                    {
                        $ret = $this->upload->data();
                        $data['full_path'] = $ret['full_path'];

                  } else {
                     $this->session->set_flashdata('error', "Upload valid image!");
                    redirect(base_url('home'));
                    $pathToUploadedFile = '';
                 
                  }
                 
                }
        $data['latitude'] = '';
        $data['longtitude'] = '';
        $address = $data['addresswork'];
        if($address!=""){
        // Get JSON results from this request
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');

        // Convert the JSON to an array
        $geo = json_decode($geo, true);

        if ($geo['status'] == 'OK') {
          // Get Lat & Long
          $data['latitude'] = $geo['results'][0]['geometry']['location']['lat'];
          $data['longtitude'] = $geo['results'][0]['geometry']['location']['lng'];
        }
        }

        $data['subject'] = 'Top Dotcor - New Hospital Information';
        $message = "Hi Admin,";
        $message .= "<br/><br/>A new hospital information has been reqested for Top Doctors.";
        $message .= "<br/><b>First Name : </b>" . $data['firstname'];
        $message .= "<br/><b>Last Name : </b>" . $data['lastname'];
        $message .= "<br/><b>Address : </b>" . $data['addresswork'];
        $message .= "<br/><b>Email : </b>" . $data['emailHospital'];
        $message .= "<br/><b>Phone Number : </b>" . $data['phoneHospital'];
        $message .= "<br/><b>Mobile Number : </b>" . $data['mobile'];
        $message .= "<br/><b>Biography : </b>" . $data['biography'];        
        $message .= "<br/><b>Location : </b>" . $location;
        $message .= "<br/><b>Work Hours : </b>" . $data['work_hours'];        
        $message .= "<br/><b>Work Type : </b> Hospital";
        $message .= "<br/><b>Latitude : </b> ".$data['latitude'];        
        $message .= "<br/><b>Longitude : </b> ".$data['longtitude'] ;               
        $message .= "<br/><br/> User Information";
        $message .= "<br/><b> Name : </b>" . $username;
        $message .= "<br/><b> Email : </b>" . $useremail;

        $message .= "<br/><br/>Kindly regards,";
        $message .= "<br/>Top Doctors team";
        $data['message'] = $message;
        $this->sendMail($data);
    }

    /**
     * Send Mail to admin for new Clinic information
     * @param mixed array $data
     */
    public function userClinicSendmail($data)
    {
         if ($data['locationClinic'] == "") {
            $location = "";
        } else {
            $this->load->model('admin/Location_master_model');
            $location_id = $data['locationClinic'];
            $location_row = $this->Location_master_model->get($location_id);
            $location = $location_row->name_en;
        }

        $this->load->model('admin/User_master_model');
        if ($this->session->userdata('user_id')) {
            $user_id = $this->session->userdata('user_id');
            $user = $this->User_master_model->get($user_id);
            $username = $user->first_name . " " . $user->last_name;
            $useremail = $user->email;
        }

                    $pathToUploadedFile = '';
                     $data['full_path'] = '';
        if($_FILES['workProfile']['size'] > 0) { // upload is the name of the file field in the form

                    $aConfig['upload_path']      = './uploads/img_mail';
                    $aConfig['allowed_types']    = 'jpg|png|jpeg|gif';                

                    $this->upload->initialize($aConfig);

                    if($this->upload->do_upload('workProfile'))
                    {
                        $ret = $this->upload->data();
                        
                        
                        $data['full_path'] = $ret['full_path'];                    

                  } else {
                     $this->session->set_flashdata('error', "Upload valid image!");
                    redirect(base_url('home'));
                    $pathToUploadedFile = '';
                 
                  }
                 
                }
                $data['latitude'] = '';
        $data['longtitude'] = '';
        $address = $data['addressClinic'];
        if($address!=""){
        // Get JSON results from this request
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');

        // Convert the JSON to an array
        $geo = json_decode($geo, true);

        if ($geo['status'] == 'OK') {
          // Get Lat & Long
          $data['latitude'] = $geo['results'][0]['geometry']['location']['lat'];
          $data['longtitude'] = $geo['results'][0]['geometry']['location']['lng'];
        }
        }

        $data['subject'] = 'Top Dotcor - New Clinic Information';
        $message = "Hi Admin,";
        $message .= "<br/><br/>A new clinic information has been reqested for Top Doctors.";
        $message .= "<br/><b>First Name : </b>" . $data['firstname'];
        $message .= "<br/><b>Last Name : </b>" . $data['lastname'];        
        $message .= "<br/><b>Address : </b>" . $data['addressClinic'];
        $message .= "<br/><b>Email : </b>" . $data['emailClinic'];
        $message .= "<br/><b>Phone Number : </b>" . $data['phoneClinic'];
        $message .= "<br/><b>Mobile Number : </b>" . $data['mobile'];        
        $message .= "<br/><b>Biography : </b>" . $data['biography'];        
        $message .= "<br/><b>Work Hours : </b>" . $data['work_hours'];                
        $message .= "<br/><b>Location : </b>" . $location;
        $message .= "<br/><b>Work Type : </b> Clinic";
        $message .= "<br/><b>Latitude : </b> ".$data['latitude'];        
        $message .= "<br/><b>Longitude : </b> ".$data['longtitude'] ;        
        $message .= "<br/><br/> User Information";
        $message .= "<br/><b> Name : </b>" . $username;
        $message .= "<br/><b> Email : </b>" . $useremail;
        $message .= "<br/><br/>Kindly regards,";
        $message .= "<br/>Top Doctors team";
        $data['message'] = $message;
        $this->sendMail($data);
    }

    /**
     * Send Mail to admin for new Lab information
     * @param mixed array $data
     */
    public function userLabSendmail($data)
    {
        if ($data['locationLab'] == "") {
            $location = "";
        } else {
            $this->load->model('admin/Location_master_model');
            $location_id = $data['locationLab'];
            $location_row = $this->Location_master_model->get($location_id);
            $location = $location_row->name_en;
        }

        $this->load->model('admin/User_master_model');
        if ($this->session->userdata('user_id')) {
            $user_id = $this->session->userdata('user_id');
            $user = $this->User_master_model->get($user_id);
            $username = $user->first_name . " " . $user->last_name;
            $useremail = $user->email;
        }

           $pathToUploadedFile = '';
                     $data['full_path'] = '';
        if($_FILES['workProfile']['size'] > 0) { // upload is the name of the file field in the form

                    $aConfig['upload_path']      = './uploads/img_mail';
                    $aConfig['allowed_types']    = 'jpg|png|jpeg|gif';                

                    $this->upload->initialize($aConfig);

                    if($this->upload->do_upload('workProfile'))
                    {
                        $ret = $this->upload->data();
                        
                        
                        $data['full_path'] = $ret['full_path'];                    

                  } else {
                     $this->session->set_flashdata('error', "Upload valid image!");
                    redirect(base_url('home'));
                    $pathToUploadedFile = '';
                 
                  }
                 
                }
        $data['latitude'] = '';
        $data['longtitude'] = '';
        $address = $data['addressLab'];
        if($address!=""){
        // Get JSON results from this request
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');

        // Convert the JSON to an array
        $geo = json_decode($geo, true);

        if ($geo['status'] == 'OK') {
          // Get Lat & Long
          $data['latitude'] = $geo['results'][0]['geometry']['location']['lat'];
          $data['longtitude'] = $geo['results'][0]['geometry']['location']['lng'];
        }
        }

        $data['subject'] = 'Top Dotcor - New Lab Information';
        $message = "Hi Admin,";
        $message .= "<br/><br/>A new lab information has been reqested for Top Doctors.";
        $message .= "<br/><b>First Name : </b>" . $data['firstname'];
        $message .= "<br/><b>Last Name : </b>" . $data['lastname'];
        $message .= "<br/><b>Address : </b>" . $data['addressLab'];
        $message .= "<br/><b>Email : </b>" . $data['emailLab'];
        $message .= "<br/><b>Phone Number : </b>" . $data['phoneLab'];
        $message .= "<br/><b>Mobile Number : </b>" . $data['mobile'];
        $message .= "<br/><b>Location : </b>" . $location;
        $message .= "<br/><b>Biography : </b>" . $data['biography'];                
        $message .= "<br/><b>Work Hours : </b>" . $data['work_hours'];        
        $message .= "<br/><b>Work Type : </b> " . $data['labtype'];
        $message .= "<br/><b>Latitude : </b> ".$data['latitude'];        
        $message .= "<br/><b>Longitude : </b> ".$data['longtitude'] ;        
        $message .= "<br/><br/> User Information";
        $message .= "<br/><b> Name : </b>" . $username;
        $message .= "<br/><b> Email : </b>" . $useremail;

        $message .= "<br/><br/>Kindly regards,";
        $message .= "<br/>Top Doctors team";
        $data['message'] = $message;
        $this->sendMail($data);
    }

    /**
     * Send email to admin
     * @param  array $data
     */
    public function sendMail($data)
    {
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->email->from('topdoctors123@gmail.com'); // change it to yours
        $this->email->to('topdoctors123@gmail.com'); // change it to yours        
      //  $this->email->cc('mayur.panchal@searchnative.in'); // change it to yours        
        $this->email->subject($data['subject']);
        if($data['full_path']!="")
        {
            $this->email->attach($data['full_path']);
        }
        $this->email->message($data['message']);
        if (!$this->email->send()) {
            $this->session->set_flashdata('error', 'Error occured while sending an email');
            redirect($_SERVER['HTTP_REFERER']);
            /*$this->response([
        'status'    => FALSE,
        'message'    => 'Error occured while sending an email'
        ], REST_Controller::HTTP_OK);*/
        }

    }

    public function getphp()
    {
        print_r(phpinfo());
    }

}
