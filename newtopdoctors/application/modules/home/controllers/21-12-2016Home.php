<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('home/User_master_model');
        $this->load->model('home/Doctor_details_model');
        $this->load->model('home/Doctor_master_en_model');
        $this->load->model('home/Doctor_master_model');
        $this->load->model('home/Work_master_model');
        $this->data['active'] = "home";
        $this->load->library('email');
    }

    function index() {
        
        $this->data['title']='TopDoctors';
        $this->__templateFront('home/index', $this->data);
    }

    function login_process() {

        $email=$_POST['loginemail'];
        $password=$_POST['loginpassword'];

        $get_row=$this->db->get_where('user_master',array('email'=>$email,'password'=>hash('md5', $password . config_item('encryption_key')),'status'=>'1'));
        if($get_row->num_rows()>0) {
            $row=$get_row->row();
            $this->session->set_userdata("user_login", "1");
            $this->session->set_userdata("user_id", $row->user_id);
            $this->session->set_userdata("user_firstname", $row->first_name);
            $this->session->set_userdata("user_lastname", $row->last_name);
                //redirect($_SERVER['HTTP_REFERER']);
        } else {
            if($this->session->userdata('site_lang') == 'arabic'){
                $errorLogin=$this->lang->line('errorLogin');
            }else{
                $errorLogin=$this->lang->line('errorLogin');
            }
            $this->session->set_flashdata('errorLogin',$errorLogin);
            //redirect(base_url());
            //redirect($_SERVER['HTTP_REFERER']);
        }
        if(isset($_POST['redirect']) && $_POST['redirect']!="")
        {
         redirect($_POST['redirect']);
        }
        else{
            redirect(base_url());
        }
    }

    function signup_process(){
         if($_POST){
                $this->User_master_model->insert(array(
                'first_name'    => $_POST['firstname'],
                'last_name'     => $_POST['lastname'],
                'email'         => $_POST['email'],
                'mobile_number' => $_POST['mobile_no'],
                'password'      => hash('md5', $_POST['c_password'] . config_item('encryption_key')),
                'country_id'    => $_POST['country_id'],
                'address'       => $_POST['address'],
            ));

                $insert_id = $this->db->insert_id();
                if(isset($_POST['c1'])){
                    $this->User_master_model->update($insert_id,array('newslatter'=> $_POST['c1']));
                }
                $this->session->set_userdata("user_login", "1");
                $this->session->set_userdata("user_id", $insert_id);
                $this->session->set_userdata("user_firstname", $_POST['firstname']);
                $this->session->set_userdata("user_lastname", $_POST['lastname']);

                if ($insert_id) {
                $this->email->set_newline("\r\n");
                $this->email->from('tejas.patel@searchnative.in', 'Top doctor');
                $this->email->to($_POST['email']);
                $this->email->subject('Top doctor registration success');
                $data['firstname'] = $_POST['firstname'];
                $data['email']     = $_POST['email'];
                $data['password']  = $_POST['password'];
                $msg = $this->load->view('template/email_template',$data,TRUE);
                $this->email->message($msg);
                $this->email->send();

                /*Email Send for Newsletter*/
                if($_POST['c1']=='1') {
                    $this->email->set_newline("\r\n");
                    $this->email->from('tejas.patel@searchnative.in', 'Top doctor');
                    $this->email->to($_POST['email']);
                    $this->email->subject('Newsletter subscribe for top doctor');
                    $dataln['firstname'] = $_POST['firstname'];
                    $msgnew = $this->load->view('template/newslatter_template',$dataln,TRUE);
                    $this->email->message($msgnew);
                    $this->email->send();
                }
            }

            if($this->session->userdata('site_lang') == 'arabic'){
                $registerSuccess=$this->lang->line('registerSuccess');
            }else{
                $registerSuccess=$this->lang->line('registerSuccess');
            }
            $this->flash_notification($registerSuccess);
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url('home'));
    }

    function fblogin(){
        $base_url=$this->config->item('base_url'); //Read the baseurl from the config.php file
        //get the Facebook appId and app secret from facebook.php which located in config directory for the creating the object for Facebook class
        $facebook = new Facebook(array(
        'appId'     => $this->config->item('appID'), 
        'secret'    => $this->config->item('appSecret'),
        ));
        
        $user = $facebook->getUser(); // Get the facebook user id 
        
        if($user){
            
            try{
                $user_profile = $facebook->api('/me?fields=id,first_name,last_name,email');  //Get the facebook user profile data
                $user_profile['email'];
                $this->session->set_userdata("user_login", "1");
                $this->session->set_userdata("user_firstname", $user_profile['first_name']);
                $this->session->set_userdata("user_lastname", $user_profile['last_name']);
                redirect(base_url('home'));
                $params = array('next' => $base_url.'fbci/logout');
                
                $ses_user=array('User'=>$user_profile,
                   'logout' =>$facebook->getLogoutUrl($params)   //generating the logout url for facebook 
                );
                $this->session->set_userdata($ses_user);
                header('Location: '.$base_url);
            }catch(FacebookApiException $e){
                error_log($e);
                $user = NULL;
            }  
            redirect($_SERVER['HTTP_REFERER']);  
        }   
        redirect($_SERVER['HTTP_REFERER']);
    }
    
    function check_user_email($param = '') {
       
        $email = $_POST['email'];
        if ($param == '') {
            $data = $this->User_master_model->get_by(array(
                'email' => $email
            ));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    function forgot_password() {
        $email=$_POST['forgotemail']; 
        if ($_POST) {
           $record = $this->db->get_where('user_master',array('email'=>$email))->row();
           
            if ($record) {
                $user_id = $record->user_id; 
                $random_string = $this->random_string_generate();
                //update token
                $this->db->update('user_master', [
                    'token' => $random_string
                ], ['email' => $record->email]);
                //generate url
                $url = base_url() . 'home/resetPassword/' . $random_string;

                /*$this->update_forgot_password_key('', $user_id, hash('md5',$random_string.config_item('encryption_key')));*/
                $this->email->set_newline("\r\n");
                $this->email->from('tejas.patel@searchnative.in', 'Top doctor');
                $this->email->to($record->email);
                $this->email->subject('Reset your top doctor password');
                $dataforgot['password'] = $random_string;
                $dataforgot['reset_link'] = $url;
                $msgforgotpass=$this->load->view('template/fotgotpass_template',$dataforgot,TRUE);

                //echo $msgforgotpass;exit;

                $this->email->message($msgforgotpass);
                $this->email->send();

                if($this->session->userdata('site_lang') == 'arabic'){
                    $registerSuccess=$this->lang->line('forgotpasswordsuccess');
                }else{
                    $registerSuccess=$this->lang->line('forgotpasswordsuccess');
                }
                $this->flash_notification($registerSuccess);

                redirect(base_url('home'));
            } else {
                if($this->session->userdata('site_lang') == 'arabic'){
                    $forgotEmailNotFound=$this->lang->line('forgotEmailNotFound');
                }else{
                    $forgotEmailNotFound=$this->lang->line('forgotEmailNotFound');
                }
                $this->session->set_flashdata('forgotEmailNotFound',$forgotEmailNotFound );
                redirect(base_url('home'));
            }
        } else {
           redirect(base_url('home'));
        }
    }

    function resetPassword($token) {
        if ($_POST) {
            if ($token == $_POST['_token']) {
                //update password and reset token and redirect
                if ($_POST['new_password'] == $_POST['confirm_password']) {
                    $this->db->update('user_master', 
                        [
                            'password' => hash('md5', $_POST['new_password'] . config_item('encryption_key')), 
                            'token' => ''
                        ], 
                        ['token' => $_POST['_token']]);
                    $this->flash_notification('Password has been successfully updated.');
                    redirect(base_url());
                } else {
                    $this->session->set_flashdata("error", "Password is mismatched.");
                    redirect(base_url() . 'home/resetPassword/' . $token);
                }
            }
        }
        
        //check entry for token
        $user = $this->db->select()->from('user_master')
            ->where('token',$token)->get()->result();

        if ($user) {
            $this->data['token'] = $token;
            //$this->__templateFront('home/reset_password', $data);
            $this->data['title']='Reset Password';
        $this->__templateFront('home/reset_password', $this->data,TRUE90);
            //$this->__template('home/reset_password',$this->data);
            //$this->load->view('home/reset_password.php');
            //show reset password form
            
        } else {
            $this->session->set_flashdata("error", "Link is Expired");
            redirect(base_url());
         //   show_404();
        }
    }

    /*function reset_password() {
         $email = $this->post('email');
         //check for data
         $user = $this->db->select()->from()->where()->get()->result();
         if (count($user)) {
            $token = $this->random_string_generate();
            //update token for user

            //generate url
            $url = base_url() . 'reset-password/' . ;
         }
    }*/

    function random_string_generate() {
        $this->load->helper('string');
        return random_string('alnum', 16);
    }

    function update_forgot_password_key($user_type, $user_id, $key) {
            $this->db->where('user_id', $user_id);
            $this->db->update('user_master', [
                'password' => $key
            ]);
    }
    
    function send_news() {
        $getSubscribe=$this->db->get_where('user_master',array('newslatter'=>1))->result();
        foreach ($getSubscribe as $getSubscribeRow)
        {
            $this->email->set_newline("\r\n");
            $this->email->to($getSubscribeRow->email);
            $this->email->from('tejas.patel@searchnative.in');
            $this->email->subject('@ Top doctor - Newsletter Alert');
            $this->email->message('Hi Here is the info you requested.');
            $this->email->send();
        }
    }

    function AddDoctor(){
        if($_POST){
            
            $this->load->model('admin/Doctor_details_model');
            $email_id = $this->Doctor_details_model->get_by(array('email'=>$_POST['email']));
              if($_POST['location']=="")
            {
                $location = "0";
            }
            else{
                $location = $_POST['location'];
            }
            if(count($email_id) > 0)
            {
               // $this->flash_notification('Email already exists!');
                $this->session->set_flashdata('error', "Email already exists!");
                 redirect(base_url('home'));
            }
                $this->Doctor_master_model->insert(array('first_name'    => '',));
                $this->Doctor_master_en_model->insert(array(
                'first_name'    => $_POST['firstname'],
                'last_name'     => $_POST['lastname'],
                'address'       => $_POST['address'],
            ));
                $this->Doctor_details_model->insert(array(
                'email'                 => $_POST['email'],
                'work_id'               => $_POST['work'],
                'mobile_number'         => $_POST['phone'],
                'location_id'           => $location,
                'speciality_id'         => $_POST['speciality'],    
                'gender'                => $_POST['gender'],
                'google_map_latitude'   => $_POST['latitude'],
                'google_map_longtude'   => $_POST['longtitude'], 
                'visibility'            => '0', 
            ));
            $this->flash_notification('Your request has been send. Doctor will be shown once the moderator will approve the request');
            redirect(base_url('home'));
        }
    }

    function latlong(){
        
        $zipcode=$_POST['address']; 
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=fam_close(fam)";
        $details=file_get_contents($url);
        $result = json_decode($details,true);
        $lat=$result['results'][0]['geometry']['location']['lat'];
        $lng=$result['results'][0]['geometry']['location']['lng'];

        echo $latlong=json_encode(array('lat'=>$lat,'lng'=>$lng));
    }

    function AddHospital(){

        if($_POST){
              if($_POST['locationwork']=="")
            {
                $location = "0";
            }
            else{
                $location = $_POST['locationwork'];
            }
            $this->Work_master_model->insert(array(
                'name_en'       => $_POST['name_en'],
                'address_en'    => $_POST['addresswork'],
                'location_id'   => $location,
                'email'         => $_POST['emailHospital'],
                'phone'         => $_POST['phoneHospital'],
                'work_type'     => 'Hospital',
                'status'        => '0',
            ));
            $this->flash_notification('Your request has been send. Hospital will be shown once the moderator will approve the request');
            redirect(base_url('home'));
        }
    }

    function AddClinic(){

        if($_POST){
              if($_POST['locationClinic']=="")
            {
                $location = "0";
            }
            else{
                $location = $_POST['locationClinic'];
            }
            $this->Work_master_model->insert(array(
                'name_en'       => $_POST['nameClinic'],
                'address_en'    => $_POST['addressClinic'],
                'location_id'   => $location,
                'email'         => $_POST['emailClinic'],
                'phone'         => $_POST['phoneClinic'],
                'work_type'     => 'Clinic',
                'is_private'    => (isset($_POST['isPrivate'])) ? '1' : '0',
                'status'        => '0',
            ));
            $this->flash_notification('Your request has been send. Clinic will be shown once the moderator will approve the request');
            redirect(base_url('home'));
        }
    }

    function AddLab(){

        if($_POST){
            if($_POST['locationLab']=="")
            {
                $location = "0";
            }
            else{
                $location = $_POST['locationLab'];
            }
            
            $this->Work_master_model->insert(array(
                'name_en'       => $_POST['nameLab'],
                'address_en'    => $_POST['addressLab'],
                'location_id'   => $location,
                'email'         => $_POST['emailLab'],
                'phone'         => $_POST['emailLab'],
                'work_type'     => $_POST['labtype'],
                'status'        => '0',
            ));
            $this->flash_notification('Your request has been send. Lab will be shown once the moderator will approve the request');
            redirect(base_url('home'));
        }
    }

    function checkDuplicateWork($param = '') {
        $workName = $this->input->post('workName');
        $data = $this->Work_master_model->get_by(array('name_en' => $workName));
        if ($data) {
            echo "false";
        } else {
            echo "true";
        }
    }

    function change_password($id) {
        if($_POST) {
                $user_id = $this->User_master_model->update($id,array(
                'password'      => hash('md5', $_POST['cpconfpassword'] . config_item('encryption_key'))));

        }
        $this->flash_notification('Password is successfully change.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function check_password($param = '') {

        $password = $this->input->post('cppassword');
        $data = $this->User_master_model->get_by(array(
            'user_id =' => $this->input->post('userid'),
            'password' => hash('md5', $password . config_item('encryption_key'))
        ));
        if ($data) {
            echo "true";
        } else {
            echo "false";
        }
    }
    
    function updateProfile(){

       //$path = dirname(FCPATH.'uploads/user_image');

       $user_id = $this->session->userdata('user_id');
       $array = array("first_name"=>$this->input->post('firstname'),
                    "last_name"=>$this->input->post('lastname'),
           "address"=>$this->input->post('address'),
           "country_id"=>$this->input->post('country_id') );
       $this->session->set_userdata('user_firstname',$this->input->post('firstname'));
       $this->session->set_userdata('user_lastname',$this->input->post('lastname'));

       if(!empty($_FILES['userprofile']['name'])) {
            $file = $_FILES['userprofile']['name'];
            $file_ext = explode('.',$file);
            $ext = strtolower(end($file_ext));
            $extension = array("jpg",'png','jpeg');
            if(in_array($ext, $extension))
            {
                /*if(file_exists($path.'/'.$user_id.'.jpg')) {
                      unlink($filename);
                } */ 
                move_uploaded_file($_FILES['userprofile']['tmp_name'], FCPATH.'uploads/user_image/'.$user_id.'.jpg');
                $this->User_master_model->update($user_id,array('photo'=>$user_id.'.jpg'));
            }
            else{
                $this->session->set_flashdata('error','Please upload valid image');
                redirect(base_url());                
            }
        }

       $this->User_master_model->update($user_id,$array);
       $this->flash_notification('Profile update successfully');
       redirect(base_url());       
       
    }

    function guestLogin(){

            $this->session->set_userdata("user_login", "2");
            $this->session->set_userdata("fullname","Guest");
            redirect($_SERVER['HTTP_REFERER']);
    }

    
}
