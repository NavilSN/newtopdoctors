<style>

.mt83{ margin-top: 83px !important; }
#contact h2{ color: #053b61; }
.title-description, .contact-details ul li {
  color: #000;
  font-weight: normal;
  font-family: 'Exo-medium';
}

</style>

<div id="contact" class="mt83 page">
<div class="container">

<div class="row">                      
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body"> 
                <div class="box-content">     
                <h2>
                  <?php echo $title; ?>
                </h2>
                                                    
                    <?php echo form_open(base_url() . 'home/resetPassword/' . $token, array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'resetpassword-form', 'target' => '_top', "enctype" => "multipart/form-data")); ?>
                    <input type="hidden" name="_token" id="token" value="<?php echo $token;?>">
                    <div class="padded">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">New Password</label>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" name="new_password" id="new_password"/>
                            </div>  
                        </div>
                      
                         <div class="form-group">
                            <label class="col-sm-2 control-label">Confirm Password</label>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" name="confirm_password" id="confirm_password"/>
                            </div>  
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-3">
                                <button type="submit" class="btn btn-info vd_bg-green" ><?php echo ucwords("update"); ?></button>
                            </div>
                        </div>
                        </form>               
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">


// For Add Hospital 

$(document).ready(function () {

  

          $("#resetpassword-form").validate({
            rules: {                
                new_password: "required",
                confirm_password: {
                            required:true,
                            equalTo : "#new_password"
                        },
                },
            messages: {               
                new_password: "Enter new password",
                confirm_password: {
                    required:"Enter Confirm password",
                    equalTo:"New password and confirm password not match"

                },
            }
        });
    });
</script>
