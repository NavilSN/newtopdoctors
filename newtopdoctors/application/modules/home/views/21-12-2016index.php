<?php 

$siteLang=$this->session->userdata('site_lang');
$this->load->model('home/Location_master_model');
$this->load->model('home/Specialty_master_model');
$this->load->model('home/Work_master_model');

$location=$this->Location_master_model->get_all_location_by_name();
$speciality=$this->Specialty_master_model->get_all_speciality();
$work=$this->Work_master_model->get_all_work();

/* Get average Rating Query*/
if($siteLang=="arabic")
{
$this->db->select('AVG(reputation + clinic + availability + approachability + technology)/5 as average_score ,date_created,comment,user_master.first_name as UFName ,user_master.last_name as ULName,doctor_master.first_name as doctorFname,doctor_master.last_name as doctorLname,doctor_details.photo, doctor_details.doctor_id,user_master.photo AS Uphoto,doctors_rating.reviewIcon');
}else{
$this->db->select('AVG(reputation + clinic + availability + approachability + technology)/5 as average_score ,date_created,comment,user_master.first_name as UFName ,user_master.last_name as ULName,doctor_master_en.first_name as doctorFname,doctor_master_en.last_name as doctorLname,doctor_details.photo, doctor_details.doctor_id,user_master.photo AS Uphoto,doctors_rating.reviewIcon');
}

$this->db->limit(2, 0);
$this->db->order_by('average_score', 'desc');
$this->db->join('user_master','user_master.user_id=doctors_rating.user_id');
$this->db->join('doctor_details','doctor_details.doctor_id=doctors_rating.doctor_id');
$this->db->join('doctor_master_en','doctor_master_en.doctor_id=doctors_rating.doctor_id');
$this->db->join('doctor_master','doctor_master.doctor_id=doctors_rating.doctor_id');
$this->db->where('doctors_rating.dstatus','1');
$this->db->group_by('doctor_details.doctor_id');
$average_score=$this->db->get('doctors_rating')->result();    

function timeAgo($time_ago){
  $cur_time   = time();
  $time_elapsed   = $cur_time - $time_ago;
  $seconds  = $time_elapsed ;
  $minutes  = round($time_elapsed / 60 );
  $hours    = round($time_elapsed / 3600);
  $days     = round($time_elapsed / 86400 );
  $weeks    = round($time_elapsed / 604800);
  $months   = round($time_elapsed / 2600640 );
  $years    = round($time_elapsed / 31207680 );
  if($seconds <= 60){echo "$seconds seconds ago";}
  else if($minutes <=60){
    if($minutes==1){echo "one minute ago";}else{echo "$minutes minutes ago";}
  }else if($hours <=24){
    if($hours==1){echo "an hour ago";}else{echo "$hours hours ago";}
  }else if($days <= 7){
    if($days==1){echo "$days day ago";}else{echo "$days days ago";}
  }else if($weeks <= 4.3){
    if($weeks==1){echo "1 week ago";}else{echo "$weeks weeks ago";}
  }else if($months <=12){
    if($months==1){echo "1 month ago";}else{echo "$months months ago";}
  }else{
    if($years==1){echo "one year ago";}else{echo "$years years ago";}
  }
}
?>
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
  <!--<link rel="stylesheet" href="/resources/demos/style.css">-->
  
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
  <script>
  $( function() {
    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
      _renderItem: function( ul, item ) {
        var li = $( "<li>" ),
          wrapper = $( "<div>", { text: item.label } );
 
        if ( item.disabled ) {
          li.addClass( "ui-state-disabled" );
        }
 
        $( "<span>", {
          style: item.element.attr( "data-style" ),
          "class": "ui-icon " + item.element.attr( "data-class" )
        })
          .appendTo( wrapper );
 
        return li.append( wrapper ).appendTo( ul );
      }
    });
 
   
    $( "#people" )
      .iconselectmenu()
      .iconselectmenu( "menuWidget")
        .addClass( "ui-menu-icons avatar" );
  } );
  
  </script>
  <style>
   
    fieldset {
      border: 0;
    }
    label {
      display: block;
    }
 
    /* select with custom icons */
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item-wrapper {
      padding: 0.5em 0 0.5em 3em;
    }
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item .ui-icon {
      height: 24px;
      width: 24px;
      top: 0.1em;
    }
   
 
    /* select with CSS avatar icons */
    option.avatar {
      background-repeat: no-repeat !important;
      padding-left: 20px;
    }
    .avatar .ui-icon {
      background-position: left top;
    }
  </style>

<!--///////////////////////////////////////////////////////
       // Search & Rate Your Doctor
       //////////////////////////////////////////////////////////-->
<div class="doctor-det-frm">
  <section class="text-center clearfix">
  <!-- <img src="images/doctors-banner.jpg" class="img-responsive"> -->
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-3 text-right">
               <div class="search-links-det">
                <ul class="tab-link-searchbox clearfix">
                  <li class="active" id="Doctors">
                    <a href="javascript:void(0)"  class="doctors-link"><?php echo $this->lang->line('homeSearchDoctors'); ?></a>
                  </li>
                  <li id="Hospital">
                    <a href="javascript:void(0)" class="hostpitals-link"><?php echo $this->lang->line('homeSearchHostpitals'); ?></a>
                  </li>
                  <li id="Clinic" >
                    <a href="javascript:void(0)" class="clinics-link"><?php echo $this->lang->line('homeSearchClinic'); ?></a>
                  </li>
                  <li  id="Lab">
                    <a href="javascript:void(0)" class="labs-link"><?php echo $this->lang->line('homeSearchLab'); ?></a>
                  </li>
                </ul>
                </div>
            </div>
          
            <div class="w-col w-col-9 text-left search-links-view">
                <form class = "form-horizontal" role = "form" action="<?php echo base_url(); ?>search" method="post" id="searchform">
                <input type="hidden" value="Doctors" name="typeofsearch" class="typeofsearch">   
                    <div class = "form-group location">
                      <div class = "col-sm-6 pull-left col-xs-12">                         
                           <?php                      
                        getChilds();
                        ?>
                      </div>
                        <script>
                          $('#select-location').select('<?php echo $location; ?>');
                          </script>
                    </div>
                    <div class = "form-group speciality">
                      <div class = "col-sm-6 pull-left col-xs-12">
                          <select class = "form-control dropdown" name="speciality" id="people">
                          <option value=""><?php echo $this->lang->line('homeSearchSpeciality'); ?></option>
                          <?php foreach ($speciality as $row_speciality) {?>
                          <!--<option value="<?php echo $row_speciality->specialties_id; ?>" style="background:url('<?php echo base_url(); ?>images/icon/speciality/<?php echo $row_speciality->specialties_id.".png"; ?>'); background-repeat: no-repeat; "> <?php if($siteLang=='arabic') echo $row_speciality->name; else echo $row_speciality->name_en; ?></option>-->
                          <option value="<?php echo $row_speciality->specialties_id; ?>" data-class="avatar" data-style="background-image: url(&apos;<?php echo base_url(); ?>images/icon/speciality/<?php echo $row_speciality->specialties_id.".png"; ?>&apos;);" > <?php if($siteLang=='arabic') echo $row_speciality->name; else echo $row_speciality->name_en; ?></option>
                          <?php  } ?>
                        </select>
                      </div>
                    </div>
                    <div class = "form-group gender">
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown" name="gender">
                           <option value=""><?php echo $this->lang->line('homeSearchGender'); ?></option>
                           <option value="1"><?php echo $this->lang->line('homeSearchGenderMale'); ?></option>
                           <option value="2"><?php echo $this->lang->line('homeSearchGenderFemale'); ?></option>
                        </select>
                      </div>
                    </div> 
                     <div class = "form-group labtype">
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown" name="labtype">
                           <option value=""><?php echo $this->lang->line('LabType'); ?></option>
                           <option value="Radiology Lab"><?php echo $this->lang->line('radiologylab'); ?></option>
                           <option value="Medical Lab"><?php echo $this->lang->line('medicallab'); ?></option>
                        </select>
                      </div>
                    </div> 
                     <div class = "form-group searchname">                       
                        <div class = "col-sm-6 pull-left col-xs-12">
                           <input type="text" class="form-control" id="searchname" name="searchname" placeholder="<?php echo $this->lang->line('homeSearchName'); ?>">
                        </div>
                     </div>
                     
                     
                     <div class = "form-group">
                        <div class = "col-sm-6 pull-left col-xs-12">
                                <div class="btn-ex-one pull-right">
                                  <button type="submit" name="searchData" id="searchData" class="ex-btn"><?php echo $this->lang->line('homeSearchBTN'); ?></button>
                                </div>                        
                               </div>
                        </div>                   
                  </form>
                </div>
             </div>          
          </div>
    </section>
</div>

<!--///////////////////////////////////////////////////////
       // ADD TO OUR LIST
       //////////////////////////////////////////////////////////-->
<div class="our-plan-parlex">
  <div class="parlex4-back">
    <div class="container">
      <div class="wrap">
        <div class="our-plans text-center">
          <h2 class="ourplan-heading"><?php echo $this->lang->line('homeAddList'); ?></h2>
          <div class="sepreater"></div>
        </div>
        <div class="w-row">
          <div class="w-col w-col-3 flip pull-left">
            <div class="plan1" data-toggle="modal" <?php  if($this->session->userdata('user_login')!=1) { ?> onclick="doctorloginpopup();" <?php  }else{ ?> data-target="#addDoctor-model" <?php } ?>>
              <div class="plan1-ser1">
                <h4><?php echo $this->lang->line('homeAddDoctor'); ?></h4>
                <p class="plan1-ser1-para"></p>
                <div class="text-center clearfix">
                  <img class="img-responsive nothover" src="<?php echo base_url(); ?>images/add-doctor.png" alt="Add a doctor">
                  <img class="img-responsive hover" src="<?php echo base_url(); ?>images/add-doctor-hover.png" alt="Add a doctor">
                </div>
              </div>
              
            </div>
          </div>
          <div class="w-col w-col-3 flip pull-left">
            <div class="plan1">
              <div class="plan1-ser1" <?php  if($this->session->userdata('user_login')!=1) { ?> onclick="hospitalloginpopup();" <?php  }else{ ?> data-target="#addHospital-model" <?php } ?>  data-toggle="modal">
                <h4><?php echo $this->lang->line('homeAddHospital'); ?></h4>
                <p class="plan1-ser1-para"></p>
                <div class="text-center clearfix">
                  <img class="img-responsive nothover" src="<?php echo base_url(); ?>images/add-hospital.png" alt="Add a hospital">
                  <img class="img-responsive hover" src="<?php echo base_url(); ?>images/add-hospital-hover.png" alt="Add a hospital">
                </div>
              </div>
            </div>
          </div>
          <div class="w-col w-col-3 flip pull-left">
            <div class="plan1 plan3" <?php  if($this->session->userdata('user_login')!=1) { ?> onclick="clinicloginpopup();" <?php  }else{ ?> data-target="#addClinic-model"  <?php } ?>  data-toggle="modal">
              <div class="plan1-ser1 plan3-ser3">
                <h4><?php echo $this->lang->line('homeAddClinic'); ?></h4>
                <p class="plan1-ser1-para"></p>
                <div class="text-center clearfix">
                  <img class="img-responsive nothover" src="<?php echo base_url(); ?>images/add-clinic.png" alt="Add a clinic">
                  <img class="img-responsive hover" src="<?php echo base_url(); ?>images/add-clinic-hover.png" alt="Add a clinic">
                </div>
              </div>              
            </div>
          </div>
          <div class="w-col w-col-3 flip pull-left">
            <div class="plan1 plan4" <?php  if($this->session->userdata('user_login')!=1) { ?> onclick="labloginpopup();" <?php  }else{ ?> data-target="#addLab-model"   <?php } ?> data-toggle="modal">
              <div class="plan1-ser1 plan4-ser4">
                <h4><?php echo $this->lang->line('homeAddLab'); ?></h4>
                <p class="plan1-ser1-para"></p>
                <div class="text-center clearfix">
                  <img class="img-responsive nothover" src="<?php echo base_url(); ?>images/add-lab.png" alt="Add a lab">
                  <img class="img-responsive hover" src="<?php echo base_url(); ?>images/add-lab-hover.png" alt="Add a lab">                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--///////////////////////////////////////////////////////
       // ARE YOU A DOCTOR?
       //////////////////////////////////////////////////////////-->
<div class="exp service-parlex">
  <div class="who_we_are">
    <div class="container">
      <div class="p20t clearfix">    

          <div class="w-col w-col-6 exp-col2 flip pull-left">
            <div class="col2-div">
              <img class="img-responsive" src="<?php echo base_url(); ?>images/are-you-a-doctor.jpg" alt="Are you a doctor">
            </div>
          </div>
        
          <div class="w-col w-col-6 exp-col2 flip pull-right">
            <div class="col1-div">
              <div class="experinc-box p100t">
                <h3 class="ourplan-heading "><?php echo $this->lang->line('homeRUDoctor'); ?></h3>
                <h4 class="flip text-left"><?php echo $this->lang->line('homeRUDoctor2'); ?></h4>
                <div class="border-buttom"></div>
                <p><?php echo $this->lang->line('homeRUDoctor3'); ?></p>
                <?php if(!$this->session->userdata('user_login')){?>
                  <div class="buttons">
                      <div class="btn-ex-one">
                        <a class="ex-btn" id="signupopen"  href="javascript:void(0);" data-toggle="modal" data-target="#loginModal"><?php echo $this->lang->line('homeSignUP'); ?></a>
                      </div>                        
                 </div>
                 <?php } ?>
               </div>
            </div>
          </div>       
      </div>   
    </div>
  </div>
</div>

  <!--///////////////////////////////////////////////////////
       // TOPDOCTORS ON THE GO
       //////////////////////////////////////////////////////////-->
<div class="exp service-parlex">
  <div class="exp-back">
    <div class="container">
      <div id="who-v-animation">
      <div class="p70t clearfix">
       
        
          <div class="w-col w-col-6 exp-col1 flip pull-left">
            <div class="col1-div">

              <div class="experinc-box p5t">
                <h3 class="ourplan-heading"><?php echo $this->lang->line('homeContent1'); ?></h3>
                <div class="border-buttom"></div>
                <h5 class="title"><?php echo $this->lang->line('homeContent2'); ?></h5>
               
                  <div class="buttons get-application">
                      <div class="btn-ex-one exp-col2 clear flip pull-left">
                        <a class="" href="#">
                            <img class="img-responsive" src="<?php echo base_url(); ?>images/google-play-btn.png" alt="Google Play">
                        </a>
                      </div>
                 </div>
               </div>
            </div>
          </div>

          <div class="w-col w-col-6 exp-col2 flip pull-right">
            <div class="col2-div">
              <?php if($siteLang=='arabic') $androidImage = 'topdoctors-on-the-go-ar.jpg'; else $androidImage = 'topdoctors-on-the-go.jpg';
                ?>
              <img class="ver-ali-bot img-responsive" src="<?php echo base_url(); ?>images/<?php echo $androidImage;?>" alt="">
            </div>
          </div>

      
      </div>
    </div>
    </div>
  </div>
</div>

<!--///////////////////////////////////////////////////////
       // TOP REVIEWS
       //////////////////////////////////////////////////////////-->

<div class="top-reviews our-plan-parlex">
  <div class="parlex4-back">
    <div class="container">
      <div class="wrap">
        <div class="our-plans text-center">
          <h2 class="ourplan-heading"><?php echo $this->lang->line('homeTopReviews'); ?></h2>
          <div class="sepreater"></div>
        </div>
        <div class="w-row">
          <?php $i=1; foreach ($average_score as $value) {?>
          <div class="w-col w-col-6 flip pull-left">
            <div class="plan1_1">
              <div class="plan1-ser1 clearfix">
                
                  <div class="review-user flip pull-left">
                      <a href="<?php echo base_url()."doctorProfile/index/".$value->doctor_id; ?>">
                      <?php if($value->Uphoto=="" || !file_exists(FCPATH.'uploads/user_image/'.$value->Uphoto)){ ?>
                      <img src="<?php echo base_url(); ?>uploads/user.jpg" class="img-responsive" alt="Review User"><?php } else { ?>
                      <img src="<?php echo base_url(); ?>uploads/user_image/<?php echo $value->Uphoto; ?>" class="img-responsive" alt="Review User"><?php } ?></a>
                  </div>
                  <div class="review-details flip pull-right">
                      <a href="<?php echo base_url()."doctorProfile/index/".$value->doctor_id; ?>"><h6 class="flip pull-left"><?php  echo $value->UFName .' '. $value->ULName;?><span class="flip pull-right"><?php echo "For ".$value->doctorFname .' '.$value->doctorLname; ?></span></h6></a>
                      <p class="text-left flip"><?php echo $value->comment; ?>
                      <?php 

                      if($value->reviewIcon!="" || $value->reviewIcon!="0") {?>
                         <img src="<?php echo base_url(); ?>images/profile_files/doctor-pic-<?php echo $value->reviewIcon; ?>.png" style="width:7%;" class="img-responsive m5l" alt="" > 
                         <?php } ?>
                         <!--<img src="<?php echo base_url(); ?>images/small-user-simple.png" class="img-responsive m5l" alt="user-icon">  -->
                      </p>
                        <div class="clearfix">
                            <table class="review-tab-det" width="100%">
                              <tr>
                                <td align="left" class="flip text-left">
                                <?php echo floatval($value->average_score); ?>
                                <input type="hidden" class="rateval<?php echo $i ?>" value="<?php echo floatval($value->average_score); ?>">
                                <div class="rateYo<?php echo $i; ?> pull-left"></div>
                                </td>
                                <td align="right" class="flip text-right">
                                    
                                      <img src="<?php echo base_url(); ?>images/link-share-review.png">
                                    
                                </td>
                              </tr>
                              <tr>
                                 <td align="left" class="flip text-left">
                                    
                                </td>
                                <td align="right" class="flip text-right">
                                  <a href="http://www.facebook.com/sharer.php?u=<?php echo base_url()."doctorProfile/index/".$value->doctor_id; ?>&title=<?php echo $value->doctorFname .' '.$value->doctorLname; ?>" target="_blank">
                                         <img src="<?php echo base_url(); ?>images/fb.png" alt="Facebook" height="32"  />
                                     </a>
                                    <!--<a href="https://twitter.com/share?url=<?php echo base_url()."doctorProfile/index/".$value->doctor_id; ?>&text=<?php echo $value->doctorFname .' '.$value->doctorLname; ?>" target="_blank">-->
                                    <a href=http://twitter.com/share?url=<?php echo base_url()."doctorProfile/index/".$value->doctor_id; ?>&text=<?php echo $value->doctorFname .' '.$value->doctorLname; ?>" target="_blank">
                                  
                                    <img src="<?php echo base_url(); ?>images/twitter.png" alt="Twitter" height="32" />
                                    </a>
                                </td>
                              </tr>
                              <tr>
                                 <td align="left" class="flip text-left" >
                                    <span><?php  $curenttime=$value->date_created;
                                    //$time_ago =strtotime($curenttime);
                                    //echo timeAgo($time_ago);
                                    echo date_duration($curenttime);
                                   ?></span>
                                </td>
                                <td align="right" class="flip text-right" style="display:none;">
                                  <span class="chat">
                                    <img src="<?php echo base_url(); ?>images/chat-icon.png">
                                  </span>
                                </td>
                              </tr>
                          
                            </table>
                      </div>
                  </div>
              </div>              
            </div>
          </div>
          <script>

            $(function () {
            $(".rateYo<?php echo $i ?>").rateYo({
              "starWidth": "15px",
              "ratedFill": "#03878A",
              "rating" : $('.rateval<?php echo $i ?>').val(),
              "readOnly":true
              })
            }); 
          </script>
          <?php $i++; } ?>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  var selector = '.tab-link-searchbox > li';
   $('.labtype').hide();
  $(selector).on('click', function(){
     // $("#searchform").reset();
      $("#searchform")[0].reset();
    $(selector).removeClass('active');
    var text=$(this).addClass('active');
    var id_attr = $(".tab-link-searchbox > li.active").attr('id');
    
    
    $('.typeofsearch').append().val(id_attr);
    if(id_attr=="Doctors"){
      $('.gender').show();      
      $('.searchname').show();
      $('.location').show();
      $('.speciality').show();
      $('.labtype').hide();
    }if(id_attr=="Hospital"){
      $('.gender').hide();   
      $('.speciality').hide();   
      $('.searchname').show();
      $('.location').show();
      $('.labtype').hide();
    }if(id_attr=="Clinic"){
      $('.gender').hide();      
      $('.searchname').show();
      $('.location').show();
      $('.speciality').hide();
      $('.labtype').hide();
    }if(id_attr=="Lab"){
      $('.gender').hide();      
      $('.searchname').show();
      $('.labtype').show();
      $('.location').show();
      $('.speciality').hide();
    }
    
  });
});
</script>

<script type="text/javascript">
function userController($scope,$http) {
  $scope.users = [];
  $http.get('<?php echo base_url(); ?>home/getlocation').success(function($data){ $scope.users=$data; });
}
</script>
  
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
  

<script>
  
 
   $(document).ready(function(){
       

    $( "#speciality" ).iconselectmenu().iconselectmenu( "menuWidget").addClass( "ui-menu-icons avatar2 class-speciality flip text-left" );
    $('#speciality-menu').parent('div').addClass('newClass');
    $("#speciality-menu > li.ui-menu-item").addClass('flip text-left');
    
    
  
     });
 
     
  
  </script>
  <style>
   
    fieldset {
      border: 0;
    }
    label {
      display: block;
    }
 
    /* select with custom icons */
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item-wrapper {
      padding: 0.5em 0 0.5em 3em;
    }
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item .ui-icon {
      height: 24px;
      width: 24px;
      top: 0.1em;
    }
   
 
    /* select with CSS avatar icons */
    option.avatar2 {
      background-repeat: no-repeat !important;
      padding-left: 20px;
    }
    .avatar2 .ui-icon {
      background-position: left top;
    }
  </style>
  <!-- Register -->
<div class="modal fade loginModal addDoctorsModal" id="addDoctor-model" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content login-modal">

            <div class="modal-body">
              <div class="text-center">
                <div role="tabpanel" class="login-tab addDoctors-tab">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation"> <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <?php echo $this->lang->line('AddDoctorPTitle'); ?>
                  </li>               
                </ul>
                <!-- Tab panes -->
              <div class="tab-content">
                  
                  <div role="tabpanel" class="tab-pane active text-center" id="addDoctorsfrm">                  
                    <form action="<?php echo base_url(); ?>home/AddDoctor" method="post" id="AddDoctor">  
                    <div class="form-group">
                        <div class="input-group">
                          <input type="text" class="form-control" name="firstname" id="firstname" placeholder="<?php echo $this->lang->line('AddDoctorPFirstname'); ?>">
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        <span class="help-block has-error" data-error='0' id="firstname-error"></span>
                    </div>

                     <div class="form-group">
                        <div class="input-group">
                          <input type="text" class="form-control" name="lastname" id="lastname" placeholder="<?php echo $this->lang->line('AddDoctorPLastname'); ?>">
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        <span class="help-block has-error" data-error='0' id="lastname-error"></span>
                    </div>

                    <div class="form-group">
                        <div class="input-group">                            
                            <input type="text" class="form-control" name="address" id="address" placeholder="<?php echo $this->lang->line('AddDoctorPAddress'); ?>">
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        <span class="help-block has-error" data-error='0' id="address-error"></span>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" name="email" id="email" placeholder="<?php echo $this->lang->line('AddDoctorPEmail'); ?>">
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        <span class="help-block has-error" id="email-error"></span>
                    <input type="hidden" class="form-control" name="latitude" id="latitude" value=""/>
                    <input type="hidden" class="form-control" name="longtitude" id="longtitude" value=""/>
                    </div>
                      <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="<?php echo $this->lang->line('AddDoctorPPhone'); ?>">
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        <span class="help-block has-error" data-error='0' id="phone-error"></span>
                      </div>
                      
                    <div class="form-group">
                        <div class="input-group mendatary">
                         <!-- <select id="location" name="location" class="form-control">
                          <option value="">--- Select <?php echo $this->lang->line('AddDoctorPLocation'); ?> ---</option>
                          <?php foreach ($location as $locationRow) { ?>
                            <option value="<?php echo $locationRow->location_id;?>"><?php if($siteLang=='arabic'){echo $locationRow->name; }else{echo $locationRow->name_en;}?></option>
                          <?php } ?>
                          </select>-->
                          <?php echo getChildsDoctor(); ?>
                          <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                    </div>

                      <div class="form-group">
                        <div style="float:left;font-weight:bold;"><?php echo $this->lang->line('AddDoctorSpecialityLabel'); ?></div>
                        <div class="input-group mendatary">
                          <select id="speciality" name="speciality" class="form-control" >
                            <option value=""><?php echo $this->lang->line('AddDoctorPSpeciality'); ?></option>                          
                             <?php foreach ($speciality as $specialityRow) { ?>
                            <!--<option value="<?php echo $specialityRow->specialties_id;?>" style="background-image:url('<?php echo base_url(); ?>images/icon/speciality/<?php echo $specialityRow->specialties_id.".png"; ?>'); background-repeat: no-repeat; "><?php if($siteLang=='arabic'){ echo $specialityRow->name;}else{echo $specialityRow->name_en; } ?></option>-->
                            <option value="<?php echo $specialityRow->specialties_id;?>" data-class="avatar2" data-style="background-image: url(&apos;<?php echo base_url(); ?>images/icon/speciality/<?php echo $specialityRow->specialties_id.".png"; ?>&apos;);" ><?php if($siteLang=='arabic'){ echo $specialityRow->name;}else{echo $specialityRow->name_en; } ?></option>
                              
                            <?php } ?>
                          </select>
                          <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        </div>

                      <div class="form-group">
                        <div style="float:left;font-weight:bold;"><?php echo $this->lang->line('AddDoctorPWork'); ?></div>
                        <div class="input-group mendatary">                          
                          <select id="work" name="work" class="form-control" > 
                            <!--<option value=""><?php echo $this->lang->line('AddDoctorPWork'); ?></option>-->                          
                             <?php foreach ($work as $workRow) { ?>
                            <option value="<?php echo $workRow->work_id;?>"><?php if($siteLang=='arabic'){echo $workRow->name; }else{echo $workRow->name_en;} ?></option>
                            <?php } ?>
                          </select>
                          <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                      </div>

                        <div class="form-group">
                        <div class="input-group mendatary">                          
                          <select id="gender" name="gender" class="form-control">
                          <option value="">--- <?php echo $this->lang->line('AddDoctorPGender'); ?> --- </option>
                             <option value="1"><?php echo $this->lang->line('AddDoctorMale'); ?></option>
                             <option value="2"><?php echo $this->lang->line('AddDoctorFemale'); ?></option>
                          </select>
                          <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        </div>                        

                    <br>
                    <div class="clearfix"></div>
                      <button type="submit" id="adddonctors_btn" class="btn btn-block bt-login"><?php echo $this->lang->line('AddPopGeneralSubmit'); ?></button>
                      <div class="clearfix"></div>
                  </form>
                  </div>
            </div>
            </div>
            </div>
            </div>
        </div>
     </div>
  </div>
<div class="modal fade loginModal addDoctorsModal" id="addHospital-model" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content login-modal">

            <div class="modal-body">
              <div class="text-center">
                <div role="tabpanel" class="login-tab addDoctors-tab">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation"> <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <?php echo $this->lang->line('AddHospitalPTitle'); ?>
                  </li>               
                </ul>
                <!-- Tab panes -->
              <div class="tab-content">
                  
                  <div role="tabpanel" class="tab-pane active text-center" id="addDoctorsfrm">                  
                    <form action="<?php echo base_url(); ?>home/AddHospital" method="post" id="AddHospital">  
                     <div class="form-group">
                        <div class="input-group">
                          <input type="text" class="form-control" name="name_en" id="name_en" placeholder="<?php echo $this->lang->line('AddWorkPTitleName'); ?>">
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        <span class="help-block has-error" data-error='0' id="name_en-error"></span>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                          <input type="text" class="form-control" name="emailHospital" id="emailHospital" placeholder="<?php echo $this->lang->line('AddWorkPTitleEmail'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" name="phoneHospital" id="phoneHospital" placeholder="<?php echo $this->lang->line('AddWorkPTitleContact'); ?>" maxlength="13" minlength="10">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group mendatary">
                         <!-- <select id="locationwork" name="locationwork" class="form-control flip text-left">
                          <option value=""><?php echo $this->lang->line('AddWorkPTitleLocation'); ?></option>
                          <?php foreach ($location as $locationRow) { ?>
                            <option value="<?php echo $locationRow->location_id;?>"><?php if($siteLang=='arabic'){echo $locationRow->name; }else{ echo $locationRow->name_en;}?></option>
                          <?php } ?>
                          </select>-->
                            
                            <?php echo getChildsWork(); ?>
                            
                          <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                      </div>

                    <div class="form-group">
                        <div class="input-group">                            
                            <input type="text" class="form-control" name="addresswork" id="addresswork" placeholder="<?php echo $this->lang->line('AddWorkPAddress'); ?>">
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        <span class="help-block has-error" data-error='0' id="addresswork-error"></span>
                    </div>
                    
                    <br>
                    <div class="clearfix"></div>
                      <button type="submit" id="adddonctors_btn" class="btn btn-block bt-login"><?php echo $this->lang->line('AddPopGeneralSubmit'); ?></button>
                      <div class="clearfix"></div>
                  </form>
                  </div>
            </div>
            </div>
            </div>
            </div>
        </div>
     </div>
  </div>


  <div class="modal fade loginModal addDoctorsModal" id="addClinic-model" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content login-modal">

            <div class="modal-body">
              <div class="text-center">
                <div role="tabpanel" class="login-tab addDoctors-tab">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation"> <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <?php echo $this->lang->line('AddClinicPTitle'); ?>
                  </li>               
                </ul>
                <!-- Tab panes -->
              <div class="tab-content">
                  
                  <div role="tabpanel" class="tab-pane active text-center" id="addDoctorsfrm">                  
                    <form action="<?php echo base_url(); ?>home/AddClinic" method="post" id="AddClinic">  
                     <div class="form-group">
                        <div class="input-group">
                          <input type="text" class="form-control" name="nameClinic" id="nameClinic" placeholder="<?php echo $this->lang->line('AddWorkPTitleName'); ?>">
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        <span class="help-block has-error" data-error='0' id="nameClinic-error"></span>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                          <input type="text" class="form-control" name="emailClinic" id="emailClinic" placeholder="<?php echo $this->lang->line('AddWorkPTitleEmail'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" maxlength="13" minlength="10" class="form-control" name="phoneClinic" id="phoneClinic" placeholder="<?php echo $this->lang->line('AddWorkPTitleContact'); ?>">
                        </div>
                    </div>

                    <!--<div class="form-group">
                        <div class="input-group">
                          <label class="flip pull-left m10-l-r"><?php //echo $this->lang->line('AddPopIsprivate'); ?>  </label>
                           <input type="checkbox" class="" name="isPrivate" id="isPrivate" placeholder="<?php echo $this->lang->line('AddPopIsprivate'); ?>" value="1"/>
                        </div>
                    </div>-->

                    <div class="form-group">
                        <div class="input-group mendatary">
                          <!--<select id="locationClinic" name="locationClinic" class="form-control">
                          <option value=""><?php echo $this->lang->line('AddWorkPTitleLocation'); ?></option>
                          <?php foreach ($location as $locationRow) { ?>
                            <option value="<?php echo $locationRow->location_id;?>"><?php if($siteLang=='arabic'){echo $locationRow->name;}else{echo $locationRow->name_en; } ?></option>
                          <?php } ?>
                          </select>-->
                            <?php echo getChildsClinic(); ?>
                          <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                      </div>

                    <div class="form-group">
                        <div class="input-group">                            
                            <input type="text" class="form-control" name="addressClinic" id="addressClinic" placeholder="<?php echo $this->lang->line('AddWorkPAddress'); ?>">
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        <span class="help-block has-error" data-error='0' id="addressClinic-error"></span>
                    </div>
                    
                    <br>
                    <div class="clearfix"></div>
                      <button type="submit" id="adddonctors_btn" class="btn btn-block bt-login"><?php echo $this->lang->line('AddPopGeneralSubmit'); ?></button>
                      <div class="clearfix"></div>
                  </form>
                  </div>
            </div>
            </div>
            </div>
            </div>
        </div>
     </div>
  </div>

  <div class="modal fade loginModal addDoctorsModal" id="addLab-model" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content login-modal">

            <div class="modal-body">
              <div class="text-center">
                <div role="tabpanel" class="login-tab addDoctors-tab">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation"> <button type="button" class="close" data-dismiss="modal">&times;</button>
                   <?php echo $this->lang->line('AddLabPTitle'); ?>
                  </li>               
                </ul>
                <!-- Tab panes -->
              <div class="tab-content">
                  
                  <div role="tabpanel" class="tab-pane active text-center" id="addDoctorsfrm">                  
                    <form action="<?php echo base_url(); ?>home/AddLab" method="post" id="AddLab">  
                     <div class="form-group">
                        <div class="input-group">
                          <input type="text" class="form-control" name="nameLab" id="nameLab" placeholder="<?php echo $this->lang->line('AddWorkPTitleName'); ?>">
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        <span class="help-block has-error" data-error='0' id="nameLab-error"></span>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                          <input type="text" class="form-control" name="emailLab" id="emailLab" placeholder="<?php echo $this->lang->line('AddWorkPTitleEmail'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                          <input  maxlength="13" minlength="10" type="text" class="form-control" name="phoneLab" id="phoneLab" placeholder="<?php echo $this->lang->line('AddWorkPTitleContact'); ?>">
                        </div>
                    </div>
                        <div class="form-group">
                        <div class="input-group mendatary">
                            <select class = "form-control dropdown" name="labtype" id="labtype">
                           <option value=""><?php echo $this->lang->line('LabType'); ?></option>
                           <option value="Radiology Lab"><?php echo $this->lang->line('radiologylab'); ?></option>
                           <option value="Medical Lab"><?php echo $this->lang->line('medicallab'); ?></option>
                        </select>
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        </div>
                        
                    <div class="form-group">
                        <div class="input-group mendatary">                         
                             <?php echo getChildsLab(); ?>
                          <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                      </div>

                    <div class="form-group">
                        <div class="input-group">                            
                            <input type="text" class="form-control" name="addressLab" id="addressLab" placeholder="<?php echo $this->lang->line('AddWorkPAddress'); ?>">
                            <div class="input-group-addon mendatary-ﬁeld flip text-left"><i class="fa fa-star"></i></div>
                        </div>
                        <span class="help-block has-error" data-error='0' id="addressLab-error"></span>
                    </div>
                    <br>
                    <div class="clearfix"></div>
                      <button type="submit" id="adddonctors_btn" class="btn btn-block bt-login"><?php echo $this->lang->line('AddPopGeneralSubmit'); ?></button>
                      <div class="clearfix"></div>
                  </form>
                  </div>
            </div>
            </div>
            </div>
            </div>
        </div>
     </div>
  </div>
  <script>   
  /*$("form").submit(function() {
            $.ajax({
            type: 'POST', 
            url: "<?php echo base_url();?>home/latlong/",
            data: { 'address': $('#address').val()},
            success: function(response){
              var data = JSON.parse(response);
               $("#latitude").append().val(data.lat);
               $("#longtitude").append().val(data.lng);
            }
            });
        });*/
    function doctorloginpopup()
    {
        <?php // / if() ?>
        alert('<?php echo $this->lang->line('Doctorerrormsg'); ?>');
        return false;
    }
    function hospitalloginpopup()
    {
         alert('<?php echo $this->lang->line('Hospitalerrormsg'); ?>');
        return false;
    }
    function clinicloginpopup()
    {
         alert('<?php echo $this->lang->line('Clinicerrormsg'); ?>');
        return false;
    }
    function labloginpopup()
    {
        alert('<?php echo $this->lang->line('Laberrormsg'); ?>');
        return false;
    }
  $(document).ready(function () {
        jQuery.validator.addMethod("mobile_no", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid contact no.');

        jQuery.validator.addMethod("email", function (value, element) {
            return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
        }, 'Please enter a valid email address.');

        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
$.ajax({
            type: 'POST', 
            url: "<?php echo base_url();?>home/latlong/",
            data: { 'address': $('#address').val()},
            success: function(response){
              var data = JSON.parse(response);
               $("#latitude").append().val(data.lat);
               $("#longtitude").append().val(data.lng);
            }
            });
        $("#AddDoctor").validate({
            rules: {
                firstname:{ required: true,
                            character:true,
                          },
                lastname: { required: true,
                            character:true,
                          },
                address: "required",
                email:{ required: true,
                        email: true,
                        remote: {
                          url: "<?php echo base_url(); ?>home/check_user_email",
                          type: "post",
                          data: {
                            email_id: function () {
                              return $("#email").val();
                              },
                            }
                          }
                        },
                phone:{
                        required: true,
                        mobile_no:true,
                        maxlength:11,
                        minlength:10,
                },
                work:"required", 
                location: "required",
                speciality: "required",
                gender: "required",
            },
            messages: {
                firstname: {required:"Enter first name",
                            character:"Please enter only aphabetics"
                           },
                lastname:{  required:"Enter last name",
                            character:"Please enter only aphabetics"
                           },
                address:"Enter address",
                email: {
                    required: "Enter email id",
                    email: "Enter valid email id",
                    remote: "Email id already exists",
                },
                phone:{ required:"Enter phone",
                        character:"Please enter only number",
                        maxlength:"Please enter only 10 number",
                           },
                work: "Select work",
                location: "Select location",
                speciality: "Select specialties",
                gender:"Select gender",
            }
        });
    });




// For Add Hospital 

$(document).ready(function () {
        jQuery.validator.addMethod("emailHospital", function (value, element) {
            return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
        }, 'Please enter a valid email address.');

        jQuery.validator.addMethod("phoneHospital", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid contact no.');

        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');

        $("#AddHospital").validate({
            rules: {
                name_en:{ required: true,
                          character:true,
                          remote: {
                          url: "<?php echo base_url(); ?>home/checkDuplicateWork",
                          type: "post",
                          data: {
                            workName: function () {
                              return $("#name_en").val();
                              },
                            }
                          }
                        },
                emailHospital:{email:true},
                phoneHospital:{phoneHospital:true,
                               maxlength:11},
                addresswork: "required",
                locationwork: "required",
            },
            messages: {
                name_en:{ required:"Enter name",
                          character:"Please enter only aphabetics",
                          remote: "This hospital already exists",
                        },
                emailHospital:{emailHospital:"Please enter valid email"},
                phoneHospital:{phoneHospital:"Please enter valid phone",
                              maxlength:"Please enter only 10 number"  },
                addresswork:"Enter address",
                locationwork: "Select location",
            }
        });
    });

// For Add Clinic 

$(document).ready(function () {
         jQuery.validator.addMethod("emailClinic", function (value, element) {
            return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
        }, 'Please enter a valid email address.');

         jQuery.validator.addMethod("phoneClinic", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid contact no.');

        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');

        $("#AddClinic").validate({
            rules: {
                nameClinic: { required: true,
                              character:true,
                              remote: {
                              url: "<?php echo base_url(); ?>home/checkDuplicateWork",
                              type: "post",
                              data: {
                              workName: function () {
                                return $("#nameClinic").val();
                              },
                              }
                            }
                          },
                emailClinic:{email:true},
                phoneClinic:{phoneClinic:true,
                              maxlength:11},
                addressClinic: "required",
                locationClinic: "required",
            },
            messages: {
                nameClinic: {required:"Enter name",
                            character:"Please enter only aphabetics",
                            remote: "This clinic already exists",
                           },
                phoneClinic:{phoneClinic:"Please enter valid phone",
                             maxlength:"Please enter only 10 number" },           
                addressClinic:"Enter address",
                locationClinic: "Select location",
            }
        });
    });
// For Add Lab 

$(document).ready(function () {
      jQuery.validator.addMethod("emailLab", function (value, element) {
            return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
        }, 'Please enter a valid email address.');

        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');

        jQuery.validator.addMethod("phoneLab", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid contact no.');


        $("#AddLab").validate({
            rules: {
                nameLab:{ required: true,
                            character:true,
                            remote: {
                              url: "<?php echo base_url(); ?>home/checkDuplicateWork",
                              type: "post",
                              data: {
                              workName: function () {
                                return $("#nameLab").val();
                              },
                              }
                            }
                          },
                emailLab:{email:true},
                phoneLab:{phoneLab:true,
                          maxlength:11},
                addressLab: "required",
                locationLab: "required",
                labtype: "required",
            },
            messages: {
                nameLab: {required:"Enter name",
                            character:"Please enter only aphabetics",
                            remote: "This lab already exists",
                           },
                phoneLab:{phoneLab:"Please enter valid phone",
                             maxlength:"Please enter only 10 number" },
                addressLab:"Enter address",
                locationLab: "Select location",
                labtype: "Select Lab Type",
            }
        });
    });
  </script>

 