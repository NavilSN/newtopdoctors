<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends MY_Model {

    protected $primary_key = 'admin_id';
    

    public function get_users() {
        return $this->db->select()
                        ->from('user')
                        ->join('role', 'role.role_id = user.role_id')
                        ->where_not_in('role_name', [
                            'Student', 'Professor'
                        ])->get()->result();
    }
    
    function get_user() {
        return $this->db->select()
                        ->from('user')
                        ->join('role', 'role.role_id = user.role_id')
                        ->where('user_id',$this->session->userdata('user_id'))->get()->result();
    }

    function password_update($id,$data) {
        $this->db->where('user_id',$id);
        $this->db->update('user',$data);
    }
    
    function profile_update($id,$data) {
        $this->db->where('user_id',$id);
        $this->db->update('user',$data);
    }
}
