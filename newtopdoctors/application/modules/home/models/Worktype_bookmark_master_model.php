<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Worktype_bookmark_master_model extends MY_Model {

    protected $primary_key = 'worktype_bookmark_id';

}
