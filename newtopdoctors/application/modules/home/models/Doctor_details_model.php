<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor_details_model extends MY_Model {
    protected $primary_key = 'doctor_id';
    
    protected function timestamps($doctor) {
        return $doctor;
    }
}
