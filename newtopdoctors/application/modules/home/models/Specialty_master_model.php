<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Specialty_master_model extends MY_Model {

    protected $primary_key = 'specialties_id';
    public function get_all_speciality()
    {
        $this->db->order_by('name_en','ASC');
        return $this->db->get('specialty_master')->result();
    }
}
