<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Location_master_model extends MY_Model {

    protected $primary_key = 'location_id';
    
    
    public function get_all_location_by_name()
    {
        $this->db->order_by('name_en','ASC');
        return $this->db->get('location_master')->result();
    }

    
}
