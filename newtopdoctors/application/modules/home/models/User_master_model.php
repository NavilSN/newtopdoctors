<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_master_model extends MY_Model {

    protected $primary_key = 'user_id';
    
    
    public function get_total_user_dorctor_review($user_id)
    {
        $this->db->select();
        $this->db->from('user_master um');
        $this->db->join('doctors_rating dr','dr.user_id=um.user_id','left');
        //$this->db->join('work_rating wr','wr.user_id=um.user_id','right');
        $this->db->where('um.user_id',$user_id);        
        return $this->db->get()->num_rows();
        //echo $this->db->last_query();
        
    }
      public function get_total_user_work_review($user_id)
    {
        $this->db->select();
        $this->db->from('user_master um');
        
        $this->db->join('work_rating wr','wr.user_id=um.user_id','left');
        $this->db->where('um.user_id',$user_id);        
        return $this->db->get()->num_rows();
        //echo $this->db->last_query();
        
    }


    /**
    check duplicate email in newsletter

    */
    public function check_newsletter_email($email)
    {
        $this->db->where('email',$email);
        return $this->db->get('newsletter')->result();
        
    }

    public function add_email_to_newsletter($email)
    {
         $this->db->insert('newsletter',array("email"=>$email));
    }


    
    public function unsubscribe_user($email)
    {
        $this->db->where('email',$email);
        $this->db->delete('newsletter');
    }

    
    
}
