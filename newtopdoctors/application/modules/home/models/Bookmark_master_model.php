<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bookmark_master_model extends MY_Model {

    protected $primary_key = 'bookmark_id';

}
