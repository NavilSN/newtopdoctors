<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor_master_model extends MY_Model {
    protected $primary_key = 'doctor_id';
}
