<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Country_master_model extends MY_Model {

    protected $primary_key = 'country_id';
}
