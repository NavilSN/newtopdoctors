<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Work_master_model extends MY_Model {
    protected $primary_key = 'work_id';
     public function get_all_work()
    {
         $this->db->where('status','1');
        $this->db->order_by('name_en','ASC');
        return $this->db->get('work_master')->result();
    }
}
