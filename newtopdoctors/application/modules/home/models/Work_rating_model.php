<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Work_rating_model extends MY_Model {

    protected $primary_key = 'work_rating_id';
     public $before_create = array('timestamps');

    /**
     * Set timestamp field
     * @param array $data
     * @return array
     */
    protected function timestamps($data) {
        $data['date_created'] = date('Y-m-d H:i:s');        
        return $data;
    }
    
    
}
