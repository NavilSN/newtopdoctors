<style type="text/css">
.contact-details h3 {
	color: #595959;
    font-size: 24px;
    font-weight: 300;
    margin-bottom: 15px;
	line-height: 1em;
    text-transform: uppercase;
}

.contact-details ul {
	margin: 0;
	padding: 0;
	list-style: none;	
}

.contact-details ul li {
	margin: 0;
	line-height: 28px;
	padding: 0;	
}

#contact-form {
	margin-bottom: 0;	
}

#contact-form p {
	margin-bottom: 1px;	
}

#contact-form input,
#contact-form textarea {
    border: none;
	-webkit-border-radius: 0;
	-moz-border-radius: 0;
    border-radius: 0;	
    -webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;	
	background: #cce9ec;
    color: #053b61;
    font-size: 16px;
    height: auto;
    padding: 15px;
	margin: 0;	
    resize: none;
}
#contact-form input::-moz-placeholder,
#contact-form textarea::-moz-placeholder
{ color: #053b61 !important; }
#contact-form input::-webkit-input-placeholder,
#contact-form textarea::-webkit-input-placeholder
{ color: #053b61 !important; }
#contact-form input {
    width: 100%;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

#contact-form textarea {
    width: 100%;
	resize: vertical;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

#contact-form .submit {
    background: #3C3F45;
    color: #23262C;
    cursor: pointer;
    display: inline-block;
    font-size: 18px;
    font-weight: 500;
    padding: 16px 40px;
    text-align: center;
    vertical-align: middle;
    width: auto;
	margin-top: 30px;
	
	-webkit-transition: background 0.1s linear 0s, color 0.1s linear 0s;	
	   -moz-transition: background 0.1s linear 0s, color 0.1s linear 0s;
		 -o-transition: background 0.1s linear 0s, color 0.1s linear 0s;
		    transition: background 0.1s linear 0s, color 0.1s linear 0s;
}

#contact-form .submit:hover {
    background: #DE5E60;
    color: #FFFFFF;
}

#response {
	margin-top: 20px;
	color: #FFFFFF;
}
.mt83{ margin-top: 83px !important; }
#contact h2{ color: #053b61; }
.title-description, .contact-details ul li {
  color: #000;
  font-weight: normal;
  font-family: 'Exo-medium';
}

</style>





<!-- Contact Section -->
<div id="contact" class="mt83 page">
<div class="container">
    <!-- Title Page -->
    <div class="row">
        <div class="w-col w-col-12 text-center">
            <div class="title-page title-spacing">
                <h2 class="title"><?php //echo $this->lang->line('getintouch'); ?></h2>
                <!--<h3 class="title-description">We’re currently accepting new client projects. We look forward to serving you.</h3>-->
            </div>
        </div>
    </div>
    <!-- End Title Page -->
    
    <!-- Contact Form -->
    <div class="row">
        <div class="w-col w-col-6 text-right">
        
            <form id="contact-form" class="contact-form" action="<?php echo base_url(); ?>contactus/sendmail" method="post">
                <p class="contact-name">
                    <input id="contact_name" type="text" placeholder="<?php echo $this->lang->line('FullName'); ?>" value="" name="fullname" required/>
                </p>
                <p class="contact-email">
                    <input id="contact_email" type="text" placeholder="<?php echo $this->lang->line('emailaddress'); ?>" value="" name="email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Please enter valid email"/>
                </p>
                <p class="contact-message">
                    <textarea id="contact_message" placeholder="<?php echo $this->lang->line('YourMsg'); ?>" name="message" rows="5" cols="40" required></textarea>
                </p>
              <!--   <p class="contact-submit">
                    <a id="contact-submit" class="submit" href="#">Send Your Email</a>
                </p> -->
                <p> <br>
                <button type="submit" id="searchData" name="searchData" class="btn btn-default search-btn"><?php echo $this->lang->line('message'); ?></button>
                </p>
                <div id="response">
                
                </div>
            </form>
         
        </div>
        
        <div class="w-col w-col-6 flip text-left">
            <div class="contact-details">
            <h3 class="flip text-left clearfix"><?php echo $this->lang->line('contactdetails'); ?></h3>
                <ul class="mb10">
                    <li ><img src="<?php base_url(); ?>images/msg-icon.png" width="3%" /> <a href="#">info@topdoctors.me</a></li>
                    <li ><img src="<?php base_url(); ?>images/tel-icon.png" width="3%" />  02-27929540</li>
                    <li ><img src="<?php base_url(); ?>images/mobile-icon-new.png" width="2%" /> 0102-111-2974</li>
                    <li >
                        <?php echo $this->lang->line('topdoctorsLine'); ?>
                                             
                       <?php //echo $this->lang->line('tophome'); ?>
                        <br>
                        <?php echo $this->lang->line('cairoLang'); ?>
                        <br>
                        <?php echo $this->lang->line('egyptline'); ?>
                        
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Contact Form -->
</div>
</div>
<script>
$(document).ready(function () {
      /* Validation for Login   */
      jQuery.validator.addMethod("email_id", function (value, element) {
            return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
        }, 'Please enter a valid email address.');

        $("#contact-form").validate({
            rules: {
                fullname:"required",
                email:{ required:true,
                        email: true
                    },
                message:"required"
                
                },
            messages: {
                fullname: {required: "Enter fullname"},
                email:{ required:"Enter email",
                        email: "Enter valid email"
                    },
                message: {required: "Enter message"}
                
            }
        });
    });
</script>
<!-- End Contact Section -->
