<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('email');
	$this->data['slug_class'] = "contactus";
    }

    function index() {
        
        $this->data['title']='TopDoctors';
        $this->__templateFront('contactus/index', $this->data);
	$this->data['slug_class'] = "contactus";
    }
    function sendmail(){

    	$this->email->set_newline("\r\n");
        $this->email->from('Contact@topdoctors.com', 'Top doctor');
        $this->email->to($_POST['email']);
        $this->email->subject('Inquiry for topdoctors');
        //$data['firstname'] = $_POST['fullname'];
        //$data['email']     = $_POST['email'];
        $msg='Hello, '.$_POST['fullname'].'<br/>';
        $msg .='Email : '.$_POST['email'].'<br/><br/><br/>';
        $msg.='Message : '.$_POST['message'].'<br/>';
        //$msg = $this->load->view('template/email_template',$data,TRUE);
        $this->email->message($msg);
        $this->email->send();

        $this->flash_notification('Your request is successfully sent.');

        redirect(base_url('contactus'));
    }
    
}
