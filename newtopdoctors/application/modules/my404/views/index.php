<div class="search-full-box">
<section class="text-center clearfix">
  
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-12 text-left">
                <form class="form-inline" action="<?php echo base_url();?>search/globalSearch" method="post">
                  <div class="form-group">
                    <span class="flip search-icon"> <i class="fa fa-search"></i></span>                    
                    <input type="text" name="searchname" class="form-control" id="" placeholder="<?php //echo $this->lang->line('searchPlaceHolder'); ?>" value="<?php  if($typeofsearch=="globalsearch"){ echo $searchname; } ?>">
                  </div>
                  <div class="form-control-btn-box">
                    <button type="submit" name="searchData" class="btn btn-default search-btn"><?php echo $this->lang->line('searchbtn'); ?></button>                   
                  </div>
                </form>
            </div>
          </div>          
      </div>
  </section>
</div>
<div class="filters-box m50t">
  <section class="text-center clearfix">
      
          <div class="w-row">
            <div class="w-col w-col-12 text-left">
            <div class="border-divider fullwidth"></div>
             <div class="filter-show-data clearfix">
                  <form class="form-horizontal" role="form">   
                    <div class="form-group">
                                             <!--<div><h1 style="color:#dedede;"><img src="http://192.168.1.36/topdoctor/images/no-record-found-new.jpg"></h1></div> -->
                          <div><h1 style="color:#dedede;">Error 404 : Page not found </h1></div> 
                                          </div>
                    <div class="col-sm-12 col-xs-12">
                          <ul class="pagination"></ul>
                      </div>
                  </form>
             </div>
            </div>
            <div class="clearfix"></div>
            </div>
      
      </section></div>