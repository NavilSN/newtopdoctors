<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class My404 extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        //if()
        
        $this->output->set_status_header('404'); // setting header to 404
        $this->data['title']='TopDoctors';
        $this->__templateFront('my404/index', $this->data);
    }
    
}