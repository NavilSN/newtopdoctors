<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('memory_limit', '-1');
class Result_model extends MY_Model {

    /**
     * Doctor result count search crieteria
     * @param mixed $data
     * @return type int
     */
    function result_count($data)
    {
        
                $siteLang=$this->session->userdata('site_lang');
                if(!empty($data['location']))
                    {     
                         $locations =getChildLocation($data['location']);
                        $this->db->where_in('doctor_details.location_id',$locations);
                        //$this->db->where('doctor_details.location_id',$data['location']);
                    }
                  if(!empty($data['speciality']))
                    {                        
                        $this->db->where('doctor_details.speciality_id',$data['speciality']);
                    }
                     
                    if(!empty($data['gender']))
                    {                    
                        $this->db->where('doctor_details.gender',$data['gender']);
                    }
                    
                    if(!empty($data['searchname']))
                    {
                       if($siteLang=='arabic'){
                         $search = trim($data['searchname']);
                                /*$search = str_replace(' ', '', $data['searchname']);
                                $patterns     = array( "/(ا|أ|آ|إ)/","/(هـ|ة|ه)/", "/(ئـ|ئ)/","/(ؤ|و)/","/(ا|ى|ي)/"); 
                                $replacements = array( "[ا|أ|آ|إ]","[هـ|ة|ه]","[ئـ|ئ]","[ؤ|و]","[ا|ى|ي]" );   
                                $query_string = preg_replace($patterns, $replacements, $search);*/

                                /* 13-07-2017 commented by mayur panchal*/
                       /* $this->db->where("replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(concat_ws(' ',doctor_master.first_name,doctor_master.last_name), 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%'");*/
                       $this->db->where("concat_ws(' ',doctor_master.first_name,doctor_master.last_name) LIKE '%".$search."%'");                        
                       
                        }else{                 

                            $this->db->where(" concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) LIKE '%".$this->db->escape_str(trim($data['searchname']))."%'");
                            
                        }
                    } 
            
                    $this->db->select('doctor_details.photo,doctor_details.phone_number AS phone,doctor_details.doctor_id AS ID,doctor_details.google_map_latitude,doctor_details.google_map_longtude,(SELECT IFNULL(AVG(doctors_rating.average_score),0) as average_score FROM doctors_rating WHERE doctor_details.doctor_id=doctors_rating.doctor_id ) as averageReview');

                    if($siteLang=='arabic')
                    {
                        $this->db->select('CONCAT(doctor_master.first_name, " ", doctor_master.last_name) AS Name,doctor_master.address as doctorAddress');
                    }else{
                        $this->db->select('CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS Name,doctor_master_en.address as doctorAddress');
                    }
                $this->db->where('doctor_details.visibility','1');
                $this->db->join('location_master', 'doctor_details.location_id=location_master.location_id','left');
                $this->db->join('specialty_master','doctor_details.speciality_id=specialty_master.specialties_id','left');
                $this->db->join('doctor_master_en', 'doctor_details.doctor_id=doctor_master_en.doctor_id','left');
                $this->db->join('doctor_master', 'doctor_details.doctor_id=doctor_master.doctor_id','left');    
                $this->db->join('doctors_rating', 'doctors_rating.doctor_id=doctor_details.doctor_id','left');   
                // order by top rated first 
               $this->db->order_by('averageReview','DESC');   
               $this->db->group_by('doctor_details.doctor_id');                             
                 return  $this->db->get('doctor_details')->num_rows();
                   

                 
               
                
    }
    
   
      /**
     * Doctor result  search crieteria
     * @param mixed $data
     * @return type mixed array
     */       
    function fetch_doctors($perpage,$page,$data)
    {
            $siteLang=$this->session->userdata('site_lang');
            
             if(!empty($data['location']))
                    {     
                        $locations =getChildLocation($data['location']);
                        $this->db->where_in('doctor_details.location_id',$locations);                    
                    }
                    
                     if(!empty($data['speciality']))
                    {                        
                        $this->db->where('doctor_details.speciality_id',$data['speciality']);
                    }
                    if(!empty($data['gender']))
                    {                    
                        $this->db->where('doctor_details.gender',$data['gender']);
                    }
                   
                    if(!empty($data['searchname']))
                    {
                        if($siteLang=='arabic'){
                            $search = trim($data['searchname']);
                             $this->db->where("concat_ws(' ',doctor_master.first_name,doctor_master.last_name) LIKE '%".$search."%'"); 
                             /*13-07-2017 commented by mayur panchal*/
/*                            $this->db->where("replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(concat_ws(' ',doctor_master.first_name,doctor_master.last_name), 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%'");*/                            
                        
                        }else{
                            
                           $this->db->where(" concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$this->db->escape_str(trim($data['searchname']))."%'");
                            
                        }
                        
                    } 
            
                    $this->db->select('doctor_details.photo,doctor_details.phone_number AS phone,doctor_details.doctor_id AS ID,doctor_details.google_map_latitude,doctor_details.google_map_longtude,doctor_details.speciality_id,"doctor" AS work_type,(SELECT IFNULL(AVG(doctors_rating.average_score),0) as average_score FROM doctors_rating WHERE doctor_details.doctor_id=doctors_rating.doctor_id ) as averageReview');

                    if($siteLang=='arabic')
                    {
                        $this->db->select('CONCAT(doctor_master.first_name, " ", doctor_master.last_name) AS Name,doctor_master.address as doctorAddress');
                    }else{
                        $this->db->select('CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS Name,doctor_master_en.address as doctorAddress');
                    }
                $this->db->where('doctor_details.visibility','1');
                $this->db->join('location_master', 'doctor_details.location_id=location_master.location_id','left');
                $this->db->join('specialty_master','doctor_details.speciality_id=specialty_master.specialties_id','left');
                $this->db->join('doctor_master_en', 'doctor_details.doctor_id=doctor_master_en.doctor_id','left');
                $this->db->join('doctor_master', 'doctor_details.doctor_id=doctor_master.doctor_id','left');    
                $this->db->join('doctors_rating', 'doctors_rating.doctor_id=doctor_details.doctor_id','left');   
                // order by top rated first 
               $this->db->order_by('averageReview','DESC');
               $this->db->group_by('doctor_details.doctor_id');                
                $this->db->limit($perpage,$page);
              return   $this->db->get('doctor_details')->result();                
               /* echo $this->db->last_query();    die;*/
                
       
    }
     /**
     * Work result count search crieteria
     * @param mixed $data
     * @return type int
     */
   function result_work($data)
   {
            $siteLang=$this->session->userdata('site_lang');
                    if(!empty($data['location']))
                    {     
                        $locations =getChildLocation($data['location']);
                        $this->db->where_in('work_master.location_id',$locations);
                        //$this->db->where('work_master.location_id',$data['location']);
                    }
                    if(!empty($data['searchname']))
                    {
                        if($siteLang=='arabic')
                        {
                            $search = $data['searchname'];
                            /*$search = str_replace(' ', '', $data['searchname']);
                                $patterns     = array( "/(ا|أ|آ|إ)/","/(هـ|ة|ه)/", "/(ئـ|ئ)/","/(ؤ|و)/","/(ا|ى|ي)/"); 
                                $replacements = array( "[ا|أ|آ|إ]","[هـ|ة|ه]","[ئـ|ئ]","[ؤ|و]","[ا|ى|ي]" );   
                                $query_string = preg_replace($patterns, $replacements, $search);
                        $this->db->where("work_master.name REGEXP '".$query_string."'");  */                
                              //  commented by mayur panchal 13-07-2017
                       /*  $this->db->where("replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(work_master.name, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%'");    */
                         $this->db->where("work_master.name LIKE '%".$search."%'");    
                        
                        }else{
                            $this->db->where(" work_master.name_en LIKE '%".$this->db->escape_str(trim($data['searchname']))."%'"); 
                        }
                    } 
                    $this->db->where('work_master.status','1');

                    $this->db->select('work_master.photo,work_master.phone AS phone,doctor_details.doctor_id,work_master.work_type,work_master.work_id AS ID,work_master.work_latitude as google_map_latitude, work_master.work_longitude as google_map_longtude, work_master.work_zoom as Zoom,(SELECT IFNULL(AVG(work_rating.average_score),0) as average_score FROM work_rating WHERE work_master.work_id=work_rating.work_id ) as averageReview');
                    if($siteLang=='arabic')
                    {
                        $this->db->select('work_master.name as Name,work_master.address as doctorAddress');
                    }
                    else
                    {
                        $this->db->select('work_master.name_en as Name,work_master.address_en as doctorAddress');
                    }
                    $this->db->join('doctor_details', 'doctor_details.work_id=work_master.work_id','LEFT');
                    $this->db->join('location_master', 'doctor_details.location_id = location_master.location_id','LEFT');
                    if($data['typeofsearch']=="Lab")
                    {
                        if($data['labtype']=="")
                         {
                             $this->db->like('work_master.work_type',"Lab");
                         }
                         else
                         {
                             $this->db->like('work_master.work_type',$data['labtype']);
                         }
                    }
                    else{
                    $this->db->where('work_master.work_type',$data['typeofsearch']);
                    }
                    $this->db->group_by('work_master.work_id');
                    // order by top rated first 
                    $this->db->order_by('averageReview',"DESC");
                    $this->db->join('work_rating','work_master.work_id=work_rating.work_id','left');
                    return $this->db->get('work_master')->num_rows();
                     //echo $this->db->last_query();die;
   }
    /**
     * work result  search crieteria
     * @param mixed $data
     * @return type
     */
   function fetch_work($per_page, $page,$data)
   {    
                    $siteLang=$this->session->userdata('site_lang');
       
       
                    if(!empty($data['location']))
                    {     
                          $locations =getChildLocation($data['location']);
                          $this->db->where_in('work_master.location_id',$locations);                        
                    }
                    if(!empty($data['searchname']))
                    {
                        if($siteLang=='arabic'){
                            $search = $data['searchname'];
                                 /*$search = str_replace(' ', '', $data['searchname']);
                                $patterns     = array( "/(ا|أ|آ|إ)/","/(هـ|ة|ه)/", "/(ئـ|ئ)/","/(ؤ|و)/","/(ا|ى|ي)/"); 
                                $replacements = array( "[ا|أ|آ|إ]","[هـ|ة|ه]","[ئـ|ئ]","[ؤ|و]","[ا|ى|ي]" );   
                                $query_string = preg_replace($patterns, $replacements, $search);
                        $this->db->where("work_master.name REGEXP '".$query_string."'");    */  $this->db->where("work_master.name LIKE '%".$search."%'");    
                        //            commented by mayur panchal 13-07-2017  
                        /*$this->db->where("replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(work_master.name, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%'");   */ 
                        
                        }else{
                            $this->db->where(" work_master.name_en LIKE '%".$this->db->escape_str(trim($data['searchname']))."%'");
                        }
                    } 
                    $this->db->where('work_master.status','1');

                    $this->db->select('work_master.photo,work_master.phone AS phone,doctor_details.doctor_id,work_master.work_type,work_master.work_id AS ID,work_master.work_latitude as google_map_latitude, work_master.work_longitude as google_map_longtude, work_master.work_zoom as Zoom,(SELECT IFNULL(AVG(work_rating.average_score),0) as average_score FROM work_rating WHERE work_master.work_id=work_rating.work_id ) as averageReview'); 
                    if($siteLang=='arabic'){
                        $this->db->select('work_master.name as Name,work_master.address as doctorAddress');
                    }else{
                        $this->db->select('work_master.name_en as Name,work_master.address_en as doctorAddress');
                    }
                    $this->db->join('doctor_details', 'doctor_details.work_id=work_master.work_id','LEFT');
                    $this->db->join('location_master', 'doctor_details.location_id = location_master.location_id','LEFT');
                    
                     if($data['typeofsearch']=="Lab")
                    {
                         if($data['labtype']=="")
                         {
                             $this->db->like('work_master.work_type',"Lab");
                         }
                         else{
                             $this->db->like('work_master.work_type',$data['labtype']);
                         }
                        
                    }
                    else{
                            $this->db->where('work_master.work_type',$data['typeofsearch']);
                    }

                    $this->db->group_by('work_master.work_id');
                    // order by top rated first 27-4-2017 mayur panchal
                    $this->db->order_by('averageReview',"DESC");                    
                    $this->db->join('work_rating','work_master.work_id=work_rating.work_id','left');
                    $this->db->limit($per_page,$page);
                       return $this->db->get('work_master')->result();
                     
                    
                    
   }


   /**
    * Global Search function for merge result
    * @param numeric $perPage
    * @param numeric $page
    * @param mixed array $data
    * @return type mixed array
    */
   function fetch_globalSearch($perPage,$page,$data) {
      $result = array();
                $search = trim($data['search']);
                $lang = $data['lang'];
                $speciality  = $data['speciality'];
                $location = $data['location'];
               /* $doctorMasterGlobalSearchResult = $this->doctorMasterGlobalSearch($search,$lang,$speciality,$location);
                $workMasterGlobalSearchResult = $this->workMasterGlobalSearch($search,$lang,$speciality,$location);
*/
/* Doctor search */
  $siteLang=$this->session->userdata('site_lang');
                    
                    
                    /* Flat Query */

                     $where = "";
                     if(!empty($location))
                    {
                        $locations =getChildLocation($location);
                        $locs = implode(",",$locations);
                        $where .= "(doctor_details.location_id IN($locs)) AND ";
                    }


                    if(!empty($speciality))
                    {
                        $where .= "(doctor_details.speciality_id=".$speciality.") AND ";
                    }
                    
                   
                    $where2 = "";
                    $where3 = "";
                        if($lang=='arabic')
                        {               
                            $select = 'CONCAT(doctor_master.first_name," ",doctor_master.last_name) AS Name,`doctor_master`.`address` as `doctorAddress`';
                            if(!empty($search))
                            {

                                $where .="(concat_ws(' ',doctor_master.first_name,doctor_master.last_name) LIKE '%".$search."%' OR ";
                                $where .="location_master.name LIKE '%".$search."%' OR ";
                                $where .="doctor_master.address LIKE '%".$search."%') AND ";

                                // commented by mayur panchal 13-07-2017                      
                                /*$where .="(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(concat_ws(' ',doctor_master.first_name,doctor_master.last_name), 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%' OR ";
                                $where .="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(location_master.name, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%' OR ";
                                $where .="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(doctor_master.address, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%') AND ";*/
                                   
                           }
                        
                    }
                    else{
                          //$this->db->where("");
                       $select = 'CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS Name, `doctor_master_en`.`address` as `doctorAddress`';   
                         $where .= "(concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$this->db->escape_str(trim($search))."%' OR ";
                         $where .= "location_master.name_en LIKE '%".$this->db->escape_str(trim($search))."%' OR ";
                         $where .="doctor_master_en.address LIKE '%".$this->db->escape_str(trim($search))."%') AND ";
                    } 


                    $where_status .= "doctor_details.visibility='1'";
                    $whereas  = $where.$where_status;

                    
                     $query = "SELECT `doctor_details`.`photo`, `doctor_details`.`phone_number` AS `phone`, `doctor_details`.`doctor_id` as ID,'doctor' as work_type, `doctor_details`.`google_map_latitude`, `doctor_details`.`google_map_longtude`,`doctor_details`.`google_map_zoom` as `Zoom`, (SELECT IFNULL(AVG(doctors_rating.average_score), 0) as average_score FROM doctors_rating WHERE doctor_details.doctor_id=doctors_rating.doctor_id ) as averageReview, $select FROM `doctor_details` LEFT JOIN `location_master` ON `doctor_details`.`location_id`=`location_master`.`location_id` LEFT JOIN `specialty_master` ON `doctor_details`.`speciality_id`=`specialty_master`.`specialties_id` LEFT JOIN `doctor_master_en` ON `doctor_details`.`doctor_id`=`doctor_master_en`.`doctor_id` LEFT JOIN `doctor_master` ON `doctor_details`.`doctor_id`=`doctor_master`.`doctor_id` LEFT JOIN `doctors_rating` ON `doctors_rating`.`doctor_id`=`doctor_details`.`doctor_id` WHERE $whereas GROUP BY doctor_details.doctor_id";
            
                    /* end */
/* Doctor end */


                    
                    
                    /* work Flat Query */

                 
            
                      
                   $where2 = "";
                     if(!empty($location))
                    {
                        $locations =getChildLocation($location);
                        $locs = implode(",",$locations);
                        $where2 .= "(work_master.location_id IN($locs)) AND ";

                    }


                    if(!empty($speciality))
                    {
                        $where2 .= "(work_master.speciality=".$speciality.") AND ";                       
                    }
                    
                    
                        if($lang=='arabic')
                        {
                            $select2 = "work_master.name as Name,work_master.address as doctorAddress";
                            if(!empty($search))
                            {



                                 $where2 .= "(work_master.name LIKE '%".$search."%' OR ";
                                 $where2 .= "work_master.address LIKE '%".$search."%' OR ";
                                 $where2 .="location_master.name LIKE '%".$search."%') AND ";
                                /*  // commented by mayur panchal 13-07-2017                                                   
                                 $where2 .= "(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(work_master.name, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%' OR ";
                                 $where2 .= "replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(work_master.address, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%' OR ";
                                $where2 .="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(location_master.name, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%') AND ";*/

                            }
                        
                    }
                    else{
                         $select2 = "work_master.name_en as Name,work_master.address_en as doctorAddress";
                         $where2 .= "(work_master.name_en LIKE '%".$this->db->escape_str(trim($search))."%' OR ";
                         $where2 .= "location_master.name_en LIKE '%".$this->db->escape_str(trim($search))."%' OR ";
                         $where2 .="work_master.address_en LIKE '%".$this->db->escape_str(trim($search))."%') AND ";
                    } 


                    $where_status2 = "work_master.status='1'";
                    $whereas2  = $where2.$where_status2;

                    $query2 =  "SELECT `work_master`.`photo`, `work_master`.`phone` AS `phone`,  `work_master`.`work_id` as ID, `work_master`.`work_type`, `work_master`.`work_latitude` as `google_map_latitude`, `work_master`.`work_longitude` as `google_map_longtude`, `work_master`.`work_zoom` as `Zoom`, (SELECT IFNULL(AVG(work_rating.average_score), 0) as average_score FROM work_rating WHERE work_master.work_id=work_rating.work_id ) as averageReview, $select2 FROM `work_master` LEFT JOIN `doctor_details` ON `doctor_details`.`work_id`=`work_master`.`work_id` LEFT JOIN `location_master` ON `work_master`.`location_id` = `location_master`.`location_id` LEFT JOIN `work_rating` ON `work_master`.`work_id`=`work_rating`.`work_id` WHERE $whereas2 GROUP BY work_master.work_id";  


                    /* end */





return          $this->db->query("(".$query.") UNION (".$query2.")  ORDER BY `averageReview` DESC LIMIT $page,$perPage")->result();      
       /*   echo $this->db->last_query();die;*/
   }

   function doctorWorkUnionQuery($search, $lang,$speciality,$location){
          /*  $result = $this->doctorMasterGlobalSearchNumRows($search, $lang,$speciality,$location);
            $result2 = $this->workMasterGlobalSearchNumRows($search, $lang,$speciality,$location);*/

             $siteLang=$this->session->userdata('site_lang');
                    
                    
                    /* Flat Query */

                     $where = "";
                     if(!empty($location))
                    {
                        $locations =getChildLocation($location);
                        $locs = implode(",",$locations);
                        $where .= "(doctor_details.location_id IN($locs)) AND ";
                    }


                    if(!empty($speciality))
                    {
                        $where .= "(doctor_details.speciality_id=".$speciality.") AND ";
                    }
                    
                   
                    $where2 = "";
                    $where3 = "";
                        if($lang=='arabic')
                        {               
                            $select = 'CONCAT(doctor_master.first_name," ",doctor_master.last_name) AS Name,`doctor_master`.`address` as `doctorAddress`';
                            if(!empty($search))
                            {

                                 $where .="(concat_ws(' ',doctor_master.first_name,doctor_master.last_name) LIKE '%".$search."%' OR ";
                                $where .="location_master.name LIKE '%".$search."%' OR ";
                                $where .="doctor_master.address LIKE '%".$search."%') AND ";
                                // commented by mayur panchal 13-07-2017                      
                                /* $where .="(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(concat_ws(' ',doctor_master.first_name,doctor_master.last_name), 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%' OR ";
                                $where .="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(location_master.name, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%' OR ";
                                $where .="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(doctor_master.address, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%') AND ";*/
                                   
                           }
                        
                    }
                    else{
                          //$this->db->where("");
                         $select = 'CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS Name, `doctor_master_en`.`address` as `doctorAddress`';   
                         $where .= "(concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$this->db->escape_str(trim($search))."%' OR ";
                         $where .= "location_master.name_en LIKE '%".$this->db->escape_str(trim($search))."%' OR ";
                         $where .="doctor_master_en.address LIKE '%".$this->db->escape_str(trim($search))."%') AND ";
                    } 


                    $where_status .= "doctor_details.visibility='1'";
                    $whereas  = $where.$where_status;

                    
                     $query = "SELECT `doctor_details`.`photo`, `doctor_details`.`phone_number` AS `phone`, `doctor_details`.`doctor_id`,'doctor' as work_type, `doctor_details`.`work_id` as work_id, `doctor_details`.`google_map_latitude`, `doctor_details`.`google_map_longtude`,`doctor_details`.`google_map_zoom` as `Zoom`, (SELECT IFNULL(AVG(doctors_rating.average_score), 0) as average_score FROM doctors_rating WHERE doctor_details.doctor_id=doctors_rating.doctor_id ) as averageReview, $select FROM `doctor_details` LEFT JOIN `location_master` ON `doctor_details`.`location_id`=`location_master`.`location_id` LEFT JOIN `specialty_master` ON `doctor_details`.`speciality_id`=`specialty_master`.`specialties_id` LEFT JOIN `doctor_master_en` ON `doctor_details`.`doctor_id`=`doctor_master_en`.`doctor_id` LEFT JOIN `doctor_master` ON `doctor_details`.`doctor_id`=`doctor_master`.`doctor_id` LEFT JOIN `doctors_rating` ON `doctors_rating`.`doctor_id`=`doctor_details`.`doctor_id` WHERE $whereas GROUP BY doctor_details.doctor_id";
            
                    /* end */

                      
                   $where2 = "";
                     if(!empty($location))
                    {
                        $locations =getChildLocation($location);
                        $locs = implode(",",$locations);
                        $where2 .= "(work_master.location_id IN($locs)) AND ";

                    }


                    if(!empty($speciality))
                    {
                        $where2 .= "(work_master.speciality=".$speciality.") AND ";                       
                    }
                    
                    
                        if($lang=='arabic')
                        {
                            $select2 = "work_master.name as Name,work_master.address as doctorAddress";
                            if(!empty($search))
                            {    

                                 $where2 .= "(work_master.name LIKE '%".$search."%' OR ";
                                 $where2 .= "work_master.address LIKE '%".$search."%' OR ";
                                 $where2 .="location_master.name LIKE '%".$search."%') AND ";  

                                 // commented by mayur panchal 13-07-2017                                                         
                               /* $where2 .= "(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(work_master.name, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%' OR ";
                                 $where2 .= "replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(work_master.address, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%' OR ";
                                $where2 .="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(location_master.name, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','') LIKE '%".$search."%') AND ";*/
                            }
                        
                    }
                    else{

                         $select2 = "work_master.name_en as Name,work_master.address_en as doctorAddress";
                         $where2 .= "(work_master.name_en LIKE '%".$this->db->escape_str(trim($search))."%' OR ";
                         $where2 .= "location_master.name_en LIKE '%".$this->db->escape_str(trim($search))."%' OR ";
                         $where2 .="work_master.address_en LIKE '%".$this->db->escape_str(trim($search))."%') AND ";
                    } 


                    $where_status2 = "work_master.status='1'";
                    $whereas2  = $where2.$where_status2;

                    $query2 =  "SELECT `work_master`.`photo`, `work_master`.`phone` AS `phone`, `doctor_details`.`doctor_id`, `work_master`.`work_type`, `work_master`.`work_id`, `work_master`.`work_latitude` as `google_map_latitude`, `work_master`.`work_longitude` as `google_map_longtude`, `work_master`.`work_zoom` as `Zoom`, (SELECT IFNULL(AVG(work_rating.average_score), 0) as average_score FROM work_rating WHERE work_master.work_id=work_rating.work_id ) as averageReview, $select2 FROM `work_master` LEFT JOIN `doctor_details` ON `doctor_details`.`work_id`=`work_master`.`work_id` LEFT JOIN `location_master` ON `work_master`.`location_id` = `location_master`.`location_id` LEFT JOIN `work_rating` ON `work_master`.`work_id`=`work_rating`.`work_id` WHERE $whereas2 GROUP BY work_master.work_id";                   

// 
             return $this->db->query("(".$query.") UNION (".$query2.") ORDER BY `averageReview` DESC ")->num_rows();
             echo $this->db->last_query();die;
            
            

            
   }
   
   
   
   
   
}
