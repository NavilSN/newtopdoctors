<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Result_model extends MY_Model {

    /**
     * Doctor result count search crieteria
     * @param mixed $data
     * @return type int
     */
    function result_count($data)
    {
                $siteLang=$this->session->userdata('site_lang');
                if(!empty($data['location']))
                    {     
                         $locations =getChildLocation($data['location']);
                        $this->db->where_in('doctor_details.location_id',$locations);
                        //$this->db->where('doctor_details.location_id',$data['location']);
                    }
                  if(!empty($data['speciality']))
                    {                        
                        $this->db->where('doctor_details.speciality_id',$data['speciality']);
                    }
                     $this->db->where('doctor_details.visibility','1');
                    if(!empty($data['gender']))
                    {                    
                        $this->db->where('doctor_details.gender',$data['gender']);
                    }
                    
                    if(!empty($data['searchname']))
                    {
                       if($siteLang=='arabic'){

                           // $this->db->where(" doctor_master.first_name LIKE '%".$data['searchname']."%' OR doctor_master.last_name LIKE '%".$data['searchname']."%'  or concat(' ',doctor_master.first_name,doctor_master.last_name) like '%".$data['searchname']."%'");
                           $this->db->where("concat_ws(' ',doctor_master.first_name,doctor_master.last_name) like '%".$this->db->escape_str(trim($data['searchname']))."%'");
                           /*$rawstring =  findword(trim($data['searchname']));
             
            // $this->db->where_in('doctor_master.first_name',$rawstring);
                            foreach($rawstring as $key => $val):
                                if($key==0){
                                $this->db->where("concat_ws(' ',doctor_master.first_name,doctor_master.last_name) like '%".$val."%'");                            
                                }
                                else{
                                    $this->db->or_where("concat_ws(' ',doctor_master.first_name,doctor_master.last_name) like '%".$val."%'");                            
                                }
                            endforeach;
                           */
                            $rawstring =  findword(trim($data['searchname']));
             
                        // $this->db->where_in('doctor_master.first_name',$rawstring);
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->or_like('doctor_master.first_name', $this->db->escape_str(trim($val)));                            
                             }
                             else{
                                 $this->db->or_like('doctor_master.first_name', $this->db->escape_str(trim($val)));                            
                             }
                         endforeach;
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->or_like('doctor_master.last_name', $this->db->escape_str(trim($val)));                            
                             }
                             else{
                                 $this->db->or_like('doctor_master.last_name', $this->db->escape_str(trim($val)));                            
                             }
                         endforeach;
                       
                        }else{
                            //$this->db->where(" doctor_master_en.first_name LIKE '%".$data['searchname']."%' OR doctor_master_en.last_name LIKE '%".$data['searchname']."%'  or concat(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$data['searchname']."%'");
                            $this->db->where(" concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$this->db->escape_str(trim($data['searchname']))."%'");
                            
                        }
                    } 
            
                    $this->db->select('doctor_details.photo,doctor_details.phone_number AS phone,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude');

                    if($siteLang=='arabic')
                    {
                        $this->db->select('CONCAT(doctor_master.first_name, " ", doctor_master.last_name) AS Name,doctor_master.address as doctorAddress');
                    }else{
                        $this->db->select('CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS Name,doctor_master_en.address as doctorAddress');
                    }

                $this->db->join('location_master', 'doctor_details.location_id=location_master.location_id','left');
                $this->db->join('specialty_master','doctor_details.speciality_id=specialty_master.specialties_id','left');
                $this->db->join('doctor_master_en', 'doctor_details.doctor_id=doctor_master_en.doctor_id','left');
                $this->db->join('doctor_master', 'doctor_details.doctor_id=doctor_master.doctor_id','left');    
              
                //$this->db->limit(100,0);
               return $this->db->get('doctor_details')->num_rows();
    }
    
   
      /**
     * Doctor result  search crieteria
     * @param mixed $data
     * @return type mixed array
     */       
    function fetch_doctors($perpage,$page,$data)
    {
            $siteLang=$this->session->userdata('site_lang');
            
             if(!empty($data['location']))
                    {     
                        $locations =getChildLocation($data['location']);
                        $this->db->where_in('doctor_details.location_id',$locations);
                        //$this->db->where('doctor_details.location_id',$data['location']);
                    }
                    $this->db->where('doctor_details.visibility','1');
                     if(!empty($data['speciality']))
                    {                        
                        $this->db->where('doctor_details.speciality_id',$data['speciality']);
                    }
                    if(!empty($data['gender']))
                    {                    
                        $this->db->where('doctor_details.gender',$data['gender']);
                    }
                   
                    if(!empty($data['searchname']))
                    {
                        if($siteLang=='arabic'){

                            //$this->db->where(" doctor_master.first_name LIKE '%".$data['searchname']."%' OR doctor_master.last_name LIKE '%".$data['searchname']."%'  or concat(' ',doctor_master.first_name,doctor_master.last_name) like '%".$data['searchname']."%'");
                            // doctor_master.first_name LIKE '%".$data['searchname']."%' OR doctor_master.last_name LIKE '%".$data['searchname']."%'  or 
                            $this->db->where("concat_ws(' ',doctor_master.first_name,doctor_master.last_name) like '%".$this->db->escape_str(trim($data['searchname']))."%'");
                               $rawstring =  findword(trim($data['searchname']));
             
                        // $this->db->where_in('doctor_master.first_name',$rawstring);
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->or_like('doctor_master.first_name', trim($val));                            
                             }
                             else{
                                 $this->db->or_like('doctor_master.first_name', trim($val));                            
                             }
                         endforeach;
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->or_like('doctor_master.last_name', trim($val));                            
                             }
                             else{
                                 $this->db->or_like('doctor_master.last_name', trim($val));                            
                             }
                         endforeach;
                            
                        }else{
                            //doctor_master_en.first_name LIKE '%".$data['searchname']."%' OR doctor_master_en.last_name LIKE '%".$data['searchname']."%'  or 
                            $this->db->where(" concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$this->db->escape_str(trim($data['searchname']))."%'");
                            //$this->db->where(" doctor_master_en.first_name LIKE '%".$data['searchname']."%' OR doctor_master_en.last_name LIKE '%".$data['searchname']."%'  or concat(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$data['searchname']."%'");
                        }
                        
                    } 
            
                    $this->db->select('doctor_details.photo,doctor_details.phone_number AS phone,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude,doctor_details.speciality_id');

                    if($siteLang=='arabic')
                    {
                        $this->db->select('CONCAT(doctor_master.first_name, " ", doctor_master.last_name) AS Name,doctor_master.address as doctorAddress');
                    }else{
                        $this->db->select('CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS Name,doctor_master_en.address as doctorAddress');
                    }

                $this->db->join('location_master', 'doctor_details.location_id=location_master.location_id','left');
                $this->db->join('specialty_master','doctor_details.speciality_id=specialty_master.specialties_id','left');
                $this->db->join('doctor_master_en', 'doctor_details.doctor_id=doctor_master_en.doctor_id','left');
                $this->db->join('doctor_master', 'doctor_details.doctor_id=doctor_master.doctor_id','left');    
               
                $this->db->limit($perpage,$page);
                return $this->db->get('doctor_details')->result();                
                //echo $this->db->last_query();    die;
                
       
    }
     /**
     * Work result count search crieteria
     * @param mixed $data
     * @return type int
     */
   function result_work($data)
   {
            $siteLang=$this->session->userdata('site_lang');
                    if(!empty($data['location']))
                    {     
                        $locations =getChildLocation($data['location']);
                        $this->db->where_in('work_master.location_id',$locations);
                        //$this->db->where('work_master.location_id',$data['location']);
                    }
                    if(!empty($data['searchname']))
                    {
                        if($siteLang=='arabic')
                        {
                          //  $this->db->where(" work_master.name LIKE '%".$this->db->escape_str(trim($data['searchname']))."%'");
                                $rawstring =  findword(trim($data['searchname']));
             
                        // $this->db->where_in('doctor_master.first_name',$rawstring);
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->like('work_master.name', $this->db->escape_str(trim($val)));                            
                             }
                             else{
                                 $this->db->or_like('work_master.name', $this->db->escape_str(trim($val)));                            
                             }
                         endforeach;
                        }else{
                            $this->db->where(" work_master.name_en LIKE '%".$this->db->escape_str(trim($data['searchname']))."%'"); 
                        }
                    } 
                    $this->db->where('work_master.status','1');

                    $this->db->select('work_master.photo,work_master.phone AS phone,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude,work_master.work_type,work_master.work_id');
                    if($siteLang=='arabic'){
                    $this->db->select('work_master.name as Name,work_master.address as doctorAddress');
                    }else{
                        $this->db->select('work_master.name_en as Name,work_master.address_en as doctorAddress');
                    }
                    $this->db->join('doctor_details', 'doctor_details.work_id=work_master.work_id','LEFT');
                    $this->db->join('location_master', 'doctor_details.location_id = location_master.location_id','LEFT');
                    if($data['typeofsearch']=="Lab")
                    {
                        if($data['labtype']=="")
                         {
                             $this->db->like('work_master.work_type',"Lab");
                         }
                         else{
                             $this->db->like('work_master.work_type',$data['labtype']);
                         }
                    }
                    else{
                    $this->db->where('work_master.work_type',$data['typeofsearch']);
                    }
                    $this->db->group_by('work_master.work_id');
                    
                    return $this->db->get('work_master')->num_rows();
   }
    /**
     * work result  search crieteria
     * @param mixed $data
     * @return type
     */
   function fetch_work($per_page, $page,$data)
   {    
                    $siteLang=$this->session->userdata('site_lang');
       
       
                    if(!empty($data['location']))
                    {     
                          $locations =getChildLocation($data['location']);
                        $this->db->where_in('work_master.location_id',$locations);
                        //$this->db->where('work_master.location_id',$data['location']);
                    }
                    if(!empty($data['searchname']))
                    {
                        if($siteLang=='arabic'){

                           // $this->db->where(" work_master.name LIKE '%".$this->db->escape_str(trim($data['searchname']))."%'");
                              $rawstring =  findword(trim($data['searchname']));
             
                        // $this->db->where_in('doctor_master.first_name',$rawstring);
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->like('work_master.name', $this->db->escape_str(trim($val)));                            
                             }
                             else{
                                 $this->db->or_like('work_master.name', $this->db->escape_str(trim($val)));                            
                             }
                         endforeach;
                        }else{
                            $this->db->where(" work_master.name_en LIKE '%".$this->db->escape_str(trim($data['searchname']))."%'");
                        }
                    } 
                    $this->db->where('work_master.status','1');

                    $this->db->select('work_master.photo,work_master.phone AS phone,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude,work_master.work_type,work_master.work_id');
                    if($siteLang=='arabic'){
                        $this->db->select('work_master.name as Name,work_master.address as doctorAddress');
                    }else{
                        $this->db->select('work_master.name_en as Name,work_master.address_en as doctorAddress');
                    }
                    $this->db->join('doctor_details', 'doctor_details.work_id=work_master.work_id','LEFT');
                    $this->db->join('location_master', 'doctor_details.location_id = location_master.location_id','LEFT');
                    
                     if($data['typeofsearch']=="Lab")
                    {
                         if($data['labtype']=="")
                         {
                             $this->db->like('work_master.work_type',"Lab");
                         }
                         else{
                             $this->db->like('work_master.work_type',$data['labtype']);
                         }
                        
                    }
                    else{
                    $this->db->where('work_master.work_type',$data['typeofsearch']);
                    }
                    $this->db->group_by('work_master.work_id');
                    $this->db->limit($per_page,$page);
                      return $this->db->get('work_master')->result();
                     //echo $this->db->last_query(); die;
                    
                    
   }

   function result_speciality($data)
   {
            $condition=array('specialty_master.name_en'=>$data['searchSpecialityValue']);
                $this->db->select('doctor_details.photo,doctor_details.phone_number AS phone,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude');

                    if($siteLang=='arabic'){
                    $this->db->select('CONCAT(doctor_master.first_name, " ", doctor_master.last_name) AS Name,doctor_master.address as doctorAddress');
                    }else{
                        $this->db->select('CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS Name,doctor_master_en.address as doctorAddress');
                    }

                $this->db->join('specialty_master', 'doctor_details.speciality_id=specialty_master.specialties_id','left');
                $this->db->join('doctor_master', 'doctor_details.doctor_id=doctor_master.doctor_id','left');
                $this->db->join('doctor_master_en', 'doctor_details.doctor_id=doctor_master_en.doctor_id','left');
                return $this->db->get_where('doctor_details', $condition)->num_rows();
   }

   function fetch_speciality($per_page, $page,$data)
   {    
            $siteLang=$this->session->userdata('site_lang');
                $condition=array('specialty_master.name_en'=>$data['searchSpecialityValue']);
                $this->db->select('doctor_details.photo,doctor_details.phone_number AS phone,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude');

                    if($siteLang=='arabic'){
                    $this->db->select('CONCAT(doctor_master.first_name, " ", doctor_master.last_name) AS Name,doctor_master.address as doctorAddress');
                    }else{
                        $this->db->select('CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS Name,doctor_master_en.address as doctorAddress');
                    }

                $this->db->join('specialty_master', 'doctor_details.speciality_id=specialty_master.specialties_id','left');
                 $this->db->join('doctor_master', 'doctor_details.doctor_id=doctor_master.doctor_id','left');
                $this->db->join('doctor_master_en', 'doctor_details.doctor_id=doctor_master_en.doctor_id','left');
                $this->db->limit($per_page,$page);
                 return $this->db->get_where('doctor_details', $condition)->result();

                 //echo $this->db->last_query();
                    
                    
   }
   /**
    * Global Search function for merge result
    * @param numeric $perPage
    * @param numeric $page
    * @param mixed array $data
    * @return type mixed array
    */
   function fetch_globalSearch($perPage,$page,$data) {
      $result = array();
                $search = trim($data['search']);
                $lang = $data['lang'];
                $speciality  = $data['speciality'];
                $location = $data['location'];
                $doctorMasterGlobalSearchResult = $this->doctorMasterGlobalSearch($search,$lang,$page,$perPage,$speciality,$location);                               
                
                $result = $this->mergerResult($result, $doctorMasterGlobalSearchResult);
                
                $workMasterGlobalSearchResult = $this->workMasterGlobalSearch($search,$lang,$page,$perPage,$speciality,$location);
                
                $result = $this->mergerResult($result, $workMasterGlobalSearchResult);
                

      return $result;

   }
   /**
    * Get Global Work Result 
    * using speciality location and name
    * @param mixed $search
    * @param string $lang
    * @param numeric $page
    * @param numeric $perPage
    * @param numeric $speciality
    * @param numeric $location
    * @return type mixed array
    */
   function workMasterGlobalSearch($search, $lang, $page,$perPage,$speciality,$location) {
           $siteLang=$this->session->userdata('site_lang');
                   
                     if(!empty($location))
                    {
                        $locations =getChildLocation($location);
                        $this->db->where_in('work_master.location_id',$locations); 
                    }
                    if(!empty($speciality))
                    {
                        $this->db->where('doctor_details.speciality_id',$speciality);
                    }
                    if(!empty($search))
                    {
                        if($lang=='arabic')
                        {
                             $rawstring =  findword(trim($search));
                             
                            $this->db->where("work_master.name LIKE '%".$this->db->escape_str(trim($search))."%'");
                            $this->db->or_where("work_master.address LIKE '%".$this->db->escape_str(trim($search))."%'");
                            $this->db->or_where("location_master.name LIKE '%".$this->db->escape_str(trim($search))."%'");
                            $rawstring =  findword(trim($search));
             
                        // $this->db->where_in('doctor_master.first_name',$rawstring);
                         foreach($rawstring as $key => $val):
                             if($key==0){
                                $this->db->like('work_master.name', trim($val));                            
                             }
                             else{                                 
                                 $this->db->or_like('work_master.name', trim($val));                            
                             }
                         endforeach;
                            
                            
                        }else{
                            $this->db->where("work_master.name_en LIKE '%".$this->db->escape_str(trim($search))."%'");
                            $this->db->or_where("work_master.address_en LIKE '%".$this->db->escape_str(trim($search))."%'");
                            $this->db->or_where("location_master.name_en LIKE '%".$this->db->escape_str(trim($search))."%'");
                        }
                        
                    } 
                    $this->db->where('work_master.status','1');

                    $this->db->select('work_master.photo,work_master.phone AS phone,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude,work_master.work_type,work_master.work_id');
                    if($siteLang=='arabic'){
                    $this->db->select('work_master.name as Name,work_master.address as doctorAddress');
                    }else{
                        $this->db->select('work_master.name_en as Name,work_master.address_en as doctorAddress');
                    }
                    $this->db->from('work_master');
                    $this->db->join('doctor_details', 'doctor_details.work_id=work_master.work_id','LEFT');
                    $this->db->join('location_master', 'doctor_details.location_id = location_master.location_id','LEFT');
                   
                    $this->db->group_by('work_master.work_id');
                    $this->db->limit($perPage,$page);
                    return $this->db->get()->result();
   }

   /**
    * Get Doctor Global Search
    * doctor search using speciality location and name wise
    * @param mixed $search
    * @param string $lang
    * @param numeric $page
    * @param numeric $perPage
    * @param numeric $speciality
    * @param numeric $location
    * @return mixed array
    */
   function doctorMasterGlobalSearch($search, $lang,$page,$perPage,$speciality,$location) {
           //$siteLang=$this->session->userdata('site_lang');
                  
                    if(!empty($location))
                    {
                        $locations =getChildLocation($location);
                        $this->db->where_in('doctor_details.location_id',$locations); 
                    }
                    if(!empty($search))
                    {
                       if($lang=='arabic'){    
                           $rawstring =  findword(trim($search));
             
                        // $this->db->where_in('doctor_master.first_name',$rawstring);
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->where("concat_ws(' ',doctor_master.first_name,doctor_master.last_name) like '%".$this->db->escape_str(trim($val))."%'");
                             }
                             else{
                             $this->db->or_where("concat_ws(' ',doctor_master.first_name,doctor_master.last_name) like '%".$this->db->escape_str(trim($val))."%'");
                             }
                         endforeach;  
                         // $this->db->where("concat_ws(' ',doctor_master.first_name,doctor_master.last_name) like '%".$this->db->escape_str(trim($search))."%'");
                        }else{        
                            
                            $this->db->where(" concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$this->db->escape_str(trim($search))."%'");                            
                        }                        
                    } 
                    if(!empty($speciality))
                    {                        
                        $this->db->where('doctor_details.speciality_id',$speciality);
                    }
                    $this->db->select('doctor_details.photo,doctor_details.phone_number AS phone,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude');

                    if($lang=='arabic')
                    {
                        $this->db->select('CONCAT(doctor_master.first_name, " ", doctor_master.last_name) AS Name,doctor_master.address as doctorAddress');
                    }else{
                        $this->db->select('CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS Name,doctor_master_en.address as doctorAddress');
                    }
                    if($search!="")
                    {
                        if($lang=='arabic'){ 
                            //$this->db->like('doctor_master.first_name',$this->db->escape_str(trim($search)));
                            //$this->db->or_like('doctor_master.last_name',$this->db->escape_str(trim($search)));
                            $rawstring =  findword(trim($search));
             
                        // $this->db->where_in('doctor_master.first_name',$rawstring);
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->or_like('doctor_master.first_name', trim($val));                            
                             }
                             else{
                                 $this->db->or_like('doctor_master.first_name', trim($val));                            
                             }
                         endforeach;
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->or_like('doctor_master.last_name', trim($val));                            
                             }
                             else{
                                 $this->db->or_like('doctor_master.last_name', trim($val));                            
                             }
                         endforeach;
                        }
                        else{
                             
                        $this->db->or_like('doctor_master_en.first_name',$this->db->escape_str(trim($search)));
                        $this->db->or_like('doctor_master_en.last_name',$this->db->escape_str(trim($search)));
                        }
                    //$this->db->like('doctor_master_en.first_name',$this->db->escape_str(trim($search)));
                    //$this->db->or_like('doctor_master_en.last_name',$this->db->escape_str(trim($search)));
                    }
                    
                $this->db->join('location_master', 'doctor_details.location_id=location_master.location_id','left');
                $this->db->join('specialty_master','doctor_details.speciality_id=specialty_master.specialties_id','left');
                $this->db->join('doctor_master_en', 'doctor_details.doctor_id=doctor_master_en.doctor_id','left');
                $this->db->join('doctor_master', 'doctor_details.doctor_id=doctor_master.doctor_id','left');    
                $this->db->where('doctor_details.visibility','1');
                $this->db->limit($perPage,$page);
                return $this->db->get('doctor_details')->result();
                //echo $this->db->last_query();die;
               //return $this->db->get('doctor_details')->result();


      //echo $this->db->last_query(); die();
   }
   
           /**
         * work master global search num rows
         * @param char $search
         * @param char $lang
         * @param int $userId
         * @return mixed array
         */
        function workMasterGlobalSearchNumRows($search, $lang,$speciality,$location) {
                 $siteLang=$this->session->userdata('site_lang');
                   
                     if(!empty($location))
                    {
                        $locations =getChildLocation($location);
                        $this->db->where_in('work_master.location_id',$locations); 
                    }
                    if(!empty($speciality))
                    {
                        $this->db->where('doctor_details.speciality_id',$speciality);
                    }
                    if(!empty($search))
                    {
                        if($lang=='arabic')
                        {
                            
                            $this->db->where("work_master.name LIKE '%".$this->db->escape_str(trim($search))."%'");
                            $this->db->or_where("work_master.address LIKE '%".$this->db->escape_str(trim($search))."%'");
                            $this->db->or_where("location_master.name LIKE '%".$this->db->escape_str(trim($search))."%'");
                             $rawstring =  findword(trim($search));
             
                        // $this->db->where_in('doctor_master.first_name',$rawstring);
                         foreach($rawstring as $key => $val):
                             if($key==0){
                                $this->db->like('work_master.name', trim($val));                            
                             }
                             else{                                 
                                 $this->db->or_like('work_master.name', trim($val));                            
                             }
                         endforeach;
                            
                            
                        }else{
                            $this->db->where("work_master.name_en LIKE '%".$this->db->escape_str(trim($search))."%'");
                            $this->db->or_where("work_master.address_en LIKE '%".$this->db->escape_str(trim($search))."%'");
                            $this->db->or_where("location_master.name_en LIKE '%".$this->db->escape_str(trim($search))."%'");
                        }
                        
                    } 
                    $this->db->where('work_master.status','1');

                    $this->db->select('work_master.photo,work_master.phone AS phone,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude,work_master.work_type,work_master.work_id');
                    if($siteLang=='arabic'){
                    $this->db->select('work_master.name as Name,work_master.address as doctorAddress');
                    }else{
                        $this->db->select('work_master.name_en as Name,work_master.address_en as doctorAddress');
                    }
                    $this->db->from('work_master');
                    $this->db->join('doctor_details', 'doctor_details.work_id=work_master.work_id','LEFT');
                    $this->db->join('location_master', 'doctor_details.location_id = location_master.location_id','LEFT');
                   
                    $this->db->group_by('work_master.work_id');
                 //   $this->db->limit($perPage,$page);
                    return $this->db->get()->num_rows();
   }

        /**
         *
         * @param char $search
         * @param char $lang
         * @param int $userId
         * @return mixed array
         */
        function doctorMasterGlobalSearchNumRows($search, $lang,$speciality,$location) {
          $siteLang=$this->session->userdata('site_lang');
                    
                    if(!empty($location))
                    {
                        $locations =getChildLocation($location);
                        $this->db->where_in('doctor_details.location_id',$locations); 
                    }
                    if(!empty($search))
                    {
                       if($lang=='arabic'){

                           $rawstring =  findword(trim($search));
             
                        // $this->db->where_in('doctor_master.first_name',$rawstring);
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->where("concat_ws(' ',doctor_master.first_name,doctor_master.last_name) like '%".$this->db->escape_str(trim($val))."%'");
                             }
                             else{
                             $this->db->or_where("concat_ws(' ',doctor_master.first_name,doctor_master.last_name) like '%".$this->db->escape_str(trim($val))."%'");
                             }
                         endforeach;
                           
                           
                     

                        }else{
                     
                            $this->db->where(" concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$this->db->escape_str(trim($search))."%'");
                            
                        }
                    } 
                    if(!empty($speciality))
                    {                        
                        $this->db->where('doctor_details.speciality_id',$speciality);
                    }
                    $this->db->select('doctor_details.photo,doctor_details.phone_number AS phone,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude');

                    if($siteLang=='arabic')
                    {
                        $this->db->select('CONCAT(doctor_master.first_name, " ", doctor_master.last_name) AS Name,doctor_master.address as doctorAddress');
                    }else{
                        $this->db->select('CONCAT(doctor_master_en.first_name, " ", doctor_master_en.last_name) AS Name,doctor_master_en.address as doctorAddress');
                    }
                    if($search!="")
                    {
                        if($lang=="arabic")
                        {
                            $rawstring =  findword(trim($search));
             
                        // $this->db->where_in('doctor_master.first_name',$rawstring);
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->or_like('doctor_master.first_name', trim($val));                            
                             }
                             else{
                                 $this->db->or_like('doctor_master.first_name', trim($val));                            
                             }
                         endforeach;
                         foreach($rawstring as $key => $val):
                             if($key==0){
                             $this->db->or_like('doctor_master.last_name', trim($val));                            
                             }
                             else{
                                 $this->db->or_like('doctor_master.last_name', trim($val));                            
                             }
                         endforeach;
                        }else{
                            $this->db->or_like('doctor_master_en.first_name',$this->db->escape_str(trim($search)));
                            $this->db->or_like('doctor_master_en.last_name',$this->db->escape_str(trim($search)));
                        }
                    }
                    
                $this->db->join('location_master', 'doctor_details.location_id=location_master.location_id','left');
                $this->db->join('specialty_master','doctor_details.speciality_id=specialty_master.specialties_id','left');
                $this->db->join('doctor_master_en', 'doctor_details.doctor_id=doctor_master_en.doctor_id','left');
                $this->db->join('doctor_master', 'doctor_details.doctor_id=doctor_master.doctor_id','left');    
               $this->db->where('doctor_details.visibility','1');
                $this->db->limit($perPage,$page);
               return $this->db->get('doctor_details')->num_rows();

      //echo $this->db->last_query(); die();
   }
   
   
   /**
    * 
    * merge work & hospital result
    * @param array $primaryArray
    * @param array $secondaryArray
    * @return type mixed array
    */

    function mergerResult($primaryArray, $secondaryArray) {
      $result = $primaryArray;
      if(!empty($secondaryArray)) {
         $result = array_merge($primaryArray, $secondaryArray);
      }
      
      return $result;
   }
   
   
}
