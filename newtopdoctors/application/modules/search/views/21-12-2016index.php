<?php  
$siteLang=$this->session->userdata('site_lang');
$this->load->model('home/Location_master_model');
$this->load->model('home/Specialty_master_model');

$locationList=$this->Location_master_model->get_all_location_by_name();
$specialityList=$this->Specialty_master_model->get_all_speciality();

if(isset($_POST['searchData']) || $this->session->userdata('typeofsearch')){
    $speciality=$speciality;
    $locationArray=$locationArray;
    $searchSpecialityValue='';
}else{
      if(isset($_POST['specialitySearch']) || $this->session->userdata('searchSpecialityValue')) {
        $locationArray=$locationArray;
      }else{
        $locationArray=array();
        $locationArray=$locationArray;
        $searchSpecialityValue='';
      }
    $location='';
    $speciality='';
    $gender='';
    $searchname='';
}

?>

<div class="search-full-box">
<section class="text-center clearfix">
  
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-12 text-left">
                <form class="form-inline" action="<?php echo base_url();?>search/globalSearch" method="post">
                  <div class="form-group">
                    <span class="flip search-icon"> <i class="fa fa-search"></i></span>                    
                    <input type="text" name="searchname" class="form-control" id="" placeholder="<?php //echo $this->lang->line('searchPlaceHolder'); ?>" value="<?php  if($typeofsearch=="globalsearch"){ echo $searchname; } ?>">
                  </div>
                  <div class="form-control-btn-box">
                    <button type="submit" name="searchData" class="btn btn-default search-btn"><?php echo $this->lang->line('searchbtn'); ?></button>                   
                  </div>
                </form>
            </div>
          </div>          
      </div>
  </section>
</div>
 <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
  <!--<link rel="stylesheet" href="/resources/demos/style.css">-->
  
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
  <script>
  $( function() {
    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
      _renderItem: function( ul, item ) {
        var li = $( "<li>" ),
          wrapper = $( "<div>", { text: item.label } );
 
        if ( item.disabled ) {
          li.addClass( "ui-state-disabled" );
        }
 
        $( "<span>", {
          style: item.element.attr( "data-style" ),
          "class": "ui-icon " + item.element.attr( "data-class" )
        })
          .appendTo( wrapper );
 
        return li.append( wrapper ).appendTo( ul );
      }
    });
 
   
    $( "#people" )
      .iconselectmenu()
      .iconselectmenu( "menuWidget")
        .addClass( "ui-menu-icons avatar" );
  } );
  
  </script>
  <style>
   
    fieldset {
      border: 0;
    }
    label {
      display: block;
    }
 
    /* select with custom icons */
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item-wrapper {
      padding: 0.5em 0 0.5em 3em;
    }
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item .ui-icon {
      height: 24px;
      width: 24px;
      top: 0.1em;
    }
   
 
    /* select with CSS avatar icons */
    option.avatar {
      background-repeat: no-repeat !important;
      padding-left: 20px;
    }
    .avatar .ui-icon {
      background-position: left top;
    }
  </style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/star-rating-svg.css">
<!--///////////////////////////////////////////////////////
       // Filters
//////////////////////////////////////////////////////////-->
<div class="filters-box m50t">
  <section class="text-center clearfix">
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-9 text-left">
            <!-- Filter details start  -->
               <div class="filters-details clearfix">
                 <h2 class="flip text-left clearfix"><?php if($typeofsearch=='Doctors'){echo $this->lang->line('homeSearchDoctors');} if($typeofsearch=='Hospital'){echo $this->lang->line('homeSearchHostpitals');}if($typeofsearch=='Clinic'){echo $this->lang->line('homeSearchClinic');}if($typeofsearch=='Lab'){echo $this->lang->line('homeSearchLab');}echo ' '.$this->lang->line('filterTitle'); ?></h2>
                 <div class="border-divider"></div>
                 <?php if($typeofsearch=='globalsearch'){ ?>
                   <form class="form-horizontal" role="form" action="<?php echo base_url(); ?>search/globalSearch" method="post">  
                    <input type="hidden" name="typeofsearch" value="<?php echo $typeofsearch; ?>">
                    <div class="form-group">
                      <?php if($typeofsearch=='Doctors'){?>
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown" name="gender">
                           <option value=""><?php echo $this->lang->line('filterGender'); ?></option>
                           <option value="1" <?php if($gender==1)echo 'selected'; ?>><?php echo $this->lang->line('filterMale'); ?></option>
                           <option value="2" <?php if($gender==2)echo 'selected'; ?>><?php echo $this->lang->line('filterFemale'); ?></option>
                        </select>
                      </div>
                        
                      <?php } ?>
                      <?php if($typeofsearch=='Doctors' || $typeofsearch=='globalsearch'){?>
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown"  id="people" name="speciality">
                           <option value=""><?php echo $this->lang->line('filterSpeciality'); ?></option>
                           <?php foreach ($specialityList as $row_speciality) {?>
                             <option value="<?php echo $row_speciality->specialties_id; ?>"  data-class="avatar" data-style="background-image: url(&apos;<?php echo base_url(); ?>images/icon/speciality/<?php echo $row_speciality->specialties_id.".png"; ?>&apos;);"    <?php if($speciality==$row_speciality->specialties_id)echo 'selected'; ?>><?php if($siteLang=='arabic') echo $row_speciality->name; else echo $row_speciality->name_en; ?></option>
                          <?php  } ?>
                        </select>
                      </div>
                      <?php  } ?>
                        <div class = "col-sm-6 pull-left col-xs-12">
                           <input type="text" class="form-control" id="searchname" name="searchname" placeholder="<?php echo $this->lang->line('filterNamePlace'); ?>" value="<?php if($searchname)echo $searchname; ?>">
                        </div>
                    </div>
                 
                    <div class = "form-group">
                      <div class = "col-sm-6 pull-left col-xs-12">                     
                          <?php echo getChilds($location); ?>                         
                      </div>
                        
                     </div>
                    
                     <div class = "form-group no-marginbot">
                        <div class = "col-sm-12 pull-left col-xs-12">                            
                            <div class="flip text-left flip">
                             <!-- <div class="pull-left">
                                <a class="btn btn-default search-btn" href="<?php echo $_SERVER['HTTP_REFERER']; ?>"><?php echo $this->lang->line('SearchBack'); ?></a>
                              </div>-->
                              <div class="pull-right">
                                <button type="submit" id="searchData" name="searchData" class="btn btn-default search-btn"><?php echo $this->lang->line('filterSearchBtn'); ?></button>
                              </div>
                            </div>                        
                        </div>                        
                      </div>  


                  </form>
                   <?php  }else{ ?>
                       <form class="form-horizontal" role="form" action="<?php echo base_url(); ?>search" method="post">  
                   
                    <input type="hidden" name="typeofsearch" value="<?php echo $typeofsearch; ?>">
                    <div class="form-group">
                      <?php if($typeofsearch=='Doctors'){?>
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown" name="gender">
                           <option value=""><?php echo $this->lang->line('filterGender'); ?></option>
                           <option value="1" <?php if($gender==1)echo 'selected'; ?>><?php echo $this->lang->line('filterMale'); ?></option>
                           <option value="2" <?php if($gender==2)echo 'selected'; ?>><?php echo $this->lang->line('filterFemale'); ?></option>
                        </select>
                      </div>
                      <?php } ?>
                      <?php if($typeofsearch=='Doctors' || $typeofsearch=='globalsearch'){?>
                      <div class = "col-sm-6 pull-left col-xs-12">
                          <select class = "form-control dropdown" id="people" name="speciality">
                           <option value=""><?php echo $this->lang->line('filterSpeciality'); ?></option>
                           <?php foreach ($specialityList as $row_speciality) {?>
                             <!--<option value="<?php echo $row_speciality->specialties_id; ?>"  style="background-image:url('<?php echo base_url(); ?>images/icon/speciality/<?php echo $row_speciality->specialties_id.".png"; ?>'); background-repeat: no-repeat; " <?php if($speciality==$row_speciality->specialties_id)echo 'selected'; ?>><?php if($siteLang=='arabic') echo $row_speciality->name; else echo $row_speciality->name_en; ?></option>-->
                           <option value="<?php echo $row_speciality->specialties_id; ?>" data-class="avatar" data-style="background-image: url(&apos;<?php echo base_url(); ?>images/icon/speciality/<?php echo $row_speciality->specialties_id.".png"; ?>&apos;);" <?php if($speciality==$row_speciality->specialties_id)echo 'selected'; ?>><?php if($siteLang=='arabic') echo $row_speciality->name; else echo $row_speciality->name_en; ?></option>
                          <?php  } ?>
                        </select>
                      </div>
                      <?php  } ?>
                    </div>
                 
                    <div class = "form-group">
                      <div class = "col-sm-6 pull-left col-xs-12">
                       
                          <?php echo getChilds($location); ?>
                         
                      </div>
                      <?php if($typeofsearch=='Lab'){?>
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown" name="labtype">
                           <option value=""><?php echo $this->lang->line('LabType'); ?></option>
                           <option value="Radiology Lab" <?php if($labtype=="Radiology Lab"){ echo "selected";} ?>><?php echo $this->lang->line('radiologylab'); ?></option>
                           <option value="Medical Lab" <?php if($labtype=="Medical Lab"){ echo "selected";} ?>><?php echo $this->lang->line('medicallab'); ?></option>
                        </select>
                      </div>
                      <?php } ?>
                    
                        <div class = "col-sm-6 pull-left col-xs-12">
                           <input type="text" class="form-control" id="searchname" name="searchname" placeholder="<?php echo $this->lang->line('filterNamePlace'); ?>" value="<?php if($searchname)echo $searchname; ?>">
                        </div>
                     </div>
                    
                     <div class = "form-group no-marginbot">
                        <div class = "col-sm-12 pull-left col-xs-12">                            
                            <div class="flip text-left flip">
                             
                              <div class="pull-right">
                                <button type="submit" id="searchData" name="searchData" class="btn btn-default search-btn"><?php echo $this->lang->line('filterSearchBtn'); ?></button>
                              </div>
                            </div>                        
                        </div>                        
                      </div>  


                  </form>
                 <?php } ?>
               </div>
             
             <div class="sorting-det">
                
             </div>                
            <div class="border-divider fullwidth"></div>
            
             <div class="filter-show-data clearfix">
                  <form role="form" class="form-horizontal">   
                    <div class="form-group">
                      <?php if(empty($locationArray)) { ?>
                       <div><h1 style="color:#dedede;"><?php echo $this->lang->line('searchNoRecords'); ?></h1></div> 
                      <?php } else{
                      $i=1;
                      $a=1;
                      foreach ($locationArray as $locationArrayRow) {
                      $this->db->select('AVG(average_score) AS average_score');
                          $doct_id = $locationArrayRow->doctor_id;
                        //$this->db->select('(SELECT NULLIF(AVG(reputation), 0) FROM doctors_rating WHERE dstatus="1" AND reputation!="0" AND doctor_id="$doct_id" GROUP BY doctor_id) AS reputation , (SELECT NULLIF(AVG(clinic), 0) FROM doctors_rating WHERE dstatus="1" AND clinic!="0" AND doctor_id="$doct_id" GROUP BY doctor_id) AS clinic, (SELECT NULLIF(AVG(availability), 0) FROM doctors_rating WHERE dstatus="1" AND availability!="0" AND doctor_id="$doct_id" GROUP BY doctor_id) AS availability, (SELECT NULLIF(AVG(approachability), 0) FROM doctors_rating WHERE dstatus="1" AND approachability!="0" AND doctor_id="$doct_id" GROUP BY doctor_id) AS approachability, (SELECT NULLIF(AVG(technology), 0) FROM doctors_rating WHERE dstatus="1" AND technology!="0" AND doctor_id="$doct_id" GROUP BY doctor_id) AS technology,AVG(reputation + clinic + availability + approachability + technology)/5 AS average_score');
                      $this->db->where('dstatus','1');
                      $averageScoreRow=$this->db->get_where('doctors_rating',array('doctor_id'=>$locationArrayRow->doctor_id))->row();

                      $this->db->select('AVG(average_score) AS average_score');
                      $this->db->where('wstatus','1');
                      $averageScoreRowWork=$this->db->get_where('work_rating',array('work_id'=>$locationArrayRow->work_id))->row();
                       ?>
                      <div class="col-sm-6 pull-left col-xs-12">
                        <?php $bookmarkedRow2= $this->db->get_where('bookmark_master',array('doctor_id'=>$locationArrayRow->doctor_id,'user_id'=>$this->session->userdata("user_id")))->row(); 

                         $bookmarkedRowWork2= $this->db->get_where('worktype_bookmark_master',array('work_id'=>$locationArrayRow->work_id,'user_id'=>$this->session->userdata("user_id")))->row();
                         
                        ?>
                         <div class="doctor-details <?php if($bookmarkedRow2->status==1){echo'bookmarks';} ?>">
                            <div class="top-box">
                              <?php if($locationArrayRow->work_type=='Hospital'){?>
                              <a href="<?php echo base_url(); ?>workProfile/index/<?php echo $locationArrayRow->work_id; ?>">
                               <img src="<?php echo base_url(); ?>images/Hospitals.png" alt="" height="150" width="150">
                             </a>
                              <?php }else if($locationArrayRow->work_type=='Clinic'){ ?>
                                <a href="<?php echo base_url(); ?>workProfile/index/<?php echo $locationArrayRow->work_id; ?>">
                                  <img src="<?php echo base_url(); ?>images/Clinics.png" alt="" height="150" width="150"> 
                                </a>
                              <?php }else if($locationArrayRow->work_type=='Medical Lab' || $locationArrayRow->work_type=='Radiology Lab'){ ?>
                                <a href="<?php echo base_url(); ?>workProfile/index/<?php echo $locationArrayRow->work_id; ?>">
                                  <img src="<?php echo base_url(); ?>images/Labs.png" alt="" height="150" width="150">
                                </a> 
                              <?php }else{ ?>
                                <a href="<?php echo base_url(); ?>doctorProfile/index/<?php echo $locationArrayRow->doctor_id; ?>">
                                  <img src="<?php echo base_url(); ?>images/Doctors.png" alt="" height="150" width="150">
                                </a>
                              <?php  } ?>
                                <?php if (count($bookmarkedRow2) == 1){?>
                                <div class="bookmarks-select">
                                  <img src="<?php echo base_url(); ?>/images/bookmarks-left.png" alt="">
                                </div>
                                <?php } ?>
                            </div>
                            <div class="bottom-box">
                              <div class="bottom-box-det">
                                
                              
                              <h2 class="flip text-left clearfix"><?php
                              if(isset($locationArrayRow->work_type)){?>
                              <a href="<?php echo base_url(); ?>workProfile/index/<?php echo $locationArrayRow->work_id; ?>">
                                <?php echo $locationArrayRow->Name; ?>
                              </a>
                              <?php   
                              }else{?>
                              <a href="<?php echo base_url(); ?>doctorProfile/index/<?php echo $locationArrayRow->doctor_id; ?>">
                                <?php echo $locationArrayRow->Name; ?>
                              </a>
                              <?php   
                              }
                              ?>
                              </h2>
                              <div class="border-divider"></div> 
                               <div class="review-det">
                                   <!-- <div class="flip text-left">-->
                                      <?php if (isset($locationArrayRow->work_type)) { ?>                                    
                                               <input type="hidden" value="<?php echo floatval($averageScoreRowWork->average_score); ?>"  id="frateViewVal<?php echo $i; ?>">
                                               <div class="my-rating-<?php echo $i; ?>"></div>
                                           <?php } else { ?>                                       
                                               <input type="hidden" value="<?php echo floatval($averageScoreRow->average_score); ?>"  id="frateViewVal<?php echo $i; ?>">
                                               <div class="my-rating-<?php echo $i; ?>"></div>
                                           <?php } ?>
                                  
                             
<script>
$(function() {


  $(".my-rating-<?php echo $i; ?>").starRating({
    totalStars: 5,
    emptyColor: 'lightgray',    
    activeColor: '#03878A',
    initialRating: $("#frateViewVal<?php echo $i; ?>").val(),
    strokeWidth: 0,
    useGradient: false,    
    starSize:'20',
      readOnly: true,
    callback: function(currentRating, $el){
    //  alert('rated ' +  currentRating);
      //console.log('DOM Element ', $el);
    }
  });


});
</script>
                                    <div class="flip text-right">
                                    <span class="title">
                                    <?php
                                    if(isset($locationArrayRow->work_type)){
                                       $this->db->select('count(*) as workReview'); 
                                       $this->db->where('wstatus','1');
                                      $workReview=$this->db->get_where('work_rating',array('work_id'=>$locationArrayRow->work_id))->row();
                                      
                                      echo $workReview->workReview.' '.$this->lang->line('DPReview');
                                    }else{
                                       $this->db->select('count(*) as doctorReview'); 
                                       $this->db->where('dstatus','1');
                                      $doctorReview=$this->db->get_where('doctors_rating',array('doctor_id'=>$locationArrayRow->doctor_id))->row();
                                      echo $doctorReview->doctorReview.' '.$this->lang->line('DPReview');
                                    }
                                    ?>
                                    
                                    </span>
                                    </div>
                                 </div> 

                               

                                 <div class="address-det">
                                     <?php if(!empty($locationArrayRow->doctorAddress)){ ?>
                                    <div class="w100p clearfix">
                                          <div class="first-one">
                                              <img src="<?php echo base_url(); ?>images/location-icon.png" alt="">
                                      </div>
                                      <div class="first-one">
                                          <span class="address">
                                            <?php 
                                             if(strlen($locationArrayRow->doctorAddress) > 40){
                                              $locationArrayRow->doctorAddress = substr($locationArrayRow->doctorAddress, 0, 40);
                                               echo substr($locationArrayRow->doctorAddress, 0, strrpos($locationArrayRow->doctorAddress, ' '));
                                              }else{
                                                echo $locationArrayRow->doctorAddress;
                                              }
                                            ?>
                                          </span>
                                      </div>  
                                    </div>
                                     <?php } ?>
                                    <div class="w100p clearfix">         
                                        <?php if($locationArrayRow->phone!=""){ ?>
                                      <div class="second-one">
                                          <img src="<?php echo base_url(); ?>images/mobile-icon-s.png" alt="">
                                      </div>
                                      <div class=" second-one">
                                          <span class="phone">
                                            <?php echo $locationArrayRow->phone;?> 
                                          </span>
                                      </div>
                                        <?php } ?>
                                    </div>
                                    <div class="w100p clearfix">                                    
                                      <div class="third-one">
                                      <?php if($locationArrayRow->speciality_id!=""){ ?>
                                        <img alt="" src="<?php echo base_url(); ?>images/icon/speciality/<?php echo $locationArrayRow->speciality_id; ?>.png">
                                      <?php } ?>
                                      </div>                                    
                                    </div> 
                                  </div>
                                </div>  
                                    <div class="w100p clearfix btn-box"> 
                                        <div class="col-sm-12 pull-left col-xs-12"> 
                                          <div class="flip text-right">   
                                              <input type="hidden" name="userId" class="userId<?php echo $a; ?>" value="<?php echo $this->session->userdata("user_id"); ?>">
                                              <input type="hidden" name="doctorId" class="doctorId<?php echo $a; ?>" value="<?php echo $locationArrayRow->doctor_id; ?>">
                                              <input type="hidden" name="workId" class="workId<?php echo $a; ?>" value="<?php echo $locationArrayRow->work_id; ?>">

                                              <?php if($this->session->userdata('user_login')==1) { 
                                                if(isset($locationArrayRow->work_type)) { ?>
                                                <button type="button" class="bookmarkwork_type<?php echo $a; ?> <?php if($bookmarkedRowWork2->status==0){ echo 'bookmarks-plus-icon';}else{echo 'bookmarks-gray-plus-icon';}?>" name="bookmarkwork_type" value="<?php echo $bookmarkedRowWork2->worktype_bookmark_id;  ?>"></button>
                                                <?php } else{ ?>
                                                <button type="button" class="bookmark<?php echo $a; ?> <?php if($bookmarkedRow2->status==0){ echo 'bookmarks-plus-icon';}else{echo 'bookmarks-gray-plus-icon';}?>" name="bookmark" value="<?php echo $bookmarkedRow2->bookmark_id;  ?>"></button>
                                                <?php } ?>   
                                                
                                              
                                              <?php } ?>
                                              <?php if(isset($locationArrayRow->work_type)){?>
                                                <a href="<?php echo base_url(); ?>workProfile/index/<?php echo 
                                              $locationArrayRow->work_id; ?>" class="btn btn-default search-btn"><?php echo $this->lang->line('SearchSeeMore'); ?></a>
                                              <?php }else{?>
                                                <a href="<?php echo base_url(); ?>doctorProfile/index/<?php echo 
                                              $locationArrayRow->doctor_id; ?>" class="btn btn-default search-btn"><?php echo $this->lang->line('SearchSeeMore'); ?></a>
                                              <?php } ?>
                                            </div>
                                          </div>   
                                        </div>
                                    
                                                                   
                            </div>
                         </div>
                      </div>
                      <script>
                        $(".bookmarkwork_type<?php echo $a; ?>").click(function() {
                          $.ajax({
                            type: 'POST', 
                            url: "<?php echo base_url();?>search/bookmarkCreatework_type",
                            data: { userId: $('.userId<?php echo $a; ?>').val(),workId:$('.workId<?php echo $a; ?>').val()},
                            success: function(response){
                              location.reload();
                            }
                          });
                        });
                      </script>
                      <script>
                        $(".bookmark<?php echo $a; ?>").click(function() {
                          $.ajax({
                            type: 'POST', 
                            url: "<?php echo base_url();?>search/bookmarkCreate",
                            data: { userId: $('.userId<?php echo $a; ?>').val(),doctorId:$('.doctorId<?php echo $a; ?>').val()},
                            success: function(response){
                              location.reload();
                            }
                          });
                        });
                      </script>
                      <?php $i++; $a++; } } ?>
                         

                    </div>
                      <div class="col-sm-12 col-xs-12">
                          <ul class = "pagination"><?php echo $links; ?></ul>
                      </div>
                  </form>
             </div>
            </div>
            <div class="w-col w-col-3 text-right">         
            <?php foreach ($locationArray as $locationArrayMapRow) {
                      if(empty($locationArrayMapRow->google_map_latitude)){
                        $displayMap='none';
                      }else{
                        $displayMap='block';
                      } 
                  }  
                  if(count($locationArray) > 0)
                  {
              ?>   
              <div class="map-det" style="display:<?php echo $displayMap; ?>">
                <!-- <a href="javascript:void(0);" class="get-direction-link"><?php echo $this->lang->line('searchGetDirection'); ?></a> -->
              <div id="map">  </div>  
              </div>
                <?php } ?>
              <script type="text/javascript">
            var locations = [
              <?php $k=1; foreach ($locationArray as $locationArrayMapRow) { ?>
                [' ',<?php echo $locationArrayMapRow->google_map_latitude;?>,<?php echo $locationArrayMapRow->google_map_longtude;?>, <?php echo $k; ?>],
                <?php $k++; } ?>
              ];
              
              var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 6,
                center: new google.maps.LatLng(<?php echo $locationArrayMapRow->google_map_latitude;?>, <?php echo $locationArrayMapRow->google_map_longtude;?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
              });
              var infowindow = new google.maps.InfoWindow();
              var marker, i;
              for (i = 0; i < locations.length; i++) { 
                marker = new google.maps.Marker({
                  position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                  map: map
                });
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                  return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                  }
                })(marker, i));
              }
            </script>
              <div class="clearfix"></div>
            <div class="featured-doctor-det m-zero-t clearfix">
               <div class="top">
                  <h3 class="flip text-left clearfix"><?php echo $this->lang->line('searchFeaturedDoctor'); ?></h3>
               </div>
               <div class="bottom">
                 <ul class="clearfix">
                  <?php $j=1; foreach($featureArray as $feaureLocationRow){?>
                   <li class="flip text-left">
                    <a href="<?php echo base_url()?>doctorProfile/index/<?php echo $feaureLocationRow->doctor_id;?>">
                      <div class="img-box">
                        <?php if(empty($feaureLocationRow->photo)){ ?>
                          <img src="<?php echo base_url(); ?>uploads/user.jpg" alt="">
                        <?php }else{?>
                          <img src="<?php echo base_url(); ?>uploads/doctor_image/<?php echo $feaureLocationRow->photo; ?>" alt="">
                          <?php } ?>
                      </div>
                      <div class="details">
                          <h4 class="flip text-left clearfix">
                              <?php echo $feaureLocationRow->first_name .' '. $feaureLocationRow->last_name; ?>
                          </h4>
                          <div class="review-det">
                          
                               <input type="hidden" value="<?php echo floatval($feaureLocationRow->featureAverageScore); ?>"  id="frateViewVals<?php echo $j; ?>">
                              <div class="my-ratings-<?php echo $j; ?> small-rating"></div>
                             
<script>
$(function() {
  $(".my-ratings-<?php echo $j; ?>").starRating({
    totalStars: 5,
    emptyColor: 'lightgray',    
    activeColor: '#03878A',
    initialRating: $("#frateViewVals<?php echo $j; ?>").val(),
    strokeWidth: 0,
    useGradient: false,    
      readOnly: true,
      starSize:'20',
    callback: function(currentRating, $el){
       //alert('rated ' +  currentRating);
      //console.log('DOM Element ', $el);
    }
  });


});
</script>


                          <div class="flip text-right">
                            <span class="title">
                            <?php echo $feaureLocationRow->doctorReview; ?> <?php echo $this->lang->line('DPReview'); ?>                                          
                            </span>
                          </div> 
                          <div class="third-one">
                                
                          <?php if($feaureLocationRow->speciality_id!=""){ ?>
                            <img alt="" src="<?php echo base_url(); ?>images/icon/speciality/<?php echo $feaureLocationRow->speciality_id; ?>.png">
                            <?php } ?>
                          </div>
                          
                          </div>
                      </div>
                    </a>
                   </li>
                   <?php $j++; } ?>
                 </ul>
               </div>
            </div>
            </div>
          </div>          
      </div>
  </section>
</div>

<script>
$('#select-location option[value="41"]');
</script>
<script src="<?php echo base_url(); ?>/js/jquery.star-rating-svg.js"></script>
<style>
    .small-rating > .jq-star{
        width : 15px !important;
        height : 15px !important;
    }
</style>