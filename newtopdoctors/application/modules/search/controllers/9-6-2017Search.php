<?php

defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', '-1');
ini_set('max_allowed_packet', '1024M');
class Search extends MY_Controller {

    function __construct() {
        parent::__construct();
        header("Cache-Control: max-age=300, must-revalidate");       
  
        $this->load->model('home/User_master_model');
        $this->load->model('home/Bookmark_master_model');
        $this->load->model('home/Worktype_bookmark_master_model');
        $this->load->library('pagination');
        $this->load->helper("url");
        $this->load->model('Result_model');
	    $this->data['slug_class'] = "search";        

    }
    
    /**
     * index function for all search functionality start here
     * return mixed array resuly
     * return feature doctor list 
     * return doctor list & work list
     */
    function index($page=0) {



                $config['per_page'] = '10';
                $config['uri_segment'] = '3';
               /* if(empty($_GET)){
                    redirect(base_url().'search/globalSearch?typeofsearch=globalsearch');
                }*/
                $q = trim($this->input->get('typeofsearch'));
                
                $query_string = explode("?", $_SERVER['REQUEST_URI']);
                 $config['enable_query_strings']=TRUE;
                $getData = array($query_string[1]);
                $config['base_url'] = base_url()."search/index/";   
                $config['suffix'] = '?'.$query_string[1];
                $config['first_url'] = base_url().'search/index/?'.$query_string[1];

                /*$param = explode("&", $query_string[1]);                */
                $data['location']       =$_GET['location'];
                $data['speciality']     =$_GET['speciality'];
                $data['gender']         =$_GET['gender'];
                $data['searchname']     =trim($_GET['searchname']);
                $data['typeofsearch']   =$_GET['typeofsearch']; 
                $data['labtype']        =  $_GET['labtype'];
            if(strlen($q)>0){
               
                if($data['typeofsearch']=='Doctors')
                {     
                    $data['locationArray'] = $this->Result_model->fetch_doctors($config['per_page'],$page,$data);
                    $config['total_rows'] = $this->Result_model->result_count($data);    
                }
                elseif ($data['typeofsearch']=='Hospital' || $data['typeofsearch']=='Clinic' ||  $data['typeofsearch']=='Lab') 
                {  
                    $data["locationArray"] = $this->Result_model->fetch_work($config["per_page"], $page,$data);
                    $config['total_rows'] = $this->Result_model->result_work($data);
                }
                else{                  

                    $this->globalSearch($page=0);                    
                }
            
                
                

            }
            else{
               $this->globalSearch($page=0);
            }

            $this->pagination->initialize($config);                 
                $data["links"] = $this->pagination->create_links();   
        $data['featureArray'] = featuresArray();
        
        
        $this->__templateFront('search/index', $data,true);
    }
    
    /**
     * Doctor Search & doctor Link
     * if doctor search from the form & data into session
     * return all doctor list
     */
    function doctor()
    {
            $data['slug_class'] = "search"; 
            $siteLang=$this->session->userdata('site_lang');
            //echo "";
             /*if($_POST['typeofsearch']!="Doctors" || $this->session->userdata('typeofsearch')!="Doctors")
             {
                
             }*/
             if(isset($_GET) && !empty($_GET))            
             { 
                 
                $data['location']       =$_GET['location'];
                $data['speciality']     =$_GET['speciality'];
                $data['gender']         =$_GET['gender'];
                $data['searchname']     =trim($_GET['searchname']);
                $data['typeofsearch']   =$_POST['typeofsearch'];  
                $this->session->set_userdata('location',$_POST['location']);
                $this->session->set_userdata('speciality',$_POST['speciality']);
                $this->session->set_userdata('gender',$_POST['gender']) ;
                $this->session->set_userdata('searchname',$_POST['searchname']);
                $this->session->set_userdata('typeofsearch',$_POST['typeofsearch']);
                }
                else{            
                    
            $data['location']       =$this->session->userdata('location');
            $data['speciality']     =$this->session->userdata('speciality');
            $data['gender']         =$this->session->userdata('gender');
            $data['searchname']     =trim($this->session->userdata('searchname'));
            $data['typeofsearch']   =$this->session->userdata('typeofsearch');  
            
             }
             

            if($data['typeofsearch']=="")
            {
                $this->session->set_userdata('typeofsearch','Doctors');
                $data['typeofsearch'] = "Doctors";
            }
            $this->load->model('Result_model');
            $total_rows = $this->Result_model->result_count($data);
            $config = array();
            $config["base_url"] = base_url() . "search/doctor";
            $config["total_rows"] = $total_rows;
            $config["per_page"] = 10;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["locationArray"] = $this->Result_model->fetch_doctors($config["per_page"], $page,$data);
            $data["links"] = $this->pagination->create_links();
         $data['featureArray'] = featuresArray();
      
        
            $this->__templateFront('search/index', $data);
    }
    
    /**
     * get All doctors 
     * return for doctor links in header
     * return all doctor list at first time
     */
    function doctors()
    {
            $data[] = '';
            if($data['typeofsearch']=="")
            {
                $this->session->set_userdata('typeofsearch','Doctors');
                $data['typeofsearch'] = "Doctors";
            }
            $this->load->model('Result_model');
            $total_rows = $this->Result_model->result_count($data);
            $config = array();
            $config["base_url"] = base_url() . "search/doctors";
            $config["total_rows"] = $total_rows;
            $config["per_page"] = 10;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["locationArray"] = $this->Result_model->fetch_doctors($config["per_page"], $page,$data);
            $data["links"] = $this->pagination->create_links();
             $data['featureArray'] = featuresArray();
      
        $data['slug_class'] = "search"; 
            $this->__templateFront('search/index', $data);
    }
    
    /**
     * get all work
     * return mixed array
     */
    function work(){

            $siteLang=$this->session->userdata('site_lang');
            $data['location']       =$this->session->userdata('location');
            $data['speciality']     =$this->session->userdata('speciality');
            $data['gender']         =$this->session->userdata('gender');
            $data['searchname']     =trim($this->session->userdata('searchname'));
            $data['typeofsearch']   =$this->session->userdata('typeofsearch'); 
            $data['labtype']        =$this->session->userdata('labtype'); 
            $this->load->model('Result_model');
            $total_rows = $this->Result_model->result_work($data);

            $config = array();
            $config["base_url"] = base_url() . "search/work";
            $config["total_rows"] = $total_rows;
            $config["per_page"] = 10;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["locationArray"] = $this->Result_model->fetch_work($config["per_page"], $page,$data);
            $data["links"] = $this->pagination->create_links();
            $data['featureArray'] = featuresArray();
      
        $data['slug_class'] = "search";
            $this->__templateFront('search/index', $data);
    }

    
    function bookmarkCreate(){
       $userId = $_POST['userId'];
       $doctorId=$_POST['doctorId'];

       $BookmarkRowCheck=$this->db->get_where('bookmark_master',array('user_id'=>$userId,'doctor_id'=>$doctorId))->row();
       if(count($BookmarkRowCheck)>0){

            if($BookmarkRowCheck->status == '0'){

                $this->Bookmark_master_model->update($BookmarkRowCheck->bookmark_id,array('status'=>'1'));

            }else{

                $this->Bookmark_master_model->update($BookmarkRowCheck->bookmark_id,array('status'=>'0'));

            }  

        }else{

           $this->Bookmark_master_model->insert(array(
            'user_id'    => $userId,
            'doctor_id'  => $doctorId,
            'status'     => '1',
            ));
           
        }
        die();
    }

    function bookmarkCreatework_type(){
       $userId = $_POST['userId'];
       $workId = $_POST['workId'];

       $BookmarkRowCheck=$this->db->get_where('worktype_bookmark_master',array('user_id'=>$userId,'work_id'=>$workId))->row();
       if(count($BookmarkRowCheck)>0){

            if($BookmarkRowCheck->status == '0'){

                $this->Worktype_bookmark_master_model->update($BookmarkRowCheck->worktype_bookmark_id,array('status'=>'1'));

            }else{

                $this->Worktype_bookmark_master_model->update($BookmarkRowCheck->worktype_bookmark_id,array('status'=>'0'));

            }  
        }else{

           $this->Worktype_bookmark_master_model->insert(array(
            'user_id'    => $userId,
            'work_id'    => $workId,
            'status'     => '1',
            ));
        }
        die();
    }
    
    /**
     * Global search & filter Search on Search results form
     */
   function globalSearch($page=0) {
       
        
       

            /* New pagination configuration  */

                $config['per_page'] = '10';
                $config['uri_segment'] = '3';

                $q = trim($this->input->get('typeofsearch'));
                
                $query_string = explode("?", $_SERVER['REQUEST_URI']);
                 $config['enable_query_strings']=TRUE;
                $getData = array($query_string[1]);
                $config['base_url'] = base_url()."search/globalSearch/";   
                $config['suffix'] = '?'.$query_string[1];
                $config['first_url'] = base_url().'search/globalSearch/?'.$query_string[1];
                /* New pagination configuration end */

                if(isset($_GET['searchname'])){
                $search = $_GET['searchname'];    
                }
                else{
                    $search = "";
                }

                if(isset($_GET['location'])){
                $location = $_GET['location'];
                }
                else{
                $location = "";
                }
                if(isset($_GET['speciality'])){
                $speciality = $_GET['speciality'];
                }
                else{
                $location = "";
                }
                $data['typeofsearch'] = "globalsearch";
                $data['searchname'] = $search;
		
		        $siteLang=$this->session->userdata('site_lang');
                            
                $config['total_rows'] = $this->Result_model->doctorWorkUnionQuery($search, $siteLang,$speciality,$location);
            

                $data['search'] = $search;
                $data['location'] = $location;
                $data['speciality'] = $speciality;
                $data['lang'] = $siteLang;
                $data["locationArray"] = $this->Result_model->fetch_globalSearch($config["per_page"], $page,$data);
                $this->pagination->initialize($config);                 
                $data["links"] = $this->pagination->create_links(); 
                
           $data['featureArray'] = featuresArray();
              $data['slug_class'] = "search";
            $this->__templateFront('search/index', $data);
	     
	}
    public function wordfind(){
     
$word = findword('اسما');
echo "<prE>";
print_r($word);
echo "</pre>";
die;


        echo $search_string = "ياسر عبد الرحمن السباعى";
        echo "<br>";
     
$patterns     = array( "/(ا|أ|آ|إ)/","/(هـ|ة|ه)/", "/(ئـ|ئ)/","/(ؤ|و)/","/(ا|ى|ي)/"); 
$replacements = array( "[ا|أ|آ|إ]","[هـ|ة|ه]","[ئـ|ئ]","[ؤ|و]","[ا|ى|ي]" );   
$query_string = preg_replace($patterns, $replacements, $search_string);
echo $query_string;die;

/*SELECT first_name,last_name FROM doctor_master WHERE concat_ws(' ',first_name,last_name) REGEXP 'ياسر عبد الرحمن السباعى[ا|أ|آ|إ]';*/
    }
}
