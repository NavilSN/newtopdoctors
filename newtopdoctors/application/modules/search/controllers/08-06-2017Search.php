<?php

defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', '-1');
ini_set('max_allowed_packet', '1024M');
class Search extends MY_Controller {

    function __construct() {
        parent::__construct();
        header("Cache-Control: max-age=300, must-revalidate");       
  
        $this->load->model('home/User_master_model');
        $this->load->model('home/Bookmark_master_model');
        $this->load->model('home/Worktype_bookmark_master_model');
        $this->load->library('pagination');
        $this->load->helper("url");
        $this->load->model('Result_model');
	$this->data['slug_class'] = "search";        

    }
    
    /**
     * index function for all search functionality start here
     * return mixed array resuly
     * return feature doctor list 
     * return doctor list & work list
     */
    function index() {
        $siteLang=$this->session->userdata('site_lang');
        $data['title']='TopDoctors';
	$this->data['slug_class'] = "search"; 
        $data['locationArray1']=array();
        $this->load->model('Result_model');
        $this->session->set_userdata('specialitySearch',$_POST['specialitySearch']);
        $this->session->set_userdata('typeofsearch',$_POST['typeofsearch']);
        $this->session->set_userdata('searchData',$_POST['searchData']);
        
        if(isset($_POST['searchData']) || $this->session->userdata('typeofsearch'))
        {         
            $data['typeofsearch']=$this->session->userdata('typeofsearch');
            if(isset($_POST))            
            { 
              
                $data['location']       =$_POST['location'];
                $data['speciality']     =$_POST['speciality'];
                $data['gender']         =$_POST['gender'];
                $data['searchname']     =trim($_POST['searchname']);
                $data['typeofsearch']   =$_POST['typeofsearch']; 
                $data['labtype']        =  $_POST['labtype'];
                $this->session->set_userdata('location',$_POST['location']);
                $this->session->set_userdata('speciality',$_POST['speciality']);
                $this->session->set_userdata('gender',$_POST['gender']) ;
                $this->session->set_userdata('searchname',$_POST['searchname']);
                $this->session->set_userdata('typeofsearch',$_POST['typeofsearch']);
                $this->session->set_userdata('labtype',$_POST['labtype']);
            }
            else{

                $data['location']       =$this->session->userdata('location');
                $data['speciality']     =$this->session->userdata('speciality');
                $data['gender']         =$this->session->userdata('gender');
                $data['searchname']     =$this->session->userdata('searchname');
                $data['labtype']     =$this->session->userdata('labtype');
               
                //$data['typeofsearch']   =$this->session->userdata('typeofsearch');  
            }
            
            if($data['typeofsearch']=='Doctors')
            {  
                $total_rows = $this->Result_model->result_count($data);
                  //echo $this->db->last_query();
                $config = array();
                $config["base_url"] = base_url() . "search/doctor";
                $config["total_rows"] = $total_rows;
                $config["per_page"] = 10;
                $config["uri_segment"] = 3;

                $this->pagination->initialize($config);

                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data["locationArray"] = $this->Result_model->fetch_doctors($config["per_page"], $page,$data);
                $data["links"] = $this->pagination->create_links();
               //  echo "<pre>";
               // print_r($data);
               
                //echo $this->db->last_query();
                //echo "<prE>";
                // print_r($data["locationArray"]);
                //die;

            }elseif ($data['typeofsearch']=='Hospital' || $data['typeofsearch']=='Clinic' ||  $data['typeofsearch']=='Lab') 
            {  
                
                $total_rows = $this->Result_model->result_work($data);
                $config = array();
                $config["base_url"] = base_url() . "search/work";
                $config["total_rows"] = $total_rows;
                $config["per_page"] = 10;
                $config["uri_segment"] = 3;

                $this->pagination->initialize($config);

                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data["locationArray"] = $this->Result_model->fetch_work($config["per_page"], $page,$data);
               
               
               
                $data["links"] = $this->pagination->create_links();
                   
            }
        }else if(isset($_POST['specialitySearch']) || $this->session->userdata('specialitySearch')){    

                //$data['searchSpecialityValue']=$_POST['searchSpecialityValue'];
                $this->session->set_userdata('searchSpecialityValue',trim($_POST['searchSpecialityValue']));

                $data['searchSpecialityValue']=$this->session->userdata('searchSpecialityValue');
                //echo $data['searchSpecialityValue']; die();
                $total_rows = $this->Result_model->result_speciality($data);
                $config = array();
                $config["base_url"] = base_url() . "search/speciality";
                $config["total_rows"] = $total_rows;
                $config["per_page"] = 10;
                $config["uri_segment"] = 3;

                $this->pagination->initialize($config);

                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data["locationArray"] = $this->Result_model->fetch_speciality($config["per_page"], $page,$data);
                $data["links"] = $this->pagination->create_links();
        }

     
        $data['featureArray'] = featuresArray();
      
        
        $this->__templateFront('search/index', $data);
    }
    
    /**
     * Doctor Search & doctor Link
     * if doctor search from the form & data into session
     * return all doctor list
     */
    function doctor()
    {
$data['slug_class'] = "search"; 
            $siteLang=$this->session->userdata('site_lang');
            //echo "";
             if($_POST['typeofsearch']!="Doctors" || $this->session->userdata('typeofsearch')!="Doctors")
             {
                $this->session->set_userdata('typeofsearch','Doctors');
             }
             if(isset($_POST) && !empty($_POST))            
             { 
                 
                $data['location']       =$_POST['location'];
                $data['speciality']     =$_POST['speciality'];
                $data['gender']         =$_POST['gender'];
                $data['searchname']     =trim($_POST['searchname']);
                $data['typeofsearch']   =$_POST['typeofsearch'];  
                $this->session->set_userdata('location',$_POST['location']);
                $this->session->set_userdata('speciality',$_POST['speciality']);
                $this->session->set_userdata('gender',$_POST['gender']) ;
                $this->session->set_userdata('searchname',$_POST['searchname']);
                $this->session->set_userdata('typeofsearch',$_POST['typeofsearch']);
                }
                else{            
                    
            $data['location']       =$this->session->userdata('location');
            $data['speciality']     =$this->session->userdata('speciality');
            $data['gender']         =$this->session->userdata('gender');
            $data['searchname']     =trim($this->session->userdata('searchname'));
            $data['typeofsearch']   =$this->session->userdata('typeofsearch');  
            
             }
             

            if($data['typeofsearch']=="")
            {
                $this->session->set_userdata('typeofsearch','Doctors');
                $data['typeofsearch'] = "Doctors";
            }
            $this->load->model('Result_model');
            $total_rows = $this->Result_model->result_count($data);
            $config = array();
            $config["base_url"] = base_url() . "search/doctor";
            $config["total_rows"] = $total_rows;
            $config["per_page"] = 10;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["locationArray"] = $this->Result_model->fetch_doctors($config["per_page"], $page,$data);
            $data["links"] = $this->pagination->create_links();
         $data['featureArray'] = featuresArray();
      
        
            $this->__templateFront('search/index', $data);
    }
    
    /**
     * get All doctors 
     * return for doctor links in header
     * return all doctor list at first time
     */
    function doctors()
    {
            $data[] = '';
            if($data['typeofsearch']=="")
            {
                $this->session->set_userdata('typeofsearch','Doctors');
                $data['typeofsearch'] = "Doctors";
            }
            $this->load->model('Result_model');
            $total_rows = $this->Result_model->result_count($data);
            $config = array();
            $config["base_url"] = base_url() . "search/doctors";
            $config["total_rows"] = $total_rows;
            $config["per_page"] = 10;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["locationArray"] = $this->Result_model->fetch_doctors($config["per_page"], $page,$data);
            $data["links"] = $this->pagination->create_links();
             $data['featureArray'] = featuresArray();
      
        $data['slug_class'] = "search"; 
            $this->__templateFront('search/index', $data);
    }
    
    /**
     * get all work
     * return mixed array
     */
    function work(){

            $siteLang=$this->session->userdata('site_lang');
            $data['location']       =$this->session->userdata('location');
            $data['speciality']     =$this->session->userdata('speciality');
            $data['gender']         =$this->session->userdata('gender');
            $data['searchname']     =trim($this->session->userdata('searchname'));
            $data['typeofsearch']   =$this->session->userdata('typeofsearch'); 
            $data['labtype']        =$this->session->userdata('labtype'); 
            $this->load->model('Result_model');
            $total_rows = $this->Result_model->result_work($data);

            $config = array();
            $config["base_url"] = base_url() . "search/work";
            $config["total_rows"] = $total_rows;
            $config["per_page"] = 10;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["locationArray"] = $this->Result_model->fetch_work($config["per_page"], $page,$data);
            $data["links"] = $this->pagination->create_links();
            $data['featureArray'] = featuresArray();
      
        $data['slug_class'] = "search";
            $this->__templateFront('search/index', $data);
    }

    function speciality()
    {
            $siteLang=$this->session->userdata('site_lang');
            
            $data['searchSpecialityValue']=trim($this->session->userdata('searchSpecialityValue'));

            $this->load->model('Result_model');
            $total_rows = $this->Result_model->result_speciality($data);
            $config = array();
            $config["base_url"] = base_url() . "search/speciality";
            $config["total_rows"] = $total_rows;
            $config["per_page"] = 10;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["locationArray"] = $this->Result_model->fetch_speciality($config["per_page"], $page,$data);
            $data["links"] = $this->pagination->create_links();
            
        $data['featureArray'] = featuresArray();
      

            $this->__templateFront('search/index', $data);
    }
    function bookmarkCreate(){
       $userId = $_POST['userId'];
       $doctorId=$_POST['doctorId'];

       $BookmarkRowCheck=$this->db->get_where('bookmark_master',array('user_id'=>$userId,'doctor_id'=>$doctorId))->row();
       if(count($BookmarkRowCheck)>0){

            if($BookmarkRowCheck->status == '0'){

                $this->Bookmark_master_model->update($BookmarkRowCheck->bookmark_id,array('status'=>'1'));

            }else{

                $this->Bookmark_master_model->update($BookmarkRowCheck->bookmark_id,array('status'=>'0'));

            }  

        }else{

           $this->Bookmark_master_model->insert(array(
            'user_id'    => $userId,
            'doctor_id'  => $doctorId,
            'status'     => '1',
            ));
           
        }
        die();
    }

    function bookmarkCreatework_type(){
       $userId = $_POST['userId'];
       $workId = $_POST['workId'];

       $BookmarkRowCheck=$this->db->get_where('worktype_bookmark_master',array('user_id'=>$userId,'work_id'=>$workId))->row();
       if(count($BookmarkRowCheck)>0){

            if($BookmarkRowCheck->status == '0'){

                $this->Worktype_bookmark_master_model->update($BookmarkRowCheck->worktype_bookmark_id,array('status'=>'1'));

            }else{

                $this->Worktype_bookmark_master_model->update($BookmarkRowCheck->worktype_bookmark_id,array('status'=>'0'));

            }  
        }else{

           $this->Worktype_bookmark_master_model->insert(array(
            'user_id'    => $userId,
            'work_id'    => $workId,
            'status'     => '1',
            ));
        }
        die();
    }
    
    /**
     * Global search & filter Search on Search results form
     */
   function globalSearch() {
       
       
       if(isset($_POST['searchData']) || $this->session->userdata('typeofsearch')=="globalsearch")
       {

           
                 if(isset($_POST['searchname']))
                 {
                    $search = $_POST['searchname'];
                    $location = $_POST['location'];
                    $speciality = $_POST['speciality'];
                    $this->session->set_userdata('globalsearch',$_POST['searchname']);      
                    $this->session->set_userdata('location',$_POST['location']);
                    $this->session->set_userdata('speciality',$_POST['speciality']);
                    
                 }
                 else{                     
                     $search = $this->session->userdata('globalsearch');                     
                     $location = $this->session->userdata('location');
                     $speciality = $this->session->userdata('speciality');
                 }
                 $this->session->set_userdata('typeofsearch',"globalsearch");
                 $data['typeofsearch'] = "globalsearch";
                 $data['searchname'] = $search;
		//$userId = $this->post('userId');
		$siteLang=$this->session->userdata('site_lang');
                              //  $page=$this->post('page');
              //  $perPage="10";
		//$result = $this->Result_model->globalSearch($search, $siteLang);
                $total_rows_work = $this->Result_model->workMasterGlobalSearchNumRows($search, $siteLang,$speciality,$location);
                $total_rows_doctor = $this->Result_model->doctorMasterGlobalSearchNumRows($search, $siteLang,$speciality,$location);
                $total_rows = ($total_rows_work + $total_rows_doctor);                
                
                $data['search'] = $search;
                $data['location'] = $location;
                $data['speciality'] = $speciality;
                $data['lang'] = $siteLang;
                $config = array();
                $config["base_url"] = base_url() . "search/globalSearch";
                if($search!="" || $location!="" || $speciality!="")
                {
                $config["total_rows"] = $total_rows;
                }
                else{
                  $config["total_rows"] = ($total_rows - 60);       
                }
                $config["per_page"] = 20;
                $config["uri_segment"] = 3;
                
                $this->pagination->initialize($config);
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                
                $data["locationArray"] = $this->Result_model->fetch_globalSearch($config["per_page"], $page,$data);
            
            //echo "<pre>";
            //print_r($data["locationArray"]);
            //die;
                if($total_rows > 0)
                {
                $data["links"] = $this->pagination->create_links();
                }
                
           $data['featureArray'] = featuresArray();
              $data['slug_class'] = "search";
            $this->__templateFront('search/index', $data);
	}     	
            
	}
}
