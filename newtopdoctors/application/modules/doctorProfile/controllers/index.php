<div class="search-full-box">
  <section class="text-center clearfix">
  <!-- <img src="images/doctors-banner.jpg" class="img-responsive"> -->
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-12 text-left">
               <!-- <form class="form-inline" action="<?php echo base_url();?>search" method="post">
                  <div class="form-group">
                    <span class="flip search-icon"> <i class="fa fa-search"></i></span>
                    <input type="text" name="searchSpecialityValue" class="form-control" id="" 
                    placeholder="<?php // echo $this->lang->line('searchPlaceHolder'); ?>" value="">
                  </div>
                  <div class="form-control-btn-box">
                    <button type="submit" name="specialitySearch" class="btn btn-default search-btn"><?php echo $this->lang->line('DPSearchBTN'); ?></button>                   
                  </div>
                </form>-->
                <form class="form-inline" action="<?php echo base_url();?>search/globalSearch" method="post">
                  <div class="form-group">
                    <span class="flip search-icon"> <i class="fa fa-search"></i></span>                    
                    <input type="text" name="searchname" class="form-control" id="" placeholder="<?php //echo $this->lang->line('searchPlaceHolder'); ?>" value="<?php  if($typeofsearch=="globalsearch"){ echo $searchname; } ?>">
                  </div>
                  <div class="form-control-btn-box">
                    <button type="submit" name="searchData" class="btn btn-default search-btn"><?php echo $this->lang->line('searchbtn'); ?></button>                   
                  </div>
                </form>
            </div>
          </div>          
      </div>
  </section>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/star-rating-svg.css">
    
<style type="text/css">
	label > input{ /* HIDE RADIO */
  visibility: hidden; /* Makes input not-clickable */
  position: absolute; /* Remove input from document flow */
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
}
label > input:checked + img{ /* (RADIO CHECKED) IMAGE STYLES */
  border:2px solid #008F9E;
   border-radius: 20px;
}
ul.b-footer-socials li {
    display: inline;
    list-style-type: none;
    margin-right: 3px;
    float: left;
}
</style>
<div class="filters-box m50t profile-only-doctor">
  <section class="text-center clearfix">
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-9 text-left">

                     <!-- doctor profile details start -->
                     <div class="doctor-profile clearfix">
                          <form role="form" class="form-horizontal">   
                            <div class="form-group">
                              <div class="col-sm-12 pull-left col-xs-12">
                                 <div class="doctor-details clearfix">
                                    <div class="left-box pull-left">
                                     <!--  <?php if(empty($doctorDetail->photo)){?>
                                      <img src="<?php echo base_url(); ?>uploads/user.jpg" alt="">
                                      <?php }else{ ?>
                                        <img src="<?php echo base_url(); ?>uploads/doctor_image/<?php echo $doctorDetail->photo; ?>" alt=""><?php } ?> -->

                                        <img src="<?php echo base_url(); ?>images/Doctors.png" alt="">
                                    </div>
                                    <div class="right-box pull-right">
                                        <h2 class="flip text-left clearfix"><?php echo $doctorDetail->first_name .' '.$doctorDetail->last_name; ?> </h2>
                                        <div class="border-divider w300"></div> 
                                        <h3 class="flip text-left clearfix">
                                          <?php //echo $doctorDetail->name_en; ?>
                                        </h3>
                                        <?php if($doctorDetail->major!=""){ ?>
                                        <h3 class="flip text-left clearfix">
                                          <?php echo $this->lang->line('Major'); ?> 
                                        </h3>
                                        <h3 class="color-green font-Medium flip text-left clearfix">
                                          <?php echo $doctorDetail->major; ?>
                                        </h3>
                                        <?php } ?>
                                        <?php if($doctorDetail->biography!=""){ ?>
                                        <h3 class="flip text-left clearfix">
                                          
                                          <?php echo $this->lang->line('Biography'); ?> 
                                        </h3>
                                        <h3 class="color-green font-Medium flip text-left clearfix">
                                          <?php echo $doctorDetail->biography; ?>
                                        </h3>
                                        <?php } ?>
                                         <div class="doctor-address-det flip text-left clearfix">
                                             <?php if($doctorDetail->address!=""){ ?>
                                            <div class="w100p clearfix">
                                                  <div class="first-one location-icon flip pull-left text-left">
                                                    <img src="<?php echo base_url(); ?>images/location-icon-new.png" alt="">
                                                  </div>
                                              <div class="first-one flip pull-left">
                                                <span class="address"><?php echo $doctorDetail->address; if($doctorDetail->Lname!=""){ echo ", ".$doctorDetail->Lname; } ?>
                                                </span>
                                              </div>  
                                            </div>
                                             <?php } ?>
                                            <?php if($doctorDetail->phone_number!=""){ ?>
                                            <div class="w100p clearfix">                                    
                                              <div class="second-one doctor-landline-num flip pull-left text-left">
                                                  <img src="<?php echo base_url(); ?>images/mobile-icon-new.png" alt="">
                                              </div>
                                              <div class=" second-one flip pull-left">
                                                  <span class="phone">
                                                  <?php echo $doctorDetail->phone_number; ?></span>
                                              </div>
                                            </div>
                                            <?php } ?>
                                             <?php if($doctorDetail->mobile_number!=""){ ?>
                                            <div class="w100p clearfix">                                    
                                              <div class="second-one doctor-personal-m flip pull-left text-left">
                                                  <img src="<?php echo base_url(); ?>images/mobile-icon-personal.png" alt="">
                                              </div>
                                              <div class=" second-one flip pull-left">
                                                <span class="phone">
                                                <?php echo $doctorDetail->mobile_number; ?>               
                                                </span>
                                              </div>
                                            </div>
                                             <?php } ?>
                                            
                                            

                                             <div class="w100p clearfix">                                    
                                              <div class="second-one email-icon flip pull-left text-left">
                                                <?php if ( $doctorDetail->work_type == 'Clinic') {?>
                                                  <img src="<?php echo base_url(); ?>images/add-clinic-hover.png" alt="">
                                                  <?php } ?>
                                                  <?php if ( $doctorDetail->work_type == 'Lab') {?>
                                                  <img src="<?php echo base_url(); ?>images/add-lab-hover.png" alt="">
                                                  <?php } ?>
                                                  <?php if ( $doctorDetail->work_type == 'Hospital') {?>
                                                  <img src="<?php echo base_url(); ?>images/add-hospital-hover.png" alt="">
                                                  <?php } ?>                                                  
                                              </div>
                                              <div class="second-one flip pull-left">
                                                  <span class="email">
                                                    <?php echo $this->lang->line($doctorDetail->work_type); ?>
                                                  </span>
                                              </div>
                                               </div>

                                              <div class="w100p clearfix">
                                                  <?php if($doctorDetail->speciality!=""){ ?>
                                              <div class="second-one email-icon flip pull-left text-left">
                                                  <img src="<?php echo base_url(); ?>images/doctors-icon.png" alt="">
                                              </div>
                                              <div class="second-one flip pull-left">
                                                  <span class="email">
                                                    <?php echo $doctorDetail->speciality; ?>
                                                  </span>
                                              </div>
                                                  <?php } ?>
                                            </div>
                                            <input type="hidden" name="userId" class="userId<?php echo $a; ?>" value="<?php echo $this->session->userdata("user_id"); ?>">
                                              <input type="hidden" name="doctorId" class="doctorId" value="<?php echo $doctorDetail->doctor_id; ?>">
                                            <?php $bookmarkedRow2= $this->db->get_where('bookmark_master',array('doctor_id'=>$doctorDetail->doctor_id,'user_id'=>$this->session->userdata("user_id")))->row(); 
                                             if($this->session->userdata('user_login')==1) { ?>
                                                <button type="button" class="Bookmark <?php if($bookmarkedRow2->status==0){ echo 'bookmarkBigIconBlue';}else{echo 'bookmarkBigIcongray';}?>" name="bookmark" value="<?php echo $bookmarkedRow2->bookmark_id;  ?>"></button>
                                                <?php } ?> 
                                                 <script>
                                                  $(".Bookmark").click(function() {
                                                    $.ajax({
                                                      type: 'POST', 
                                                      url: "<?php echo base_url();?>search/bookmarkCreate",
                                                      data: { userId: $('.userId').val(),doctorId:$('.doctorId').val()},
                                                      success: function(response){
                                                        location.reload();
                                                      }
                                                    });
                                                  });
                                                </script>  
                                            </div>   
                                    </div>
                                 </div>
                              </div>
                            </div>
                          </form>
                    </div>
                    <!-- doctor profile details end -->

                    <div class="border-divider fullwidth"></div>
                    <!-- ********** Doctor REVIEW rating start ********** -->
                      <div class="reviews-content clearfix flip text-left">
                          <h2 class="font-Medium clearfix"> 
                            <span class="flip pull-left"><?php echo $this->lang->line('DPReview'); ?> </span>
                            <span class="reviews-num flip pull-left"><?php echo $doctorDetail->reviewCount; ?></span>
                          </h2>                
                          <div class="border-divider"></div>
                          
                          <table class="rating-details">
                              <tbody>
                                  <tr>
                                    <th>
                                      <?php echo $this->lang->line('DPReputation'); ?>
                                    </th>
                                     <td>
                                      <div class="star-rat">
                                          <?php $reputation =  getRatingByDoctors('reputation',$doctorDetail->doctor_id); 
                                          
                                          ?>
                                          
                                          
                                      <input type="hidden" class="RateavgReputationVal" value="<?php echo $reputation->reputation; ?>">
                                      <div class="RateavgReputation"></div>
                                      <script>
                                        $(function () {
                                        $(".RateavgReputation").rateYo({
                                          "starWidth": "20px",
                                          "ratedFill": "#03878A",
                                          "rating" : <?php if($reputation[0]->reputation > 0){ echo $reputation[0]->reputation; }else{ echo "0"; } ?>,
                                          "readOnly":true
                                          })
                                        }); 
                                      </script>  
                                      </div>
                                       <div class="review-num"><?php echo $doctorDetail->countreputation; ?></div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>
                                      <?php echo $this->lang->line('DPClinic'); ?>
                                    </th>
                                    <td>
                                      <div class="star-rat">
                                          <?php $clinic =  getRatingByDoctors('clinic',$doctorDetail->doctor_id); 
                                          
                                          ?>
                                      <input type="hidden" class="RateavgClinicVal" value="<?php echo $doctorDetail->avgClinic; ?>">
                                      <div class="RateavgClinic"></div>
                                      <script>
                                        $(function () {
                                        $(".RateavgClinic").rateYo({
                                          "starWidth": "20px",
                                          "ratedFill": "#03878A",
                                          "rating" : <?php if($clinic[0]->clinic > 0){ echo $clinic[0]->clinic; }else{ echo "0"; } ?>,
                                          "readOnly":true
                                          })
                                        }); 
                                      </script>  
                                      </div>
                                      <div class="review-num"><?php echo $doctorDetail->countclinic; ?></div>
                                    </td>                     
                                  </tr>
                                  <tr>
                                    <th>
                                    <?php echo $this->lang->line('DPAvailability'); ?>
                                    </th>
                                    <td>
                                      <div class="star-rat">
                                          <?php $availability =  getRatingByDoctors('availability',$doctorDetail->doctor_id); ?>
                                      <input type="hidden" class="RateavgAvailabilityVal" value="<?php echo $doctorDetail->avgAvailability; ?>">
                                      <div class="RateavgAvailability"></div>
                                      <script>
                                        $(function () {
                                        $(".RateavgAvailability").rateYo({
                                          "starWidth": "20px",
                                          "ratedFill": "#03878A",
                                          "rating" : <?php if($availability[0]->availability > 0){ echo $availability[0]->availability; }else{ echo "0"; } ?>,
                                          "readOnly":true
                                          })
                                        }); 
                                      </script>
                                      </div>
                                      <div class="review-num"><?php echo $doctorDetail->countavailability; ?></div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>
                                      <?php echo $this->lang->line('DPApproachability'); ?>
                                    </th>
                                    <td>
                                      <div class="star-rat">
                                           <?php $approachability =  getRatingByDoctors('approachability',$doctorDetail->doctor_id); ?>
                                      <input type="hidden" class="RateavgApproachabilityVal" value="<?php echo $doctorDetail->avgApproachability; ?>">
                                      <div class="RateavgApproachability"></div>
                                      <script>
                                        $(function () {
                                        $(".RateavgApproachability").rateYo({
                                          "starWidth": "20px",
                                          "ratedFill": "#03878A",
                                          "rating" : <?php if($approachability[0]->approachability > 0){ echo $approachability[0]->approachability; }else{ echo "0"; } ?>,
                                          "readOnly":true
                                          })
                                        }); 
                                      </script>
                                      </div>
                                      <div class="review-num"><?php echo $doctorDetail->countapproachability; ?></div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>
                                      <?php echo $this->lang->line('DPTechnology'); ?>
                                    </th>
                                    <td>
                                      <div class="star-rat">
                                              <?php $technology =  getRatingByDoctors('technology',$doctorDetail->doctor_id); ?>
                                      <input type="hidden" class="RateavgTechnologyVal" value="<?php echo $doctorDetail->avgTechnology; ?>">
                                      <div class="RateavgTechnology"></div>
                                      <script>
                                        $(function () {
                                        $(".RateavgTechnology").rateYo({
                                          "starWidth": "20px",
                                          "ratedFill": "#03878A",
                                          "rating" : <?php if($technology[0]->technology > 0){ echo $technology[0]->technology; }else{ echo "0"; } ?>,
                                          "readOnly":true
                                          })
                                        }); 
                                      </script>
                                      </div>
                                      <div class="review-num"><?php echo $doctorDetail->counttechnology; ?></div>
                                    </td>
                                  </tr>
                                </tbody>
                          </table>
                      </div>
                    <!-- ********** Doctor REVIEW rating end ********** -->
                  <?php $a=1; foreach ($ReviewResult as $ReviewResultRow) {
                    if(!empty($ReviewResultRow->comment)){?>
                     <div class="excellent flip text-left clearfix">
                          <h3 class="clearfix">
                             <span class="title flip pull-left">
                                  <?php echo $ReviewResultRow->summary; ?>
                            </span>
                            <span class="star-rat flip pull-left">
                              <input type="hidden" class="rateViewVal<?php echo $a; ?>" value="<?php echo $ReviewResultRow->avgscore; ?>">
                              <div class="RateView<?php echo $a; ?>"></div>
                                  <script>
                                    $(function () {
                                    $(".RateView<?php echo $a; ?>").rateYo({
                                      "starWidth": "15px",
                                      "ratedFill": "#03878A",
                                      "rating" : $('.rateViewVal<?php echo $a; ?>').val(),
                                      "readOnly":true
                                      })
                                    }); 
                                  </script>               
                            </span>
                          </h3>
                          <div class="clearfix">

                              
                              <div class="flip pull-left col-md-11 clearfix">
                                  <div class="content flip text-left clearfix">
                                    <p class="flip text-left clearfix">
                                      <?php echo $ReviewResultRow->comment; ?>
                                    </p>
                                  </div>

                              </div>

                          </div>
                          <?php
                          //echo $ReviewResultRow->reviewIcon;
                          if($ReviewResultRow->reviewIcon!="" || $ReviewResultRow->reviewIcon!="0") {?>
                          <div class="excellent-doctor-pic">                          

                          <img src="<?php echo base_url(); ?>images/profile_files/doctor-pic-<?php echo $ReviewResultRow->reviewIcon; ?>.png">
                          </div>
                          <?php } ?>
                          <div class="review-profile-pic">
                              
                            <?php if(!empty($ReviewResultRow->photo) && file_exists(FCPATH.'uploads/user_image/'.$ReviewResultRow->photo && $ReviewResultRow->visibility=="1")){ ?>
                            <img src="<?php echo base_url(); ?>uploads/user_image/<?php echo $ReviewResultRow->photo; ?>">
                            <?php }else{ ?>
                            <img src="<?php echo base_url(); ?>uploads/user.jpg">
                              <?php } ?>
                            <span class="review-name-date">
                                <?php if($ReviewResultRow->visibility=="0"){ ?>
                             	<?php echo $ReviewResultRow->first_name.' '.$ReviewResultRow->last_name.", "; ?>
                                <?php }else{ ?>
                                    <?php echo $this->lang->line('AnonymousName'); ?>
                                <?php } ?>
                             	<?php echo " ".date('Y/m/d',strtotime($ReviewResultRow->date_created)); ?>
                            </span>
                          </div>
                          <div class="flip pull-left col-md-1 clearfix">
                                  <!--<div class="review-profile-pic">
                                    <img src="<?php echo base_url(); ?>uploads/user.jpg"> 
                                    <span class="title flip pull-left">
                              		<?php echo $ReviewResultRow->first_name.' '.$ReviewResultRow->last_name; ?> 
                            		</span>                                
                                  </div>-->

                              </div>
                          <div class="border-divider fullwidth light"></div>
                      </div>
                   <?php  }
                   ?>
                     
                  <?php $a++; } ?>
                  <!-- ********** REVIEW Form details start ********** -->
                  <?php  if($this->session->userdata('user_id')){?>
                      <div class="write-your-review clearfix">
                        <div class="clearfix">
                          <h3 class="title flip pull-left">
                            <span>
                              <?php echo $this->lang->line('DPWriteReview'); ?>  
                            </span>
                          </h3>
                          <div class="border-divider"></div>
                        </div>
                    <div class="w100p clearfix positionrelative">
                       <div class="flip pull-left w40p">
                        <?php $rowDoctorDisable=$this->db->get_where('doctors_rating',array('user_id'=>$this->session->userdata('user_id'),'doctor_id'=>$doctorDetail->doctor_id))->row(); ?>
                          <form class="review-frm" id="reviews-form" action="<?php echo base_url();?>doctorProfile/reviewCreate" method="post">
                            <input type="hidden" name="DoctorId" value="<?php echo $this->uri->segment(3); ?>">
                              <div class="form-group clearfix">
                                  <label class="flip text-left"><?php echo $this->lang->line('DPFRMName'); ?></label>
                                  <input type="text" class="form-control" name="reviewName" id="reviewName" value="<?php echo $this->session->userdata('user_firstname') .' '.$this->session->userdata('user_lastname'); ?>" disabled>
                              </div>
                              <div class="form-group clearfix">
                                  <label class="flip text-left"><?php echo $this->lang->line('DPFRMSummary'); ?></label>
                                  <input type="text" class="form-control" name="reviewSummary" id="reviewSummary"<?php if(count($rowDoctorDisable)>0){?>disabled<?php } ?>>
                              </div>
                              <div class="form-group clearfix">
                                <label class="flip text-left"><?php echo $this->lang->line('DPReview'); ?></label>
                                <textarea class="form-control w100p" name="reviews" id="reviews" rows="4" <?php if(count($rowDoctorDisable)>0){?>disabled<?php } ?>></textarea>
                              </div>  
                              <div class="form-group clearfix">
                                  <label class="flip text-left displayblock w100p"><?php echo $this->lang->line('chooseicon'); ?></label>
                                  <div class="choose-doctor clearfix">
                                  <ul class="b-footer-socials">
                                  <li>
                                  <label>
								    <input type="radio"  name="reviewIcon" value="1" />
								    <img src="<?php echo base_url(); ?>images/profile_files/doctor-pic-1.png" title="<?php echo $this->lang->line('Hardtoreach'); ?>" >
								  </label></li>
                                    <li><label>
								    <input type="radio"   name="reviewIcon" value="2" />
                                     <img src="<?php echo base_url(); ?>images/profile_files/doctor-pic-2.png" title="<?php echo $this->lang->line('Amazing'); ?>" >
                                      </label></li>
                                      <li><label>
								    <input type="radio"  name="reviewIcon" value="3" />
                                      <img src="<?php echo base_url(); ?>images/profile_files/doctor-pic-3.png" title="<?php echo $this->lang->line('MakeHomeVisits'); ?>"></label></li>
                                      <li><label>
								    <input type="radio" name="reviewIcon" value="4" />
                                      <img src="<?php echo base_url(); ?>images/profile_files/doctor-pic-4.png" title="<?php echo $this->lang->line('GreatHelp'); ?>" ></label></li>
                                        <li><label>
								    <input type="radio" name="reviewIcon" value="5" />
                                      <img src="<?php echo base_url(); ?>images/profile_files/doctor-pic-5.png"  title="<?php echo $this->lang->line('AngryDoctor'); ?>" ></label></li>
                                        <li><label>
								    <input type="radio" name="reviewIcon" value="6" />
                                      <img src="<?php echo base_url(); ?>images/profile_files/doctor-pic-6.png" title="<?php echo $this->lang->line('Reachable'); ?>" >  </label></li>
                                        <li><label>
								    <input type="radio"  name="reviewIcon" value="7" />                                   
                                      <img src="<?php echo base_url(); ?>images/profile_files/doctor-pic-7.png" title="<?php echo $this->lang->line('OldDoctor'); ?>"></label></li>
                                    <li>    <label>
								    <input type="radio" name="reviewIcon" value="8" />
                                      <img src="<?php echo base_url(); ?>images/profile_files/doctor-pic-8.png" title="<?php echo $this->lang->line('Like'); ?>" >    </label></li>
                                      </ul>
                                  </div>
                                  <input type="hidden" name="Reputation" id="Reputation" value=""> 
                            <input type="hidden" name="Clinic" id="Clinic" value=""> 
                            <input type="hidden" name="Availability" id="Availability" value=""> 
                            <input type="hidden" name="Approachability" id="Approachability" value=""> 
                            <input type="hidden" name="Technology" id="Technology" value=""> 
                              </div>
                               <div class="form-group clearfix">
                                   <input type="checkbox" class="checkboxvisibility" name="visibility" value="0" /> 
                                   <label class="flip text-left anonymoustitle" ><?php echo $this->lang->line('Anonymous'); ?></label>                                
                                
                              </div> 
                              <div class="clearfix"></div>
                              <div class="form-group clearfix">

                              <div class="g-recaptcha" data-sitekey="6Ld8ZQsUAAAAALMKvbogwvGU7l57vWnxBIGoncgJ"></div>                    
                              </div>
                              <div class="clearfix"></div>
                              
                              <div class="form-group clearfix flip text-left w100p">
                                <button type="submit" class="btn btn-primary"<?php if($this->session->userdata('user_login')!=1 || count($rowDoctorDisable)>0) {echo 'disabled'; } ?> ><?php echo $this->lang->line('DPSubmitReview'); ?></button>
                              </div> 
                          </form>
                        </div>
                        <div class="flip pull-left w48p ml12p">
                        <?php if($this->session->userdata('user_login')=='1'){$readOnly='false';}else{$readOnly='true';}
                            
                            $countReviewRow=$this->db->get_where('doctors_rating',array('doctor_id'=>$this->uri->segment(3),'user_id'=>$this->session->userdata('user_id')))->row();
                           ?>

                           <table class="rating-details">
                            <tr>
                              <th>
                                <?php echo $this->lang->line('DPReputation'); ?>
                              </th>
                               <td>
                                <div class="star-rat">
                                  <div class="Reputation"></div>
                                  <?php if($countReviewRow->reputation == 0 && $this->session->userdata('user_login')=='1'){
                                      $readOnly='false';         
                                  }else{
                                      $readOnly='true';
                                  } ?>
                                  <script>  
                                    $(function () {
                                      $(".Reputation").rateYo({"starWidth": "15px",
                                        "ratedFill": "#03878A",
                                        "readOnly":<?php echo $readOnly; ?>,
                                        "fullStar": true,
                                        "rating" : <?php if(empty($countReviewRow->reputation)) echo 0; else echo $countReviewRow->reputation; ?>,
                                        onSet: function (rating, rateYoInstance) {
                                          //alert('Thanks for rate');
                                          $('#Reputation').append().val(rating);
                                        }
                                      });
                                    });
                                  </script>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <th>
                              <?php echo $this->lang->line('DPClinic'); ?>
                              </th>
                              <td>
                                <div class="star-rat">
                                  <div class="Clinic"></div>
                                  <?php if($countReviewRow->clinic == 0 && $this->session->userdata('user_login')=='1'){
                                      $readOnly='false';         
                                  }else{
                                      $readOnly='true';
                                  } ?>
                                  <script>
                                      //alert(<?php echo $countReviewRow->clinic; ?>);
                                    $(function () {
                                      $(".Clinic").rateYo({"starWidth": "15px",
                                        "ratedFill": "#03878A",
                                        "readOnly":<?php echo $readOnly; ?>,
                                        "fullStar": true,
                                        "rating" : <?php if(empty($countReviewRow->clinic)) echo 0; else echo $countReviewRow->clinic; ?>,
                                        onSet: function (rating, rateYoInstance) {
                                          //alert('Thanks for rate');
                                          $('#Clinic').append().val(rating);
                                        }
                                      });
                                    });
                                  </script>
                                  </div>
                                
                              </td>                     
                            </tr>
                            <tr>
                              <th>
                                <?php echo $this->lang->line('DPAvailability'); ?>
                              </th>
                              <td>
                                <div class="star-rat">
                                  <div class="Availability"></div>
                                  <?php if($countReviewRow->availability == 0 && $this->session->userdata('user_login')=='1'){
                                      $readOnly='false';         
                                  }else{
                                      $readOnly='true';
                                  } ?>
                                  <script>
                                    $(function () {
                                      $(".Availability").rateYo({"starWidth": "15px",
                                        "ratedFill": "#03878A",
                                        "readOnly":<?php echo $readOnly; ?>,
                                        "fullStar": true,
                                        "rating" : <?php if(empty($countReviewRow->availability)) echo 0; else echo $countReviewRow->availability; ?>,
                                        onSet: function (rating, rateYoInstance) {
                                        //alert('Thanks for rate');
                                        $('#Availability').append().val(rating);
                                        }
                                      });
                                    });
                                  </script> 
                                </div>
                                
                              </td>
                            </tr>
                            <tr>
                              <th>
                                <?php echo $this->lang->line('DPApproachability'); ?>
                              </th>
                              <td>
                                <div class="star-rat">
                                  <div class="Approachability"></div>
                                  <?php if($countReviewRow->approachability == 0 && $this->session->userdata('user_login')=='1'){
                                      $readOnly='false';         
                                  }else{
                                      $readOnly='true';
                                  } ?>
                                  <script>
                                    $(function () {
                                      $(".Approachability").rateYo({"starWidth": "15px",
                                        "ratedFill": "#03878A",
                                        "readOnly":<?php echo $readOnly; ?>,
                                        "fullStar": true,
                                        "rating" : <?php if(empty($countReviewRow->approachability)) echo 0; else echo $countReviewRow->approachability; ?>,
                                        onSet: function (rating, rateYoInstance) {
                                          $('#Approachability').append().val(rating);
                                        }
                                      });
                                    });
                                  </script>  
                                </div>
                              
                              </td>
                            </tr>
                            <tr>
                              <th>
                                <?php echo $this->lang->line('DPTechnology'); ?>
                              </th>
                              <td>
                                <div class="star-rat">
                                  <div class="Technology"></div>
                                  <?php if($countReviewRow->technology == 0 && $this->session->userdata('user_login')=='1'){
                                      $readOnly='false';         
                                  }else{
                                      $readOnly='true';
                                  } ?>
                                  <script>
                                    $(function () {
                                      $(".Technology").rateYo({"starWidth": "15px",
                                        "ratedFill": "#03878A",
                                        "readOnly":<?php echo $readOnly; ?>,
                                        "fullStar": true,
                                        "rating" : <?php if(empty($countReviewRow->technology)) echo 0; else echo $countReviewRow->technology; ?>,
                                        onSet: function (rating, rateYoInstance) {
                                          $('#Technology').append().val(rating);
                                        }
                                      });
                                    });
                                  </script> 
                                </div>
                                
                              </td>
                            </tr>
                          </table>
                          <form id="RateInsert">
                            
                            <input type="hidden" name="RateValueDoctorId" id="RateValueDoctorId" value="<?php echo $this->uri->segment(3); ?>"> 
                            <input type="hidden" name="RateValueUserId" id="RateValueUserId" value="<?php echo $this->session->userdata('user_id'); ?>"> 
                          </form>


                          
                         </div>
                    </div> 
                            
                      </div>
                    <?php }else{ ?>
                      <div class="write-your-review clearfix">
                        <div class="clearfix">
                          <h3 class="title flip pull-left">
                            <span>
                              <?php echo $this->lang->line('reviewlogin'); ?>  
                                <a id="signinopen" class="w-nav-link menu-li signIn-link" data-target="#loginModal" data-toggle="modal" href="javascript:;" style="max-width: 940px;">
<?php echo $this->lang->line('menuSignIn'); ?>
                                    
                                    </a>
                            </span>
                          </h3>
                          <div class="border-divider"></div>
                        </div>
                        </div>
                  <?php } ?>
        <!-- ********** REVIEW Form details End ********** -->              
            </div>
            <!-- Right side part -->
            <div class="w-col w-col-3 text-right"> 
              <?php if(empty($doctorDetail->google_map_latitude)){
                        $displayMap='none';
                    }else{
                        $displayMap='block';
                    }          ?>
              <div class="map-det clearfix" style="display:<?php echo $displayMap; ?>" >
               <div class="top">
                  <h3 class="flip text-left clearfix"><?php echo $this->lang->line('DPOnMap'); ?></h3>
               </div>
               <div class="bottom">
                  <div id="map"></div>  
               </div>
              </div>
              <div class="clearfix"></div>
              <div class="featured-doctor-det clearfix">
               <div class="top">
                  <h3 class="flip text-left clearfix"><?php echo $this->lang->line('searchFeaturedDoctor'); ?></h3>
               </div>
               <div class="bottom">
                 <ul class="clearfix">
                  <?php $k=1; foreach($featureArray as $feaureLocationRow){?>
                  <a href="<?php echo base_url()?>doctorProfile/index/<?php echo $feaureLocationRow->doctor_id;?>">
                   <li class="flip text-left">
                      <div class="img-box">
                        <?php if(empty($feaureLocationRow->photo)){ ?>
                          <img src="<?php echo base_url(); ?>uploads/user.jpg" alt="" />
                        <?php }else{?>
                          <img src="<?php echo base_url(); ?>uploads/doctor_image/<?php echo $feaureLocationRow->photo; ?>" alt="" />
                          <?php } ?>
                      </div>
                      <div class="details">
                          <h4 class="flip text-left clearfix">
                              <?php echo $feaureLocationRow->first_name .' '. $feaureLocationRow->last_name; ?>
                          </h4>
                          <div class="review-det">
                              <?php // $res=  getReview($feaureLocationRow->doctor_id); 
                                  /*echo "<pre>";
                                  foreach($res as $frow):
                                  print_r($frow->average_score);
                                  endforeach;
                                  echo "</pre>";
                                  */
                                  ?>
                              <input type="hidden" value="<?php echo floatval($feaureLocationRow->featureAverageScore); ?>"  id="frateViewVal<?php echo $k; ?>">
                              <div class="my-rating-<?php echo $k; ?>"></div>
                              
<script>
$(function() {


  $(".my-rating-<?php echo $k; ?>").starRating({
    totalStars: 5,
    emptyColor: 'lightgray',    
    activeColor: '#03878A',
    initialRating: $("#frateViewVal<?php echo $k; ?>").val(),
    strokeWidth: 0,
    useGradient: false,    
      readOnly: true,
    callback: function(currentRating, $el){
    //  alert('rated ' +  currentRating);
      //console.log('DOM Element ', $el);
    }
  });


});
</script>

                          <!--<div class="flip text-left">
                              <input type="hidden" value="<?php echo floatval($feaureLocationRow->featureAverageScore); ?>"  id="frateViewVal<?php echo $k; ?>">
                              
                            <div  id="rateYoD<?php echo $k; ?>"></div>
                          </div>
                                <script>
                                    $(document).ready(function () {
                                        var rating = $('#frateViewVal<?php echo $k; ?>').val();
                                 //alert(rating);
                                 console.log(rating);
                                    $("#rateYoD<?php echo $k; ?>").rateYo({      
                                      "starWidth":"15px",
                                      "ratedFill": "#03878A",
                                      "rating" : $('#frateViewVal<?php echo $k; ?>').val(),  
                                      "readOnly":true
                                      });
                                       var total_width = parseInt(rating*55);                                      
                                       $("#rateYoD<?php echo $k; ?>").css({
                                         width:total_width+"%"
                                      });
                                    }); 
                                    
                                  </script> -->
                          

                          <div class="flip text-right">
                            <span class="title">
                              <?php echo $feaureLocationRow->doctorReview; ?>
                              <?php echo $this->lang->line('DPReview'); ?>
                            </span>
                          </div> 
                          <div class="third-one">
                          <?php if($feaureLocationRow->speciality_id!=""){ ?>
                            <img alt="" src="<?php echo base_url(); ?>images/icon/speciality/<?php echo $feaureLocationRow->speciality_id; ?>.png">
                            <?php } ?>
                          </div>                                   
                          </div>
                      </div>
                    </a>
                   </li>
                              
                   <?php $k++; } ?>
                 </ul>
               </div>
            </div>
          </div>
        </div>          
      </div>
  </section>
</div>
<div class="mt50 clearfix"></div> 
<script type="text/javascript">
  var locations = [
  [' ',<?php echo $doctorDetail->google_map_latitude;?>,<?php echo $doctorDetail->google_map_longtude;?>,1],];
  var map = new google.maps.Map(document.getElementById('map'), {
  zoom: <?php echo $doctorDetail->google_map_zoom; ?>,
  center: new google.maps.LatLng(<?php echo $doctorDetail->google_map_latitude;?>, <?php echo $doctorDetail->google_map_longtude;?>),
  mapTypeId: google.maps.MapTypeId.ROADMAP});
  var infowindow = new google.maps.InfoWindow();
  var marker, i;
  for (i = 0; i < locations.length; i++) { 
    marker = new google.maps.Marker({
    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
    map: map
  });
  google.maps.event.addListener(marker, 'click', (function(marker, i) {
  return function() {
    infowindow.setContent(locations[i][0]);
    infowindow.open(map, marker);
  }
})(marker, i));
}
</script>
<script>
/*
setInterval(function(){ 
  var Reputation=$('#Reputation').val(); 
  var Clinic=$('#Clinic').val(); 
  var Availability=$('#Availability').val(); 
  var Approachability=$('#Approachability').val(); 
  var Technology=$('#Technology').val(); 
  var DoctorId=$('#RateValueDoctorId').val();
  var UserId=$('#RateValueUserId').val();
  if(Reputation != "" ){
      $.ajax({
      type: 'POST', 
      url: "<?php echo base_url();?>doctorProfile/userRateUpdate",
      data: { 'Reputation':Reputation,'doctorId':DoctorId,'userId':UserId },
         success: function(response){
        }
      });
    }
    if(Clinic != "" ){
      $.ajax({
      type: 'POST', 
      url: "<?php echo base_url();?>doctorProfile/userRateUpdate",
      data: { 'Clinic':Clinic,'doctorId':DoctorId,'userId':UserId },
      success: function(response){
        }
      });
    }
    if(Availability != "" ){
      $.ajax({
      type: 'POST', 
      url: "<?php echo base_url();?>doctorProfile/userRateUpdate",
      data: { 'Availability':Availability,'doctorId':DoctorId,'userId':UserId },
      success: function(response){
        }
      });
    }
    if(Approachability != "" ){
      $.ajax({
      type: 'POST', 
      url: "<?php echo base_url();?>doctorProfile/userRateUpdate",
      data: { 'Approachability':Approachability,'doctorId':DoctorId,'userId':UserId },
      success: function(response){
        }
      });
    }
    if(Technology != "" ){
      $.ajax({
      type: 'POST', 
      url: "<?php echo base_url();?>doctorProfile/userRateUpdate",
      data: { 'Technology':Technology,'doctorId':DoctorId,'userId':UserId },
      success: function(response){
        }
      });
    }
}, 1000);*/
</script>

<script>
$(document).ready(function () {
      /* Validation for Login   */
        $("#reviews-form").validate({
            rules: {
                reviewSummary:"required",
                reviews:"required"
                
                },
            messages: {
                reviewSummary: {required: "Enter summary"},
                reviews: {required: "Enter review"}
                
            }
        });
    });
</script>
<script src="<?php echo base_url(); ?>/js/jquery.star-rating-svg.js"></script>
