<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DoctorProfile extends MY_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Africa/Cairo');
		$this->load->model('home/User_master_model');
		$this->load->model('home/Bookmark_master_model');
		$this->load->model('home/Doctors_rating_model');
		$this->data['slug_class'] = "doctorProfile";
		$this->load->helper('captcha'); // captcha helper
	}

	function index() {
		
		$doctorId = end(explode('-', $this->uri->segment(3))) ?: $this->uri->segment(3);
		$lang = end(explode('/', $this->uri->segment(4))) ?: $this->uri->segment(4);
		if($lang){
			
			if($lang=="ar"){
			  $this->session->set_userdata('site_lang',"arabic");	
			  
			  redirect(base_url() . 'doctorProfile/index/'.$this->uri->segment(3));

			}
			else{
			  $this->session->set_userdata('site_lang',"english");	
			  redirect(base_url() . 'doctorProfile/index/'.$this->uri->segment(3));
			}
		}
		$siteLang  = $this->session->userdata('site_lang');
		if (empty($doctorId)) {
			redirect(base_url('home'));
		} else {

			$this->db->select('doctor_details.photo,doctor_details.phone_number,doctor_details.mobile_number,doctor_details.email,(SELECT count(doctors_rating.doctor_id) FROM `doctors_rating` WHERE dstatus="1" AND doctor_id="' . $doctorId . '") as reviewCount,doctor_details.google_map_latitude,doctor_details.google_map_longtude,IFNULL(AVG(reputation), 0) as avgReputation,IFNULL(AVG(clinic),0) as avgClinic,IFNULL(AVG(availability),0) as avgAvailability,IFNULL(AVG(approachability),0) as avgApproachability,IFNULL(AVG(technology),0) as avgTechnology, (SELECT count( *) FROM `doctors_rating` WHERE dstatus="1" AND doctor_id="' . $doctorId . '" and (reputation) <> 0) as countreputation,(SELECT count( *) FROM `doctors_rating` WHERE  dstatus="1" AND doctor_id="' . $doctorId . '" and (clinic) <> 0) as countclinic,(SELECT count( *) FROM `doctors_rating` WHERE  dstatus="1" AND doctor_id="' . $doctorId . '" and (availability) <> 0) as countavailability,(SELECT count( *) FROM `doctors_rating` WHERE  dstatus="1" AND doctor_id="' . $doctorId . '" and (approachability) <> 0) as countapproachability,(SELECT count( *) FROM `doctors_rating` WHERE  dstatus="1" AND doctor_id="' . $doctorId . '" and (technology) <> 0) as counttechnology,work_master.work_type,specialty_master.name_en,doctor_details.speciality_id,doctor_details.doctor_id,doctor_details.google_map_zoom');
			if ($siteLang == 'arabic') {
				$this->db->select('doctor_master.first_name,doctor_master.last_name,doctor_master.address,location_master.name as Lname,doctor_master.major AS major,doctor_master.biography AS biography,specialty_master.name AS speciality,doctor_details.working_hours as WorkHours');
			} else {
				$this->db->select('doctor_master_en.first_name,doctor_master_en.last_name,doctor_master_en.address,location_master.name_en as Lname,doctor_master_en.major AS major,doctor_master_en.biography AS biography,specialty_master.name_en AS speciality,doctor_details.working_hours_en as WorkHours');
			}

			$this->db->join('doctor_master_en', 'doctor_master_en.doctor_id=doctor_details.doctor_id', 'left');
			$this->db->join('doctor_master', 'doctor_master.doctor_id=doctor_details.doctor_id', 'left');
			$this->db->join('specialty_master', 'specialty_master.specialties_id=doctor_details.speciality_id', 'left');
			$this->db->join('doctors_rating', 'doctors_rating.doctor_id=doctor_details.doctor_id', 'left');
			$this->db->join('work_master', 'work_master.work_id=doctor_details.work_id', 'left');
			$this->db->join('location_master', 'location_master.location_id=doctor_details.location_id', 'left');
			$this->db->where('doctor_details.doctor_id', $doctorId);
			//  $this->db->where('doctors_rating.dstatus','1');
			$this->db->group_by('doctor_details.doctor_id');
			$data['doctorDetail'] = $this->db->get('doctor_details')->row();
			$data['doctorId'] = $doctorId;

			/*For Review */
			$data['ReviewResult'] = $this->db->select('(reputation + clinic + availability + approachability + technology)/5 as avgscore,doctor_details.*,user_master.*,doctors_rating.*,doctors_rating.reviewIcon,doctors_rating.summary')
				->join('doctor_details', 'doctor_details.doctor_id=doctors_rating.doctor_id', 'left')
				->join('user_master', 'user_master.user_id=doctors_rating.user_id', 'left')
				->where('doctors_rating.doctor_id', $doctorId)
				->where('doctors_rating.dstatus', '1')
				->order_by('doctors_rating.rating_id', 'desc')
				->get('doctors_rating')
				->result();

		}

		$data['featureArray'] = featuresArray();
		if ($this->session->userdata('user_id')) {
			$useremail = $this->db->get_where('user_master', array('user_id' => $this->session->userdata('user_id')))->row();
			$data['useremail'] = $useremail->email;
		} else {
			$data['useremail'] = "";
		}
		$data['slug_class'] = "doctorProfile";
		$data['title'] = 'TopDoctors';
		$data['redirectURL'] = current_url();

		/* Captcha code configuration */

        $config = array(
           
            'img_path'      => 'captcha_images/',
            'img_url'       => base_url().'captcha_images/',
            'img_width'     => '150',
            'img_height'    => 33,
            'word_length'   => 4,
            'font_size'     => 20,
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(0, 0, 0),
                'text' => array(0, 0, 0),
                'grid' => array(203, 203, 203)
        	),
        	'img_id'        => 'Imageid',
        	'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
      	    'expiration' => 7200

        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Send captcha image to view
        $data['captchaImg'] = $captcha['image'];
        
		/* end captcha */

		$this->__templateFront('doctorProfile/index', $data);
	}

	public function refresh(){
        // Captcha configuration
        $config = array(
        	
            'img_path'      => 'captcha_images/',
            'img_url'       => base_url().'captcha_images/',
            'img_width'     => '150',
            'img_height'    => 33,
            'word_length'   => 4,
            'font_size'     => 20,
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(0, 0, 0),
                'text' => array(0, 0, 0),
                'grid' => array(203, 203, 203)
        	),
        	'img_id'        => 'Imageid',
        	'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
       	    'expiration' => 7200

        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
    }

	function reviewCreate() {
		$this->load->helper('text');
		$this->form_validation->set_rules('reviewName', 'reviewName', 'xss_clean');
		$this->form_validation->set_rules('reviewSummary', 'reviewSummary', 'xss_clean');
		$this->form_validation->set_rules('reviews', 'reviews', 'xss_clean');
		$Reputation = $_POST['Reputation'];
		$Clinic = $_POST['Clinic'];
		$Availability = $_POST['Availability'];
		$Approachability = $_POST['Approachability'];
		$Technology = $_POST['Technology'];
		$reviewIcon = $this->input->post('reviewIcon');


		$inputCaptcha = $this->input->post('captcha');
         $sessCaptcha = $this->session->userdata('captchaCode');


		if (isset($_POST['visibility']) == "0") {
			$visibility = '0';
		} else {
			$visibility = '1';
		}
		if($inputCaptcha === $sessCaptcha){
		/*if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
			$secret = '6Lfumg8UAAAAAJK8NWvVwDTED6wVv2ZxXNuAuD20';
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);
			if ($responseData->success) {*/
				$userId = $this->session->userdata('user_id');
				$checkReviewDuplicate = $this->db->get_where('doctors_rating', array('user_id' => $userId, 'doctor_id' => $this->input->post('DoctorId')))->row();
				$string = $this->input->post('reviews');
				//    $position = strpos($var1,$string);
				$disallowed = array("ابن كلب", "ولاد كلب", "ابن الحمار", "بيضان", "معرّص", "ابن العرص", "ابن الوسخة", "ابن
الشرموطة", "ابن القحبة", "ابن المتناكة", "ابن
اللبوة", "وسخ", "خولات", "خول", "علق", "علوق", "بهيمة", "بهايم", "كس أمك", "طيزك حمرا", "طيزك", "الله
ينعل", "يلعن أبو", "أمك", "ابوك", "حمار", "خرة", "شخاح", "ناك", "ينيك", "ولاد وسخة", "ولاد
الوسخة", "الوسخ", "وسخ", "ابن ال", "بنت ال", "ولاد ال", "وسخ", "اوساخ", "اوسخ", "أوساخ", "بقر", "بقرة", "يا
حيوان", "يا حيوانات", "يا كلب", "كلاب", "بهايم", "بهيم", "بهيمه", "منيكه", "منياك", "ابن الوسخة", "ابن
المره", "ديك", "صفحةبنت", "عرص", "كسمك", "معرص", "الوسخة", "ولاد", "بنت", "وسخ", "وساخة", "الوسخه", "a7a", "احا", "أحا", "متناكة", "كس", "ام", "امك", "حمار", "Fool", "fuck", "vagina", "Idiot", "bastard", "name", "Motherfucker");
				$string = word_censor($string, $disallowed, 'Beep!');
				$mysymmary = $this->input->post('reviewSummary');
				$string2 = word_censor($mysymmary, $disallowed, 'Beep!');

				$position = strrchr($string, "Beep!");
				$position2 = strrchr($string2, "Beep!");

				//echo $string;die;
				$matches = array();
				/* $matchFound = preg_match_all(
					                  "/\b(" . implode($badWords, "|") . ")\b/i", $string, $matches
				*/
				//echo $matchFound;die;
				$haystack = $position . " " . $position2;
				$needle = 'Beep!';
				if (strpos($haystack, $needle) !== false) {

					$status = '0';
				} else {
					$status = '1';
				}
				/*if ($position == "Beep!" || $position2=="Beep!") {

					                    $status = '0';

					                } else {
					                    $status = '1';

				*/

				if ($this->input->post('reviewIcon') == "") {
					$reviewIcon = '0';
				} else {
					$reviewIcon = $this->input->post('reviewIcon');
				}
				$Reputation = $_POST['Reputation'];
				$Clinic = $_POST['Clinic'];
				$Availability = $_POST['Availability'];
				$Approachability = $_POST['Approachability'];
				$Technology = $_POST['Technology'];
				$average_score = ($Reputation + $Clinic + $Availability + $Approachability + $Technology) / 5;
				if (count($checkReviewDuplicate) == 0) {

					$user_ip = $this->get_client_ip();
					$this->Doctors_rating_model->insert(array(
						'comment' => $this->input->post('reviews'),
						'summary' => $this->input->post('reviewSummary'),
						'doctor_id' => $this->input->post('DoctorId'),
						'reputation' => $Reputation,
						'clinic' => $Clinic,
						'availability' => $Availability,
						'approachability' => $Approachability,
						'technology' => $Technology,
						'average_score' => $average_score,
						'user_id' => $userId,
						'user_ip' => $user_ip,
						'visibility' => $visibility,
						'reviewIcon' => $reviewIcon,
						'dstatus' => $status,
					));
				} else {
					$this->Doctors_rating_model->update($checkReviewDuplicate->rating_id, array(
						'comment' => $this->input->post('reviews'),
						'reviewIcon' => $reviewIcon,
						'dstatus' => $status,
					));
				}
				$this->flash_notification('Thanks for the review');
			/*} else {

				$this->session->set_flashdata('reputation', $Reputation);
				$this->session->set_flashdata('clinic', $Clinic);
				$this->session->set_flashdata('availability', $Availability);
				$this->session->set_flashdata('approachability', $Approachability);
				$this->session->set_flashdata('technology', $Technology);
				$this->session->set_flashdata('visibility', $visibility);
				$this->session->set_flashdata('reviewIcon', $reviewIcon);

				$this->session->set_flashdata('reviews', $_POST['reviews']);
				$this->session->set_flashdata('reviewSummary', $_POST['reviewSummary']);
				$this->session->set_flashdata('error', 'Robot verification failed, please try again');
			}*/
		} else {
			$this->session->set_flashdata('reputation', $Reputation);
			$this->session->set_flashdata('clinic', $Clinic);
			$this->session->set_flashdata('availability', $Availability);
			$this->session->set_flashdata('approachability', $Approachability);
			$this->session->set_flashdata('technology', $Technology);
			$this->session->set_flashdata('visibility', $visibility);
			$this->session->set_flashdata('reviewIcon', $reviewIcon);
			$this->session->set_flashdata('reviews', $_POST['reviews']);
			$this->session->set_flashdata('reviewSummary', $_POST['reviewSummary']);
			$this->session->set_flashdata('error', 'Captcha code was not match, please try again.');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	function userRateUpdate() {
		$userId = $_POST['userId'];
		$doctorId = $_POST['doctorId'];
		$Reputation = $_POST['Reputation'];
		$Clinic = $_POST['Clinic'];
		$Availability = $_POST['Availability'];
		$Approachability = $_POST['Approachability'];
		$Technology = $_POST['Technology'];

		$checkRow = $this->db->get_where('doctors_rating', array('doctor_id' => $doctorId, 'user_id' => $userId))->row();
		if (count($checkRow) == 0) {
			$this->db->insert('doctors_rating', array('doctor_id' => $doctorId, 'user_id' => $userId));
		}
		if (count($checkRow) > 0 && !empty($Reputation)) {
			$this->db->where('user_id', $userId);
			$this->db->where('doctor_id', $doctorId);
			$this->db->update('doctors_rating', array('reputation' => $Reputation));
		} else if (count($checkRow) > 0 && !empty($Clinic)) {
			$this->db->where('user_id', $userId);
			$this->db->where('doctor_id', $doctorId);
			$this->db->update('doctors_rating', array('clinic' => $Clinic));
		} else if (count($checkRow) > 0 && !empty($Availability)) {
			$this->db->where('user_id', $userId);
			$this->db->where('doctor_id', $doctorId);
			$this->db->update('doctors_rating', array('availability' => $Availability));
		} else if (count($checkRow) > 0 && !empty($Approachability)) {
			$this->db->where('user_id', $userId);
			$this->db->where('doctor_id', $doctorId);
			$this->db->update('doctors_rating', array('approachability' => $Approachability));
		} else if (count($checkRow) > 0 && !empty($Technology)) {
			$this->db->where('user_id', $userId);
			$this->db->where('doctor_id', $doctorId);
			$this->db->update('doctors_rating', array('technology' => $Technology));
		}
		$average_score = ($checkRow->reputation + $checkRow->clinic + $checkRow->availability + $checkRow->approachability + $checkRow->technology) / 5;
		$this->db->where('user_id', $userId);
		$this->db->where('doctor_id', $doctorId);
		$this->db->update('doctors_rating', array('average_score' => $average_score));
	}
	function check_user_email($param = '') {

		$email = $this->input->post('email_id');

		if ($param == '') {
			$data = $this->Doctor_details_model->get_by(array('email' => $email));
			if ($data) {
				echo "1";
			} else {
				echo "0";
			}
		} else {
			$data = $this->Doctor_details_model->get_by(array(
				'doctor_id !=' => $this->input->post('userid'),
				'email' => $email,
			));
			if ($data) {
				echo "1";
			} else {
				echo "0";
			}
		}
	}
	function check_user_email_duplicate($param = '') {

		$email = $this->input->post('email_id');

		if ($param == '') {
			$data = $this->Doctor_details_model->get_by(array('email' => $email));
			if ($data) {
				echo "false";
			} else {
				echo "true";
			}
		} else {
			$data = $this->Doctor_details_model->get_by(array(
				'doctor_id !=' => $this->input->post('userid'),
				'email' => $email,
			));
			if ($data) {
				echo "false";
			} else {
				echo "true";
			}
		}
	}

	function enquireCreate() {
		$this->load->model('Enquiry_model');
		if ($this->session->userdata('user_id')) {
			$user_id = $this->session->userdata('user_id');
		} else {
			$user_id = '0';
		}

		$work_id = $this->input->post('work_id');
		$work_type = $this->input->post('work_type');
		$description = $this->input->post('Enquiry');
		$name = $this->input->post('enquiryName');
		$email = $this->input->post('enquiryEmail');
		$phone = $this->input->post('enquiryPhone');

		//insert enquiry
		$insert_id = $this->Enquiry_model->insert(array(
			'user_id' => $user_id,
			'work_id' => $work_id,
			'work_type' => $work_type,
			'description' => $description,
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
		));

		if ($insert_id) {
			//send email to user and doctor
			$email_data['description'] = $description;
			$email_data['to_email'] = $email;
			$email_data['user_name'] = $name;
			$email_data['user_email'] = $email;
			$email_data['user_mobile'] = $phone;

			$this->get_work_type_details($work_id, $user_id, $work_type, $email_data);
			$this->flash_notification('Thank you for your enquiry. Your message has been sent successfully.');
			redirect($_SERVER['HTTP_REFERER']);
			/*$this->response([
				            'status'   => TRUE,
				            'message'   => 'Thank you for your enquiry. Your message has been sent successfully.'
			*/
		} else {

			$this->session->set_flashdata('error', 'Error occured while enquiry');
			redirect($_SERVER['HTTP_REFERER']);
			/*$this->response([
				            'status'   => FALSE,
				            'message'   => 'Error occured while inserting record'
			*/
		}
	}

	/**
	 * Get work type or doctor details
	 * @param  string $id
	 * @param  string $type
	 * @param  array $email_data
	 */
	function get_work_type_details($id, $user_id, $type, $email_data) {

		if (isset($id) && isset($type)) {
			$this->load->model('admin/User_master_model');
			$this->load->model('admin/Doctor_details_model');

			$user = $this->User_master_model->get($user_id);
			if ($user) {
				$email_data['to_email'] = $user->email;
				$email_data['user_name'] = $user->first_name . ' ' . $user->last_name;
				$email_data['user_email'] = $user->email;
				$email_data['user_mobile'] = $email_data['user_mobile'];
			} else {
				$email_data['to_email'] = $email_data['to_email'];
				$email_data['user_name'] = $email_data['user_name'];
				$email_data['user_email'] = $email_data['user_email'];
				$email_data['user_mobile'] = $email_data['user_mobile'];

			}
			switch ($type) {
			case 'Doctor':
				# code...

				$this->load->model('Doctor_details_model');
				$doctor = $this->Doctor_details_model->doctor_details($id);
				$email_data['name'] = $doctor->first_name . ' ' . $doctor->last_name;
				$email_data['email'] = $doctor->email;
				$email_data['phone'] = $doctor->phone_number;
				$email_data['mobile'] = $doctor->mobile_number;
				$this->email_to_user($email_data);
				$this->email_to_doctor($email_data);
				break;
			case 'Clinic':
			case 'Medical Lab':
			case 'Radiology Lab':
			case 'Lab':
				# code...
				$this->load->model('admin/Work_master_model');
				$clinic = $this->Work_master_model->get($id);
				$email_data['name'] = $clinic->name_en;
				$email_data['email'] = $clinic->email;
				$email_data['phone'] = $clinic->phone;
				$email_data['mobile'] = '';
				$this->email_to_user($email_data);
				$this->email_to_doctor($email_data);
				break;
			default:
				$email_data = array();
			}

		}
	}

	/**
	 * Email to user
	 * @param  array  $data
	 */
	function email_to_user($data = array()) {
		$data['subject'] = 'Top Dotcor - Enquiry Confirmation';
		$message = "Hi " . $data['user_name'] . ",";
		$message .= "<br/><br/>Thanks for your enquiry.";
		$message .= "<br/><br/>We've mention doctor information which we hope you'll find useful.";
		$message .= "<br/><br/><b>Your enquiry: </b>" . $data['description'];
		$message .= "<br/><br/><b>Doctor Name: </b>" . $data['name'];
		$message .= "<br/><b>Doctor Email: </b>" . $data['email'];
		$message .= "<br/><b>Doctor Phone: </b>" . $data['phone'];
		$message .= "<br/><b>Doctor Mobile: </b>" . $data['mobile'];
		$message .= "<br/><br/>We looking forward to hearing from you.";
		$message .= "<br/><br/>Kindly regards,";
		$message .= "<br/>Top Doctors team";
		$data['message'] = $message;
		$this->sendMail($data);
	}

	/**
	 * Email to doctor
	 * @param  array
	 */
	function email_to_doctor($data = array()) {
		$data['subject'] = 'Top Dotcor - Enquiry Confirmation';
		$data['to_email'] = $data['email'];
		$message = "Hi " . $data['name'] . ",";
		$message .= "<br/><br/><b>User enquiry: <b/>" . $data['description'];
		$message .= "<br/><br/>We've mention doctor information which we hope you'll find useful.";
		$message .= "<br/><br/><b>User Name: </b>" . $data['user_name'];
		$message .= "<br/><b>User Mobile: </b>" . $data['user_mobile'];
		$message .= "<br/><b>User Email: </b>" . $data['user_email'];
		$data['message'] = $message;
		$this->sendMail($data);
	}

	/**
	 * Send email to doctor and user
	 * @param  array $data
	 */
	function sendMail($data) {
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('topdoctors123@gmail.com'); // change it to yours
		$this->email->to($data['to_email']); // change it to yours
		$this->email->subject($data['subject']);
		$this->email->message($data['message']);
		if (!$this->email->send()) {
			$this->session->set_flashdata('error', 'Error occured while sending an email');
			redirect($_SERVER['HTTP_REFERER']);
			/*$this->response([
				            'status'   => FALSE,
				            'message'   => 'Error occured while sending an email'
			*/
		}
	}

	/**
	 * get user ip
	 *
	 **/
	function get_client_ip() {
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP'])) {
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		} else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		} else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		} else if (isset($_SERVER['HTTP_FORWARDED'])) {
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		} else if (isset($_SERVER['REMOTE_ADDR'])) {
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		} else {
			$ipaddress = 'UNKNOWN';
		}

		return $ipaddress;
	}
/**
 * Searches for the first match.
 */
	/*function find($param = '', $lang = '') {

		if ($lang == "td_en") {
			$this->session->set_userdata('site_lang', "english");
		} else {

			$this->session->set_userdata('site_lang', "arabic");
		}

		$this->load->model('Doctor_details_model');

		if (strpos($param, " ") !== false) {
			$key = str_replace('_', '-', $param);
		} else {
			$key = str_replace('_', '-', urldecode($param));
		}

		$doctor = $this->Doctor_details_model->get_by(array('url_key' => $key));

		if ($doctor) {
			redirect(base_url() . 'doctorProfile/index/' . $doctor->doctor_id);
		} else {
			$this->load->model('Work_master_model');
			//echo $
			$work = $this->Work_master_model->get_by(array('url_key' => $key));

			if ($work) {
				redirect(base_url() . 'workProfile/index/' . $work->work_id);
			}
		}
		redirect(base_url());
	}*/

	function find($param = '', $lang = '') {

		if ($lang == "td_en") {
			$this->session->set_userdata('site_lang', "english");
		} else {

			$this->session->set_userdata('site_lang', "arabic");
		}

		$this->load->model('Doctor_details_model');

		if (strpos($param, " ") !== false) {
			$key = str_replace('_', '-', $param);
		} else {
			$key = str_replace('_', '-', urldecode($param));
		}

		//$doctor = $this->Doctor_details_model->get_by(array('url_key' => $key));
		$doctor = $this->Doctor_details_model->get_name($key);
		$name = urlencode($doctor->Name);
		
		if ($doctor) {
			redirect(base_url() . 'doctorProfile/index/'. $name.'-'.$doctor->doctor_id);
		} else {
			$this->load->model('Work_master_model');
			//echo $
			$work = $this->Work_master_model->get_name($key);
			$name = urlencode($work->Name);
			if ($work) {
				redirect(base_url() . 'workProfile/index/'. $name.'-'.$work->work_id);
			}
		}
		redirect(base_url());
	}

}
