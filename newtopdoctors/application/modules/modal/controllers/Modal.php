<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Modal extends MY_Controller {

    /**
     * Constructor
     * 
     * @return void
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Modal popup
     * @param string $page_name
     * @param string $param2
     * @param string $param3
     */
    function popup($page_name = '', $param2 = '', $param3 = '') {
        $module_page = explode('_', $page_name);
        $page_name = '';

        $filter = array(
            'user', 'media','subject','cms','admin'
        );

        if (in_array($module_page[0], $filter)) {
            foreach ($module_page as $row) {
                $page_name .= $row . '/';
            }
        } else {
            if (count($module_page) == 3) {
                $page_name = $module_page[0] . '_' . $module_page[1] . '/' . $module_page[2];
            } else {
                foreach ($module_page as $row) {
                    $page_name .= $row . '/';
                }
            }
        }
        $page_name = rtrim($page_name, '/');
        $page_data['param2'] = $param2;
        $page_data['param3'] = $param3;
        
        if ($page_name == "modal_view_profile") {
            $page_data['professor'] = $this->db->get_where("professor", array("professor_id" => $param2))->result();
        }
        $page_data['action_page_name'] = 'abd';

        $this->__modal($page_name, $page_data);
    }

}
