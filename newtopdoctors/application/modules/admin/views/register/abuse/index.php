<!-- Start .row -->

<div class="row">                      

    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body">
                 
                <a class="links"  href="<?php echo base_url(); ?>admin/user/createabusive" target="_blank"><i class="fa fa-plus"></i> Create </a>
            <div class="table-responsive" >
                <form name="bulk_action_form" action="<?php echo base_url(); ?>admin/user/abusedelete" method="post" onsubmit="return validate();"/>
                 <table id="datatable-list-1" class="table table-striped table-bordered table-responsive datatableList" cellspacing=0 width=100%>
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th><input name="checkbox" type="checkbox" class="check" id="checkAll"/><div class="delete-btn"><input type="submit" class="btn btn-danger deleteLink delete-icon-style" name="bulk_delete_submit" value="Delete"/></div></th>
                                <th>Word</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($words as $rows) {?>
                                <tr>
                                    <td>Id</td>
                                    <td><input type="checkbox" name="checkbox[]" id="checkbox[]" value="<?php echo $rows->abuse_id;?>"/></td>
                                    <td><?php echo $rows->words; ?></td>                                    
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End contentwrapper -->
</div>
<!-- End #content -->

<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '0' : '1';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this);
        url = "<?php echo base_url(); ?>admin/user/update_status/";
        $.ajax({
          type:"POST",
          url: url,
          data: {id:$(current_element).attr('data'),status:status},
          success: function(data)
          {   
            location.reload();
          }
        });
      }      
   
});
$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
})

function validate(){
    //alert("test"); return false;
    var chks = document.getElementsByName('checkbox[]');
    var hasChecked = false;
    for (var j = 0; j < chks.length; j++){
        if (chks[j].checked){
            var result = confirm("Are you sure to remove this information ?");
            if (result) {
                hasChecked = true;
                break;
            }if (hasChecked == false){
                return false;
            }
        }
    }if (hasChecked == false){
        alert("Please select at least one.");
        return false;
    }return true;

}

$(document).ready(function(){
var table = $('.datatableList').DataTable({         
        "aoColumnDefs" : [ {
            'bSortable' : false,
            'aTargets' : [ 1 ]
        } ]
    });
   
});
</script>