<?php


?>
<div class="row">                      
    <div class="col-lg-12">
        <div class="panel-default">
            <div class="panel-body"> 
                <div class="">
                    <span><h3>Add Abuse Word</h3></span>
                </div>
                
                <?php echo form_open(base_url() . 'admin/user/createabusive', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'frmstudent', 'target' => '_top', "enctype" => "multipart/form-data")); ?>
                <div class="padded">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Word"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="word" id="word" />
                        </div>
                    </div>		

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-3">
                            <button type="submit" class="btn btn-info vd_bg-green"><?php echo ucwords("Add");?></button>
                        </div>
                    </div>            
                </div>   
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
/*$("form").submit(function() {
            $.ajax({
            type: 'POST', 
            url: "<?php echo base_url();?>admin/doctor/latlong/",
            data: { 'address': $('#address_en').val()},
            success: function(response){
              var data = JSON.parse(response);
               $("#latitude").append().val(data.lat);
               $("#longtitude").append().val(data.lng);
            }
            });
        });*/
    $(document).ready(function () {
        jQuery.validator.addMethod("mobile_no", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid contact no.');
        jQuery.validator.addMethod("email_id", function (value, element) {
            return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
        }, 'Please enter a valid email address.');

        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');

        $("#frmstudent").validate({
            rules: {
                f_name:"required",
                l_name:"required",
                f_name_en:"required",
                l_name_en:"required",
                address_en:"required",
                email_id:{  required: true,
                            email: true,
                            remote: {
                                url: "<?php echo base_url(); ?>admin/doctor/check_user_email",
                                type: "post",
                                data: {
                                     email: function () {
                                        return $("#email_id").val();
                                    },
                                }
                            }
                        },
                work_id:"required", 
                location: "required",
                specialties: "required",
                profilePhoto: {
                    extension: 'gif|png|jpg|jpeg',
                },    
            },
            messages: {
                f_name: "Enter first name",
                l_name:"Enter last name",
                f_name_en: "Enter first name",
                l_name_en:"Enter last name",
                address_en:"Enter address",
                email_id: {
                    required: "Enter email id",
                    email_id: "Enter valid email id",
                    remote: "Email id already exists",
                },
                work_id: "Select work",
                location: "Select location",
                specialties: "Select specialties",
                profilePhoto: {
                    extension: "Upload valid file",
                }
            }
        });
    });
</script>


<script type="text/javascript">
$(document).ready(function() {
    var country = ["Australia", "Bangladesh", "Denmark", "Hong Kong", "Indonesia", "Netherlands", "New Zealand", "South Africa"];
    $("#country").select2({
        data: country
    });
});
</script>