<!-- Start .row -->
<div class="row">                      

    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body">
              <!--   <form id="form-filter" class="form-horizontal">
                  <div class="row">

                    <div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="First Name" class="form-control" id="FirstName">                
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Last Name" class="form-control" id="LastName">                
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Mobile Number" class="form-control" id="Mobile_number">                
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Email" class="form-control" id="Email">                
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="w98p">
                                     <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>               
                        </div>
                    </div>

                  
                    </div>
                </form>-->
                <div class="clearfix"></div>
                <div class="border-bottom"></div>
                     <!--<a class="links" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/register_create');" href="#" id="navfixed" data-toggle="tab"><i class="fa fa-plus"></i> Register </a>-->
            <div class="table-responsive" >
                <form name="bulk_action_form" action="<?php echo base_url(); ?>admin/user/deleteAll" method="post" onsubmit="return validate();"/>
                 <table id="datatable-list-1" class="table table-striped table-bordered table-responsive datatableList" cellspacing=0 width=100%>
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th><input name="checkbox" type="checkbox" class="check" id="checkAll"/><div class="delete-btn"><input type="submit" class="btn btn-danger deleteLink delete-icon-style" name="bulk_delete_submit" value="Delete"/></div></th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                            <td>
                                 <div class="w98p">
                                <input type="text" name="fromId" id="fromId" class="form-control" placeholder="From"></div>
                                 <div class="w98p">
                               <input type="text" name="fromTo" id="fromTo" class="form-control" placeholder="To"></div>
                            </td>
                            <td></td>
                            <td><div class="col-sm-4">
                                <div class="w98p">
                                            <input type="text" placeholder="First Name" class="form-control" id="FirstName">                
                                </div>
                            </div></td>
                            <td><div class="col-sm-4">
                                <div class="w98p">
                                            <input type="text" placeholder="Last Name" class="form-control" id="LastName">                
                                </div>
                            </div></td>
                            <td><div class="col-sm-4">
                                <div class="w98p">
                                            <input type="text" placeholder="Email" class="form-control" id="Email">                
                                </div>
                            </div></td>
                            <td><div class="col-sm-4">
                                <div class="w98p">
                                            <input type="text" placeholder="Mobile Number" class="form-control" id="Mobile_number">                
                                </div>
                            </div></td>
                            <td></td>
                            <td><button type="button" id="btn-filter" class="btn btn-primary">Filter</button></td> 

                        </tr>
                        </thead>
                        <!--<tbody>
                        <?php
                        foreach ($doctor as $row) {?>
                                <tr>
                                    <td><input type="checkbox" name="checkbox[]" id="checkbox[]" value="<?php echo $row->user_id;?>"/></td>
                                    <td><?php echo $row->first_name; ?></td>
                                    <td><?php echo $row->last_name; ?></td>
                                    <td><?php echo $row->email; ?></td>
                                    <td><?php echo $row->mobile_number; ?></td>
                                    <td><i data="<?php echo $row->user_id;?>" class="status_checks btn
                                    <?php echo ($row->status)?'btn-success': 'btn-danger'?>"><?php echo ($row->status)? 'Active' : 'Inactive'?></i></td>
                                    <td class="menu-action">
                                        <a href="<?php echo base_url(); ?>admin/user/edit/<?php echo $row->user_id;?>" target="_blank"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a>
                                        <a data-placement="top" data-toggle="tooltip" onclick="confirm_modal('<?php echo base_url(); ?>admin/user/delete/<?php echo $row->user_id; ?>');" href="#" data-original-title="" title=""><span class="label label-danger mr6 mb6"><i aria-hidden="true" class="fa fa-trash-o"></i>Delete</span></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>-->
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End contentwrapper -->
</div>
<!-- End #content -->

<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '0' : '1';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this);
        url = "<?php echo base_url(); ?>admin/user/update_status/";
        $.ajax({
          type:"POST",
          url: url,
          data: {id:$(current_element).attr('data'),status:status},
          success: function(data)
          {   
            location.reload();
          }
        });
      }      
   
});
$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
})

function validate(){
    //alert("test"); return false;
    var chks = document.getElementsByName('checkbox[]');
    var hasChecked = false;
    for (var j = 0; j < chks.length; j++){
        if (chks[j].checked){
            var result = confirm("Are you sure to remove this information ?");
            if (result) {
                hasChecked = true;
                break;
            }if (hasChecked == false){
                return false;
            }
        }
    }if (hasChecked == false){
       // alert("Please select at least one.");
        return false;
    }return true;

}

$(document).ready(function(){
var table = $('.datatableList').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url()?>admin/user/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                data.from = $("#fromId").val();
                data.to = $("#fromTo").val();
                data.mobile = $('#Mobile_number').val();
                data.firstname = $('#FirstName').val();
                data.lastname = $('#LastName').val();
                data.email = $('#Email').val();              
            }            
        },
        initComplete: function() {
            $('.datatableList input[type="text"]').unbind();
            $('.datatableList input[type="text"]').bind('keyup', function(e) {
                if(e.keyCode == 13) {
                      table.page( 'next' ).draw( 'page' );
                     table.ajax.reload(null,false);
                }
            });
        },
        "aoColumnDefs" : [ {
            'bSortable' : false,
            'aTargets' : [ 1 ]
        } ],
        "searching":false,
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "paging": true,
        "pagingType": "full_numbers"
    });

    $('#btn-filter').click(function(){ //button filter event click
      table.page( 'next' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });
});
</script>
<script src="<?php echo base_url(); ?>assets/js/plugins/tables-data.js"></script>
