<?php
$this->load->model('admin/User_master_model');
$this->load->model('admin/Country_master_model');

// $row=$this->User_master_model->get($param2);

$Country = $this->Country_master_model->get_all();
?>
    <div class="row">
        <div class="col-lg-12">
            <!-- col-lg-12 start here -->
            <div class="panel-default toggle panelMove panelClose panelRefresh">
                <div class="panel-body">
                    <div class="tab-pane box" id="add" style="padding: 5px">
                        <div class="box-content">

                            <form name="frmstudentedit" id="frmstudentedit" method="post" action="<?php echo base_url() ?>admin/user/update/<?php
echo $row->user_id; ?>" enctype="multipart/form-data" class="form-horizontal form-groups-bordered validate">
                                <input type="hidden" name="txtuserid" id="txtuserid" value="<?php
echo $row->user_id; ?>">
                                <input type="hidden" name="studentid" id="studentid" value="<?php
echo $row->user_id; ?>">

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php
echo ucwords("First Name"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="f_name" id="f_name" value="<?php
echo $row->first_name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php
echo ucwords("Last Name"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="l_name" id="l_name"  value="<?php
echo $row->last_name; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php
echo ucwords("Email Id"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="email_id" id="email_id" value="<?php
echo $row->email; ?>" />
                            <span id="emailerror" style="color: red"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php
echo ucwords("Mobile No"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="mobileno" id="mobileno" value="<?php
echo $row->mobile_number; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php
echo ucwords("Country"); ?></label>
                        <div class="col-sm-3">
                            <select name="country_id" class="form-control" id="country_id">
                                <option value="">Select</option>
                                <?php

foreach ($Country as $row_country) {
    ?>
                                    <option value="<?php
echo $row_country->country_id ?>" <?php
if ($row->country_id == $row_country->country_id) {?>selected<?php
}?>><?php
echo $row_country->country_name ?></option>
                                    <?php
}?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php
echo ucwords("Address"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="address" id="address" ><?php
echo $row->address; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php
echo ucwords("Status"); ?></label>
                        <div class="col-sm-3">
                            <select name="status" class="form-control" id="status">
                                <option value="">Select</option>
                                    <option value="1" <?php

if ($row->status == '1') {?>selected<?php
}?>>Active</option>
                                    <option value="0" <?php

if ($row->status == '0') {?>selected<?php
}?>>Inactive</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-3">
                            <button type="submit" class="btn btn-info vd_bg-green"><?php
echo ucwords("Update"); ?></button>
                        </div>
                    </div>
                </div>
                </form>
                </div>
            </div>
            </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#birthdate1").datepicker({
        });
        $("#basic-datepicker").datepicker({
            endDate: new Date(),
            format: "MM d, yyyy",
            autoclose: true});

        jQuery.validator.addMethod("mobile_no", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid contact no.');
        jQuery.validator.addMethod("email_id", function (value, element) {
            return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
        }, 'Please enter a valid email address.');

        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');

        $("#frmstudentedit").validate({
            rules: {
                f_name:{    required: true,
                            character: true,
                        },
                l_name: {   required: true,
                            character: true,
                        },
                email_id:
                        {
                            required: true,
                            email: true,
                            remote: {
                                url: "<?php
echo base_url(); ?>admin/user/check_user_email/edit",
                                type: "post",
                                data: {
                                    email: function () {
                                        return $("#email_id").val();
                                    },
                                    userid: function () {
                                        return $("#txtuserid").val();
                                    },
                                }
                            }
                        },
                 mobileno:{
                            required: true,
                            maxlength: 11,
                            mobile_no: true,
                            minlength: 10,
                        },
                country_id: "required",
                address:"required",
            },
            messages: {
                f_name: {   required: "Enter first name",
                            character: "Enter valid name",
                        },
                l_name:{    required: "Enter last name",
                            character: "Enter valid name",
                        },
                email_id: {
                        required: "Enter email",
                        email: "Enter valid email",
                        remote: "Email already exist.",
                },
                mobileno:{  required: "Enter mobile no",
                            maxlength: "Enter maximum 10 digit number",
                            mobile_no: "Enter valid mobile number",
                            minlength: "Enter minimum 10 digit number",
                        },
                country_id: "Select Country",
                address: "Enter address",
            }
        });
    });
</script>