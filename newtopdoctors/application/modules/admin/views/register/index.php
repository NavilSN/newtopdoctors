<!-- Start .row -->
<div class="row">

<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<!-- polyfiller file to detect and load polyfills -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script>
  webshims.setOptions('waitReady', false);
  webshims.setOptions('forms-ext', {types: 'date'});
  webshims.polyfill('forms forms-ext');
</script>
<style type="text/css">
    .input-button-size-2{
        margin-left:-44px !important;
        margin-right:3px !important;
    }
</style>
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body">

                <div class="clearfix"></div>
                <div class="border-bottom"></div>

            <div class="table-responsive" >
                <form name="bulk_action_form" action="<?php echo base_url(); ?>admin/user/deleteAll" method="post" onsubmit="return validate();"/>
                <div class="loadingtopdoctor" >Loading&#8230;</div>
                 <table id="datatable-list-1" class="table table-striped table-bordered table-responsive datatableList" cellspacing=0 width=100%>
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th><div class="delete-btn"><input type="submit" class="btn btn-danger deleteLink delete-icon-style" name="bulk_delete_submit" value="Delete"/></div></th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Status</th>
                                <th>Registration from</th>
                                <th>Registration date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                            <td>
                                 <div class="w98p">
                                <input type="text" name="fromId" id="fromId" class="form-control" placeholder="From"></div>
                                 <div class="w98p">
                               <input type="text" name="fromTo" id="fromTo" class="form-control" placeholder="To"></div>
                            </td>
                            <td><input name="checkbox" type="checkbox" class="check" id="checkAll"/></td>
                            <td><div class="col-sm-4">
                                <div class="w98p">
                                            <input type="text" placeholder="First Name" class="form-control" id="FirstName">
                                </div>
                            </div></td>
                            <td><div class="col-sm-4">
                                <div class="w98p">
                                            <input type="text" placeholder="Last Name" class="form-control" id="LastName">
                                </div>
                            </div></td>
                            <td><div class="col-sm-4">
                                <div class="w98p">
                                            <input type="text" placeholder="Email" class="form-control" id="Email">
                                </div>
                            </div></td>
                            <td><div class="col-sm-4">
                                <div class="w98p">
                                            <input type="text" placeholder="Mobile Number" class="form-control" id="Mobile_number">
                                </div>
                            </div></td>
                            <td><div class="w98p">
                                    <select name="status" class="form-control dropdown user-success" id="status">
                                        <option value="">Select</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>

                                    </select>
                                </div></td>
                            <td >
                                <select name="registration_from" class="form-control dropdown user-success" id="registration_from">
                                        <option value="">Select</option>
                                        <option value="1">Website</option>
                                        <option value="2">App</option>

                                    </select>
                            </td>
                            <td ><div class="w98p">
                                            <input type="date" placeholder="From" class="form-control" id="registration_date">
                                            <input type="date" placeholder="To" class="form-control" id="registration_date_to">
                                </div></td>
                            <td><button type="button" id="btn-filter" class="btn btn-primary">Filter</button></td>

                        </tr>
                        </thead>

                    </table>
                    </form>
                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End contentwrapper -->
</div>
<!-- End #content -->

<!-- <script src="//code.jquery.com/jquery-1.10.2.min.js"></script> -->
<script src="<?php echo base_url(); ?>js/1.10.9/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>js/1.10.9/input.js"></script>
<script type="text/javascript">
$(document).ajaxStart(function () {
    $('.loadingtopdoctor').show();
});
$(document).ajaxComplete(function () {
    $('.loadingtopdoctor').hide();
});
$(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '0' : '1';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this);
        url = "<?php echo base_url(); ?>admin/user/update_status/";
        $.ajax({
          type:"POST",
          url: url,
          data: {id:$(current_element).attr('data'),status:status},
          success: function(data)
          {
            location.reload();
          }
        });
      }

});
$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
})

function validate(){
    //alert("test"); return false;
    var chks = document.getElementsByName('checkbox[]');
    var hasChecked = false;
    for (var j = 0; j < chks.length; j++){
        if (chks[j].checked){
            var result = confirm("Are you sure to remove this information ?");
            if (result) {
                hasChecked = true;
                break;
            }if (hasChecked == false){
                return false;
            }
        }
    }if (hasChecked == false){
       // alert("Please select at least one.");
        return false;
    }return true;

}

$(document).ready(function(){
var table = $('.datatableList').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url() ?>admin/user/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                data.from = $("#fromId").val();
                data.to = $("#fromTo").val();
                data.mobile = $('#Mobile_number').val();
                data.firstname = $('#FirstName').val();
                data.lastname = $('#LastName').val();
                data.email = $('#Email').val();
                data.user_status=$("#status").val();
                data.registration_from=$("#registration_from").val();
                data.registration_date=$("#registration_date").val();
                data.registration_date_to=$("#registration_date_to").val();
                
                

            }
        },
        initComplete: function() {
            $('.datatableList input[type="text"]').unbind();
            $('.datatableList input[type="text"]').bind('keyup', function(e) {
                if(e.keyCode == 13) {
                    //  table.page( 'next' ).draw( 'page' );
                    table.page( 'first' ).draw( 'page' );
                     table.ajax.reload(null,false);
                }
            });
        },
        "aoColumnDefs" : [ {
            'bSortable' : false,
            'aTargets' : [ 1,9 ]
        } ],
        "searching":false,
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "paging": true,
        "pagingType": "input",
        "order": [[ 0, "desc" ]]
    });

    $('#btn-filter').click(function(){ //button filter event click
    //  table.page( 'next' ).draw( 'page' );
    table.page( 'first' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $("#status").change(function(){
        table.page( 'first' ).draw( 'page' );
    table.ajax.reload(null,false);  //just reload table
    });
    $("#registration_from").change(function(){
        table.page( 'first' ).draw( 'page' );
    table.ajax.reload(null,false);  //just reload table
    });
    $("#registration_date_to").change(function(){
        table.page( 'first' ).draw( 'page' );
    table.ajax.reload(null,false);  //just reload table
    });
    $("#registration_date").change(function(){
        table.page( 'first' ).draw( 'page' );
    table.ajax.reload(null,false);  //just reload table
    });
    
    
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });
});
</script>