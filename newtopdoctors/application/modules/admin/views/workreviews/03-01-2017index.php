<div class="row"> 
    <div class="col-lg-12">
        <div class="panel-default">
            <div class="panel-body">
            <div class="table-responsive" >
        <table  class="table table-striped table-bordered table-responsive example datatableList display nowrap" cellspacing=0 width=100%>
                        <thead>
                            <tr>
                                <th>Rating Id</th>
                                <th>Work Id</th>                                
                                <th>Work Type</th>
                                <th>Average Rating</th>
                                <th>Reviews Count</th>                                
                                <th>Comment</th>
                                <th>Date Posted</th>                                
                                <th>Status</th>
                            </tr>
                        </thead>
                      <!--  <tbody>
                        <?php //echo '<pre>'; print_r($reviews); die();
                        foreach ($reviews as $row) {
                            //if()
                            if($row->wstatus==0){
                              $btn =   'btn-success';
                              $status = 'Active';
                            }
                            else{
                                 $btn = 'btn-danger';
                                 $status = 'Inactive';
                            }
                            
                            
                            
                            ?>
                                <tr>
                                    <td><?php echo $row->work_rating_id; ?></td>
                                    <td><?php echo $row->work_id; ?></td>                                                                     
                                    <td><?php echo $row->work_type; ?></td>
                                    <td><?php echo $row->average_score; ?></td>
                                    <td><?php echo get_work_total_review($row->work_id);//$row->totlaUser; ?></td>
                                    <td><?php echo $row->comment; ?></td>
                                    <td><?php echo $row->date_created; ?></td>                                    
                                    <td><i data="<?php echo $row->work_rating_id; ?>" id="<?php echo $row->work_rating_id; ?>" class="status_checks btn <?php echo $btn; ?>"><?php echo $status; ?></i></td>
                                </tr>
                            <?php } ?>
                        </tbody>-->
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script>
 $(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '1' : '0';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this).attr('id');        
        url = "<?php echo base_url(); ?>admin/workreviews/update_status/";
        $.ajax({
          type:"POST",
          url: url,
          data: {id:current_element,status:status},
          success: function(data)
          {   
             // alert(data);
            location.reload();
          }
        });
      }      
    });
</script>
<script type="text/javascript">

$(document).ready(function() {
    
var table = $('.datatableList').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url()?>admin/workreviews/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                           
            }            
        },
        initComplete: function() {
            $('.datatableList input[type="text"]').unbind();
            $('.datatableList input[type="text"]').bind('keyup', function(e) {
                if(e.keyCode == 13) {
                      table.page( 'next' ).draw( 'page' );
                     table.ajax.reload(null,false);
                }
            });
        },
        "aoColumnDefs" : [ {
            'bSortable' : false,
            'aTargets' : [ 1 ]
        } ],
        "searching":false,
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "paging": true,
        "pagingType": "full_numbers"
    });

    $('#btn-filter').click(function(){ //button filter event click
      table.page( 'next' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });
/*
var table = $('.datatableList').DataTable({ 
        
        "searching":false,
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "paging": true,
        "pagingType": "full_numbers"
    });

*/

});
</script>
<script src="<?php echo base_url(); ?>assets/js/plugins/tables-data.js"></script>
