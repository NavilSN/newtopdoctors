
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<!-- polyfiller file to detect and load polyfills -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script>
  webshims.setOptions('waitReady', false);
  webshims.setOptions('forms-ext', {types: 'date'});
  webshims.polyfill('forms forms-ext');
  
 
</script>

<style type="text/css">
    input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,
    input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success{ width: 150px !important; }

</style>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-default">
            <div class="panel-body">
            <div class="table-responsive" >
            <form name="bulk_action_form" action="<?php echo base_url(); ?>admin/workreviews/deleteAll" method="post" onsubmit="return validate();"/>
             <div class="loadingtopdoctor" >Loading&#8230;</div>
        <table  class="table table-striped table-bordered table-responsive example datatableList display nowrap work_Reviews" cellspacing=0 width=100%>
                        <thead>
                            <tr>
                                <th>Rating Id</th>
                                <th><div class="delete-btn"><input type="submit" class="btn btn-danger deleteLink delete-icon-style" name="bulk_delete_submit" value="Delete"/></div></th>
                                <th>Work Id</th>
                                <th>User</th>
                                <th>Work Type</th>
                                <th>Average Rating</th>
                                <th>Reviews Count</th>
                                <th>Comment</th>
                                <th>Date Posted</th>
                                <th>Review From</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <thead>
                            <td>
                                    <input id="search_from_id" class="form-control" type="text" placeholder="From" />
                                    <input id="search_to_id" class="form-control" type="text" placeholder="To" />
                                </td>
                                <td><input name="checkbox" type="checkbox" class="check" id="checkAll"/></td>
                                <td>
                                    <input id="search_work_id" class="form-control" type="text" />
                                </td>
                                <td>
                                    <input id="search_user" class="form-control" type="text" />
                                </td>
                                <?php
$work_types = $this->db->select('work_type')
    ->from('work_master')
    ->where('work_type!=""')
    ->group_by('work_type')
    ->get()
    ->result();
?>
                                <td>
                                    <select id="search_work" class="form-control" name="search_work">
                                        <option value="">All</option>
                                        <?php
foreach ($work_types as $type) {?>
                                        <option value="<?php echo $type->work_type ?>"><?php echo $type->work_type ?></option>
                                        <?php }?>
                                    </select>
                                </td>
                                <td>
                                    <input id="search_avg_rating" class="form-control" type="text" />
                                </td>
                                <td>
                                    <input id="search_count" class="form-control" type="text" placeholder="From" />
                                    <input id="search_count_to" class="form-control" type="text" placeholder="to" />
                                </td>
                                <td>
                                    <input id="search_comment" class="form-control" type="text" />
                                </td>
                                <td class="Created_at_from">
                                    <div class="w98p">
                                    <input id="search_date" class="form-control datepicker"  placeholder="From"  type="date" />
                                    <input id="date_to" class="form-control datepicker"    placeholder="To"  type="date" />
                                    <span style="width: 28px; display: inline-block;"></span>
                                    </div>
                                </td>
                                <td><select name="registration_from" id="registration_from" class="form-control dropdown">
                                    <option value="">Select</option>
                                    <option value="1">Website</option>
                                    <option value="2">App</option>
                                </select></td>
                                <td><select name="review_status" id="review_status" class="form-control dropdown">
                                    <option value="">Select</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select></td>
                        </thead>
                      
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- <script src="//code.jquery.com/jquery-1.10.2.min.js"></script> -->
<script>
 $(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '1' : '0';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this).attr('id');
        url = "<?php echo base_url(); ?>admin/workreviews/update_status/";
        $.ajax({
          type:"POST",
          url: url,
          data: {id:current_element,status:status},
          success: function(data)
          {
             // alert(data);
            location.reload();
          }
        });
      }
    });

$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
})

function validate(){
    //alert("test"); return false;
    var chks = document.getElementsByName('checkbox[]');
    var hasChecked = false;
    for (var j = 0; j < chks.length; j++){
        if (chks[j].checked){
            var result = confirm("Are you sure to remove this information ?");
            if (result) {
                hasChecked = true;
                break;
            }if (hasChecked == false){
                return false;
            }
        }
    }if (hasChecked == false){
       // alert("Please select at least one.");
        return false;
    }return true;

}
</script>
<script src="<?php echo base_url(); ?>js/1.10.9/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>js/1.10.9/input.js"></script>
<script type="text/javascript">
$(document).ajaxStart(function () {
    $('.loadingtopdoctor').show();
});
$(document).ajaxComplete(function () {
    $('.loadingtopdoctor').hide();
});

$(document).ready(function() {

var table = $('.datatableList').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url() ?>admin/workreviews/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                data.avg_rating = $("#search_avg_rating").val();
                data.review_count = $("#search_review_count").val();
                data.date = $("#search_date").val();
                data.date_to = $('#date_to').val();
                data.search_work = $("#search_work").val();
                data.from_id = $('#search_from_id').val();
                data.to_id = $('#search_to_id').val();
                data.work_id = $('#search_work_id').val();
                data.comment = $('#search_comment').val();
                data.search_count = $('#search_count').val();
                data.search_count_to = $('#search_count_to').val();
                 data.search_user = $("#search_user").val();
                 data.registration_from = $("#registration_from").val();
                 data.review_status = $("#review_status").val();
            }
        },
        initComplete: function() {
            $('.datatableList input[type="text"]').unbind();
            $('.datatableList input[type="text"]').bind('keyup', function(e) {
                if(e.keyCode == 13) {
                    table.page( 'first' ).draw( 'page' );
                    $("input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success").css({'width':"115px !important"});
                   //   table.page( 'next' ).draw( 'page' );
                     table.ajax.reload(null,false);
                     css_include();
                }
            });
        },
        "aoColumnDefs" : [ {
            'bSortable' : false,
            'aTargets' : [ 1,9,10 ]
        } ],
        "searching":false,
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "paging": true,
        "pagingType": "input",
        "order": [[ 0, "desc" ]]
    });

    $('#btn-filter').click(function(){ //button filter event click
        table.page( 'first' ).draw( 'page' );
        $("input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success").css({'width':"115px !important"});
    //  table.page( 'next' ).draw( 'page' );

        table.ajax.reload(null,false);  //just reload table
css_include();
    });
    $("#search_avg_rating").on('blur',function(){
        table.page( 'first' ).draw( 'page' );
        $("input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success").css({'width':"115px !important"});
    table.ajax.reload(null,false);  //just reload table
    css_include();
    });
    $("#search_work").on('change', function(){
        table.page( 'first' ).draw( 'page' );
        $("input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success").css({'width':"115px !important"});
    table.ajax.reload(null,false);  //just reload table
    css_include();
    });
    $(".datepicker").change(function(){
        table.page( 'first' ).draw( 'page' );
        $("input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success").css({'width':"115px !important"});
    table.ajax.reload(null,false);  //just reload table
    css_include();
    });


    $("#registration_from").change(function(){  
    table.page( 'first' ).draw( 'page' );  
         $("input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success").css({'width':'150px !important'});
        table.ajax.reload(null,false);  //just reload table
        css_include();
    });

    $("#review_status").change(function(){
        table.page( 'first' ).draw( 'page' );
     $("input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success").css({'width':'150px !important'});
    table.ajax.reload(null,false);  //just reload table
    css_include();
    });
   
    $('#btn-reset').click(function(){ //button reset event click
        table.page( 'first' ).draw( 'page' );
        $("input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success").css({'width':"115px !important"});
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
        css_include();
    });
        table.on( 'draw', function () {
     $('tr td:nth-child(8)').each(function (){
          $(this).addClass('comment-det')
        /*  $(".comment-det").css({'white-space':'-o-pre-wrap','word-wrap':'break-word','white-space':'pre-wrap','white-space':'-moz-pre-wrap','white-space':'-pre-wrap'});*/
     })
    });

});
 $(".ws-date").on('change',function(){
    $(".ws-date").css({'width':"130px !important"});
});
</script>
<script type="text/javascript">

/*$(window).load(function(){
    setTimeout(function() {
        $("div.comment-text").each(function() {
            var new_string = '';
            console.log($(this).html());
            var res = $(this).html().split(" ");
            console.log(res);
            for (i = 0; i < res.length; i++) {
                new_string += " " + res[i];
                if (i % 4 == 0) {
                    new_string += "<br />";
                }
            }
            console.log(new_string);
            $(this).html(new_string);
        });
    }, 500);
    
});
function css_include(){
        setTimeout(function() {
        $("div.comment-text").each(function() {
            var new_string = '';
            console.log($(this).html());
            var res = $(this).html().split(" ");
            console.log(res);
            for (i = 0; i < res.length; i++) {
                new_string += " " + res[i];
                if (i % 4 == 0) {
                    new_string += "<br />";
                }
            }
            console.log(new_string);
            $(this).html(new_string);
        });
    }, 500);
}*/
</script>
