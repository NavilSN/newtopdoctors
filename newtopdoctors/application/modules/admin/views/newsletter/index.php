<div class="row"> 
    <div class="col-lg-12">
        <div class="panel-default">
            <div class="panel-body">
       <div class="clearfix">  
                <div class="row">
                    <div class="col-sm-3" style="display: none">
                      
                    </div>
                      
                    <div class="col-sm-3  text-right" style="float: right">
                         <form action="<?php echo base_url(); ?>admin/newsletter/export" method="post" name="export_excel">
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Export Data</button>
                                </div>
                            </div>
                        </form>                 
                    </div>
                </div>
            </div>      

            <div class="table-responsive" >
             <form id="form-filter" name="bulk_action_form" action="<?php echo base_url(); ?>admin/speciality/deleteAll" method="post" onsubmit="return validate();"/>
               <div class="loadingtopdoctor" >Loading&#8230;</div>
        <table  class="table table-striped table-bordered table-responsive example datatableList display nowrap 
newsletter_detail" cellspacing=0 width=100% id="newsletter_listt">
                        <thead>
                            <tr>
                                <th>Id</th>                                
                                <th>Email</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                            <td>
                                 <div class="w98p">
                                <input type="text" name="fromId" id="fromId" class="form-control" placeholder="From"></div>
                                 <div class="w98p">
                               <input type="text" name="fromTo" id="fromTo" class="form-control" placeholder="To"></div>
                            </td>
                          
                            <td class="email_input">
                                <div class="w98p">
                                    <input type="text" placeholder="email" class="form-control" id="email">
                                </div>
                            </td>                                                         
                        
                        </tr>
                        </thead>
                       <!-- <tbody>
                        <?php //echo '<pre>'; print_r($reviews); die();
                        foreach ($newsletter as $row) {
                            //if()
                            
                            
                            
                            
                            ?>
                                <tr>
                                    <td><?php echo $row->id; ?></td>
                                    <td><?php echo $row->email; ?></td>              
                                </tr>
                            <?php } ?>
                        </tbody>-->
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script>
 $(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '1' : '0';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this).attr('id');        
        url = "<?php echo base_url(); ?>admin/newsletter/update_status/";
        $.ajax({
          type:"POST",
          url: url,
          data: {id:current_element,status:status},
          success: function(data)
          {   
             // alert(data);
            location.reload();
          }
        });
      }      
    });
</script>
<script src="<?php echo base_url(); ?>js/1.10.9/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>js/1.10.9/input.js"></script>
<script type="text/javascript">
$(document).ajaxStart(function () {
    $('.loadingtopdoctor').show();
});
$(document).ajaxComplete(function () {
    $('.loadingtopdoctor').hide();
});

$(document).ready(function() {

var table = $('#newsletter_listt').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url()?>admin/newsletter/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                data.from = $("#fromId").val();
                data.to = $("#fromTo").val();
                data.email = $('#email').val();                
            }            
        },
        initComplete: function() {
            $('#newsletter_listt input[type="text"]').unbind();
            $('#newsletter_listt input[type="text"]').bind('keyup', function(e) {
                if(e.keyCode == 13) {

                table.page( 'first' ).draw( 'page' );
                   //   table.page( 'next' ).draw( 'page' );
                     table.ajax.reload(null,false);
                }
            });
        },
        "aoColumnDefs" : [ {
            'bSortable' : true,
            'aTargets' : [ 0 ]
        } ],
        "searching":false,
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',        
        "paging": true,
        "pagingType": "input"
    });

    $('#btn-filter').click(function(){ //button filter event click
      table.page( 'first' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });


   

   /* $(".datatableList tbody tr").live( 'click',function() { 
           // console.log(($(this).val()));    
        //window.open('http://www.example.com');
    } );
*/
    /*$('.datatableList tbody').on( 'click', 'tr', function () {

       // console.log($(this).find('td').text());return false;

        var data = table.cell(this).data();   //td element data 
        console.log(data);return false; 
        var column = table.cell( this ).index().column; //column index
        var row = table.row( this ).index();  // Row index
        var  rowid = $(this).closest('tr').attr('id'); //Get Row Id
        var columnvisible = table.cell( this ).index().columnVisible //visble column index
    } );*/

    /*table.MakeCellsEditable({
        "onUpdate": myCallbackFunction
    });


    function myCallbackFunction(updatedCell, updatedRow, oldValue) {
        //alert(updatedCell.data());return false;
        console.log("The new value for the cell is: " + updatedCell.data());
        console.log("The old value for that cell was: " + oldValue);
        console.log("The values for each cell in that row are: " + updatedRow.data());
    }*/

});
</script>