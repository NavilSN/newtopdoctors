<!-- Start .row -->
<div class="row">                      

    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body">
                <a class="links" target="_blank" href="<?php echo base_url(); ?>admin/AdminManagement/create" ><i class="fa fa-plus"></i>Create</a>
            <div class="table-responsive" >
                 <table id="datatable-list" class="table table-striped table-bordered table-responsive" cellspacing=0 width=100%>
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($AdminManagement as $row) {?>
                                <tr>
                                    <td><?php echo $row->first_name; ?></td>
                                    <td><?php echo $row->last_name; ?></td>
                                    <td><?php echo $row->email; ?></td>
                                    <td><?php echo $row->mobile; ?></td>
                                    <td class="menu-action">
                                        <a  href="<?php echo base_url(); ?>admin/AdminManagement/edit/<?php echo $row->admin_id; ?>" target="_blank"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a>
                                        <?php if($row->admin_id!=$this->session->userdata('admin_id')){?>
                                        <a data-placement="top" data-toggle="tooltip" onclick="confirm_modal('<?php echo base_url(); ?>admin/AdminManagement/delete/<?php echo $row->admin_id; ?>');" href="#" data-original-title="" title=""><span class="label label-danger mr6 mb6"><i aria-hidden="true" class="fa fa-trash-o"></i>Delete</span></a>
                                        <?php }?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End contentwrapper -->
</div>
<!-- End #content -->
<script src="<?php echo base_url(); ?>assets/js/plugins/tables-data.js"></script>
<script type="text/javascript">
      var t = $('#datatable-list').DataTable({
        "columnDefs": [{
                "searchable": false,
                "orderable": false,
                
            }],
        "order": [[1, 'asc']],
        "language": {
            "emptyTable": "No data available"
        }
    });

    t.on('order.dt search.dt', function () {
        t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell) {
            //cell.innerHTML = i + 1;
        });
    }).draw(); 
</script>

