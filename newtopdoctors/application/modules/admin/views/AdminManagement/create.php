<!-- Start .row -->
<div class="row">                      
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body"> 
                <div class="box-content">     
                    
                    <?php echo form_open(base_url() . 'admin/AdminManagement/create', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'user-create-form', 'target' => '_top', "enctype" => "multipart/form-data")); ?>
                    <div class="padded">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">First Name</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="first_name" id="first_name"/>
                            </div>
                        </div>												
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Last Name</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="last_name" id="last_name"/>
                            </div>	
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-3">
                                <input type="email" class="form-control" name="email" 
                                       autocomplete="off" id="email"/>
                            </div>	
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" name="password" id="password"/>
                            </div>  
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Gender</label>
                            <div class="col-sm-3">
                                <select id="gender" name="gender" class="form-control">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>	
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mobile</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="mobile" id="mobile"/>
                            </div>	
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">City</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="city" id="city"/>
                            </div>	
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-3">
                                <textarea id="address" class="form-control" name="address"><?php echo $user->address; ?></textarea>
                            </div>	
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo ucwords("Photo"); ?></label>
                            <div class="col-sm-3">
                                <input type="file" name="photo" id="photo" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-info vd_bg-green" ><?php echo ucwords("add"); ?></button>
                            </div>
                        </div>
                        </form>               
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*$.validator.setDefaults({
        submitHandler: function (form) {
            form.submit();
        }
    });*/
    $(document).ready(function () {
        $("#user-create-form").validate({
            rules: {
                first_name: "required",
                last_name: "required",
                email: {
                            required: true,
                            email: true,
                            remote: {
                                url: "<?php echo base_url(); ?>admin/check_user_email/profile",
                                type: "post",
                                data: {
                                     email: function () {
                                        return $("#email").val();
                                    },
                                    userid: function () {
                                          return $("#txtuserid").val();
                                    },
                                }
                            }
                        },
                password: "required",
                mobile: "required",
                city: "required",
                address: "required",
                photo: {
                    extension: 'gif|png|jpg|jpeg',
                }, 
            },

            messages: {
                first_name: "Enter first name",
                last_name: "Enter last name",
                password: "Enter password",
                email: {
                    required: "Enter email id",
                    email: "Enter valid email id",
                    remote: "Email id already exists",
                },
                mobile: "Enter mobile no",
                city: "Enter city",
                address: "Enter address",
                photo: {
                    extension: "Upload valid file",
                }
            }
        });
    });
</script>