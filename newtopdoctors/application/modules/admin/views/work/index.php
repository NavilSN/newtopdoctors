<!-- Start .row -->
<?php $this->load->model('admin/Location_master_model');

?>
<!-- cdn for modernizr, if you haven't included it already -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<!-- polyfiller file to detect and load polyfills -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script>
  webshims.setOptions('waitReady', false);
  webshims.setOptions('forms-ext', {types: 'date'});
  webshims.polyfill('forms forms-ext');
</script>
<style type="text/css">
    input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,
    input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success{ width: 150px !important; }

</style>
<div class="row">
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body">

                <div class="clearfix">
                <div class="row">
                <div class="col-sm-3">
                        <a class="links" href="<?php echo base_url(); ?>admin/work/create" target="_blank"><i class="fa fa-plus"></i> Create </a>
                    </div>
                        <form action="<?php echo base_url(); ?>admin/work/import" method="post" name="importCSV" id="importCSV" enctype="multipart/form-data">
                          <a href="<?php echo base_url(); ?>csv/sample-data.xls" target="_blank" download="" style="text-decoration: none;">Sample File</a>
                    <div class="col-sm-3 text-right">
                             <div class="control-group">
                                <div>
                                    <input type="file" name="importData" id="importData">
                                </div>
                            </div>
                    </div>
                    <div class="col-sm-3 text-left">
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Import Data</button>
                                </div>
                            </div>
                    </div>
                        </form>
                    <div class="col-sm-3  text-right">
                         <form action="<?php echo base_url(); ?>admin/work/export" method="post" name="export_excel">
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Export Data</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
                <div class="border-bottom"></div>
            <div class="table-responsive" >
                <form id="form-filter" name="bulk_action_form" action="<?php echo base_url(); ?>admin/work/deleteAll" method="post" onsubmit="return validate();"/>
                     <div class="loadingtopdoctor" >Loading&#8230;</div>
                 <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                 <table  class="table table-striped table-bordered table-responsive example datatableList display nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th><div class="delete-btn"><input type="submit" class="btn btn-danger deleteLink delete-icon-style" name="bulk_delete_submit" value="Delete"/></div></th>
                    <th>Name In Arabic</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Mobile</th>
                    <th>Work Type</th>
                    <th>Location</th>
                    <th>Created Date</th>
                    <th>Updated Date</th>
                    <th>Status</th>
                    <th>Action</th>
                    </tr>
                    </thead>
                    <thead>
                        <td>
                            <div class="w98p">
                                <input type="text" name="fromId" id="fromId" class="form-control" placeholder="From">
                            </div>
                            <div class="w98p">
                               <input type="text" name="fromTo" id="fromTo" class="form-control" placeholder="To">
                            </div>
                        </td>
                        <td><input name="checkbox" type="checkbox" class="check" id="checkAll"/></td>
                        <td>
                        
                        <div class="w98p">
                                    <input type="text" placeholder="Name (Arabic)" class="form-control" id="FirstNamear" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name (Arabic)'">
                        </div>
                        </td>
                        <td>
                        <div class="w98p">
                                    <input type="text" placeholder="Email" class="form-control" id="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'">
                        </div>
                        </td>
                        <td>
                         
                            <div class="w98p">
                                        <input type="text" placeholder="Phone" class="form-control" id="Phone" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone'">
                            </div>
                        </td>
                        <td>
                            <div class="w98p">
                                        <input type="text" placeholder="Mobile" class="form-control" id="Mobile" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile'">
                            </div>
                        </td>
                        <td>
                         
                            <div class="w98p">
                                        <input type="text" placeholder="Work Type" class="form-control" id="Work_type" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Work Type'">
                            </div>
                        </td>
                        <td>
                         
                            <div class="w98p">
                                <?php echo getChildsAdmin(); ?>

                            </div>
                        </td>
                        <td class="Created_at_from">
                         
                            <div class="w98p">
                                        <input type="date" placeholder="From" class="form-control datepicker" id="FromDate" onfocus="this.placeholder = ''" onblur="this.placeholder = 'From'">
                                        <input type="date" placeholder="To" class="form-control datepicker"  id="ToDate" onfocus="this.placeholder = ''" onblur="this.placeholder = 'To'">
                                        <span style="width: 28px; display: inline-block;"></span>
                            </div>
                        </td>
                        <td class="Updated_at_from"> 
                            <div class="w98p">
                            <input id="search_updated_from" placeholder="From" class="form-control datepicker" type="date"  onfocus="this.placeholder = ''" onblur="this.placeholder = 'From'" />
                            <input id="search_updated_to"  placeholder="To" class="form-control datepicker" type="date"  onfocus="this.placeholder = ''" onblur="this.placeholder = 'To'" />
                            <span style="width: 28px; display: inline-block;"></span>
                            </div>
                        </td>
                        <td>

                        </td>

                        <td>
                        
                            <div class="w98p">
                                         <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            </div>
                        
                        </td>
                    </thead>

                    </table>
                </form>
                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End contentwrapper -->
</div>
<!-- End #content -->

<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
    $('#form-filter table.datatableList tr > td .w98p #FromDate').on('click',function(){
        alert(1);
    })
  </script> -->
<script src="<?php echo base_url(); ?>js/1.10.9/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>js/1.10.9/input.js"></script>
<script type="text/javascript">
$(document).ajaxStart(function () {
    $('.loadingtopdoctor').show();
});
$(document).ajaxComplete(function () {
    $('.loadingtopdoctor').hide();
});

$(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '0' : '1';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this);
        url = "<?php echo base_url(); ?>admin/work/update_status/";
        $.ajax({
          type:"POST",
          url: url,
          data: {id:$(current_element).attr('data'),status:status},
          success: function(data)
          {
            location.reload();
          }
        });
      }
    });
$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
})

function validate(){
    //alert("test"); return false;
    var chks = document.getElementsByName('checkbox[]');
    var hasChecked = false;
    for (var j = 0; j < chks.length; j++){
        if (chks[j].checked){
            var result = confirm("Are you sure to remove this information ?");
            if (result) {
                hasChecked = true;
                break;
            }if (hasChecked == false){
                return false;
            }
        }
    }if (hasChecked == false){
        //lert("Please select at least one.");
        return false;
    }return true;

}

$(document).ready(function (){

    $("#importCSV").validate({
        rules: {
            importData: {required: true,
                    extension: 'xls|xlsx',
                },
        },messages: {
            importData: {
                    required: "Please choose file",
                    extension: "Please upload only xls file",
                }
        }
    });
});
</script>

<script type="text/javascript">

$(document).ready(function() {

var editor;

var table = $('.datatableList').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url() ?>admin/work/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                data.from = $("#fromId").val();
                data.to = $("#fromTo").val();
                data.nameAR = $('#FirstNamear').val();
                data.email = $('#email').val();
                data.phone = $('#Phone').val();
                data.mobile = $('#Mobile').val();
                data.work_type = $('#Work_type').val();
                data.created_date_from = $('#FromDate').val();
                data.created_date_to = $('#ToDate').val();
                data.location = $('#select-location').val();
                data.search_updated_from = $('#search_updated_from').val();
                data.search_updated_to = $('#search_updated_to').val();
            }
        },
        initComplete: function() {
            $('.datatableList input[type="text"]').unbind();
            $('.datatableList input[type="text"]').bind('keyup', function(e) {
                if(e.keyCode == 13) {
                   
                     table.page( 'first' ).draw( 'page' );
                     table.ajax.reload(null,false);
                }
            });
              $('#select-location').change(function(){
                
                    //table.page( 'next' ).draw( 'page' );
                    //button filter event click
                    table.page( 'first' ).draw( 'page' );
                    table.ajax.reload(null,false);  //just reload table
              });
        },
        "aoColumnDefs" : [ {
            'bSortable' : false,
            'aTargets' : [ 1 ]
        } ],
        "searching":false,
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "paging": true,
        "pagingType": "input",
        "order": [[ 0, "desc" ]]
    });

    $('#btn-filter').click(function(){ //button filter event click
   table.page( 'first' ).draw( 'page' );
   
        table.ajax.reload(null,false);  //just reload table
    });
    $('.datepicker').change(function(){ //button filter event click
   table.page( 'first' ).draw( 'page' );
     //     table.page( 'next' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        table.page( 'first' ).draw( 'page' );
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });


});
</script>

    <style>
    #select-location {
    width: auto;
}
#bulk_action_form input{
width: auto;
}
    </style>
