<!-- Start .row -->
<?php $this->load->model('admin/Location_master_model'); 

?>
<!-- cdn for modernizr, if you haven't included it already -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<!-- polyfiller file to detect and load polyfills -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script>
  webshims.setOptions('waitReady', false);
  webshims.setOptions('forms-ext', {types: 'date'});
  webshims.polyfill('forms forms-ext');
</script>
<div class="row">                      
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body">

                <div class="clearfix"> 
                <div class="row">
                <div class="col-sm-3">
                        <a class="links" href="<?php echo base_url(); ?>admin/work/create" target="_blank"><i class="fa fa-plus"></i> Create </a>
                    </div>
                        <form action="<?php echo base_url(); ?>admin/work/import" method="post" name="importCSV" id="importCSV" enctype="multipart/form-data">
                    <div class="col-sm-3 text-right">
                             <div class="control-group">
                                <div>
                                    <input type="file" name="importData" id="importData">
                                </div>
                            </div>
                    </div>
                    <div class="col-sm-3 text-left">
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Import Data</button>
                                </div>
                            </div>
                    </div>
                        </form>
                    <div class="col-sm-3  text-right">
                         <form action="<?php echo base_url(); ?>admin/work/export" method="post" name="export_excel">
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Export Data</button>
                                </div>
                            </div>
                        </form>                 
                    </div>
                </div>
            </div>      
            <div class="clearfix"></div>
                <div class="border-bottom"></div>
            <div class="table-responsive" >
                <form id="form-filter" name="bulk_action_form" action="<?php echo base_url(); ?>admin/work/deleteAll" method="post" onsubmit="return validate();"/>
                 <button type="button" id="btn-reset" class="btn btn-default">Reset</button> 
                 <table  class="table table-striped table-bordered table-responsive example datatableList display nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th><input name="checkbox" type="checkbox" class="check" id="checkAll"/><div class="delete-btn"><input type="submit" class="btn btn-danger deleteLink delete-icon-style" name="bulk_delete_submit" value="Delete"/></div></th>
                    <th>Name In Arabic</th>
                    <th>Name In English</th>
                    <th>Phone</th>
                    <th>Work Type</th> 
                    <th>Location</th>
                    <th>Date</th>    
                    <th>Status</th>    
                    <th>Action</th>
                    </tr>
                    </thead>
                    <thead>
                        <td>
                            <div class="w98p">
                                <input type="text" name="fromId" id="fromId" class="form-control" placeholder="From"></div>
                                 <div class="w98p">
                               <input type="text" name="fromTo" id="fromTo" class="form-control" placeholder="To"></div>
                            </td>
                        <td></td>
                        <td>
                        <div class="col-sm-3">
                        <div class="w98p">
                                    <input type="text" placeholder="Name (Arabic)" class="form-control" id="FirstNamear" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name (Arabic)'">                
                        </div>
                        </div>
                        </td>
                        <td><div class="col-sm-3">
                        <div class="w98p">
                                    <input type="text" placeholder="Name (English)" class="form-control" id="Fistnameen" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name (English)'">                
                        </div>
                        </div>
                        </td>
                        <td>
                         <div class="col-sm-3">
                            <div class="w98p">
                                        <input type="text" placeholder="Phone" class="form-control" id="Phone" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone'">                
                            </div>
                        </div></td>
                        <td>
                         <div class="col-sm-3">
                            <div class="w98p">
                                        <input type="text" placeholder="Work Type" class="form-control" id="Work_type" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Work Type'">                
                            </div>
                        </div>
                        </td>
                        <td>
                         <div class="col-sm-3">
                            <div class="w98p">
                                <?php  echo getChilds(); ?>
                                        <!--<input type="text" placeholder="Location" class="form-control" id="Location" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Location'">                -->
                            </div>
                        </div>
                        </td>
                        <td>
                         <div class="col-sm-3">
                            <div class="w98p">
                                        <input type="date" placeholder="From" class="form-control datepicker" id="FromDate" onfocus="this.placeholder = ''" onblur="this.placeholder = 'From'"> 
                                        <input type="date" placeholder="To" class="form-control datepicker"  id="ToDate" onfocus="this.placeholder = ''" onblur="this.placeholder = 'To'">                
                            </div>
                        </div>
                        </td>                        
                        <td>
                         
                        </td>
                      
                        <td>
                        <div class="col-sm-6">
                            <div class="w98p">
                                         <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>             
                            </div>
                        </div>
                        </td>
                    </thead>
                    <!-- <tbody>
                        <?php
                        foreach ($work as $row) {?>
                                <tr>
                                    <td><input type="checkbox" name="checkbox[]" id="checkbox[]" value="<?php echo $row->work_id;?>"/></td>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->name_en; ?></td>
                                    <td><?php echo $row->phone; ?></td>
                                    <td><?php if($row->work_type=="Clinic"){echo 'Clinic';} if ($row->work_type=="Hospital"){echo 'Hospital';} if($row->work_type=="Lab") {echo 'Lab';} ?></td>
                                    <td><?php echo $this->Location_master_model->get($row->location_id)->name_en; ?></td>
                                    <td><i data="<?php echo $row->work_id;?>" class="status_checks btn
                                    <?php echo ($row->status)?'btn-success': 'btn-danger'?>"><?php echo ($row->status)? 'Active' : 'Inactive'?></i></td>
                                    <td><?php echo $row->created_date; ?></td>
                                    <td class="menu-action">
                                        <a href="<?php echo base_url(); ?>admin/work/edit/<?php echo $row->work_id; ?>" target="_blank"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a>
                                        <a data-placement="top" data-toggle="tooltip" onclick="confirm_modal('<?php echo base_url(); ?>admin/work/delete/<?php echo $row->work_id; ?>');" href="#" data-original-title="" title=""><span class="label label-danger mr6 mb6"><i aria-hidden="true" class="fa fa-trash-o"></i>Delete</span></a>
                                    </td>
                                </tr>
                            <?php } ?>
                    </tbody> -->
                    </table>
                </form>
                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End contentwrapper -->
</div>
<!-- End #content -->

<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
    $('#form-filter table.datatableList tr > td .w98p #FromDate').on('click',function(){
        alert(1);
    })
  </script> -->

<script type="text/javascript">
$(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '0' : '1';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this);
        url = "<?php echo base_url(); ?>admin/work/update_status/";
        $.ajax({
          type:"POST",
          url: url,
          data: {id:$(current_element).attr('data'),status:status},
          success: function(data)
          {   
            location.reload();
          }
        });
      }      
    });
$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
})

function validate(){
    //alert("test"); return false;
    var chks = document.getElementsByName('checkbox[]');
    var hasChecked = false;
    for (var j = 0; j < chks.length; j++){
        if (chks[j].checked){
            var result = confirm("Are you sure to remove this information ?");
            if (result) {
                hasChecked = true;
                break;
            }if (hasChecked == false){
                return false;    
            }
        }
    }if (hasChecked == false){
        //lert("Please select at least one.");
        return false;
    }return true;

}

$(document).ready(function (){
   
    $("#importCSV").validate({
        rules: {
            importData: {required: true,
                    extension: 'csv',
                },
        },messages: {
            importData: {
                    required: "Please choose file",
                    extension: "Please upload only csv file",
                }
        }
    });
});
</script>

<script type="text/javascript">

$(document).ready(function() {
 
var editor;    

var table = $('.datatableList').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url()?>admin/work/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                data.from = $("#fromId").val();
                data.to = $("#fromTo").val();
                data.nameAR = $('#FirstNamear').val();
                data.nameEN = $('#Fistnameen').val();
                data.phone = $('#Phone').val();
                data.work_type = $('#Work_type').val();
                data.created_date_from = $('#FromDate').val();
                data.created_date_to = $('#ToDate').val();
                data.location = $('#select-location').val();
            }            
        },
        initComplete: function() {
            $('.datatableList input[type="text"]').unbind();
            $('.datatableList input[type="text"]').bind('keyup', function(e) {
                if(e.keyCode == 13) {
                      table.page( 'next' ).draw( 'page' );
                     table.ajax.reload(null,false);
                }
            });
              $('#select-location').change(function(){ 
                    table.page( 'next' ).draw( 'page' );
                    //button filter event click
                    table.ajax.reload(null,false);  //just reload table
              });
        },
        "aoColumnDefs" : [ {
            'bSortable' : false,
            'aTargets' : [ 1 ]
        } ],
        "searching":false,
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "paging": true,
        "pagingType": "full_numbers"        
    });

    $('#btn-filter').click(function(){ //button filter event click
      table.page( 'next' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $('.datepicker').change(function(){ //button filter event click
          table.page( 'next' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });


});
</script>
<script src="<?php echo base_url(); ?>assets/js/plugins/tables-data.js"></script>
