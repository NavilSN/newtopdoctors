    <div class="row">                      
        <div class="col-lg-12">
            <!-- col-lg-12 start here -->
            <div class="panel-default toggle panelMove panelClose panelRefresh">
                <div class="panel-body">
                    <div class="tab-pane box" id="add" style="padding: 5px">
                        <div class="box-content">  
                                  <?php $wr_id =  $row->work_id; ?>
                            <form name="frmstudentedit" id="frmstudentedit" method="post" action="<?= base_url() ?>admin/work/update/<?php echo $row->work_id; ?>" enctype="multipart/form-data" class="form-horizontal form-groups-bordered validate"> 
                                <input type="hidden" name="txtuserid" id="txtuserid" value="<?php echo $row->work_id;?>">
                                <input type="hidden" name="work_id" id="work_id" value="<?php echo $row->work_id;?>">
                                
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("First Name Arabic"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="fname_ar" id="fname_ar" value="<?php echo $row->firstname_ar ?>" />
                          
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Last Name Arabic"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="lname_ar" id="lname_ar"  value="<?php echo $row->lastname_ar ?>" />                          
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("First Name in English"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="fname_en" id="fname_en"  value="<?php echo $row->firstname_en ?>" />
                        </div>
                    </div>      
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Last Name in English"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="lname_en" id="lname_en"  value="<?php echo $row->lastname_en ?>" />
                        </div>
                    </div>             
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Address"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="address" id="address" ><?php echo $row->address;  ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Address in English"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="address_en" id="address_en" ><?php echo $row->address_en;  ?></textarea>
                        </div>
                    </div>     
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Biography"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="biography" id="biography" ><?php echo $row->work_biography;  ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Biography in English"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="biography_en" id="biography_en" ><?php echo $row->work_biography_en;  ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work Hours"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="work_hours" value="<?php echo $row->work_hours;  ?>" id="work_hours" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work Hours  in English"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="work_hours_en"  value="<?php echo $row->work_hours_en;  ?>" id="work_hours_en" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("phone"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="phone" id="phone"  placeholder="enter semicolon separated phone" value="<?php echo $row->phone;  ?>" />
                        </div>
                    </div>
                                <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Mobile No"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control"  placeholder="enter semicolon separated mobile " name="mobileno" id="mobileno" value="<?php echo $row->mobileno;  ?>"  />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Email"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="email" id="email" value="<?php echo $row->email;  ?>"  />
                            <span id="emailerror" style="color: red"></span>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Location"); ?></label>
                        <div class="col-sm-3">                       
                                <?php echo getChildsAdmin($row->location_id); ?>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work Type"); ?></label>
                        <div class="col-sm-3">
                            <select name="work_type" class="form-control" id="work_type">
                                <option value="">Select</option>
                                <option value="Clinic" <?php if($row->work_type=='Clinic') echo "selected"; ?>>Clinic</option>
                                <option value="Hospital" <?php if($row->work_type=='Hospital') echo "selected"; ?>>Hospital</option>                                
                                <option value="Radiology Lab" <?php if($row->work_type=='Radiology Lab') echo "selected"; ?>>Radiology Lab</option>
                                <option value="Medical Lab" <?php if($row->work_type=='Medical Lab') echo "selected"; ?>>Medical Lab</option>
                            </select>
                        </div>
                    </div>

                   
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Latitude"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="google_map_latitude" id="google_map_latitude" value="<?php echo $row->work_latitude;  ?>" />
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Longitude"); ?></label>
                        <div class="col-sm-3">
                            <input type="text"  class="form-control" name="google_map_longtude" id="google_map_longtude" value="<?php echo $row->work_longitude;  ?>"/>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Zoom"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" maxlength="2" minlength="1" class="form-control" name="google_map_zoom" id="google_map_zoom"  value="<?php echo $row->work_zoom;  ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Doctor List"); ?></label>
                        <div class="col-sm-3">
                        <?php
                        if(count($doctorWork)>0){
                        foreach ($doctorWork as $doctorWorkRow) { ?>                           
                            <a href="<?php echo base_url(); ?>admin/doctor/edit/<?php echo $doctorWorkRow->doctorId; ?>" target="_blank"><span class="label label-primary mr6 mb6"><?php echo $doctorWorkRow->FirstName.' '.$doctorWorkRow->LastName;  ?></span></a>
                        <?php } }else{?>
                        <label class="col-sm-5 control-label">No Record</label>
                        <?php } ?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Profile Photo"); ?></label>
                        <div class="col-sm-3">
                            <input type="file" class="form-control" name="profilePhoto" id="profilePhoto" />
                            <input type="hidden"  name="existingfile" value="<?php echo $row->photo; ?>" />
                            <span id="imgerror" style="color:red;"></span>
                            <?php if(empty($row->photo)){?>
                            <img src="<?php echo base_url();?>/uploads/user.jpg" width="50" height="50" >
                            <?php }else{?>
                           <span id="removeImg"> <img src="<?php echo base_url();?>/uploads/workimage/<?php echo $row->photo; ?>" width="50" height="50" >
                           <a  onclick="return imageremove('<?php echo $wr_id; ?>','<?php echo $row->photo; ?>');"><i class="fa fa-times" aria-hidden="true"></i></a></span>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Status"); ?></label>
                        <div class="col-sm-3">
                            <select name="status" class="form-control" id="status">
                                <option value="1" <?php if($row->status=='1') echo "selected"; ?>>Active</option>
                                <option value="0" <?php if($row->status=='0') echo "selected"; ?>>Inactive</option>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-3">
                            <button type="submit" class="btn btn-info vd_bg-green"><?php echo ucwords("Update");?></button>
                             <a title="" data-original-title="" href="#" onclick="confirm_modal('<?php echo base_url(); ?>admin/work/delete/<?php echo $wr_id; ?>');" data-toggle="tooltip" data-placement="top" style="" ><span class="label label-danger mr6 mb6" style="padding:7px;"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</span></a>
                        </div>
                    </div>            
                </div>   
                </form>
                </div>
            </div>
            </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
function imageremove(doctor_id,photo){
    //alert(doctor_id);
    var doc_id =  doctor_id;
    var image = photo;

var txt;
    var r = confirm("Are you sure want to remove this image!");
    if (r == true) {
       $.ajax({        
        type:"POST",
        url:"<?php echo base_url(); ?>admin/work/removeImage/"+doc_id,
        data:'photo='+image,
        success:function(response){
            //alert(response);
            if(response=="1")
            {
           $("#removeImg").hide(1000);
            }
        }

       });
    } else {
        txt = "You pressed Cancel!";
    }


}
    $(document).ready(function () {
        if($('#work_type').val()=='Clinic'){
            $('.isPrivate').show();
        }
    $('#work_type').on('change', function() {
        if(this.value=='Clinic'){
            $('.isPrivate').show();
        }else{
            $('.isPrivate').hide();
            $('#isPrivate').removeAttr('checked');

        }
    });

        $("#birthdate1").datepicker({
        });
        $("#basic-datepicker").datepicker({
            endDate: new Date(),
            format: "MM d, yyyy",
            autoclose: true});

        jQuery.validator.addMethod("mobile_no", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid contact no.');
        jQuery.validator.addMethod("email_id", function (value, element) {
            return this.optional(element) || /^(\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}\s*?;?\s*?)+$/.test(value);
        }, 'Please enter a valid email address.');

        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');

        jQuery.validator.addMethod("zip_code", function (value, element) {
            return this.optional(element) || /^[0-9]+$/.test(value);
        }, 'Please enter a valid zip code.');

        $("#frmstudentedit").validate({
           rules: {
                 fname_ar:"required",
                lname_ar:"required",
                fname_en:"required",
                lname_en:"required",
                address:"required",
                address_en:"required",
                /*email:{     email_id: true,
                            remote: {
                                url: "<?php echo base_url(); ?>admin/work/check_user_email/edit",
                                type: "post",
                                data: {
                                     email: function () {
                                        return $("#email").val();
                                    },
                                     userid: function () {
                                        return $("#txtuserid").val();
                                    },
                                }
                            }
                        },*/
                
                work_type: "required",                
                profilePhoto: {
                    extension: 'gif|png|jpg|jpeg',
                },     
            },
            messages: {
                 fname_ar:"Enter first name",
                lname_ar:"Enter last name",
                fname_en:"Enter first name",
                lname_en:"Enter last name",
                address:"Enter Address",
                address_en:"Enter Address",
                /*email: {
                   // required: "Enter email",
                    email_id: "Enter valid email",
                   // remote: "Email already exists",
                }, */               
                work_type: "Select Work Type",
                profilePhoto: {
                    extension: "Upload valid file",
                }
            }
        });
    });
</script>
