<?php 
$this->load->model('admin/Doctor_master_en_model');
$this->load->model('admin/Work_master_model');
$this->load->model('admin/Location_master_model');
//$doctor=$this->Work_master_model->get_all_doctors_data();
$location=$this->Location_master_model->get_all();
?>
<div class="row">                      
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body"> 
              

                <?php echo form_open(base_url() . 'admin/work/create', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'frmstudent', 'target' => '_top', "enctype" => "multipart/form-data")); ?>
                <div class="padded">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("First Name Arabic"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="fname_ar" id="fname_ar" />
                          
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Last Name Arabic"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="lname_ar" id="lname_ar" />                          
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("First Name in English"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="fname_en" id="fname_en" />
                        </div>
                    </div>      
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Last Name in English"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="lname_en" id="lname_en" />
                        </div>
                    </div>       
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Address"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="address" id="address" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Address in English"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="address_en" id="address_en" ></textarea>
                        </div>
                    </div>  
                     <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Biography"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="biography" id="biography" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Biography in English"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="biography_en" id="biography_en" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work Hours"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="work_hours" id="work_hours" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work Hours  in English"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="work_hours_en" id="work_hours_en" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("phone"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control"  placeholder="enter semicolon separated phone " name="phone" id="phone" />
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Mobile No"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="mobileno"  placeholder="enter semicolon separated mobile " id="mobileno" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Email"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="email" id="email"  />
                            <span id="emailerror" style="color: red"></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Location"); ?></label>
                        <div class="col-sm-3">
                           
                            <?php echo getChildsAdmin(); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work Type"); ?></label>
                        <div class="col-sm-3">
                            <select name="work_type" class="form-control" id="work_type">
                                <option value="">Select</option>
                                <option value="Clinic">Clinic</option>
                                <option value="Hospital">Hospital</option>
                                <option value="Radiology Lab">Radiology Lab</option>
                                <option value="Medical Lab">Medical Lab</option>
                            </select>
                        </div>                        
                    </div>
                      <div class="form-group" >
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Latitude"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="google_map_latitude" id="google_map_latitude" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Longitude"); ?></label>
                        <div class="col-sm-3">
                            <input type="text"  class="form-control" name="google_map_longtude" id="google_map_longtude" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Zoom"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" maxlength="2" minlength="1" class="form-control" name="google_map_zoom" id="google_map_zoom" value=""/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Profile Photo"); ?></label>
                        <div class="col-sm-3">
                            <input type="file" class="form-control" name="profilePhoto" id="profilePhoto" />
                            <span id="imgerror" style="color:red;"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Status"); ?></label>
                        <div class="col-sm-3">
                            <select name="status" class="form-control" id="status">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-3">
                            <button type="submit" class="btn btn-info vd_bg-green"><?php echo ucwords("Add");?></button>
                        </div>
                    </div>            
                </div>   
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


$(document).ready(function () {

    $('#work_type').on('change', function() {
      if(this.value=='clinic'){
        $('.isPrivate').show();
      }else{
        $('.isPrivate').hide();
      }
    });

         jQuery.validator.addMethod("mobile_no", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid contact no.');
        
        jQuery.validator.addMethod("email_id", function (value, element) {
            return this.optional(element) || /^(\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}\s*?;?\s*?)+$/.test(value);
        }, 'Please enter a valid email address.');
        $("#frmstudent").validate({
            rules: {
                 fname_ar:"required",
                lname_ar:"required",
                fname_en:"required",
                lname_en:"required",
                address:"required",
                address_en:"required",                
                /*email:{ email_id: true,
                        remote: {
                            url: "<?php echo base_url(); ?>admin/work/check_user_email",
                            type: "post",
                            data: {
                                email: function () {
                                return $("#email").val();
                                },
                            }
                        }
                },*/
                location: "required",          
                work_type: "required",            
                profilePhoto: {
                    extension: 'gif|png|jpg|jpeg',
                },     
            },
            messages: {
                fname_ar:"Enter first name",
                lname_ar:"Enter last name",
                fname_en:"Enter first name",
                lname_en:"Enter last name",
                address:"Enter Address",
                address_en:"Enter Address",               
                /*email: {
                    email_id: "Enter valid email",
                   // remote: "Email already exists",
                },*/
                location: "Select location",
                work_type: "Select Work Type",
                profilePhoto: {
                    extension: "Upload valid file",
                }
            }
        });
    });
</script>
