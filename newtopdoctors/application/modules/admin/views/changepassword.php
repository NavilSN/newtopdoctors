<?php
$this->load->model('admin/Admin_model');
$user = $this->Admin_model->get($param2);
?>
<!-- Start .row -->
<div class="row">                      
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body"> 
                <div class="box-content">     
                                                    
                    <?php echo form_open(base_url() . 'admin/change_password/' . $user->admin_id, array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'user-create-form', 'target' => '_top', "enctype" => "multipart/form-data")); ?>
                    <input type="hidden" name="txtuserid" id="txtuserid" value="<?php echo $user->admin_id;?>">
                    <div class="padded">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" name="password" id="password" required="" />
                            </div>	
                        </div>
                         <div class="form-group">
                            <label class="col-sm-4 control-label">New Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" name="newpassword" id="newpassword"  required="" />
                            </div>  
                        </div>
                         <div class="form-group">
                            <label class="col-sm-4 control-label">Confirm Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" name="cpassword" id="cpassword"  required="" />
                            </div>  
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-info vd_bg-green" ><?php echo ucwords("add"); ?></button>
                            </div>
                        </div>
                        </form>               
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $.validator.setDefaults({
        submitHandler: function (form) {
            form.submit();
        }
    });
    $().ready(function () {
        $("#user-create-form").validate({
            rules: {
                password: {
                            required: true,
                            remote: {
                                url: "<?php echo base_url(); ?>admin/check_password",
                                type: "post",
                                data: {
                                     password: function () {
                                        return $("#password").val();
                                    },
                                    userid: function () {
                                          return $("#txtuserid").val();
                                    },
                                }
                            }
                        },
                newpassword: "required",
                cpassword: {
                            required:true,
                            equalTo : "#newpassword"
                        },
                },
            messages: {
                password: {
                    required: "Enter password",
                    remote: "Password not match with our system",
                },
                newpassword: "Enter new password",
                cpassword: {
                    required:"Enter Confirm password",
                    equalTo:"New password and confirm password not match"

                },
            }
        });
    });
$("#myModalLabel2").text('Change Password'); 
</script>