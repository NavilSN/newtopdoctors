
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<!-- polyfiller file to detect and load polyfills -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script>
  webshims.setOptions('waitReady', false);
  webshims.setOptions('forms-ext', {types: 'date'});
  webshims.polyfill('forms forms-ext');

</script>
<style type="text/css">
    input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,
input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success{ width: 150 !important; }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-default">
            <div class="panel-body">
            <div class="table-responsive" >
            <form name="bulk_action_form" action="<?php echo base_url(); ?>admin/reviews/deleteAll" method="post" onsubmit="return validate();"/>
             <div class="loadingtopdoctor" >Loading&#8230;</div>
            <table class="table table-striped table-bordered table-responsive example datatableList display nowrap review_data" cellspacing=0 width=100%>
                        <thead>
                            <tr>
                                <th>Rating Id</th>
                                <th><div class="delete-btn"><input type="submit" class="btn btn-danger deleteLink delete-icon-style" name="bulk_delete_submit" value="Delete"/></div></th>
                                <th>Doctor Id</th>
                                <th>User</th>
                                <th>Average Rating</th>
                                <th>Review Count</th>
                                <th>Comment</th>
                                <th>Date Posted</th>
                                <th>Review From</th>                                
                                <th>Status</th>

                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <td>
                                    <input id="search_from_id" class="form-control" type="text" placeholder="From" />
                                    <input id="search_to_id" class="form-control" type="text" placeholder="To" />
                                </td>
                                <td><input name="checkbox" type="checkbox" class="check" id="checkAll"/></td>
                                <td>
                                    <input id="search_doctor_id" class="form-control" type="text" />
                                </td>
                                <td>
                                    <input id="search_user" class="form-control" type="text" />
                                </td>
                                <td>
                                    <input id="search_avg_rating" class="form-control" type="text" />
                                </td>
                                <td>
                                    <input id="search_total_count" class="form-control" type="text" placeholder="From" />
                                    <input id="search_count_to" class="form-control" type="text" placeholder="To" />
                                </td>
                                <td style="width: 150px !important;">
                                    <div class="w98p">
                                        <input id="search_comment" class="form-control" type="text" />
                                    </div>
                                </td>
                                <td class="Created_at_from">
                                    <div class="w98p">
                                    <input id="search_date" class="form-control datepicker" placeholder="From" type="date" />
                                    <input id="date_to" class="form-control datepicker" placeholder="To" type="date" />
                                    <span style="width: 28px; display: inline-block;"></span>
                                    </div>
                                </td>
                                <td><select name="registration_from" id="registration_from" class="form-control dropdown">
                                    <option value="">Select</option>
                                    <option value="1">Website</option>
                                    <option value="2">App</option>
                                </select>
                                </td>
                                <td><select name="review_status" class="form-control dropdown" id="review_status">
                                    <option value="">Select</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select></td>
                            </tr>
                        </thead>                    
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- <script src="//code.jquery.com/jquery-1.10.2.min.js"></script> -->
<script>
 $(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '1' : '0';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this).attr('id');
        var url_status = "<?php echo base_url(); ?>admin/reviews/update_status/";
        $.ajax({
          type:"POST",
          url: url_status,
          data: {id:current_element,status:status},
          success: function(data)
          {
             // return false;
             // alert(data);
            location.reload();
          }
        });
      }
    });

$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
})

function validate(){
    //alert("test"); return false;
    var chks = document.getElementsByName('checkbox[]');
    var hasChecked = false;
    for (var j = 0; j < chks.length; j++){
        if (chks[j].checked){
            var result = confirm("Are you sure to remove this information ?");
            if (result) {
                hasChecked = true;
                break;
            }if (hasChecked == false){
                return false;
            }
        }
    }if (hasChecked == false){
       // alert("Please select at least one.");
        return false;
    }return true;

}
</script>

<script src="<?php echo base_url(); ?>js/1.10.9/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>js/1.10.9/input.js"></script>
<script type="text/javascript">
$(document).ajaxStart(function () {
    $('.loadingtopdoctor').show();
});
$(document).ajaxComplete(function () {
    $('.loadingtopdoctor').hide();
});

    $(document).ready(function(){
var table = $('.datatableList').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url() ?>admin/reviews/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                data.avg_rating = $("#search_avg_rating").val();
                data.review_count = $("#search_review_count").val();
                data.date = $("#search_date").val();
                data.date_to = $('#date_to').val();
                data.from_id = $('#search_from_id').val();
                data.to_id = $('#search_to_id').val();
                data.doctor_id = $('#search_doctor_id').val();
                data.comment = $('#search_comment').val();
                data.search_total_count = $('#search_total_count').val();
                data.search_count_to = $('#search_count_to').val();
                data.search_user = $("#search_user").val();
                data.registration_from = $("#registration_from").val();
                data.review_status = $("#review_status").val();
            }
        },
        initComplete: function() {
            $('.datatableList input[type="text"]').unbind();
            $('.datatableList input[type="text"]').bind('keyup', function(e) {
                if(e.keyCode == 13) {
                    $("input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success").css({'width':'150px !important'});
                      table.page( 'first' ).draw( 'page' );
                     table.ajax.reload(null,false);
                }
            });
        },
        "aoColumnDefs" : [ {
            'bSortable' : false,
            'aTargets' : [ 1,8,9 ]
        } ],
        "searching":false,
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "paging": true,
        "pagingType": "input",
	"order": [[ 0, "desc" ]]
    });

    $('#btn-filter').click(function(){ //button filter event click
      //table.page( 'next' ).draw( 'page' );
          /*$("input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success,input.ws-date.ws-inputreplace.form-control.datepicker.has-input-buttons.user-success").css({'width':'150px !important'});*/
          table.page( 'first' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $("#search_avg_rating").change(function(){
            table.page( 'first' ).draw( 'page' );
    table.ajax.reload(null,false);  //just reload table
    });
    $(".datepicker").change(function(){
            table.page( 'first' ).draw( 'page' );
    table.ajax.reload(null,false);  //just reload table
    });
    /*$("#date_to").change(function(){
    table.ajax.reload(null,false);  //just reload table
    });*/

    $("#registration_from").change(function(){
            table.page( 'first' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $("#review_status").change(function(){
table.page( 'first' ).draw( 'page' );
    table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });
 /*   table.on( 'draw', function () {
     $('tr td:nth-child(6)').each(function (){
          $(this).addClass('comment-det')
          $(".comment-det").css({'white-space':'-o-pre-wrap','word-wrap':'break-word','white-space':'pre-wrap','white-space':'-moz-pre-wrap','white-space':'-pre-wrap'});
     })
    });*/
});


/*
$(document).ready(function() {

var table = $('.datatableList').DataTable({

        "searching":false,
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "paging": true,
        "pagingType": "full_numbers"
    });



});*/
</script>
