<?php
$this->load->model('admin/Doctor_details_model');
$this->load->model('admin/Doctor_master_en_model');
$this->load->model('admin/Doctor_master_model');
$this->db->select('doctor_master.doctor_id,doctor_master_en.first_name as en_f_name,doctor_master_en.last_name as en_l_name,doctor_master_en.address,doctor_details.email,doctor_details.mobile_number,specialty_master.name_en as spmaster_name_en');
$this->db->join('doctor_master_en', 'doctor_master.doctor_id = doctor_master_en.doctor_id');
$this->db->join('doctor_details', 'doctor_master.doctor_id = doctor_details.doctor_id');
$this->db->join('specialty_master', 'doctor_details.speciality_id = specialty_master.specialties_id');
$doctor=$this->db->get('doctor_master')->result();



$this->db->select('d.first_name AS f_ar,d.last_name AS l_ar,sm.name_en AS speciality, lm.name_en AS location,dd.doctor_id AS doctor_id,dd.email, dd.mobile_number,dd.phone_number,dd.gender,dd.created_at,dd.updated_at');
$this->db->from('doctor_master d');
$this->db->join('doctor_details dd','dd.doctor_id=d.doctor_id','left');
$this->db->join('specialty_master sm', 'dd.speciality_id = sm.specialties_id','left');
$this->db->join('location_master lm', 'dd.location_id = lm.location_id','left');
//$this->db->limit(50);
$doctors =  $this->db->get()->result();

$this->db->order_by('name_en');
$speciality = $this->db->get('specialty_master')->result();

//print_r($speciality);

?>
<!-- cdn for modernizr, if you haven't included it already -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<!-- polyfiller file to detect and load polyfills -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script>
  webshims.setOptions('waitReady', false);
  webshims.setOptions('forms-ext', {types: 'date'});
  webshims.polyfill('forms forms-ext');
</script>


<div class="row">                      
    <div class="col-lg-12">
        <div class="panel-default">
            <div class="panel-body">

         
            

            <div class="clearfix">	
	            <div class="row">
				    <div class="col-sm-3">
			            <a class="links" href="<?php echo base_url(); ?>admin/doctor/create" target="_blank"><i class="fa fa-plus"></i> Create </a>
				    </div>
		                <form action="<?php echo base_url(); ?>admin/doctor/import" method="post" name="importCSV" id="importCSV" enctype="multipart/form-data">
 <a href="<?php echo base_url(); ?>csv/sample-data.xls" target="_blank" download="" style="text-decoration: none;">Sample File</a>
				    <div class="col-sm-3 text-right">
		                     <div class="control-group">
		                        <div>
		                            <input type="file" name="importData" id="importData">
		                        </div>
		                    </div>
		            </div>
		            <div class="col-sm-3 text-left">
		                    <div class="control-group">
		                        <div class="controls">
		                            <button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Import Data</button>
		                        </div>
		                    </div>
				    </div>
		                </form>
				    <div class="col-sm-3  text-right">
		                 <form action="<?php echo base_url(); ?>admin/doctor/export" method="post" name="export_excel">
		                    <div class="control-group">
		                        <div class="controls">
		                            <button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Export Data</button>
		                        </div>
		                    </div>
		                </form>				    
				    </div>
				</div>
			</div>		

               
            <div class="table-responsive" >
                <form name="bulk_action_form" action="<?php echo base_url(); ?>admin/doctor/deleteAll" method="post" onsubmit="return validate();" id="form-filter"/>
                <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                 <!-- <table border="0" cellpadding="5" cellspacing="5">
        <tbody><tr>
            <td>From :</td>
            <td><input id="min" name="min" type="text"></td>
        </tr>
        <tr>
            <td>To:</td>
            <td><input id="max" name="max" type="text"></td>
        </tr>
    </tbody></table> -->
                 <table  class="table table-hover table-striped table-bordered table-responsive example datatableList display nowrap" cellspacing="0" width="100%">
                     
                <thead>
                <tr>
                   <th>Id</th>
                   <th ><input name="checkbox" type="checkbox" class="check" id="checkAll"/><div class="delete-btn"><input type="submit" class="btn btn-danger deleteLink delete-icon-style" name="bulk_delete_submit" value="Delete"/></div></th>
                    <th> Name</th>
                    <!--<th>Last Name</th>                                      -->
                    <th>Email</th>                   
                    <th>Phone no</th>                   
                    <th>Mobile no</th>                   
                    <th>Specialty</th>                   
                    <th>Location</th>                   
                    <th>Created Date</th>
                    <th>Updated Date</th>                    
                    <th>Action</th>
                </tr>        
                
                </thead>
                <thead>
                    <tr>
                   <!--  <form id="form-filter" class="form-horizontal"> -->
                    <td>
                       <!--   <div class="w98p">
                        <input type="text" name="page" id="pageno" class="form-control" placeholder="Page"></div>-->
                         <div class="w98p">
                        <input type="text" name="fromId" id="fromId" class="form-control" placeholder="From"></div>
                         <div class="w98p">
                       <input type="text" name="fromTo" id="fromTo" class="form-control" placeholder="To"></div>
                    </td>
                    <td></td>

                    <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Name" class="form-control" id="FirstName">                
                        </div>
                    </div></td>
                   <!-- <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Last Name" class="form-control" id="LastName">                
                        </div>
                    </div></td>-->
                     <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Email" class="form-control" id="Email">                
                        </div>
                    </div></td>
                    <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Phone Number" class="form-control" id="Phone_number">                
                        </div>
                    </div></td>
                     <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Mobile Number" class="form-control" id="Mobile_number">                
                        </div>
                    </div></td>
                    <td><div class="col-sm-4">
                        <div class="w98p">
                            <input type="text" placeholder="Speciality" class="form-control" id="Speciality">                
                        </div>
                    </div></td>
                    <td><div class="col-sm-4">
                        <div class="w98p">
                            <?php  echo getChildsAdmin(); ?>
                               
                        </div>
                    </div></td>
                    <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="date" placeholder="Created Date From" class="form-control datepicker" id="Created_at_from" value="<?php echo set_value('date'); ?>">
                                        
                                    <input type="date" placeholder="Created Date To" class="form-control datepicker" id="Created_at_to" value="<?php echo set_value('date'); ?>">                
                        </div>
                    </div></td>
                    <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="date" placeholder="Updated Date From" class="form-control datepicker" id="Updated_at_from">
                                    <input type="date" placeholder="Updated Date To" class="form-control datepicker" id="Updated_at_to">

                        </div>
                    </div></td>
                    <td><button type="button" id="btn-filter" class="btn btn-primary">Filter</button></td>                
                </tr>
                </thead>
             
                </table>
                <br><br><br>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script>

$(document).ready(function (){
//    $('.datepicker').datepicker({
//            format: 'yyyy/mm/dd',
//            todayHighlight: true,
//            autoclose: true
//
//        });
    $("#importCSV").validate({
        rules: {
            importData: {required: true,
                    extension: 'csv',
                },
        },messages: {
            importData: {
                    required: "Please choose file",
                    extension: "Please upload only csv file",
                }
        }
    });

     $(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '0' : '1';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this);
        url = "<?php echo base_url(); ?>admin/doctor/update_status/";
        $.ajax({
          type:"POST",
          url: url,
          data: {id:$(current_element).attr('data'),status:status},
          success: function(data)
          {   
            location.reload();
          }
        });
      }      
    });
});

 $("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
})

function validate(){
    //alert("test"); return false;
    var chks = document.getElementsByName('checkbox[]');
    var hasChecked = false;
    for (var j = 0; j < chks.length; j++){
        if (chks[j].checked){
            var result = confirm("Are you sure to remove this information ?");
            if (result) {
                hasChecked = true;
                break;
            }if(hasChecked == false){
                return false;
            }
        }
    }if (hasChecked == false){
       // alert("Please select at least one.");
        return false;
    }return true;
    
}
</script>
<script src="<?php echo base_url(); ?>js/1.10.9/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>js/1.10.9/input.js"></script>
<!-- <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="https://editor.datatables.net/extensions/Editor/js/dataTables.editor.min.js"></script> -->
<script type="text/javascript">

$(document).ready(function() {

var editor;    
    /*$('.datatableList tfoot th').each( function () {
var title = $('.datatableList thead th').eq( $(this).index() ).text();
$(this).html( '<input type="text" placeholder="'+title+'" />' );
} );*/
var table = $('.datatableList').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url()?>admin/doctor/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                
                data.pageno = $("#pageno").val();
                data.from = $("#fromId").val();
                data.to = $("#fromTo").val();
                data.mobile_number = $('#Mobile_number').val();
                data.firstName = $('#FirstName').val();
                data.lastName = $('#LastName').val();
                data.email = $('#Email').val();
                data.gender = $('#Gender').val();
                data.speciality = $('#Speciality').val();
                data.location = $('#select-location').val();
                data.phone_number = $('#Phone_number').val();
                data.mobile_number = $('#Mobile_number').val();
                data.created_at_from = $('#Created_at_from').val();
                data.created_at_to = $('#Created_at_to').val();
                data.updated_at_from = $('#Updated_at_from').val();                
                data.updated_at_to = $('#Updated_at_to').val();
                
                //data.from_id =  $('#from_id').val();
                //data.to_id =  $('#to_id').val();
                
            }            
        },
        initComplete: function() {
        $('.datatableList input[type="text"]').unbind();
            $('.datatableList input[type="text"]').bind('keyup', function(e) {
                if(e.keyCode == 13) {
               //table.page( 'previous' ).draw( 'page' );
                     table.ajax.reload(null,false);
                 //    $(".disablesort").removeClass('sorting_asc');
                  //   $(".disablesort").addClass('sorting');
                }
            });
             $('#select-location').change(function(){ 
              // table.page( 'previous' ).draw( 'page' );
                    //button filter event click
                    table.ajax.reload(null,false);  //just reload table
              });
              
        },
            "aoColumnDefs" : [ {
            'bSortable' : true,
            'aTargets' : [ 0 ]
        } ],
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "searching": false,
        "paging": true,
        "pagingType": "input",
        "order": [[ 0, "asc" ]]

    });
    

    $('#btn-filter').click(function(){ //button filter event click
    table.page( 'next' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $('.datepicker').change(function(){ 
        //button filter event click
        table.page( 'next' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        
        table.ajax.reload(null,false);  //just reload table
    });
   

});

</script>
    <style>
    #select-location {
    width: auto;
}
#bulk_action_form input{
width: auto !important;
}
    </style>

