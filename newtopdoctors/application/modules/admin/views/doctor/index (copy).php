<?php
$this->load->model('admin/Doctor_details_model');
$this->load->model('admin/Doctor_master_en_model');
$this->load->model('admin/Doctor_master_model');
$this->db->select('doctor_master.doctor_id,doctor_master_en.first_name as en_f_name,doctor_master_en.last_name as en_l_name,doctor_master_en.address,doctor_details.email,doctor_details.mobile_number,specialty_master.name_en as spmaster_name_en');
$this->db->join('doctor_master_en', 'doctor_master.doctor_id = doctor_master_en.doctor_id');
$this->db->join('doctor_details', 'doctor_master.doctor_id = doctor_details.doctor_id');
$this->db->join('specialty_master', 'doctor_details.speciality_id = specialty_master.specialties_id');
$doctor=$this->db->get('doctor_master')->result();



$this->db->select('d.first_name AS f_ar,d.last_name AS l_ar,sm.name_en AS speciality, lm.name_en AS location,dd.doctor_id AS doctor_id,dd.email, dd.mobile_number,dd.phone_number,dd.gender,dd.created_at,dd.updated_at');
$this->db->from('doctor_master d');
$this->db->join('doctor_details dd','dd.doctor_id=d.doctor_id','left');
$this->db->join('specialty_master sm', 'dd.speciality_id = sm.specialties_id','left');
$this->db->join('location_master lm', 'dd.location_id = lm.location_id','left');
//$this->db->limit(50);
$doctors =  $this->db->get()->result();

$speciality = $this->db->get('specialty_master')->result();

//print_r($speciality);

?>
<div class="row">                      
    <div class="col-lg-12">
        <div class="panel-default">
            <div class="panel-body">

            <!--<form id="form-filter" class="form-horizontal">
            
                    <div class="form-group">
                        <label for="FirstName" class="col-sm-2 control-label">First Name</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="FirstName">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label">Last Name</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="LastName">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Gender" class="col-sm-2 control-label">Gender</label>
                        <div class="col-sm-4">
                            <select name="Gender" id="Gender" class="form-control">
                                <option value="0">-Select Gender-</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Location" class="col-sm-2 control-label">Location</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="Location">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Mobile_number" class="col-sm-2 control-label">Mobile_number</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="Mobile_number">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="Email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Speciality" class="col-sm-2 control-label">Speciality</label>
                        <div class="col-sm-4">
                            <select name="Speciality" id="Speciality" class="form-control">
                                <option value="0">-Select Speciality-</option>
                                <?php
                                $option = '';
                                foreach ($speciality as $key => $value) {
                                 $option .= "<option value='".$value->name_en."'>";
                                 $option .= $value->name_en;
                                 $option .= '</option>';
                                }
                                echo $option;
                                ?>
                            </select>
                           
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Phone_number" class="col-sm-2 control-label">Phone_number</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="Phone_number">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Created_at" class="col-sm-2 control-label">Created_at</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="Created_at">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Updated_at" class="col-sm-2 control-label">Updated_at</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="Updated_at">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </form>-->
            <form id="form-filter" class="form-horizontal">
            
                <div class="row">
                    <div class="col-sm-3">
                        <div class="w98p">
                        <input type="text" placeholder="First Name" class="form-control" id="FirstName">
                        </div>
                    </div>  
                    <div class="col-sm-3">
                        <div class="w98p">
                        <input type="text" placeholder="Last Name" class="form-control" id="LastName">
                        </div>
                    </div>  
                     <div class="col-sm-3">
                        <div class="w98p">
                        <input type="text" placeholder="Phone No." class="form-control" id="Phone_number">
                        </div>
                    </div>  
                  <!--  <div class="col-sm-3">
                        <div class="w98p">
                        <input type="text" placeholder="Mobile No." class="form-control" id="Mobile_number">
                        </div>
                    </div>  -->
                  
                    <div class="clearfix"></div> 
                    <div class="col-sm-3">
                        <div class="w98p">
                        <input type="text" placeholder="Created Date" class="form-control" id="Created_at">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="w98p">
                        <input type="text" placeholder="Updated Date" class="form-control" id="Updated_at">
                        </div>
                    </div>
                     <div class="col-sm-3">
                        <div class="w98p">
                            
                        <input type="text" placeholder="From Id" class="form-control" id="from_id">
                        </div>
                    </div>
                     <div class="col-sm-3">
                        <div class="w98p">
                        <input type="text" placeholder="To Id" class="form-control" id="to_id">
                        </div>
                    </div>
                    
                    
                    
                    <div class="col-sm-3">
                        <div class="w98p">
                        <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="w98p">
                        <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </div>   
                <div class="border-bottom"></div>       

                </form>

            <div class="clearfix">	
	            <div class="row">
				    <div class="col-sm-3">
			            <a class="links" href="<?php echo base_url(); ?>admin/doctor/create" target="_blank"><i class="fa fa-plus"></i> Create </a>
				    </div>
		                <form action="<?php echo base_url(); ?>admin/doctor/import" method="post" name="importCSV" id="importCSV" enctype="multipart/form-data">
				    <div class="col-sm-3 text-right">
		                     <div class="control-group">
		                        <div>
		                            <input type="file" name="importData" id="importData">
		                        </div>
		                    </div>
		            </div>
		            <div class="col-sm-3 text-left">
		                    <div class="control-group">
		                        <div class="controls">
		                            <button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Import Data</button>
		                        </div>
		                    </div>
				    </div>
		                </form>
				    <div class="col-sm-3  text-right">
		                 <form action="<?php echo base_url(); ?>admin/doctor/export" method="post" name="export_excel">
		                    <div class="control-group">
		                        <div class="controls">
		                            <button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Export Data</button>
		                        </div>
		                    </div>
		                </form>				    
				    </div>
				</div>
			</div>		

               
            <div class="table-responsive" >
                 <form name="bulk_action_form" action="<?php echo base_url(); ?>admin/doctor/deleteAll" method="post" onsubmit="return validate();"/>
                 <!-- <table border="0" cellpadding="5" cellspacing="5">
        <tbody><tr>
            <td>From :</td>
            <td><input id="min" name="min" type="text"></td>
        </tr>
        <tr>
            <td>To:</td>
            <td><input id="max" name="max" type="text"></td>
        </tr>
    </tbody></table> -->
                 <table  class="table table-hover table-striped table-bordered table-responsive example datatableList display nowrap" cellspacing="0" width="100%">
                     
                <thead>
                <tr>
                   <th>Id</th>
                   <th><input name="checkbox" type="checkbox" class="check" id="checkAll"/><div class="delete-btn"><input type="submit" class="btn btn-danger deleteLink delete-icon-style" name="bulk_delete_submit" value="Delete"/></div></th>
                    <th>First Name</th>
                    <th>Last Name</th>                                      
                    <th>Email</th>                   
                    <th>Phone no</th>                   
                    <th>Mobile no</th>                   
                    <th>Specialty</th>                   
                    <th>Location</th>                   
                    <th>Created Date</th>
                    <th>Updated Date</th>                    
                    <th>Action</th>
                </tr>        
                
                </thead>
             
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script>

$(document).ready(function (){
    $("#importCSV").validate({
        rules: {
            importData: {required: true,
                    extension: 'csv',
                },
        },messages: {
            importData: {
                    required: "Please choose file",
                    extension: "Please upload only csv file",
                }
        }
    });

     $(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '0' : '1';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this);
        url = "<?php echo base_url(); ?>admin/doctor/update_status/";
        $.ajax({
          type:"POST",
          url: url,
          data: {id:$(current_element).attr('data'),status:status},
          success: function(data)
          {   
            location.reload();
          }
        });
      }      
    });
});

 $("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
})

function validate(){
    //alert("test"); return false;
    var chks = document.getElementsByName('checkbox[]');
    var hasChecked = false;
    for (var j = 0; j < chks.length; j++){
        if (chks[j].checked){
            var result = confirm("Are you sure to remove this information ?");
            if (result) {
                hasChecked = true;
                break;
            }if(hasChecked == false){
                return false;
            }
        }
    }if (hasChecked == false){
        alert("Please select at least one.");
        return false;
    }return true;
    
}
</script>
<!-- <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="https://editor.datatables.net/extensions/Editor/js/dataTables.editor.min.js"></script> -->
<script type="text/javascript">

$(document).ready(function() {

var editor;    
    /*$('.datatableList tfoot th').each( function () {
var title = $('.datatableList thead th').eq( $(this).index() ).text();
$(this).html( '<input type="text" placeholder="'+title+'" />' );
} );*/
var table = $('.datatableList').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url()?>admin/doctor/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                data.mobile_number = $('#Mobile_number').val();
                data.firstName = $('#FirstName').val();
                data.lastName = $('#LastName').val();
                data.email = $('#Email').val();
                data.gender = $('#Gender').val();
                data.speciality = $('#Speciality').val();
                data.location = $('#Location').val();
                data.phone_number = $('#Phone_number').val();
                //data.mobile_number = $('#Mobile_number').val();
                data.created_at = $('#Created_at').val();
                data.updated_at = $('#Updated_at').val();
                data.from_id =  $('#from_id').val();
                data.to_id =  $('#to_id').val();
                
            }            
        },
        initComplete: function() {
        $('#form-filter-click input').unbind();
        $('#form-filter-click input').bind('keyup', function(e) {
            if(e.keyCode == 13) {
                 table.ajax.reload(null,false);
            }
        });
        },
            "aoColumnDefs" : [ {
            'bSortable' : false,
            'aTargets' : [ 1 ]
        } ],
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "searching": false
    });

    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });

});
</script>