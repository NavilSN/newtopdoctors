<?php
$this->load->model('admin/Doctor_details_model');
$this->load->model('admin/Doctor_master_en_model');
$this->load->model('admin/Doctor_master_model');
$this->load->model('admin/Location_master_model');
$this->load->model('admin/Specialty_master_model');
$this->load->model('admin/Work_master_model');

$location=$this->Location_master_model->get_all_location_by_name();
$specialties=$this->Specialty_master_model->get_all_speciality();
$work=$this->Work_master_model->get_all_work();

?>
    <div class=row>                      
        <div class="col-lg-12">
            <!-- col-lg-12 start here -->
            <div class="panel-default toggle panelMove panelClose panelRefresh">
                <div class="panel-body">
                    <div class="tab-pane box" id="add" style="padding: 5px">
                        <div class="box-content">  
                                   <?php $dr_id =  $row->doctor_id; ?>
                            <form name="frmstudentedit" id="frmstudentedit" method="post" action="<?= base_url() ?>admin/doctor/update/<?php echo $row->doctor_id; ?>" enctype="multipart/form-data" class="form-horizontal form-groups-bordered validate"> 
                            <input type="hidden" name="txtuserid" id="txtuserid" value="<?php echo $row->doctor_id;?>">
                            <input type="hidden" name="studentid" id="studentid" value="<?php echo $row->doctor_id;?>">
                                
                   <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("First Name"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="f_name" id="f_name" value="<?php echo $row->first_name; ?>" />
                        </div>
                    </div>                                              
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Last Name"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="l_name" id="l_name" value="<?php echo $row->last_name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Address"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="address" id="address"><?php echo $row->address; ?></textarea>
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Major"); ?></label>
                        <div class="col-sm-3">
                            <select class="form-control" name="major" id="major">
                                <option value="">Select</option>
                                <?php foreach ($specialties as $srows) {
                                   //$arrSpeciality=explode(',',$row->major); 
                                 ?>
                                    <option value="<?php echo $srows->specialties_id; ?>"  <?php  if($srows->specialties_id==$row->major){ echo "selected='selected'"; } //(in_array(,$arrSpeciality)) ? 'selected' : ''?>><?php echo $srows->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Biography"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="biography" id="biography" ><?php echo $row->biography; ?></textarea>
                        </div>
                    </div>
                    <div class="">
                        <span><h3>English</h3></span>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("First Name"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="f_name_en" id="f_name_en" value="<?php echo $row1->first_name; ?>" />
                        </div>
                    </div>                                              
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Last Name"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="l_name_en" id="l_name_en" value="<?php echo $row1->last_name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Address"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="address_en" id="address_en" ><?php echo $row1->address; ?></textarea>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" name="latitude" id="latitude" value="<?php echo $row1->google_map_latitude; ?>"/>
                    <input type="hidden" class="form-control" name="longtitude" id="longtitude" value="<?php echo $row1->google_map_longtude; ?>"/>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Major"); ?></label>
                        <div class="col-sm-3">
                            <select class="form-control" name="major_en" id="major_en" >
                                <option value="">Select</option>
                                <?php foreach ($specialties as $row) {
                                   //$arrSpeciality=explode(',',$row2->speciality_id); 
                                 ?>
                                    <option value="<?php echo $row->specialties_id; ?>" <?php if($row->specialties_id==$row1->major){ echo "selected='selected'";} ?>><?php echo $row->name_en; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Biography"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="biography_en" id="biography_en"  ><?php echo $row1->biography; ?></textarea>
                        </div>
                    </div>
                    <div class="">
                        <span><h3>Other Information</h3></span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Email Id"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="email_id" id="email_id" value="<?php echo $row2->email; ?>"  />
                            <span id="emailerror" style="color: red"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work"); ?></label>
                        <div class="col-sm-3">
                            <select name="work_id[]" class="form-control" id="work_id" multiple>
                                <option value="">Select</option>
                                <?php foreach ($work as $row) {
                                    $arrWorkID=explode(',',$row2->work_id); 
                                 ?>
                                    <option value="<?php echo $row->work_id; ?>" <?= (in_array($row->work_id,$arrWorkID)) ? 'selected' : ''?>><?php echo $row->name_en.' '.$row->work_type; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Location"); ?></label>
                        <div class="col-sm-3">                           
                            <?php echo getChildsAdmin($row2->location_id); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Specialties"); ?></label>
                        <div class="col-sm-3">
                            <select name="specialties" class="form-control" id="specialties" >
                                <option value="">Select</option>
                                <?php foreach ($specialties as $row) {
                                   //$arrSpeciality=explode(',',$row2->speciality_id); 
                                 ?>
                                    <option value="<?php echo $row->specialties_id; ?>" style="background-image:url('<?php echo base_url(); ?>images/icon/speciality/<?php echo $row->specialties_id.".png"; ?>'); background-repeat: no-repeat; " <?php if($row->specialties_id==$row2->speciality_id){ echo "selected='selected'";} ?>><?php echo $row->name_en; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work Hours"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="work_hours" id="work_hours" value="<?php echo $row2->working_hours; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work Hours (ENG)"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="work_hours_en" id="work_hours_en" value="<?php echo $row2->working_hours_en; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Phone No"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control"  placeholder="enter semicolon separated mobile " maxlength="100" name="phoneno" id="phoneno" value="<?php echo $row2->phone_number; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Mobile No"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control"  placeholder="enter semicolon separated mobile "  maxlength="100" name="mobileno" id="mobileno" value="<?php echo $row2->mobile_number; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Latitude"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="google_map_latitude" id="google_map_latitude" value="<?php echo $row2->google_map_latitude; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Longitude"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="google_map_longtude" id="google_map_longtude" value="<?php echo $row2->google_map_longtude; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Zoom"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" maxlength="2" minlength="1" class="form-control" name="google_map_zoom" id="google_map_zoom" value="<?php echo $row2->google_map_zoom; ?>"/>
                        </div>
                    </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Is Featured"); ?></label>
                        <div class="col-sm-3">
                            <input type="checkbox"  name="featured" value="1" class="form-control" <?php if($row2->featured=="1"){ echo "checked='checked'";} ?>  />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Gender"); ?></label>
                        <div class="col-sm-3">
                            <input type="radio" name="gen" value="1" <?php if($row2->gender==1) echo "checked";?>>Male
                            <input type="radio" name="gen" value="2" <?php if($row2->gender==2) echo "checked";?>>Female
                        </div>
                        <div class="col-sm-offset-4 col-sm-5">
                            <label for="gen" class="error"></label></div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Profile Photo"); ?></label>
                        <div class="col-sm-3">
                            <input type="file" class="form-control" name="profilePhoto" id="profilePhoto" />
                            <input type="hidden"  name="existingfile" value="<?php echo $row2->photo; ?>" />
                            <span id="imgerror" style="color:red;"></span>
                            <?php if(empty($row2->photo)){?>
                            <img src="<?php echo base_url();?>/uploads/user.jpg" width="50" height="50" >
                            <?php }else{?>
                           <span id="removeImg"> <img src="<?php echo base_url();?>/uploads/doctor_image/<?php echo $row2->photo; ?>" width="50" height="50" >
                           <a  onclick="return imageremove('<?php echo $dr_id; ?>','<?php echo $row2->photo; ?>');"><i class="fa fa-times" aria-hidden="true"></i></a></span>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Status"); ?></label>
                        <div class="col-sm-3">
                            <select name="status" class="form-control" id="visibility">
                                    <option value="1" <?php if($row2->visibility=='1'){ ?>selected<?php } ?>>Visible</option>
                                    <option value="0" <?php if($row2->visibility=='0'){ ?>selected<?php } ?>>Hidden</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-3">
                            <button type="submit" class="btn btn-info vd_bg-green"><?php echo ucwords("Update");?></button>
                           <a title="" data-original-title="" href="#" onclick="confirm_modal('<?php echo base_url(); ?>admin/doctor/delete/<?php echo $dr_id; ?>');" data-toggle="tooltip" data-placement="top" style="" ><span class="label label-danger mr6 mb6" style="padding:7px;"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</span></a>
                        </div>
                    </div>            
                </div>   
                </form>
                </div>
            </div>
            </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
function imageremove(doctor_id,photo){
    //alert(doctor_id);
    var doc_id =  doctor_id;
    var image = photo;
var txt;
    var r = confirm("Are you sure want to remove this image!");
    if (r == true) {
       $.ajax({        
        type:"POST",
        url:"<?php echo base_url(); ?>admin/doctor/removeImage/"+doc_id,
         data:'photo='+image,
        success:function(response){
            //alert(response);
            if(response=="1")
            {
           $("#removeImg").hide(1000);
            }
        }

       });
    } else {
        txt = "You pressed Cancel!";
    }


}

/*$("form").submit(function() {
    $.ajax({
    type: 'POST', 
    url: "<?php echo base_url();?>admin/doctor/latlong/",
    data: { 'address': $('#address_en').val()},
    success: function(response){
        var data = JSON.parse(response);
        $("#latitude").append().val(data.lat);
        $("#longtitude").append().val(data.lng);
    }
    });
}); */ 
    $(document).ready(function () {
        $("#birthdate1").datepicker({
        });
        $("#basic-datepicker").datepicker({
            endDate: new Date(),
            format: "MM d, yyyy",
            autoclose: true});

        jQuery.validator.addMethod("mobile_no", function (value, element) {
            return this.optional(element) || /^\d+(;\d+)*$/.test(value);
        }, 'Please enter a valid contact no.');
      /*  jQuery.validator.addMethod("email_id", function (value, element) {
            return this.optional(element) || /^(\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}\s*?,?\s*?)+$/.test(value);
        }, 'Please enter a valid email address.');*/
        jQuery.validator.addMethod("email_id", function (value, element) {
            return this.optional(element) || /^(\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}\s*?;?\s*?)+$/.test(value);
        }, 'Please enter a valid email address.');
        

        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');

        $("#frmstudentedit").validate({
            rules: {
                f_name:"required",
                l_name:"required",
                f_name_en:"required",
                l_name_en:"required",
                 address:"required",
                address_en:"required",
                /*email_id:{  
                            email_id: true,
                            remote: {
                                url: "<?php echo base_url(); ?>admin/doctor/check_user_email/edit",
                                type: "post",
                                data: {
                                     email: function () {
                                        return $("#email_id").val();
                                    },
                                    userid: function () {
                                        return $("#txtuserid").val();
                                    },
                                }
                            }
                        },*/
                work_id:"required", 
                location: "required",    
                specialties: "required",            
                profilePhoto: {
                    extension: 'gif|png|jpg|jpeg',
                },     
            },
            messages: {
                f_name: "Enter first name",
                l_name:"Enter last name",
                f_name_en: "Enter first name",
                l_name_en:"Enter last name",
                  address:"Enter address",
                address_en:"Enter address",
               /* email_id: {
                    
                     email_id: "Enter valid email id",
                  //  remote: "Email id already exists",
                },*/
                work_id: "Select work",
                location: "Select location",   
                specialties: "Select specialties",
                profilePhoto: {
                    extension: "Upload valid file",
                }
            }
        });
    });
</script>
