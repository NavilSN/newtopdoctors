<?php

$this->load->model('admin/Location_master_model');
$this->load->model('admin/Specialty_master_model');
$this->load->model('admin/Work_master_model');
$location=$this->Location_master_model->get_all_location_by_name();
$specialties=$this->Specialty_master_model->get_all_speciality();
$work=$this->Work_master_model->get_all_work(); 
?>
<div class="row">                      
    <div class="col-lg-12">
        <div class="panel-default">
            <div class="panel-body"> 
                <div class="">
                    <span><h3>Arabic</h3></span>
                </div>
                
                <?php echo form_open(base_url() . 'admin/doctor/create', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'frmstudent', 'target' => '_top', "enctype" => "multipart/form-data")); ?>
                <div class="padded">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("First Name"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="f_name" id="f_name" />
                        </div>
                    </div>												
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Last Name"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="l_name" id="l_name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Address"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="address" id="address" ></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Major"); ?></label>
                        <div class="col-sm-3">
                            <select name="major" id="major" class="form-control" >
                                <option value="">Select</option>
                                <?php foreach ($specialties as $row) { ?>
                                    <option value="<?php echo $row->specialties_id; ?>"  ><?php echo $row->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Biography"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="biography" id="biography" ></textarea>
                        </div>
                    </div>
                    <div class="">
                        <span><h3>English</h3></span>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("First Name"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="f_name_en" id="f_name_en" />
                        </div>
                    </div>                                              
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Last Name"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="l_name_en" id="l_name_en" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Address"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="address_en" id="address_en" ></textarea>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" name="latitude" id="latitude" value=""/>
                    <input type="hidden" class="form-control" name="longtitude" id="longtitude" value=""/>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Major"); ?></label>
                        <div class="col-sm-3">
                            <select name="major_en" id="major_en" class="form-control"  >
                                <option value="">Select</option>
                                <?php foreach ($specialties as $row) { ?>
                                    <option value="<?php echo $row->specialties_id; ?>"><?php echo $row->name_en; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Biography"); ?></label>
                        <div class="col-sm-3">
                            <textarea class="form-control" name="biography_en" id="biography_en" ></textarea>
                        </div>
                    </div>
                    <div class="">
                        <span><h3>Other Information</h3></span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Email Id"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="email_id" id="email_id"  />
                            <span id="emailerror" style="color: red"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work"); ?></label>
                        <div class="col-sm-3">
                            <select name="work_id[]" class="form-control " id="work_id" multiple>
                                <option value="">Select</option>
                                <?php foreach ($work as $row) { ?>
                                    <option value="<?php echo $row->work_id; ?>"><?php echo $row->name_en.' '.$row->work_type; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Location"); ?></label>
                        <div class="col-sm-3">
                           
                            <?php echo getChildsAdmin(); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Specialties"); ?></label>
                        <div class="col-sm-3">
                            <select name="specialties" class="form-control" id="specialties" data-live-search="true" >
                                <option value="">Select</option>
                                <?php foreach ($specialties as $row) { ?>
                                    <option value="<?php echo $row->specialties_id; ?>" style="background-image:url('<?php echo base_url(); ?>images/icon/speciality/<?php echo $row->specialties_id.".png"; ?>'); background-repeat: no-repeat; "><?php echo $row->name_en; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work Hours"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="work_hours" id="work_hours" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Work Hours (ENG)"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control"  name="work_hours_en" id="work_hours_en" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Phone No"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="enter semicolon separated phone "  maxlength="100" name="phoneno" id="phoneno" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Mobile No"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control"  placeholder="enter semicolon separated mobile " maxlength="100" name="mobileno" id="mobileno" />
                        </div>
                    </div>
                     
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Latitude"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="google_map_latitude" id="google_map_latitude" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Longitude"); ?></label>
                        <div class="col-sm-3">
                            <input type="text"  class="form-control" name="google_map_longtude" id="google_map_longtude" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Google map Zoom"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" maxlength="2" minlength="1" class="form-control" name="google_map_zoom" id="google_map_zoom" value=""/>
                        </div>
                    </div>
                        <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Is Featured"); ?></label>
                        <div class="col-sm-3">
                            <input type="checkbox" name="featured" value="1" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Gender"); ?></label>
                        <div class="col-sm-3">
                            <input type="radio" name="gen" value="1" checked>Male
                            <input type="radio" name="gen" value="2" >Female
                        </div>
                        <div class="col-sm-offset-4 col-sm-5">
                            <label for="gen" class="error"></label></div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Profile Photo"); ?></label>
                        <div class="col-sm-3">
                            <input type="file" class="form-control" name="profilePhoto" id="profilePhoto" />
                            <span id="imgerror" style="color:red;"></span>
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Status"); ?></label>
                        <div class="col-sm-3">
                            <select name="status" class="form-control" id="visibility">
                                    <option value="1">Visible</option>
                                    <option value="0">Hidden</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-3">
                            <button type="submit" class="btn btn-info vd_bg-green"><?php echo ucwords("Add");?></button>
                        </div>
                    </div>            
                </div>   
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
/*$("form").submit(function() {
            $.ajax({
            type: 'POST', 
            url: "<?php echo base_url();?>admin/doctor/latlong/",
            data: { 'address': $('#address_en').val()},
            success: function(response){
              var data = JSON.parse(response);
               $("#latitude").append().val(data.lat);
               $("#longtitude").append().val(data.lng);
            }
            });
        });*/
    $(document).ready(function () {
        jQuery.validator.addMethod("mobile_no", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid contact no.');
        jQuery.validator.addMethod("email_id", function (value, element) {
            return this.optional(element) || /^(\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}\s*?;?\s*?)+$/.test(value);
        }, 'Please enter a valid email address.');

        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');

        $("#frmstudent").validate({
            rules: {
                f_name:"required",
                l_name:"required",
                f_name_en:"required",
                l_name_en:"required",
                address:"required",
                address_en:"required",
                /*email_id:{  
                             email_id: true,
                            remote: {
                                url: "<?php echo base_url(); ?>admin/doctor/check_user_email",
                                type: "post",
                                data: {
                                     email: function () {
                                        return $("#email_id").val();
                                    },
                                }
                            }
                        },*/

                work_id:"required", 
                location: "required",       
                specialties: "required",         
                profilePhoto: {
                    extension: 'gif|png|jpg|jpeg',
                },    
            },
            messages: {
                f_name: "Enter first name",
                l_name:"Enter last name",
                f_name_en: "Enter first name",
                l_name_en:"Enter last name",
                 address:"Enter address",
                address_en:"Enter address",
                /*email_id: {
                   
                    email_id: "Enter valid email id",
                   // remote: "Email id already exists",
                },*/

                work_id: "Select work",
                location: "Select location", 
                 specialties: "Select specialties",               
                profilePhoto: {
                    extension: "Upload valid file",
                }
            }
        });
    });
</script>


<script type="text/javascript">
$(document).ready(function() {
    var country = ["Australia", "Bangladesh", "Denmark", "Hong Kong", "Indonesia", "Netherlands", "New Zealand", "South Africa"];
    $("#country").select2({
        data: country
    });
});
</script>
