<?php
$this->load->model('admin/Specialty_master_model');

//$row=$this->Specialty_master_model->get($param2);
?>
    <div class=row>                      
        <div class="col-lg-12">
            <!-- col-lg-12 start here -->
            <div class="panel-default toggle panelMove panelClose panelRefresh">
                <div class="panel-body">
                    <div class="tab-pane box" id="add" style="padding: 5px">
                        <div class="box-content">  
                           
                            <form name="frmstudentedit" id="frmstudentedit" method="post" action="<?= base_url() ?>admin/speciality/update/<?php echo $row->specialties_id; ?>" enctype="multipart/form-data" class="form-horizontal form-groups-bordered validate"> 
                                <input type="hidden" name="txtuserid" id="txtuserid" value="<?php echo $row->specialties_id;?>">
                                <input type="hidden" name="studentid" id="studentid" value="<?php echo $row->specialties_id;?>">
                                
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Name"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="speciality_name" id="speciality_name" value="<?php echo $row->name; ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Name in English"); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="name_en" id="name_en" value="<?php echo $row->name_en; ?>" />
                        </div>
                    </div>                                              
                   
                   <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucwords("Status"); ?></label>
                        <div class="col-sm-3">
                            <select name="status" class="form-control" id="status">
                                <option value="1" <?php if($row->status==1){echo "selected";} ?>>Active</option>
                                <option value="0" <?php if($row->status==0){echo "selected";} ?>>Inactive</option>
                            </select>
                        </div>
                    </div>            
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-3">
                            <button type="submit" class="btn btn-info vd_bg-green"><?php echo ucwords("Add");?></button>
                        </div>
                    </div>            
                </div>   
                </form>
                </div>
            </div>
            </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#birthdate1").datepicker({
        });
        $("#basic-datepicker").datepicker({
            endDate: new Date(),
            format: "MM d, yyyy",
            autoclose: true});

        jQuery.validator.addMethod("mobile_no", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid contact no.');
        jQuery.validator.addMethod("email_id", function (value, element) {
            return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
        }, 'Please enter a valid email address.');

        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');

        $("#frmstudentedit").validate({
            rules: {
                speciality_name:"required",
                name_en:"required",
                status: "required",
            },
            messages: {
                speciality_name: "Enter Name",
                name_en: "Enter Name in English",
                status: "Enter status",
            }
        });
    });
</script>
