<!-- Start .row -->
<!-- cdn for modernizr, if you haven't included it already -->
<!-- <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script> -->
<!-- polyfiller file to detect and load polyfills -->
<!-- <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script> -->
<script>
  /*webshims.setOptions('waitReady', false);
  webshims.setOptions('forms-ext', {types: 'date'});
  webshims.polyfill('forms forms-ext');*/
</script>
<div class="row">                      
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body">
                 <div class="clearfix"> 
                <div class="row">
                    <div class="col-sm-3">
                        <a class="links" href="<?php echo base_url(); ?>admin/speciality/create" target="_blank"><i class="fa fa-plus"></i> Create </a>
                    </div>
                        <form action="<?php echo base_url(); ?>admin/speciality/import" method="post" name="importCSV" id="importCSV" enctype="multipart/form-data">

                    <div class="col-sm-3 text-right">
                             <div class="control-group">
                                <div>
                                    <input type="file" name="importData" id="importData">
                                </div>
                            </div>
                    </div>
                    <div class="col-sm-3 text-left">
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Import Data</button>
                                </div>
                            </div>
                    </div>
                        </form>
                    <div class="col-sm-3  text-right">
                         <form action="<?php echo base_url(); ?>admin/speciality/export" method="post" name="export_excel">
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Export Data</button>
                                </div>
                            </div>
                        </form>                 
                    </div>
                </div>
            </div>      
            <div class="clearfix"></div>
            <div class="border-bottom"></div>
            <div class="table-responsive" >
              <form id="form-filter" name="bulk_action_form" action="<?php echo base_url(); ?>admin/speciality/deleteAll" method="post" onsubmit="return validate();"/>
                <div class="loadingtopdoctor" >Loading&#8230;</div>
                 <table id="speciality_listt" class="table table-striped table-bordered table-responsive example datatableList display nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th><div class="delete-btn"><input type="submit" class="btn btn-danger deleteLink delete-icon-style" name="bulk_delete_submit" value="Delete"/></div></th>
                                <th>In Arabic</th>
                                <th>In Engish</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                            <td>
                                 <div class="w98p">
                                <input type="text" name="fromId" id="fromId" class="form-control" placeholder="From"></div>
                                 <div class="w98p">
                               <input type="text" name="fromTo" id="fromTo" class="form-control" placeholder="To"></div>
                            </td>
                            <td><input name="checkbox" type="checkbox" class="check" id="checkAll"/></td>
                            <td><div class="col-sm-4">
                                <div class="w98p">
                                <input type="text" placeholder="Name (Arabic)" class="form-control" id="FirstNamear">                
                            </div>
                            </div></td>
                            <td><div class="col-sm-4">
                               <div class="w98p">
                                <input type="text" placeholder="Name (English)" class="form-control" id="LastNameen">                
                                </div>
                            </div></td>
                            
                            <td></td>
                            <td><button type="button" id="btn-filter" class="btn btn-primary">Filter</button></td> 
                        
                        </tr>
                        </thead>
                       
                    </table>
                  </form>
                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End contentwrapper -->
</div>
<!-- End #content -->
<script type="text/javascript">
$(document).ajaxStart(function () {
    $('.loadingtopdoctor').show();
});
$(document).ajaxComplete(function () {
    $('.loadingtopdoctor').hide();
});

$(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '0' : '1';
      var msg = (status=='0')? 'Deactivate' : 'Activate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this);
        url = "<?php echo base_url(); ?>admin/speciality/update_status/";
        $.ajax({
          type:"POST",
          url: url,
          data: {id:$(current_element).attr('data'),status:status},
          success: function(data)
          {   
            location.reload();
          }
        });
      }      
    });

$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
})

function validate(){
    //alert("test"); return false;
    var chks = document.getElementsByName('checkbox[]');
    var hasChecked = false;
    for (var j = 0; j < chks.length; j++){
        if (chks[j].checked){
            var result = confirm("Are you sure to remove this information ?");
            if (result) {
                hasChecked = true;
                break;
            }if (hasChecked == false){
                return false;
            }
        }
    }if (hasChecked == false){
       // alert("Please select at least one.");
        return false;
    }return true;

}

$(document).ready(function (){
    $("#importCSV").validate({
        rules: {
            importData: {required: true,
                    extension: 'csv',
                },
        },messages: {
            importData: {
                    required: "Please choose file",
                    extension: "Please upload only csv file",
                }
        }
    });
});
</script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script> -->
<!-- <script src="https://code.jquery.com/jquery-1.12.2.min.js" integrity="sha256-lZFHibXzMHo3GGeehn1hudTAP3Sc0uKXBXAzHX1sjtk=" crossorigin="anonymous"></script> -->
<!-- <script src="http://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>/assets/js/dataTables.cellEdit.js"></script> -->
<script src="<?php echo base_url(); ?>js/1.10.9/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>js/1.10.9/input.js"></script>
<script type="text/javascript">

$(document).ready(function() {

var table = $('#speciality_listt').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url()?>admin/speciality/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                data.from = $("#fromId").val();
                data.to = $("#fromTo").val();
                data.nameAR = $('#FirstNamear').val();
                data.nameEN = $('#LastNameen').val();
            }            
        },
        initComplete: function() {
            $('#speciality_listt input[type="text"]').unbind();
            $('#speciality_listt input[type="text"]').bind('keyup', function(e) {
                if(e.keyCode == 13) {
                      table.page( 'first' ).draw( 'page' );
                     table.ajax.reload(null,false);
                }
            });
        },
         "aoColumnDefs" : [ {
            'bSortable' : false,
            'aTargets' : [ 1,5 ]
        } ],
        "searching":false,
        "dom": '<"top"i>fp<"bottom"lrt><"clear">',        
        "paging": true,
        "pagingType": "input",
        "order": [[ 0, "desc" ]]
    });

    $('#btn-filter').click(function(){ //button filter event click
     // table.page( 'next' ).draw( 'page' );
     table.page( 'first' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });


   

   /* $(".datatableList tbody tr").live( 'click',function() { 
           // console.log(($(this).val()));    
        //window.open('http://www.example.com');
    } );
*/
    /*$('.datatableList tbody').on( 'click', 'tr', function () {

       // console.log($(this).find('td').text());return false;

        var data = table.cell(this).data();   //td element data 
        console.log(data);return false; 
        var column = table.cell( this ).index().column; //column index
        var row = table.row( this ).index();  // Row index
        var  rowid = $(this).closest('tr').attr('id'); //Get Row Id
        var columnvisible = table.cell( this ).index().columnVisible //visble column index
    } );*/

    /*table.MakeCellsEditable({
        "onUpdate": myCallbackFunction
    });


    function myCallbackFunction(updatedCell, updatedRow, oldValue) {
        //alert(updatedCell.data());return false;
        console.log("The new value for the cell is: " + updatedCell.data());
        console.log("The old value for that cell was: " + oldValue);
        console.log("The values for each cell in that row are: " + updatedRow.data());
    }*/

});
</script>