<!-- Start .row -->
<div class="row">                      
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body">
                <a class="links" href="<?php echo base_url(); ?>admin/location/create" target="_blank"><i class="fa fa-plus"></i> Create </a>
                <div class="table-responsive" >
                    <form name="bulk_action_form" action="<?php echo base_url(); ?>admin/location/deleteAll" method="post" onsubmit="return validate();"/>
                    <table class="table table-striped table-bordered table-responsive example datatableList display nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><input name="checkbox" type="checkbox" class="check" id="checkAll"/>
                                    <div class="delete-btn"><input type="submit" class="btn btn-danger deleteLink delete-icon-style" name="deleteAll" value="Delete"/>
                                <!-- <select name="statusAll" id="statusAll">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select> -->
                                    </div></th>
                                 <th>Location Id</th>
				<th>Name</th>
                                <th>Name En</th>
                                <th>Parent Id</th>                                
                                <th>Created</th>
                                <th>Updated</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($location as $row) { ?>
                                <tr>
                                    <td><input type="checkbox" name="checkbox[]" id="checkbox[]" value="<?php echo $row->location_id; ?>" class="checkbox"/>
                                        <td><?php echo $row->location_id; ?></td>
				 <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->name_en; ?></td>
                                    <td><?php echo $row->parent_id; ?></td>                                   
                                    <td><?php echo $row->created_date; ?></td>
                                    <td><?php echo $row->modified_date; ?></td>
                                    <td><i data="<?php echo $row->location_id; ?>" class="status_checks btn
                                           <?php echo ($row->status) ? 'btn-success' : 'btn-danger' ?>"><?php echo ($row->status) ? 'Active' : 'Inactive' ?></i></td>
                                    <td class="menu-action">
                                        <a href="<?php echo base_url(); ?>admin/location/edit/<?php echo $row->location_id; ?>" target="_blank"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a>
                                        <a data-placement="top" data-toggle="tooltip" onclick="confirm_modal('<?php echo base_url(); ?>admin/location/delete/<?php echo $row->location_id; ?>');" href="#" data-original-title="" title=""><span class="label label-danger mr6 mb6"><i aria-hidden="true" class="fa fa-trash-o"></i>Delete</span></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End contentwrapper -->
</div>
<!-- End #content -->
<script type="text/javascript">
    $(document).on('click', '.status_checks', function () {
        var status = ($(this).hasClass("btn-success")) ? '0' : '1';
        var msg = (status == '0') ? 'Deactivate' : 'Activate';
        if (confirm("Are you sure to " + msg)) {
            var current_element = $(this);
            url = "<?php echo base_url(); ?>admin/location/update_status/";
            $.ajax({
                type: "POST",
                url: url,
                data: {id: $(current_element).attr('data'), status: status},
                success: function (data)
                {
                    location.reload();
                }
            });
        }
    });

    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    })
    function validate() {
        var chks = document.getElementsByName('checkbox[]');
        var hasChecked = false;
        for (var j = 0; j < chks.length; j++) {
            if (chks[j].checked) {
                var result = confirm("Are you sure to remove this information ?");
                if (result) {
                    hasChecked = true;
                    break;
                }
                if (hasChecked == false) {
                    return false;
                }
            }
        }
        if (hasChecked == false) {
            alert("Please select at least one.");
            return false;
        }
        return true;

    }

    $("#statusAll").change(function () {
        var selectedValue = this.value;
        var chks = document.getElementsByName('checkbox[]');
        var hasChecked = false;
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked) {
                var dataRow = [chks[i].value];
                var result = confirm("Are you sure to change this information ?");
                if (result) {
                    $.ajax({
                        url: '<?php echo base_url(); ?>admin/location/statusAll',
                        type: 'POST',
                        data: {option: selectedValue, checkbox: dataRow},
                        dataType: "json",
                        success: function () {
                            console.log("Data sent!");
                        }
                    });
                }
            }
            /*if(hasChecked == false){
             return false;
             }*/

        }
        if (hasChecked == false) {
            alert("Please select at least one.");
            return false;
        }
        return true;
    });
</script>
<script type="text/javascript">

    $(document).ready(function () {

        var table = $('.datatableList').DataTable({
            //"processing": true, 
            // "serverSide": true,
            "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [0]
                }],
            "searching": true,
            "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        });

        /* $('#btn-filter').click(function(){ //button filter event click
         table.ajax.reload(null,false);  //just reload table
         });
         $('#btn-reset').click(function(){ //button reset event click
         $('#form-filter')[0].reset();
         table.ajax.reload(null,false);  //just reload table
         });*/


    });
</script>
<script src="<?php echo base_url(); ?>assets/js/plugins/tables-data.js"></script>
