<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Location_master_model');
        $this->load->helper('language'); 
        $this->load->helper('text');
	date_default_timezone_set('Africa/Cairo');
         if ($this->session->userdata('admin_login') != 1){
            redirect(base_url().'admin/login');
        } 
    }
    function index() {
        $this->data['title']  = "Location"; 
        $this->data['page']   = "location";
        $this->data['location'] = $this->Location_master_model->get_order_by_parent_id();
        $this->__template('admin/location/index', $this->data);
    }
  
    function create() {
        if ($_POST) {
            if($_POST['parent']=="")
            {
                $parent = '0';
            }
            else{
                $parent = $_POST['parent'];
            }
                $this->Location_master_model->insert(array(
                'name'      => trim($_POST['location_name']),
                'name_en'   => trim($_POST['name_en']),
                'status'    => $_POST['status'],
                'parent_id' =>$parent
            ));
                
            $this->flash_notification('Location is successfully inserted.');
            redirect(base_url('admin/location'));
        } 
        $this->data['location'] = $this->Location_master_model->get_parent_location();
        $this->__template('admin/location/create',$this->data);       
    }

    function update($id) {
          if($_POST) {
             if($_POST['parent']=="" || $_POST['parent']=="0")
            {
                $parent = '0';
            }
            else{
                $parent = $_POST['parent'];
            }
                $Location = $this->Location_master_model->update($this->input->post('studentid'),array(
                'name'      => trim($_POST['location_name']),
                'name_en'   => trim($_POST['name_en']),
                'status'    => $_POST['status'],
                'parent_id'    => $parent
                ));
            $this->flash_notification('Location is successfully updated.');
        }
        redirect(base_url('admin/location'));
    }

    function delete($id) {
        $this->Location_master_model->delete($id);
        $this->flash_notification("Location successfully deleted");
        redirect(base_url() . 'admin/location/');
    }
    
    function update_status(){
        $status=$_POST['status']; 
        $id=$_POST['id'];

        $this->Location_master_model->update($id,array(
            'status'        => $status,
        ));   
    }

    function deleteAll(){
        foreach ($_POST['checkbox'] as $key => $value) {
            $this->Location_master_model->delete($value);
        }
        $this->flash_notification("Location successfully deleted");
        redirect(base_url('admin/location'));
    }

    function statusAll(){
        foreach ($_POST['checkbox'] as $key => $value) {
            if($_POST['option']=='1'){
                $this->Location_master_model->update($value,array('status'=>'1'));
            }else{
                $this->Location_master_model->update($value,array('status'=>'0'));
            }
                
        }
        //$this->flash_notification("Location status changed successfully");
        // redirect(base_url('admin/location'));
    }
     function edit($id) {
        $this->data['title'] = "Location";
        $this->data['page'] = "location";  
        
        $this->data['location'] = $this->Location_master_model->get_parent_location();
        $this->data['row']=$this->Location_master_model->get($id); 
       
        $this->__template('admin/location/edit', $this->data);
    }
}
