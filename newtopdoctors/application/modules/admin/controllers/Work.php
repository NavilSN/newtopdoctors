<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Work extends MY_Controller
{

    /**
     * Construct function of work module
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Work_master_model');
        $this->load->model('admin/Doctor_master_en_model');
        $this->load->model('admin/Location_master_model');
        $this->load->model('admin/Getallwork_model');
        $this->load->helper('language');
        $this->load->helper('text');
        date_default_timezone_set('Africa/Cairo');
        if ($this->session->userdata('admin_login') != 1) {
            redirect(base_url() . 'admin/login');
        }
    }

    /**
     * Display work list
     */
    public function index()
    {
        $this->data['title'] = "Work";
        $this->data['page'] = "work";
        $this->data['work'] = $this->Work_master_model->get_all();
        $this->__template('admin/work/index', $this->data);
    }

    /**
     * Redirect on Create form page
     */
    public function create()
    {

        if ($_POST) {
            $name_ar = $_POST['fname_ar'] . ' ' . $_POST['lname_ar'];
            $name_en = $_POST['fname_en'] . ' ' . $_POST['lname_en'];
            $work_id = $this->Work_master_model->insert(array(
                'name' => trim($name_ar),
                'name_en' => trim($name_en),
                'address' => trim($_POST['address']),
                'address_en' => trim($_POST['address_en']),
                'phone' => trim($_POST['phone']),
                'email' => trim($_POST['email']),
                'location_id' => trim($_POST['location']),
                'work_type' => trim($_POST['work_type']),
                'status' => trim($_POST['status']),
                'work_biography' => trim($_POST['biography']),
                'work_biography_en' => trim($_POST['biography_en']),
                'work_hours' => trim($_POST['work_hours']),
                'work_hours_en' => trim($_POST['work_hours_en']),
                'mobileno' => trim($_POST['mobileno']),
                'work_latitude' => trim($_POST['google_map_latitude']),
                'work_longitude' => trim($_POST['google_map_longtude']),
                'work_zoom' => trim($_POST['google_map_zoom']),
                'firstname_en' => trim($_POST['fname_en']),
                'lastname_en' => trim($_POST['lname_en']),
                'firstname_ar' => trim($_POST['fname_ar']),
                'lastname_ar' => trim($_POST['lname_ar']),
            ));
            if(!empty($_FILES['profilePhoto']['name'])) {

                $file = explode(".", $_FILES['profilePhoto']['name']);
                $ext = strtolower(end($file));
                $newfile = date('dmYhis').'.'.$ext;
                move_uploaded_file($_FILES['profilePhoto']['tmp_name'], FCPATH.'uploads/workimage/'.$newfile);

                $this->Work_master_model->update( $work_id,array('photo'=>$newfile));
            }
            $this->flash_notification('Work is successfully inserted.');
            redirect(base_url('admin/work'));
        }
        $this->__template('admin/work/create');
    }

    /**
     * Call edit form for work
     * @param int $id
     */
    public function edit($id)
    {
        $this->data['title'] = "Work";
        $this->data['page'] = "work";
        $this->data['row'] = $this->Work_master_model->get($id);
        $this->data['doctorWork'] = $this->Work_master_model->get_all_doctors_data($id);

        $this->__template('admin/work/edit', $this->data);
    }

    /**
     * UPdate Work data
     * @param int $id
     */
    public function update($id)
    {

        if ($_POST) {
            $name_ar = $_POST['fname_ar'] . ' ' . $_POST['lname_ar'];
            $name_en = $_POST['fname_en'] . ' ' . $_POST['lname_en'];
             $this->Work_master_model->update($this->input->post('work_id'), array(
                'name' => trim($name_ar),
                'name_en' => trim($name_en),
                'address' => trim($_POST['address']),
                'email' => trim($_POST['email_id']),
                'address' => trim($_POST['address']),
                'address_en' => trim($_POST['address_en']),
                'phone' => trim($_POST['phone']),
                'email' => trim($_POST['email']),
                'location_id' => trim($_POST['location']),
                'work_type' => trim($_POST['work_type']),
                'status' => trim($_POST['status']),
                'work_biography' => trim($_POST['biography']),
                'work_biography_en' => trim($_POST['biography_en']),
                'work_hours' => trim($_POST['work_hours']),
                'work_hours_en' => trim($_POST['work_hours_en']),
                'mobileno' => trim($_POST['mobileno']),
                'work_latitude' => trim($_POST['google_map_latitude']),
                'work_longitude' => trim($_POST['google_map_longtude']),
                'work_zoom' => trim($_POST['google_map_zoom']),
                'firstname_en' => trim($_POST['fname_en']),
                'lastname_en' => trim($_POST['lname_en']),
                'firstname_ar' => trim($_POST['fname_ar']),
                'lastname_ar' => trim($_POST['lname_ar']),
            ));
            if(!empty($_FILES['profilePhoto']['name'])) {
              $existingfile = $_POST['existingfile'];
               unlink(FCPATH.'uploads/workimage/'.$existingfile); // delete existing image

                $file = explode(".", $_FILES['profilePhoto']['name']);
                $ext = strtolower(end($file));
                $newfile = date('dmYhis').'.'.$ext;
                move_uploaded_file($_FILES['profilePhoto']['tmp_name'], FCPATH.'uploads/workimage/'.$newfile);
                $this->Work_master_model->update($id,array('photo'=>$newfile));


            }

            $this->flash_notification('Work is successfully updated.');
        }
        redirect(base_url('admin/work'));
    }

    /**
     * Delete work
     * @param int $id
     */
    public function delete($id)
    {
        $row = $this->Work_master_model->get($id);
        $photo = $row->photo;
        @unlink(FCPATH.'/uploads/workimage/'.$photo);
        $this->Work_master_model->delete($id);
        $this->flash_notification("Work successfully deleted");
        redirect(base_url() . 'admin/work');
    }

    /**
     * Check Duplicate email
     * @param string $param
     */
    public function check_user_email($param = '')
    {

        $email = $this->input->post('email');
        if ($param == '') {
            $data = $this->Work_master_model->get_by(array(
                'email' => $email,
            ));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        } else {

            $data = $this->Work_master_model->get_by(array(
                'work_id !=' => $this->input->post('userid'),
                'email' => $email,
            ));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    /**
     * Update Work Status active inactive
     */
    public function update_status()
    {
        $status = $_POST['status'];
        $id = $_POST['id'];

        $student_id = $this->Work_master_model->update($id, array(
            'status' => $status,
        ));
    }

    /**
     * Delete Multiple  Data of work
     */
    public function deleteAll()
    {
        foreach ($_POST['checkbox'] as $key => $value) {
            $this->Work_master_model->delete($value);
        }
        $this->flash_notification("Work successfully deleted");
        redirect(base_url('admin/work'));
    }

    /**
     * Export in csv Work Data
     */
    public function export()
    {
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";

        $this->load->library('Excel');

        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Users list');

        $this->excel->getActiveSheet()->SetCellValue('A1', 'FNameAR');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'LNameAR');
        $this->excel->getActiveSheet()->SetCellValue('C1', 'AddressAR');
        $this->excel->getActiveSheet()->SetCellValue('D1', 'FNameEN');
        $this->excel->getActiveSheet()->SetCellValue('E1', 'LNameEN');
        $this->excel->getActiveSheet()->SetCellValue('F1', 'AddressEN');
        $this->excel->getActiveSheet()->SetCellValue('G1', 'Email');
        $this->excel->getActiveSheet()->SetCellValue('H1', 'Location');
        $this->excel->getActiveSheet()->SetCellValue('I1', 'Speciality');
        $this->excel->getActiveSheet()->SetCellValue('J1', 'Status');
        $this->excel->getActiveSheet()->SetCellValue('K1', 'Phone');
        $this->excel->getActiveSheet()->SetCellValue('L1', 'Mobile');
        $this->excel->getActiveSheet()->SetCellValue('M1', 'latitude');
        $this->excel->getActiveSheet()->SetCellValue('N1', 'longitude');
        $this->excel->getActiveSheet()->SetCellValue('O1', 'Zoom');
        $this->excel->getActiveSheet()->SetCellValue('P1', 'BiographyEN');
        $this->excel->getActiveSheet()->SetCellValue('Q1', 'BiographyAR');
        $this->excel->getActiveSheet()->SetCellValue('R1', 'WorkingHours');
        $this->excel->getActiveSheet()->SetCellValue('S1', 'WorkingHoursEN');
        $this->excel->getActiveSheet()->SetCellValue('T1', 'WorkName');
        $this->excel->getActiveSheet()->SetCellValue('U1', 'Gender');
        $this->excel->getActiveSheet()->SetCellValue('V1', 'MajorEN');
        $this->excel->getActiveSheet()->SetCellValue('W1', 'MajorAR');
        $this->excel->getActiveSheet()->SetCellValue('X1', 'WorkType');

        $work_data = $this->Work_master_model->getExportData();

        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($work_data, null, 'A2');

        $filename = 'ListOFwork.xls'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        $objWriter->save('php://output');
    }

    /**
     * Import Work Data
     * Getting all work data and insert into database
     * we are using utf8 function for
     */
    public function import()
    {
        $file = $_FILES['importData']['tmp_name'];
        $file_upload = $_FILES['importData']['name'];
        $exp = explode(".", $file_upload);
        $filext = strtolower(end($exp));
        $extension = array('xls', 'xlsx');
        if (!in_array($filext, $extension)) {
            $this->session->set_flashdata("error", "Invalid file"); // invalid uploaded file format
            redirect(base_url('admin/work'));
        }

        //load the excel library
        $this->load->library('excel');
        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        //get only the Cell Collection
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            //header will/should be in row 1 only. of course this can be modified to suit your need.
            if ($row == 1) {
                $header[$row][$column] = $data_value;
            } else {

                $arr_data[$row][$column] = $data_value;

            }

        }

//send the data in an array format
        $data['header'] = $header;
        $data['values'] = $arr_data;
        $insert_csv = array();
        if ($data['header'][1]['A'] === "FNameAR" && $data['header'][1]['B'] === "LNameAR" && $data['header'][1]['C'] === "AddressAR" && $data['header'][1]['D'] === "FNameEN" && $data['header'][1]['E'] === "LNameEN" && $data['header'][1]['F'] === "AddressEN" && $data['header'][1]['G'] === "Email" && $data['header'][1]['H'] === "Location" &&
            $data['header'][1]['I'] === "Speciality" && $data['header'][1]['J'] === "Status" && $data['header'][1]['K'] === "Phone" && $data['header'][1]['L'] === "Mobile" && $data['header'][1]['M'] === "latitude" && $data['header'][1]['N'] === "longitude" && $data['header'][1]['O'] === "Zoom" && $data['header'][1]['P'] === "BiographyEN" && $data['header'][1]['Q'] === "BiographyAR" && $data['header'][1]['R'] === "WorkingHours" && $data['header'][1]['S'] === "WorkingHoursEN" && $data['header'][1]['T'] === "WorkName" && $data['header'][1]['U'] === "Gender" && $data['header'][1]['V'] === "MajorEN" && $data['header'][1]['W'] === "MajorAR" && $data['header'][1]['X'] === "WorkType") // check header column index
        {
            foreach ($arr_data as $row) {

                /* Set xls row to $insert_csv array for insert */
                $insert_csv['FNameAR'] = $row['A'];
                $insert_csv['LNameAR'] = $row['B'];
                $insert_csv['AddressAR'] = $row['C'];
                $insert_csv['FNameEN'] = $row['D'];
                $insert_csv['LNameEN'] = $row['E'];
                $insert_csv['AddressEN'] = $row['F'];
                $insert_csv['Email'] = $row['G'];
                $insert_csv['Location'] = $row['H'];
                $insert_csv['Speciality'] = $row['I'];
                $insert_csv['Status'] = $row['J'];
                $insert_csv['Phone'] = $row['K'];
                $insert_csv['Mobile'] = $row['L'];
                $insert_csv['latitude'] = $row['M'];
                $insert_csv['longitude'] = $row['N'];
                $insert_csv['Zoom'] = $row['O'];
                $insert_csv['BiographyEN'] = $row['P'];
                $insert_csv['BiographyAR'] = $row['Q'];
                $insert_csv['WorkingHours'] = $row['R'];
                $insert_csv['WorkingHoursEN'] = $row['S'];
                $insert_csv['WorkName'] = $row['T'];
                $insert_csv['Gender'] = $row['U'];
                $insert_csv['MajorEN'] = $row['V'];
                $insert_csv['MajorAR'] = $row['W'];
                $insert_csv['WorkType'] = $row['X'];

                /* Set The validate content */
                $insert_csv['FNameEN'] = $this->validateInput($insert_csv['FNameEN']);
                $insert_csv['LNameEN'] = $this->validateInput($insert_csv['LNameEN']);
                $insert_csv['FNameAR'] = $this->validateInput($insert_csv['FNameAR']);
                $insert_csv['LNameAR'] = $this->validateInput($insert_csv['LNameAR']);
                $insert_csv['AddressAR'] = $this->validateInput($insert_csv['AddressAR']);
                $insert_csv['AddressEN'] = $this->validateInput($insert_csv['AddressEN']);
                $insert_csv['Email'] = $this->validateInput($insert_csv['Email']);
                $insert_csv['Mobile'] = $this->validateInput($insert_csv['Mobile']);
                $insert_csv['Phone'] = $this->validateInput($insert_csv['Phone']);
                $insert_csv['Location'] = $this->validateInput($insert_csv['Location']);
                $insert_csv['Speciality'] = $this->validateInput($insert_csv['Speciality']);
                $insert_csv['Status'] = $this->validateInput($insert_csv['Status']);
                $insert_csv['latitude'] = $this->validateInput($insert_csv['latitude']);
                $insert_csv['longitude'] = $this->validateInput($insert_csv['longitude']);
                $insert_csv['Zoom'] = $this->validateInput($insert_csv['Zoom']);
                $insert_csv['BiographyEN'] = $this->validateInput($insert_csv['BiographyEN']);
                $insert_csv['BiographyAR'] = $this->validateInput($insert_csv['BiographyAR']);
                $insert_csv['WorkingHours'] = $this->validateInput($insert_csv['WorkingHours']);
                $insert_csv['WorkingHoursEN'] = $this->validateInput($insert_csv['WorkingHoursEN']);
                $insert_csv['WorkName'] = $this->validateInput($insert_csv['WorkName']);
                $insert_csv['Gender'] = $this->validateInput($insert_csv['Gender']);
                $insert_csv['MajorEN'] = $this->validateInput($insert_csv['MajorEN']);
                $insert_csv['MajorAR'] = $this->validateInput($insert_csv['MajorAR']);
                $insert_csv['WorkType'] = $this->validateInput($insert_csv['WorkType']);

                /* end string validate*/

                $nameAR = $insert_csv['FNameAR'] . ' ' . $insert_csv['LNameAR'];
                $nameEN = $insert_csv['FNameEN'] . ' ' . $insert_csv['LNameEN'];

                if ($insert_csv['Status'] == 'Active') {
                    $status = '1';
                } else {
                    $status = '0';
                }

                $insert_csv['Status'] = $status;

                if ($insert_csv['WorkType'] == "") {
                    $insert_csv['WorkType'] = "Medical Lab";
                }
                $this->db->where('name_en', $insert_csv['Location']);
                $LocationRow = $this->db->get('location_master')->row();
                $data = array(
                    'name' => $nameAR,
                    'name_en' => $nameEN,
                    'address' => $insert_csv['AddressAR'],
                    'address_en' => $insert_csv['AddressEN'],
                    'phone' => $insert_csv['Phone'],
                    'email' => $insert_csv['Email'],
                    'location_id' => $LocationRow->location_id,
                    'work_type' => $insert_csv['WorkType'],
                    'status' => $insert_csv['Status'],
                    'work_biography' => $insert_csv['BiographyAR'],
                    'work_biography_en' => $insert_csv['BiographyEN'],
                    'work_hours' => $insert_csv['WorkingHours'],
                    'work_hours_en' => $insert_csv['WorkingHoursEN'],
                    'mobileno' => $insert_csv['Mobile'],
                    'work_latitude' => $insert_csv['latitude'],
                    'work_longitude' => $insert_csv['longitude'],
                    'work_zoom' => $insert_csv['Zoom'],
                    'firstname_en' => $insert_csv['FNameEN'],
                    'lastname_en' => $insert_csv['LNameEN'],
                    'firstname_ar' => $insert_csv['FNameAR'],
                    'lastname_ar' => $insert_csv['LNameAR'],
                );

                $CheckRow = '';
                $return = "true";
                if (!empty($insert_csv['Email']) || !empty($insert_csv['Phone']) || !empty($insert_csv['Mobile'])) {
                    if (!empty($insert_csv['Email'])) {

                        $this->db->where('email', $insert_csv['Email']);
                    }
                    if (!empty($insert_csv['Phone'])) {
                        $this->db->or_where('phone', $insert_csv['Phone']);
                    }
                    if (!empty($insert_csv['Mobile'])) {
                        $this->db->or_where('mobileno', $insert_csv['Mobile']);
                    }
                    $CheckRow = $this->db->get('work_master')->row();
                }
                 if (!empty($insert_csv['Email'])) {
                        $validate = $this->emailValidate($insert_csv['Email']);

                        if ($validate == "invalid email") {
                            $return = "false";
                        }
                    }
                if ($CheckRow == "") {

                    //echo $insert_csv['Email'];


                    /*  if(empty($insert_csv['FNameEN']) || empty($insert_csv['LNameEN']) || empty($insert_csv['FNameAR']) || empty($insert_csv['LNameAR']) || empty($insert_csv['AddressAR']) || empty($insert_csv['AddressEN'])){
                    $return = "false";

                    }*/
                    if (!empty($insert_csv['FNameEN']) && !empty($insert_csv['LNameEN'])) {

                        if ($this->nameValidate($insert_csv['FNameEN']) == "Invalid" && $this->nameValidate($insert_csv['LNameEN']) == "Invalid") {
                            $return = "false";
                        }

                    }

                    if ($return == "true") {

                        $data['created_date'] = date("Y-m-d H:i:s");
                        $this->db->insert('work_master', $data);
                    } else {

                    }

                } else {
                    if ($return == "true") {

                        $data['modified_date'] = date("Y-m-d H:i:s");
                        $this->db->where('work_id', $CheckRow->work_id);
                        $this->db->update('work_master', $data);
                    }
                }

            }
            $this->flash_notification("Data successfully Import.");
            redirect(base_url('admin/work'));
        } else {

            $this->session->set_flashdata("error", "Columns mismatch. Please download the sample file to make sure of column indexes.");
            redirect(base_url('admin/work'));
        }
    }

    /**
     * Character set check
     * @param String $string
     * @return string
     */
    public function is_utf8($string)
    {
        return (mb_detect_encoding($string, 'UTF-8', true) == 'UTF-8');
    }

    /**
     * Check arabic Character set
     * @param String $utf8
     * @return String
     */
    public function utf8($utf8)
    {
        if (mb_detect_encoding($utf8, 'UTF-8', true) == 'UTF-8') {

            return $utf8;
        } else {

            $utf8 = iconv("windows-1256", "utf-8", $utf8);
            return $utf8;
        }
    }

    /************************* WORK LISTING ************/

    public function ajax_list()
    {

        if ($_POST['location'] != "") {
            $location_list = getChildLocation($_POST['location']);
        } else {
            $location_list = '';
        }
        $list = $this->Getallwork_model->get_datatables($location_list);
        /*echo $this->db->last_query();die;*/
        $data = array();
        // $no = $_POST['start'];
        foreach ($list as $customers) {
            //$no++;
            $urlEdit = base_url() . "admin/work/edit/" . $customers->work_id;
            $btn = ($customers->status) ? 'btn-success' : 'btn-danger';
            $status = ($customers->status) ? 'Active' : 'Inactive';
            $row = array();
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->work_id . '</a>';
            $row[] = '<input type="checkbox" name="checkbox[]" id="checkbox[]" value="' . $customers->work_id . '"/>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->name . '</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->email . '</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->phone . '</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->mobileno . '</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->work_type . '</a>';

            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->loc_name . '</a>';
            // $row[] = $customers->mobile_number;

            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->created_date . '</a>';
            $row[] = $customers->modified_date;
            $row[] = '<i data="' . $customers->work_id . '" class="status_checks btn ' . $btn . '">' . $status . '</i>';

            //$row[] = $customers->created_at;
            //$row[] = $customers->updated_at;
            $urlEdit = base_url() . "admin/work/edit/" . $customers->work_id;
            $urlDelete = base_url() . "admin/work/delete/" . $customers->work_id;
            $row[] = '<a data-placement="top" data-toggle="tooltip" href="' . $urlEdit . '" target="_blank"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a><a data-placement="top" data-toggle="tooltip" onclick="confirm_modal(' . "'" . $urlDelete . "'" . ');" href="#" data-original-title="" title=""><span class="label label-danger mr6 mb6"><i aria-hidden="true" class="fa fa-trash-o"></i>Delete</span></a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Getallwork_model->count_all(),
            "recordsFiltered" => $this->Getallwork_model->count_filtered($location_list),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    /**
    check email is validate or not
    @param $email
    return string
     **/
    public function emailValidate($email)
    {

        $regex = '/^(\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}\s*?;?\s*?)+$/';
        return $email = (preg_match($regex, $email)) ? $email : "invalid email";

    }
    /**
    Validate all input trim the spaces strip slashes and html specialcharacter validate
     **/
    public function validateInput($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        $data = strip_tags($data);
        return $data;
    }

    /**
    First name last name validation alphanumeric
     **/
    public function nameValidate($name)
    {
        return (preg_match('/^[a-z0-9 .\-]+$/i', $name)) ? $name : "Invalid";
    }

    function removeImage($id){
        $this->load->helper("file");

        $image = array('photo' => '');
        if($id!="")
        {
        echo $this->Work_master_model->update($id,$image);
        unlink(FCPATH.'uploads/workimage/'.$_POST['photo']);

        }

    }





    /**************************** END *************************/
}
