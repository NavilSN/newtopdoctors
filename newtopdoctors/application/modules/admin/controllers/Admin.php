<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

    function __construct() {

	parent::__construct();
        $this->load->model('Admin_model');
        $this->load->library('email');
    }

    function index() {

        if ($this->session->userdata('admin_login') != 1){
            redirect(base_url().'admin/login');
        }
        $this->dashboard();
    }

    function login() {
        if ($_POST) {
            $email = trim($this->input->post('email'));
            $password = trim($this->input->post('password'));
            $this->process_login($email, $password);
        }                                           
        $this->data['title'] = 'User Login';
        $this->load->view('admin/user/login', $this->data);
    }

    function profile($id) {
            if($_POST) {
                $admin_id = $this->Admin_model->update($id,array(
                'first_name'    => $_POST['first_name'],
                'last_name'     => $_POST['last_name'],
                'email'         => $_POST['email'],
                'gender'        => $_POST['gender'],
                'mobile'        => $_POST['mobile'],
                'city'          => $_POST['city'],
                'address'       => $_POST['address'],
            ));   
            if($_FILES['photo']['name'] != ''){   
                unlink('uploads/admin_image/'.$id.'.jpg');
                move_uploaded_file($_FILES['photo']['tmp_name'], 'uploads/admin_image/'.$id.'.jpg');
                $this->Admin_model->update($id,array('photo'    => $id.'.jpg',));
            }   
            $this->flash_notification('Profile is successfully updated.');
        }
        redirect(base_url('admin/dashboard'));
    }

    function process_login($email = '', $password = '') {
            
            $get_row=$this->db->get_where('admin',array('email'=>$email,'password'=>md5($password)));
            if($get_row->num_rows()>0)
            {
                $row=$get_row->row();
                $this->session->set_userdata("admin_login", "1");
                $this->session->set_userdata("admin_id", $row->admin_id);
                $this->session->set_userdata("admin_firstname", $row->first_name);
                $this->session->set_userdata("admin_lastname", $row->last_name);

                redirect(base_url('admin/dashboard'));
        } else {
            $this->session->set_flashdata('error', 'Invalid email or password');
            redirect(base_url('admin/login'));
        }
    }

    function __hash($str) {
        return hash('md5', $str . config_item('encryption_key'));
    }

    function dashboard() {

      if ($this->session->userdata('admin_login') != 1){
            redirect(base_url().'admin/login');
        }
        
        $this->data['title'] = 'Dashboard';

        $this->data['page'] = 'dashboard';
        $this->data['todolist'] = array();
        $this->__template('admin/user/dashboard', $this->data);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url('admin/login'));
    }

    function change_password($id) {
        if($_POST) {
                $admin_id = $this->Admin_model->update($id,array(
                'password'      => md5($_POST['cpassword'])));
        }
        $this->flash_notification('Password is successfully change.');
        redirect(base_url('admin/dashboard'));
    }

    function check_password($param = '') {

        $password = $this->input->post('password');
        $data = $this->Admin_model->get_by(array(
            'admin_id =' => $this->input->post('userid'),
            'password' => md5($password)
        ));
        if ($data) {
            echo "true";
        } else {
            echo "false";
        }
    }

    function forgot_password() {
        $this->load->model('admin/Site_model');
        if ($_POST) {
           $record = $this->Site_model->is_user_email_present($_POST['email']);
            if ($record) {
                $user_id = $record->admin_id;
                $random_string = $this->random_string_generate();
               
                $this->update_forgot_password_key('', $user_id, md5($random_string));
                $url = $this->forgot_password_url('', $user_id, $random_string);
              
                $this->email->set_newline("\r\n");
                $this->email->from('tejas.patel@searchnative.in', 'Top doctor');
                $this->email->to($_POST['email']);
                $this->email->subject('Reset your top doctor password');
                $message = "Please find a below password";
                $message .= "<br/>";
                $get_record = $this->Site_model->is_user_email_present($record->email);
                $message .= 'New Password:- '.$random_string;

                $this->email->message($message);
                $this->email->send();
                $this->session->set_flashdata('success', 'Please check email to reset your password.');
                redirect(base_url('admin/login'));
            } else {
                // email is not registered in the system
                $this->session->set_flashdata('email_not_found', 'Email is not registered in the system.');
                redirect(base_url('admin/login'));
            }
        } else {
           redirect(base_url('admin/login'));
        }
    }

    function random_string_generate() {
        $this->load->helper('string');
        return random_string('alnum', 16);
    }
    
    function update_forgot_password_key($user_type, $user_id, $key) {
        if ( $user_id != '' && $key != '') {
            $this->Site_model->update_forgot_password_key($user_type, $user_id, $key);
        } else {
            redirect(base_url('admin/login'));
        }
    }
    
    function forgot_password_url($user_type, $user_id, $random_string) {
        $this->load->library('encrypt');
        $base_url = base_url();
        $user_type = hash('md5', $user_type . config_item('encryption_key'));

        return $base_url . 'user/reset_password/' .$user_id. '/' . 'user' . '/' . $random_string;
    }
    
    function reset_password($user_id = '', $user_type = '', $key = '') {
        $this->load->model('site/Site_model');
        if ($_POST) {
            if ($this->compare_reset_password($_POST['password'], $_POST['confirm_password'])) {
                $data = array(
                    'password' => $this->__hash(trim($_POST['password']))
                );
                $user_data = $this->Site_model->update_password($user_type, $user_id, $data);
                $this->Site_model->reset_forgot_password_key($user_data['type'], $user_data['type_id'], $user_data['user_id']);

                $this->flash_notification('success', 'Password was successully reseted.');
                redirect(base_url('user/login'));
            } else {
                $this->flash_notification('danger', 'Password was mismatched.');
                redirect(base_url('user/reset_password/' . $user_id . '/' . $user_type . '/' . $key));
            }
        }
     
        if($user_id && $user_type && $key) {
            $is_key_present = $this->Site_model->check_for_forgot_password_key($user_type, $key);
            if ($is_key_present) {
                $this->data['title'] = 'Forgot Password';
                $this->load->view('user/user/reset_password', $this->data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }
    
    function check_user_type_hash($hash) {
        if (hash('md5', 'user'.config_item('encryption_key')) == $hash)
            return 'user';
    }
    
    function compare_reset_password($password, $confirm_password) {
        
        if (trim($password) == trim($confirm_password)) {
            return TRUE;
        }

        return FALSE;
    }

    function email_user_credential($user) {
        $this->load->model('email/Email_model');
        $this->load->helper('email/system_email');
        $subject = 'Top Doctor Login Credentials';
        $message = 'Hello, ' . $user['first_name'] . ' ' . $user['last_name'];
        $message .= "<br/>Your email address is registered with Top Doctor. ";
        $message .= "Now you can access your Top Doctor account by following credentials.";
        $message .= "<br/>Url: " .base_url().'user/login';
        $message .= "<br/>Email: " . $user['email'];
        $message .= "<br/>Password: " . $user['password'];
        Modules::run('email/email/setemail', $user['email'], $subject, $message, $attachment=array());
        return;
    }
    
    function password_compare_and_update($user_id, $password) {
        $user = $this->User_model->get($user_id);

        if ($user->password !== $password) {
            $password = Modules::run('user/__hash', $password);
            $this->User_model->update($user_id, array(
                'password' => $password
            ));
        }
    }

    function check_user_email($param = '') {
       
        $email = $this->input->post('email');
        if ($param == '') {
            $data = $this->Admin_model->get_by(array(
                'email' => $email
            ));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        } else {
            $data = $this->Admin_model->get_by(array(
                'admin_id !=' => $this->input->post('userid'),
                'email' => $email
            ));

            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }
}
