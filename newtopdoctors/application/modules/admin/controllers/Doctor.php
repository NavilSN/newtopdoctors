<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Doctor_details_model');
        $this->load->model('admin/Doctor_master_en_model');
        $this->load->model('admin/Doctor_master_model');
        $this->load->model('admin/Getalldoctor_model');

        $this->load->helper('language'); 
        $this->load->helper('text');
        date_default_timezone_set('Africa/Cairo');
        if ($this->session->userdata('admin_login') != 1){
            redirect(base_url().'admin/login');
        } 
    }
    function index() {
        $this->data['title']  = "Doctor"; 
        $this->data['page']   = "doctor";
        $this->__template('admin/doctor/index', $this->data);
    }
    
    function dashboard(){
        $this->data['page'] = 'dashboard';
        $this->__template('doctor/dashboard', $this->data);
    }
    function create() {
        if($_POST){            
                $this->Doctor_master_model->insert(array(
                'first_name'    => trim($_POST['f_name']),
                'last_name'     => trim($_POST['l_name']),
                'address'       => $_POST['address'],
                'major'         => $_POST['major'],
                'biography'     => $_POST['biography'],
            ));
                $this->Doctor_master_en_model->insert(array(
                'first_name'    => trim($_POST['f_name_en']),
                'last_name'     => trim($_POST['l_name_en']),
                'address'       => $_POST['address_en'],
                'major'         => $_POST['major_en'],
                'biography'     => $_POST['biography_en']
            ));
                $insert_id=$this->db->insert_id();
                if($_POST['work_id']!="")
                {
                $work_id = implode(',',$_POST['work_id']);
                }
                else{
                    $work_id = '';
                }
                 if (isset($_POST['featured']) == "1") {
                    $featured = '1'; // display featured list
                } else {
                    $featured = '0'; // hide from featured list uncheck
                }
                $this->Doctor_details_model->insert(array(
                'doctor_id'=>$insert_id,
                'email'                 => $_POST['email_id'],
                'work_id'               => $work_id,
                'location_id'           => $_POST['location'],
                'speciality_id'         => $_POST['specialties'],
                'working_hours'         => $_POST['work_hours'],
                'working_hours_en'         => $_POST['work_hours_en'],
                'phone_number'          => $_POST['phoneno'],
                'mobile_number'         => $_POST['mobileno'],
                'speciality_id'         => $_POST['specialties'],    
                'gender'                => $_POST['gen'],
                'google_map_latitude'   => $_POST['google_map_latitude'],
                'google_map_longtude'   => $_POST['google_map_longtude'],    
                'google_map_zoom'   => $_POST['google_map_zoom'],  
                'visibility'            => $_POST['status'], 
                'featured'              => $featured 
            ));
            $insert_id=$this->db->insert_id();

            if(!empty($_FILES['profilePhoto']['name'])) {
                  $file = explode(".", $_FILES['profilePhoto']['name']);
                $ext = strtolower(end($file));
                $newfile = date('dmYhis').'.'.$ext;         
                move_uploaded_file($_FILES['profilePhoto']['tmp_name'], FCPATH.'uploads/doctor_image/'.$newfile);
                
               // move_uploaded_file($_FILES['profilePhoto']['tmp_name'], FCPATH.'uploads/doctor_image/'.$insert_id.'.jpg');
                $this->Doctor_details_model->update($insert_id,array('photo'=>$newfile));
            }
            $this->flash_notification('Doctor is successfully inserted.');
            redirect(base_url('admin/doctor'));
        }
        $this->__template('admin/doctor/create');
    }

    /**
     * Update doctor data function
     * @param int $id
     */
    function update($id) {
            $doctor_id=$this->input->post('studentid');
            if($_POST) {
               $this->Doctor_master_model->update($this->input->post('studentid'),array(
                'first_name'    => trim($_POST['f_name']),
                'last_name'     => trim($_POST['l_name']),
                'address'       => $_POST['address'],
                'major'         => $_POST['major'],
                'biography'     => $_POST['biography'],
            ));
               $this->Doctor_master_en_model->update($this->input->post('studentid'),array(
                'first_name'    => trim($_POST['f_name_en']),
                'last_name'     => trim($_POST['l_name_en']),
                'address'       => $_POST['address_en'],
                'major'         => $_POST['major_en'],
                'biography'     => $_POST['biography_en'],
            ));
               if($_POST['work_id']!="")
                {
                $work_id = implode(',',$_POST['work_id']);
                }
                else{
                    $work_id = '';
                }
                 if (isset($_POST['featured']) == "1") {
                    $featured = '1'; // display featured list
                } else {
                    $featured = '0'; // hide from featured list uncheck
                }
                $this->Doctor_details_model->update($this->input->post('studentid'),array(
                'email'                 => $_POST['email_id'],
                'work_id'               =>  $work_id,
                'location_id'           => $_POST['location'],
                'speciality_id'         => $_POST['specialties'],
                'working_hours'         => $_POST['work_hours'],
                'working_hours_en'         => $_POST['work_hours_en'],
                'phone_number'          => $_POST['phoneno'],
                'mobile_number'         => $_POST['mobileno'],
                'speciality_id'         => $_POST['specialties'],    
                'gender'                => $_POST['gen'], 
                'google_map_latitude'   => $_POST['google_map_latitude'],
                'google_map_longtude'   => $_POST['google_map_longtude'],    
                'google_map_zoom'   => $_POST['google_map_zoom'],    
                'visibility'            => $_POST['status'], 
                'featured'              => $featured 
            ));

            if(!empty($_FILES['profilePhoto']['name'])) {
               // unlink('/uploads/doctor_image/'.$doctor_id.'.jpg');
               // move_uploaded_file($_FILES['profilePhoto']['tmp_name'], FCPATH.'uploads/doctor_image/'.$doctor_id.'.jpg');
                $existingfile = $_POST['existingfile'];
               unlink(FCPATH.'uploads/doctor_image/'.$existingfile); // delete existing image

                $file = explode(".", $_FILES['profilePhoto']['name']);
                $ext = strtolower(end($file));
                $newfile = date('dmYhis').'.'.$ext;  
                move_uploaded_file($_FILES['profilePhoto']['tmp_name'], FCPATH.'uploads/doctor_image/'.$newfile);                  
                $this->Doctor_details_model->update($doctor_id,array('photo'=>$newfile));
            }
            $this->flash_notification('Doctor is successfully updated.');
        }
        redirect(base_url('admin/doctor'));
    }

    function delete($id) {
        
        $row = $this->Doctor_details_model->get($id);
        $photo = $row->photo;
        @unlink(FCPATH.'/uploads/doctor_image/'.$photo);
        $this->load->model('admin/Doctors_rating_model');
        $this->db->where("doctor_id",$id);
        $this->db->delete("doctors_rating");
        $this->Doctor_master_model->delete($id);
        $this->Doctor_master_en_model->delete($id);
        $this->Doctor_details_model->delete($id);
        $this->flash_notification("Doctor successfully deleted");
        redirect(base_url() . 'admin/doctor/', 'refresh');
    }

    function latlong(){
        
        $zipcode=$_POST['address']; 
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=fam_close(fam)";
        $details=file_get_contents($url);
        $result = json_decode($details,true);
        $lat=$result['results'][0]['geometry']['location']['lat'];
        $lng=$result['results'][0]['geometry']['location']['lng'];

        echo $latlong=json_encode(array('lat'=>$lat,'lng'=>$lng));
    }
    
    function check_user_email($param = '') {
       
        $email = $this->input->post('email_id');
        if ($param == '') {
            $data = $this->Doctor_details_model->get_by(array('email' => $email));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        } else {
            $data = $this->Doctor_details_model->get_by(array(
                'doctor_id !=' => $this->input->post('userid'),
                'email' => $email
            ));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    function deleteAll(){

        foreach ($_POST['checkbox'] as $key => $value) {
	$this->db->delete('doctors_rating',array("doctor_id"=>$value));
           $this->Doctor_details_model->delete($value);
           $this->Doctor_master_model->delete($value);
           $this->Doctor_master_en_model->delete($value);
        }
        $this->flash_notification("Doctor successfully deleted");
        redirect(base_url('admin/doctor'));
    }

    

     function export(){
       //echo 'test';die();
        $this->load->dbutil();
        $this->load->helper('file');
       // $this->load->helper('csv');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";     
     
  
        $this->load->library('Excel');
    
        $this->excel->setActiveSheetIndex(0);
    
        $this->excel->getActiveSheet()->setTitle('Users list');
    /**
    FNameAR LNameAR AddressAR   FNameEN LNameEN AddressEN   Email   Location    Speciality  Status  Phone   Mobile  latitude    longitude   Zoom    BiographyEN BiographyAR WorkingHours    WorkingHoursEN  WorkName    Gender  MajorEN MajorAR WorkType

    */
       
        $this->excel->getActiveSheet()->SetCellValue('A1', 'FNameAR');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'LNameAR');
        $this->excel->getActiveSheet()->SetCellValue('C1', 'AddressAR');
        $this->excel->getActiveSheet()->SetCellValue('D1', 'FNameEN');  
        $this->excel->getActiveSheet()->SetCellValue('E1', 'LNameEN');
        $this->excel->getActiveSheet()->SetCellValue('F1', 'AddressEN');
        $this->excel->getActiveSheet()->SetCellValue('G1', 'Email');
        $this->excel->getActiveSheet()->SetCellValue('H1', 'Location');
        $this->excel->getActiveSheet()->SetCellValue('I1', 'Speciality');
        $this->excel->getActiveSheet()->SetCellValue('J1', 'Status');        
        $this->excel->getActiveSheet()->SetCellValue('K1', 'Phone');
        $this->excel->getActiveSheet()->SetCellValue('L1', 'Mobile');         
        $this->excel->getActiveSheet()->SetCellValue('M1', 'latitude');
        $this->excel->getActiveSheet()->SetCellValue('N1', 'longitude');
        $this->excel->getActiveSheet()->SetCellValue('O1', 'Zoom');
        $this->excel->getActiveSheet()->SetCellValue('P1', 'BiographyEN');
        $this->excel->getActiveSheet()->SetCellValue('Q1', 'BiographyAR');         
        $this->excel->getActiveSheet()->SetCellValue('R1', 'WorkingHours');
        $this->excel->getActiveSheet()->SetCellValue('S1', 'WorkingHoursEN');
        $this->excel->getActiveSheet()->SetCellValue('T1', 'WorkName');         
        $this->excel->getActiveSheet()->SetCellValue('U1', 'Gender');
        $this->excel->getActiveSheet()->SetCellValue('V1', 'MajorEN');
        $this->excel->getActiveSheet()->SetCellValue('W1', 'MajorAR');
        $this->excel->getActiveSheet()->SetCellValue('X1', 'WorkType');
        
          
         
  
        $doctor_data = $this->Doctor_details_model->getExportData();

  
        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($doctor_data, null, 'A2');
 
        $filename='ListOFDoctor.xls'; //save our workbook as this file name
 
        header('Content-Type: application/vnd.ms-excel'); //mime type
 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
 
        header('Cache-Control: max-age=0'); //no cache
                    
        
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
 
        
        $objWriter->save('php://output');

    }
    /**
    ** Import Doctor Data with XLSX file
    **
    */

    function import()
    {
          $file = $_FILES['importData']['tmp_name'];
          $file_upload = $_FILES['importData']['name'];
          $exp = explode(".", $file_upload);
          $filext = strtolower(end($exp));
          $extension = array('xls','xlsx');
          if(!in_array($filext, $extension))
          {            
            $this->session->set_flashdata("error", "Invalid file");
            redirect(base_url('admin/doctor'));
          }
          
//load the excel library
$this->load->library('excel');
//read file from path
$objPHPExcel = PHPExcel_IOFactory::load($file);
//get only the Cell Collection
$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
//extract to a PHP readable array format
foreach ($cell_collection as $cell) {
    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
    //header will/should be in row 1 only. of course this can be modified to suit your need.
    if ($row == 1) {
        $header[$row][$column] = $data_value;
    } else {

        $arr_data[$row][$column] = $data_value;

    }

}

//send the data in an array format
$data['header'] = $header;
$data['values'] = $arr_data;


 $insert_csv = array();
    
        
        
if($data['header'][1]['A']==="FNameAR" && $data['header'][1]['B']==="LNameAR" && $data['header'][1]['C']==="AddressAR" &&  $data['header'][1]['D']==="FNameEN" && $data['header'][1]['E']==="LNameEN" && $data['header'][1]['F']==="AddressEN" && $data['header'][1]['G']==="Email" && $data['header'][1]['H']==="Location" &&
    $data['header'][1]['I']==="Speciality" && $data['header'][1]['J']==="Status" && $data['header'][1]['K']==="Phone" && $data['header'][1]['L']==="Mobile" && $data['header'][1]['M']==="latitude" && $data['header'][1]['N']==="longitude" && $data['header'][1]['O']==="Zoom" && $data['header'][1]['P']==="BiographyEN" && $data['header'][1]['Q']==="BiographyAR" && $data['header'][1]['R']==="WorkingHours" && $data['header'][1]['S']==="WorkingHoursEN" && $data['header'][1]['T']==="WorkName" && $data['header'][1]['U']==="Gender" && $data['header'][1]['V']==="MajorEN" && $data['header'][1]['W']==="MajorAR" && $data['header'][1]['X']==="WorkType" )
{


                foreach ($arr_data as $row) {

    
    
                $insert_csv['FNameAR']          =   $row['A'];
                $insert_csv['LNameAR']          =   $row['B'];
                $insert_csv['AddressAR']        =   $row['C'];
                $insert_csv['FNameEN']          =   $row['D'];
                $insert_csv['LNameEN']          =   $row['E'];
                $insert_csv['AddressEN']        =   $row['F'];
                $insert_csv['Email']            =   $row['G'];                
                $insert_csv['Location']         =   $row['H'];
                $insert_csv['Speciality']       =   $row['I'];
                $insert_csv['Status']           =   $row['J'];                
                $insert_csv['Phone']            =   $row['K'];
                $insert_csv['Mobile']           =   $row['L'];
                $insert_csv['latitude']         =   $row['M'];
                $insert_csv['longitude']        =   $row['N'];
                $insert_csv['Zoom']             =   $row['O'];                
                $insert_csv['BiographyEN']      =   $row['P'];
                $insert_csv['BiographyAR']      =   $row['Q'];
                $insert_csv['WorkingHours']     =   $row['R'];
                $insert_csv['WorkingHoursEN']   =   $row['S'];
                $insert_csv['WorkName']         =   $row['T'];                
                $insert_csv['Gender']           =   $row['U'];
                $insert_csv['MajorEN']          =   $row['V'];
                $insert_csv['MajorAR']          =   $row['W'];
                $insert_csv['WorkType']         =   $row['X'];
      
                /* Set The validate content */
                    $insert_csv['FNameEN'] = $this->validateInput($insert_csv['FNameEN']);
                    $insert_csv['LNameEN'] = $this->validateInput($insert_csv['LNameEN']);
                    $insert_csv['FNameAR']= $this->validateInput($insert_csv['FNameAR']);
                    $insert_csv['LNameAR']= $this->validateInput($insert_csv['LNameAR']);
                    $insert_csv['AddressAR']= $this->validateInput($insert_csv['AddressAR']);
                    $insert_csv['AddressEN']= $this->validateInput($insert_csv['AddressEN']);
                    $insert_csv['Email'] = $this->validateInput($insert_csv['Email']);
                    $insert_csv['Mobile'] = $this->validateInput($insert_csv['Mobile']);
                    $insert_csv['Phone']= $this->validateInput($insert_csv['Phone']);
                    $insert_csv['Location'] = $this->validateInput($insert_csv['Location']);
                    $insert_csv['Speciality'] = $this->validateInput($insert_csv['Speciality']);
                    $insert_csv['Status'] = $this->validateInput($insert_csv['Status']);                
                    $insert_csv['latitude'] =$this->validateInput($insert_csv['latitude']);
                    $insert_csv['longitude']= $this->validateInput($insert_csv['longitude']);
                    $insert_csv['Zoom'] = $this->validateInput($insert_csv['Zoom']);    
                    $insert_csv['BiographyEN']=$this->validateInput($insert_csv['BiographyEN']); 
                    $insert_csv['BiographyAR']= $this->validateInput($insert_csv['BiographyAR']); 
                    $insert_csv['WorkingHours']= $this->validateInput($insert_csv['WorkingHours']);
                    $insert_csv['WorkingHoursEN']= $this->validateInput($insert_csv['WorkingHoursEN']);
                    $insert_csv['WorkName']= $this->validateInput($insert_csv['WorkName']);
                    $insert_csv['Gender']=$this->validateInput($insert_csv['Gender']);    
                    $insert_csv['MajorEN']=$this->validateInput($insert_csv['MajorEN']);  
                    $insert_csv['MajorAR']=$this->validateInput($insert_csv['MajorAR']);  
                    $insert_csv['WorkType'] = $this->validateInput($insert_csv['WorkType']);

                /* end string validate*/




                if($insert_csv['Gender']=='Male'){
                    $genstatus='1';
                }else{
                    $genstatus='2';
                }

                $insert_csv['Gender'] = $genstatus;
                if($insert_csv['Status']=='Active'){
                    $status='1';
                }else{
                    $status='0';
                }

                $insert_csv['Status'] = $status;


            $this->db->where('name_en', $insert_csv['WorkName']);
            $WorkMasteRow=$this->db->get('work_master')->row();

            $this->db->where('name_en', $insert_csv['Location']);
            $LocationRow=$this->db->get('location_master')->row();

            $this->db->where('name_en', $insert_csv['Speciality']);
            $SpecialityRow=$this->db->get('specialty_master')->row();

            $this->db->where('name_en', $insert_csv['MajorEN']);
            $MajorEN=$this->db->get('specialty_master')->row();

            $this->db->where('name', $insert_csv['MajorAR']);
            $MajorAR=$this->db->get('specialty_master')->row();            

            $dataDM = array(
                        'first_name' => trim($insert_csv['FNameAR']),
                        'last_name' => trim($insert_csv['LNameAR']),
                        'address' => $insert_csv['AddressAR'],
                        'major' =>$MajorAR->specialties_id,
                        'biography' =>$insert_csv['BiographyAR']
                       );
            $dataDME = array(
                        'first_name' => trim($insert_csv['FNameEN']),
                        'last_name' => trim($insert_csv['LNameEN']),
                        'address' => $insert_csv['AddressEN'],
                        'major' =>$MajorEN->specialties_id,
                        'biography' =>$insert_csv['BiographyEN']
                       );
            $dataDD = array(
                        'email' => $insert_csv['Email'],
                        'work_id' => $WorkMasteRow->work_id,
                        'location_id' => $LocationRow->location_id,
                        'speciality_id'=> $SpecialityRow->specialties_id,
                        'visibility' => $insert_csv['Status'],
                        'working_hours'=>$insert_csv['WorkingHours'],
                        'phone_number' => $insert_csv['Phone'],                        
                        'mobile_number' => $insert_csv['Mobile'],
                         'gender' => $insert_csv['Gender'],
                        'google_map_latitude' => $insert_csv['latitude'],
                        'google_map_longtude' =>  $insert_csv['longitude'],
                        'google_map_zoom' =>$insert_csv['Zoom'],
                        'working_hours_en'     =>$insert_csv['WorkingHoursEN']
                       );



          
             $CheckRow = '';
              $return ="true";
            if(!empty($insert_csv['Email']) || !empty($insert_csv['Phone']) || !empty($insert_csv['Mobile']))
            {
                if(!empty($insert_csv['Email']))
                {
                      $this->db->where('email',$insert_csv['Email']);
                }
                if( !empty($insert_csv['Phone']) ){
                      $this->db->or_where('phone_number',$insert_csv['Phone']);
                }
                if( !empty($insert_csv['Mobile'])){
                      $this->db->or_where('mobile_number',$insert_csv['Mobile']);
                }
            $CheckRow=$this->db->get('doctor_details')->row();
            }

            if($CheckRow==""){
                  
                    
              
                //echo $insert_csv['Email'];
                if(!empty($insert_csv['Email'])){
                     $validate = $this->emailValidate($insert_csv['Email']);
                    if($validate=="invalid email"){                        
                        $return = "false";
                    }
                }

                /*if(empty($insert_csv['FNameEN']) || empty($insert_csv['LNameEN']) || empty($insert_csv['FNameAR']) || empty($insert_csv['LNameAR']) || empty($insert_csv['AddressAR']) || empty($insert_csv['AddressEN'])){
                    $return = "false";

                }*/
                if(!empty($insert_csv['FNameEN']) && !empty($insert_csv['LNameEN'])){
                    
                    if($this->nameValidate($insert_csv['FNameEN'])=="Invalid" && $this->nameValidate($insert_csv['LNameEN'])=="Invalid" ){
                        $return = "false";
                    }

                }




                if( $return=="true" )
                {
                $this->db->insert('doctor_master', $dataDM);
                $this->db->insert('doctor_master_en', $dataDME);
                $this->db->insert('doctor_details', $dataDD);
                }
                else{

                }

            }else{
              

                if( $return=="true" )
                {
              
                $this->Doctor_master_model->update($CheckRow->doctor_id,$dataDM);
                $this->Doctor_master_en_model->update($CheckRow->doctor_id,$dataDME);
                $this->Doctor_details_model->update($CheckRow->doctor_id,$dataDD);
                }
            }
        
        
        
    }
          
            $this->flash_notification("Data successfully Import.");
            redirect(base_url('admin/doctor'));
        }
        else{
            
            $this->session->set_flashdata("error", "Columns mismatch. Please download the sample file to make sure of column indexes.");
            redirect(base_url('admin/doctor'));
        }
    }
    function importss(){
        $fp = fopen($_FILES['importData']['tmp_name'],'r') or die("can't open file");       
        $target_dir = FCPATH."uploads/csv_files/";
$target_file = $target_dir . basename($_FILES["importData"]["name"]);
        move_uploaded_file($_FILES["importData"]["tmp_name"], $target_file);
         log_message('debug', "Import file name and type: " . $_FILES['importData']['name']);
        $count=0;
        while($csv_line = fgetcsv($fp,1024)){
set_time_limit(60);
            $count++;
            if($count==1){
                continue; 
            }
            for ($i = 0; $i < count($csv_line); $i++){
                
                $insert_csv = array();
                 $insert_csv['FNameAR']      =    $this->utf8($csv_line[0]);
                $insert_csv['LNameAR']      =    $this->utf8($csv_line[1]);
                $insert_csv['AddressAR']      =    $this->utf8($csv_line[2]);
                $insert_csv['FNameEN']      =    $csv_line[3];
                $insert_csv['LNameEN']      =    $csv_line[4];
                $insert_csv['AddressEN']    =  $csv_line[5];
                $insert_csv['Email']        =      $csv_line[6];
                $insert_csv['WorkName']     =   $csv_line[7];
                $insert_csv['Location']     =   $csv_line[8];
                $insert_csv['Speciality']   = $csv_line[9];
                $insert_csv['Status']       =     $csv_line[10];
                $insert_csv['WorkingHours']      =    $this->utf8($csv_line[11]);
                $insert_csv['Phone'] = $csv_line[12];
                $insert_csv['Mobile'] = $csv_line[13];
                $insert_csv['Gender'] = $csv_line[14];
                $insert_csv['latitude'] = $csv_line[15];
                $insert_csv['longitude'] = $csv_line[16];
                $insert_csv['Zoom'] = $csv_line[17];
                  if($insert_csv['Gender']=='Male'){
                    $genstatus='1';
                }else{
                    $genstatus='2';
                }

                $insert_csv['Gender'] = $genstatus;
                if($insert_csv['Status']=='Active'){
                    $status='1';
                }else{
                    $status='0';
                }

                $insert_csv['Status'] = $status;

            }
            $this->db->like('name_en', $insert_csv['WorkName']);
            $WorkMasteRow=$this->db->get('work_master')->row();

            $this->db->like('name_en', $insert_csv['Location']);
            $LocationRow=$this->db->get('location_master')->row();

            $this->db->like('name_en', $insert_csv['Speciality']);
            $SpecialityRow=$this->db->get('specialty_master')->row();
            $dataDM = array(
                        'first_name' => trim($insert_csv['FNameAR']),
                        'last_name' => trim($insert_csv['LNameAR']),
                        'address' => $insert_csv['AddressAR'],
                       );
            $dataDME = array(
                        'first_name' => trim($insert_csv['FNameEN']),
                        'last_name' => trim($insert_csv['LNameEN']),
                        'address' => $insert_csv['AddressEN'],
                       );
            $dataDD = array(
                        'email' => $insert_csv['Email'],
                        'work_id' => $WorkMasteRow->work_id,
                        'location_id' => $LocationRow->location_id,
                        'speciality_id'=> $SpecialityRow->specialties_id,
                        'visibility' => $insert_csv['Status'],
                        'working_hours'=>$insert_csv['WorkingHours'],
                        'phone_number' => $insert_csv['Phone'],                        
                        'mobile_number' => $insert_csv['Mobile'],
                         'gender' => $insert_csv['Gender'],
                        'google_map_latitude' => $insert_csv['latitude'],
                        'google_map_longtude' =>  $insert_csv['longitude'],
                        'google_map_zoom' =>$insert_csv['Zoom']     
                       );



           // $CheckRow=$this->db->get_where('doctor_details',array('email'=>$insert_csv['Email']))->row();

          /*  if(count($CheckRow)==0){
                $this->db->insert('doctor_master', $dataDM);
                $this->db->insert('doctor_master_en', $dataDME);
                $this->db->insert('doctor_details', $dataDD);
            }*/
           // if((empty($CheckRow->email)) || ($CheckRow->email!=$insert_csv['Email'])){
             $CheckRow = '';
            if(!empty($insert_csv['Email']) || !empty($insert_csv['Phone']) || !empty($insert_csv['Mobile']))
            {
                if(!empty($insert_csv['Email']))
                {
                      $this->db->where('email',$insert_csv['Email']);
                }
                if( !empty($insert_csv['Phone']) ){
                      $this->db->or_where('phone_number',$insert_csv['Phone']);
                }
                if( !empty($insert_csv['Mobile'])){
                      $this->db->or_where('mobile_number',$insert_csv['Mobile']);
                }
            $CheckRow=$this->db->get('doctor_details')->row();
            }

            if($CheckRow==""){
                $this->db->insert('doctor_master', $dataDM);
                $this->db->insert('doctor_master_en', $dataDME);
                $this->db->insert('doctor_details', $dataDD);
            }else{
             
                $this->Doctor_master_model->update($CheckRow->doctor_id,$dataDM);
                $this->Doctor_master_en_model->update($CheckRow->doctor_id,$dataDME);
                $this->Doctor_details_model->update($CheckRow->doctor_id,$dataDD);
            }
        }
        $this->flash_notification("Data successfully Import.");
        redirect(base_url('admin/doctor'));
    }
  
    public function is_utf8($string) 
    {
        return (mb_detect_encoding($string, 'UTF-8', true) == 'UTF-8');
    }
    public function utf8($utf8){   
        if(mb_detect_encoding($utf8,'UTF-8',true) =='UTF-8'){

        return $utf8;
        } else {

        $utf8=iconv("windows-1256","utf-8",$utf8);
        return $utf8;
        }
    }
    
    /**
     * Get Doctor data pagination
     * 
     */
    public function ajax_list()
    {
        //$list1=array();
        //$list2=array();
        //$list1 = $this->Doctor_master_en_model->get_datatables();
        //$list2 = $this->Doctor_details_model->get_datatables();
        if($_POST['location']!="")
        {
        $location_list = getChildLocation($_POST['location']);
        }else{
            $location_list = '';
        }
        $list=$this->Getalldoctor_model->get_datatables($location_list);
     
        $data = array();
       // $no = $_POST['start'];
        foreach ($list as $customers) {
            //$no++;
         $urlEdit = base_url()."admin/doctor/edit/".$customers->doctor_id;
            $btn = ($customers->visibility)?'btn-success': 'btn-danger';
            $status = ($customers->visibility)? 'Active' : 'Inactive';
            $row = array();            
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->doctor_id.'</a>';            
            $row[] = '<input type="checkbox" name="checkbox[]" id="checkbox[]" value="'.$customers->doctor_id.'"/>';                                    
            $row[] = '<a data-placement="top" data-toggle="tooltip"  target="_blank"  href="'.$urlEdit.'" target="_blank">'.$customers->first_name.'</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip"  target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->last_name.'</a>';                        
            $row[] = '<a data-placement="top" data-toggle="tooltip"  target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->email.'</a>';                        
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->phone_number.'</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->mobile_number.'</a>';   
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->speciality.'</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->Lname.'</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->created_at.'</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->updated_at.'</a>';            
           
            
            $urlDelete = base_url()."admin/doctor/delete/".$customers->doctor_id;
                    $row[] = '<a data-placement="top" data-toggle="tooltip" class="e-btn" href="'.$urlEdit.'" target="_blank"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a><a data-placement="top" class="del-btn" data-toggle="tooltip" onclick="confirm_modal('."'".$urlDelete."'".');" href="#" data-original-title="" title=""><span class="label label-danger mr6 mb6"><i aria-hidden="true" class="fa fa-trash-o"></i>Delete</span></a>';
            
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Getalldoctor_model->count_all(),
                        "recordsFiltered" => $this->Getalldoctor_model->count_filtered($location_list),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    /**
     * Update doctor status
     */
    function update_status(){
        $status=$_POST['status']; 
        $id=$_POST['id'];
        $this->Doctor_details_model->update($id,array('visibility'=> $status));  
    }

    /**
     * Doctor edit form data
     * @param int $id
     */
    function edit($id) {
        $this->data['title'] = "Doctor";
        $this->data['page'] = "doctor";  
        $this->data['row']=$this->Doctor_master_model->get($id);
        $this->data['row1']=$this->Doctor_master_en_model->get($id);
        $this->data['row2']=$this->Doctor_details_model->get($id); 
        $this->__template('admin/doctor/edit', $this->data);
    }
      
    
    /**
    check email is validate or not 
    @param $email 
    return string
    **/
    function emailValidate($email){
        
           
 $regex = '/^(\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}\s*?;?\s*?)+$/'; 
 return  $email = (preg_match($regex, $email))?$email:"invalid email";

    }
    /**
    Validate all input trim the spaces strip slashes and html specialcharacter validate
    **/
        function validateInput($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      $data = strip_tags($data);
      return $data;
    }

    /**
    First name last name validation alphanumeric
    **/
    function nameValidate($name){
        return (preg_match('/^[a-z0-9 .\-]+$/i', $name))?$name:"Invalid";
    }
  
    function removeImage($id){
        $image = array('photo' => '');
        if($id!="")
        {
        echo $this->Doctor_details_model->update($id,$image);
          unlink(FCPATH.'uploads/doctor_image/'.$_POST['photo']);       
        }
        
    }
   
}
