<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminManagement extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Admin_model');
        $this->load->helper('language'); 
        $this->load->helper('text');
	date_default_timezone_set('Africa/Cairo');
        if ($this->session->userdata('admin_login') != 1){
            redirect(base_url().'admin/login'); 
        }
    }
    function index() {
        $this->data['title'] = "Admin Management";
        $this->data['page'] = "AdminManagement";
        $this->data['AdminManagement'] = $this->Admin_model->get_all();
        $this->__template('admin/AdminManagement/index', $this->data);
    }
    function create(){
        if($_POST) {
                $admin_id = $this->Admin_model->insert(array(
                'first_name'    => trim($_POST['first_name']),
                'last_name'     => trim($_POST['last_name']),
                'email'         => trim($_POST['email']),
                'password'      => md5($_POST['password']),
                'gender'        => $_POST['gender'],
                'mobile'        => trim($_POST['mobile']),
                'city'          => trim($_POST['city']),
                'address'       => $_POST['address'],
            ));
            if($_FILES['photo']['name'] != ''){   
                unlink('uploads/admin_image/'.$admin_id.'.jpg');
                move_uploaded_file($_FILES['photo']['tmp_name'], 'uploads/admin_image/'.$admin_id.'.jpg');
                $this->Admin_model->update($admin_id,array('photo'    => $admin_id.'.jpg',));
            }      
            $this->flash_notification('Admin is successfully updated.');
            redirect(base_url('admin/AdminManagement'));
        }
        $this->data['title'] = "Admin Management";
        $this->data['page'] = "AdminManagement";
        $this->data['create'] = $this->Admin_model->get_all();
        $this->__template('admin/AdminManagement/create', $this->data);
    }
 function edit($id){
       
        $this->data['title'] = "Admin Management";
        $this->data['page'] = "AdminManagement";
        $this->data['user'] = $this->Admin_model->get($id);
        //echo "<pe";
        $this->__template('admin/AdminManagement/edit', $this->data);
    }

    function update($id) {
            if($_POST) {
                $admin_id = $this->Admin_model->update($id,array(
                'first_name'    => trim($_POST['first_name']),
                'last_name'     => trim($_POST['last_name']),
                'email'         => trim($_POST['email']),
                'gender'        => $_POST['gender'],
                'mobile'        => trim($_POST['mobile']),
                'city'          => trim($_POST['city']),
                'address'       => trim($_POST['address']),
            ));   
            if($_FILES['photo']['name'] != ''){   
                unlink('uploads/admin_image/'.$id.'.jpg');
                move_uploaded_file($_FILES['photo']['tmp_name'], 'uploads/admin_image/'.$id.'.jpg');
                $this->Admin_model->update($id,array('photo'    => $id.'.jpg',));
            }   
            $this->flash_notification('Admin is successfully updated.');
        }
        redirect(base_url('admin/AdminManagement'));
    }

    function delete($id) {
        $this->Admin_model->delete($id);
        unlink('uploads/admin_image/'.$id.'.jpg');
        $this->flash_notification("Admin successfully deleted");
        redirect(base_url() . 'admin/AdminManagement/', 'refresh');
    }
    function check_user_email($param = '') {
        $email = $this->input->post('email_id'); 
        if ($param == '') {
            $data = $this->User_master_model->get_by(array('email' => $email));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        } else {
            $data = $this->User_master_model->get_by(array(
                'user_id !=' => $this->input->post('userid'),
                'email' => $email
            ));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }
}
