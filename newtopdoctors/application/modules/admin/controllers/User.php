<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/User_master_model');
        $this->load->model('admin/Getalluser_model');
        $this->load->helper('language');
        $this->load->helper('text');
        date_default_timezone_set('Africa/Cairo');
        if ($this->session->userdata('admin_login') != 1) {
            redirect(base_url() . 'admin/login');
        }
    }
    public function index()
    {
        $this->data['title'] = "User";
        $this->data['page'] = "register";
        $this->data['doctor'] = $this->User_master_model->get_all();
        $this->__template('admin/register/index', $this->data);
    }

    public function dashboard()
    {
        $this->data['page'] = 'dashboard';
        $this->__template('register/dashboard', $this->data);
    }
    public function update($id)
    {
        if ($_POST) {

            $student_id = $this->User_master_model->update($this->input->post('studentid'), array(
                'first_name' => trim($_POST['f_name']),
                'last_name' => trim($_POST['l_name']),
                'email' => $_POST['email_id'],
                'mobile_number' => trim($_POST['mobileno']),
                'country_id' => $_POST['country_id'],
                'address' => $_POST['address'],
                'status' => $_POST['status'],
            ));
            $this->flash_notification('Register is successfully updated.');
        }
        redirect(base_url('admin/user'));
    }

    public function edit($id)
    {

        $this->data['title'] = "User";
        $this->data['page'] = "register";
        $this->data['row'] = $this->User_master_model->get($id);
        $this->__template('admin/register/edit', $this->data);
    }

    public function delete($id)
    {
        $this->User_master_model->delete($id);
        $this->flash_notification("Register successfully deleted");
        redirect(base_url() . 'admin/user/', 'refresh');
    }
    public function check_user_email($param = '')
    {
        $email = $this->input->post('email_id');
        if ($param == '') {
            $data = $this->User_master_model->get_by(array('email' => $email));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        } else {
            $data = $this->User_master_model->get_by(array(
                'user_id !=' => $this->input->post('userid'),
                'email' => $email,
            ));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }
    // Get Lat-Long using zipcode
    public function latlong($latitude = "", $langtitude = "")
    {
        $zipcode = $_POST['zip'];
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $zipcode . "&sensor=false";
        $details = file_get_contents($url);
        $result = json_decode($details, true);
        $lat = $result['results'][0]['geometry']['location']['lat'];
        $lng = $result['results'][0]['geometry']['location']['lng'];
        echo $latlong = json_encode(array('lat' => $lat, 'lng' => $lng));
    }

    public function update_status()
    {
        $status = $_POST['status'];
        $id = $_POST['id'];
        $student_id = $this->User_master_model->update($id, array('status' => $status));
    }

    public function deleteAll()
    {
        foreach ($_POST['checkbox'] as $key => $value) {
            $this->User_master_model->delete($value);
        }
        $this->flash_notification("User successfully deleted");
        redirect(base_url('admin/user'));
    }

    public function ajax_list()
    {
        //$list1=array();
        //$list2=array();
        //$list1 = $this->Doctor_master_en_model->get_datatables();
        //$list2 = $this->Doctor_details_model->get_datatables();
        $list = $this->Getalluser_model->get_datatables();

        //$list=array_merge($list1,$list2);
        //echo '<pre>';
        //print_r($list);
        // die();
        $data = array();
        // $no = $_POST['start'];
        foreach ($list as $customers) {
            //$no++;
            $urlEdit = base_url() . "admin/user/edit/" . $customers->user_id;
            $btn = ($customers->status) ? 'btn-success' : 'btn-danger';
            $status = ($customers->status) ? 'Active' : 'Inactive';
            $row = array();
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->user_id . '</a>';
            $row[] = '<input type="checkbox" name="checkbox[]" id="checkbox[]" value="' . $customers->user_id . '"/>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->first_name . '</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->last_name . '</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->email . '</a>';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="' . $urlEdit . '" target="_blank">' . $customers->mobile_number . '</a>';
            $row[] = '<i data="' . $customers->user_id . '" class="status_checks btn ' . $btn . '">' . $status . '</i>';
            if ($customers->register_from == 1) {
                $row[] = 'Website';
            } else {
                $row[] = 'App';
            }

            if ($customers->registration_date != '0000-00-00') {
                $row[] = $customers->registration_date;
            } else {
                $row[] = '';
            }

            //$urlEdit = base_url()."admin/user/edit/".$customers->user_id;
            $urlDelete = base_url() . "admin/user/delete/" . $customers->user_id;
            $row[] = '<a data-placement="top" data-toggle="tooltip" href="' . $urlEdit . '" target="_blank"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a><a data-placement="top" data-toggle="tooltip" onclick="confirm_modal(' . "'" . $urlDelete . "'" . ');" href="#" data-original-title="" title=""><span class="label label-danger mr6 mb6"><i aria-hidden="true" class="fa fa-trash-o"></i>Delete</span></a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Getalluser_model->count_all(),
            "recordsFiltered" => $this->Getalluser_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function createabusive()
    {

        if ($_POST) {
            $array = array("words" => $_POST['word']);
            $this->db->insert("abuse_words", $array);
            $this->flash_notification("Abuse successfully added");
            redirect(base_url('admin/user/abusive'));
        }

        $this->data['title'] = "Abuse Words";
        $this->data['page'] = "register";

        $this->__template('admin/register/abuse/create', $this->data);
    }

    public function abusive($param = '', $param2 = '')
    {

        $this->data['title'] = "Abuse Words";
        $this->data['page'] = "register";
        $this->data['words'] = $this->db->get('abuse_words')->result();

        $this->__template('admin/register/abuse/index', $this->data);
    }

}
