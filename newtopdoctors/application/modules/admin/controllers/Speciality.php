<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Speciality extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Specialty_master_model');
        $this->load->model('admin/Getallspecialty_model');
        $this->load->helper('language'); 
        $this->load->helper('text');
	date_default_timezone_set('Africa/Cairo');

         if ($this->session->userdata('admin_login') != 1){
            redirect(base_url().'admin/login');
        } 
    }
    function index() {
        $this->data['title']  = "Speciality"; 
        $this->data['page']   = "speciality";
        $this->data['speciality'] = $this->Specialty_master_model->get_all();
        $this->__template('admin/speciality/index', $this->data);
    }
    
    function dashboard()
    {
        $this->data['page'] = 'dashboard';
        $this->__template('speciality/dashboard', $this->data);
    }
    function create() {
        if ($_POST) {
                $doctor_id = $this->Specialty_master_model->insert(array(
                'name'      => trim($_POST['speciality_name']),
                'name_en'   => trim($_POST['name_en']),
                'status'    => $_POST['status'],
            ));
            $this->flash_notification('Speciality is successfully inserted.');
            redirect(base_url('admin/speciality'));
        }   
        $this->__template('admin/speciality/create');     
    }

    function edit($id) {
        $this->data['title']  = "Speciality"; 
        $this->data['page']   = "speciality";
        $this->data['row']    = $this->Specialty_master_model->get($id);
        $this->__template('admin/speciality/edit', $this->data);
    }

    function update($id) {
          if($_POST) {
              
                $hospital_id = $this->Specialty_master_model->update($this->input->post('studentid'),array(
                'name'      => trim($_POST['speciality_name']),
                'name_en'   => trim($_POST['name_en']),
                'status'    => $_POST['status'],
                ));
            $this->flash_notification('Speciality is successfully updated.');
        }
        redirect(base_url('admin/speciality'));
    }

    function delete($id) {
        $this->Specialty_master_model->delete($id);
        $this->flash_notification("Speciality successfully deleted");
        redirect(base_url() . 'admin/speciality/', 'refresh');
    }
    
    function update_status(){
        $status=$_POST['status']; 
        $id=$_POST['id'];
        $this->Specialty_master_model->update($id,array('status'=> $status));   
    }

    function deleteAll(){
        foreach ($_POST['checkbox'] as $key => $value) {
           $this->Specialty_master_model->delete($value);
        }
        $this->flash_notification("Speciality successfully deleted");
        redirect(base_url('admin/speciality'));
    }

    function export(){
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
       // $filename = "ListOFSpecialty.csv";
        $result = $this->db->query("SELECT name AS NameArbic,name_en AS NameEnglish,case when status = '1' then 'Active' when status = '0' then 'Inactive' end as Status  FROM specialty_master");
        
        $this->load->library('Excel');
        //activate worksheet number 1
  //$this->excel->removeSheetByIndex(0);

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
          $this->excel->getActiveSheet()->SetCellValue('A1', 'NameArbic');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'NameEnglish');
        $this->excel->getActiveSheet()->SetCellValue('C1', 'Status');
  
        $this->excel->getActiveSheet()->setTitle('Speciality list');
        
            $users = $this->db->query("SELECT name AS NameArbic,name_en AS NameEnglish,case when status = '1' then 'Active' when status = '0' then 'Inactive' end as Status  FROM specialty_master")->result_array();
  
  
        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($users, null, 'A2');
 
        $filename='ListOFSpeciality.xls'; //save our workbook as this file name
 
        header('Content-Type: application/vnd.ms-excel'); //mime type
 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
 
        header('Cache-Control: max-age=0'); //no cache
                    
        
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
 
        
        $objWriter->save('php://output');
        
        //$data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
        //force_download($filename, $data);
    }

    function import(){
        $fp = fopen($_FILES['importData']['tmp_name'],'r') or die("can't open file");
        $count=0;
        while($csv_line = fgetcsv($fp,1024)){
            $count++;
            if($count==1){
                continue; 
            }
            for ($i = 0; $i < count($csv_line); $i++){
                
                $insert_csv = array();
                $insert_csv['NameArbic'] = $this->utf8($csv_line[0]);
                $insert_csv['NameEnglish'] = $csv_line[1];
                $insert_csv['Status'] = $csv_line[2];
                
                if($insert_csv['Status']=='Active'){
                    $status='1';
                }else{
                    $status='0';
                }

                $insert_csv['Status'] = $status;

            }
            $data = array(
                        'name' => trim($insert_csv['NameArbic']),
                       'name_en' => trim($insert_csv['NameEnglish']),
                       'mipmap' => str_replace(' ', '_', trim($insert_csv['NameEnglish'])),
                       'status' => $insert_csv['Status'],
                       );
            $CheckRow=$this->db->get_where('specialty_master',array('name_en like'=>'%'.$data["name_en"].'%'))->row();
            if(count($CheckRow)==0){
                $this->db->insert('specialty_master', $data);
            }
        }
        $this->flash_notification("Data successfully Import.");
        redirect(base_url('admin/speciality'));
    }

    /**
     * Character set check
     * @param String $string
     * @return string
     */
    public function is_utf8($string) 
    {
        return (mb_detect_encoding($string, 'UTF-8', true) == 'UTF-8');
    }
    
    /**
     * Check arabic Character set
     * @param String $utf8
     * @return String
     */
    public function utf8($utf8){   
        if(mb_detect_encoding($utf8,'UTF-8',true) =='UTF-8'){

        return $utf8;
        } else {

        $utf8=iconv("windows-1256","utf-8",$utf8);
        return $utf8;
        }
    }

    public function ajax_list()
    {
        $list=$this->Getallspecialty_model->get_datatables();
        $data = array();
       // $no = $_POST['start'];
        foreach ($list as $customers) {
            //$no++;
            $urlEdit = base_url()."admin/speciality/edit/".$customers->specialties_id;
            $btn = ($customers->status)?'btn-success': 'btn-danger';
            $status = ($customers->status)? 'Active' : 'Inactive';
            $row = array();
            //$row[] = '<a href="#">';
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->specialties_id.'</a>';            
            $row[] = '<input type="checkbox" name="checkbox[]" id="checkbox[]" value="'.$customers->specialties_id.'"/>';                        
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->name.'</a>'; 
            $row[] = '<a data-placement="top" data-toggle="tooltip" target="_blank" href="'.$urlEdit.'" target="_blank">'.$customers->name_en.'</a>';           
              
            $row[] = '<i data="'.$customers->specialties_id.'" class="status_checks btn '.$btn.'">'.$status.'</i>';
            // $row[] = '</a>';         
            $urlEdit = base_url()."admin/speciality/edit/".$customers->specialties_id;
            $urlDelete = base_url()."admin/speciality/delete/".$customers->specialties_id;
            $row[] = '<a data-placement="top" data-toggle="tooltip" href="'.$urlEdit.'" target="_blank"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a><a data-placement="top" data-toggle="tooltip" onclick="confirm_modal('."'".$urlDelete."'".');" href="#" data-original-title="" title=""><span class="label label-danger mr6 mb6"><i aria-hidden="true" class="fa fa-trash-o"></i>Delete</span></a>';
            
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Getallspecialty_model->count_all(),
                        "recordsFiltered" => $this->Getallspecialty_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

}
