<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reviews extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Doctors_rating_model');
        $this->load->helper('language'); 
        if ($this->session->userdata('admin_login') != 1){
            redirect(base_url().'admin/login'); 
        }
    }
    
    function index() {
        $this->data['title'] = "Reviews";
        $this->data['page'] = "reviews";
        $this->data['reviews'] = $this->Doctors_rating_model->get_user_reivews();
        $this->__template('admin/reviews/index', $this->data);
    }
    
    
     function update_status(){
          $status=$_POST['status'];         
          $id=$_POST['id'];  
        
        $this->db->where("rating_id",$id);
        $this->db->update("doctors_rating",array('dstatus'=> $status));
        //echo $this->db->last_query();
      // $this->Doctors_rating_model->update($id,array('dstatus'=> $status));  
    }
    
    /**
     * get review with pagination
     */
    
      public function ajax_list()
    {
          $this->load->model('admin/Getallrating_model');
        //$list1=array();
        //$list2=array();
        //$list1 = $this->Doctor_master_en_model->get_datatables();
        //$list2 = $this->Doctor_details_model->get_datatables();
        $list=$this->Getallrating_model->get_datatables();
        
        //$list=array_merge($list1,$list2);
        //echo '<pre>';
        //print_r($list);
        // die();
        $data = array();
       // $no = $_POST['start'];
        foreach ($list as $customers) {

          if(isset($customers->total_count))
          {
            $total_count = $customers->total_count;
          }
          else{
            $total_count = get_doctor_total_review($customers->doctor_id);
          }
            //$no++;
                    if($customers->dstatus==0){
                              $btn =   'btn-success';
                              $status = 'Active';
                            }
                            else{
                                 $btn = 'btn-danger';
                                 $status = 'Inactive';
                            }
                            if($customers->review_from=="1")
                            {
                                $review_from = "Website";
                            }
                            else{
                              $review_from = "App";
                            }
                             if($customers->visibility=="0" && $customers->user_id!="")
                            {
                              $username = $customers->ufname." ".$customers->ulname;
                              //echo "$username";die;
                            }else
                            {
                              $username = "Anonymous";
                            }
           // $btn = ($customers->dstatus)?'btn-success': 'btn-danger';
           // $status = ($customers->dstatus)? 'Active' : 'Inactive';
            $row = array();
            $row[] = $customers->rating_id;
            $row[] = '<input type="checkbox" name="checkbox[]" id="checkbox[]" value="' . $customers->rating_id . '"/>';                        
            $row[] = $customers->doctor_id;
            $row[] = $username;
            $row[] = $customers->average_score;            
            $row[] = $total_count;             
            $row[] = "<div style='white-space: normal;width:250px;word-break: break-word;'>".$customers->comment."</div>";               
            $row[] = $customers->date_created;                           
            $row[] =$review_from; 
            $row[] = '<i data="'.$customers->rating_id.'" id="'.$customers->rating_id.'" class="status_checks btn '.$btn.'">'.$status.'</i>';     
                                     
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Getallrating_model->count_all(),
                        "recordsFiltered" => $this->Getallrating_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }


    public function deleteAll()
    {
        foreach ($_POST['checkbox'] as $key => $value) {
            $this->Doctors_rating_model->delete($value);
        }
        $this->flash_notification("Reviews successfully deleted");
        redirect(base_url('admin/reviews'));
    }
    
}
