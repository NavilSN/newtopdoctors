<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Newsletter_model');
        //$this->load->model('admin/GetallNewsletter_model');
        $this->load->helper('language'); 
        if ($this->session->userdata('admin_login') != 1){
            redirect(base_url().'admin/login'); 
        }
    }
    
    function index() {
        $this->data['title'] = "Newsletter";
        $this->data['page'] = "newsletter";
        $this->data['newsletter'] = $this->Newsletter_model->get_all_newsletter();
        $this->__template('admin/newsletter/index', $this->data);
    }
    
    function export()
    {
          //echo 'test';die();
        $this->load->dbutil();
        $this->load->helper('file');
       // $this->load->helper('csv');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";     
  
        $this->load->library('Excel');
    
        $this->excel->setActiveSheetIndex(0);
    
        $this->excel->getActiveSheet()->setTitle('Users list');
 
       
         $this->excel->getActiveSheet()->SetCellValue('A1', 'Email');
  
        $users = $this->db->query("SELECT email from newsletter where status='1'")->result_array();
  
  
        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($users, null, 'A2');
 
        $filename='ListOfSubscriber.xls'; //save our workbook as this file name
 
        header('Content-Type: application/vnd.ms-excel'); //mime type
 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
 
        header('Cache-Control: max-age=0'); //no cache
                    
        
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
 
        
        $objWriter->save('php://output');
    }

 /**
     * Get Speciality list calling ajax (Datatable)
     */
    public function ajax_list()
    {
        $this->load->model('admin/Getallnewsletter_model');
        $list=$this->Getallnewsletter_model->get_datatables();
        $data = array();
       // $no = $_POST['start'];
        foreach ($list as $customers) {
            //$no++;
            //$urlEdit = base_url()."admin/speciality/edit/".$customers->specialties_id;
         //   $btn = ($customers->status)?'btn-success': 'btn-danger'; // status button color class active
         //   $status = ($customers->status)? 'Active' : 'Inactive'; // status button color class inactive
            $row = array();            
            $row[] =$customers->id;            
            $row[] = $customers->email;                        
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Getallnewsletter_model->count_all(),
                        "recordsFiltered" => $this->Getallnewsletter_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    }
