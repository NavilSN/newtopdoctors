<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site_model extends CI_Model {

    /**
     * Constructor
     * 
     * @return void
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Check for user email is present or not
     * @param string $email
     * @return object
     */
    function is_user_email_present($email) {
        //$users = ['student', 'admin', 'professor'];
//        foreach ($users as $user) {
            $record = $this->db->select()
                            ->from('admin')
                            ->where('email', $email)
                            ->get()->row();

            if ($record) {
                return $record;
            }
       // }

        return '';
    }

    /**
     * Update forgot password key
     * @param string $user_type
     * @param string $user_id
     * @param string $key
     */
    function update_forgot_password_key($user_type, $user_id, $key) {
             $this->db->where('admin_id', $user_id);
            $this->db->update('admin', [
                'password' => $key
            ]);
        
//             switch ($user_type) {
//            case 'admin':
//                $this->db->where('admin_id', $user_id);
//                $this->db->update('admin', [
//                    'forgot_password_link' => $key
//                ]);
//                break;
//            case 'student':
//                $this->db->where('std_id', $user_id);
//                $this->db->update('student', [
//                    'forgot_password_link' => $key
//                ]);
//                break;
//            case 'professor':
//                $this->db->where('professor_id', $user_id);
//                $this->db->update('professor', [
//                    'forgot_password_link' => $key
//                ]);
//                break;
//        }
    }

    /**     
     * Check for forgot password link
     * @param string $type
     * @param string $key
     * @return int
     */
    function check_for_forgot_password_key($type, $key) {
        return $this->db->get_where($type, [
                    'password' => $key
                ])->num_rows();
    }

    /**
     * Update user password
     * @param string $type
     * @param string $id
     * @param string $data
     */
    function update_password($type, $id, $data) {
        $user_data = array();
        $this->db->where('admin_id', $id);
            $this->db->update($type, $data);
            $user_data['type_id'] = 'admin_id';
//        switch ($type) {
//            case 'admin':
//                $this->db->where('admin_id', $id);
//                $this->db->update('admin', $data);
//                $user_data['type_id'] = 'admin_id';
//                break;
//            case 'student':
//                $this->db->where('std_id', $id);
//                $this->db->update('student', $data);
//                $user_data['type_id'] = 'std_id';
//                break;
//            case 'professor':
//                $this->db->where('professor_id', $id);
//                $this->db->update('professor', $data);
//                $user_data['type_id'] = 'professor_id';
//                break;
//        }
        $user_data['type'] = $type;
        $user_data['user_id'] = $id;

        return $user_data;
    }

    /**
     * Reset forgot password link
     * @param string $type
     * @param string $type_id
     * @param string $id
     */
    function reset_forgot_password_key($type, $type_id, $id) {
        $this->db->where($type_id, $id);
        $this->db->update($type, [
            'password' => ''
        ]);
    }

    /**
     * user comment delete permission
     * @param String $user_role
     * @param int $user_id
     * @param int $comment_id
     */
    function get_user_comment_delete_permission($user_role, $user_id, $comment_id) {
        $this->db->where("user_role", $user_role);
        $this->db->where("user_role_id", $user_id);
        $this->db->where("forum_comment_id", $comment_id);
        return $this->db->get("forum_comment")->row();
    }

    /**
     * delete comment
     * @param int $id
     */
    function delete_comment($id = '') {
        $this->db->delete("forum_comment", array("forum_comment_id" => $id));
    }

    /**
     * update comment data
     * @param mixed $data
     * @param int $id
     */
    function update_comment($data, $id) {
        $this->db->update("forum_comment", $data, array("forum_comment_id" => $id));
    }

}
