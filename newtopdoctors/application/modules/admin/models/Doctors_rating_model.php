<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Doctors_rating_model extends MY_Model {

    protected $primary_key = 'rating_id';

    function get_user_reivews(){
//        return $this->db->query('SELECT dr.rating_id, dr.average_score, CONCAT(um.first_name, " ", um.last_name) AS UserName, CONCAT(dme.first_name, " ", dme.last_name) AS DoctorName, dr.comment, dr.visibility, dr.date_created, dr.dstatus, dme.doctor_id, count(dr.user_id) as totlaUser FROM doctors_rating dr LEFT JOIN user_master um ON um.user_id=dr.user_id LEFT JOIN doctor_master_en dme ON dme.doctor_id=dr.doctor_id GROUP by dr.doctor_id')->result();
    	return $this->db->select('dr.rating_id,dr.average_score,CONCAT(um.first_name," ",um.last_name) AS UserName,CONCAT(dme.first_name," ",dme.last_name) AS DoctorName, dr.comment,dr.visibility,dr.date_created,dr.dstatus,dme.doctor_id')
    			->join('user_master um','um.user_id=dr.user_id','left')
    			->join('doctor_master_en dme','dme.doctor_id=dr.doctor_id','left')
    			->get('doctors_rating dr')
    			->result();


        
        /*
         * SELECT dr.rating_id, dr.average_score, CONCAT(um.first_name, " ", um.last_name) AS UserName, CONCAT(dme.first_name, " ", dme.last_name) AS DoctorName, dr.comment, dr.visibility, dr.date_created, dr.dstatus, dme.doctor_id, count(dr.user_id) as tot FROM doctors_rating dr LEFT JOIN user_master um ON um.user_id=dr.user_id LEFT JOIN doctor_master_en dme ON dme.doctor_id=dr.doctor_id GROUP by dr.doctor_id
         */
    }
}
