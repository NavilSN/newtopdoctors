<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Getallworkrating_model extends MY_Model
{
    protected $primary_key = 'work_rating_id';

    public $table = 'wrm,work_master';
    public $column_order = array('wrm.work_rating_id','work_master.work_id','user_master.first_name', 'work_master.work_id', 'work_master.work_type', 'wrm.average_score', 'total_count', 'wrm.comment', 'wrm.date_created', 'wrm.date_created', 'wrm.wstatus','wrm.review_from');
    public $column_search = array('wrm.work_rating_id','work_master.work_id','work_master.work_id', 'user_master.first_name', 'work_master.work_type', 'wrm.average_score', 'total_count', 'wrm.comment', 'wrm.date_created', 'wrm.wstatus','wrm.review_from'); //set column field database for datatable searchable
    public $order = array('wrm.work_rating_id' => 'desc');

    private function _get_datatables_query()
    {
        $this->db->select('wrm.*,work_master.work_type,user_master.first_name as ufname, user_master.last_name as ulname,(SELECT COUNT(work_id) FROM work_rating WHERE wrm.work_id = work_id) AS total_count');
        $this->db->from('work_rating wrm');
        $this->db->join('work_master', 'wrm.work_id=work_master.work_id', 'left');
        $this->db->join('user_master', 'wrm.user_id=user_master.user_id', 'left');
        //$this->db->join('location_master','location_master.location_id=work_master.location_id');
        //$this->db->join('doctor_details','doctor_master_en.doctor_id=doctor_details.doctor_id','right');
        //$this->db->join('specialty_master','specialty_master.specialties_id=doctor_details.speciality_id','left');
        //$this->db->join('location_master','location_master.location_id=doctor_details.location_id','left');

        $avg_rating = trim($this->input->post('avg_rating'));
        $review_count = trim($this->input->post('review_count'));
        $date_from = trim($this->input->post('date'));
        $date_to = trim($this->input->post('date_to'));
        $search_work = trim($this->input->post('search_work'));
        $from_id = trim($this->input->post('from_id'));
        $to_id = trim($this->input->post('to_id'));
        $work_id = trim($this->input->post('work_id'));
        $comment = trim($this->input->post('comment'));
        $search_count = trim($this->input->post('search_count'));
        $search_count_to = trim($this->input->post('search_count_to'));
        $search_user = trim($this->input->post('search_user'));
        $registration_from = $this->input->post('registration_from');
        $review_status = $this->input->post('review_status');

          if($review_status=="1" || $review_status=="0"){
            $this->db->where('wrm.wstatus',$review_status);
        }
        if($registration_from){
            $this->db->where('wrm.review_from',$registration_from);
        }      


        if ($avg_rating) {
            $this->db->where('wrm.average_score', $avg_rating);
        }
         /* if($search_user){
         $this->db->where("work_rating.visibility","0"); 
         $this->db->where(" concat_ws(' ',user_master.first_name,user_master.last_name) like '%".$this->db->escape_str($search_user)."%'");           

        }*/
         if($search_user){
            if($search_user=="Anonymous" || $search_user=="anonymous")
            {
                $this->db->where("wrm.visibility","1"); 
            }else{
            $this->db->where("wrm.visibility","0"); 
            $this->db->where(" concat_ws(' ',user_master.first_name,user_master.last_name) like '%".$this->db->escape_str($search_user)."%'");          
            }

         
         

        }

        if ($date_from && $date_to) {
            $date_from .= " 00:00:00";
            $date_to .= " 23:59:59";
            $this->db->where("wrm.date_created BETWEEN '$date_from' AND '$date_to'");
        }else{
            if($date_from){
                $this->db->where("wrm.date_created >= '$date_from'");
            }
            if($date_to){
                $this->db->where("wrm.date_created <= '$date_to'");   
            }
        }

        if ($search_work) {
            $this->db->where('work_master.work_type', $search_work);
        }

        if ($from_id && $to_id) {
            $this->db->where("wrm.work_rating_id BETWEEN '$from_id' AND '$to_id'");
        }else{
            if($from_id)
            {
                $this->db->where("wrm.work_rating_id >= '$from_id'");
            }
            if($to_id)
            {
                $this->db->where("wrm.work_rating_id <= '$to_id'");   
            }
        }

        if ($work_id) {
            $this->db->where("wrm.work_id", $work_id);
        }

        if ($comment) {
            $this->db->like("wrm.comment", $comment, 'both');
        }

        if ($search_count && $search_count_to) {
             $this->db->where("wrm.work_id IN (SELECT work_id
                 FROM work_rating
                 GROUP BY work_id
                 HAVING count(work_id) between $search_count and $search_count_to
                 ORDER BY work_id)");
            /*$this->db->group_by("wrm.work_id");
            $this->db->having("COUNT(wrm.work_id) >= $search_count AND COUNT(wrm.work_id) <= $search_count_to");*/
        }
        else{
            if($search_count){
                 $this->db->where("wrm.work_id IN (SELECT work_id
                 FROM work_rating
                 GROUP BY work_id
                 HAVING count(work_id) >= $search_count)");
                /*$this->db->group_by("wrm.work_id");
                $this->db->having("COUNT(wrm.work_id) >= '$search_count'");*/
            }
            if($search_count_to){
                $this->db->where("wrm.work_id IN (SELECT work_id
                 FROM work_rating
                 GROUP BY work_id
                 HAVING count(work_id) <= $search_total_count)");
                /*$this->db->group_by("wrm.work_id");
                $this->db->having("COUNT(wrm.work_id) <= '$search_count_to'");*/   
            }
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                {
                    $this->db->group_end();
                }
                //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('work_rating');
        return $this->db->count_all_results();
    }
}
