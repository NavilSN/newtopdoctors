<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Work_master_model extends MY_Model {
    protected $primary_key = 'work_id';
      public $before_create = array('timestamps');
public $before_update = array('update_timestamps');    
 /**
     * Set timestamp field
     * @param array $work
     * @return array
     */
    protected function timestamps($work) {
        $work['created_date'] = date('Y-m-d H:i:s');        
        $work['modified_date'] = date('Y-m-d H:i:s');
        return $work;
    }
    
 /**
     * Set update timestamp field
     * @param array $work
     * @return array
     */
    protected function update_timestamps($work) {
        $work['modified_date'] = date('Y-m-d H:i:s');
        return $work;
    }

    public function get_all_work()
    {
        $this->db->order_by('name_en','ASC');
        return $this->db->get('work_master')->result();
    }

    /**
    * get all doctor data which is connected to work
    * return mixed array
    */
	function get_all_doctors_data($id)
	{
		$this->db->select('dd.doctor_id AS doctorId,dme.first_name AS FirstName,dme.last_name AS LastName');
		$this->db->join('doctor_details dd',("FIND_IN_SET(dd.work_id , wm.work_id)"), 'right');
		$this->db->join('doctor_master_en dme','dme.doctor_id=dd.doctor_id', 'right');
		return $this->db->get_where('work_master wm',array('wm.work_id'=>$id))->result();
	}

    /**
    *  all work data for export xls file
    * return mixed array
    */
    function getExportData(){
         return $this->db->query("SELECT firstname_ar as FNameAR,lastname_ar as LNameAR,address as AddressAR,
                firstname_en as FNameEN,lastname_en as LNameEN,address_en as AddressEN,
                email as Email,lm.name_en AS LocationName,GROUP_CONCAT(sm.name_en) AS specialityName,
                case when work_master.status = '1' then 'Active' when work_master.status = '0' then 'Inactive' end as Status,work_master.phone AS Phone, work_master.mobileno as Mobile,            work_master.work_latitude as GoogleMapLatitude,work_master.work_longitude as GoogleMapLongitude,work_master.work_zoom as GoogleMapZoom,work_master.work_biography_en as BiographyEN,work_master.work_biography as BiographyAR, work_master.work_hours as WorkingHours,work_master.work_hours_en as WorkingHoursEN,'' as WorkName,'' as Gender,
                '' as MajorEN,'' as MajorAR,work_type as WorkType FROM work_master LEFT JOIN location_master AS lm on lm.location_id=work_master.location_id  LEFT JOIN specialty_master AS sm on FIND_IN_SET(sm.specialties_id,work_master.speciality) GROUP BY work_master.work_id")->result_array();
    }
}
