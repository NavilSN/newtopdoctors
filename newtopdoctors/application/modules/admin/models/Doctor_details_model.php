<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor_details_model extends MY_Model {
    protected $primary_key = 'doctor_id';
    
    public $before_create = array('timestamps');
    public $before_update = array('update_timestamps');    
    /**
     * Set timestamp field
     * @param array $doctor
     * @return array
     */
    protected function timestamps($doctor) {
        $doctor['created_at'] = date('Y-m-d H:i:s');        
        $doctor['updated_at'] = date('Y-m-d H:i:s');
        return $doctor;
    }
    
    /**
     * Set update timestamp field
     * @param array $doctor
     * @return array
     */
    protected function update_timestamps($doctor) {
        $doctor['updated_at'] = date('Y-m-d H:i:s');
        return $doctor;
    }
    /**
	 * Get doctor details
	 * @param  string $id 
	 * @return object
	 */
	function doctor_details($id) {
		return $this->db->select()
			->from('doctor_details')
			->join('doctor_master_en', 'doctor_master_en.doctor_id = doctor_details.doctor_id')
			->where('doctor_details.doctor_id', $id)
			->get()
			->row();
	}
     /**
    * Return All data of doctor arabic data english data and other details
    * return mixed array
    *
    */

    function getExportData(){
       return $this->db->query("SELECT dm.first_name AS FirstName,
            dm.last_name AS lastName,
            dm.address AS Address,
            dme.first_name AS FirstNameEnglish,
            dme.last_name AS lastNameEnglish,
            dme.address AS AddressEnglish,
            dd.email AS Email,
            lm.name_en AS LocationName,
            GROUP_CONCAT(sm.name_en) AS specialityName,
            case when dd.visibility = '1' then 'Active' when dd.visibility = '0' then 'Inactive' end as Status, 
            dd.phone_number as PhoneNumber, 
            dd.mobile_number as MobileNumber ,            
            dd.google_map_latitude as GoogleMapLatitude,
             dd.google_map_longtude as GoogleMapLongitude,
             dd.google_map_zoom as GoogleMapZoom,
             dme.biography as BiographyEN,
             dm.biography as BiographyAR,
             dd.working_hours as WorkingHours,
             dd.working_hours_en as WorkingHoursEN,
             wm.name_en AS WorkName, 
             case when dd.gender = '1' then 'Male' when dd.gender = '0' then 'Female' end as Gender,
             GROUP_CONCAT(sm.name_en) as MajorEN,
             GROUP_CONCAT(sm.name) as MajorAR,
             '' as WorkType  FROM doctor_details AS dd  LEFT JOIN doctor_master AS dm on dm.doctor_id=dd.doctor_id LEFT JOIN doctor_master_en AS dme on dme.doctor_id=dd.doctor_id LEFT JOIN work_master AS wm on wm.work_id=dd.work_id LEFT JOIN location_master AS lm on lm.location_id=dd.location_id LEFT JOIN specialty_master AS sm on FIND_IN_SET(sm.specialties_id,dd.speciality_id) GROUP BY dd.doctor_id")->result_array();

    }
}
