<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Work_rating_model extends MY_Model {

    protected $primary_key = 'work_rating_id';

    function get_user_reivews(){

    	return $this->db->select('wr.work_rating_id,wr.average_score,CONCAT(um.first_name," ",um.last_name) AS UserName,w.name AS Name, wr.comment,wr.visibility,wr.date_created,wr.wstatus,w.work_id,w.work_type,(SELECT count(wr.user_id) FROM work_rating wr left join `work_master` wm on wm.work_id = wr.work_id) AS totlaUser')
    			->join('user_master um','um.user_id=wr.user_id','left')
    			->join('work_master w','w.work_id=wr.work_id','left')
    			->get('work_rating wr')
    			->result();
    }
}
