<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Location_master_model extends MY_Model {

    protected $primary_key = 'location_id';
    public $before_create = array('timestamps');
    public $before_update = array('update_timestamps');

    /**
    * Set the timestamps before creating the new location
    * @param $location_master array
    * @return array
    */
    protected function timestamps($location_master) {
        $location_master['created_date'] = $location_master['modified_date'] = date('Y-m-d H:i:s');

        return $location_master;
    }

    /**
    * Set the timestamp before updating the location details
    * @param $location_master array
    * @return array
    */
    protected function update_timestamps($location_master) {
        $location_master['modified_date'] = date('Y-m-d H:i:s');
        
        return $location_master;
    }

    public function get_all_location_by_name()
    {
        $this->db->order_by('name_en','ASC');
        return $this->db->get('location_master')->result();
    }
    
    public function get_parent_location()
    {
        $this->db->where('parent_id','0');                
        return $this->db->get('location_master')->result();
    }
    
    public function get_order_by_parent_id()
    {
        $this->db->order_by('parent_id','ASC');
        return $this->db->get('location_master')->result();
    }

}
