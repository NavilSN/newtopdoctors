<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_model extends MY_Model {

    protected $primary_key = 'id';

    public function get_all_newsletter()
    {
    	$this->db->where('status','1');
    	return $this->db->get('newsletter')->result();
    }

}
