<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Getalluser_model extends MY_Model
{
    protected $primary_key = 'user_id';

    public $table = 'user_master';
    public $column_order = array('user_master.user_id','user_master.user_id','user_master.first_name', 'user_master.last_name', 'user_master.email', 'user_master.mobile_number','user_master.status','register_from','registration_date','user_master.user_id');
    public $column_search = array('user_master.user_id','user_master.user_id','user_master.first_name', 'user_master.last_name', 'user_master.email', 'user_master.mobile_number','user_master.status','register_from','registration_date','user_master.user_id'); //set column field database for datatable searchable
    public $order = array('user_master.user_id' => 'desc');

    private function _get_datatables_query()
    {
        $this->db->select('user_master.*');
        $this->db->from('user_master');
        //$this->db->join('location_master','location_master.location_id=work_master.location_id');
        //$this->db->join('doctor_details','doctor_master_en.doctor_id=doctor_details.doctor_id','right');
        //$this->db->join('specialty_master','specialty_master.specialties_id=doctor_details.speciality_id','left');
        //$this->db->join('location_master','location_master.location_id=doctor_details.location_id','left');

        if (trim($this->input->post('from')) && trim($this->input->post('to'))) {
            $this->db->where('user_master.user_id BETWEEN "' . trim($this->input->post('from')) . '" and "' . trim($this->input->post('to')) . '"');
        }
        else{
            if($this->input->post('from')){
                $this->db->where('user_master.user_id >= "' . trim($this->input->post('from')) . '"');   
            }
            if($this->input->post('to'))
            {
                $this->db->where('user_master.user_id <= "' . trim($this->input->post('to')) . '"');                    
            }
        }

        if (trim($this->input->post('registration_date')) && trim($this->input->post('registration_date_to'))) {
            $this->db->where('user_master.registration_date BETWEEN "' . trim($this->input->post('registration_date')) . '" and "' . trim($this->input->post('registration_date_to')) . '"');
        }
        else{
            if($this->input->post('registration_date')){
                $this->db->where('user_master.registration_date >= "' . trim($this->input->post('registration_date')) . '"');   
            }
            if($this->input->post('registration_date_to'))
            {
                $this->db->where('user_master.registration_date <= "' . trim($this->input->post('registration_date_to')) . '"');                    
            }
        }


        if ($this->input->post('user_status')=="1" || $this->input->post('user_status')=="0") {
            $this->db->where('user_master.status', trim($this->input->post('user_status')));
        }
        if ($this->input->post('registration_from')) {
            $this->db->where('user_master.register_from', trim($this->input->post('registration_from')));
        }


        
        if ($this->input->post('firstname')) {
            $this->db->like('user_master.first_name', trim($this->input->post('firstname')));
        }
        if ($this->input->post('lastname')) {
            $this->db->like('user_master.last_name', trim($this->input->post('lastname')));
        }
        if ($this->input->post('mobile')) {
            $this->db->like('user_master.mobile_number', trim($this->input->post('mobile')));
        }
        if ($this->input->post('email')) {
            $this->db->like('user_master.email', trim($this->input->post('email')));
        }
        

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                {
                    $this->db->group_end();
                }
                //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('user_master');
        return $this->db->count_all_results();
    }
}
