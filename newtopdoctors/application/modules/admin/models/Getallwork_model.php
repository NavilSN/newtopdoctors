<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Getallwork_model extends MY_Model
{
    protected $primary_key = 'work_id';

    public $table = 'work_master,location_master';
    public $column_order = array('work_master.work_id','work_master.work_id','work_master.name', 'email','phone','mobileno', 'work_type', 'location_master.name_en', 'work_master.created_date', 'work_master.modified_date');
    public $column_search = array('work_master.work_id','work_master.work_id','work_master.name', 'email','phone','mobileno', 'work_type', 'location_master.name_en', 'work_master.created_date', 'work_master.modified_date'); //set column field database for datatable searchable
    public $order = array('work_master.work_id' => 'desc');

    private function _get_datatables_query($location_list = '')
    {
        $this->db->select('work_master.*,location_master.name_en as loc_name,location_master.location_id');
        $this->db->from('work_master');
        $this->db->join('location_master', 'location_master.location_id=work_master.location_id', 'LEFT');
        //$this->db->join('doctor_details','doctor_master_en.doctor_id=doctor_details.doctor_id','right');
        //$this->db->join('specialty_master','specialty_master.specialties_id=doctor_details.speciality_id','left');
        //$this->db->join('location_master','location_master.location_id=doctor_details.location_id','left');

        if (trim($this->input->post('from')) && trim($this->input->post('to'))) {
            $this->db->where('work_master.work_id BETWEEN "' . trim($this->input->post('from')) . '" and "' . trim($this->input->post('to')) . '"');
        }
        else{
            if($this->input->post('from')){
            $this->db->where('work_master.work_id >= "' . trim($this->input->post('from')) . '"');
            }
            if($this->input->post('to')){
                $this->db->where('work_master.work_id <= "' . trim($this->input->post('to')) . '"');
            }
        }
        if ($this->input->post('nameAR')) {
             /*$name_ar = substitute_word($this->input->post('nameAR'));*/
          /*  $this->db->where("replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(work_master.firstname_ar, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','')='".$name_ar."'");
              $this->db->or_where("replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(work_master.lastname_ar, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','')='".$name_ar."'");*/
              $namear = trim($this->input->post('nameAR'));
              $this->db->like("work_master.name",$namear);
                /*$rawstring =  findword(trim($this->input->post('nameAR')));
             
            $like = "";
             foreach($rawstring as $key => $val):
                 if($key==0){
                    $like = "work_master.name LIKE '%".trim($val)."%'";
                 //$this->db->like('doctor_master.first_name', trim($val));

                 }
                 else{
                        $like .= " OR work_master.name LIKE '%".trim($val)."%'";
                    // $this->db->or_like('doctor_master.first_name', trim($val));                            
                 }
             endforeach;
             $this->db->where("($like)");*/
        }
        if ($this->input->post('nameEN')) {
            $this->db->like('work_master.name_en', $this->db->escape_str(trim($this->input->post('nameEN'))));
        }
        if ($this->input->post('email')) {
            $this->db->like('work_master.email', $this->db->escape_str(trim($this->input->post('email'))));
        }

        if ($this->input->post('phone')) {
            $this->db->like('work_master.phone', trim($this->input->post('phone')));
        }
        if ($this->input->post('mobile')) {
            $this->db->like('work_master.mobileno', trim($this->input->post('mobile')));
        }

        if ($this->input->post('created_date_from') || $this->input->post('created_date_to')) {
            if ($this->input->post('created_date_from') != "") {
                $this->db->where('work_master.created_date >=', date('Y-m-d 00:00:00', strtotime(trim($this->input->post('created_date_from')))));
            }
            if ($this->input->post('created_date_to') != "") {
                $this->db->where('work_master.created_date <=', date('Y-m-d 23:59:59', strtotime(trim($this->input->post('created_date_to')))));
            }
        }

        /* if($this->input->post('created_date'))
        {
        $this->db->where('work_master.created_date', $this->input->post('created_date'));
        }*/

        if ($this->input->post('location')) {
            $this->db->where_in('location_master.location_id', $location_list);
            // $this->db->like('location_master.name_en', trim($this->input->post('location')));
        }

        if ($this->input->post('work_type')) {
            $this->db->like('work_master.work_type', trim($this->input->post('work_type')));
        }

        //updated
        $search_updated_from = trim($this->input->post('search_updated_from'));
        $search_updated_to = trim($this->input->post('search_updated_to'));

        if ($search_updated_from && $search_updated_to) {
            $search_updated_from .= ' 00:00:00';
            $search_updated_to .= ' 23:59:59';
            $this->db->where("work_master.modified_date BETWEEN '$search_updated_from' AND '$search_updated_to'");
        }
        else{
            if($search_updated_from){
                $search_updated_from .= ' 00:00:00';
                $this->db->where("work_master.modified_date >= '$search_updated_from'");
            }
            if($search_updated_to){
                $search_updated_to .= ' 23:59:59';
                $this->db->where("work_master.modified_date <= '$search_updated_to'");
            }
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {

            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                {
                    $this->db->group_end();
                }
                //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

    }

    public function get_datatables($location_list = '')
    {
        $this->_get_datatables_query($location_list);

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered($location_list = '')
    {
        if ($location_list != "") {
            $this->_get_datatables_query($location_list);
        } else {
            $this->_get_datatables_query();
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        //die;
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('work_master');
        return $this->db->count_all_results();
    }
}
