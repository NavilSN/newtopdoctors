<?php
ini_set('max_execution_time', 300);
defined('BASEPATH') or exit('No direct script access allowed');
ini_set('memory_limit', '-1');
class Getalldoctor_model extends MY_Model
{
    protected $primary_key = 'doctor_id';

    public $table = 'doctor_master_en,doctor_details,doctor_master';
    public $column_order = array('doctor_details.doctor_id','doctor_details.doctor_id', 'doctor_master.first_name', 'doctor_master.last_name',  'doctor_details.email','doctor_details.phone_number','doctor_details.mobile_number', 'specialty_master.name_en', 'location_master.name_en',  'doctor_details.created_at','doctor_details.updated_at');
    public $column_search = array('doctor_details.doctor_id','doctor_details.doctor_id','doctor_master.first_name', 'doctor_master.last_name', 'doctor_details.email','doctor_details.phone_number','doctor_details.mobile_number', 'specialty_master.name_en', 'location_master.name_en','doctor_details.created_at',  'doctor_details.updated_at'); //set column field database for datatable searchable
    public $order = array('doctor_details.doctor_id' => 'desc');

    private function _get_datatables_query($location_list = '')
    {
        $this->db->select('doctor_details.doctor_id,doctor_master_en.first_name,doctor_master_en.last_name,doctor_details.email,doctor_details.phone_number,location_master.name_en AS Lname, doctor_master.first_name AS first_name, doctor_master.last_name AS last_name,specialty_master.name_en AS speciality,doctor_details.mobile_number,doctor_details.created_at,doctor_details.updated_at');
        $this->db->from('doctor_master_en');
        $this->db->join('doctor_master', 'doctor_master.doctor_id=doctor_master_en.doctor_id');
        $this->db->join('doctor_details', 'doctor_master_en.doctor_id=doctor_details.doctor_id', 'right');
        $this->db->join('specialty_master', 'specialty_master.specialties_id=doctor_details.speciality_id', 'left');
        $this->db->join('location_master', 'location_master.location_id=doctor_details.location_id', 'left');

        if ($this->input->post('from') && $this->input->post('to')) {
            $this->db->where('doctor_master.doctor_id BETWEEN "' . trim($this->input->post('from')) . '" and "' . trim($this->input->post('to')) . '"');
        } else {
            if ($this->input->post('from')) {
                $this->db->where('doctor_master.doctor_id >= "' . trim($this->input->post('from')) . '"');
            }
            if ($this->input->post('to')) {
                $this->db->where('doctor_master.doctor_id <= "' . trim($this->input->post('to')) . '"');
            }
        }

        if ($this->input->post('mobile_number')) {
            $this->db->like('mobile_number', trim($this->input->post('mobile_number')));
        }

        if ($this->input->post('email')) {
            $this->db->like('email', trim($this->input->post('email')));
        }
        if ($this->input->post('gender')) {
            $gender = '';
            switch ($this->input->post('gender')) {
                case 1:
                    $gender = 1;
                    break;
                case 2:
                    $gender = 2;
                    break;
                default:
                    $gender;
            }
            $this->db->where('gender', $gender);
        }
        if ($this->input->post('speciality')) {
            $this->db->like('specialty_master.name_en', trim($this->input->post('speciality')));
        }
        if ($this->input->post('location')) {

            $this->db->where_in('location_master.location_id', $location_list);
            //$this->db->like('location_master.name_en', trim($this->input->post('location')));
        }
        if ($this->input->post('phone_number')) {
            $this->db->like('phone_number', trim($this->input->post('phone_number')));
        }

        if ($this->input->post('created_at_from') || $this->input->post('created_at_to')) {
            if ($this->input->post('created_at_from') != "") {
                $this->db->where('doctor_details.created_at >=', date('Y-m-d 00:00:00', strtotime(trim($this->input->post('created_at_from')))));
            }
            if ($this->input->post('created_at_to') != "") {
                $this->db->where('doctor_details.created_at <=', date('Y-m-d 23:59:59', strtotime(trim($this->input->post('created_at_to')))));
            }
        }
        if ($this->input->post('updated_at_from') || $this->input->post('updated_at_to')) {
            if ($this->input->post('updated_at_from') != "") {
                $this->db->where('doctor_details.updated_at >=', date('Y-m-d 00:00:00', strtotime(trim($this->input->post('updated_at_from')))));
            }
            if ($this->input->post('updated_at_to') != "") {
                $this->db->where('doctor_details.updated_at <=', date('Y-m-d 23:59:59', strtotime(trim($this->input->post('updated_at_to')))));
            }
            //$this->db->where('doctor_details.created_at >=', date('Y-m-d H:i:s',strtotime($this->input->post('created_at'))));
            //
            //$this->db->where('doctor_details.created_at >=', date('Y-m-d H:i:s',strtotime($this->input->post('created_at'))));
            //$this->db->where('doctor_details.updated_at <=',  date('Y-m-d H:i:s',strtotime($this->input->post('updated_at'))));
        }

       /* $like = '';
        if ($this->input->post('firstName')) {
            $name_ar = substitute_word(trim($this->input->post('firstName')));
            $this->db->where("replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(doctor_master.first_name, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','')='".$name_ar."'");
        }
        $like2 = '';
        if ($this->input->post('lastName')) {
            $name_ar = substitute_word(trim($this->input->post('lastName')));
            $this->db->where("replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(doctor_master.last_name, 'أ', ''),'ا',''),'إ',''),'آ',''),'ى',''),'ي',''),'هـ',''),'ة',''),'ه',''),'ئـ',''),'ئ',''),'ؤ',''),'و',''),' ','')='".$name_ar."'");

        }*/
        $like = '';
          if($this->input->post('firstName'))
        {
           
             /*$rawstring =  findword(trim($this->input->post('firstName')));*/
             $fname = trim($this->input->post('firstName'));
            $this->db->like('doctor_master.first_name', $fname);
             /*foreach($rawstring as $key => $val):
                 if($key==0){
                    $like = "doctor_master.first_name LIKE '".trim($val)."'";
                 //$this->db->like('doctor_master.first_name', trim($val));

                 }
                 else{
                        $like .= " OR doctor_master.first_name LIKE '".trim($val)."'";
                    // $this->db->or_like('doctor_master.first_name', trim($val));                            
                 }
             endforeach;*/
             
            
        }
        $like2 = '';
        if($this->input->post('lastName'))
        {
            
             /*$rawstring =  findword(trim($this->input->post('lastName')));*/
            $lname =  trim($this->input->post('lastName'));
             $this->db->like('doctor_master.last_name', trim($lname));    
            // $this->db->where_in('doctor_master.first_name',$rawstring);
             /*foreach($rawstring as $key => $val):
                 if($key==0){
                     $like2 ="doctor_master.last_name LIKE '%".trim($val)."%' ";
                  //$this->db->like('doctor_master.last_name', trim($val));                            
                 }
                 else{
                    $like2 .=" OR doctor_master.last_name LIKE '%".trim($val)."%' ";
                    // $this->db->or_like('doctor_master.last_name', trim($val));                            
                 }
             endforeach;*/
            //$this->db->like('doctor_master.last_name', trim($this->input->post('lastName')));
        }
      /* if($this->input->post('firstName') && $this->input->post('lastName'))
       {
                $this->db->where("($like) AND ($like2)");
       }
       else{
               if($this->input->post('firstName'))
               {
                    $this->db->where("($like)");
               }
               if($this->input->post('lastName'))
               {
                    $this->db->where("($like2)");
               }
        }*/

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                {
                    $this->db->group_end();
                }
                //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables($location_list)
    {
        $this->_get_datatables_query($location_list);

        /*if($this->input->post('pageno')!="")
        {
        $start = ($this->input->post('pageno') * $_POST['length']);

        }
        else{
        $start = $_POST['start'];

        }*/
        if ($_POST['length'] != -1)
        //     $this->db->limit($_POST['length'], $start);
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        //  echo $this->db->last_query();
        //  die;
        return $query->result();
    }

    public function count_filtered($location_list = '')
    {
        if ($location_list != "") {
            $this->_get_datatables_query($location_list);
        } else {
            $this->_get_datatables_query();
        }

        $query = $this->db->get();
        //echo $this->db->last_query();
        //die;
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('doctor_details');
        return $this->db->count_all_results();
    }
}
