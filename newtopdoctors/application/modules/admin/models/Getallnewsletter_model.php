<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Getallnewsletter_model extends MY_Model {
    protected $primary_key = 'id';

    var $table = 'newsletter';
    var $column_order = array('newsletter.id','newsletter.email'); 
    var $column_search = array('newsletter.id','newsletter.email');
    var $order = array('newsletter.id' => 'asc');

    private function _get_datatables_query()
    {
        $this->db->select('newsletter.*'); 
        $this->db->from('newsletter');
        //$this->db->join('doctor_details','doctor_master_en.doctor_id=doctor_details.doctor_id','right');
        //$this->db->join('specialty_master','specialty_master.specialties_id=doctor_details.speciality_id','left');
        //$this->db->join('location_master','location_master.location_id=doctor_details.location_id','left');
        if($this->input->post('from') && $this->input->post('to'))
        {   
            $this->db->where('newsletter.id BETWEEN "'.$this->input->post('from'). '" and "'. $this->input->post('to').'"');
        }  

        if($this->input->post('email'))
        {
            $this->db->like('newsletter.email', trim($this->input->post('email')));
        }
        
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();       
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from('newsletter');
        return $this->db->count_all_results();
    }
}
