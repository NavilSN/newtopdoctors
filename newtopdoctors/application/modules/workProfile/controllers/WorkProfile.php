<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class WorkProfile extends MY_Controller {

    function __construct() {
        parent::__construct();
         date_default_timezone_set('Africa/Cairo');
        $this->load->model('home/User_master_model');
        $this->load->model('home/Work_rating_model');
	$this->data['slug_class'] = "workProfile";
    $this->load->helper('captcha'); // captcha helper
    }

    function index() {
        $siteLang=$this->session->userdata('site_lang');
        $workId = end(explode('-', $this->uri->segment(3))) ?: $this->uri->segment(3);
       if(empty($workId)){
          redirect(base_url('home'));
       }else{

          $this->db->select('work_master.photo,work_master.phone,work_master.email,(SELECT count(work_rating.work_id) FROM `work_rating` WHERE wstatus="1" AND work_id="'.$workId.'") as reviewCount,IFNULL(AVG(reputation), 0) as avgReputation,IFNULL(AVG(clinic),0) as avgClinic,IFNULL(AVG(availability),0) as avgAvailability,IFNULL(AVG(approachability),0) as avgApproachability,IFNULL(AVG(technology),0) as avgTechnology,
(SELECT count( *) FROM `work_rating` WHERE work_id="'.$workId.'" and wstatus="1" and (reputation) <> 0) as countreputation,
(SELECT count( *) FROM `work_rating` WHERE work_id="'.$workId.'"  and wstatus="1"  and (clinic) <> 0) as countclinic,
(SELECT count( *) FROM `work_rating` WHERE work_id="'.$workId.'"  and wstatus="1"  and (availability) <> 0)  as countavailability,
(SELECT count( *) FROM `work_rating` WHERE work_id="'.$workId.'"  and wstatus="1"  and (approachability) <> 0) as countapproachability,
(SELECT count( *) FROM `work_rating` WHERE work_id="'.$workId.'"  and wstatus="1"  and (technology) <> 0) as counttechnology,work_master.work_type,work_master.work_id,work_master.work_latitude,work_master.work_longitude,work_master.work_zoom,work_master.mobileno');
            if($siteLang=='arabic'){
                $this->db->select('work_master.name AS name,work_master.address AS address, work_master.work_biography as biography,work_master.work_hours as WorkHours');
            }else{
                $this->db->select('work_master.name_en AS name,work_master.address_en AS address, work_master.work_biography_en as biography,work_master.work_hours_en as WorkHours');
            }
            //$this->db->join('doctor_details','work_master.work_id=doctor_details.work_id','left');
            $this->db->join('work_rating','work_rating.work_id=work_master.work_id','left');
            //$this->db->join('specialty_master','specialty_master.specialties_id=doctor_details.speciality_id','left');
            //$this->db->join('location_master','location_master.location_id=doctor_details.location_id','left');
          $this->db->where('work_master.work_id',$workId);
                $this->db->group_by('work_master.work_id');
               // $this->db->where('work_rating.wstatus','1');
          $data['workDetail']=$this->db->get('work_master')->row();
          $data['workId'] = $workId;
            //echo $this->db->last_query(); die();

            $data['ReviewResult']=$this->db->select('(reputation + clinic + availability + approachability + technology)/5 as avgscore,work_master.*,user_master.*,work_rating.*')
            ->join('work_master','work_master.work_id=work_rating.work_id','left')
            ->join('user_master','user_master.user_id=work_rating.user_id','left')
            ->where('work_rating.work_id',$workId)
            ->where('work_rating.wstatus','1')
	    ->order_by('work_rating.work_rating_id','desc')
            ->get('work_rating')->result();
            //echo $this->db->last_query(); die();
        }
       $data['featureArray'] = featuresArray();


       $data['title']='TopDoctors';
	$data['slug_class'] = "workProfile";
          $data['redirectURL'] = current_url();

          /* Captcha code configuration */

        $config = array(
           
            'img_path'      => 'captcha_images/',
            'img_url'       => base_url().'captcha_images/',
            'img_width'     => '150',
            'img_height'    => 33,
            'word_length'   => 4,
            'font_size'     => 20,
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(0, 0, 0),
                'text' => array(0, 0, 0),
                'grid' => array(203, 203, 203)
            ),
            'img_id'        => 'Imageid',
            'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'expiration' => 7200

        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCodes');
        $this->session->set_userdata('captchaCodes',$captcha['word']);
        
        // Send captcha image to view
        $data['captchaImg'] = $captcha['image'];
        
        /* end captcha */


        $this->__templateFront('workProfile/index', $data);
    }

    public function refresh(){
        // Captcha configuration
        $config = array(
            
            'img_path'      => 'captcha_images/',
            'img_url'       => base_url().'captcha_images/',
            'img_width'     => '150',
            'img_height'    => 33,
            'word_length'   => 4,
            'font_size'     => 20,
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(0, 0, 0),
                'text' => array(0, 0, 0),
                'grid' => array(203, 203, 203)
            ),
            'img_id'        => 'Imageid',
            'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'expiration' => 7200

        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCodes');
        $this->session->set_userdata('captchaCodes',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
    }


    function reviewCreate(){
        $this->load->helper('text');
         $Reputation = $_POST['Reputation'];
                    $Clinic = $_POST['Clinic'];
                    $Availability = $_POST['Availability'];
                    $Approachability = $_POST['Approachability'];
                    $Technology = $_POST['Technology'];
                       $reviewIcon = $this->input->post('reviewIcon');
                       if (isset($_POST['visibility']) == "0") {
                    $visibility = '0';
                } else {
                    $visibility = '1';
                }
                $inputCaptcha = $this->input->post('captcha');
         $sessCaptcha = $this->session->userdata('captchaCodes');
         if($inputCaptcha === $sessCaptcha){
       /* if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
            $secret = '6Lfumg8UAAAAAJK8NWvVwDTED6wVv2ZxXNuAuD20';
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success){*/
                $userId=$this->session->userdata('user_id');
                $checkReviewDuplicate=$this->db->get_where('work_rating',
                    array('user_id'=>$userId,'work_id'=>$this->input->post('WorkId')))->row();
                $badWords = array("Fool","fuck","vagina","Idiot","bastard","name","Motherfucker");
                  $string = $this->input->post('reviews');
             //    $position = strpos($var1,$string);
    $disallowed = array("ابن كلب","ولاد كلب","ابن الحمار","بيضان","معرّص","ابن العرص","ابن الوسخة","ابن
الشرموطة","ابن القحبة","ابن المتناكة","ابن
اللبوة","وسخ","خولات","خول","علق","علوق","بهيمة","بهايم","كس أمك","طيزك حمرا","طيزك","الله
ينعل","يلعن أبو","أمك","ابوك","حمار","خرة","شخاح","ناك","ينيك","ولاد وسخة","ولاد
الوسخة","الوسخ","وسخ","ابن ال","بنت ال","ولاد ال","وسخ","اوساخ","اوسخ","أوساخ","بقر","بقرة","يا
حيوان","يا حيوانات","يا كلب","كلاب","بهايم","بهيم","بهيمه","منيكه","منياك","ابن الوسخة","ابن
المره","ديك","صفحةبنت","عرص","كسمك","معرص","الوسخة","ولاد","بنت","وسخ","وساخة","الوسخه","a7a","احا","أحا","متناكة","كس","ام","امك","حمار","Fool","fuck","vagina","Idiot","bastard","name","Motherfucker");
            $string = word_censor($string, $disallowed, 'Beep!');
              $mysymmary = $this->input->post('reviewSummary');
                $string2 = word_censor($mysymmary, $disallowed, 'Beep!');

                $position = strrchr($string, "Beep!");
                $position2 = strrchr($string2, "Beep!");
                //echo $string;die;
                $matches = array();
                /* $matchFound = preg_match_all(
                  "/\b(" . implode($badWords, "|") . ")\b/i", $string, $matches
                  ); */
                //echo $matchFound;die;
                $haystack = $position . " ".$position2;
                $needle = 'Beep!';
                if( strpos( $haystack, $needle ) !== false ) {

                       $status = '0';
                }
                else{
                     $status = '1';
                }
          

                if (isset($_POST['visibility']) == "0") {
                    $visibility = '0';
                } else {
                    $visibility = '1';
                }

                 if($this->input->post('reviewIcon')=="")
                {
                    $reviewIcon = '0';
                }
                else{
                    $reviewIcon = $this->input->post('reviewIcon');
                }
            $Reputation=$_POST['Reputation'];
            $Clinic=$_POST['Clinic'];
            $Availability=$_POST['Availability'];
            $Approachability=$_POST['Approachability'];
            $Technology=$_POST['Technology'];
            $average_score=($Reputation+$Clinic+$Availability+$Approachability+$Technology)/5;
                if(count($checkReviewDuplicate)==0){
                       $user_ip = $this->get_client_ip();
                    $this->Work_rating_model->insert(array(
                        'comment'    => $this->input->post('reviews'),
                        'summary'    => $this->input->post('reviewSummary'),
                        'work_id'    => $this->input->post('WorkId'),
                        'reputation' =>$Reputation,
                        'clinic' =>$Clinic,
                        'availability' => $Availability,
                        'approachability'=>$Approachability,
                        'technology'=>$Technology,
                        'average_score'=>$average_score,
                        'user_id'    => $userId,
                        'user_ip'=>$user_ip,
                        'visibility' => $visibility,
                        'reviewIcon' => $reviewIcon,
                        'wstatus'   =>$status
                    ));

                }else{
                    $this->Work_rating_model->update($checkReviewDuplicate->work_rating_id,array(
                        'comment' => $this->input->post('reviews'),
                        'reviewIcon' =>$reviewIcon,
                        'wstatus'   =>$status
                   ));

                }
                $this->flash_notification('Thanks for the review');
          /*  }else{
                 $this->session->set_flashdata('reputation',$Reputation);
                $this->session->set_flashdata('clinic',$Clinic);
                $this->session->set_flashdata('availability',$Availability);
                $this->session->set_flashdata('approachability',$Approachability);
                $this->session->set_flashdata('technology',$Technology);
                $this->session->set_flashdata('visibility',$visibility);
                $this->session->set_flashdata('reviewIcon',$reviewIcon);
                 $this->session->set_flashdata('reviews',$_POST['reviews']);
                 $this->session->set_flashdata('reviewSummary',$_POST['reviewSummary']);
                $this->session->set_flashdata('error', 'Robot verification failed, please try again');
            }*/
        }else{
             $this->session->set_flashdata('reputation',$Reputation);
                $this->session->set_flashdata('clinic',$Clinic);
                $this->session->set_flashdata('availability',$Availability);
                $this->session->set_flashdata('approachability',$Approachability);
                $this->session->set_flashdata('technology',$Technology);
                $this->session->set_flashdata('visibility',$visibility);
                $this->session->set_flashdata('reviewIcon',$reviewIcon);
            $this->session->set_flashdata('reviews',$_POST['reviews']);
            $this->session->set_flashdata('reviewSummary',$_POST['reviewSummary']);
            $this->session->set_flashdata('error','Captcha code was not match, please try again.');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    function userRateUpdate(){
        $userId=$_POST['userId'];
        $workId=$_POST['workId'];
        $Reputation=$_POST['Reputation'];
        $Clinic=$_POST['Clinic'];
        $Availability=$_POST['Availability'];
        $Approachability=$_POST['Approachability'];
        $Technology=$_POST['Technology'];



        $checkRow=$this->db->get_where('work_rating',array('work_id'=>$workId,'user_id'=>$userId))->row();
        if(count($checkRow)==0){
            $this->db->insert('work_rating',array('work_id'=>$workId,'user_id'=>$userId));
        }
        if(count($checkRow)>0 && !empty($Reputation)){
            $this->db->where('user_id',$userId);
            $this->db->where('work_id',$workId);
            $this->db->update('work_rating',array('reputation'=>$Reputation));
        }else if(count($checkRow)>0 && !empty($Clinic)){
            $this->db->where('user_id',$userId);
            $this->db->where('work_id',$workId);
            $this->db->update('work_rating',array('clinic'=>$Clinic));
        }else if(count($checkRow)>0 && !empty($Availability)){
            $this->db->where('user_id',$userId);
            $this->db->where('work_id',$workId);
            $this->db->update('work_rating',array('availability'=>$Availability));
        }else if(count($checkRow)>0 && !empty($Approachability)){
            $this->db->where('user_id',$userId);
            $this->db->where('work_id',$workId);
            $this->db->update('work_rating',array('approachability'=>$Approachability));
        }else if(count($checkRow)>0 && !empty($Technology)){
            $this->db->where('user_id',$userId);
            $this->db->where('work_id',$workId);
            $this->db->update('work_rating',array('technology'=>$Technology));
        }
        $average_score=($checkRow->reputation+$checkRow->clinic+$checkRow->availability+$checkRow->approachability+$checkRow->technology)/5;
            $this->db->where('user_id',$userId);
            $this->db->where('work_id',$workId);
            $this->db->update('work_rating',array('average_score'=>$average_score));
    }
 function check_user_email($param = '') {

        $email = $this->input->post('email_id');

        if ($param == '') {
            $data = $this->Work_master_model->get_by(array('email' => $email));
            if ($data) {
                echo "1";
            } else {
                echo "0";
            }
        } else {
            $data = $this->Doctor_details_model->get_by(array(
                'doctor_id !=' => $this->input->post('userid'),
                'email' => $email
            ));
            if ($data) {
                echo "1";
            } else {
                echo "0";
            }
        }
    }
    function check_user_email_duplicate($param = '') {

        $email = $this->input->post('email_id');

        if ($param == '') {
            $data = $this->Doctor_details_model->get_by(array('email' => $email));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        } else {
            $data = $this->Doctor_details_model->get_by(array(
                'doctor_id !=' => $this->input->post('userid'),
                'email' => $email
            ));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }
    /**
   * get user ip
   **/
   function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
}

