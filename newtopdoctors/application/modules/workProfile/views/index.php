
<div class="search-full-box">
  <section class="text-center clearfix">
  
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-12 text-left">
              
                <form class="form-inline" action="<?php echo base_url();?>search/globalSearch" method="post">
                  <div class="form-group">
                    <span class="flip search-icon"> <i class="fa fa-search"></i></span>
                    <input type="text" name="searchname" class="form-control" id="" placeholder="<?php //echo $this->lang->line('searchPlaceHolder'); ?>" value="<?php  if($typeofsearch=="globalsearch"){ echo $searchname; } ?>">
                  </div>
                  <div class="form-control-btn-box">
                    <button type="submit" name="searchData" class="btn btn-default search-btn"><?php echo $this->lang->line('searchbtn'); ?></button>
                  </div>
                </form>
            </div>
          </div>
      </div>
  </section>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/star-rating-svg.css">
<style type="text/css">
  label > input{ /* HIDE RADIO */
  visibility: hidden; /* Makes input not-clickable */
  position: absolute; /* Remove input from document flow */
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
}
label > input:checked + img{ /* (RADIO CHECKED) IMAGE STYLES */
  border:2px solid #008F9E;
   border-radius: 20px;
}
ul.b-footer-socials li {
    display: inline;
    list-style-type: none;
    margin-right: 3px;
    float: left;
}
 .input-captcha{
        width: 40%;
        float: left;
    }
    #captImg{
        float: left;
        margin: 0 10px 0 10px;
    }
</style>
<div class="filters-box m50t profile-only-doctor">
  <section class="text-center clearfix">
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-9 text-left">

                     <!-- doctor profile details start -->
                     <div class="doctor-profile clearfix">
                          <form role="form" class="form-horizontal">
                            <div class="form-group">
                              <div class="col-sm-12 pull-left col-xs-12">
                                 <div class="doctor-details clearfix">
                                    <div class="left-box pull-left">
                               <?php if($workDetail->work_type=='Hospital'){?>
                               <?php if(!file_exists(FCPATH.'uploads/workimage/'.$workDetail->photo) || $workDetail->photo==""){ ?>
                               <img src="<?php echo base_url(); ?>images/Hospitals.png" alt="" >
                               <?php }else{ ?>
                        <img src="<?php echo base_url(); ?>uploads/workimage/<?php echo $workDetail->photo; ?>" alt="" height="200">
                                <?php } ?>
                              <?php }else if($workDetail->work_type=='Clinic'){ ?>
                                  <img src="<?php echo base_url(); ?>images/Clinics.png" alt="" >
                              <?php }else if($workDetail->work_type=='Radiology Lab' || $workDetail->work_type=='Medical Lab' ){ ?>
                                  <img src="<?php echo base_url(); ?>images/Labs.png" alt="" >
                              <?php } ?>
                                    </div>
                                    <div class="right-box pull-right">
                                        <h2 class="flip text-left clearfix"><?php echo $workDetail->name; ?> </h2>
                                        <div class="border-divider w300"></div>
                                        <h3 class="flip text-left clearfix">
                                          <?php echo $workDetail->name_en; ?>
                                        </h3>
                                         <?php if ($workDetail->biography != "") { ?>
                                                <h3 class="flip text-left clearfix">

                                                    <?php echo $this->lang->line('Biography'); ?>
                                                </h3>
                                                <h3 class="color-green font-Medium flip text-left clearfix">
                                                    <?php echo $workDetail->biography; ?>
                                                </h3>
                                            <?php } ?>
                                             <?php if ($workDetail->WorkHours != "") { ?>
                                                    <div class="w100p clearfix">
                                                        <div class="second-one doctor-landline-num flip pull-left text-left">
                                                          <i class="fa fa-clock-o color-green" aria-hidden="true"></i>
                                                        </div>
                                                        <div class=" second-one flip pull-left">
                                                            <span class="phone">
                                                                <?php echo $workDetail->WorkHours; ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                         <div class="doctor-address-det flip text-left clearfix">
                                            <div class="w100p clearfix">
                                                <?php if( $workDetail->address!=""){ ?>
                                                  <div class="first-one location-icon flip pull-left text-left">
                                                    <img src="<?php echo base_url(); ?>images/location-icon-new.png" alt="">
                                                  </div>
                                              <div class="first-one flip pull-left">
                                                <span class="address"><?php echo $workDetail->address; if($workDetail->Lname!=""){ echo ", ".$workDetail->Lname; } ?>
                                                </span>
                                              </div>
                                                <?php } ?>
                                            </div>

                                            
                                                  <?php
$useragent=$_SERVER['HTTP_USER_AGENT'];
                                                   if($workDetail->phone!=""){ ?>
                                            <div class="w100p clearfix">
                                              <div class="second-one doctor-personal-m flip pull-left text-left">
                                                  <img src="<?php echo base_url(); ?>images/mobile-icon-new.png" alt="">
                                              </div>
                                              <div class=" second-one flip pull-left">
                                                <span class="phone">
                                                <?php $phone = explode(";",$workDetail->phone); 
                                                //$workDetail->phone; 

                                            $pcount = count($phone);
                                            $pstart = 1;
                                            foreach (@$phone as $rowphone) { 
                                              $tel = "tel:".$rowphone;
                                              ?>
                                                <a href="<?php echo $tel; ?>" class="displaytel" target="_blank" ><?php echo $rowphone; ?></a><?php if($pcount > $pstart){ ?>,<?php } ?>
                                            

                                            <?php
                                            $pstart++;
                                             } ?> 
                                                </span>
                                              </div>
                                            </div>
                                                  <?php } ?>
                                                        <?php if($workDetail->mobileno!=""){ ?>
                                            <div class="w100p clearfix">
                                              <div class="second-one doctor-personal-m flip pull-left text-left">
                                                  <img src="<?php echo base_url(); ?>images/mobile-icon-personal.png" alt="">
                                              </div>
                                              <div class=" second-one flip pull-left">
                                                <span class="phone">
                                                <?php $mobileno = explode(";",$workDetail->mobileno); 
                                                //$workDetail->phone; 
                                               
                                            $mcount = count($mobileno);
                                            $mstart = 1;
                                            foreach (@$mobileno as $rowmobileno) {   
                                              $mob = "tel:".$rowmobileno; ?>
                                                <a href="<?php echo $mob; ?>" target="_blank" ><?php echo $rowmobileno; ?></a><?php if($mcount > $mstart){ ?>,<?php } ?>

                                            <?php
                                            $mstart++;
                                             } ?> 
                                                </span>
                                              </div>
                                            </div>
                                                  <?php } ?>

                                             <div class="w100p clearfix">
                                              <div class="second-one email-icon flip pull-left text-left">
                                                <?php if ( $workDetail->work_type == 'Clinic') {?>
                                                  <img src="<?php echo base_url(); ?>images/add-clinic-hover.png" alt="">
                                                  <?php } ?>
                                                  <?php if ( $workDetail->work_type == 'Lab') {?>
                                                  <img src="<?php echo base_url(); ?>images/add-lab-hover.png" alt="">
                                                  <?php } ?>
                                                  <?php if ( $workDetail->work_type == 'Hospital') {?>
                                                  <img src="<?php echo base_url(); ?>images/add-hospital-hover.png" alt="">
                                                  <?php } ?>
                                              </div>
                                                 <?php if($workDetail->work_type!=""){ ?>
                                              <div class="second-one flip pull-left">
                                                  <span class="email">
                                                    <?php echo $this->lang->line($workDetail->work_type); ?>
                                                  </span>
                                              </div>
                                                 <?php } ?>
                                               </div>

                                               <input type="hidden" name="userId" class="userId<?php echo $a; ?>" value="<?php echo $this->session->userdata("user_id"); ?>">
                                              <input type="hidden" name="workId" class="workId" value="<?php echo $workDetail->work_id; ?>">
                                            <?php $bookmarkedRow2= $this->db->get_where('worktype_bookmark_master',array('work_id'=>$workDetail->work_id,'user_id'=>$this->session->userdata("user_id")))->row();
                                             if($this->session->userdata('user_login')==1) { ?>
                                                <button type="button" class="Bookmark <?php if($bookmarkedRow2->status==0){ echo 'bookmarkBigIconBlue';}else{echo 'bookmarkBigIcongray';}?> bookmarkDetailIcon bookmark-top-icon" name="bookmark" value="<?php echo $bookmarkedRow2->worktype_bookmark_id;  ?>"></button>
                                                <?php } ?>
                                                 <script>
                                                  $(".Bookmark").click(function() {
                                                    $.ajax({
                                                      type: 'POST',
                                                      url: "<?php echo base_url();?>search/bookmarkCreatework_type",
                                                      data: { userId: $('.userId').val(),workId:$('.workId').val()},
                                                      success: function(response){
                                                        location.reload();
                                                      }
                                                    });
                                                  });
                                                </script>

                                              
                                           </div>
                                    </div>
                                 </div>
                              </div>
                            </div>
                          </form>
                    </div>
                    <!-- doctor profile details end -->

                    <div class="border-divider fullwidth"></div>
                    <!-- ********** Doctor REVIEW rating start ********** -->
                      <div class="reviews-content clearfix flip text-left">
                          <h2 class="font-Medium clearfix">
                            <span class="flip pull-left"><?php echo $this->lang->line('DPReview'); ?> </span>
                            <span class="reviews-num flip pull-left"><?php echo $workDetail->reviewCount; ?></span>
                          </h2>
                          <div class="border-divider"></div>

                          <table class="rating-details">
                              <tbody>
                                  <tr>
                                    <th>
                                      <?php echo $this->lang->line('DPReputation'); ?>
                                    </th>
                                     <td>
                                      <div class="star-rat">
                                          <?php $reputation = getRatingByworks("reputation",$workDetail->work_id);
                                        
                                          ?>
                                      <input type="hidden" class="RateavgReputationVal" value="<?php echo $workDetail->avgReputation; ?>">
                                      <div class="RateavgReputation"></div>
                                      <script>
                                        $(function () {
                                        $(".RateavgReputation").rateYo({
                                          "starWidth": "20px",
                                          "ratedFill": "#03878A",
                                          "rating" : <?php if($reputation[0]->reputation > 0){ echo $reputation[0]->reputation; }else{ echo "0"; } ?>,
                                          "readOnly":true
                                          })
                                        });
                                      </script>
                                      </div>
                                       <div class="review-num"><?php echo $workDetail->countreputation; ?></div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>
                                      <?php echo $this->lang->line('DPClinic'); ?>
                                    </th>
                                    <td>
                                      <div class="star-rat">
                                          <?php $clinic = getRatingByworks("clinic",$workDetail->work_id); ?>
                                      <input type="hidden" class="RateavgClinicVal" value="<?php echo $workDetail->avgClinic; ?>">
                                      <div class="RateavgClinic"></div>
                                      <script>
                                        $(function () {
                                        $(".RateavgClinic").rateYo({
                                          "starWidth": "20px",
                                          "ratedFill": "#03878A",
                                          "rating" : <?php if($clinic[0]->clinic > 0){ echo $clinic[0]->clinic; }else{ echo "0"; } ?>,
                                          "readOnly":true
                                          })
                                        });
                                      </script>
                                      </div>
                                      <div class="review-num"><?php echo $workDetail->countclinic; ?></div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>
                                    <?php echo $this->lang->line('DPAvailability'); ?>
                                    </th>
                                    <td>
                                      <div class="star-rat">
                                             <?php $availability = getRatingByworks("availability",$workDetail->work_id); ?>
                                      <input type="hidden" class="RateavgAvailabilityVal" value="<?php echo $workDetail->avgAvailability; ?>">
                                      <div class="RateavgAvailability"></div>
                                      <script>
                                        $(function () {
                                        $(".RateavgAvailability").rateYo({
                                          "starWidth": "20px",
                                          "ratedFill": "#03878A",
                                          "rating" : <?php if($availability[0]->availability > 0){ echo $availability[0]->availability; }else{ echo "0"; } ?>,
                                          "readOnly":true
                                          })
                                        });
                                      </script>
                                      </div>
                                      <div class="review-num"><?php echo $workDetail->countavailability; ?></div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>
                                      <?php echo $this->lang->line('DPApproachability'); ?>
                                    </th>
                                    <td>
                                      <div class="star-rat">
                                          <?php $approachability = getRatingByworks("approachability",$workDetail->work_id); ?>
                                      <input type="hidden" class="RateavgApproachabilityVal" value="<?php echo $workDetail->avgApproachability; ?>">
                                      <div class="RateavgApproachability"></div>
                                      <script>
                                        $(function () {
                                        $(".RateavgApproachability").rateYo({
                                          "starWidth": "20px",
                                          "ratedFill": "#03878A",
                                          "rating" : <?php if($approachability[0]->approachability > 0){ echo $approachability[0]->approachability; }else{ echo "0"; } ?>,
                                          "readOnly":true
                                          })
                                        });
                                      </script>
                                      </div>
                                      <div class="review-num"><?php echo $workDetail->countapproachability; ?></div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>
                                      <?php echo $this->lang->line('DPTechnology'); ?>
                                    </th>
                                    <td>
                                      <div class="star-rat">
                                           <?php $technology = getRatingByworks("technology",$workDetail->work_id); ?>
                                      <input type="hidden" class="RateavgTechnologyVal" value="<?php echo $workDetail->avgTechnology; ?>">
                                      <div class="RateavgTechnology"></div>
                                      <script>
                                        $(function () {
                                        $(".RateavgTechnology").rateYo({
                                          "starWidth": "20px",
                                          "ratedFill": "#03878A",
                                          "rating" : <?php if($technology[0]->technology > 0){ echo $technology[0]->technology; }else{ echo "0"; } ?>,
                                          "readOnly":true
                                          })
                                        });
                                      </script>
                                      </div>
                                      <div class="review-num"><?php echo $workDetail->counttechnology; ?></div>
                                    </td>
                                  </tr>
                                </tbody>
                          </table>
                      </div>
                    <!-- ********** Doctor REVIEW rating end ********** -->
                  <?php $a=1; foreach ($ReviewResult as $ReviewResultRow) {
                   // if(!empty($ReviewResultRow->comment)){?>
                       <div class="excellent flip text-left clearfix">
                          <h3 class="clearfix">
                              <span class="title flip pull-left">
                                  <?php echo $ReviewResultRow->summary; ?>
                            </span>
                          
                            <span class="star-rat flip pull-left">
                              <input type="hidden" class="rateViewVal<?php echo $a; ?>" value="<?php echo $ReviewResultRow->avgscore; ?>">
                              <div class="RateView<?php echo $a; ?>"></div>
                                  <script>
                                    $(function () {
                                    $(".RateView<?php echo $a; ?>").rateYo({
                                      "starWidth": "15px",
                                      "ratedFill": "#03878A",
                                      "rating" : $('.rateViewVal<?php echo $a; ?>').val(),
                                      "readOnly":true
                                      })
                                    });
                                  </script>
                            </span>
                          </h3>
                          <div class="content flip text-left clearfix">
                            <p class="flip text-left clearfix">
                              <?php echo $ReviewResultRow->comment; ?>
                            </p>
                          </div>
                        <?php
                       
                        if($ReviewResultRow->reviewIcon!="" || $ReviewResultRow->reviewIcon!="0") {?>
                          <div class="excellent-doctor-pic">
                          <img src="<?php echo base_url(); ?>images/profile_files/hospital_review_<?php echo $ReviewResultRow->reviewIcon; ?>.png">
                          </div>
                          <?php } ?>
                          <div class="review-profile-pic">
                              <?php if(!empty($ReviewResultRow->photo) && file_exists(FCPATH.'uploads/user_image/'.$ReviewResultRow->photo && $ReviewResultRow->visibility=="1")){ ?>
                         
                            <img src="<?php echo base_url(); ?>uploads/user_image/<?php echo $ReviewResultRow->photo; ?>">
                            <?php }else{ ?>
                            <img src="<?php echo base_url(); ?>uploads/user.jpg">
                              <?php } ?>
                            <span class="review-name-date">
                                 <?php if($ReviewResultRow->visibility=="0"){ ?>
                            <?php echo $ReviewResultRow->first_name.' '.$ReviewResultRow->last_name." "; ?>
                                <?php }else{ ?>
                                    <?php echo $this->lang->line('AnonymousName'); ?>
                                <?php } ?>
                              <?php echo " ".date('Y/m/d',strtotime($ReviewResultRow->date_created)); ?>
                            </span>
                          </div>

                          <div class="border-divider fullwidth light"></div>
                      </div>
                    <?php //}?>

                  <?php $a++; } ?>

                  <!-- ********** REVIEW Form details start ********** -->
                  <?php if($this->session->userdata('user_id')) {?>
                      <div class="write-your-review clearfix">
                        <div class="clearfix">
                          <h3 class="title flip pull-left">
                            <span>
                              <?php echo $this->lang->line('DPWriteReview'); ?>
                            </span>
                          </h3>
                          <div class="border-divider"></div>
                        </div>
                    <div class="w100p clearfix positionrelative">
                       <div class="flip pull-left w40p">
                        <?php $rowDoctorDisable=$this->db->get_where('work_rating',array('user_id'=>$this->session->userdata('user_id'),'work_id'=>$workDetail->work_id))->row(); ?>
                          <form class="review-frm" action="<?php echo base_url();?>workProfile/reviewCreate" method="post" id="reviews-frm">
                            <input type="hidden" name="WorkId" value="<?php echo $workId; ?>">
                              <div class="form-group clearfix">
                                  <label class="flip text-left"><?php echo $this->lang->line('DPFRMName'); ?></label>
                                  <input type="text" class="form-control" name="reviewName" id="reviewName" value="<?php echo $this->session->userdata('user_firstname') .' '.$this->session->userdata('user_lastname'); ?>" disabled>
                              </div>
                              <div class="form-group clearfix">
                                  <label class="flip text-left"><?php echo $this->lang->line('DPFRMSummary'); ?></label>
                                  <input type="text" class="form-control" name="reviewSummary" value="<?php echo $this->session->flashdata('reviewSummary'); ?>" id="reviewSummary"<?php if(count($rowDoctorDisable)>0){?>disabled<?php } ?> value="">
                              </div>

                              <div class="form-group clearfix">
                                <label class="flip text-left"><?php echo $this->lang->line('DPReview'); ?></label>
                                <textarea class="form-control w100p" name="reviews" id="reviews" rows="4" <?php if(count($rowDoctorDisable)>0){?>disabled<?php } ?>><?php echo $this->session->flashdata('reviews'); ?></textarea>
                              </div>
                              <div class="form-group clearfix">
                                  <label class="flip text-left displayblock w100p"><?php echo $this->lang->line('choosewicon'); ?></label>
                                  <div class="choose-doctor clearfix">
                                  <ul class="b-footer-socials">
                                  <li>
                                  <label>
                                  <input type="radio"  name="reviewIcon" value="1" <?php if($this->session->flashdata('reviewIcon')=="1"){ echo "checked='checked'"; } ?> />
                                  <img src="<?php echo base_url(); ?>images/profile_files/hospital_review_1.png" title="<?php echo $this->lang->line('GreatHelp'); ?>" >
                                </label></li>
                                        <li><label>
                                      <input type="radio"   name="reviewIcon" value="2"  <?php if($this->session->flashdata('reviewIcon')=="2"){ echo "checked='checked'"; } ?>   />
                                     <img src="<?php echo base_url(); ?>images/profile_files/hospital_review_2.png" title="<?php echo $this->lang->line('Clean'); ?>" >
                                      </label></li>
                                      <li><label>
                                    <input type="radio"  name="reviewIcon" value="3" <?php if($this->session->flashdata('reviewIcon')=="3"){ echo "checked='checked'"; } ?>   />
                                      <img src="<?php echo base_url(); ?>images/profile_files/hospital_review_3.png" title="<?php echo $this->lang->line('Dirty'); ?>"></label></li>
                                      <li><label>
                                    <input type="radio" name="reviewIcon" value="4" <?php if($this->session->flashdata('reviewIcon')=="4"){ echo "checked='checked'"; } ?>  />
                                      <img src="<?php echo base_url(); ?>images/profile_files/hospital_review_4.png" title="<?php echo $this->lang->line('LongWaiting'); ?>" ></label></li>
                                      </ul>
                                  </div>
                              </div>
                            <div class="form-group clearfix">
            <input type="checkbox" class="checkboxvisibility"  name="visibility" <?php if($this->session->flashdata('visibility')=="1"){ echo "checked='checked'"; } ?>  value="0" />
                                <label class="flip text-left anonymoustitle" ><?php echo $this->lang->line('Anonymous'); ?></label>

                              </div>
                              <div class="clearfix"></div>
                              <div class="form-group clearfix">
                                <input name="captcha" type="text" maxlength="6" class="form-control input-captcha"  value="" />
                                                    <p id="captImg"><?php echo $captchaImg; ?></p>
    <a href="javascript:void(0);" class="refreshCaptcha" ><img src="<?php echo base_url().'images/refresh.png'; ?>"/></a></div>
                              <!-- <div class="g-recaptcha" data-sitekey="6Lfumg8UAAAAAGWPZ8Fx7Ibgn2iN2znFyEwhUrPa"></div> -->
                              <div class="clearfix"></div>
                              <input type="hidden" name="Reputation" id="Reputation" value="">
                            <input type="hidden" name="Clinic" id="Clinic" value="">
                            <input type="hidden" name="Availability" id="Availability" value="">
                            <input type="hidden" name="Approachability" id="Approachability" value="">
                            <input type="hidden" name="Technology" id="Technology" value="">

                              <div class="form-group clearfix flip text-left w100p pl30p">
                                <button type="submit" class="btn btn-primary"<?php if($this->session->userdata('user_login')!=1 || count($rowDoctorDisable)>0) {echo 'disabled'; } ?> ><?php echo $this->lang->line('DPSubmitReview'); ?></button>
                              </div>
                          </form>
                        </div>
                        <div class="flip pull-left w48p ml12p">
                        <?php if($this->session->userdata('user_login')=='1'){$readOnly='false';}else{$readOnly='true';}

                            $countReviewRow=$this->db->get_where('work_rating',array('work_id'=>$workId,'user_id'=>$this->session->userdata('user_id')))->row();
                           ?>

                           <table class="rating-details">
                            <tr>
                              <th>
                                <?php echo $this->lang->line('DPReputation'); ?>
                              </th>
                               <td>
                                <div class="star-rat">
                                  <div class="Reputation"></div>
                                  <?php if($countReviewRow->reputation == 0 && $this->session->userdata('user_login')=='1'){
                                      $readOnly='false';
                                  }else{
                                      $readOnly='true';
                                  } ?>
                                  <script>
                                    $(function () {
                                      $(".Reputation").rateYo({"starWidth": "15px",
                                        "ratedFill": "#03878A",
                                        "readOnly":<?php echo $readOnly; ?>,
                                        "fullStar": true,
                                        "rating" : <?php if($this->session->flashdata('reputation')){ echo $this->session->flashdata('reputation'); }else{ if(empty($countReviewRow->reputation)) echo 0; else echo $countReviewRow->reputation; }?>,
                                        onSet: function (rating, rateYoInstance) {
                                          //alert('Thanks for rate');
                                          $('#Reputation').append().val(rating);
                                        }
                                      });
                                    });
                                  </script>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <th>
                              <?php echo $this->lang->line('DPClinic'); ?>
                              </th>
                              <td>
                                <div class="star-rat">
                                  <div class="Clinic"></div>
                                  <?php if($countReviewRow->clinic == 0 && $this->session->userdata('user_login')=='1'){
                                      $readOnly='false';
                                  }else{
                                      $readOnly='true';
                                  } ?>
                                  <script>
                                    $(function () {
                                      $(".Clinic").rateYo({"starWidth": "15px",
                                        "ratedFill": "#03878A",
                                        "readOnly":<?php echo $readOnly; ?>,
                                        "fullStar": true,
                                        "rating" : <?php if($this->session->flashdata('clinic')){ echo $this->session->flashdata('clinic'); }else{  if(empty($countReviewRow->clinic)) echo 0; else echo $countReviewRow->clinic; } ?>,
                                        onSet: function (rating, rateYoInstance) {
                                          //alert('Thanks for rate');
                                          $('#Clinic').append().val(rating);
                                        }
                                      });
                                    });
                                  </script>
                                  </div>

                              </td>
                            </tr>
                            <tr>
                              <th>
                                <?php echo $this->lang->line('DPAvailability'); ?>
                              </th>
                              <td>
                                <div class="star-rat">
                                  <div class="Availability"></div>
                                  <?php if($countReviewRow->availability == 0 && $this->session->userdata('user_login')=='1'){
                                      $readOnly='false';
                                  }else{
                                      $readOnly='true';
                                  } ?>
                                  <script>
                                    $(function () {
                                      $(".Availability").rateYo({"starWidth": "15px",
                                        "ratedFill": "#03878A",
                                        "readOnly":<?php echo $readOnly; ?>,
                                        "fullStar": true,
                                        "rating" : <?php if($this->session->flashdata('availability')){ echo $this->session->flashdata('availability'); }else{  if(empty($countReviewRow->availability)) echo 0; else echo $countReviewRow->availability; } ?>,
                                        onSet: function (rating, rateYoInstance) {
                                        //alert('Thanks for rate');
                                        $('#Availability').append().val(rating);
                                        }
                                      });
                                    });
                                  </script>
                                </div>

                              </td>
                            </tr>
                            <tr>
                              <th>
                                <?php echo $this->lang->line('DPApproachability'); ?>
                              </th>
                              <td>
                                <div class="star-rat">
                                  <div class="Approachability"></div>
                                  <?php if($countReviewRow->approachability == 0 && $this->session->userdata('user_login')=='1'){
                                      $readOnly='false';
                                  }else{
                                      $readOnly='true';
                                  } ?>
                                  <script>
                                    $(function () {
                                      $(".Approachability").rateYo({"starWidth": "15px",
                                        "ratedFill": "#03878A",
                                        "readOnly":<?php echo $readOnly; ?>,
                                        "fullStar": true,
                                        "rating" : <?php if($this->session->flashdata('approachability')){ echo $this->session->flashdata('approachability'); }else{  if(empty($countReviewRow->approachability)) echo 0; else echo $countReviewRow->approachability; } ?>,
                                        onSet: function (rating, rateYoInstance) {
                                          $('#Approachability').append().val(rating);
                                        }
                                      });
                                    });
                                  </script>
                                </div>

                              </td>
                            </tr>
                            <tr>
                              <th>
                                <?php echo $this->lang->line('DPTechnology'); ?>
                              </th>
                              <td>
                                <div class="star-rat">
                                  <div class="Technology"></div>
                                  <?php if($countReviewRow->technology == 0 && $this->session->userdata('user_login')=='1'){
                                      $readOnly='false';
                                  }else{
                                      $readOnly='true';
                                  } ?>
                                  <script>
                                    $(function () {
                                      $(".Technology").rateYo({"starWidth": "15px",
                                        "ratedFill": "#03878A",
                                        "readOnly":<?php echo $readOnly; ?>,
                                        "fullStar": true,
                                        "rating" : <?php if($this->session->flashdata('technology')){ echo $this->session->flashdata('technology'); }else{  if(empty($countReviewRow->technology)) echo 0; else echo $countReviewRow->technology; } ?>,
                                        onSet: function (rating, rateYoInstance) {
                                          $('#Technology').append().val(rating);
                                        }
                                      });
                                    });
                                  </script>
                                </div>

                              </td>
                            </tr>
                          </table>
                         



                         </div>
                    </div>

                      </div>
                  <?php }else{ ?>
                      <div class="write-your-review clearfix">
                        <div class="clearfix">
                          <h3 class="title flip pull-left">
                            <span>
                              <?php echo $this->lang->line('reviewlogin'); ?>
                                <a id="signinopen" class="w-nav-link menu-li signIn-link" data-target="#loginModal" data-toggle="modal" href="javascript:;" style="max-width: 940px;">
                                <i class="fa fa-user"></i>

            <?php echo $this->lang->line('menuSignIn'); ?>


                                </a>
                            </span>
                          </h3>
                          <div class="border-divider"></div>
                        </div>
                        </div>
                  <?php } ?>
        <!-- ********** REVIEW Form details End ********** -->
            </div>
            <!-- Right side part -->
            <div class="w-col w-col-3 text-right">
            <?php 
                    $lat = $workDetail->work_latitude;
                    $long = $workDetail->work_longitude;
            if((!empty($workDetail->work_latitude) && !empty($workDetail->work_longitude)) && (is_numeric("$lat") && is_numeric("$long"))){
                    $displayMap='block';
                  }else{

                    $displayMap='none';
                  } ?>
              <div class="map-det clearfix" style="display:<?php echo $displayMap; ?>">
               <div class="top">
                  <h3 class="flip text-left clearfix"><?php echo $this->lang->line('DPOnMap'); ?></h3>
               </div>
               <div class="bottom">
                  <div id="map"></div>
               </div>
              </div>
              <div class="clearfix"></div>
              <div class="featured-doctor-det clearfix">
               <div class="top">
                  <h3 class="flip text-left clearfix"><?php echo $this->lang->line('searchFeaturedDoctor'); ?></h3>
               </div>
               <div class="bottom">
                 <ul class="clearfix">
                  <?php $j=1; foreach($featureArray as $feaureLocationRow){?>
                  <a href="<?php echo base_url()?>doctorProfile/index/<?php echo urlencode($feaureLocationRow->first_name); ?>-<?php echo urlencode($feaureLocationRow->last_name); ?>-<?php echo $feaureLocationRow->doctor_id;?>">
                   <li class="flip text-left">
                      <div class="img-box">
                        <?php if(empty($feaureLocationRow->photo)){ ?>
                          <img src="<?php echo base_url(); ?>uploads/user.jpg" alt="">
                        <?php }else{?>
                          <img src="<?php echo base_url(); ?>uploads/doctor_image/<?php echo $feaureLocationRow->photo; ?>" alt="">
                          <?php } ?>
                      </div>
                      <div class="details">
                          <h4 class="flip text-left clearfix">
                              <?php echo $feaureLocationRow->first_name .' '. $feaureLocationRow->last_name; ?>
                          </h4>
                           <?php $review_ratings =  get_rating_of_doctor($feaureLocationRow->doctor_id); // get particular doctor ratings?>
                          <div class="review-det">

                               <input type="hidden" value="<?php echo floatval($review_ratings->featureAverageScore); ?>"  id="frateViewVal<?php echo $j; ?>">
                              <div class="my-rating-<?php echo $j; ?>"></div>

<script>
$(function() {


  $(".my-rating-<?php echo $j; ?>").starRating({
    totalStars: 5,
    emptyColor: 'lightgray',
    activeColor: '#03878A',
    initialRating: $("#frateViewVal<?php echo $j; ?>").val(),
    strokeWidth: 0,
    useGradient: false,
      readOnly: true,
    callback: function(currentRating, $el){
    //  alert('rated ' +  currentRating);
      //console.log('DOM Element ', $el);
    }
  });


});
</script>
                         
                          <div class="flip text-right">
                            <span class="title">
                              <?php echo $review_ratings->doctorReview; ?>
                              <?php echo $this->lang->line('DPReview'); ?>
                            </span>
                          </div>
                           <div class="third-one">

                          <?php if($feaureLocationRow->speciality_id!=""){ ?>
                            <img alt="" src="<?php echo base_url(); ?>images/icon/speciality/<?php echo $feaureLocationRow->speciality_id; ?>.png">
                            <?php } ?>
                          </div>

                          </div>
                      </div>
                    </a>
                   </li>
                   <?php $j++; } ?>
                 </ul>
               </div>
            </div>
          </div>
        </div>
      </div>
  </section>
</div>
<div class="mt50 clearfix"></div>
<script type="text/javascript">


/*var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
  if(isMobile==true)
  {
      //$(".displaytel");
  }
  else{
    $('selector').attr('href','#');
  }*/

    <?php //if($doctorDetail->google_map_latitude!=""){ ?>
      var address = <?php echo json_encode($workDetail->address);?>;
  var locations = [
  ['',<?php echo $workDetail->work_latitude;?>,<?php echo $workDetail->work_longitude;?>,1],];
  var map = new google.maps.Map(document.getElementById('map'), {
  zoom: <?php if(!empty($workDetail->work_zoom)) { echo $workDetail->work_zoom; }else{ echo "10";}?>,

  center: new google.maps.LatLng(<?php echo $workDetail->work_latitude;?>, <?php echo $workDetail->work_longitude;?>),
  mapTypeId: google.maps.MapTypeId.ROADMAP});
  var infowindow = new google.maps.InfoWindow();
  var marker, i;
  for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
    map: map
  });
 /* google.maps.event.addListener(marker, 'click', (function(marker, i) {
  return function() {
    infowindow.setContent(locations[i][0]);
    infowindow.open(map, marker);
  }
})(marker, i));*/
}
  <?php //} ?>
</script>

<script>
$(document).ready(function () {
      /* Validation for Login   */
        $("#reviews-frm").validate({
            rules: {
                reviewSummary:"required",
                reviews:"required"

                },
            messages: {
                reviewSummary: {required: "Enter summary"},
                reviews: {required: "Enter review"}

            }
        });
    });
</script>
<script src="<?php echo base_url(); ?>/js/jquery.star-rating-svg.js"></script>


    <div id="modalPage" class="csspopup-overlay arabic-popup">
        <div class="modalContainer csspopup-popup mapContainer">
            <div class="csspopup-close" onclick="hideModal()">X</div>
                  <div class="popup">
                    <div class="link-popup clearfix">
                       <div id="dvMap"  style="height: 430px; width: 646px;"></div>                      
                    </div>                
                  </div>
        </div>
    </div>    
<script type="text/javascript">
   $(document).ready(function () {
     $('#modalPage').hide()
 });

 $('.map-det').on('click', function () {
     $('#modalPage').show();
     myFunction();
 });
 $('.map-det').dblclick(function(){
   $('#modalPage').show();
     myFunction();
 });

 $(document).mouseup(function (e) {
     var popup = $(".modalContainer");
     if (!$('.map-det').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
         $("#modalPage").hide();
     }
 });
 function myFunction() {
     
  var locations = [
  ['',<?php echo $workDetail->work_latitude;?>,<?php echo $workDetail->work_longitude;?>,1],];
  var map = new google.maps.Map(document.getElementById('dvMap'), {
  zoom: <?php if(!empty($workDetail->work_zoom)) { echo $workDetail->work_zoom; }else{ echo "10";}?>,

  center: new google.maps.LatLng(<?php echo $workDetail->work_latitude;?>, <?php echo $workDetail->work_longitude;?>),
  mapTypeId: google.maps.MapTypeId.ROADMAP});
  var infowindow = new google.maps.InfoWindow();
  var marker, i;
  for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
    map: map
  });
  /*google.maps.event.addListener(marker, 'click', (function(marker, i) {
  return function() {
    infowindow.setContent(locations[i][0]);
    infowindow.open(map, marker);
  }
})(marker, i));*/
}
           
        }
       function hideModal(){
        $("#modalPage").hide();
       }
</script>

<script>
    $(document).ready(function(){
        $('.refreshCaptcha').on('click', function(){
            $("#captImg").html('processing...').css({'width':'150px','height':'50px','color':'#000000'});
            $.get('<?php echo base_url().'workProfile/refresh'; ?>', function(data){
                $('#captImg').html(data);
            });
        });
    });
    </script>
<!-- end map popup  -->