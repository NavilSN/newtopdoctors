</div>
</div>
<!-- Start #footer  -->
<!--<div id="footer" class="clearfix sidebar-page right-sidebar-page">
    
    <p class="pull-left">Copyrights &copy; <?php echo date('Y'); ?> <a href="http://searchnative.com/" class="color-blue strong" target="_blank">Top Doctor</a>. All rights reserved.</p>    

</div>-->
<!-- End #footer  -->

<!-- / #wrapper --><!-- Back to top -->
<div id="back-to-top"><a href="#">Back to Top</a></div>
<style type="text/css">
    .panel.panel-default.toggle.panelMove.panelClose.panelRefresh {overflow: hidden;}
</style>
<!-- Javascripts -->
<script src="<?php echo base_url(); ?>assets/js/plugins/pace.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/select2.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/forms-validation.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/plugins/tables-data.js"></script>-->
<script src="<?php echo base_url(); ?>assets/js/jquery.toaster.js"></script>
<script src="<?php echo base_url(); ?>assets/js/multiselect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/summernote.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

<script>
$( document ).ready(function() {
    $('th').css('width', '');
});
<?php
$message = $this->session->flashdata('flash_message');
if ($message != '') {
    ?>
        $.toaster({
            priority: 'success',
            title: 'Success! ',
            message: '<?php echo $message; ?>',
            timeOut: 10000
        });
<?php } ?>

<?php $error=$this->session->flashdata('error');
if(!empty($error)){?>
   $.toaster({
            priority: 'danger',
            title: 'Error',
            message: '<?php echo $error; ?>',
            timeOut: 15000
        });
<?php } ?>
$(".toaster").css({'top':"62px"});
</script>
<?php include 'modal.php'; ?>
</body>
</html>

