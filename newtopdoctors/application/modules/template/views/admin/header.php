
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Top Doctor Admin Panel</title>
        <link rel="icon" type="image/ico" href="<?php echo base_url(); ?>images/favicon.ico">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Import google fonts - Heading first/ text second -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel=stylesheet type=text/css>
        <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel=stylesheet type=text/css>
        <!-- Css files -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/event_calendar/eventCalendar.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/event_calendar/eventCalendar_theme_responsive.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.min.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.min.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin-custom.css"/>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-select.min.css" rel="stylesheet" />
        

        <!-- JS Files -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/img/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/img/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/img/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/img/ico/apple-touch-icon-57-precomposed.png">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/ico/favicon.ico" type="image/png">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery.dataTables.css">
        <!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
        <meta name="msapplication-TileColor" content="#3399cc">
        <script>
            var base_url = '<?php echo base_url(); ?>';
        </script>
<style>                
body {top: 0 !important;}
</style>
</head>

    <body class="<?php echo $this->router->fetch_method(); ?>" style="min-height: 100%">
        <div id="header">
            <nav class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url().'admin/dashboard'; ?>">
                        <img src="<?php echo base_url(); ?>assets/img/admin-logo.png" alt="logo">
                    </a>
                </div>
                <div id="navbar-no-collapse" class="navbar-no-collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <!--Sidebar collapse button-->
                            <a href="#" class="collapseBtn leftbar"><i class="fa fa-bars" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-right usernav">
                        <li>
                            <div id="google_translate_element"></div>
                        </li>
                       <?php $adminRow=$this->db->get_where('admin',array('admin_id'=>$this->session->userdata('admin_id')))->row(); ?>
                        <li class="dropdown">
                             <a href="#" class="dropdown-toggle avatar" data-toggle="dropdown"><?php if(empty($adminRow->photo)){?><img src="<?php echo base_url(); ?>uploads/user.jpg" alt="" class="image"><?php }else{?><img src="<?php echo base_url(); ?>uploads/admin_image/<?php echo $adminRow->photo; ?>" alt="" class="image"> <?php }?>
                                <span class="txt"><?php echo $this->session->userdata('admin_firstname').' '.$this->session->userdata('admin_lastname'); ?></span> <b class="caret"></b>
                            </a>
                            <?php ?>
                              <ul class="dropdown-menu right">
                                <li class="menu">
                                    <ul>
                                        <li>
                                            <a href="<?php echo base_url().'admin/dashboard'; ?>"><i class="fa fa-dashboard" aria-hidden="true"></i>Home</a>
                                        </li>
                                        <li><a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_profile/<?php echo $this->session->userdata('admin_id'); ?>');">
                                                <i class="fa fa-user" aria-hidden="true"></i>Edit profile</a>
                                        </li>
                                        <li><a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_changepassword/<?php echo $this->session->userdata('admin_id'); ?>');">
                                                <i class="fa fa-user" aria-hidden="true"></i>Change Password</a>
                                        </li>
                                        <li><a href="<?php echo base_url(); ?>admin/logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url(); ?>admin/logout">
                                <i class="fa fa-sign-out" aria-hidden="true"></i><span class="txt">Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.nav-collapse -->
            </nav>
            <!-- /navbar -->
        </div>
        <!-- / #header -->
        <div id="wrapper">
            <!-- #wrapper --><!--Sidebar background-->
            <div id="sidebarbg" class="hidden-lg hidden-md hidden-sm hidden-xs"></div>
            <!--Sidebar content-->
            <div id="sidebar" class="page-sidebar hidden-lg hidden-md hidden-sm hidden-xs">
                <div class="shortcuts">
                    <ul>
                        <?php 
                        if($this->session->userdata('role_id')==1)
                        {
                            ?>
                            <li><a href="#" title="System Settings" class="tip">
                                    <i class="fa fa-life-ring" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li><a href="#" title="Database backup" class="tip">
                                    <i class="fa fa-database" aria-hidden="true"></i>
                                </a>
                            </li>
                        <?php } ?>
                        <li><a href="#" title="Profile" class="tip"> <!-- onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_profile/<?php echo $this->session->userdata('admin_id'); ?>');" -->
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End search -->
                <!-- Start .sidebar-inner -->
                <div class="sidebar-inner">
                    <!-- Start .sidebar-scrollarea -->
                    <div class="sidebar-scrollarea">
                        <div class="sidenav">
                            <div class="sidebar-widget mb0">
                                <h6 class="title mb0">Navigation</h6>
                            </div>
                            <!-- End .sidenav-widget -->
                            <div class="mainnav">
                                <ul>
                                    <?php $pages = array('AdminManagement','register','doctor','speciality','work','location','reviews','workreviews','newsletter');?>                                    
                                    <li class="hasSub<?php echo highlight_menu($page, $pages); ?>">
                                        <a href="#" class="<?php echo exapnd_not_expand_menu($page, $pages); ?>"><i class="icomoon-icon-arrow-down-2 s16 hasDrop"></i><i class="s16 fa fa-chain"></i>
                                            <span class="txt">Basic Management</span></a>
                                        <ul <?php echo navigation_show_hide_ul($page, $pages); ?>>
                                                <li>
                                                    <a id="link-AdminManagement" href="<?php echo base_url(); ?>admin/AdminManagement">
                                                        <i class="s16 icomoon-icon-signup"></i>
                                                        <span class="txt">Admin Management</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a id="link-register" href="<?php echo base_url(); ?>admin/user">
                                                        <i class="s16 icomoon-icon-signup"></i>
                                                        <span class="txt">User</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a id="link-doctor" href="<?php echo base_url(); ?>admin/doctor">
                                                        <i class="s16 icomoon-icon-user-plus-2"></i>
                                                        <span class="txt">Doctor</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a id="link-speciality" href="<?php echo base_url(); ?>admin/speciality">
                                                        <i class="s16 icomoon-icon-user-4"></i>
                                                        <span class="txt">Speciality</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a id="link-work" href="<?php echo base_url(); ?>admin/work">
                                                        <i class="s16 icomoon-icon-eyedropper"></i>
                                                        <span class="txt">Work</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a id="link-location" href="<?php echo base_url(); ?>admin/location">
                                                        <i class="s16 icomoon-icon-location-3"></i>
                                                        <span class="txt">Location</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a id="link-reviews" href="<?php echo base_url(); ?>admin/reviews">
                                                        <i class="s16 icomoon-icon-location-3"></i>
                                                        <span class="txt">Reviews</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a id="link-workreviews" href="<?php echo base_url(); ?>admin/workreviews">
                                                        <i class="s16 icomoon-icon-location-3"></i>
                                                        <span class="txt">Work Reviews</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a id="link-newsletter" href="<?php echo base_url(); ?>admin/newsletter">
                                                        <i class="s16 icomoon-icon-location-3"></i>
                                                        <span class="txt"> Newsletter </span>
                                                    </a>
                                                </li>
                                            </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- End sidenav -->
                        <!-- End .sidenav-widget -->
                    </div>
                    <!-- End .sidebar-scrollarea -->
                </div>
                <!-- End .sidebar-inner -->
            </div>
            <!-- End #sidebar --><!--Sidebar background-->
            <div id="content" class="page-content clearfix">
                <div class="contentwrapper">
                    <!--Content wrapper-->
                    <div class="heading">
                        <h3><?php echo $title; ?></h3>
                        <div class="resBtnSearch"><a href=#><span class="s16 icomoon-icon-search-3"></span></a></div>
                        <div class="search_box">
                        </div>                           
                        <?php echo create_breadcrumb(); ?>
                        <?php echo set_active_menu($page); ?>
                    </div>
                    <!-- End  / heading-->
 
     
