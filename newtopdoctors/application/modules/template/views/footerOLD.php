<?php 
$this->load->model('home/Country_master_model');
$country=$this->Country_master_model->get_all('country_name');
?>
<div class="footer clearfix">
    <div class="footer-top-det">
        <div class="container"> 
              
                <div class="w-col w-col-4">
                    <div class="footer-logo">
                        <img src="<?php echo base_url(); ?>images/footer-logo.jpg" class="img-responsive" alt="Logo">
                    </div>
                    <p>
                      <?php echo $this->lang->line('footerContent'); ?>
                    </p>
                    <br>
                    <address>
                      <i class="fa fa-home p10r"></i> <span>
                      <?php echo $this->lang->line('footerAddress'); ?>
                      </span>
                      </address>
                      <br>
                      <a href="#"><i class="fa fa-envelope p10r"></i><?php echo $this->lang->line('footerEmail'); ?></a>
                      <br>
                      <a href="#"><i class="fa fa-phone p10r"></i> <?php echo $this->lang->line('footerPhone'); ?></a>
                    
                </div>
             
              
                <div class="w-col w-col-4">
                    <div class="footer-logo">
                        <img src="<?php echo base_url(); ?>images/footer-logo.jpg" class="img-responsive" alt="Logo">
                    </div>
                    <p>
                      <?php echo $this->lang->line('footerContentB'); ?>
                    </p>
                    <br>
                    <address>
                      <i class="fa fa-home p10r"></i> <span>
                        <?php echo $this->lang->line('footerAddressB'); ?>
                      </span>
                      </address>
                      <br>
                      <a href="#"><i class="fa fa-envelope p10r"></i> <?php echo $this->lang->line('footerEmailB'); ?></a>
                      <br>
                      <a href="#"><i class="fa fa-phone p10r"></i> <?php echo $this->lang->line('footerPhoneB'); ?></a>
                    
                </div>
           
            
                <div class="w-col w-col-4">
                    <h3 class="title-footer">
                      <?php echo $this->lang->line('footerUseFullLink'); ?>
                    </h3>
                    <div class="border-buttom"></div>
                    <ul class="footer-links clearfix">
                      <li>
                        <a href="#"> <i class="fa fa-play p5r"></i> <?php echo $this->lang->line('footerMenuAboutUs'); ?> </a>
                      </li>
                      <li>
                        <a href="#"> <i class="fa fa-play p5r"></i><?php echo $this->lang->line('footerMenuLatestBlog'); ?></a>
                      </li>
                      <li>
                        <a href="#"> <i class="fa fa-play p5r"></i><?php echo $this->lang->line('footerMenuTermCondition'); ?></a>
                      </li>
                      <li>
                        <a href="#"> <i class="fa fa-play p5r"></i> <?php echo $this->lang->line('footerMenuPrivacyPolicy'); ?> </a>
                      </li>
                      <li>
                        <a href="#"> <i class="fa fa-play p5r"></i> <?php echo $this->lang->line('footerMenuContactUS'); ?> </a>
                      </li>
                    </ul>
                    
                </div>
        
          
          </div>   
    </div>
    <div class="footer-bottom-det">
        <?php echo $this->lang->line('footerLine'); ?><i class="fa fa-copyright"></i> <?php echo $this->lang->line('footerCopyRight'); ?>
    </div>
</div>
  <!-- -Login Modal -->
  <div class="modal fade loginModal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content login-modal">
            <div class="modal-body">
              <div class="text-center">
                <div role="tabpanel" class="login-tab">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="signintab active"><a id="signin-taba" href="#loginfrm" aria-controls="loginfrm" role="tab" data-toggle="tab"><?php echo $this->lang->line('Login'); ?></a></li>
                  <li role="presentation" class="signuptab"><a id="signup-taba" href="#registerFrm" aria-controls="registerFrm" role="tab" data-toggle="tab"><?php echo $this->lang->line('Register'); ?></a></a></li>
                  <!-- <li role="presentation"><a id="forgetpass-taba" href="#forget_password" aria-controls="forget_password" role="tab" data-toggle="tab">Forget Password</a></li> -->
                </ul>
            
                <!-- Tab panes -->
              <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active text-center" id="loginfrm">
                    <?php $errorLogin = $this->session->flashdata('errorLogin');
                          if ($errorLogin != '') { ?>
                              <span id="login_fail" class="response_error" style="color:red;"><?php echo $this->session->flashdata('errorLogin'); ?></span>
                    <?php } ?>
                    <div class="clearfix"></div>
                    <form name="userForm" id="loginform" action="<?php echo base_url(); ?>home/login_process" method="post" novalidate>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-at"></i></div>
                            <input type="text" class="form-control" id="loginemail" name="loginemail" placeholder="<?php echo $this->lang->line('loginEmail'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                            <input type="password" class="form-control" id="loginpassword" name="loginpassword" placeholder="<?php echo $this->lang->line('loginPassword'); ?>">
                        </div>
                        <span class="help-block has-error" id="loginpassword-error"></span>
                    </div>                      
                      <div class="login-modal-footer">
                          <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">                         
                                  <a class="forgetpass-tab" href="javascript:void(0);" data-toggle="modal" data-target="#forgotpasswordModel"> <?php echo $this->lang->line('loginForgotPass'); ?> </a>
                                </div>                                
                          </div>
                      </div>
                      <button type="submit" id="login_btn" class="btn btn-block bt-login" data-loading-text="Signing In...."><?php echo $this->lang->line('loginSubmitBTN'); ?></button>
                            <!--    <div class="col-xs-4 col-sm-4 col-md-4">
                                  <i class="fa fa-check"></i>
                                  <a href="javascript:;" class="signup-tab"> Sign Up </a>
                                </div> -->
                      <div class="login-modal-footer">
                          <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">                         
                                  <div class="facebook-link">
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" class=""> <i class="fa fa-facebook"></i>
                                    <span class="facebook"><?php echo $this->lang->line('loginFacebook'); ?></span> </a>
                                  </div>
                                  <div class="guest-link">
                                    <a href="javascript:void(0)" class=""><?php echo $this->lang->line('loginGuest'); ?> </a>
                                  </div>
                                </div>   
                                <div id="fb-root"></div>
                             
                          </div>
                      </div>

                  </form>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="registerFrm">                  
                      <span id="registration_fail" class="response_error" style="display: none;">Registration failed, please try again.</span>
                    <div class="clearfix"></div>
                    <form class="clearfix" id="registerform" name="registerform" action="<?php echo base_url(); ?>home/signup_process" method="post">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-at"></i></div>
                            <input type="text" class="form-control" id="firstname" placeholder="<?php echo $this->lang->line('regFirstName'); ?>" name="firstname">
                        </div>
                        <span class="help-block has-error" data-error='0' id="firstname-error"></span>
                      </div>
                      <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-at"></i></div>
                            <input type="text" class="form-control" id="lastname" placeholder="<?php echo $this->lang->line('regLastName'); ?>" name="lastname">
                        </div>
                        <span class="help-block has-error" data-error='0' id="lastname-error"></span>
                      </div>
                      <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-at"></i></div>
                            <input type="text" class="form-control" id="email_id" placeholder="<?php echo $this->lang->line('regEmail'); ?>" name="email">
                        </div>
                        <span class="help-block has-error" id="email-error"></span>
                    </div>
                      <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-mobile"></i></div>
                            <input type="text" class="form-control" id="remail" placeholder="<?php echo $this->lang->line('regMobileNumber'); ?>" name="mobile_no" maxlength="13">
                        </div>
                        <span class="help-block has-error" data-error='0' id="remail-error"></span>
                      </div>
                       <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                            <input type="password" class="form-control" id="password" placeholder="<?php echo $this->lang->line('regPassword'); ?>" name="password">
                        </div>
                        <span class="help-block has-error" id="password-error"></span>
                    </div>
                       <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                            <input type="password" class="form-control" id="c_password" placeholder="<?php echo $this->lang->line('regCPassword'); ?>" name="c_password">
                        </div>
                        <span class="help-block has-error" id="c_password-error"></span>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-at"></i></div>
                            <input type="text" class="form-control" id="address" placeholder="<?php echo $this->lang->line('regAddress'); ?>" name="address">
                        </div>
                      </div>
                    <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-globe"></i></div>
                          <select class="form-control" name="country_id" id="country_id">
                          <option value=""><?php echo $this->lang->line('regCountry'); ?></option>
                          <?php foreach ($country as $row_country) {?>
                             <option value="<?php echo $row_country->country_id; ?>"><?php echo $row_country->country_name; ?></option>
                          <?php  } ?>
                        </select>
                        </div>
                      </div>
                  <div class="checkbox-det">  
                    <div class="login-modal-footer">
                    <div class="form-group text-left">                     
                      
                      <label class="checkbox-1 checknl" for="c1"><span class="checkboxchecknl"></span><?php echo $this->lang->line('regNewLatter'); ?></label>
                      <input type="checkbox" id="c1" name="c1" class="form-control" value="1"/>
                    </div>
                    </div>
                    <div class="login-modal-footer">
                    <div class="form-group text-left">
                      <label class="checkbox-1 checktc" for="c2"><span class="checkboxchecktc"></span><?php echo $this->lang->line('regTermCondition'); ?></label>
                      <input type="checkbox" id="c2" name="c2" class="form-control" title="<?php echo $this->lang->line('validRegTermCondition'); ?>" required/>
                    </div>
                    </div> 
                  </div>
                    <div class="clearfix"></div>
                      <button type="submit" id="register_btn" class="btn btn-block bt-login" data-loading-text="Registering...."><?php echo $this->lang->line('regBTN'); ?></button>                
                    <div class="login-modal-footer last">
                          <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">                         
                                  <div class="facebook-link">
                                    <a href="javascript:;" class=""> <i class="fa fa-facebook"></i>
                                    <span class="facebook"><?php echo $this->lang->line('regFacebook'); ?></span> </a>
                                  </div>                                  
                                </div>                                
                          </div>
                    </div>
                  </form>
                  </div>
                  <div role="tabpanel" class="tab-pane text-center" id="forget_password">
                    &nbsp;&nbsp;
                      <span id="reset_fail" class="response_error" style="display: none;"></span>
                    <div class="clearfix"></div>
                    <form>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                            <input type="text" class="form-control" id="femail" placeholder="Email">
                        </div>
                        <span class="help-block has-error" data-error='0' id="femail-error"></span>
                      </div>
                      
                      <button type="button" id="reset_btn" class="btn btn-block bt-login" data-loading-text="Please wait...."><?php echo $this->lang->line('regForgotPass'); ?></button>
                    <div class="clearfix"></div>
                    <div class="login-modal-footer">
                        <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <i class="fa fa-lock"></i>
                          <a href="javascript:;" class="signin-tab"> <?php echo $this->lang->line('regSignIn'); ?> </a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <i class="fa fa-check"></i>
                          <a href="javascript:;" class="signup-tab"> <?php echo $this->lang->line('regSignUp'); ?> </a>
                        </div>
                      </div>
                      </div>
                  </form>
                  </div>
                </div>
            </div>
            </div>
            </div>
        </div>
     </div>
  </div>

<div class="modal fade loginModal" id="forgotpasswordModel" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content login-modal">
            <div class="modal-body">
              <div class="text-center">
                <div role="tabpanel" class="login-tab">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" style="width:100%;" class="active"><a id="forgotpasswordA" href="#forgotpassfrm" aria-controls="forgotpass" role="tab" data-toggle="tab"><?php echo $this->lang->line('ForgotPassword'); ?></a></li>
                </ul>
         <div role="tabpanel" class="tab-pane active text-center" id="forgotpassfrm">
                    <?php $forgotEmailNotFound = $this->session->flashdata('forgotEmailNotFound');
                          if ($forgotEmailNotFound != '') { ?>
                              <span id="login_fail" class="response_error" style="color:red;"><?php echo $this->session->flashdata('forgotEmailNotFound'); ?></span>
                    <?php } ?>
                    <div class="clearfix"></div>
                    <form name="userForm" id="loginform" action="<?php echo base_url(); ?>home/forgot_password" method="post" novalidate>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-at"></i></div>
                            <input type="text" class="form-control" id="forgotemail" name="forgotemail" placeholder="E-mail">
                        </div>
                    </div>
                
                      <button type="submit" id="login_btn" class="btn btn-block bt-login" data-loading-text="Signing In...."><?php echo $this->lang->line('forgotPassSubmitBTN'); ?></button>
                           
                  </form>
                </div>
                </div>
              </div>
            </div>
          </div>
     </div>
  </div>
  <!-- - Login Model Ends Here -->
  
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.toaster.js"></script>
    
    
     <script type="text/javascript">
  window.fbAsyncInit = function() {
    //Initiallize the facebook using the facebook javascript sdk
     FB.init({ 
       appId:'<?php echo $this->config->item('appID'); ?>', // App ID 
     cookie:true, // enable cookies to allow the server to access the session
       status:true, // check login status
     xfbml:true, // parse XFBML
     oauth : true //enable Oauth 
     });
   };
   //Read the baseurl from the config.php file
   (function(d){
           var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement('script'); js.id = id; js.async = true;
           js.src = "//connect.facebook.net/en_US/all.js";
           ref.parentNode.insertBefore(js, ref);
         }(document));
  //Onclick for fb login
 $('.facebook').click(function(e) {
    FB.login(function(response) {
    if(response.authResponse) {
      parent.location ='<?php echo base_url(); ?>home/fblogin'; //redirect uri after closing the facebook popup
    }
 },{scope: 'email,publish_actions'}); //permissions for facebook
});
   </script>       
    <script>
      $(document).ready(function(){
        $(document).on('click','.signup-tab',function(e){
           e.preventDefault();
           $('#signup-taba').tab('show');
        }); 
  
        $(document).on('click','.signin-tab',function(e){
           e.preventDefault();
           $('#signin-taba').tab('show');
        });
        
        $(document).on('click','.forgetpass-tab',function(e){
           e.preventDefault();
           $('#forgetpass-taba').tab('show');
        });

        $(document).on('click','#signinopen',function(e){
           e.preventDefault();
           $('#signin-taba').tab('show');
           $('#loginfrm').addClass('active');
           $('#registerFrm').removeClass('active');
        });

        $(document).on('click','#signupopen',function(e){
           e.preventDefault();
           $('#signup-taba').tab('show');
           $('#registerFrm').addClass('active');
           $('#loginfrm').removeClass('active');
        });

        $(document).on('click','.forgetpass-tab',function(e){
           e.preventDefault();
           $('#forgetpass-taba').tab('show');
        });

        $("label.checknl").click(function(){
        $("span.checkboxchecknl").toggleClass("active");
        });

        $("label.checktc").click(function(){
        $("span.checkboxchecktc").toggleClass("active");
        });
$('.modal-backdrop').css('min-height', '900px');
      }); 
    </script>

    <?php $errorLogin = $this->session->flashdata('errorLogin');
        if ($errorLogin != '') { ?>
            <script>
             $('#loginModal').modal('show');
            </script>
    <?php } ?>

    <?php $forgotEmailNotFound = $this->session->flashdata('forgotEmailNotFound');
        if ($forgotEmailNotFound != '') { ?>
            <script>
             $('#forgotpasswordModel').modal('show');
            </script>
    <?php } ?>


<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("mobile_no", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid contact no.');
        jQuery.validator.addMethod("email_id", function (value, element) {
            return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
        }, 'Please enter a valid email address.');

        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');

        /* Validation for Login   */
        $("#loginform").validate({
            rules: {
                loginemail:"required",
                loginpassword:"required" 
                },
            messages: {
                loginemail: {required: "Enter email"},
                loginpassword: {required: "Enter password"}
            }
        });

        
        $("#registerform").validate({
          ignore: ':hidden:not(:checkbox)',
            rules: {

                firstname:{    required: true,
                            character: true,
                        },
                lastname: {   required: true,
                            character: true,
                        },
                email:
                        {
                            required: true,
                            email: true,
                            remote: {
                                url: "<?php echo base_url(); ?>home/check_user_email",
                                type: "post",
                                data: {
                                    email: function () {
                                        return $("#email_id").val();
                                    },
                                }
                            }
                        },
                mobile_no:{
                            required: true,
                            maxlength: 14,
                            mobile_no: true,
                            minlength: 10,
                        },
                 password:{required:true,minlength: 6},
                 c_password:{required:true,
                             equalTo: "#password", 
                            },
                 address:"required",        
                 country_id:"required",
                },
            messages: {
                firstname: {   required: "<?php echo $this->lang->line('validRegFirstname'); ?>",
                            character: "<?php echo $this->lang->line('validRegfirstValid'); ?>",
                        },
                lastname:{    required: "<?php echo $this->lang->line('validRegLastName'); ?>",
                            character: "<?php echo $this->lang->line('validRegLastvalid'); ?>",
                        },
                email: {
                        required: "<?php echo $this->lang->line('validRegEmail'); ?>",
                        email: "<?php echo $this->lang->line('validRegEmailvalid'); ?>",
                        remote: "<?php echo $this->lang->line('validRegEmailexist'); ?>",
                },
                mobile_no:{  required: "<?php echo $this->lang->line('validRegMobile'); ?>",
                            maxlength: "<?php echo $this->lang->line('validRegMobilemax'); ?>",
                            mobile_no: "<?php echo $this->lang->line('validRegMobilevalid'); ?>",
                            minlength: "<?php echo $this->lang->line('validRegMobileMin'); ?>",
                        },
                password:{required: "<?php echo $this->lang->line('validRegPassword'); ?>",
                          minlength: "<?php echo $this->lang->line('validRegPasswordMin'); ?>",},
                c_password:{required: "<?php echo $this->lang->line('validRegCPassword'); ?>",
                            equalTo:"<?php echo $this->lang->line('validRegPasswordEqual'); ?>"},
                address: "<?php echo $this->lang->line('validRegAddress'); ?>",
                country_id: "<?php echo $this->lang->line('validRegCountry'); ?>",
            }
        });
    });

<?php
$message = $this->session->flashdata('flash_message');
if ($message != '') {
    ?>
        $.toaster({
            priority: 'success',
            title: '<?php echo $this->lang->line("success"); ?>',
            message: '<?php echo $message; ?>',
            timeOut: 5000
        });
<?php }
$error=$this->session->flashdata('error');
if(!empty($error)){?>
   $.toaster({
            priority: 'danger',
            title: 'Error',
            message: '<?php echo $error; ?>',
            timeOut: 5000
        });
<?php } ?>
$(".toaster").css({'top':"62px",'width':'100%'});
</script>
<!--For signup Popup background height  -->
<script>
$( window ).on( 'mousemove mouseup', function() {
    var $modal     = $('.modal-dialog')
      , $backdrop  = $('.modal-backdrop')
      , el_height  = $modal.innerHeight();
    $backdrop.css({
        height: el_height + 20,
        minHeight: '100%',
        margin: 'auto'
    });
    $modal.css({
        padding: '4px',
        maxWidth: '900px',
        margin: '10px auto'
    });
});
</script>
<script src="<?php echo base_url(); ?>js/normal.js" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>
</html>