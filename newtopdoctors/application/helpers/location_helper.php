<?php


// Get all parents for admin in english
function getParentsAdmin() {
    $CI = & get_instance();
    
    $siteLang = $CI->session->userdata('site_lang');
 
        $result = $CI->db->select()
        ->from('location_master')
        ->where(array(
            'parent_id' => 0,
            'status' =>'1'
        ))
        ->order_by('name_en', 'ASC')
        ->get();

    return $result->result();
 
}    


function getChildsAdmin($id = "") {
        $CI = & get_instance();
        $parents = getParent();
        echo "<select name='location' class='form-control dropdown' id='select-location'>";
        echo "<option value=0>Location</option>";
        foreach ($parents as $row) {
            $selected = "";
            if ($row->location_id == $id)
                $selected = 'selected="selected"';
           
            echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name_en . "</option>";
        
            
           
             $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name_en','asc')
                ->get()
                ->result();   
           


                if (count($getChilds))
                    echo "<option disabled>-----</option>";
                foreach ($getChilds as $child) {
                    $selected = "";
                    if ($child->location_id == $id)
                        $selected = 'selected="selected"';
                   
                        echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name_en ."</option>";
                   
                }
                if (count($getChilds))
                    echo "<option disabled>----------</option>";
        }
        echo "</select>";
}


// Get all parents
function getParent() {
    $CI = & get_instance();
    
    $siteLang = $CI->session->userdata('site_lang');
    if($siteLang=='arabic'){
        $result = $CI->db->select()
            ->from('location_master')
            ->where(array('parent_id' => 0, 'status' =>'1'))
            ->order_by('name', 'ASC')
            ->get();

        return $result->result();
    }else{    
        $result = $CI->db->select()
        ->from('location_master')
        ->where(array(
            'parent_id' => 0,
            'status' =>'1'
        ))
        ->order_by('name_en', 'ASC')
        ->get();

    return $result->result();
    }
}    


function getParentIdFromName($parent = "", $lang = 'en') {
    $CI = & get_instance();
    if ($lang == 'ar') {
        $result = $CI->db->select()
                ->from('location_master')
                ->where('name', $parent)
                ->get();
        return $result->row();
    } else {
        $result = $CI->db->select()
                ->from('location_master')
                ->where('name_en', $parent)
                ->get();
        return $result->row();
    }
    
}
function getChildInfo($parentId = "") {
    $CI = & get_instance();

    $result = $childs = $CI->db->select()
        ->from('location_master')
        ->where('parent_id', $parentId)
        ->get();

    return $result->result();
}
function getChilds($id = "") {
        $CI = & get_instance();
        $parents = getParent();
        echo "<select name='location' class='form-control dropdown' id='select-location'>";
        echo "<option value=0>".$CI->lang->line('AddDoctorPLocation')."</option>";
        foreach ($parents as $row) {
            $selected = "";
            if ($row->location_id == $id)
                $selected = 'selected="selected"';
            $siteLang = $CI->session->userdata('site_lang');
    if($siteLang=='arabic'){
           echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name . "</option>";
    }else{
            echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name_en . "</option>";
        }
            
             if($siteLang=='arabic'){
            $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name','asc')
                ->get()
                ->result();
            }
            else{
             $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name_en','asc')
                ->get()
                ->result();   
            }


                if (count($getChilds))
                    echo "<option disabled>-----</option>";
                foreach ($getChilds as $child) {
                    $selected = "";
                    if ($child->location_id == $id)
                        $selected = 'selected="selected"';
                    if($siteLang=='arabic'){
                    echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name ."</option>";
                    }else{
                        echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name_en ."</option>";
                    }
                }
                if (count($getChilds))
                    echo "<option disabled>----------</option>";
        }
        echo "</select>";
}
function getChildsDoctor($id = "") {
        $CI = & get_instance();
        $parents = getParent();
        echo "<select name='location' class='form-control dropdown' id='select-location'>";
        echo "<option value=''>".$CI->lang->line('AddDoctorPLocation')."</option>";
        foreach ($parents as $row) {
            $selected = "";
            if ($row->location_id == $id)
                $selected = 'selected="selected"';
            $siteLang = $CI->session->userdata('site_lang');
    if($siteLang=='arabic'){
           echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name . "</option>";
    }else{
            echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name_en . "</option>";
        }
            
             if($siteLang=='arabic'){
            $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name','asc')
                ->get()
                ->result();
            }
            else{
             $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name_en','asc')
                ->get()
                ->result();   
            }


                if (count($getChilds))
                    echo "<option disabled>-----</option>";
                foreach ($getChilds as $child) {
                    $selected = "";
                    if ($child->location_id == $id)
                        $selected = 'selected="selected"';
                    if($siteLang=='arabic'){
                    echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name ."</option>";
                    }else{
                        echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name_en ."</option>";
                    }
                }
                if (count($getChilds))
                    echo "<option disabled>----------</option>";
        }
        echo "</select>";
}
function getChildsWork($id = "") {
        $CI = & get_instance();
        $parents = getParent();
        echo "<select id='locationwork' name='locationwork' class='form-control dropdown' >";
        echo "<option value=''>".$CI->lang->line('AddDoctorPLocation')."</option>";
        foreach ($parents as $row) {
            $selected = "";
            if ($row->location_id == $id)
                $selected = 'selected="selected"';
            $siteLang = $CI->session->userdata('site_lang');
    if($siteLang=='arabic'){
           echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name . "</option>";
    }else{
            echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name_en . "</option>";
        }
            
             if($siteLang=='arabic'){
            $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name','asc')
                ->get()
                ->result();
            }
            else{
             $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name_en','asc')
                ->get()
                ->result();   
            }


                if (count($getChilds))
                    echo "<option disabled>-----</option>";
                foreach ($getChilds as $child) {
                    $selected = "";
                    if ($child->location_id == $id)
                        $selected = 'selected="selected"';
                    if($siteLang=='arabic'){
                    echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name ."</option>";
                    }else{
                        echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name_en ."</option>";
                    }
                }
                if (count($getChilds))
                    echo "<option disabled>----------</option>";
        }
        echo "</select>";
}


function getChildsClinic($id = "") {
        $CI = & get_instance();
        $parents = getParent();
        echo "<select id='locationClinic' name='locationClinic' class='form-control dropdown' >";
        echo "<option value=''>".$CI->lang->line('AddDoctorPLocation')."</option>";
        foreach ($parents as $row) {
            $selected = "";
            if ($row->location_id == $id)
                $selected = 'selected="selected"';
            $siteLang = $CI->session->userdata('site_lang');
    if($siteLang=='arabic'){
           echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name . "</option>";
    }else{
            echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name_en . "</option>";
        }
            
             if($siteLang=='arabic'){
            $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name','asc')
                ->get()
                ->result();
            }
            else{
             $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name_en','asc')
                ->get()
                ->result();   
            }


                if (count($getChilds))
                    echo "<option disabled>-----</option>";
                foreach ($getChilds as $child) {
                    $selected = "";
                    if ($child->location_id == $id)
                        $selected = 'selected="selected"';
                    if($siteLang=='arabic'){
                    echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name ."</option>";
                    }else{
                        echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name_en ."</option>";
                    }
                }
                if (count($getChilds))
                    echo "<option disabled>----------</option>";
        }
        echo "</select>";
}

function getChildsLab($id = "") {
        $CI = & get_instance();
        $parents = getParent();
        echo "<select id='locationLab' name='locationLab' class='form-control dropdown' required >";
        echo "<option value=''>".$CI->lang->line('AddDoctorPLocation')."</option>";
        foreach ($parents as $row) {
            $selected = "";
            if ($row->location_id == $id)
                $selected = 'selected="selected"';
            $siteLang = $CI->session->userdata('site_lang');
    if($siteLang=='arabic'){
           echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name . "</option>";
    }else{
            echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name_en . "</option>";
        }
            
             if($siteLang=='arabic'){
            $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name','asc')
                ->get()
                ->result();
            }
            else{
             $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name_en','asc')
                ->get()
                ->result();   
            }


                if (count($getChilds))
                    echo "<option disabled>-----</option>";
                foreach ($getChilds as $child) {
                    $selected = "";
                    if ($child->location_id == $id)
                        $selected = 'selected="selected"';
                    if($siteLang=='arabic'){
                    echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name ."</option>";
                    }else{
                        echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name_en ."</option>";
                    }
                }
                if (count($getChilds))
                    echo "<option disabled>----------</option>";
        }
        echo "</select>";
}

function getChildsLocation($id = "") {
        $CI = & get_instance();
        $parents = getParent();
        echo "<select name='parent' class='form-control dropdown' id='parent'>";
        echo "<option value=0>".$CI->lang->line('AddDoctorPLocation')."</option>";
        foreach ($parents as $row) {
            $selected = "";
            if ($row->location_id == $id)
                $selected = 'selected="selected"';
            $siteLang = $CI->session->userdata('site_lang');
    if($siteLang=='arabic'){
           echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name . "</option>";
    }else{
            echo "<option " . $selected . " value=" . $row->location_id . ">" . $row->name_en . "</option>";
        }
            
             if($siteLang=='arabic'){
            $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name','asc')
                ->get()
                ->result();
            }
            else{
             $getChilds = $CI->db->select()
                ->from('location_master')
                ->where(array(
                    'parent_id' => $row->location_id,
                    'status'    =>'1'
                ))
                ->order_by('name_en','asc')
                ->get()
                ->result();   
            }


                if (count($getChilds))
                    echo "<option disabled>-----</option>";
                foreach ($getChilds as $child) {
                    $selected = "";
                    if ($child->location_id == $id)
                        $selected = 'selected="selected"';
                    if($siteLang=='arabic'){
                    echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name ."</option>";
                    }else{
                        echo "<option " . $selected . " value=" . $child->location_id . ">&nbsp;&nbsp;&nbsp;" . $child->name_en ."</option>";
                    }
                }
                if (count($getChilds))
                    echo "<option disabled>----------</option>";
        }
        echo "</select>";
}


function getChildLocation($id)
{
    $CI = &get_instance();
    $CI->db->select('location_id');
    $CI->db->where('status','1');
    $CI->db->where('parent_id',$id);
    $result = $CI->db->get('location_master')->result();    
    $values[] = $id;
    foreach ($result as $key => $val):
        $values[] = $val->location_id;
    endforeach;
    
    return $values;
    
}

function getChildLocations($id)
{
    $CI = &get_instance();
    $CI->db->select('lms.location_id');
    $CI->db->where('lms.status','1');
    $CI->db->where('lms.parent_id',$id);
    $result = $CI->db->get('location_master lms')->result();    
    $values[] = $id;
    foreach ($result as $key => $val):
        $values[] = $val->location_id;
    endforeach;
    
    return $values;
    
}

function getReview($id)
{
    $CI = &get_instance();
    $CI->db->select('average_score');
    $CI->db->where('doctor_id',$id);
    return $CI->db->get('doctors_rating')->result();
}
