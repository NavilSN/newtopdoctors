<?php
  ini_set('memory_limit', '-1');
  ini_set('max_execution_time', '-1');
  function strposa($haystack, $needle, $offset=0) {
    if(!is_array($needle)) $needle = array($needle);
    foreach($needle as $query) {
      if(mb_strpos($haystack, $query, $offset) !== false) return true;
    }
    return false;
  }

  function substr_count_a( $haystack, $needle ) {
    $count = 0;
    foreach ($needle as $substring) {
      $count += mb_substr_count( $haystack, $substring);
    }
    return $count;
  }

  function mb_substr_replace($original, $replacement, $position, $length) {
    $startString = mb_substr($original, 0, $position, "UTF-8");
    $endString = mb_substr($original, $position + $length, mb_strlen($original), "UTF-8");
    $out = $startString . $replacement . $endString;
    return $out;
  }

  function findword($word1) {
    $wordmain = explode(" ", $word1);
    mb_internal_encoding("UTF-8");
    $ls[0] = array(
      'ا',
      'أ',
      'إ',
      'آ'
    );
    $ls[1] = array(
      'ا',
       'ى',
       'ي'
     );
     $ls[2] = array(
       'هـ',
       'ة',
       'ه'
     );
     $ls[3] = array(
       'ئـ',
       'ئ'
     );
     $ls[4] = array(
       'ؤ',
       'و'
     );
     $loop=0;
     $count_similiars_in_word=0;
     /*echo "<pre>";*/
   $p=0;
     foreach ($wordmain as $word) {
      unset($new_word_array);
       $new_word_array[] = $word;

       foreach($ls as $lskey => $l) {
         if(strposa($word, $l)) {
           $loop++;
           $count_similiars_in_word=substr_count_a($word, $l);
           foreach($l as $replace_l) {
             foreach($l as $second_replace_l) {
               foreach($new_word_array as $new_word) {
                 $new_word_array[]= str_replace($replace_l, $second_replace_l, $new_word );
                 $new_word_array=array_unique($new_word_array);
               }
             }
           }
            for($i1=0; $i1<$count_similiars_in_word; $i1++) {
              for($i2=0;$i2<$count_similiars_in_word; $i2++) {
                $current_l=current($l);
                next($l);
                $start_pos=-1;
                for($i3=0; $i3<$count_similiars_in_word; $i3++) {
                  if(!empty($current_l)) {
                    $start_pos=mb_strpos($word, $current_l, $start_pos+1);
                    if($start_pos) {
                      foreach($l as $replace_l) {
                        //echo count($new_word_array)."<br>";
                        //echo "test<br>";
                        $new_word_array=array_unique($new_word_array);
                        //echo count($new_word_array)."<br>";
                        foreach($new_word_array as $n_word) {
                          $new_word_array[]=mb_substr_replace($n_word, $replace_l, $start_pos, 1);
                        }
                      }
                    }
                  }
                }
              }
            }
         }
       }
      $final_array[$p] = array_unique($new_word_array);
      $p++;
    }
 /*   echo "</pre>";*/
    $array = $final_array;
    if (count($new_word_array) > 0) {
      /*echo "<pre>";*/
      $n = 0;
      foreach ($array as $arr) {
        if ($n == 0) {
          $fa = $arr;
        } else {
          unset($combine);
          foreach ($fa as $str) {
            foreach ($arr as $str1) {
              $combine[] = $str . " " . $str1;
            }
          }
          unset($fa);
          $fa = $combine;
        }
        $n++;
      }
      $temp = array_unique($fa);
      /*print_r($temp);
      die();*/

      return $temp;
    } else {
      return false;
    }
  }

  
/* Remove all represntaive character from string  
    * @param $search_string 
    */

    function substitute_word($search_string=""){

              $array  = array('ا','أ','إ','آ','ى','ي','هـ','ة','ه','ئـ','ئ','ؤ','و',' ');

              for($i=0;$i<=count($array);$i++)
                {
                  $search_string = str_replace($array[$i],'', $search_string);    
                }
              return $search_string;
    }

    /**
    @param $language change site lang
    set session of language
    */
    function switchLangs($language = "") {   
              $CI =&get_instance();     
              $language = ($language != "") ? $language : "english";
              $CI->session->set_userdata('site_lang', $language);                 
    }

  //findword("الطبية - فرع المعادى");
?>
