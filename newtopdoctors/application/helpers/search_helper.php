<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/**
 * 
 * @param id $major_id
 * @return string
 */
    function get_major_name($major_id)
    {
        $CI = &get_instance();
       
        $siteLang=$CI->session->userdata('site_lang');
         $CI->db->where('specialties_id',$major_id); 
        if($siteLang=='arabic'){
           $query = $CI->db->get('specialty_master')->row();
           return $query->name;
        }else{
         $query= $CI->db->get('specialty_master')->row();            
          return $query->name_en;
        }
        
    }
    
    /**
     * Features doctor array
     */
    function featuresArray()
    {
        $CI = &get_instance();
        /*$siteLang=$CI->session->userdata('site_lang');
          $CI->db->select('AVG(reputation  + clinic + availability + approachability + technology )/5 as featureAverageScore,count(doctors_rating.doctor_id) as doctorReview , doctor_details.doctor_id , doctor_details.photo,doctor_details.speciality_id');
        if($siteLang=='arabic'){
            $CI->db->select('doctor_master.first_name , doctor_master.last_name');
        }else{
            $CI->db->select('doctor_master_en.first_name , doctor_master_en.last_name');
        }
        $CI->db->from('doctor_details');
        
        $CI->db->join('doctor_master_en','doctor_details.doctor_id=doctor_master_en.doctor_id','LEFT');
        $CI->db->join('doctor_master','doctor_details.doctor_id=doctor_master.doctor_id','LEFT');
        $CI->db->join('doctors_rating','doctor_details.doctor_id=doctors_rating.doctor_id','RIGHT');
        $CI->db->where('doctor_details.featured','1');
        $CI->db->where('doctors_rating.dstatus','1');        
        $CI->db->group_by('doctor_details.doctor_id');  
        $CI->db->order_by('AVG(doctors_rating.average_score)','DESC');
        $CI->db->limit(10,0);
        $CI->db->get()->result();
        echo $CI->db->last_query();;die;*/
        $siteLang=$CI->session->userdata('site_lang');
          $CI->db->select('doctor_details.doctor_id , doctor_details.photo,doctor_details.speciality_id');
        if($siteLang=='arabic'){
            $CI->db->select('doctor_master.first_name , doctor_master.last_name');
        }else{
            $CI->db->select('doctor_master_en.first_name , doctor_master_en.last_name');
        }
        $CI->db->from('doctor_details');
        
        $CI->db->join('doctor_master_en','doctor_details.doctor_id=doctor_master_en.doctor_id','LEFT');
        $CI->db->join('doctor_master','doctor_details.doctor_id=doctor_master.doctor_id','LEFT');        
        $CI->db->where('doctor_details.featured','1');        
        $CI->db->group_by('doctor_details.doctor_id');  
        $CI->db->order_by('doctor_details.doctor_id','DESC');
        $CI->db->limit(10,0);
        return $CI->db->get()->result();
    }
    
    /**
     * single doctor rating details for featured array
     * @param int $id
     * @return mixed array
     */
    function get_rating_of_doctor($id)
    {
        $CI =&get_instance();
        $CI->db->select('IFNULL(AVG(reputation  + clinic + availability + approachability + technology )/5,0) as featureAverageScore,count(doctors_rating.doctor_id) as doctorReview');
        $CI->db->where('doctors_rating.dstatus','1'); 
        $CI->db->where('doctors_rating.doctor_id',$id);
        return $CI->db->get('doctors_rating')->row();
        //echo $CI->db->last_query();
        //die;
    }
    
    function getRatingByDoctors($ratingcolumn,$doctor_id)
    {
         $CI = &get_instance();
         //SELECT IFNULL(AVG(clinic),0) as clinic FROM `doctors_rating` WHERE dstatus='1' AND doctor_id='2' and clinic!='0' 
         $CI->db->select("IFNULL(AVG($ratingcolumn),0) as $ratingcolumn");         
         //$CI->db->select("AVG($ratingcolumn) AS $ratingcolumn");
         $CI->db->where('dstatus','1');
         $CI->db->where('doctor_id',$doctor_id);
 
        $CI->db->where("$ratingcolumn !=0");
        //$CI->db->where("$ratingcolumn!='0'");         
       return   $CI->db->get('doctors_rating')->result();

         
    }
    function getRatingByworks($ratingcolumn,$work_id)
    {
         $CI = &get_instance();
         //$CI->db->select("(SELECT NULLIF(SUM($ratingcolumn), 0)/COUNT($ratingcolumn) FROM work_rating WHERE wstatus='1' AND $ratingcolumn!='0' AND work_id='".$work_id."') AS $ratingcolumn");         
         
         //$CI->db->select("AVG($ratingcolumn) AS $ratingcolumn");
         $CI->db->select("IFNULL(AVG($ratingcolumn),0) as $ratingcolumn");
         $CI->db->where('wstatus','1');
         $CI->db->where('work_id',$work_id);

        $CI->db->where("$ratingcolumn !=", "0");
        //$CI->db->where("$ratingcolumn!='0'");                  
        return $CI->db->get('work_rating')->result();
        //echo $CI->db->last_query();die;
         
    }

