<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @param int $doctor_id
 * @return int
 */
function get_doctor_total_review($doctor_id)
{
    $CI= &get_instance();
    $CI->db->select('*');    
    $CI->db->where('doctor_id',$doctor_id);
    return $CI->db->get('doctors_rating')->num_rows();
    
}

/**
 * 
 * @param int $work_id
 * @return int
 */
function get_work_total_review($work_id)
{
    $CI= &get_instance();
    $CI->db->select('*');    
    $CI->db->where('work_id',$work_id);
    return $CI->db->get('work_rating')->num_rows();
    
}


?>