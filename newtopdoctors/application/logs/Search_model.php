<?php

class Search_model extends MY_Model
{

    public function globalFilterSearch($type)
    {
        $final_result = array();
        $work_type_result = array();
        $doctor_result = array();

        switch ($type) {
            case 'hospital':
            case 'clinic':
            case 'lab':
                //work type search
                $work_type_result = $this->db->select()
                    ->from('work_master')
                    ->where('work_master.work_type', $type)
                    ->get()
                    ->result();
                break;

            case 'doctor':
                //doctor search
                $doctor_result = $this->db->select()
                    ->from('doctor_master_en')
                    ->get()
                    ->result();
                break;

            default:
                //both combined search
        }

        $final_result = array_merge($work_type_result, $doctor_result);

        return $final_result;
    }

    public function filterSearch($filters)
    {

        $doctors = array();
        $work = array();
        if ($filters['searchType'] != "") {
            if ($filters['searchType'] == "doctor") {
                $filters['page'] = ($filters['page'] - 1);
                $page = ($filters['page'] * $filters['perPage']);
                if ($filters['lang'] == 'ar') {
                    $result = $this->db->select('dm.doctor_id AS Id, CONCAT(TRIM(d.first_name), " ", TRIM(d.last_name)) AS Name, d.address AS Address, IFNULL(dd.phone_number,"") AS Phone,sm.mipmap as Mipmap, dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE dd.doctor_id=doctors_rating.doctor_id) as RatingAvg, (select COUNT(dr.doctor_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) AS TotalReview,dd.working_hours AS WorkingHour, "doctor" as WorkType');
                } else {
                    $this->db->select('dm.doctor_id AS Id, CONCAT(TRIM(dm.first_name), " ", TRIM(dm.last_name)) AS Name, dm.address AS Address,IFNULL(dd.phone_number,"") AS Phone,sm.mipmap as Mipmap,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE dd.doctor_id=doctors_rating.doctor_id) as RatingAvg, (select COUNT(dr.doctor_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) AS TotalReview,dd.working_hours AS WorkingHour,  "doctor" as WorkType');
                }

                if ($filters['isnearby'] == 1) {
                    if ($filters['lat'] != "" || $filters['long'] != "") {
                        $this->db->select('SQRT(POW(69.1 * (dd.google_map_latitude - ' . $filters["lat"] . '), 2) + POW(69.1 * (' . $filters["long"] . ' - dd.google_map_longtude) * COS(dd.google_map_latitude / 57.3), 2)) AS distance');
                    }
                }

                if ($filters['location'] != "" || $filters['speciality'] != "" || $filters['isnearby'] == 1 || $filters['topratestfirst'] == 1 || $filters['name'] != "" || $filters['gender'] != "") {
                    $this->db->group_start();
                    $this->db->where('dd.visibility', '1');
                }

                $this->db->from('doctor_master_en dm');
                $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
                $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
                $this->db->join('doctor_master d', 'd.doctor_id = dm.doctor_id', 'left');
                $this->db->join('location_master lm', 'lm.location_id=dd.location_id', 'left');
                $this->db->join('specialty_master sm', 'sm.specialties_id=dd.speciality_id', 'left');
                $this->db->join('bookmark_master bm', 'bm.doctor_id=dd.doctor_id', 'left');
                $this->db->join('user_master um', 'um.user_id=bm.user_id', 'left');

                //$this->db->join('doctors_rating dr','dr.doctor_id=d.doctor_id','left');
                if ($filters['location'] != "") {

                    if ($filters['lang'] == "ar") {
                        $this->db->where('lm.name', $filters['location']);
                        if (count($filters['childs']) > 0) {
                            $this->db->or_where_in('lm.name', $filters['childs']);
                        }

                    } else {
                        $this->db->where('lm.name_en', $filters['location']);
                        if (count($filters['childs']) > 0) {
                            $this->db->or_where_in('lm.name_en', $filters['childs']);
                        }

                    }
                }

                if ($filters['speciality'] != "") {
                    if ($filters['lang'] == "ar") {
                        $this->db->where('sm.name', $filters['speciality']);
                    } else {
                        $this->db->where('sm.name_en', $filters['speciality']);
                    }

                }
                if ($filters['isnearby'] == 1) {
                    if ($filters['lat'] != "" || $filters['long'] != "") {
                        $this->db->order_by('distance', 'ASC');
                        $this->db->having('distance <= ', 10);
                    }
                    /*{

                $this->db->where("(6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filters['lat'] . " - dd.google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filters['lat'] . " * PI() / 180)
                 * COS(dd.google_map_latitude * PI() / 180) * POWER(SIN((" . $filters['long'] . " - dd.google_map_longtude) * PI() / 180 / 2), 2))) <= '10')");
                }*/
                }
                if ($filters['topratestfirst'] == 1) {

                    $this->db->order_by('RatingAvg', 'DESC');
                }
                if ($filters['name'] != "") {

                    /*doctor_master_en.first_name LIKE '%".$data['searchname']."%' OR doctor_master_en.last_name LIKE '%".$data['searchname']."%'  or concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$data['searchname']."%'"*/
                    if ($filters['lang'] == "ar") {
                        //$rawstring = findword($filters['name']);
                        $this->db->or_like('CONCAT(TRIM(d.first_name), " ", TRIM(d.last_name))', $filters['name'], 'left');
                        /*foreach ($rawstring as $key => $value) {
                        if ($key == 0) {
                        $this->db->like('d.first_name', $value, 'left');
                        } else {
                        $this->db->or_like('d.last_name', $value, 'left');
                        $this->db->or_like('CONCAT(TRIM(d.first_name), " ", TRIM(d.last_name))', $value, 'left');
                        }
                        }*/
                        /*$this->db->where("d.first_name LIKE '%".$filters['name']."%' OR d.last_name LIKE '%".$filters['name']."%'  or concat_ws(' ',d.first_name,d.last_name) like '%".$filters['name']."%'");*/
                    } else {
                        $this->db->where("dm.first_name LIKE '%" . $filters['name'] . "%' OR dm.last_name LIKE '%" . $filters['name'] . "%'  or concat_ws(' ',dm.first_name,dm.last_name) like '%" . $filters['name'] . "%'");
                    }
                    //$this->db->like('dm.first_name',$filters['name']);
                    //$this->db->or_like('dm.last_name',$filters['name']);
                }

                //$this->db->where('dd.visibility','1');
                if ($filters['gender'] != "") {
                    $this->db->where('dd.gender', $filters['gender']);
                }

                if ($filters['location'] != "" || $filters['speciality'] != "" || $filters['isnearby'] == 1 || $filters['topratestfirst'] == 1 || $filters['name'] != "" || $filters['gender'] != "") {
                    $this->db->group_end();
                }
                $this->db->where('dd.visibility', '1');

                if ($page == '' || $page <= 0) {
                    $filters['perPage'] = 10;
                    $page = 0;
                }
                $this->db->order_by('RatingAvg DESC, TotalReview DESC');
                $this->db->limit($filters['perPage'], $page);
                $this->db->group_by('Id');
                $doctors = $this->db->get()->result();
                //  echo $this->db->last_query();
                //die;

            }
            if ($filters['searchType'] == "hospital" || $filters['searchType'] == "clinic" || $filters['searchType'] == "Radiology Lab" || $filters['searchType'] == "Medical Lab" || $filters['searchType'] == "lab") {
                $filters['page'] = ($filters['page'] - 1);
                $page = ($filters['page'] * $filters['perPage']);
                if ($filters['lang'] == 'ar') {

                    $this->db->select('w.work_id AS Id, w.name AS Name, w.address AS Address, w.phone AS Phone, "null" AS Latitude,"null" AS Longitude , bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,( select AVG(wr.average_score)/COUNT(wr.user_id) from work_rating wr where w.work_id = wr.work_id) as RatingAvg, (select COUNT(wr.work_id) from work_rating wr where w.work_id = wr.work_id) AS TotalReview, "null" AS WorkingHour, w.work_type as WorkType');

                } else {
                    $this->db->select('w.work_id AS Id, w.name_en AS Name, w.address_en AS Address, w.phone AS Phone, "null" AS Latitude,"null" AS Longitude , bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,( select AVG(wr.average_score)/COUNT(wr.user_id) from work_rating wr where w.work_id = wr.work_id) as RatingAvg, (select COUNT(wr.work_id) from work_rating wr where w.work_id = wr.work_id) AS TotalReview, "null" AS WorkingHour, w.work_type as WorkType');
                }
                $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');
                if ($filters['isnearby'] == 1) {
                    if ($filters['lat'] != "" || $filters['long'] != "") {
                        $this->db->select('SQRT(POW(69.1 * (dd.google_map_latitude - ' . $filters["lat"] . '), 2) + POW(69.1 * (' . $filters["long"] . ' - dd.google_map_longtude) * COS(dd.google_map_latitude / 57.3), 2)) AS distance');
                    }
                }
                $this->db->from('work_master w');
                $this->db->where('w.work_type', $filters['searchType']);
                $this->db->join('location_master lm', 'lm.location_id=w.location_id', 'left');
                $this->db->join('doctor_details dd', 'dd.work_id = w.work_id', 'left');
                $this->db->join('specialty_master sm', 'sm.specialties_id=dd.speciality_id', 'left');
                $this->db->join('worktype_bookmark_master bm', 'bm.work_id=w.work_id', 'left');
                $this->db->join('user_master um', 'um.user_id=bm.user_id', 'left');
                $this->db->where('w.status', '1');

                if ($filters['name'] != "") {
                    if ($filters['lang'] == "ar") {
                        $this->db->like("w.name", $filters['name'], 'both');
                    } else {
                        $this->db->like("w.name_en", $filters['name'], 'both');
                    }

                }

                //for lab only
                if ($filters['searchType'] == "Radiology Lab" ||
                    $filters['searchType'] == "Medical Lab") {

                } else {
                    //for other
                    if ($filters['speciality'] != "") {

                        if ($filters['lang'] == "ar") {
                            //$where = "FIND_IN_SET('".$filters['speciality']."', Spec)";
                            //$this->db->where($where);
                            //
                            $this->db->where('sm.name', $filters['speciality']);
                        } else {
                            //$this->db->where_in('sm.name_en', $filters['speciality']);
                            //$where = "FIND_IN_SET('".$filters['speciality']."', Spec)";
                            //$this->db->where($where);
                            $this->db->where('sm.name_en', $filters['speciality']);
                        }
                    }
                }

                if ($filters['location'] != "") {
                    // echo $filters['location'];die;
                    if ($filters['lang'] == "ar") {
                        $this->db->where('lm.name', $filters['location']);
                        if (count($filters['childs']) > 0) {
                            $this->db->or_where_in('lm.name', $filter['childs']);
                        }

                    } else {
                        $this->db->where('lm.name_en', $filters['location']);
                        if (count($filters['childs']) > 0) {
                            $this->db->or_where_in('lm.name_en', $filter['childs']);
                        }

                    }
                }

                if ($filters['isnearby'] == 1) {
                    if ($filters['lat'] != "" || $filters['long'] != "") {
                        $this->db->order_by('distance', 'ASC');
                        $this->db->having('distance <= ', 10);
                        /*$this->db->where("(6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filters['lat'] . " - dd.google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filters['lat'] . " * PI() / 180)
                     * COS(dd.google_map_latitude * PI() / 180) * POWER(SIN((" . $filters['long'] . " - dd.google_map_longtude) * PI() / 180 / 2), 2))) <= '10')");*/
                    }
                }
                if ($filters['topratestfirst'] == 1) {

                    $this->db->order_by('RatingAvg', 'DESC');
                }
                if ($page == '' || $page <= 0) {
                    $filters['perPage'] = 10;
                    $page = 0;
                }
                $this->db->order_by('RatingAvg', 'DESC');
                $this->db->limit($filters['perPage'], $page);
                $this->db->group_by('Id');
                $work = $this->db->get()->result();
                //echo $this->db->last_query();exit;

            }
            $total_result = array_merge($doctors, $work);
        } else {
            $filters['perPage'] = 10;
            $filters['page'] = ($filters['page'] - 1);
            $page = ($filters['page'] * $filters['perPage']);
            /* Both result here when type is not define */

            /*** doctor start here  **/
            if ($filters['lang'] == 'ar') {

                $result = $this->db->select('dm.doctor_id AS Id, CONCAT(d.first_name, " ", d.last_name) AS Name, d.address AS Address, IFNULL(dd.phone_number,"") AS Phone,sm.mipmap as Mipmap, dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,(SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE dd.doctor_id=doctors_rating.doctor_id ) as RatingAvg, (select COUNT(dr.doctor_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) AS TotalReview,dd.working_hours AS WorkingHour, "doctor" as WorkType');

            } else {
                $this->db->select('dm.doctor_id AS Id, CONCAT(dm.first_name, " ", dm.last_name) AS Name, dm.address AS Address, IFNULL(dd.phone_number,"") AS Phone,sm.mipmap as Mipmap,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE dd.doctor_id=doctors_rating.doctor_id ) as RatingAvg, (select COUNT(dr.doctor_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) AS TotalReview,dd.working_hours AS WorkingHour,  "doctor" as WorkType');
            }
            if ($filters['isnearby'] == 1) {
                if ($filters['lat'] != "" || $filters['long'] != "") {
                    $this->db->select('SQRT(POW(69.1 * (dd.google_map_latitude - ' . $filters["lat"] . '), 2) + POW(69.1 * (' . $filters["long"] . ' - dd.google_map_longtude) * COS(dd.google_map_latitude / 57.3), 2)) AS distance');
                }
            }
            $this->db->from('doctor_master_en dm');
            $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
            $this->db->join('doctor_master d', 'd.doctor_id = dm.doctor_id', 'left');
            //$this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
            $this->db->join('location_master lm', 'lm.location_id=dd.location_id', 'left');
            $this->db->join('specialty_master sm', 'sm.specialties_id=dd.speciality_id', 'left');
            $this->db->join('bookmark_master bm', 'bm.doctor_id=dd.doctor_id AND bm.user_id = ' . $filters['userId'], 'left');
            $this->db->join('user_master um', 'um.user_id=bm.user_id', 'left');
            $this->db->where('dd.visibility', '1');
            //$this->db->join('doctors_rating dr','dr.doctor_id=d.doctor_id','left');

            if ($filters['name'] != "") {

                /*doctor_master_en.first_name LIKE '%".$data['searchname']."%' OR doctor_master_en.last_name LIKE '%".$data['searchname']."%'  or concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$data['searchname']."%'"*/
                if ($filters['lang'] == "ar") {
                    $this->db->group_start();
                    $this->db->where("d.first_name LIKE '%" . $filters['name'] . "%' OR d.last_name LIKE '%" . $filters['name'] . "%'  or concat_ws(' ',d.first_name,d.last_name) like '%" . $filters['name'] . "%'");
                    $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->where("dm.first_name LIKE '%" . $filters['name'] . "%' OR dm.last_name LIKE '%" . $filters['name'] . "%'  or concat_ws(' ',dm.first_name,dm.last_name) like '%" . $filters['name'] . "%'");
                    $this->db->group_end();
                }
                //$this->db->like('dm.first_name',$filters['name']);
                //$this->db->or_like('dm.last_name',$filters['name']);
            }

            if ($filters['location'] != "") {
                // echo $filters['location'];die;
                if ($filters['lang'] == "ar") {
                    $this->db->where('lm.name', $filters['location']);
                } else {
                    $this->db->where('lm.name_en', $filters['location']);
                }
            }

            if ($filters['speciality'] != "") {
                if ($filters['lang'] == "ar") {
                    $this->db->where('sm.name', $filters['speciality']);
                } else {
                    $this->db->where('sm.name_en', $filters['speciality']);
                }

            }

            if ($filters['isnearby'] == 1) {
                if ($filters['lat'] != "" || $filters['long'] != "") {
                    $this->db->order_by('distance', 'ASC');
                    $this->db->having('distance <= ', 10);
                }
            }
            /*if($filters['isnearby']==1)
            {
            if($filters['lat']!="" || $filters['long']!="")
            {

            $this->db->where("(6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filters['lat'] . " - dd.google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filters['lat'] . " * PI() / 180)
             * COS(dd.google_map_latitude * PI() / 180) * POWER(SIN((" . $filters['long'] . " - dd.google_map_longtude) * PI() / 180 / 2), 2))) <= '10')");
            }
            }*/
            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }

            $this->db->where('dd.visibility', '1');
            if ($filters['gender'] != "") {
                $this->db->where('dd.gender', $filters['gender']);
            }
            if ($page == '' || $page <= 0) {
                $filters['perPage'] = 10;
                $page = 0;
            }
            $this->db->order_by('RatingAvg', 'DESC');
            $this->db->limit($filters['perPage'], $page);
            $this->db->group_by('Id');
            $doctors = $this->db->get()->result();
            //  echo $this->db->last_query();
            /******** end doctor  ***/

            /**   work   **/
            if ($filters['lang'] == 'ar') {

                $this->db->select('w.work_id AS Id, w.name AS Name, w.address AS Address, w.phone AS Phone, "null" AS Latitude,"null" AS Longitude , bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,(SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE w.work_id=work_rating.work_id ) as RatingAvg, (select COUNT(wr.work_id) from work_rating wr where w.work_id = wr.work_id) AS TotalReview, "null" AS WorkingHour, w.work_type as WorkType');

            } else {
                $this->db->select('w.work_id AS Id, w.name_en AS Name, w.address_en AS Address, w.phone AS Phone, "null" AS Latitude,"null" AS Longitude , bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,(SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE w.work_id=work_rating.work_id ) as RatingAvg, (select COUNT(wr.work_id) from work_rating wr where w.work_id = wr.work_id) AS TotalReview, "null" AS WorkingHour, w.work_type as WorkType');
            }
            $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');
            $this->db->from('work_master w');

            $this->db->join('location_master lm', 'lm.location_id=w.location_id', 'left');
            $this->db->join('specialty_master sm', 'sm.specialties_id=w.speciality', 'left');
            $this->db->join('worktype_bookmark_master bm', 'bm.work_id=w.work_id AND bm.user_id = ' . $filters['userId'], 'left');
            $this->db->join('user_master um', 'um.user_id=bm.user_id', 'left');
            $this->db->where('w.status', '1');
            if ($filters['name'] != "") {
                if ($filters['lang'] == "ar") {
                    $this->db->like("w.name", $filters['name'], 'both');
                } else {
                    $this->db->like("w.name_en", $filters['name'], 'both');
                }

            }
            if ($filters['speciality'] != "") {
                if ($filters['lang'] == "ar") {
                    $this->db->like('sm.name', $filters['speciality']);
                } else {
                    $this->db->like('sm.name_en', $filters['speciality']);
                }
            }
            if ($filters['location'] != "") {
                // echo $filters['location'];die;
                if ($filters['lang'] == "ar") {
                    $this->db->where('lm.name', $filters['location']);
                } else {
                    $this->db->where('lm.name_en', $filters['location']);
                }
            }

            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }
            if ($page == '' || $page <= 0) {
                $filters['perPage'] = 10;
                $page = 0;
            }
            $this->db->order_by('RatingAvg DESC, FIELD(w.work_type, "Hospital", "Clinic", "Radiology Lab", "Medical Lab")');
            $this->db->limit($filters['perPage'], $page);
            $this->db->group_by('Id');
            $work = $this->db->get()->result();
//echo $this->db->last_query();
            /*** end work ***/
//die;

            $total_result = array_merge($doctors, $work);

            //$this->db;
        }
        return $total_result;

    }
    public function search($filters)
    {
        $filters['page'] = ($filters['page'] - 1);
        $page = ($filters['page'] * $filters['perPage']);
        if ($filters['searchType'] == 'doctor') {

            if (!empty($filters['speciality'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('sm.name', $filters['speciality']);
                } else {
                    $this->db->where('sm.name_en', $filters['speciality']);
                }

            }

            if (!empty($filters['location'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('cm.name', $filters['location']);
                } else {
                    $this->db->where('cm.name_en', $filters['location']);
                }

            }

            if (!empty($filters['gender'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('dd.gender', $filters['gender']);
                } else {
                    $this->db->where('dd.gender', $filters['gender']);
                }
            }

            if (!empty($filters['name'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->like("d.first_name", $filters['name']);
                    $this->db->or_like("d.last_name", $filters['name']);
                } else {
                    $this->db->like("dm.first_name", $filters['name']);
                    $this->db->or_like("dm.last_name", $filters['name']);
                }
            }

            if ($filters['lang'] == 'ar') {

                $result = $this->db->select('dm.doctor_id AS DoctorId, CONCAT(d.first_name, " ", d.last_name) AS Name, d.address AS Address, dd.phone_number AS Phone,sm.mipmap as Mipmap, dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus');

            } else {
                $this->db->select('dm.doctor_id AS DoctorId, CONCAT(dm.first_name, " ", dm.last_name) AS Name, dm.address AS Address, dd.phone_number AS Phone,sm.mipmap as Mipmap, dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus');
            }
            $this->db->from('doctor_master_en dm');
            $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
            $this->db->join('doctor_master d', 'd.doctor_id = dm.doctor_id', 'left');
            $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
            $this->db->join('location_master cm', 'cm.location_id = dd.location_id', 'left');
            $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
            $this->db->join('bookmark_master bm', 'bm.doctor_id = dd.doctor_id AND bm.user_id = ' . $filters['userId'], 'left');
            $this->db->group_by('dm.doctor_id');
            if ($filters['perPage'] == '' || $page == '' || $page <= 0) {
                $filters['perPage'] = 10;
                $page = 0;
            }

            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }
            if ($filters['isnearby'] == 1) {
                if ($filters['lat'] != "" || $filters['long'] != "") {

                    $this->db->where("(6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filters['lat'] . " - dd.google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filters['lat'] . " * PI() / 180)
                                                            * COS(dd.google_map_latitude * PI() / 180) * POWER(SIN((" . $filters['long'] . " - dd.google_map_longtude) * PI() / 180 / 2), 2))) <= '10')");
                }
            }
            $this->db->limit($filters['perPage'], $page);
            // $this->db->last_query();
            //die;
            return $this->db->get()->result();

        } else if ($filters['searchType'] == 'lab') {
            if (!empty($filters['speciality'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('sm.name', $filters['speciality']);
                } else {
                    $this->db->where('sm.name_en', $filters['speciality']);
                }
            }
            if (!empty($filters['location'])) {

                if ($filters['lang'] == 'ar') {
                    $this->db->where('cm.name', $filters['location']);
                } else {
                    $this->db->where('cm.name_en', $filters['location']);
                }

            }
            if (!empty($filters['name'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->or_like("wm.name", $filters['name']);
                } else {
                    $this->db->or_like("wm.name_en", $filters['name']);
                }

            }

            if ($filters['lang'] == 'ar') {

                $this->db->select("wm.work_id AS WorkId, wm.name AS Name, wm.address AS Address, wm.work_type AS WorkType, wm.phone AS Phone,wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,'null' AS Latitude,'null' AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus ");
            } else {

                $this->db->select("wm.work_id AS WorkId, wm.name_en AS Name, wm.address_en AS Address, wm.work_type AS WorkType, wm.phone AS Phone, wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,'null' AS Latitude,'null' AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus");

            }
            $this->db->from("work_master wm");

            $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = wm.work_id)AS Mipmap');

            //$this->db->join("doctor_master_en dm", "dm.doctor_id = wm.doctor_id", "left");
            $this->db->join('location_master cm', 'cm.location_id = wm.location_id', 'left');
            $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id AND wbm.user_id = ' . $filters['userId'], 'left');
            $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
            $this->db->group_by('wm.work_id');
            $this->db->where('wm.work_type', 'Lab');
            if ($filters['perPage'] == '' ||
                $page == '' || $page <= 0) {
                $filters['perPage'] = 10;
                $page = 0;
            }
            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }
            $this->db->limit($filters['perPage'], $page);
            return $this->db->get()->result();

        } else if ($filters['searchType'] == 'hospital') {
            if (!empty($filters['speciality'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('sm.name', $filters['speciality']);
                } else {
                    $this->db->where('sm.name_en', $filters['speciality']);
                }
            }
            if (!empty($filters['location'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('cm.name', $filters['location']);
                } else {
                    $this->db->where('cm.name_en', $filters['location']);
                }
            }
            if (!empty($filters['name'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->or_like("wm.name", $filters['name']);
                } else {
                    $this->db->or_like("wm.name_en", $filters['name']);
                }
            }
            if ($filters['lang'] == 'ar') {

                $this->db->select("wm.work_id AS WorkId, wm.name AS Name, wm.address AS Address, wm.work_type AS WorkType, wm.phone AS Phone, wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,'null' AS Latitude,'null' AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus");
            } else {

                $this->db->select("wm.work_id AS WorkId, wm.name_en AS Name, wm.address_en AS Address, wm.work_type AS WorkType, wm.phone AS Phone, wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,'null' AS Latitude,'null' AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus");
            }
            $this->db->from("work_master wm");

            $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = wm.work_id)AS Mipmap');

            //$this->db->join("doctor_master_en dm", "dm.doctor_id = wm.doctor_id", "left");
            $this->db->join('location_master cm', 'cm.location_id = wm.location_id', 'left');
            $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id AND wbm.user_id = ' . $filters['userId'], 'left');
            $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
            $this->db->group_by('wm.work_id');
            $this->db->where('wm.work_type', 'Hospital');
            if ($filters['perPage'] == '' ||
                $page == '' || $page <= 0) {
                $filters['perPage'] = 10;
                $page = 0;
            }
            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }
            $this->db->limit($filters['perPage'], $page);
            //$this->db->get()->result();
            // echo $this->db->last_query();die;

            return $this->db->get()->result();

        } else if ($filters['searchType'] == 'clinic') {

            if (!empty($filters['speciality'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('sm.name', $filters['speciality']);
                } else {
                    $this->db->where('sm.name_en', $filters['speciality']);
                }
            }
            if (!empty($filters['location'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('cm.name', $filters['location']);
                } else {
                    $this->db->where('cm.name_en', $filters['location']);
                }
            }
            if (!empty($filters['name'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->or_like("wm.name", $filters['name']);
                } else {
                    $this->db->or_like("wm.name_en", $filters['name']);
                }
            }
            if ($filters['lang'] == 'ar') {
                $this->db->select("wm.work_id AS WorkId, wm.name AS Name, wm.address AS Address, wm.work_type AS WorkType, wm.phone AS Phone,wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus");
            } else {
                $this->db->select("wm.work_id AS WorkId, wm.name_en AS Name, wm.address_en AS Address, wm.work_type AS WorkType, wm.phone AS Phone,wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,'null' AS Latitude,'null' AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus");
            }
            $this->db->from("work_master wm");

            $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = wm.work_id)AS Mipmap');
            //$this->db->join("doctor_master_en dm", "dm.doctor_id = wm.doctor_id", "left");
            $this->db->join('location_master cm', 'cm.location_id = wm.location_id', 'left');
            $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id AND wbm.user_id = ' . $filters['userId'], 'left');
            $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
            $this->db->group_by('wm.work_id');
            $this->db->where('wm.work_type', 'Clinic');
            if ($filters['perPage'] == '' ||
                $page == '' || $page <= 0) {
                $filters['perPage'] = 10;
                $page = 0;
            }
            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }
            $this->db->limit($filters['perPage'], $page);
            return $this->db->get()->result();
        }

        //work_master w
        if ($filters['lang'] == 'ar') {
            $this->db->select('w.work_id AS WorkId, w.work_type AS WorkType, w.name AS Name, w.address AS Address, w.phone AS Phone, specialty_master.name AS Speciality, doctor_details.photo AS Photo,specialty_master.mipmap as Mipmap, dr.average_score as RatingAvg,COUNT(dr.rating_id) AS TotalReview,doctor_details.google_map_latitude AS Latitude,doctor_details.google_map_longtude AS Longitude');
        } else {
            $this->db->select('w.work_id AS WorkId, w.work_type AS WorkType, w.name_en AS Name, w.address_en AS Address, w.phone AS Phone, specialty_master.name_en AS Speciality, doctor_details.photo AS Photo,specialty_master.mipmap as Mipmap, dr.average_score as RatingAvg,COUNT(dr.rating_id) AS TotalReview,doctor_details.google_map_latitude AS Latitude,doctor_details.google_map_longtude AS Longitude');
        }

        $this->db->from('work_master w');
        $this->db->join('doctor_master_en', 'doctor_master_en.doctor_id = w.doctor_id', 'left');
        $this->db->join('doctor_details', 'doctor_details.work_id = w.work_id', 'left');
        $this->db->join('doctor_master', 'doctor_master.doctor_id = doctor_details.doctor_id', 'left');
        $this->db->join('specialty_master', 'specialty_master.specialties_id = doctor_details.speciality_id', 'left');
        $this->db->join('location_master', 'location_master.location_id = doctor_details.location_id', 'left');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = doctor_details.doctor_id', 'left');
        $this->db->group_by('doctor_master_en.doctor_id');

        if (!empty($filters['name'])) {
            if ($filters['lang'] == 'ar') {

                $this->db->like('doctor_master.first_name', $filters['name']);
                $this->db->or_like('doctor_master.last_name', $filters['name']);
                $this->db->or_like('w.name', $filters['name']);

            } else {

                $this->db->like('doctor_master_en.first_name', $filters['name']);
                $this->db->or_like('doctor_master_en.last_name', $filters['name']);
                $this->db->or_like('w.name_en', $filters['name']);

            }
        }

        if (!empty($filters['speciality'])) {
            if ($filters['lang'] == 'ar') {
                $this->db->where('specialty_master.name', $filters['speciality']);
            } else {
                $this->db->where('specialty_master.name_en', $filters['speciality']);
            }
            if (!empty($filters['search'])) {
                if ($filters['lang'] == 'ar') {

                    $this->db->like('doctor_master.first_name', $filters['search']);
                    $this->db->or_like('doctor_master.last_name', $filters['search']);
                    $this->db->or_like('w.name', $filters['search']);

                } else {
                    $this->db->like('doctor_master_en.first_name', $filters['search']);
                    $this->db->or_like('doctor_master_en.last_name', $filters['search']);
                    $this->db->or_like('w.name_en', $filters['search']);
                }
            }
        }

        if (!empty($filters['location'])) {
            if ($filters['lang'] == 'ar') {
                $this->db->where('location_master.name', $filters['location']);
            } else {
                $this->db->where('location_master.name_en', $filters['location']);
            }

            if (!empty($filters['search'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->like('doctor_master.first_name', $filters['search']);
                    $this->db->or_like('doctor_master.last_name', $filters['search']);
                    $this->db->or_like('w.name', $filters['search']);
                } else {
                    $this->db->like('doctor_master_en.first_name', $filters['search']);
                    $this->db->or_like('doctor_master_en.last_name', $filters['search']);
                    $this->db->or_like('w.name_en', $filters['search']);
                }
            }
        }

        if (!empty($filters['gender'])) {
            if ($filters['lang'] == 'ar') {
                $this->db->where('doctor_details.gender', $filters['gender']);
            } else {
                $this->db->where('doctor_details.gender', $filters['gender']);
            }

            if (!empty($filters['search'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->like('doctor_master.first_name', $filters['search']);
                    $this->db->or_like('doctor_master.last_name', $filters['search']);
                    $this->db->or_like('w.name', $filters['search']);
                } else {
                    $this->db->like('doctor_master_en.first_name', $filters['search']);
                    $this->db->or_like('doctor_master_en.last_name', $filters['search']);
                    $this->db->or_like('w.name_en', $filters['search']);
                }

            }
        }
        if ($filters['perPage'] == '' ||
            $page == '' || $page <= 0) {
            $filters['perPage'] = 10;
            $page = 0;
        }
        if ($filters['topratestfirst'] == 1) {

            $this->db->order_by('RatingAvg', 'DESC');
        }
        $this->db->limit($filters['perPage'], $page);
        $this->db->order_by('FIELD(work_type, "Hospital", "Clinic", "Radiology Lab", "Medical Lab")');
        $result = $this->db->get();
        return $result->result();
    }

    public function globalSearch($search, $lang, $userId, $page, $perPage)
    {
        $result = array();
        $search = trim($search);
        $doctorMasterGlobalSearchResult = $this->doctorMasterGlobalSearch($search, $lang, $userId, $page, $perPage);
        $result = $this->mergerResult($result, $doctorMasterGlobalSearchResult);

        $workMasterGlobalSearchResult = $this->workMasterGlobalSearch($search, $lang, $userId, $page, $perPage);
        $result = $this->mergerResult($result, $workMasterGlobalSearchResult);

        return $result;

    }

    /**
     * Work master global search
     * @param  string $search
     * @param  string $lang
     * @param  string $userId
     * @param  string $page
     * @param  string $perPage
     * @return array
     */
    public function workMasterGlobalSearch($search, $lang, $userId, $page, $perPage)
    {
        $page = ($page - 1);
        $page = ($page * $perPage);
        $result = array();
        if ($lang == 'ar') {
            $result = $this->workMasterGlobalSearchAr($search, $lang, $userId, $page, $perPage);
        } else {
            $result = $this->workMasterGlobalSearchEn($search, $lang, $userId, $page, $perPage);
        }

        return $result;
    }

    /**
     * Work master global search if lang is En or any other
     * @param  string $search
     * @param  string $lang
     * @param  string $userId
     * @param  string $page
     * @param  string $perPage
     * @return array
     */
    public function workMasterGlobalSearchEn($search, $lang, $userId, $page, $perPage)
    {
        $this->db->select('w.name_en AS Name,w.work_id AS WorkId, w.address_en AS Address, w.phone AS Phone, w.photo AS Photo,w.work_type AS WorkType,"null" AS Latitude,"null" AS Longitude, (SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE w.work_id=work_rating.work_id ) as RatingAvg, COUNT(wr.work_id) AS TotalReview,wbm.user_id AS BookmarkUserId, wbm.status AS BookmarkStatus');
        $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');

        $this->db->from('work_master w');

        //table join
        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');
        $this->db->join('location_master lm', 'lm.location_id = w.location_id', 'left');

        //like and where condition
        $this->db->where('w.status', '1');
        $this->db->like('w.name_en', $search, 'both');
        $this->db->or_like('w.address_en', $search, 'both');
        $this->db->or_like('w.phone', $search, 'both');
        $this->db->or_like('lm.name_en', $search, 'both');
        $this->db->group_by('w.work_id');
        $this->db->order_by('RatingAvg DESC, FIELD(w.work_type, "Hospital", "Clinic", "Radiology Lab", "Medical Lab")');
        if ($perPage == '' || $page == '' || $page <= 0) {
            $perPage = 10;
            $page = 0;
        }
        $this->db->limit($perPage, $page);

        return $this->db->get()->result();
    }

    /**
     * Global search for work master if lang is AR
     * @param  string $search
     * @param  string $lang
     * @param  string $userId
     * @param  string $page
     * @param  string $perPage
     * @return array
     */
    public function workMasterGlobalSearchAr($search, $lang, $userId, $page, $perPage)
    {
        $this->db->select('w.name AS Name,w.work_id AS WorkId, w.address AS Address, w.phone AS Phone, w.photo AS Photo,w.work_type AS WorkType,"null" AS Latitude,"null" AS Longitude, (SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE w.work_id=work_rating.work_id ) as RatingAvg, COUNT(wr.work_id) AS TotalReview');
        $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');

        $this->db->from('work_master w');

        //table join
        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');
        $this->db->join('location_master lm', 'lm.location_id = w.location_id', 'left');

        //like and where conditions
        if (isset($search)) {
            $this->db->where('w.name != ', '');
        } else {
            //$rawstring = findword($search);
            $this->db->like('w.name', $search, 'both');
            /*foreach ($rawstring as $key => $value) {
            if ($key == 0) {
            $this->db->like('w.name', $value, 'both');
            } else {
            $this->db->or_like('w.name', $value, 'both');
            }
            }*/

            //$this->db->like('w.name', $search, 'both');
            $this->db->or_like('w.address', $search, 'both');
            $this->db->or_like('w.phone', $search, 'both');
            $this->db->or_like('lm.name', $search, 'both');
        }
        $this->db->where('w.status', '1');

        $this->db->group_by('w.work_id');
        $this->db->order_by('RatingAvg DESC, FIELD(work_type, "Hospital", "Clinic", "Radiology Lab", "Medical Lab")');
        if ($perPage == '' || $page == '' || $page <= 0) {
            $perPage = 10;
            $page = 0;
        }
        $this->db->limit($perPage, $page);

        return $this->db->get()->result();
    }

    /**
     * Doctor search if lang is AR
     * @param  string $search
     * @param  string $lang
     * @param  string $userId
     * @param  string $page
     * @param  string $perPage
     * @return array
     */
    public function doctorMasterGlobalSearchAr($search, $lang, $userId, $page, $perPage)
    {
        $this->db->select('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name)) AS Name,dd.doctor_id AS WorkId, d.address AS Address, dd.phone_number AS Phone,group_concat(sm.mipmap) AS Mipmap, "Doctor" AS WorkType,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE dd.doctor_id=doctors_rating.doctor_id) as RatingAvg, COUNT(dr.doctor_id) AS TotalReview');

        $this->db->from('doctor_master d');

        //table join
        $this->db->join('doctor_details dd', 'dd.doctor_id = d.doctor_id', 'left');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
        $this->db->join('specialty_master sm', ("FIND_IN_SET(sm .specialties_id , dd.speciality_id)"), 'left');
        $this->db->join('bookmark_master bm', 'bm.doctor_id = d.doctor_id AND bm.user_id = ' . $userId, 'left');
        $this->db->join('location_master lm', 'lm.location_id = dd.location_id', 'left');

        //like and where conditions
        if (isset($search)) {
            $this->db->where('d.first_name != ', '');
            $this->db->or_where('d.last_name != ', '');
        } else {
            //$rawstring = findword($search);
            $this->db->like('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name))', $search, 'both');
            /*foreach ($rawstring as $key => $value) {
            if ($key == 0) {
            $this->db->like('d.first_name', $value, 'both');
            } else {
            $this->db->or_like('d.last_name', $value, 'both');
            $this->db->or_like('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name))', $value, 'both');
            $this->db->or_like('dd.phone_number', $value, 'both');
            $this->db->or_like('dd.email', $value, 'both');
            }
            }*/
            /*$this->db->like('d.first_name', $search, 'both');
        $this->db->or_like('d.last_name', $search, 'both');
        $this->db->or_like('dd.phone_number', $search, 'both');
        $this->db->or_like('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name))', $search, 'both');
        $this->db->or_like('dd.email', $search, 'both');
        $this->db->or_like('lm.name', $search, 'both');
        $this->db->or_like('sm.name', $search, 'both');*/
        }
        $this->db->where('dd.visibility', '1');
        $this->db->order_by('RatingAvg DESC, TotalReview DESC');

        $this->db->group_by('d.doctor_id');
        if ($perPage == '' || $page == '' || $page <= 0) {
            $perPage = 10;
            $page = 0;
        }
        $this->db->limit($perPage, $page);

        return $this->db->get()->result();
    }

    /**
     * Doctor search if lang is En or any other
     * @param  string $search
     * @param  string $lang
     * @param  string $userId
     * @param  string $page
     * @param  string $perPage
     * @return array
     */
    public function doctorMasterGlobalSearchEn($search, $lang, $userId, $page, $perPage)
    {
        if ($page <= 0) {
            $page = 1;
        }
        if ($userId == false || $userId == '' || $userId == null) {
            $userId = 0;
        }

        $this->db->select('CONCAT(TRIM(dm.first_name)," ", TRIM(dm.last_name)) AS Name,dd.doctor_id AS WorkId, dm.address AS Address, dd.phone_number AS Phone,group_concat(sm.mipmap) AS Mipmap,"Doctor" AS WorkType,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude,bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE dd.doctor_id=doctors_rating.doctor_id) as RatingAvg, COUNT(dr.doctor_id) AS TotalReview');

        //table join
        $this->db->from('doctor_master_en dm');
        $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
        //$this->db->join('work_master w', 'w.work_id = dd.work_id', 'right');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
        $this->db->join('doctor_master d', 'd.doctor_id = dm.doctor_id', 'left');
        $this->db->join('specialty_master sm', ("FIND_IN_SET(sm .specialties_id , dd.speciality_id)"), 'left');
        $this->db->join('bookmark_master bm', 'bm.doctor_id = dm.doctor_id AND bm.user_id = ' . $userId, 'left');
        $this->db->join('location_master lm', 'lm.location_id = dd.location_id', 'left');

        //like and where condition
        $this->db->where('dd.visibility', '1');
        /*$this->db->where(" (dm.first_name LIKE '%$search%'
        OR dm.last_name LIKE %$search%
        OR CONCAT(TRIM(dm.first_name), ' ', TRIM(dm.last_name)) LIKE %$search%
        OR dd.email LIKE %$search%
        OR lm_name_en LIKE %$search%
        OR sm.name_en LIKE %$search%) ");*/

        $this->db->group_start();
        $this->db->like('dm.first_name', $search, 'both');
        $this->db->or_like('dm.last_name', $search, 'both');
        $this->db->or_like('dd.phone_number', $search, 'both');
        $this->db->or_like('CONCAT(TRIM(dm.first_name)," ", TRIM(dm.last_name))', $search, 'both');
        $this->db->or_like('dd.email', $search, 'both');
        $this->db->or_like('lm.name_en', $search, 'both');
        $this->db->or_like('sm.name_en', $search, 'both');
        $this->db->group_end();
        $this->db->order_by('RatingAvg DESC, TotalReview DESC');

        $this->db->group_by('dm.doctor_id');
        if ($perPage == '' || $page == '' || $page <= 0) {
            $perPage = 10;
            $page = 0;
        }
        $this->db->limit($perPage, $page);

        return $this->db->get()->result();
    }

    /**
     * Global search for doctors
     * @param  string $search
     * @param  string $lang
     * @param  string $userId
     * @param  string $page
     * @param  string $perPage
     * @return array
     */
    public function doctorMasterGlobalSearch($search, $lang, $userId, $page, $perPage)
    {
        $page = ($page - 1);
        $page = ($page * $perPage);
        $result = array();
        if ($lang == 'ar') {
            $result = $this->doctorMasterGlobalSearchAr($search, $lang = 'ar', $userId, $page, $perPage = 10);
        } else {
            $result = $this->doctorMasterGlobalSearchEn($search, $lang = 'en', $userId, $page, $perPage = 10);
        }

        return $result;
    }

    /*function specialityGlobalSearch($search, $lang) {
    if($lang=='ar'){
    $this->db->select('CONCAT(d.first_name, d.last_name) AS Name,dd.work_id AS WorkId, d.address AS Address, dd.phone_number AS Phone, sm.name AS Speciality,group_concat(sm.mipmap) as Mipmap,w.work_type AS WorkType,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude');
    }else{
    $this->db->select('CONCAT(dm.first_name, dm.last_name) AS Name,dd.work_id AS WorkId, dm.address AS Address, dd.phone_number AS Phone, sm.name_en AS Speciality,group_concat(sm.mipmap) as Mipmap,w.work_type AS WorkType,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude');
    }
    $this->db->from('doctor_details dd'); //specialty_master sm
    $this->db->join('specialty_master sm',("FIND_IN_SET(sm .specialties_id , dd.speciality_id)"), 'left');
    $this->db->join('work_master w', 'w.work_id = dd.work_id', 'left'); //doctor_details dd
    $this->db->join('doctor_master_en dm', 'dm.doctor_id = dd.doctor_id', 'left'); //doctor_master_en dm
    $this->db->join('doctor_master d', 'd.doctor_id = dd.doctor_id', 'left');
    $this->db->like('sm.name_en', $search, 'both');
    $this->db->group_by('dm.doctor_id');
    return $this->db->get()->result();
    }*/

    /*function locationGlobalSearch($search, $lang) {
    if($lang=='ar'){
    $this->db->select('wm.name AS Name,wm.work_id AS WorkId,wm.address AS Address, wm.phone AS Phone,group_concat(sm.mipmap) as Mipmap,wm.work_type AS WorkType,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude');
    }else{
    $this->db->select('wm.name_en AS Name,wm.work_id AS WorkId,wm.address_en AS Address, wm.phone AS Phone,group_concat(sm.mipmap) as Mipmap,wm.work_type AS WorkType,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude');
    }
    $this->db->from('work_master wm');
    $this->db->join('doctor_details dd', 'dd.work_id = wm.work_id', 'left');
    $this->db->join('specialty_master sm',("FIND_IN_SET(sm .specialties_id , dd.speciality_id)"), 'left');
    $this->db->join('country_master cm', 'cm.country_id = dd.location_id', 'left');
    $this->db->like('cm.country_name', $search, 'both');
    $this->db->group_by('wm.work_id');
    return $this->db->get()->result();
    }*/

    public function mergerResult($primaryArray, $secondaryArray)
    {
        $result = $primaryArray;
        if (!empty($secondaryArray)) {
            $result = array_merge($primaryArray, $secondaryArray);
        }

        return $result;
    }

    /**
     * Hospital search in ar
     * @param  string $userId
     * @param  string $page
     * @param  string $perPage
     * @return array
     */
    public function hospitalSearchAr($userId, $page, $perPage)
    {
        $this->db->select('w.work_id AS HospitalId, w.name AS Name, w.address AS Address, w.phone AS Phone,wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude, (SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE w.work_id=work_rating.work_id ) as RatingAvg, COUNT(wr.work_id) AS TotalReview, "Hospital" AS WorkType');
        //$this->db->select('(SELECT GROUP_CONCAT(sm.name_en) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Speciality');
        $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');
        $this->db->from('work_master w');

        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');
        $this->db->where('w.work_type', 'Hospital');
        $this->db->where('w.status', '1');
        $this->db->where('w.name != ', '');
        $this->db->order_by('RatingAvg', 'DESC');
        $this->db->group_by('w.work_id');
        if ($perPage == '' || $page == '' || $page <= 0) {
            $perPage = 10;
            $page = 0;
        }
        $this->db->limit($perPage, $page);

        return $this->db->get()->result();
    }

    /**
     * Hospital search result in en
     * @param  string $userId
     * @param  string $page
     * @param  string $perPage
     * @return array
     */
    public function hospitalSearchEn($userId, $page, $perPage)
    {
        $this->db->select('w.work_id AS HospitalId, w.name_en AS Name, w.address_en AS Address, w.phone AS Phone,COUNT(wr.user_id) AS TotalReview,wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude, (SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE w.work_id=work_rating.work_id ) as RatingAvg, COUNT(wr.work_id) AS TotalReview, "Hospital" AS WorkType');
        //$this->db->select('(SELECT GROUP_CONCAT(sm.name_en) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Speciality');
        $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');
        $this->db->from('work_master w');

        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');
        $this->db->where('w.work_type', 'Hospital');
        $this->db->where('w.status', '1');
        $this->db->where('w.name_en !=', '');
        $this->db->order_by('RatingAvg', 'DESC');
        $this->db->group_by('w.work_id');
        if ($perPage == '' || $page == '' || $page <= 0) {
            $perPage = 10;
            $page = 0;
        }
        $this->db->limit($perPage, $page);

        return $this->db->get()->result();
    }

    /**
     * Hospital search result
     * @param  string $lang
     * @param  string $userId
     * @param  string $page
     * @param  string $perPage
     * @return array
     */
    public function hospitalSearch($lang, $userId, $page, $perPage = '10')
    {
        $page = ($page - 1);
        $page = ($page * $perPage);
        $result = array();
        if ($lang == 'ar') {
            $result = $this->hospitalSearchAr($userId, $page, $perPage);
        } else {
            $result = $this->hospitalSearchEn($userId, $page, $perPage);
        }

        return $result;
    }

    public function clinicSearch($lang, $userId, $page, $perPage)
    {

        $page = ($page - 1);
        $page = ($page * $perPage);
        if ($lang == 'ar') {
            $this->db->select('w.work_id AS ClinicId, w.name AS Name, w.address AS Address, w.phone AS Phone,wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude, (SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE w.work_id=work_rating.work_id ) as RatingAvg,COUNT(wr.work_id) AS TotalReview, "Clinic" AS WorkType, "" AS Speciality, "" AS Mipmap');
        } else {
            $this->db->select('w.work_id AS ClinicId, w.name_en AS Name, w.address_en AS Address, w.phone AS Phone,wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude, (SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE w.work_id=work_rating.work_id ) as RatingAvg,COUNT(wr.work_id) AS TotalReview, "Clinic" AS WorkType, "" AS Speciality, "" AS Mipmap');
        }
        $this->db->from('work_master w');

        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');
        $this->db->where('w.work_type', 'Clinic');
        $this->db->where('w.status', '1');
        $this->db->where('Name != ', '');
        $this->db->where('Address IS NOT NULL', null, false);
        $this->db->order_by('RatingAvg', 'DESC');
        $this->db->group_by('w.work_id');
        if ($perPage == '' || $page == '' || $page <= 0) {
            $perPage = 10;
            $page = 0;
        }

        $this->db->limit($perPage, $page);
        return $this->db->get()->result();
    }

    public function labSearch($filter)
    {
        $filter['page'] = ($filter['page'] - 1);
        $page = ($filter['page'] * $filter['perPage']);
        if ($filter['lang'] == 'ar') {
            $this->db->select('wm.work_id AS LabId, wm.name AS Name, wm.address AS Address, wm.phone AS Phone, wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude,COUNT(wr.work_id) AS TotalReview, (SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE wm.work_id=work_rating.work_id ) as RatingAvg, "Radiology Lab" AS WorkType, sm.name_en AS Speciality, sm.mipmap AS Mipmap');
        } else {
            $this->db->select('wm.work_id AS LabId, wm.name_en AS Name, wm.address_en AS Address, wm.phone AS Phone,wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude,COUNT(wr.work_id) AS TotalReview, (SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE wm.work_id=work_rating.work_id ) as RatingAvg, "Radiology Lab" AS WorkType, sm.name_en AS Speciality, sm.mipmap AS Mipmap');
        }
        $this->db->from('work_master wm');

        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id AND wbm.user_id = ' . $filter['userId'], 'left');
        $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
        $this->db->join('doctor_details dd', 'dd.work_id = wm.work_id', 'left');
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
        $this->db->where('work_type', $filter['speciality']);
        $this->db->where('wm.status', '1');
        if ($filter['lang'] == 'ar') {
            $this->db->where('wm.name != ', '');
        } else {
            $this->db->where('wm.name_en != ', '');
        }

        //$this->db->where('Address IS NOT NULL', NULL, FALSE);

        if (!empty($filter['gender'])) {
            if ($filters['lang'] == 'ar') {
                $this->db->where('dd.gender', $filters['lang']);
            } else {
                $this->db->where('dd.gender', $filters['lang']);
            }
        }

        /*if(!empty($filter['speciality'])) {
        if($filters['lang']=='ar'){
        $this->db->where('sm.name', $filter['speciality']);
        }else{
        $this->db->where('sm.name_en', $filter['speciality']);
        }
        }*/
        if ($filter['perPage'] == '' ||
            $page == '' || $page <= 0) {
            $filter['perPage'] = 10;
            $page = 0;
        }
        $this->db->order_by('RatingAvg', 'DESC');
        $this->db->limit($filter['perPage'], $page);
        $this->db->group_by('wm.work_id');
        $result = $this->db->get();
        return $result->result();
    }

    public function doctorSearch($filter)
    {

        $filter['page'] = ($filter['page'] - 1);
        $page = ($filter['page'] * $filter['perPage']);
        if ($filter['lang'] == 'ar') {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(d.first_name," ", d.last_name) AS Name, d.address AS Address, dd.phone_number AS Phone, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE dd.doctor_id=doctors_rating.doctor_id ) as RatingAvg, COUNT(dr.rating_id) AS TotalReview,sm.mipmap as Mipmap, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus');
        } else {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(dm.first_name," ", dm.last_name) AS Name, dm.address AS Address, dd.phone_number AS Phone, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE dd.doctor_id=doctors_rating.doctor_id ) as RatingAvg, COUNT(dr.rating_id) AS TotalReview, sm.mipmap as Mipmap, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus');
        }

        $this->db->from('doctor_details dd'); //doctor_master_en dm
        $this->db->join('doctor_master_en dm', 'dd.doctor_id = dm.doctor_id', 'left');
        $this->db->join('doctor_master d', 'd.doctor_id = dd.doctor_id', 'left');
        $this->db->join('location_master cm', 'cm.location_id = dd.location_id', 'left'); //country_master cm
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id ', 'left'); //
        $this->db->join('bookmark_master bm', 'bm.doctor_id = dd.doctor_id AND bm.user_id = ' . $filter['userId'], 'left'); //
        //$this->db->join('user_master um', 'bm.user_id = bm.user_id', 'left');
        if (!empty($filter['gender'])) {
            if ($filters['lang'] == 'ar') {
                $this->db->where('dd.gender', $filter['gender']);
            } else {
                $this->db->where('dd.gender', $filter['gender']);
            }

        }

        if (!empty($filter['location'])) {
            //var_dump($filter['childs']);exit;
            if ($filter['lang'] == 'ar') {
                $this->db->where('cm.name', $filter['location']);
                if (count($filter['childs']) > 0) {
                    $this->db->or_where_in('cm.name', $filter['childs']);
                }

            } else {
                $this->db->where('cm.name_en', $filter['location']);
                if (count($filter['childs']) > 0) {
                    $this->db->or_where_in('cm.name_en', $filter['childs']);
                }

            }
        }

        if (!empty($filter['speciality'])) {
            if ($filter['lang'] == 'ar') {
                $this->db->where('sm.name', $filter['speciality']);
            } else {
                $this->db->where('sm.name_en', $filter['speciality']);
            }
        }
        if ($filter['perPage'] == '' ||
            $page == '' || $page <= 0) {
            $filter['perPage'] = 10;
            $page = 0;
        }
        $this->db->where("CONCAT(dm.first_name,' ', dm.last_name) != ", ' ');
        $this->db->order_by('RatingAvg DESC, TotalReview DESC');
        $this->db->limit($filter['perPage'], $page);
        $this->db->group_by('dd.doctor_id');
        $result = $this->db->get();
        return $result->result();
    }

    public function bookmarkStatus()
    {
        $this->db->select('bm.status AS Status,dd.doctor_id AS DoctorId');
        $this->db->from('bookmark_master bm');
        $this->db->join('doctor_details dd', 'bm.doctor_id = dd.doctor_id', 'left');
        $this->db->group_by('bm.doctor_id');
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * Doctor's Total Number of Rows
     * $filter mixed array
     */
    public function doctorSearchNumRows($filter)
    {

        if ($filter['lang'] == 'ar') {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(d.first_name," ", d.last_name) AS Name, d.address AS Address, dd.phone_number AS Phone, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,sm.mipmap as Mipmap, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus');
        } else {
            $this->db->select('dm.doctor_id AS DoctorId, CONCAT(dm.first_name," ", dm.last_name) AS Name, dm.address AS Address, dd.phone_number AS Phone, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, dd.google_map_zoom AS Zoom, dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,sm.mipmap as Mipmap, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus');
        }

        $this->db->from('doctor_details dd'); //doctor_master_en dm
        $this->db->join('doctor_master_en dm', 'dd.doctor_id = dm.doctor_id', 'left');
        $this->db->join('doctor_master d', 'd.doctor_id = dd.doctor_id', 'left');
        $this->db->join('location_master cm', 'cm.location_id = dd.location_id', 'left'); //country_master cm
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id ', 'left'); //
        $this->db->join('bookmark_master bm', 'bm.doctor_id = dd.doctor_id AND bm.user_id = ' . $filter['userId'], 'left'); //
        $this->db->join('user_master um', 'bm.user_id = bm.user_id', 'left');
        if (!empty($filter['gender'])) {
            if ($filters['lang'] == 'ar') {
                $this->db->where('dd.gender', $filter['gender']);
            } else {
                $this->db->where('dd.gender', $filter['gender']);
            }

        }

        if (!empty($filter['location'])) {
            //var_dump($filter['childs']);exit;
            if ($filter['lang'] == 'ar') {
                $this->db->where('cm.name', $filter['location']);
                if (count($filter['childs']) > 0) {
                    $this->db->or_where_in('cm.name', $filter['childs']);
                }

            } else {
                $this->db->where('cm.name_en', $filter['location']);
                if (count($filter['childs']) > 0) {
                    $this->db->or_where_in('cm.name_en', $filter['childs']);
                }

            }
        }

        if (!empty($filter['speciality'])) {
            if ($filter['lang'] == 'ar') {
                $this->db->where('sm.name', $filter['speciality']);
            } else {
                $this->db->where('sm.name_en', $filter['speciality']);
            }
        }
        $this->db->where("CONCAT(dm.first_name,' ', dm.last_name) !=", ' ');
        $this->db->group_by('dd.doctor_id');
        $result = $this->db->get();
        return $result->num_rows();
    }

    /**
     * labsearchnumrows
     * @param mixed array $filter
     * @return type int
     */
    public function labSearchNumRows($filter)
    {

        if ($filter['lang'] == 'ar') {
            $this->db->select('wm.work_id AS LabId, wm.name AS Name, wm.address AS Address, wm.phone AS Phone, wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude,COUNT(wr.work_id) AS TotalReview,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg');
        } else {
            $this->db->select('wm.work_id AS LabId, wm.name_en AS Name, wm.address_en AS Address, wm.phone AS Phone,wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude,COUNT(wr.work_id) AS TotalReview,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg');
        }
        $this->db->from('work_master wm');

        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id AND wbm.user_id = ' . $filter['userId'], 'left');
        $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
        $this->db->join('doctor_details dd', 'dd.work_id = wm.work_id', 'left');
        $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
        $this->db->where('work_type', $filter['speciality']);
        $this->db->where('wm.status', '1');
        if ($filter['lang'] == 'ar') {
            $this->db->where('wm.name != ', '');
        } else {
            $this->db->where('wm.name_en != ', '');
        }

        //$this->db->where('Address IS NOT NULL', NULL, FALSE);

        if (!empty($filter['gender'])) {
            if ($filters['lang'] == 'ar') {
                $this->db->where('dd.gender', $filters['lang']);
            } else {
                $this->db->where('dd.gender', $filters['lang']);
            }
        }

        /*if(!empty($filter['speciality'])) {
        if($filters['lang']=='ar'){
        $this->db->where('sm.name', $filter['speciality']);
        }else{
        $this->db->where('sm.name_en', $filter['speciality']);
        }
        }*/
        $this->db->group_by('wm.work_id');
        $result = $this->db->get();
        return $result->num_rows();
    }

    /**
     *
     * @param char $lang
     * @param int $userId
     * @return mixed array
     */
    public function clinicSearchNumRows($lang, $userId)
    {

        if ($lang == 'ar') {
            $this->db->select('w.work_id AS ClinicId, w.name AS Name, w.address AS Address, w.phone AS Phone,wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg,COUNT(wr.work_id) AS TotalReview');
        } else {
            $this->db->select('w.work_id AS ClinicId, w.name_en AS Name, w.address_en AS Address, w.phone AS Phone,wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg,COUNT(wr.work_id) AS TotalReview');
        }
        $this->db->from('work_master w');

        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');
        $this->db->where('w.work_type', 'Clinic');
        $this->db->where('w.status', '1');
        $this->db->where('Name != ', '');
        $this->db->where('Address IS NOT NULL', null, false);
        $this->db->group_by('w.work_id');

        return $this->db->get()->num_rows();
    }

    /**
     * Total hospital search result in ar
     * @param  string $userId
     * @return int
     */
    public function hospitalSearchNumRowsAr($userId)
    {
        $this->db->select('w.work_id AS HospitalId, w.name AS Name, w.address AS Address, w.phone AS Phone,wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg, COUNT(wr.work_id) AS TotalReview');
        $this->db->from('work_master w');

        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');
        $this->db->where('w.work_type', 'Hospital');
        $this->db->where('w.status', '1');
        $this->db->where('w.name != ', '');
        $this->db->group_by('w.work_id');

        return $this->db->get()->num_rows();
    }

    /**
     * Total hospital search result in en
     * @param  string $userId
     * @return int
     */
    public function hospitalSearchNumRowsEn($userId)
    {
        $this->db->select('w.work_id AS HospitalId, w.name_en AS Name, w.address_en AS Address, w.phone AS Phone,COUNT(wr.user_id) AS TotalReview,wbm.status AS BookmarkStatus,wbm.user_id AS BookmarkUserId,"null" AS Latitude,"null" AS Longitude,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg, COUNT(wr.work_id) AS TotalReview');
        //$this->db->select('(SELECT GROUP_CONCAT(sm.name_en) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Speciality');
        //$this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');
        $this->db->from('work_master w');

        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');
        $this->db->where('w.work_type', 'Hospital');
        $this->db->where('w.status', '1');
        $this->db->where('w.name_en !=', '');
        $this->db->group_by('w.work_id');

        return $this->db->get()->num_rows();
    }

    /**
     * Total hospital search result
     * @param  string $lang
     * @param  string $userId
     * @return int
     */
    public function hospitalSearchNumRows($lang = 'en', $userId)
    {
        $count = 0;
        if ($lang == 'ar') {
            $count = $this->hospitalSearchNumRowsAr($userId);
        } else {
            $count = $this->hospitalSearchNumRowsEn($userId);
        }

        return $count;
    }

    /**
     * Total work master search result
     * @param  string $search
     * @param  string $lang
     * @param  string $userId
     * @return int
     */
    public function workMasterGlobalSearchNumRows($search, $lang, $userId)
    {
        $totalRows = 0;
        if ($lang == 'ar') {
            $totalRows = $this->workMasterGlobalSearchNumRowsAr($search, $lang, $userId);
        } else {
            $totalRows = $this->workMasterGlobalSearchNumRowsEn($search, $lang, $userId);
        }

        return $totalRows;
    }

    /**
     * Total work master search result for En and other lang
     * @param  string $search
     * @param  string $lang
     * @param  string $userId
     * @return int
     */
    public function workMasterGlobalSearchNumRowsEn($search, $lang, $userId)
    {
        $this->db->select('w.name_en AS Name,w.work_id AS WorkId, w.address_en AS Address, w.phone AS Phone, w.photo AS Photo,w.work_type AS WorkType,"null" AS Latitude,"null" AS Longitude,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg, COUNT(wr.work_id) AS TotalReview,wbm.user_id AS BookmarkUserId, wbm.status AS BookmarkStatus');
        $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');
        $this->db->from('work_master w');

        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('location_master lm', 'lm.location_id = w.location_id', 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');

        $this->db->like('w.name_en', $search, 'both');
        $this->db->or_like('w.address_en', $search, 'both');
        $this->db->or_like('w.phone', $search, 'both');
        $this->db->or_like('lm.name_en', $search, 'both');
        $this->db->where('w.status', '1');
        $this->db->group_by('w.work_id');
        //$this->db->order_by('FIELD(work_type, "Hospital", "Clinic", "Lab")');

        return $this->db->get()->num_rows();
    }

    /**
     * Total work master search result for lang Ar
     * @param  string $search
     * @param  string $lang
     * @param  string $userId
     * @return int
     */
    public function workMasterGlobalSearchNumRowsAr($search, $lang, $userId)
    {
        $this->db->select('w.name AS Name,w.work_id AS WorkId, w.address AS Address, w.phone AS Phone, w.photo AS Photo,w.work_type AS WorkType,"null" AS Latitude,"null" AS Longitude,SUM(wr.average_score)/COUNT(wr.user_id) as RatingAvg, COUNT(wr.work_id) AS TotalReview');
        $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');

        $this->db->from('work_master w');

        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('location_master lm', 'lm.location_id = w.location_id', 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');

        //like and join conditions
        if (isset($search)) {
            $this->db->where('w.name != ', '');
        } else {
            //$rawstring = findword($search);
            $this->db->like('w.name', $search, 'both');
            /*foreach ($rawstring as $key => $value) {
            if ($key == 0) {
            $this->db->like('w.name', $value, 'both');
            } else {
            $this->db->or_like('w.name', $value, 'both');
            }
            }*/
            $this->db->or_like('w.address', $search, 'both');
            $this->db->or_like('w.phone', $search, 'both');
            $this->db->or_like('lm.name', $search, 'both');
        }
        $this->db->where('w.status', '1');

        $this->db->group_by('w.work_id');
        //$this->db->order_by('FIELD(work_type, "Hospital", "Clinic", "Lab")');

        return $this->db->get()->num_rows();
    }

    /**
     * Total doctor global search result for Ar
     * @param  string $search
     * @param  string $userId
     * @return int
     */
    public function doctorMasterGlobalSearchNumRowsAr($search, $userId)
    {
        $this->db->select('CONCAT(d.first_name," ", d.last_name) AS Name,dd.doctor_id AS WorkId, d.address AS Address, dd.phone_number AS Phone,group_concat(sm.mipmap) AS Mipmap, "Doctor" AS WorkType,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude,SUM(dr.average_score)/COUNT(dr.user_id) as RatingAvg, COUNT(dr.doctor_id) AS TotalReview');
        $this->db->from('doctor_master d');
        $this->db->join('doctor_details dd', 'dd.doctor_id = d.doctor_id', 'left'); //doctor_details dd
        //$this->db->join('work_master w', 'w.work_id = dd.work_id', 'left'); //doctor_details dd
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left'); //doctor_details dd
        $this->db->join('specialty_master sm', ("FIND_IN_SET(sm .specialties_id , dd.speciality_id)"), 'left');
        $this->db->join('bookmark_master bm', 'bm.doctor_id = d.doctor_id AND bm.user_id = ' . $userId, 'left'); //doctor_details dd

        //like and join condition
        if (isset($search)) {
            $this->db->where('d.first_name != ', '');
            $this->db->or_where('d.last_name != ', '');
        } else {
            //$rawstring = findword($search);
            $this->db->or_like('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name))', $search, 'both');
            /*foreach ($rawstring as $key => $value) {
            if ($key == 0) {
            $this->db->like('d.first_name', $value, 'both');
            } else {
            $this->db->or_like('d.last_name', $value, 'both');
            $this->db->or_like('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name))', $value, 'both');
            $this->db->or_like('dd.phone_number', $value, 'both');
            $this->db->or_like('dd.email', $value, 'both');
            }
            }*/
            /*$this->db->like('d.first_name', $search, 'both');
        $this->db->or_like('d.last_name', $search, 'both');
        $this->db->or_like('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name))', $search, 'both');
        $this->db->or_like('dd.phone_number', $search, 'both');
        $this->db->or_like('dd.email', $search, 'both');*/
        }
        $this->db->where('dd.visibility', '1');
        $this->db->group_by('d.doctor_id');

        return $this->db->get()->num_rows();
    }

    /**
     * Total doctor global search result for En or any other lang
     * @param  string $search
     * @param  string $userId
     * @return int
     */
    public function doctorMasterGlobalSearchNumRowsEn($search, $userId)
    {
        $this->db->select('CONCAT(dm.first_name," ",dm.last_name) AS Name,dd.doctor_id AS WorkId, dm.address AS Address, dd.phone_number AS Phone,group_concat(sm.mipmap) AS Mipmap,"Doctor" AS WorkType,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude,bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,SUM(dr.average_score)/COUNT(dr.user_id) as RatingAvg, COUNT(dr.doctor_id) AS TotalReview');
        //table join
        $this->db->from('doctor_master_en dm');
        $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
        //$this->db->join('work_master w', 'w.work_id = dd.work_id', 'right');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
        $this->db->join('doctor_master d', 'd.doctor_id = dm.doctor_id', 'left');
        $this->db->join('specialty_master sm', ("FIND_IN_SET(sm .specialties_id , dd.speciality_id)"), 'left');
        $this->db->join('bookmark_master bm', 'bm.doctor_id = dm.doctor_id AND bm.user_id = ' . $userId, 'left');
        $this->db->join('location_master lm', 'lm.location_id = dd.location_id', 'left');

        //like and where condition
        $this->db->where('dd.visibility', '1');
        /*$this->db->where(" (dm.first_name LIKE '%$search%'
        OR dm.last_name LIKE %$search%
        OR CONCAT(TRIM(dm.first_name), ' ', TRIM(dm.last_name)) LIKE %$search%
        OR dd.email LIKE %$search%
        OR lm_name_en LIKE %$search%
        OR sm.name_en LIKE %$search%) ");*/

        $this->db->group_start();
        $this->db->like('dm.first_name', $search, 'both');
        $this->db->or_like('dm.last_name', $search, 'both');
        $this->db->or_like('dd.phone_number', $search, 'both');
        $this->db->or_like('CONCAT(TRIM(dm.first_name)," ", TRIM(dm.last_name))', $search, 'both');
        $this->db->or_like('dd.email', $search, 'both');
        $this->db->or_like('lm.name_en', $search, 'both');
        $this->db->or_like('sm.name_en', $search, 'both');
        $this->db->group_end();

        $this->db->group_by('dm.doctor_id');
        return $this->db->get()->num_rows();
    }

    /**
     * Total doctor global search result
     * @param  string $search
     * @param  string $lang
     * @param  string $userId
     * @return int
     */
    public function doctorMasterGlobalSearchNumRows($search, $lang, $userId)
    {
        $totalRows = 0;
        if ($lang == 'ar') {
            $totalRows = $this->doctorMasterGlobalSearchNumRowsAr($search, $userId);
        } else {
            $totalRows = $this->doctorMasterGlobalSearchNumRowsEn($search, $userId);
        }

        return $totalRows;
    }

    public function searchNumRows($filters)
    {

        if ($filters['searchType'] == 'doctor') {

            if (!empty($filters['speciality'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('sm.name', $filters['speciality']);
                } else {
                    $this->db->where('sm.name_en', $filters['speciality']);
                }

            }

            if (!empty($filters['location'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('cm.name', $filters['location']);
                } else {
                    $this->db->where('cm.name_en', $filters['location']);
                }

            }

            if (!empty($filters['gender'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('dd.gender', $filters['gender']);
                } else {
                    $this->db->where('dd.gender', $filters['gender']);
                }
            }

            if (!empty($filters['name'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->like("d.first_name", $filters['name']);
                    $this->db->or_like("d.last_name", $filters['name']);
                } else {
                    $this->db->like("dm.first_name", $filters['name']);
                    $this->db->or_like("dm.last_name", $filters['name']);
                }
            }

            if ($filters['lang'] == 'ar') {

                $result = $this->db->select('dm.doctor_id AS DoctorId, CONCAT(d.first_name, " ", d.last_name) AS Name, d.address AS Address, dd.phone_number AS Phone,sm.mipmap as Mipmap, dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus');

            } else {
                $this->db->select('dm.doctor_id AS DoctorId, CONCAT(dm.first_name, " ", dm.last_name) AS Name, dm.address AS Address, dd.phone_number AS Phone,sm.mipmap as Mipmap, dr.average_score as RatingAvg, COUNT(dr.rating_id) AS TotalReview,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus');
            }
            $this->db->from('doctor_master_en dm');
            $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
            $this->db->join('doctor_master d', 'd.doctor_id = dm.doctor_id', 'left');
            $this->db->join('specialty_master sm', 'sm.specialties_id = dd.speciality_id', 'left');
            $this->db->join('location_master cm', 'cm.location_id = dd.location_id', 'left');
            $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
            $this->db->join('bookmark_master bm', 'bm.doctor_id = dd.doctor_id AND bm.user_id = ' . $filters['userId'], 'left');
            $this->db->group_by('dm.doctor_id');
            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }
            if ($filters['isnearby'] == 1) {
                if ($filters['lat'] != "" || $filters['long'] != "") {

                    $this->db->where("(6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filters['lat'] . " - dd.google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filters['lat'] . " * PI() / 180)
                                                            * COS(dd.google_map_latitude * PI() / 180) * POWER(SIN((" . $filters['long'] . " - dd.google_map_longtude) * PI() / 180 / 2), 2))) <= '10')");
                }
            }
            return $this->db->get()->num_rows();

        } else if ($filters['searchType'] == 'lab') {
            if (!empty($filters['speciality'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('sm.name', $filters['speciality']);
                } else {
                    $this->db->where('sm.name_en', $filters['speciality']);
                }
            }
            if (!empty($filters['location'])) {

                if ($filters['lang'] == 'ar') {
                    $this->db->where('cm.name', $filters['location']);
                } else {
                    $this->db->where('cm.name_en', $filters['location']);
                }

            }
            if (!empty($filters['name'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->or_like("wm.name", $filters['name']);
                } else {
                    $this->db->or_like("wm.name_en", $filters['name']);
                }

            }

            if ($filters['lang'] == 'ar') {

                $this->db->select("wm.work_id AS WorkId, wm.name AS Name, wm.address AS Address, wm.work_type AS WorkType, wm.phone AS Phone,wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,'null' AS Latitude,'null' AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus ");
            } else {

                $this->db->select("wm.work_id AS WorkId, wm.name_en AS Name, wm.address_en AS Address, wm.work_type AS WorkType, wm.phone AS Phone, wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,'null' AS Latitude,'null' AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus");

            }
            $this->db->from("work_master wm");

            $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = wm.work_id)AS Mipmap');

            //$this->db->join("doctor_master_en dm", "dm.doctor_id = wm.doctor_id", "left");
            $this->db->join('location_master cm', 'cm.location_id = wm.location_id', 'left');
            $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id AND wbm.user_id = ' . $filters['userId'], 'left');
            $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
            $this->db->group_by('wm.work_id');
            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }
            $this->db->where('wm.work_type', 'Lab');
            return $this->db->get()->num_rows();

        } else if ($filters['searchType'] == 'hospital') {
            if (!empty($filters['speciality'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('sm.name', $filters['speciality']);
                } else {
                    $this->db->where('sm.name_en', $filters['speciality']);
                }
            }
            if (!empty($filters['location'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('cm.name', $filters['location']);
                } else {
                    $this->db->where('cm.name_en', $filters['location']);
                }
            }
            if (!empty($filters['name'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->or_like("wm.name", $filters['name']);
                } else {
                    $this->db->or_like("wm.name_en", $filters['name']);
                }
            }
            if ($filters['lang'] == 'ar') {

                $this->db->select("wm.work_id AS WorkId, wm.name AS Name, wm.address AS Address, wm.work_type AS WorkType, wm.phone AS Phone, wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,'null' AS Latitude,'null' AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus");
            } else {

                $this->db->select("wm.work_id AS WorkId, wm.name_en AS Name, wm.address_en AS Address, wm.work_type AS WorkType, wm.phone AS Phone, wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,'null' AS Latitude,'null' AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus");
            }
            $this->db->from("work_master wm");

            $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = wm.work_id)AS Mipmap');

            //$this->db->join("doctor_master_en dm", "dm.doctor_id = wm.doctor_id", "left");
            $this->db->join('location_master cm', 'cm.location_id = wm.location_id', 'left');
            $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id AND wbm.user_id = ' . $filters['userId'], 'left');
            $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
            $this->db->group_by('wm.work_id');
            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }
            $this->db->where('wm.work_type', 'Hospital');
            return $this->db->get()->num_rows();

        } else if ($filters['searchType'] == 'clinic') {

            if (!empty($filters['speciality'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('sm.name', $filters['speciality']);
                } else {
                    $this->db->where('sm.name_en', $filters['speciality']);
                }
            }
            if (!empty($filters['location'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->where('cm.name', $filters['location']);
                } else {
                    $this->db->where('cm.name_en', $filters['location']);
                }
            }
            if (!empty($filters['name'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->or_like("wm.name", $filters['name']);
                } else {
                    $this->db->or_like("wm.name_en", $filters['name']);
                }
            }
            if ($filters['lang'] == 'ar') {
                $this->db->select("wm.work_id AS WorkId, wm.name AS Name, wm.address AS Address, wm.work_type AS WorkType, wm.phone AS Phone,wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus");
            } else {
                $this->db->select("wm.work_id AS WorkId, wm.name_en AS Name, wm.address_en AS Address, wm.work_type AS WorkType, wm.phone AS Phone,wr.average_score as RatingAvg, COUNT(wr.work_id) AS TotalReview,'null' AS Latitude,'null' AS Longitude,wbm.user_id AS BookmarkUserId,wbm.status AS BookmarkStatus");
            }
            $this->db->from("work_master wm");

            $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = wm.work_id)AS Mipmap');
            //$this->db->join("doctor_master_en dm", "dm.doctor_id = wm.doctor_id", "left");
            $this->db->join('location_master cm', 'cm.location_id = wm.location_id', 'left');
            $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = wm.work_id AND wbm.user_id = ' . $filters['userId'], 'left');
            $this->db->join('work_rating wr', 'wr.work_id = wm.work_id', 'left');
            $this->db->group_by('wm.work_id');
            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }
            $this->db->where('wm.work_type', 'Clinic');
            return $this->db->get()->num_rows();
        }

        //work_master w
        if ($filters['lang'] == 'ar') {
            $this->db->select('w.work_id AS WorkId, w.work_type AS WorkType, w.name AS Name, w.address AS Address, w.phone AS Phone, specialty_master.name AS Speciality, doctor_details.photo AS Photo,specialty_master.mipmap as Mipmap, dr.average_score as RatingAvg,COUNT(dr.rating_id) AS TotalReview,doctor_details.google_map_latitude AS Latitude,doctor_details.google_map_longtude AS Longitude');
        } else {
            $this->db->select('w.work_id AS WorkId, w.work_type AS WorkType, w.name_en AS Name, w.address_en AS Address, w.phone AS Phone, specialty_master.name_en AS Speciality, doctor_details.photo AS Photo,specialty_master.mipmap as Mipmap, dr.average_score as RatingAvg,COUNT(dr.rating_id) AS TotalReview,doctor_details.google_map_latitude AS Latitude,doctor_details.google_map_longtude AS Longitude');
        }

        $this->db->from('work_master w');
        $this->db->join('doctor_master_en', 'doctor_master_en.doctor_id = w.doctor_id', 'left');
        $this->db->join('doctor_details', 'doctor_details.work_id = w.work_id', 'left');
        $this->db->join('doctor_master', 'doctor_master.doctor_id = doctor_details.doctor_id', 'left');
        $this->db->join('specialty_master', 'specialty_master.specialties_id = doctor_details.speciality_id', 'left');
        $this->db->join('location_master', 'location_master.location_id = doctor_details.location_id', 'left');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = doctor_details.doctor_id', 'left');
        $this->db->group_by('doctor_master_en.doctor_id');

        if (!empty($filters['name'])) {
            if ($filters['lang'] == 'ar') {

                $this->db->like('doctor_master.first_name', $filters['name']);
                $this->db->or_like('doctor_master.last_name', $filters['name']);
                $this->db->or_like('w.name', $filters['name']);

            } else {

                $this->db->like('doctor_master_en.first_name', $filters['name']);
                $this->db->or_like('doctor_master_en.last_name', $filters['name']);
                $this->db->or_like('w.name_en', $filters['name']);

            }

        }

        if (!empty($filters['speciality'])) {
            if ($filters['lang'] == 'ar') {
                $this->db->where('specialty_master.name', $filters['speciality']);
            } else {
                $this->db->where('specialty_master.name_en', $filters['speciality']);
            }
            if (!empty($filters['search'])) {
                if ($filters['lang'] == 'ar') {

                    $this->db->like('doctor_master.first_name', $filters['search']);
                    $this->db->or_like('doctor_master.last_name', $filters['search']);
                    $this->db->or_like('w.name', $filters['search']);

                } else {
                    $this->db->like('doctor_master_en.first_name', $filters['search']);
                    $this->db->or_like('doctor_master_en.last_name', $filters['search']);
                    $this->db->or_like('w.name_en', $filters['search']);
                }
            }
        }

        if (!empty($filters['location'])) {
            if ($filters['lang'] == 'ar') {
                $this->db->where('location_master.name', $filters['location']);
            } else {
                $this->db->where('location_master.name_en', $filters['location']);
            }

            if (!empty($filters['search'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->like('doctor_master.first_name', $filters['search']);
                    $this->db->or_like('doctor_master.last_name', $filters['search']);
                    $this->db->or_like('w.name', $filters['search']);
                } else {
                    $this->db->like('doctor_master_en.first_name', $filters['search']);
                    $this->db->or_like('doctor_master_en.last_name', $filters['search']);
                    $this->db->or_like('w.name_en', $filters['search']);
                }
            }
        }

        if (!empty($filters['gender'])) {
            if ($filters['lang'] == 'ar') {
                $this->db->where('doctor_details.gender', $filters['gender']);
            } else {
                $this->db->where('doctor_details.gender', $filters['gender']);
            }

            if (!empty($filters['search'])) {
                if ($filters['lang'] == 'ar') {
                    $this->db->like('doctor_master.first_name', $filters['search']);
                    $this->db->or_like('doctor_master.last_name', $filters['search']);
                    $this->db->or_like('w.name', $filters['search']);
                } else {
                    $this->db->like('doctor_master_en.first_name', $filters['search']);
                    $this->db->or_like('doctor_master_en.last_name', $filters['search']);
                    $this->db->or_like('w.name_en', $filters['search']);
                }

            }
        }
        if ($filters['topratestfirst'] == 1) {

            $this->db->order_by('RatingAvg', 'DESC');
        }
        $result = $this->db->get();
        return $result->num_rows();
    }

    public function filterSearchNumRows($filters)
    {

        //   $doctors = array();
        //  $work = array();
        if ($filters['searchType'] != "") {
            if ($filters['searchType'] == "doctor") {

                if ($filters['lang'] == 'ar') {

                    $result = $this->db->select('dm.doctor_id AS Id, CONCAT(d.first_name, " ", d.last_name) AS Name, d.address AS Address, dd.phone_number AS Phone,sm.mipmap as Mipmap, dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,( select SUM(dr.average_score)/COUNT(dr.user_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) as RatingAvg, (select COUNT(dr.doctor_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) AS TotalReview,dd.working_hours AS WorkingHour, "doctor" as work_type');

                } else {
                    $this->db->select('dm.doctor_id AS Id, CONCAT(dm.first_name, " ", dm.last_name) AS Name, dm.address AS Address, dd.phone_number AS Phone,sm.mipmap as Mipmap,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,( select SUM(dr.average_score)/COUNT(dr.user_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) as RatingAvg, (select COUNT(dr.doctor_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) AS TotalReview,dd.working_hours AS workinghours,  "doctor" as work_type');
                }

                if ($filters['isnearby'] == 1) {
                    if ($filters['lat'] != "" || $filters['long'] != "") {
                        $this->db->select('SQRT(POW(69.1 * (dd.google_map_latitude - ' . $filters["lat"] . '), 2) + POW(69.1 * (' . $filters["long"] . ' - dd.google_map_longtude) * COS(dd.google_map_latitude / 57.3), 2)) AS distance');
                    }
                }

                $this->db->from('doctor_master_en dm');
                $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
                $this->db->join('doctor_master d', 'd.doctor_id = dm.doctor_id', 'left');
                $this->db->join('location_master lm', 'lm.location_id=dd.location_id', 'left');
                $this->db->join('specialty_master sm', 'sm.specialties_id=dd.speciality_id', 'left');
                $this->db->join('bookmark_master bm', 'bm.doctor_id=dd.doctor_id', 'left');
                $this->db->join('user_master um', 'um.user_id=bm.user_id', 'left');
                //$this->db->join('doctors_rating dr','dr.doctor_id=d.doctor_id','left');
                if ($filters['location'] != "" || $filters['speciality'] != "" || $filters['isnearby'] == 1 || $filters['topratestfirst'] == 1 || $filters['name'] != "" || $filters['gender'] != "") {
                    $this->db->group_start();
                    $this->db->where('dd.visibility', '1');
                }
                if ($filters['location'] != "") {

                    if ($filters['lang'] == "ar") {
                        $this->db->where('lm.name', $filters['location']);
                        if (count($filters['childs']) > 0) {
                            $this->db->or_where_in('lm.name', $filters['childs']);
                        }

                    } else {
                        $this->db->where('lm.name_en', $filters['location']);
                        if (count($filters['childs']) > 0) {
                            $this->db->or_where_in('lm.name_en', $filters['childs']);
                        }

                    }
                }

                if ($filters['speciality'] != "") {
                    if ($filters['lang'] == "ar") {
                        $this->db->where('sm.name', $filters['speciality']);
                    } else {
                        $this->db->where('sm.name_en', $filters['speciality']);
                    }

                }
                if ($filters['isnearby'] == 1) {
                    if ($filters['lat'] != "" || $filters['long'] != "") {
                        $this->db->order_by('distance', 'ASC');
                        $this->db->having('distance <= ', 10);
                    }
                    /*{

                $this->db->where("(6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filters['lat'] . " - dd.google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filters['lat'] . " * PI() / 180)
                 * COS(dd.google_map_latitude * PI() / 180) * POWER(SIN((" . $filters['long'] . " - dd.google_map_longtude) * PI() / 180 / 2), 2))) <= '10')");
                }*/
                }
                /*if($filters['isnearby']==1)
                {
                if($filters['lat']!="" || $filters['long']!="")
                {

                $this->db->where("(6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filters['lat'] . " - dd.google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filters['lat'] . " * PI() / 180)
                 * COS(dd.google_map_latitude * PI() / 180) * POWER(SIN((" . $filters['long'] . " - dd.google_map_longtude) * PI() / 180 / 2), 2))) <= '10')");
                }
                }*/
                if ($filters['topratestfirst'] == 1) {

                    $this->db->order_by('RatingAvg', 'DESC');
                }
                if ($filters['name'] != "") {

                    /*doctor_master_en.first_name LIKE '%".$data['searchname']."%' OR doctor_master_en.last_name LIKE '%".$data['searchname']."%'  or concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$data['searchname']."%'"*/
                    if ($filters['lang'] == "ar") {
                        //$rawstring = findword($filters['name']);
                        $this->db->like('CONCAT(d.first_name, " ", d.last_name)', $filters['name'], 'both');
                        /*foreach ($rawstring as $key => $value) {
                        if ($key == 0) {
                        $this->db->like('d.first_name', $value, 'left');
                        } else {
                        $this->db->or_like('d.last_name', $value, 'left');
                        $this->db->or_like('CONCAT(d.first_name, " ", d.last_name)', $value, 'left');
                        }
                        }*/
                        /*$this->db->where("d.first_name LIKE '%".$filters['name']."%' OR d.last_name LIKE '%".$filters['name']."%'  or concat_ws(' ',d.first_name,d.last_name) like '%".$filters['name']."%'");*/
                    } else {
                        $this->db->where("dm.first_name LIKE '%" . $filters['name'] . "%' OR dm.last_name LIKE '%" . $filters['name'] . "%'  or concat_ws(' ',dm.first_name,dm.last_name) like '%" . $filters['name'] . "%'");
                    }
                    //$this->db->like('dm.first_name',$filters['name']);
                    //$this->db->or_like('dm.last_name',$filters['name']);
                }
                if ($filters['gender'] != "") {
                    $this->db->where('dd.gender', $filters['gender']);
                }

                if ($filters['location'] != "" || $filters['speciality'] != "" || $filters['isnearby'] == 1 || $filters['topratestfirst'] == 1 || $filters['name'] != "" || $filters['gender'] != "") {
                    $this->db->group_end();
                }
                $this->db->where('dd.visibility', '1');
                $doctors = $this->db->get()->num_rows();
                //  echo $this->db->last_query();
                //die;

            }
            if ($filters['searchType'] == "hospital" || $filters['searchType'] == "clinic" || $filters['searchType'] == "Radiology Lab" || $filters['searchType'] == "Medical Lab" || $filters['searchType'] == "lab") {

                if ($filters['lang'] == 'ar') {

                    $this->db->select('w.work_id AS Id, w.name AS Name, w.address AS Address, w.phone AS Phone, "null" AS Latitude,"null" AS Longitude , bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,( select SUM(wr.average_score)/COUNT(wr.user_id) from work_rating wr where w.work_id = wr.work_id) as RatingAvg, (select COUNT(wr.work_id) from work_rating wr where w.work_id = wr.work_id) AS TotalReview, "null" AS WorkingHour, w.work_type as WorkType');

                } else {
                    $this->db->select('w.work_id AS Id, w.name_en AS Name, w.address_en AS Address, w.phone AS Phone, "null" AS Latitude,"null" AS Longitude , bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,( select SUM(wr.average_score)/COUNT(wr.user_id) from work_rating wr where w.work_id = wr.work_id) as RatingAvg, (select COUNT(wr.work_id) from work_rating wr where w.work_id = wr.work_id) AS TotalReview, "null" AS WorkingHour, w.work_type as WorkType');
                }
                $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');

                if ($filters['isnearby'] == 1) {
                    if ($filters['lat'] != "" || $filters['long'] != "") {
                        $this->db->select('SQRT(POW(69.1 * (dd.google_map_latitude - ' . $filters["lat"] . '), 2) + POW(69.1 * (' . $filters["long"] . ' - dd.google_map_longtude) * COS(dd.google_map_latitude / 57.3), 2)) AS distance');
                    }
                }

                $this->db->from('work_master w');
                $this->db->where('w.work_type', $filters['searchType']);
                $this->db->join('location_master lm', 'lm.location_id=w.location_id', 'left');
                $this->db->join('doctor_details dd', 'dd.work_id = w.work_id', 'left');
                $this->db->join('specialty_master sm', 'sm.specialties_id=dd.speciality_id', 'left');
                $this->db->join('worktype_bookmark_master bm', 'bm.work_id=w.work_id', 'left');
                $this->db->join('user_master um', 'um.user_id=bm.user_id', 'left');

                //for lab only
                if ($filters['searchType'] == "Radiology Lab" ||
                    $filters['searchType'] == "Medical Lab") {

                } else {
                    //for other
                    if ($filters['speciality'] != "") {

                        if ($filters['lang'] == "ar") {
                            //$where = "FIND_IN_SET('".$filters['speciality']."', Spec)";
                            //$this->db->where($where);
                            //
                            $this->db->where('sm.name', $filters['speciality']);
                        } else {
                            //$this->db->where_in('sm.name_en', $filters['speciality']);
                            //$where = "FIND_IN_SET('".$filters['speciality']."', Spec)";
                            //$this->db->where($where);
                            $this->db->where('sm.name_en', $filters['speciality']);
                        }
                    }
                }
                if ($filters['location'] != "") {
                    // echo $filters['location'];die;
                    if ($filters['lang'] == "ar") {
                        $this->db->where('lm.name', $filters['location']);
                    } else {
                        $this->db->where('lm.name_en', $filters['location']);
                    }
                }
                if ($filters['name'] != "") {
                    if ($filters['lang'] == "ar") {
                        $this->db->like("w.name", $filters['name'], 'both');
                    } else {
                        $this->db->like("w.name_en", $filters['name'], 'both');
                    }

                }

                if ($filters['isnearby'] == 1) {
                    if ($filters['lat'] != "" || $filters['long'] != "") {
                        $this->db->order_by('distance', 'ASC');
                        $this->db->having('distance <= ', 10);
                        /*$this->db->where("(6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filters['lat'] . " - dd.google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filters['lat'] . " * PI() / 180)
                     * COS(dd.google_map_latitude * PI() / 180) * POWER(SIN((" . $filters['long'] . " - dd.google_map_longtude) * PI() / 180 / 2), 2))) <= '10')");*/
                    }
                }

                if ($filters['topratestfirst'] == 1) {

                    $this->db->order_by('RatingAvg', 'DESC');
                }
                $this->db->where('w.status', '1');
                $this->db->group_by('w.work_id');

                $work = $this->db->get()->num_rows();

            }

            $total_result = ($doctors + $work);
        } else {
            if ($filters['lang'] == 'ar') {
                $result = $this->db->select('dm.doctor_id AS Id, CONCAT(TRIM(d.first_name), " ", TRIM(d.last_name)) AS Name, d.address AS Address, dd.phone_number AS Phone,sm.mipmap as Mipmap, dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,( select SUM(dr.average_score)/COUNT(dr.user_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) as RatingAvg, (select COUNT(dr.doctor_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) AS TotalReview,dd.working_hours AS WorkingHour, "doctor" as WorkType');
            } else {
                $this->db->select('dm.doctor_id AS Id, CONCAT(TRIM(dm.first_name), " ", TRIM(dm.last_name)) AS Name, dm.address AS Address, dd.phone_number AS Phone,sm.mipmap as Mipmap,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,( select SUM(dr.average_score)/COUNT(dr.user_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) as RatingAvg, (select COUNT(dr.doctor_id) from doctors_rating dr where dd.doctor_id = dr.doctor_id) AS TotalReview,dd.working_hours AS WorkingHour,  "doctor" as WorkType');
            }

            if ($filters['isnearby'] == 1) {
                if ($filters['lat'] != "" || $filters['long'] != "") {
                    $this->db->select('SQRT(POW(69.1 * (dd.google_map_latitude - ' . $filters["lat"] . '), 2) + POW(69.1 * (' . $filters["long"] . ' - dd.google_map_longtude) * COS(dd.google_map_latitude / 57.3), 2)) AS distance');
                }
            }

            if ($filters['location'] != "" || $filters['speciality'] != "" || $filters['isnearby'] == 1 || $filters['topratestfirst'] == 1 || $filters['name'] != "" || $filters['gender'] != "") {
                $this->db->group_start();
                $this->db->where('dd.visibility', '1');
            }

            $this->db->from('doctor_master_en dm');
            $this->db->join('doctor_details dd', 'dd.doctor_id = dm.doctor_id', 'left');
            $this->db->join('doctor_master d', 'd.doctor_id = dm.doctor_id', 'left');
            $this->db->join('location_master lm', 'lm.location_id=dd.location_id', 'left');
            $this->db->join('specialty_master sm', 'sm.specialties_id=dd.speciality_id', 'left');
            $this->db->join('bookmark_master bm', 'bm.doctor_id=dd.doctor_id AND bm.user_id = ' . $filters['userId'], 'left');
            $this->db->join('user_master um', 'um.user_id=bm.user_id', 'left');

            //$this->db->join('doctors_rating dr','dr.doctor_id=d.doctor_id','left');
            if ($filters['location'] != "") {

                if ($filters['lang'] == "ar") {
                    $this->db->where('lm.name', $filters['location']);
                    if (count($filters['childs']) > 0) {
                        $this->db->or_where_in('lm.name', $filters['childs']);
                    }

                } else {
                    $this->db->where('lm.name_en', $filters['location']);
                    if (count($filters['childs']) > 0) {
                        $this->db->or_where_in('lm.name_en', $filters['childs']);
                    }

                }
            }

            if ($filters['speciality'] != "") {
                if ($filters['lang'] == "ar") {
                    $this->db->where('sm.name', $filters['speciality']);
                } else {
                    $this->db->where('sm.name_en', $filters['speciality']);
                }

            }
            if ($filters['isnearby'] == 1) {
                if ($filters['lat'] != "" || $filters['long'] != "") {
                    $this->db->order_by('distance', 'ASC');
                    $this->db->having('distance <= ', 10);
                }
                /*{

            $this->db->where("(6371.0 * 2 * ASIN(SQRT(POWER(SIN((" . $filters['lat'] . " - dd.google_map_latitude) * PI() / 180 / 2), 2) + COS(" . $filters['lat'] . " * PI() / 180)
             * COS(dd.google_map_latitude * PI() / 180) * POWER(SIN((" . $filters['long'] . " - dd.google_map_longtude) * PI() / 180 / 2), 2))) <= '10')");
            }*/
            }
            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }
            if ($filters['name'] != "") {

                /*doctor_master_en.first_name LIKE '%".$data['searchname']."%' OR doctor_master_en.last_name LIKE '%".$data['searchname']."%'  or concat_ws(' ',doctor_master_en.first_name,doctor_master_en.last_name) like '%".$data['searchname']."%'"*/
                if ($filters['lang'] == "ar") {
                    //$rawstring = findword($filters['name']);
                    $this->db->or_like('CONCAT(TRIM(d.first_name), " ", TRIM(d.last_name))', $filters['name'], 'left');
                    /*foreach ($rawstring as $key => $value) {
                    if ($key == 0) {
                    $this->db->like('d.first_name', $value, 'left');
                    } else {
                    $this->db->or_like('d.last_name', $value, 'left');
                    $this->db->or_like('CONCAT(TRIM(d.first_name), " ", TRIM(d.last_name))', $value, 'left');
                    }
                    }*/
                    /*$this->db->where("d.first_name LIKE '%".$filters['name']."%' OR d.last_name LIKE '%".$filters['name']."%'  or concat_ws(' ',d.first_name,d.last_name) like '%".$filters['name']."%'");*/
                } else {
                    $this->db->where("dm.first_name LIKE '%" . $filters['name'] . "%' OR dm.last_name LIKE '%" . $filters['name'] . "%'  or concat_ws(' ',dm.first_name,dm.last_name) like '%" . $filters['name'] . "%'");
                }
                //$this->db->like('dm.first_name',$filters['name']);
                //$this->db->or_like('dm.last_name',$filters['name']);
            }

            //$this->db->where('dd.visibility','1');
            if ($filters['gender'] != "") {
                $this->db->where('dd.gender', $filters['gender']);
            }

            if ($filters['location'] != "" || $filters['speciality'] != "" || $filters['isnearby'] == 1 || $filters['topratestfirst'] == 1 || $filters['name'] != "" || $filters['gender'] != "") {
                $this->db->group_end();
            }
            $this->db->where('dd.visibility', '1');

            if ($page == '' || $page <= 0) {
                $filters['perPage'] = 10;
                $page = 0;
            }
            $this->db->limit($filters['perPage'], $page);
            $this->db->group_by('Id');
            $doctors = $this->db->get()->num_rows();
            /******** end doctor  ***/

            /**   work   **/
            if ($filters['lang'] == 'ar') {

                $this->db->select('w.work_id AS Id, w.name AS Name, w.address AS Address, w.phone AS Phone, "null" AS Latitude,"null" AS Longitude , bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,( select SUM(wr.average_score)/COUNT(wr.user_id) from work_rating wr where w.work_id = wr.work_id) as RatingAvg, (select COUNT(wr.work_id) from work_rating wr where w.work_id = wr.work_id) AS TotalReview, "null" AS WorkingHour, w.work_type as work_type');

            } else {
                $this->db->select('w.work_id AS Id, w.name_en AS Name, w.address_en AS Address, w.phone AS Phone, "null" AS Latitude,"null" AS Longitude , bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus,( select SUM(wr.average_score)/COUNT(wr.user_id) from work_rating wr where w.work_id = wr.work_id) as RatingAvg, (select COUNT(wr.work_id) from work_rating wr where w.work_id = wr.work_id) AS TotalReview, "null" AS WorkingHour, w.work_type as work_type');
            }
            $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd
 left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');
            $this->db->from('work_master w');
            $this->db->where('w.status', '1');
            $this->db->join('location_master lm', 'lm.location_id=w.location_id', 'left');
            $this->db->join('specialty_master sm', 'sm.specialties_id=w.speciality', 'left');
            $this->db->join('worktype_bookmark_master bm', 'bm.work_id=w.work_id AND bm.user_id = ' . $filters['userId'], 'left');
            $this->db->join('user_master um', 'um.user_id=bm.user_id', 'left');
            if ($filters['speciality'] != "") {
                if ($filters['lang'] == "ar") {
                    $this->db->or_like('sm.name', $filters['speciality']);
                } else {
                    $this->db->or_like('sm.name_en', $filters['speciality']);
                }
            }
            if ($filters['location'] != "") {
                // echo $filters['location'];die;
                if ($filters['lang'] == "ar") {
                    $this->db->where('lm.name', $filters['location']);
                } else {
                    $this->db->where('lm.name_en', $filters['location']);
                }
            }
            if ($filters['name'] != "") {
                if ($filters['lang'] == "ar") {
                    $this->db->like("w.name", $filters['name']);
                } else {
                    $this->db->like("w.name_en", $filters['name']);
                }

            }
            if ($filters['topratestfirst'] == 1) {

                $this->db->order_by('RatingAvg', 'DESC');
            }

            $work = $this->db->get()->num_rows();

            /*** end work ***/

            $total_result = ($doctors + $work);

            //$this->db;
        }
        return $total_result;

    }

    public function global_search_new($search, $lang, $userId, $page, $perPage)
    {
        $result = array();
        $doctor_result = $this->doctor_global_search_result($search, $lang, $userId, $page, $perPage);
        $result = $this->mergerResult($result, $doctor_result);

        $work_result = $this->work_global_search_result($search, $lang, $userId, $page, $perPage);
        $result = $this->mergerResult($result, $work_result);

        return $result;
    }

    public function doctor_global_search_result($search, $lang, $userId, $page, $perPage)
    {
        $result = array();

        if ($lang == 'ar') {
            $result = $this->doctor_global_search_result_ar($search, $userId, $page, $perPage);
        } else {
            $result = $this->doctor_global_search_result_en($search, $userId, $page, $perPage);
        }

        return $result;
    }

    public function doctor_global_search_result_count($search, $lang, $userId)
    {
        $count = 0;

        if ($lang == 'ar') {
            $count = $this->doctor_global_search_result_ar_count($search, $userId);
        } else {
            $count = $this->doctor_global_search_result_en_count($search, $userId);
        }

        return $count;
    }

    public function work_global_search_result_count($search, $lang, $userId)
    {
        $count = 0;

        if ($lang == 'ar') {
            $count = $this->work_global_search_result_count_ar($search, $userId);
        } else {
            $count = $this->work_global_search_result_count_en($search, $userId);
        }

        return $count;
    }

    public function work_global_search_result($search, $lang, $userId, $page, $perPage)
    {
        $result = array();

        if ($lang == 'ar') {
            $result = $this->work_global_search_result_ar($search, $userId, $page, $perPage);
        } else {
            $result = $this->work_global_search_result_en($search, $userId, $page, $perPage);
        }

        return $result;
    }

    public function work_global_search_result_ar($search, $userId, $page, $perPage)
    {
        $this->db->select('w.name AS Name,w.work_id AS WorkId, w.address AS Address, w.phone AS Phone, w.photo AS Photo,w.work_type AS WorkType,"null" AS Latitude,"null" AS Longitude, (SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE w.work_id=work_rating.work_id ) as RatingAvg, COUNT(wr.work_id) AS TotalReview');
        $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');

        $this->db->from('work_master w');

        //table join
        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');
        $this->db->join('location_master lm', 'lm.location_id = w.location_id', 'left');

        //like and where conditions
        if ($search != '') {
            //$rawstring = findword($search);
            $this->db->like('w.name', $search, 'both');
            /*foreach ($rawstring as $key => $value) {
            if ($key == 0) {
            $this->db->like('w.name', $value, 'both');
            } else {
            $this->db->or_like('w.name', $value, 'both');
            }
            }*/

            //$this->db->like('w.name', $search, 'both');
            $this->db->or_like('w.address', $search, 'both');
            $this->db->or_like('w.phone', $search, 'both');
            $this->db->or_like('lm.name', $search, 'both');
        }

        $this->db->where('w.status', '1');

        $this->db->group_by('w.work_id');
        $this->db->order_by('RatingAvg DESC, FIELD(work_type, "Hospital", "Clinic", "Radiology Lab", "Medical Lab")');

        $this->db->limit($perPage, $page);

        return $this->db->get()->result();
    }

    public function work_global_search_result_en($search, $userId, $page, $perPage)
    {
        $sql = "SELECT w.work_id AS WorkId, w.name_en AS Name, w.address_en AS Address, w.phone AS Phone, w.photo AS Photo, w.work_type AS WorkType,  w.work_latitude AS Latitude, w.work_longitude AS Longitude, (SELECT AVG(work_rating.average_score) FROM work_rating WHERE w.work_id = work_rating.work_id) AS RatingAvg, (SELECT COUNT(work_rating.work_id) FROM work_rating WHERE w.work_id = work_rating.work_id) AS TotalReview, (SELECT GROUP_CONCAT(sm.mipmap) FROM doctor_details dd LEFT JOIN specialty_master sm ON dd.speciality_id = sm.specialties_id WHERE dd.work_id = w.work_id) AS Mipmap
            FROM work_master w
            LEFT JOIN worktype_bookmark_master wm ON wm.work_id = w.work_id AND wm.user_id = $userId
            WHERE w.status = '1'
            AND (name_en LIKE '%$search%')
            GROUP BY w.work_id
            ORDER BY RatingAvg DESC, FIELD(w.work_type, 'Hospital', 'Clinic', 'Radiology Lab', 'Medical Lab')
            LIMIT $page, $perPage";

        return $this->db->query($sql)->result();
    }

    public function work_global_search_result_count_en($search, $userId)
    {
        $sql = "SELECT w.work_id AS WorkId, w.name_en AS Name, w.address AS Address, w.phone AS Phone, w.photo AS Photo, w.work_type AS WorkType,  w.work_latitude AS Latitude, w.work_longitude AS Longitude, (SELECT AVG(work_rating.average_score) FROM work_rating WHERE w.work_id = work_rating.work_id) AS RatingAvg, (SELECT COUNT(work_rating.work_id) FROM work_rating WHERE w.work_id = work_rating.work_id) AS TotalReview, (SELECT GROUP_CONCAT(sm.mipmap) FROM doctor_details dd LEFT JOIN specialty_master sm ON dd.speciality_id = sm.specialties_id WHERE dd.work_id = w.work_id) AS Mipmap
            FROM work_master w
            LEFT JOIN worktype_bookmark_master wm ON wm.work_id = w.work_id AND wm.user_id = $userId
            WHERE w.status = '1'
            AND (name_en LIKE '%$search%')
            GROUP BY w.work_id
            ORDER BY RatingAvg DESC, FIELD(w.work_type, 'Hospital', 'Clinic', 'Radiology Lab', 'Medical Lab')";

        return $this->db->query($sql)->num_rows();
    }

    public function work_global_search_result_count_ar($search, $userId)
    {
        $this->db->select('w.name AS Name,w.work_id AS WorkId, w.address AS Address, w.phone AS Phone, w.photo AS Photo,w.work_type AS WorkType,"null" AS Latitude,"null" AS Longitude, (SELECT IFNULL(AVG(work_rating.average_score),0) FROM work_rating WHERE w.work_id=work_rating.work_id ) as RatingAvg, COUNT(wr.work_id) AS TotalReview');
        $this->db->select('(SELECT GROUP_CONCAT(sm.mipmap) FROM `doctor_details` dd left join specialty_master sm on dd.speciality_id = sm.specialties_id where dd.work_id = w.work_id)AS Mipmap');

        $this->db->from('work_master w');

        //table join
        $this->db->join('worktype_bookmark_master wbm', 'wbm.work_id = w.work_id AND wbm.user_id = ' . $userId, 'left');
        $this->db->join('work_rating wr', 'wr.work_id = w.work_id', 'left');
        $this->db->join('location_master lm', 'lm.location_id = w.location_id', 'left');

        //like and where conditions
        if ($search != '') {
            //$rawstring = findword($search);
            $this->db->like('w.name', $search, 'both');
            /*foreach ($rawstring as $key => $value) {
            if ($key == 0) {
            $this->db->like('w.name', $value, 'both');
            } else {
            $this->db->or_like('w.name', $value, 'both');
            }
            }*/

            //$this->db->like('w.name', $search, 'both');
            $this->db->or_like('w.address', $search, 'both');
            $this->db->or_like('w.phone', $search, 'both');
            $this->db->or_like('lm.name', $search, 'both');
        }

        $this->db->where('w.status', '1');

        $this->db->group_by('w.work_id');
        $this->db->order_by('RatingAvg DESC, FIELD(work_type, "Hospital", "Clinic", "Radiology Lab", "Medical Lab")');

        return $this->db->get()->num_rows();
    }

    public function doctor_global_search_result_ar($search, $userId, $page, $perPage)
    {
        $this->db->select('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name)) AS Name,dd.doctor_id AS WorkId, d.address AS Address, dd.phone_number AS Phone,group_concat(sm.mipmap) AS Mipmap, "Doctor" AS WorkType,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE dd.doctor_id=doctors_rating.doctor_id) as RatingAvg, COUNT(dr.doctor_id) AS TotalReview');

        $this->db->from('doctor_master d');

        //table join
        $this->db->join('doctor_details dd', 'dd.doctor_id = d.doctor_id', 'left');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
        $this->db->join('specialty_master sm', ("FIND_IN_SET(sm .specialties_id , dd.speciality_id)"), 'left');
        $this->db->join('bookmark_master bm', 'bm.doctor_id = d.doctor_id AND bm.user_id = ' . $userId, 'left');
        $this->db->join('location_master lm', 'lm.location_id = dd.location_id', 'left');

        //like and where conditions
        if ($search != '') {
            //$rawstring = findword($search);
            $this->db->like('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name))', $search, 'both');
            /*foreach ($rawstring as $key => $value) {
        if ($key == 0) {
        $this->db->like('d.first_name', $value, 'both');
        } else {
        $this->db->or_like('d.last_name', $value, 'both');
        $this->db->or_like('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name))', $value, 'both');
        $this->db->or_like('dd.phone_number', $value, 'both');
        $this->db->or_like('dd.email', $value, 'both');
        }
        }*/
        }

        $this->db->where('dd.visibility', '1');
        $this->db->order_by('RatingAvg DESC, TotalReview DESC');

        $this->db->group_by('d.doctor_id');
        $this->db->limit($perPage, $page);

        return $this->db->get()->result();
    }

    public function doctor_global_search_result_en($search, $userId, $page, $perPage)
    {
        $sql = "SELECT TRIM(CONCAT(first_name, ' ', last_name)) AS Name, dd.doctor_id AS WorkId, dm.address AS Address, dd.phone_number AS Phone, sm.mipmap AS Mipmap, 'Doctor' AS WorkType, TRIM(dd.google_map_latitude) AS Latitude, TRIM(dd.google_map_longtude) AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus, (SELECT IFNULL(AVG(doctors_rating.average_score), 0) FROM doctors_rating WHERE dd.doctor_id = doctors_rating.doctor_id) AS RatingAvg, (SELECT COUNT(doctors_rating.doctor_id) FROM doctors_rating WHERE dd.doctor_id = doctors_rating.doctor_id) AS TotalReview
            FROM doctor_master_en dm
            INNER JOIN doctor_details dd ON dd.doctor_id = dm.doctor_id
            LEFT JOIN specialty_master sm ON sm.specialties_id = dd.speciality_id
            LEFT JOIN bookmark_master bm ON bm.doctor_id = dm.doctor_id AND bm.user_id = $userId
            WHERE dd.visibility = 1
            AND (CONCAT(first_name, ' ', last_name) LIKE '%$search%')
            GROUP BY dm.doctor_id
            ORDER BY RatingAvg DESC, TotalReview DESC
            LIMIT $page, $perPage";

        return $this->db->query($sql)->result();
    }

    public function doctor_global_search_result_en_count($search, $userId)
    {
        $sql = "SELECT TRIM(CONCAT(first_name, ' ', last_name)) AS Name, dd.doctor_id AS WorkId, dm.address AS Address, dd.phone_number AS Phone, sm.mipmap AS Mipmap, 'Doctor' AS WorkType, dd.google_map_latitude AS Latitude, dd.google_map_longtude AS Longitude, bm.user_id AS BookmarkUserId, bm.status AS BookmarkStatus, (SELECT IFNULL(AVG(doctors_rating.average_score), 0) FROM doctors_rating WHERE dd.doctor_id = doctors_rating.doctor_id) AS RatingAvg, (SELECT COUNT(doctors_rating.doctor_id) FROM doctors_rating WHERE dd.doctor_id = doctors_rating.doctor_id) AS TotalReview
            FROM doctor_master_en dm
            INNER JOIN doctor_details dd ON dd.doctor_id = dm.doctor_id
            LEFT JOIN specialty_master sm ON sm.specialties_id = dd.speciality_id
            LEFT JOIN bookmark_master bm ON bm.doctor_id = dm.doctor_id AND bm.user_id = $userId
            WHERE dd.visibility = 1
            AND (CONCAT(first_name, ' ', last_name) LIKE '%$search%')
            GROUP BY dm.doctor_id
            ORDER BY RatingAvg DESC, TotalReview DESC";

        return $this->db->query($sql)->num_rows();
    }

    public function doctor_global_search_result_ar_count($search, $userId)
    {
        $this->db->select('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name)) AS Name,dd.doctor_id AS WorkId, d.address AS Address, dd.phone_number AS Phone,group_concat(sm.mipmap) AS Mipmap, "Doctor" AS WorkType,dd.google_map_latitude AS Latitude,dd.google_map_longtude AS Longitude, (SELECT IFNULL(AVG(doctors_rating.average_score),0) FROM doctors_rating WHERE dd.doctor_id=doctors_rating.doctor_id) as RatingAvg, COUNT(dr.doctor_id) AS TotalReview');

        $this->db->from('doctor_master d');

        //table join
        $this->db->join('doctor_details dd', 'dd.doctor_id = d.doctor_id', 'left');
        $this->db->join('doctors_rating dr', 'dr.doctor_id = dd.doctor_id', 'left');
        $this->db->join('specialty_master sm', ("FIND_IN_SET(sm .specialties_id , dd.speciality_id)"), 'left');
        $this->db->join('bookmark_master bm', 'bm.doctor_id = d.doctor_id AND bm.user_id = ' . $userId, 'left');
        $this->db->join('location_master lm', 'lm.location_id = dd.location_id', 'left');

        //like and where conditions
        if ($search != '') {
            //$rawstring = findword($search);
            $this->db->or_like('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name))', $search, 'both');
            /*foreach ($rawstring as $key => $value) {
        if ($key == 0) {
        $this->db->like('d.first_name', $value, 'both');
        } else {
        $this->db->or_like('d.last_name', $value, 'both');
        $this->db->or_like('CONCAT(TRIM(d.first_name)," ", TRIM(d.last_name))', $value, 'both');
        $this->db->or_like('dd.phone_number', $value, 'both');
        $this->db->or_like('dd.email', $value, 'both');
        }
        }*/
        }

        $this->db->where('dd.visibility', '1');
        $this->db->order_by('RatingAvg DESC, TotalReview DESC');

        $this->db->group_by('d.doctor_id');

        return $this->db->get()->num_rows();
    }

    public function all_doctor_result($start = 0, $limit = 10, $user_id = 0)
    {
        //find doctor
        $query = "SELECT doctor_master_en.doctor_id AS WorkId,
                COUNT(doctors_rating.doctor_id) AS TotalReview,
                AVG(average_score) AS RatingAvg,
                CONCAT(doctor_master_en.first_name, ' ', doctor_master_en.last_name) AS Name,
                doctor_master_en.address AS Address,
                doctor_details.phone_number AS Phone,
                'Doctor' AS WorkType,
                doctor_details.google_map_latitude AS Latitude,
                doctor_details.google_map_longtude As Longitude,
                bookmark_master.user_id AS BookmarkUserId,
                bookmark_master.status AS BookmarkStatus,
                specialty_master.mipmap AS Mipmap
                FROM doctor_master_en
            LEFT JOIN doctors_rating
                ON doctor_master_en.doctor_id = doctors_rating.doctor_id
            INNER JOIN doctor_details ON doctor_details.doctor_id = doctor_master_en.doctor_id
            LEFT JOIN bookmark_master ON doctor_master_en.doctor_id = bookmark_master.doctor_id AND bookmark_master.user_id = $user_id
            LEFT JOIN specialty_master ON doctor_details.speciality_id = specialty_master.specialties_id
            WHERE doctor_details.visibility = 1
            GROUP BY doctor_master_en.doctor_id
            ORDER BY RatingAvg DESC, TotalReview DESC
            LIMIT $start, $limit";

        return $this->db->query($query)->result();
    }

    public function all_works_result($start = 0, $limit = 10, $user_id = 0)
    {
        $query = "SELECT work_master.work_id AS WorkId,
                work_master.name_en AS Name,
                work_master.address_en AS Address,
                work_master.phone AS Phone,
                '' AS Photo,
                work_master.work_type AS WorkType,
                work_master.work_latitude AS Latitude,
                work_master.work_longitude AS Longitude,
                worktype_bookmark_master.user_id AS BookmarkUserId,
                worktype_bookmark_master.status AS BookmarkStatus,
                AVG(work_rating.average_score) AS RatingAvg,
                COUNT(work_rating.work_id) AS TotalReview,
                specialty_master.mipmap AS Mipmap
            FROM work_master
            LEFT JOIN work_rating
                ON work_master.work_id = work_rating.work_id
            LEFT JOIN worktype_bookmark_master
                ON work_master.work_id = worktype_bookmark_master.work_id
                AND worktype_bookmark_master.user_id = $user_id
            LEFT JOIN specialty_master
                ON work_master.speciality = specialty_master.specialties_id
            WHERE work_master.status = '1'
            GROUP BY work_master.work_id
            ORDER BY RatingAvg DESC, FIELD(work_master.work_type, 'Hospital', 'Clinic', 'Radiology Lab', 'Medical Lab')
            LIMIT $start, $limit";

        return $this->db->query($query)->result();
    }

    public function all_doctor_result_ar($start = 0, $limit = 10, $user_id = 0)
    {
        //find doctor
        $query = "SELECT doctor_master.doctor_id AS WorkId,
                COUNT(doctors_rating.doctor_id) AS TotalReview,
                AVG(average_score) AS RatingAvg,
                CONCAT(doctor_master.first_name, ' ', doctor_master.last_name) AS Name,
                doctor_master.address AS Address,
                doctor_details.phone_number AS Phone,
                'Doctor' AS WorkType,
                doctor_details.google_map_latitude AS Latitude,
                doctor_details.google_map_longtude As Longitude,
                bookmark_master.user_id AS BookmarkUserId,
                bookmark_master.status AS BookmarkStatus,
                specialty_master.mipmap AS Mipmap
                FROM doctor_master
            LEFT JOIN doctors_rating
                ON doctor_master.doctor_id = doctors_rating.doctor_id
            INNER JOIN doctor_details ON doctor_details.doctor_id = doctor_master.doctor_id
            LEFT JOIN bookmark_master ON doctor_master.doctor_id = bookmark_master.doctor_id AND bookmark_master.user_id = $user_id
            LEFT JOIN specialty_master ON doctor_details.speciality_id = specialty_master.specialties_id
            WHERE doctor_details.visibility = 1
            GROUP BY doctor_master.doctor_id
            ORDER BY RatingAvg DESC, TotalReview DESC
            LIMIT $start, $limit";

        return $this->db->query($query)->result();
    }

    public function all_works_result_ar($start = 0, $limit = 10, $user_id = 0)
    {
        $query = "SELECT work_master.work_id AS WorkId,
                work_master.name AS Name,
                work_master.address AS Address,
                work_master.phone AS Phone,
                '' AS Photo,
                work_master.work_type AS WorkType,
                work_master.work_latitude AS Latitude,
                work_master.work_longitude AS Longitude,
                worktype_bookmark_master.user_id AS BookmarkUserId,
                worktype_bookmark_master.status AS BookmarkStatus,
                AVG(work_rating.average_score) AS RatingAvg,
                COUNT(work_rating.work_id) AS TotalReview,
                specialty_master.mipmap AS Mipmap
            FROM work_master
            LEFT JOIN work_rating
                ON work_master.work_id = work_rating.work_id
            LEFT JOIN worktype_bookmark_master
                ON work_master.work_id = worktype_bookmark_master.work_id
                AND worktype_bookmark_master.user_id = $user_id
            LEFT JOIN specialty_master
                ON work_master.speciality = specialty_master.specialties_id
            WHERE work_master.status = '1'
            GROUP BY work_master.work_id
            ORDER BY RatingAvg DESC, FIELD(work_master.work_type, 'Hospital', 'Clinic', 'Radiology Lab', 'Medical Lab')
            LIMIT $start, $limit";

        return $this->db->query($query)->result();
    }
}
