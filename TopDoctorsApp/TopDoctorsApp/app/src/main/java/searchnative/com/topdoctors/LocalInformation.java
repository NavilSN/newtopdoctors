package searchnative.com.topdoctors;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by root on 9/9/16.
 */
public class LocalInformation {

    private static String LANG = "en";

    public static String getLocaleLang() {
        if (LANG.toString().isEmpty())
            LANG = "en";

        return LANG;
    }

    public static String setLocalLang(String lang) {
        return LANG = lang;
    }
}
