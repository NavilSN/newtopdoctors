package searchnative.com.topdoctors;

import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.media.Image;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.vision.text.Line;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by root on 25/11/16.
 */

public class ReviewDetailsFragment extends Fragment {

    private RatingBar reputation_rating, clinic_ratingbar, availability_ratingbar,
            approachability_ratingbar, technology_ratingbar;
    private TextView write_review_reputation, write_review_clinic_accessibility, write_review_availability_in_emergencies,
            write_review_approachability, write_review_technology_and_equipment, commentLabel, userReviewComment,
            reviews_profile_title;
    private Typeface typeface;
    private String userLoginId, reviewId, name, type, URL;
    private ProgressBar progressBar;
    private LinearLayout linearLayout;
    UserReviewDetails userReviewDetails;
    private ImageView reviews_swipe_menu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getContext(), new Crashlytics());
        View view = inflater.inflate(R.layout.review_details, container, false);
        //supportMapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.google_map_fragment);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        userLoginId = Preference.getValue(getContext(), "LOGIN_ID", "");
        reviewId = getArguments().getString("id");
        name = getArguments().getString("name");
        type = getArguments().getString("type");

        reputation_rating = (RatingBar) getView().findViewById(R.id.reputation_rating);
        clinic_ratingbar = (RatingBar) getView().findViewById(R.id.clinic_ratingbar);
        availability_ratingbar = (RatingBar) getView().findViewById(R.id.availability_ratingbar);
        approachability_ratingbar = (RatingBar) getView().findViewById(R.id.approachability_ratingbar);
        technology_ratingbar = (RatingBar) getView().findViewById(R.id.technology_ratingbar);
        reviews_swipe_menu = (ImageView) getView().findViewById(R.id.reviews_swipe_menu);
        reviews_profile_title = (TextView) getView().findViewById(R.id.reviews_profile_title);

        write_review_reputation = (TextView) getView().findViewById(R.id.write_review_reputation);
        write_review_clinic_accessibility = (TextView) getView().findViewById(R.id.write_review_clinic_accessibility);
        write_review_availability_in_emergencies = (TextView) getView().findViewById(R.id.write_review_availability_in_emergencies);
        write_review_approachability = (TextView) getView().findViewById(R.id.write_review_approachability);
        write_review_technology_and_equipment = (TextView) getView().findViewById(R.id.write_review_technology_and_equipment);
        commentLabel = (TextView) getView().findViewById(R.id.commentLabel);
        userReviewComment = (TextView) getView().findViewById(R.id.userReviewComment);
        reviews_profile_title = (TextView) getView().findViewById(R.id.reviews_profile_title);

        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ExoMedium.otf");

        reviews_swipe_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawerLayout);
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        write_review_reputation.setTypeface(typeface);
        write_review_clinic_accessibility.setTypeface(typeface);
        write_review_availability_in_emergencies.setTypeface(typeface);
        write_review_approachability.setTypeface(typeface);
        write_review_technology_and_equipment.setTypeface(typeface);
        commentLabel.setTypeface(typeface);
        userReviewComment.setTypeface(typeface);
        reviews_profile_title.setText(name + getResources().getString(R.string.user_review));
        reviews_profile_title.setTypeface(typeface);

        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        linearLayout = (LinearLayout) getView().findViewById(R.id.doctorAllReviews);
        linearLayout.setVisibility(View.INVISIBLE);

        if (type.equals("work")) {
            URL = AppConfig.getWebServiceUrl() + "work_review/workReviewDetails";

        } else {
            URL = AppConfig.getWebServiceUrl() + "review/doctorReviewDetails";
        }
        userReviewDetails = new UserReviewDetails();
        userReviewDetails.execute();
    }

    class UserReviewDetails extends AsyncTask<String, Void, String> {
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            //String URL = AppConfig.getWebServiceUrl() + "work_review/workReviewDetails";
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            //Log.v("URL", URL);
            nameValuePairs.add(new BasicNameValuePair("reviewId", reviewId));
            nameValuePairs.add(new BasicNameValuePair("userId", userLoginId));
            nameValuePairs.add(new BasicNameValuePair("lang", LocalInformation.getLocaleLang()));

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressBar.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);
            new RenderReviewDetails(result);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }

    class RenderReviewDetails {
        RenderReviewDetails(String result) {
            try {
                JSONObject jsonMainObject = new JSONObject(result);
                JSONArray jsonArray = jsonMainObject.getJSONArray("result");
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                Log.v("JSON", jsonObject.toString());
                reputation_rating.setRating(Float.valueOf(jsonObject.getString("reputation")));
                clinic_ratingbar.setRating(Float.valueOf(jsonObject.getString("clinic")));
                availability_ratingbar.setRating(Float.valueOf(jsonObject.getString("availability")));
                approachability_ratingbar.setRating(Float.valueOf(jsonObject.getString("approachability")));
                technology_ratingbar.setRating(Float.valueOf(jsonObject.getString("technology")));
                if (!jsonObject.getString("comment").equals(""))
                    userReviewComment.setText(jsonObject.getString("comment"));
                else
                    userReviewComment.setText("No comment available");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        userReviewDetails.cancel(true);
    }
}
