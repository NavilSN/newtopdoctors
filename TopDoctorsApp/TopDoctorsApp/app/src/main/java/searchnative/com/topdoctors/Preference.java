package searchnative.com.topdoctors;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by root on 8/9/16.
 */
public class Preference {

    private static SharedPreferences sharedPreferences = null;

    public static void openPref(Context context) {
        sharedPreferences = context.getSharedPreferences("PREF_FILE", Context.MODE_PRIVATE);
    }

    public static String getValue(Context context, String key, String value) {
        Preference.openPref(context);
        String result = Preference.sharedPreferences.getString(key, value);
        //Preference.sharedPreferences = null;

        return result;
    }

    public String getValueNew(Context context, String key, String value) {
        Preference.openPref(context);
        String result = Preference.sharedPreferences.getString(key, value);
        //Preference.sharedPreferences = null;

        return result;
    }

    public static void setValue(Context context, String key, String value) {
        Preference.openPref(context);
        SharedPreferences.Editor editor = Preference.sharedPreferences.edit();
        editor.putString(key, value);
        boolean iscommit = editor.commit();
       // Log.d("ISCommit?" , iscommit + " Key "+key + "value " + value);
        //editor = null;
        //Preference.sharedPreferences = null;
    }

    public static void setRecentlyViewed(Context context, String name, List list) {
        Preference.openPref(context);
        SharedPreferences.Editor editor = Preference.sharedPreferences.edit();

        Set<String> set = new HashSet<String>();
        set.addAll(list);
        editor.putStringSet(name, set);
        editor.commit();
    }

    public static void resetPreference(Context context, String name) {
        Preference.openPref(context);
        SharedPreferences.Editor editor = Preference.sharedPreferences.edit();
        editor.remove(name);
        editor.commit();
    }

    public static List getRecentlyViewed(Context context, String name) {
        List list = new ArrayList();
        try {

            Preference.openPref(context);
            Set<String> set = Preference.sharedPreferences.getStringSet(name, null);

            if(set != null)
            {
                int setSize = set.size();
                if ( setSize > 0 )
                    //Log.v("set", set.toString());
                    list.addAll(set);
                //List<String> list = new ArrayList<String>(set);

            }

        }catch (NullPointerException e)
        {
            Log.v("Recently viewed:",e.getMessage()+"");
        }

        return list;
    }

    /*public static void setMenuVisibilityProfile(Context context) {
        setValue(context, "menuVisibility", "true");
    }

    public static void resetMenuVisibilityProfile(Context context) {
        setValue(context, "menuVisibility", "");
    }

    public static boolean getMenuVisisbily(Context context) {
        boolean isMenuSet = false;
        String result = getValue(context, "menuVisibility", "");
        if (result.equals("true")) {
            isMenuSet = true;
            setValue(context, "menuVisibility", "");
        }

        return isMenuSet;
    }*/
}
