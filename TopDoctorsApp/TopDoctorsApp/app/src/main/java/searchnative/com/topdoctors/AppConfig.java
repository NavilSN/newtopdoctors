package searchnative.com.topdoctors;

/**
 * Created by root on 14/9/16.
 */
public class AppConfig {

    //static String WEB_SERVICE_URL = "http://dev.searchnative.com/topdoctors/api/";

    //static String WEB_SERVICE_URL = "http://dev.searchnative.com/topdoctor/api/";

    //static String WEB_SERVICE_URL = "http://192.168.1.28/top-doctors/api/";

//    static String WEB_SERVICE_URL = "http://192.168.1.28/topdoctors_me/api/";

    //static String WEB_SITE_URL = "http://192.168.1.36/topdoctors_me/";
    //static String WEB_SERVICE_URL = "http://192.168.1.36/topdoctors_me/api/";

    //live server url
    static String WEB_SITE_URL = "http://new.topdoctors.me/";
    static String WEB_SERVICE_URL = "http://new.topdoctors.me/api/";



    /**
     * Get web service url
     * @return string
     */
    public static String getWebServiceUrl() {
        return WEB_SERVICE_URL;
    }

    public static void setWebServiceUrl(String url) {
        WEB_SERVICE_URL = url;
    }

    public static String getWebSiteUrl() {
        return WEB_SITE_URL;
    }

}
