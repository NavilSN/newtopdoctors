package searchnative.com.topdoctors;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class BaseActivity extends AppCompatActivity
        //implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,LocationListener
{


    //define the variables
    static DrawerLayout mDrawerLayout;
    public static NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    TabFragment tabFragment = new TabFragment();
    private String localeLanguage;
    private ProgressDialog mProgressDialog;
    ImageView topArrow;
    RelativeLayout relativeLayout;
    GuestLogin guestLogin;
    private boolean isGuest;

    Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    // double lat = 0.0,lon = 0.0;
    public static String buttonFragment = "BaseActivity";

    public static String isviewfrom = "NoView";

    private ImageView profilePic;
    private View hView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        Log.d("isviewfrom", "isviewfrom" + isviewfrom);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().hide();
        guestLogin = new GuestLogin();
        isGuest = guestLogin.getGuestLogin(BaseActivity.this);

        localeLanguage = Preference.getValue(getBaseContext(), "APP_LANG", "");




        /* 20 Dec
        try{

            if (!isGooglePlayServicesAvailable()) {
                Toast.makeText(BaseActivity.this, "Google Play Service is unavailable", Toast.LENGTH_SHORT).show();
            }

            buildGoogleApiClient();

        }catch (Exception e)
        {
            Log.v("Location Exception",""+e.getMessage());
        }
        */


        //RecentlyViewed.add("hello");
        //RecentlyViewed.setRecentlyViewedInPreference(BaseActivity.this, "recentlyViewed");
        //RecentlyViewed.resetPreference(BaseActivity.this, "recentlyViewed");
        //RecentlyViewed.setRecentlyViewedInPreference(BaseActivity.this, RecentlyViewed.name);
        Log.v("preference log", RecentlyViewed.initFromPreference(BaseActivity.this, "recentlyViewed").toString());

        setContentView(R.layout.activity_base);


        //set font
        final NavigationView navView = (NavigationView) findViewById(R.id.navigation_view);
        Menu m = navView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(mi);
        }

        //setup the drawer layout and navigation view
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        /*topArrow = (ImageView) headerview.findViewById(R.id.header);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(HomeActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                drawer.closeDrawer(GravityCompat.START);
            }
        });*/

        //inflate the first fragment
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, tabFragment).commit();

        setNavigation();

        //setup click event on navigation view
        /*mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mNavigationView.getMenu().findItem(menuItem.getItemId()).setChecked(true);
                mDrawerLayout.closeDrawers();
                if(menuItem.getItemId() == R.id.logout_btn) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    RecentlyViewed.recentlyViewed = new ArrayList();
                    RecentlyViewed.setRecentlyViewedInPreference(BaseActivity.this, RecentlyViewed.name);
                    Preference.setValue(BaseActivity.this, "PREF_FNAME", "");
                    Preference.setValue(BaseActivity.this, "PREF_ADDRESS", "");
                    Preference.setValue(BaseActivity.this, "PREF_ISLOGIN", "");
                    Preference.setValue(BaseActivity.this, "LOGIN_ID", "");
                    Preference.setValue(BaseActivity.this, "USER_TYPE", "");
                    Preference.setValue(BaseActivity.this, "PROFILE_PIC", "");

                    //fb logout
                    FacebookSdk.sdkInitialize(getApplicationContext());
                    LoginManager.getInstance().logOut();

                    //remove guest
                    guestLogin.removeGuestLogin(BaseActivity.this);

                    startActivity(new Intent(BaseActivity.this, LoginActivity.class));
                    finish();
                } else if(menuItem.getItemId() == R.id.add_a_doctor) {
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    startActivity(new Intent(BaseActivity.this, AddDoctorActivity.class));
                    mNavigationView.getMenu().findItem(menuItem.getItemId()).setChecked(false);
                    //FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    //fragmentTransaction.replace(R.id.containerView, new AddDoctor()).addToBackStack(null).commit();
                } else if(menuItem.getItemId() == R.id.search) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    Preference.setValue(BaseActivity.this, "SEARCH_REDIRECT", "true");
                    tabFragment.tabLayout.getTabAt(1).select();


                    Intent i = new Intent(BaseActivity.this,SearchFilterPop.class);
                    i.putExtra("isCustom",true);
                    startActivity(i);

                } else if(menuItem.getItemId() == R.id.edit_profile) {
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    tabFragment.tabLayout.getTabAt(0).select();
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.primary_layout, new ProfileFragment()).addToBackStack("loginUserProfile").commit();
                } else if(menuItem.getItemId() == R.id.add_a_clinic) {
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    startActivity(new Intent(BaseActivity.this, AddClinicActivity.class));
                } else if(menuItem.getItemId() == R.id.add_a_lab) {
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    startActivity(new Intent(BaseActivity.this, AddLabActivity.class));
                } else if(menuItem.getItemId() == R.id.top_ten_doctors) {
                    tabFragment.tabLayout.getTabAt(2).select();
                } else if(menuItem.getItemId() == R.id.bookmarks) {
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    tabFragment.tabLayout.getTabAt(1).select();
                    Preference.setValue(BaseActivity.this, "USER_BOOKMARKS", "true"); //USER_BOOKMARKS

                } else if (menuItem.getItemId() == R.id.recently_viewed) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    tabFragment.tabLayout.getTabAt(1).select();
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.search_layout, new RecentlyViewedFragment()).addToBackStack("recently_viewed").commit();
                    *//*Log.v("Recent view", RecentlyViewed.getRecentlyViewedFromPreference(BaseActivity.this,
                            RecentlyViewed.name).toString());*//*
                } else if (menuItem.getItemId() == R.id.nearby) {
                    setLatLongToPreference();
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    tabFragment.tabLayout.getTabAt(1).select();
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.search_layout, new NearbyResult()).addToBackStack("nearby").commit();
                } else if (menuItem.getItemId() == R.id.add_a_hospital) {
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    startActivity(new Intent(BaseActivity.this, AddHospitalActivity.class));
                } else if (menuItem.getItemId() == R.id.share) {
                    //social share top doctors
                    String name = "Top Doctors";
                    String message = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
                    String address = "123 Eccles Old Road, New Salford Road, East";
                    String phone = "+(971) 55 12345678";
                    String email = "info@topdoctors.com";
                    String website = "http://dev.searchnative.com/topdoctor";
                    String appUsrl = "";

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "Name: " + name + "\n" + message + "\n" + "Address: "
                            + address + "\n" + "Phone: " + phone + "\n" + "Email" + email + "\n" + "Website: "
                            + website + "\n" + "Download the app: " + appUsrl
                    );
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.social_text)));
                } else if(menuItem.getItemId() == R.id.change_language) {
                    LayoutInflater layoutInflater = LayoutInflater.from(BaseActivity.this);
                    View promptView = layoutInflater.inflate(R.layout.change_laguage_prompt, null);

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseActivity.this);
                    alertDialog.setView(promptView);

                    RadioGroup radioGroup = (RadioGroup) promptView.findViewById(R.id.languageGroup);

                    final String APP_LANG = Preference.getValue(getBaseContext(), "APP_LANG", "");
                    if (APP_LANG.equals("en_US")) {
                        radioGroup.check(R.id.englishUS);
                    } else if (APP_LANG.equals("ar")) {
                        radioGroup.check(R.id.arabicEgypt);
                    } else {
                        radioGroup.check(R.id.englishUS);
                    }

                    String[] language;
                    final Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/ExoMedium.otf");

                    final TextView title = (TextView) promptView.findViewById(R.id.textView1);
                    title.setTypeface(typeface, typeface.BOLD);

                    alertDialog
                            .setCancelable(true)
                            .setPositiveButton(getResources().getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if(!localeLanguage.equals(APP_LANG)) {
                                                Locale locale = new Locale(localeLanguage);
                                                Locale.setDefault(locale);
                                                Configuration config = new Configuration();
                                                config.locale = locale;
                                                getBaseContext().getResources().updateConfiguration(config,
                                                        getBaseContext().getResources().getDisplayMetrics());
                                                //LocaleHelper.onCreate(BaseActivity.this, "ar");
                                                LocalInformation.setLocalLang(localeLanguage);

                                                Preference.setValue(getBaseContext(), "APP_LANG", localeLanguage);

                                                mProgressDialog = new ProgressDialog(BaseActivity.this, R.style.AppCompatAlertDialogStyle);
                                                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                                                mProgressDialog.setCancelable(false);
                                                mProgressDialog.show();

                                                LocationData.mInstance = null;
                                                LocationData.getmInstance();
                                                LocationData.getmInstance().setCountryList();

                                                SpecialityData.mInstance = null;
                                                SpecialityData.getmInstance();
                                                SpecialityData.getmInstance().setSpecialityList();

                                                startThreadDelay();
                                                //startActivity(new Intent(BaseActivity.this, BaseActivity.class));
                                            }
                                        }
                                    })
                            .setNegativeButton(getResources().getString(R.string.cancel),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    });

                    final AlertDialog dialog = alertDialog.create();
                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialogInterface) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources()
                                    .getColor(R.color.colorPrimary));
                            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources()
                                    .getColor(R.color.colorPrimary));
                        }
                    });
                    dialog.show();
                }

                return false;
            }

        });*/

        //set login username and address
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/ExoMedium.otf");
        hView = mNavigationView.getHeaderView(0);
        TextView newuser = (TextView) hView.findViewById(R.id.login_user_name);
        TextView navAddress = (TextView) hView.findViewById(R.id.login_user_address);
        newuser.setTypeface(font);
        navAddress.setTypeface(font);
        Intent sender = getIntent();
        Bundle extras = sender.getExtras();
        String currentUser = Preference.getValue(BaseActivity.this, "PREF_FNAME", "");
        String currentAddress = Preference.getValue(BaseActivity.this, "PREF_ADDRESS", "");
        profilePic = (ImageView) hView.findViewById(R.id.imageView);
        if (!isGuest) {

            // 20Dec
            //setLatLongToPreference();

            /*if (!Preference.getValue(BaseActivity.this, "PROFILE_PIC", "").equals("")) {*/
                /*Picasso.with(BaseActivity.this)
                        .load(Preference.getValue(BaseActivity.this, "PROFILE_PIC", ""))
                        .resize(50, 50)
                        .centerCrop()
                        .into(profilePic);*/
               /* new DownloadImageTask(profilePic)
                        .execute(Preference.getValue(BaseActivity.this, "PROFILE_PIC", ""));*/

            // Fetching User Profile Image
            new UserProfileDetails().execute();


          /*  } else {
                profilePic.setImageResource(R.drawable.user_profile_default_image);
            }*/


        } else {

            // 20Dec
            //setLatLongToPreference();
            //Log.v("In ", "else profile");
            profilePic.setImageResource(R.drawable.user_profile_default_image);
        }

        newuser.setText(currentUser);
        navAddress.setText(currentAddress);
        topArrow = (ImageView) hView.findViewById(R.id.top_arrow);
        topArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
            }
        });

        relativeLayout = (RelativeLayout) hView.findViewById(R.id.user_image_logo);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //check for Internet
                if (!NetworkInfoHelper.isOnline(BaseActivity.this)) {
                    Toast.makeText(BaseActivity.this, getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                    return;
                }
                mDrawerLayout.closeDrawers();
                if (isGuest) {
                    guestLogin.confirmAlert(BaseActivity.this);
                    return;
                }
                getSupportFragmentManager().popBackStack("loginUserPr", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                tabFragment.tabLayout.getTabAt(0).select();
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.primary_layout, new ProfileFragment()).addToBackStack("loginUserPr").commit();
            }
        });
        /**
         * Setup Drawer Toggle of the Toolbar
         */

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        hideMenu();
    }

    private void showInputMethodPicker() {
        InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        if (imeManager != null) {
            imeManager.showInputMethodPicker();
        } else {
            Toast.makeText(this, "Keyboard is not picking", Toast.LENGTH_LONG).show();
        }
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.englishUS:
                if (checked)
                    localeLanguage = "en_US";
                // Pirates are the best
                break;
            case R.id.arabicEgypt:
                if (checked)
                    localeLanguage = "ar";
                // Ninjas rule
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add_a_clinic) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/ExoMedium.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    private void startThreadDelay() {
        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler.sendEmptyMessage(0);
            }
        };
        thread.start();

    }

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message message) {
            super.handleMessage(message);


            if (SpecialityData.getmInstance().specialityList.size() == 0
                    || LocationData.getmInstance().locationList.size() == 0) {
//                Toast.makeText(BaseActivity.this,"Recursive call",Toast.LENGTH_SHORT).show();
                startThreadDelay();
                return;
            }
//            Toast.makeText(BaseActivity.this,"Ready",Toast.LENGTH_SHORT).show();
            mProgressDialog.dismiss();
            //startActivity(new Intent(BaseActivity.this, BaseActivity.class));
            Intent i = new Intent(BaseActivity.this, BaseActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);


            //3showInputMethodPicker();

        }
    };

    private Boolean exit = false;

    private Boolean isPrimary = false;

    @Override
    public void onBackPressed() {
        mFragmentManager = getSupportFragmentManager();
        int fragmentBackStack = mFragmentManager.getBackStackEntryCount();
        DrawerLayout mDrawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            if (fragmentBackStack == 0) {
                if (exit) {

                    /**
                     * Nikunj + Jaymin
                     * removing Data of Location,Country and Speciality
                     */

                    try {

                        if (TabFragment.timer != null) {
                            TabFragment.timer.cancel();
                        }

                        SpecialityData.mInstance = null;
                        CountryData.mInstance = null;
                        LocationData.mInstance = null;

                        finish();

                    } catch (Exception e) {
                        Log.v("Exception back pressed ", e.getMessage() + "");
                    }


                } else {
                    tabFragment.tabLayout.getTabAt(0).select();
                    if (isPrimary) {
                        Toast.makeText(this, getResources().getString(R.string.app_exit_message),
                                Toast.LENGTH_SHORT).show();
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                                isPrimary = false;
                            }
                        }, 3 * 1000);
                    } else {
                        isPrimary = true;
                    }
                }
            } else {
                super.onBackPressed();
                Log.v("back: ", "pressed");
                Preference.setValue(BaseActivity.this, "resetMenuVisibility", "");
                //hideMenu();
                isPrimary = false;
            }
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }


    /* 20 Dec
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(100); // Update location every second

        try{

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);


            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            if (mLastLocation != null) {
                lat = mLastLocation.getLatitude();
                lon = mLastLocation.getLongitude();
            }

        }catch (SecurityException e)
        {
            Log.v("SecurityException",""+e.getMessage());
        }
        catch (Exception e)
        {
            Log.v("Exception",""+e.getMessage());
        }
        //Toast.makeText(SearchFilterPop.this, ""+lat+","+lon, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lon = location.getLongitude();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        buildGoogleApiClient();
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }



    public void setLatLongToPreference(){

//        Log.v("Latitude",lat+"");
//        Log.v("Longitude",lon+"");

//        lat = 23.012;
//        lon = 72.153;

        if(lat != 0.0 && lon != 0.0){
            Preference.setValue(BaseActivity.this, "Latitude", String.valueOf(lat));
            Preference.setValue(BaseActivity.this, "Longitude", String.valueOf(lon));
        }
        else{
            Preference.setValue(BaseActivity.this, "Latitude", "");
            Preference.setValue(BaseActivity.this, "Longitude", "");
            //Toast.makeText(BaseActivity.this,"Please enable location services",Toast.LENGTH_SHORT).show();
        }
    }
                */
    public static void hideMenu() {
        Menu nav_Menu = mNavigationView.getMenu();
        nav_Menu.findItem(R.id.write_a_review).setVisible(false);
        nav_Menu.findItem(R.id.claim_profile).setVisible(false);
        nav_Menu.findItem(R.id.share).setVisible(false);
    }

    public static void showMenu() {
        Menu nav_Menu = mNavigationView.getMenu();
        Log.d("isviewfrom", "isviewform" + isviewfrom);
        if (isviewfrom.equals("hospital")) {
            nav_Menu.findItem(R.id.claim_profile).setVisible(false);
            nav_Menu.findItem(R.id.write_a_review).setVisible(true);
            nav_Menu.findItem(R.id.share).setVisible(true);
        } else if (isviewfrom.equals("NoView")) {
            nav_Menu.findItem(R.id.claim_profile).setVisible(false);
            nav_Menu.findItem(R.id.write_a_review).setVisible(false);
            nav_Menu.findItem(R.id.share).setVisible(false);
        } else {
            nav_Menu.findItem(R.id.claim_profile).setVisible(true);
            nav_Menu.findItem(R.id.write_a_review).setVisible(true);
            nav_Menu.findItem(R.id.share).setVisible(true);
        }

//        nav_Menu.findItem(R.id.write_a_review).setVisible(true);
//        nav_Menu.findItem(R.id.share).setVisible(true);
    }

    public void setNavigation() {
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mNavigationView.getMenu().findItem(menuItem.getItemId()).setChecked(true);
                mDrawerLayout.closeDrawers();

                if (menuItem.getItemId() == R.id.write_a_review) {
                    if (buttonFragment.equals("DoctorProfile")) {
                        DoctorProfileFragment.writeReviewButton.performClick();
                    } else if (buttonFragment.equals("HospitalProfile")) {
                        HospitalProfileFragment.writeReviewButton.performClick();
                    } else if (buttonFragment.equals("ClinicProfile")) {
                        ClinicProfileFragment.clinicWriteReview.performClick();
                    } else if (buttonFragment.equals("LabProfile")) {
                        LabProfileFragment.labWriteReview.performClick();
                    }

                }

                if (menuItem.getItemId() == R.id.share) {
                    if (buttonFragment.equals("DoctorProfile")) {
                        DoctorProfileFragment.socialShare.performClick();
                    } else if (buttonFragment.equals("HospitalProfile")) {
                        HospitalProfileFragment.hospitalShare.performClick();
                    } else if (buttonFragment.equals("ClinicProfile")) {
                        ClinicProfileFragment.clinicShare.performClick();
                    } else if (buttonFragment.equals("LabProfile")) {
                        LabProfileFragment.labShare.performClick();
                    }

                }

                if (menuItem.getItemId() == R.id.claim_profile) {
                    if (buttonFragment.equals("DoctorProfile")) {
                        DoctorProfileFragment.claimProfile.performClick();
                    } else if (buttonFragment.equals("ClinicProfile")) {
                        ClinicProfileFragment.claimProfile.performClick();
                    } else if (buttonFragment.equals("LabProfile")) {
                        LabProfileFragment.claimProfile.performClick();
                    }
                }

                if (menuItem.getItemId() == R.id.logout_btn) {

                    if (TabFragment.timer != null) {
                        TabFragment.timer.cancel();
                    }

                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    RecentlyViewed.recentlyViewed = new ArrayList();
                    RecentlyViewed.pointer = 0;
                    RecentlyViewed.setRecentlyViewedInPreference(BaseActivity.this, RecentlyViewed.name);
                    Preference.setValue(BaseActivity.this, "PREF_FNAME", "");
                    Preference.setValue(BaseActivity.this, "PREF_ADDRESS", "");
                    Preference.setValue(BaseActivity.this, "PREF_ISLOGIN", "");
                    Preference.setValue(BaseActivity.this, "LOGIN_ID", "");
                    Preference.setValue(BaseActivity.this, "USER_TYPE", "");
                    Preference.setValue(BaseActivity.this, "PROFILE_PIC", "");

                    //fb logout
                    FacebookSdk.sdkInitialize(getApplicationContext());
                    LoginManager.getInstance().logOut();

                    //remove guest
                    guestLogin.removeGuestLogin(BaseActivity.this);


                    LocationData.mInstance = null;
                    CountryData.mInstance = null;
                    SpecialityData.mInstance = null;


                    finish();
//                    startActivity(new Intent(BaseActivity.this, LoginActivity.class));
                    Intent i = new Intent(BaseActivity.this, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);


                    //finish();
                } else if (menuItem.getItemId() == R.id.add_a_doctor) {
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    startActivity(new Intent(BaseActivity.this, AddDoctorActivity.class));
                    mNavigationView.getMenu().findItem(menuItem.getItemId()).setChecked(false);
                    //FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    //fragmentTransaction.replace(R.id.containerView, new AddDoctor()).addToBackStack(null).commit();
                } else if (menuItem.getItemId() == R.id.search) {
                    //check for Internet
                    if (!NetworkInfoHelper.isOnline(BaseActivity.this)) {
                        Toast.makeText(BaseActivity.this, getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                        return true;
                    }
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    Preference.setValue(BaseActivity.this, "SEARCH_REDIRECT", "true");
                    tabFragment.tabLayout.getTabAt(1).select();

                    SearchFragment.linearLayout.removeAllViews();


                    //searchFragment.
                    Intent i = new Intent(BaseActivity.this, SearchFilterPop.class);
                    i.putExtra("isCustom", true);
                    startActivity(i);

                } else if (menuItem.getItemId() == R.id.edit_profile) {
                    //check for Internet
                    if (!NetworkInfoHelper.isOnline(BaseActivity.this)) {
                        Toast.makeText(BaseActivity.this, getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                        return true;
                    }
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    getSupportFragmentManager().popBackStack("loginUserPr", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    tabFragment.tabLayout.getTabAt(0).select();
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.primary_layout, new ProfileFragment()).addToBackStack("loginUserPr").commit();
                } else if (menuItem.getItemId() == R.id.add_a_clinic) {
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    startActivity(new Intent(BaseActivity.this, AddClinicActivity.class));
                } else if (menuItem.getItemId() == R.id.add_a_lab) {
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    startActivity(new Intent(BaseActivity.this, AddLabActivity.class));
                } else if (menuItem.getItemId() == R.id.top_ten_doctors) {
                    //check for Internet
                    if (!NetworkInfoHelper.isOnline(BaseActivity.this)) {
                        Toast.makeText(BaseActivity.this, getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                        return true;
                    }
                    tabFragment.tabLayout.getTabAt(2).select();
                } else if (menuItem.getItemId() == R.id.bookmarks) {
                    //check for Internet
                    if (!NetworkInfoHelper.isOnline(BaseActivity.this)) {
                        Toast.makeText(BaseActivity.this, getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                        return true;
                    }
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    tabFragment.tabLayout.getTabAt(1).select();
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.search_layout, new BookmarkFragment()).addToBackStack("bookmarks").commit();
                    //Preference.setValue(BaseActivity.this, "USER_BOOKMARKS", "true"); //USER_BOOKMARKS

                } else if (menuItem.getItemId() == R.id.recently_viewed) {//check for Internet
                    if (!NetworkInfoHelper.isOnline(BaseActivity.this)) {
                        Toast.makeText(BaseActivity.this, getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                        return true;
                    }

                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    tabFragment.tabLayout.getTabAt(1).select();
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.search_layout, new RecentlyViewedFragment()).addToBackStack("recently_viewed").commit();
                    Log.v("Recent view", RecentlyViewed.getRecentlyViewedFromPreference(BaseActivity.this,
                            RecentlyViewed.name).toString());
                } else if (menuItem.getItemId() == R.id.nearby) {
                    //check for Internet
                    if (!NetworkInfoHelper.isOnline(BaseActivity.this)) {
                        Toast.makeText(BaseActivity.this, getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                        return true;
                    }

                    // 20Dec
                    // setLatLongToPreference();


                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    tabFragment.tabLayout.getTabAt(1).select();
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.search_layout, new NearbyResult()).addToBackStack("nearby").commit();
                } else if (menuItem.getItemId() == R.id.add_a_hospital) {
                    if (isGuest) {
                        guestLogin.confirmAlert(BaseActivity.this);
                        return true;
                    }
                    startActivity(new Intent(BaseActivity.this, AddHospitalActivity.class));
                } /*else if (menuItem.getItemId() == R.id.share) {
                    //social share top doctors
                    String name = "Top Doctors";
                    String message = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
                    String address = "123 Eccles Old Road, New Salford Road, East";
                    String phone = "+(971) 55 12345678";
                    String email = "info@topdoctors.com";
                    String website = "http://dev.searchnative.com/topdoctor";
                    String appUsrl = "";

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "Name: " + name + "\n" + message + "\n" + "Address: "
                            + address + "\n" + "Phone: " + phone + "\n" + "Email" + email + "\n" + "Website: "
                            + website + "\n" + "Download the app: " + appUsrl
                    );
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.social_text)));
                }*/ else if (menuItem.getItemId() == R.id.change_language) {
                    //check for Internet
                    if (!NetworkInfoHelper.isOnline(BaseActivity.this)) {
                        Toast.makeText(BaseActivity.this, getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                        return true;
                    }
                    LayoutInflater layoutInflater = LayoutInflater.from(BaseActivity.this);
                    View promptView = layoutInflater.inflate(R.layout.change_laguage_prompt, null);

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseActivity.this);
                    alertDialog.setView(promptView);

                    RadioGroup radioGroup = (RadioGroup) promptView.findViewById(R.id.languageGroup);

                    final String APP_LANG = Preference.getValue(getBaseContext(), "APP_LANG", "");
                    if (APP_LANG.equals("en_US")) {
                        radioGroup.check(R.id.englishUS);
                    } else if (APP_LANG.equals("ar")) {
                        radioGroup.check(R.id.arabicEgypt);
                    } else {
                        radioGroup.check(R.id.englishUS);
                    }

                    String[] language;
                    final Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/ExoMedium.otf");

                    final TextView title = (TextView) promptView.findViewById(R.id.textView1);
                    title.setTypeface(typeface, typeface.BOLD);

                    alertDialog
                            .setCancelable(true)
                            .setPositiveButton(getResources().getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (!localeLanguage.equals(APP_LANG)) {
                                                Locale locale = new Locale(localeLanguage);
                                                Locale.setDefault(locale);
                                                Configuration config = new Configuration();
                                                config.locale = locale;
                                                getBaseContext().getResources().updateConfiguration(config,
                                                        getBaseContext().getResources().getDisplayMetrics());
                                                //LocaleHelper.onCreate(BaseActivity.this, "ar");
                                                LocalInformation.setLocalLang(localeLanguage);

                                                Preference.setValue(getBaseContext(), "APP_LANG", localeLanguage);

                                                mProgressDialog = new ProgressDialog(BaseActivity.this, R.style.AppCompatAlertDialogStyle);
                                                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                                                mProgressDialog.setCancelable(false);
                                                mProgressDialog.show();

                                                LocationData.mInstance = null;
                                                LocationData.getmInstance();
                                                LocationData.getmInstance().setCountryList();

                                                SpecialityData.mInstance = null;
                                                SpecialityData.getmInstance();
                                                SpecialityData.getmInstance().setSpecialityList();

                                                if (TabFragment.timer != null) {
                                                    TabFragment.timer.cancel();
                                                }


                                                //SplashActivity splashActivity = new SplashActivity();
                                                //splashActivity.clearPreference();
                                                CustomSearchHandle.resetPaginateCounterForBookmarks(BaseActivity.this);
                                                startThreadDelay();
                                                //startActivity(new Intent(BaseActivity.this, BaseActivity.class));
                                            }
                                        }
                                    })
                            .setNegativeButton(getResources().getString(R.string.cancel),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    });

                    final AlertDialog dialog = alertDialog.create();
                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialogInterface) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources()
                                    .getColor(R.color.colorPrimary));
                            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources()
                                    .getColor(R.color.colorPrimary));
                        }
                    });
                    dialog.show();
                }

                return false;
            }

        });
    }


   /* @Override
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }*/


    class UserProfileDetails extends AsyncTask<String, Void, String> {

        Bitmap bitmap;
        final String URL = AppConfig.getWebServiceUrl() + "profile/userProfile";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        String quickUserId = Preference.getValue(BaseActivity.this, "LOGIN_ID", "");

        @Override
        protected String doInBackground(String... params) {
            Log.v("Login Id", quickUserId);
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();

                //check for user type
                String userType = Preference.getValue(BaseActivity.this, "USER_TYPE", "");
                Log.v("Login type", userType);

                Drawable drawable = getResources().getDrawable(R.drawable.user_profile_default_image);
                bitmap = ((BitmapDrawable) drawable).getBitmap();

                return mClient.execute(httpPost, responseHandler);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.v("User Profile", result);
            try {
                if (result != null) {
                    Log.v("Profile data", result);
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    String imageUrl = jsonArray.getJSONObject(0).getString("Photo");
                    new DownloadImage((ImageView) hView.findViewById(R.id.imageView)).execute(imageUrl);
                } else {
                    profilePic.setImageResource(R.drawable.user_profile_default_image);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            try {
                if (result != null)
                    profilePic.setImageBitmap(result);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    protected void onStop() {
        super.onStop();

//        Toast.makeText(BaseActivity.this, CheckingBackGround.isAppIsInBackground(BaseActivity.this)+"", Toast.LENGTH_SHORT).show();

        if (CheckingBackGround.isAppIsInBackground(BaseActivity.this)) {
            if (TabFragment.timer != null) {
                TabFragment.timer.cancel();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (TabFragment.timer != null) {
            TabFragment.timer.cancel();
            TabFragment.timer.start();
        }
    }
}
