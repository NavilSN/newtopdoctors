package searchnative.com.topdoctors;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.media.Image;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.ShareActionProvider;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

/**
 * Created by root on 26/9/16.
 */
public class DoctorProfileFragment extends Fragment {

    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    TabFragment tabFragment = new TabFragment();
    final String WEB_SERVICE_URL = AppConfig.getWebServiceUrl();
   // private ProgressDialog mProgressDialog;
    private Typeface typeface;
    private SupportMapFragment supportMapFragment;
    private double latitude, longitude;
    private LatLng doctorProfileLatlong;
    private TextView totalReviews;
    public static TextView writeReviewButton, claimProfile;
    private List specialityIcon;
    private ImageView photoOrVideo;
    public static ImageView socialShare;
    private ShareActionProvider mShareActionProvider;
    private String doctorProfileEmail, doctorProfileName, doctorProfilePhone, doctorProfileMessage,
            doctorProfileAddress, doctorProfileRating= "", doctorId;

    private RatingBar reputationRating, clinicRatingbar, availabilityRatingbar, approachabilityRatingbar, technologyRatingbar;
    private TextView totalReputationReview, totalReviewClinicAccessibility, totalAvailibilityInEmergencies,
            totalReviewApproachability, totalReviewTechnologyAndEquipment, reviewsTitle, callText, workinghours,
            d_30kText, makeEnquiry;
    LinearLayout writeYourReview, doctorWriteReview;
    private String appLang = LocalInformation.getLocaleLang();
    private ImageView doctorShare, doctorBookmarks;
    private String loginUserId;
    private LinearLayout doctorProfileDetails, avgRating;
    private ProgressBar progressBar;
    private String quickDoctorId;
    GuestLogin guestLogin;
    private boolean isGuest;
    private LinearLayout mapLayout;
    private ScrollView parentScroll;
    private Set<String> set = new HashSet<String>();
    private List iconList = new ArrayList();
    private int totalReviewCount = 0;
    private ImageView transparent_image;
    private RelativeLayout map_relative_layout;

    //Asynck class
    DoctorProfile doctorProfileAsynck;
    DoctorAvgRating doctorAvgRatingAsynck;
    DoctorAllReviews doctorAllReviewsAsynck;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getContext(), new Crashlytics());
        View view = inflater.inflate(R.layout.doctor_profile, container, false);
        BaseActivity.isviewfrom="";


        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        BaseActivity.isviewfrom="";
        //isfromprimary=getArguments().getString("isfrom");
        doctorId = getArguments().getString("id");
        Preference.setValue(getContext(), "resetMenuVisibility", "true");
        //recent viewed
        RecentlyViewed.add("D" + doctorId);
        RecentlyViewed.setRecentlyViewedInPreference(getContext(), "recentlyViewed");

        final String doctorName = getArguments().getString("name");
        guestLogin = new GuestLogin();
        isGuest = guestLogin.getGuestLogin(getContext());

        loginUserId = Preference.getValue(getContext(), "LOGIN_ID", "");

        typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/ExoMedium.otf");

        totalReviews = (TextView) getView().findViewById(R.id.total_reviews);
        writeReviewButton = (TextView) getView().findViewById(R.id.write_a_review_button);
        socialShare = (ImageView) getView().findViewById(R.id.social_share);
        photoOrVideo = (ImageView) getView().findViewById(R.id.photos_or_video);
        reputationRating = (RatingBar) getView().findViewById(R.id.reputation_rating);
        clinicRatingbar = (RatingBar) getView().findViewById(R.id.clinic_ratingbar);
        availabilityRatingbar = (RatingBar) getView().findViewById(R.id.availability_ratingbar);
        approachabilityRatingbar = (RatingBar) getView().findViewById(R.id.approachability_ratingbar);
        writeYourReview = (LinearLayout) getView().findViewById(R.id.write_your_review);
        doctorShare = (ImageView) getView().findViewById(R.id.doctorShare);
        doctorBookmarks = (ImageView) getView().findViewById(R.id.doctorBookmarks);
        workinghours = (TextView) getView().findViewById(R.id.workinghours);
        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        doctorProfileDetails = (LinearLayout) getView().findViewById(R.id.doctorProfileDetails);
        doctorWriteReview = (LinearLayout) getView().findViewById(R.id.doctorWriteReview);
        claimProfile = (TextView) getView().findViewById(R.id.claim_profile_button);
        mapLayout = (LinearLayout) getView().findViewById(R.id.doctor_profile_map_layout);
        parentScroll = (ScrollView) getView().findViewById(R.id.home_page_form);
        //child_scroll = (ScrollView) getView().findViewById(R.id.child_scroll);
        transparent_image = (ImageView) getView().findViewById(R.id.transparent_image);
        map_relative_layout = (RelativeLayout) getView().findViewById(R.id.map_relative_layout);
        avgRating = (LinearLayout) getView().findViewById(R.id.avgRating);
        makeEnquiry = (TextView) getView().findViewById(R.id.makeEnquiry);

        transparent_image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        parentScroll.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        parentScroll.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        parentScroll.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });

        //scroll
        /*parentScroll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                child_scroll.getParent().requestDisallowInterceptTouchEvent(false);
                return false;
            }
        });

        child_scroll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });*/

        doctorProfileDetails.setVisibility(View.INVISIBLE);
        //d_30kText = (TextView) getView().findViewById(R.id.d_30kText);

        totalReputationReview = (TextView) getView().findViewById(R.id.total_reputation_review);
        totalReviewClinicAccessibility = (TextView) getView().findViewById(R.id.total_review_clinic_accessibility);
        totalAvailibilityInEmergencies = (TextView) getView().findViewById(R.id.total_availibility_in_emergencies);
        totalReviewApproachability = (TextView) getView().findViewById(R.id.total_review_approachability);
        technologyRatingbar = (RatingBar) getView().findViewById(R.id.technology_ratingbar);
        totalReviewTechnologyAndEquipment = (TextView) getView().findViewById(R.id.total_review_technology_and_equipment);
        reviewsTitle = (TextView) getView().findViewById(R.id.reviewsTitle);
        callText = (TextView) getView().findViewById(R.id.call_text);
        reviewsTitle.setTypeface(typeface, typeface.BOLD);
        workinghours.setTypeface(typeface);
        //d_30kText.setTypeface(typeface);
        claimProfile.setTypeface(typeface, typeface.BOLD);
        makeEnquiry.setTypeface(typeface);

        specialityIcon = new ArrayList();
        specialityIcon.addAll(SpecialityData.getmInstance().specialityListIcon);

        ImageView imageView = (ImageView) getView().findViewById(R.id.doctor_profile_swipe_menu);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawerLayout);
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        writeYourReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isGuest) {
                    guestLogin.confirmAlert(getContext());
                    return;
                }
                Intent intent = new Intent(getContext(), WriteReviewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("id", doctorId);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        doctorWriteReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isGuest) {
                    guestLogin.confirmAlert(getContext());
                    return;
                }
                Intent intent = new Intent(getContext(), WriteReviewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("id", doctorId);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        //photo and video
        photoOrVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if (isGuest) return;
                startActivity(new Intent(getContext(), PhotoOrVideoActivity.class));
            }
        });

        //total review
        totalReviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //check for Internet
                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putString("id", doctorId);
                bundle.putString("name", doctorName);

                FragmentManager fragmentManager;
                fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction;
                fragmentTransaction = fragmentManager.beginTransaction();

                Reviews reviews = new Reviews();
                reviews.setArguments(bundle);

                fragmentTransaction.addToBackStack("hospital_profile");
                fragmentTransaction.replace(R.id.search_layout, reviews).commit();
            }
        });

        writeReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isGuest) {
                    guestLogin.confirmAlert(getContext());
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putString("id", doctorId);
                bundle.putString("name", doctorName);

                Intent intent = new Intent(getContext(), WriteReviewActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        //social share
        socialShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //checkForNullValue();
                shareIt(doctorProfileName, CheckForNullValues.setValue(doctorProfilePhone), doctorProfileAddress,
                        CheckForNullValues.setValue(doctorProfileRating));
            }
        });

        doctorShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //checkForNullValue();
                shareIt(doctorProfileName, CheckForNullValues.setValue(doctorProfilePhone), doctorProfileAddress,
                        CheckForNullValues.setValue(doctorProfileRating));
            }
        });

        doctorProfileAsynck = new DoctorProfile();
        doctorProfileAsynck.execute(doctorId);

        /*mapLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                parentScroll.requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });*/
    }
/*
    private void checkForNullValue()
    {
       if(doctorProfileRating.equals(""))
       {
           doctorProfileRating = "0";
       }
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        BaseActivity.isviewfrom="";
    }

    @Override
    public void onDetach() {
        super.onDetach();
        BaseActivity.isviewfrom="NoView";
//        if(isfromprimary!=null){
//            if(isfromprimary.equals("primary")){
//                TabFragment.viewPager.setCurrentItem(1);
//            }
//        }



    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BaseActivity.isviewfrom="NoView";
//        if(isfromprimary!=null){
//            if(isfromprimary.equals("primary")){
//                TabFragment.viewPager.setCurrentItem(1);
//            }
//        }


    }

    /*public void shareIt(final String name, final String phone, final String address, final String rating) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Name: " + name + "\n" + "Phone: " + phone + "\n" + "Address: "
                + address + "\n" + "Rating: " + rating);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share via"));

        *//*ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentTitle("Game Result Highscore")
                .setContentDescription("My new highscore is " + sum.getText() + "!!")
                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=de.ginkoboy.flashcards"))

                //.setImageUrl(Uri.parse("android.resource://de.ginkoboy.flashcards/" + R.drawable.logo_flashcards_pro))
                .setImageUrl(Uri.parse("http://bagpiper-andy.de/bilder/dudelsack%20app.png"))
                .build();

        shareDialog.show(linkContent);*//*

    }*/

    public void shareIt(final String name, final String phone, final String address, final String rating) {
        if (isGuest) {
            guestLogin.confirmAlert(getContext());
            return;
        }
        String shareName = name.replace(" ", "-");
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        if (LocalInformation.getLocaleLang().equals("ar")) {
            String url = AppConfig.getWebSiteUrl() + "doctorProfile/index/" + doctorId + "-" + shareName + "/ar";
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Name: " + name + "\n" + "Profile: "
                    + url);
        } else {
            String url = AppConfig.getWebSiteUrl() + "doctorProfile/index/" + shareName + "-" + doctorId + "/en";
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Name: " + name + "\n" + "Profile: "
                    + url);
        }

        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share via"));

        /*ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentTitle("Game Result Highscore")
                .setContentDescription("My new highscore is " + sum.getText() + "!!")
                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=de.ginkoboy.flashcards"))

                //.setImageUrl(Uri.parse("android.resource://de.ginkoboy.flashcards/" + R.drawable.logo_flashcards_pro))
                .setImageUrl(Uri.parse("http://bagpiper-andy.de/bilder/dudelsack%20app.png"))
                .build();

        shareDialog.show(linkContent);*/

    }

    // Call to update the share intent
    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    class DoctorProfile extends AsyncTask<String, Void, String> {
        String URL = WEB_SERVICE_URL + "profile/doctorProfile";

        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        String quickDcotorId = doctorId;

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("doctorId", quickDcotorId));
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));
            Log.v("URL", URL);
            Log.v("name value", nameValuePairs.toString());

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                Log.v("Doctor Profile", result);
                new DisplayDoctorProfile(result);
            } else {
                if(getActivity() != null){
                    Toast.makeText(getActivity(), getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            doctorProfileDetails.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();*/
        }
    }

    /**
     * Display doctor profile
     */
    public class DisplayDoctorProfile {
        DisplayDoctorProfile(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                //Log.v("JSON", jsonObject.toString());
                JSONArray profileDataArray = jsonObject.getJSONArray("data");

                for(int i = 0; i < profileDataArray.length(); i++) {
                    final JSONObject profile = profileDataArray.getJSONObject(i);
                    Log.v("Name", profile.toString());
                    //name
                    TextView name = (TextView) getView().findViewById(R.id.profile_detail_name);
                    name.setText(profile.getString("Name"));
                    doctorProfileName = profile.getString("Name");
                    name.setTypeface(typeface, typeface.BOLD);

                    //speciality icon
                    ImageView doctorSpecialityIcon = (ImageView) getView().findViewById(R.id.speciality);
                    try {

                        int id = getContext().getResources().getIdentifier(
                                specialityIcon.get(getCategoryPos(profile.getString("Mipmap"))).toString(),
                                "mipmap", getContext().getPackageName());
                        doctorSpecialityIcon.setImageResource(id);

                    }
                    catch (Exception e)
                    {
                        Log.v("Exception",e.getMessage()+"");
                    }

                    //title
                    TextView title = (TextView) getView().findViewById(R.id.profile_detail_title);
                    title.setText(profile.getString("Name"));
                    title.setTypeface(typeface, typeface.BOLD);

                    //address
                    TextView address = (TextView) getView().findViewById(R.id.textView6);
                    address.setText(profile.getString("Address").toString().trim());
                    address.setTypeface(typeface);
                    doctorProfileAddress = profile.getString("Address").toString().trim();

                    //phone
                    /**
                     * Nikunj
                     * Date 14 Dec 2016 5:40 PM
                     *
                     * If Phone number is not available it's field is gone
                     */

                    RelativeLayout relativeLayoutPhone = (RelativeLayout) getView().findViewById(R.id.doctor_call);
                    View viewPhone = getView().findViewById(R.id.doctor_call_view);
                    if(profile.getString("Phone").equals(""))
                    {
                        relativeLayoutPhone.setVisibility(View.GONE);
                        viewPhone.setVisibility(View.GONE);
                    }else{
                        relativeLayoutPhone.setVisibility(View.VISIBLE);
                        viewPhone.setVisibility(View.VISIBLE);
                    }

                    TextView doctorPhone = (TextView) getView().findViewById(R.id.textView8);
                    callText.setText(getResources().getString(R.string.call));
                    callText.setTypeface(typeface, typeface.BOLD);
                    doctorPhone.setText(" (" + profile.getString("Phone") + ")");
                    doctorPhone.setTypeface(typeface);
                    doctorProfilePhone = profile.getString("Phone");

                    //get direction
                    TextView getDirection = (TextView) getView().findViewById(R.id.textView7);
                    getDirection.setTypeface(typeface, typeface.BOLD);

                    //write a review
                    TextView writeReview = (TextView) getView().findViewById(R.id.write_a_review_button);
                    writeReview.setTypeface(typeface);

                    //claim profile
                    final String profileName = profile.getString("Name");
                    claimProfile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (isGuest) {
                                guestLogin.confirmAlert(getContext());
                                return;
                            }
                            Intent intent = new Intent(getContext(), ClaimProfileActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("name", profileName);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });

                    makeEnquiry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getContext(), MakeEnquiryActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("user_id",loginUserId);
                            bundle.putString("work_id", doctorId);
                            bundle.putString("work_type", "Doctor");
                            bundle.putString("name", profileName);
                            intent.putExtras(bundle);

                            startActivity(intent);
                        }
                    });

                    //rating
                    RatingBar ratingBar = (RatingBar) getView().findViewById(R.id.doctor_user_rating);
                    if(!profile.getString("RatingAvg").equals("null") &&
                            !profile.getString("RatingAvg").isEmpty()) {
                        ratingBar.setRating(Float.valueOf(profile.getString("RatingAvg")));
                        doctorProfileRating = String.valueOf(profile.getString("RatingAvg"));
                    }

                    //review
                    TextView totalReview = (TextView) getView().findViewById(R.id.total_reviews);
                    if(!profile.getString("TotalReview").equals("null") &&
                            !profile.getString("TotalReview").isEmpty()) {
                        totalReview.setText(profile.getString("TotalReview") + " " + getResources().getString(R.string.reviews));
                    } else {
                        totalReview.setText(0 + " " + getResources().getString(R.string.reviews));
                    }
                    totalReview.setTypeface(typeface);

                    final String callDialer = profile.getString("Phone");
                    RelativeLayout hospitalCall = (RelativeLayout) getView().findViewById(R.id.doctor_call);
                    hospitalCall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
                            builderSingle.setIcon(R.mipmap.call);
                            builderSingle.setTitle(R.string.select_one_number);

                            String search = "--";
                            String search1 = "-";
                            String search2 = ";";
                            String[] array = new String[10];
                            if (callDialer.indexOf(search) != -1) {
                                array = callDialer.replaceAll(" ", "").split("\\--", -1);
                            } else if (callDialer.indexOf(search1) != -1) {
                                array = callDialer.replaceAll(" ", "").split("\\-", -1);
                            } else if (callDialer.indexOf(search2) != -1) {
                                array = callDialer.replaceAll(" ", "").split("\\;", -1);
                            }

                            if (array.length == 10) {
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:" + callDialer.trim()));
                                startActivity(intent);
                            } else {
                                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
                                arrayAdapter.addAll(array);

                                builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String strName = arrayAdapter.getItem(which);
                                        Intent intent = new Intent(Intent.ACTION_DIAL);
                                        intent.setData(Uri.parse("tel:" + strName));
                                        startActivity(intent);

                                    }
                                });
                                builderSingle.show();
                            }
                        }
                    });

                    if(!profile.getString("Latitude").equals("null") &&
                            !profile.getString("Latitude").isEmpty() &&
                            !profile.getString("Longitude").equals("null") &&
                            !profile.getString("Longitude").isEmpty()) {
                        latitude = profile.getDouble("Latitude");
                        longitude = profile.getDouble("Longitude");
                    }

                    if(latitude == 0.0 || longitude == 0.0)
                    {
                        View border2 = getView().findViewById(R.id.viewWidth);
                        border2.setVisibility(View.GONE);
                        LinearLayout layout = (LinearLayout) getView().findViewById(R.id.doctor_profile_map_layout);
                        layout.setVisibility(View.GONE);
                        map_relative_layout.setVisibility(View.GONE);
                    }
                    //set latitude and longitude
                    doctorProfileLatlong = new LatLng(latitude, longitude);
                    final String doctorAddress = profile.getString("Name");
                    //google map
                    try {
                        FragmentManager fragmentManager = getChildFragmentManager();
                        //GoogleMap map = (WorkaroundMapFragment) getSup.findFragmentById(R.id.doctor_profile_map_layout);
                        supportMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.doctor_profile_map_layout);


                        if(supportMapFragment == null) {
                            supportMapFragment = SupportMapFragment.newInstance();
                            supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                                @Override
                                public void onMapReady(final GoogleMap map) {
                                    try {
                                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                                        map.addMarker(new MarkerOptions().position(doctorProfileLatlong).title(doctorAddress)
                                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.doctor_location)));
                                        //map.setMyLocationEnabled(true);
                                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(doctorProfileLatlong, 15));
                                        map.setTrafficEnabled(true);
                                        map.setIndoorEnabled(true);
                                        map.setBuildingsEnabled(true);
                                        map.getUiSettings().setScrollGesturesEnabled(true);
                                        map.getUiSettings().setZoomControlsEnabled(true);
                                        }
                                        catch (SecurityException e)
                                        {
                                            Log.v("Security Exception", e.getMessage()+"");
                                        }
                                        catch (Exception e) {
                                            Log.v("Exception", e.getMessage()+"");
                                        }

                                        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                                            @Override
                                            public void onMapClick(LatLng latLng) {
                                                Log.d("arg0", "toched");
                                            }
                                        });
                                    }
                                });
                            /*((WorkaroundMapFragment) getFragmentManager().findFragmentById(R.id.map))
                                    .setListener(new WorkaroundMapFragment.OnTouchListener() {
                                        @Override
                                        public void onTouch() {
                                            parentScroll.requestDisallowInterceptTouchEvent(true);
                                        }
                                    });*/
                            fragmentManager.beginTransaction().replace(R.id.doctor_profile_map_layout, supportMapFragment).commit();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //get direction
                    RelativeLayout relativeLayout = (RelativeLayout) getView().findViewById(R.id.doctor_get_direction_layout);
                    relativeLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(String.valueOf(latitude).isEmpty() || String.valueOf(longitude).isEmpty()) {
                                Toast.makeText(getContext(),
                                        getResources().getString(R.string.location_is_not_available),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                final Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://maps.google.com/maps?" + "saddr=my location" +"&daddr=" + latitude + "," + longitude));
                                intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
                                startActivity(intent);
                            }

                        }
                    });

                    //bookmarks
                    /*final ImageView addToBookmarks = (ImageView) getView().findViewById(R.id.doctorBookmarks);
                    addToBookmarks.setTag(1);
                    String bookmarkStatus = profile.getString("BookmarkStatus");
                    if ( ! (bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                        String bookmarkUserId = profile.getString("BookmarkUserId");
                        if (bookmarkUserId.equals(loginUserId) && bookmarkStatus.equals("1")) {
                            addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                            addToBookmarks.setTag(2);
                        }
                    }
                    addToBookmarks.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            doctorBookMarks(doctorId);
                            if (Integer.parseInt(addToBookmarks.getTag().toString()) == 1) {
                                addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                addToBookmarks.setTag(2);
                            } else {
                                addToBookmarks.setImageResource(R.mipmap.bookmarks);
                                addToBookmarks.setTag(1);
                            }
                        }
                    });*/

                }

                //bookmark
                final LinearLayout withoutBookmark = (LinearLayout) getView().findViewById(R.id.withoutBookmark);
                final LinearLayout withBookmark = (LinearLayout) getView().findViewById(R.id.withBookmark);
                String bookmarkStatus = "", bookmarkUserId = "";
                if ( ! (jsonObject.getString("BookmarkStatus").equals("null") ||
                        jsonObject.getString("BookmarkStatus").isEmpty()))
                    bookmarkStatus = jsonObject.getString("BookmarkStatus");
                if ( ! (jsonObject.getString("BookmarkUserId").equals("null") ||
                        jsonObject.getString("BookmarkUserId").isEmpty()))
                    bookmarkUserId = jsonObject.getString("BookmarkUserId");
                final ImageView addToBookmarks = (ImageView) getView().findViewById(R.id.doctorBookmarks);
                addToBookmarks.setTag(1);
                if ( ! (bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                    if (bookmarkUserId.equals(loginUserId) && bookmarkStatus.equals("1")) {
                        addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                        addToBookmarks.setTag(2);
                        withBookmark.setVisibility(View.VISIBLE);
                        withoutBookmark.setVisibility(View.GONE);
                    }
                }
                addToBookmarks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //check for Internet
                        if ( ! NetworkInfoHelper.isOnline(getContext())) {
                            Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                            return;
                        }
                        if (isGuest) {
                            guestLogin.confirmAlert(getContext());
                            return;
                        }
                        doctorBookMarks(doctorId);
                        if (Integer.parseInt(addToBookmarks.getTag().toString()) == 1) {
                            addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                            addToBookmarks.setTag(2);
                            withBookmark.setVisibility(View.VISIBLE);
                            withoutBookmark.setVisibility(View.GONE);
                        } else {
                            addToBookmarks.setImageResource(R.mipmap.bookmarks);
                            addToBookmarks.setTag(1);
                            withBookmark.setVisibility(View.GONE);
                            withoutBookmark.setVisibility(View.VISIBLE);
                        }
                    }
                });
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private int getCategoryPos(String category) {
        return specialityIcon.indexOf(category);
    }

    class DoctorAvgRating extends AsyncTask<String, Void,String> {

        String quickDoctorId = doctorId;

        String URL = WEB_SERVICE_URL + "review/reviewRating";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("doctorId", quickDoctorId));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            if(result != null){
                new RenderAvgReview(result);
            }else{
                if(getContext() != null){
                    Toast.makeText(getContext(), getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);

        }
    }

    class RenderAvgReview {
        RenderAvgReview(String result) {
            Log.v("avg rating", result);
            float doctorAvgRating = 0.0f;
            //LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.)
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONObject ratingOnject = jsonObject.getJSONObject("data");

                //Reputation
                JSONObject reputationObject = ratingOnject.getJSONObject("reputation");
                if(reputationObject.has("avg") && (! reputationObject.getString("avg").equals("null")))
                {
                    reputationRating.setRating(Float.valueOf(reputationObject.getString("avg")));
                    doctorAvgRating += Float.valueOf(reputationObject.getString("avg"));
                }
                else {
                    Log.v("Reputation Avg is Null","Avg is null.");
                    reputationRating.setRating(0.0f);
                }

                totalReputationReview.setText(reputationObject.getString("count"));
                TextView writeReviewReputation = (TextView) getView().findViewById(R.id.write_review_reputation);
                writeReviewReputation.setTypeface(typeface);

                //clinic
                JSONObject clinicObject = ratingOnject.getJSONObject("clinic");

                if(clinicObject.has("avg") && (! clinicObject.getString("avg").equals("null")))
                {
                    clinicRatingbar.setRating(Float.valueOf(clinicObject.getString("avg")));
                    doctorAvgRating += Float.valueOf(clinicObject.getString("avg"));
                }
                else {
                    Log.v("Clinic Avg is Null","Avg is null.");
                    clinicRatingbar.setRating(0.0f);

                }

                //clinicRatingbar.setRating(Float.valueOf(clinicObject.getString("avg")));
                totalReviewClinicAccessibility.setText(clinicObject.getString("count"));
                TextView write_review_clinic_accessibility = (TextView) getView().findViewById(R.id.write_review_clinic_accessibility);
                write_review_clinic_accessibility.setTypeface(typeface);

                //availability
                JSONObject availabilityObject = ratingOnject.getJSONObject("availability");

                if(availabilityObject.has("avg") && (! availabilityObject.getString("avg").equals("null")))
                {
                    availabilityRatingbar.setRating(Float.valueOf(availabilityObject.getString("avg")));
                    doctorAvgRating += Float.valueOf(availabilityObject.getString("avg"));
                }
                else {
                    Log.v("availabilityRatingbar","Avg is null.");
                    availabilityRatingbar.setRating(0.0f);

                }

                //availabilityRatingbar.setRating(Float.valueOf(availabilityObject.getString("avg")));
                totalAvailibilityInEmergencies.setText(availabilityObject.getString("count"));
                TextView write_review_availability_in_emergencies = (TextView) getView().findViewById(R.id.write_review_availability_in_emergencies);
                write_review_availability_in_emergencies.setTypeface(typeface);

                //approachability
                JSONObject approachabilityObject = ratingOnject.getJSONObject("approachability");

                if(approachabilityObject.has("avg") && (! approachabilityObject.getString("avg").equals("null")))
                {
                    approachabilityRatingbar.setRating(Float.valueOf(approachabilityObject.getString("avg")));
                    doctorAvgRating += Float.valueOf(approachabilityObject.getString("avg"));
                }
                else {
                    Log.v("approachabilityRating","Avg is null.");
                    approachabilityRatingbar.setRating(0.0f);

                }

                //approachabilityRatingbar.setRating(Float.valueOf(approachabilityObject.getString("avg")));
                totalReviewApproachability.setText(approachabilityObject.getString("count"));
                TextView write_review_approachability = (TextView) getView().findViewById(R.id.write_review_approachability);
                write_review_approachability.setTypeface(typeface);

                //technology
                JSONObject technologyObject = ratingOnject.getJSONObject("technology");

                if(technologyObject.has("avg") && (! technologyObject.getString("avg").equals("null")))
                {
                    technologyRatingbar.setRating(Float.valueOf(technologyObject.getString("avg")));
                    doctorAvgRating += Float.valueOf(technologyObject.getString("avg"));
                }
                else {
                    Log.v("technologyRatingbar","Avg is null.");
                    technologyRatingbar.setRating(0.0f);

                }

                //technologyRatingbar.setRating(Float.valueOf(technologyObject.getString("avg")));
                totalReviewTechnologyAndEquipment.setText(technologyObject.getString("count"));
                TextView write_review_technology_and_equipment = (TextView) getView().findViewById(R.id.write_review_technology_and_equipment);
                write_review_technology_and_equipment.setTypeface(typeface);
                RatingBar ratingBar = (RatingBar) getView().findViewById(R.id.doctor_user_rating);
                ratingBar.setRating(doctorAvgRating / 5);
                //Log.v("Avg Review", String.valueOf(doctorAvgRating / 5));

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    class DoctorAllReviews extends AsyncTask<String, Void, String> {

        String quickDoctorId = doctorId;
        String URL = WEB_SERVICE_URL + "review/ratingUserDetail";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            Log.v("Doctor id", quickDoctorId);
            nameValuePairs.add(new BasicNameValuePair("doctorId", quickDoctorId));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if(result != null){
                new RenderDoctorAllReviews(result);
            } else{
                if(getContext() != null){
                    Toast.makeText(getContext(), getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            progressBar.setVisibility(View.GONE);
            doctorProfileDetails.setVisibility(View.VISIBLE);
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            //navProfilePic.setImageBitmap(result);
            //progressBar.setVisibility(View.GONE);
            //profileDetails.setVisibility(View.VISIBLE);
        }
    }

    class RenderDoctorAllReviews {
        RenderDoctorAllReviews(String result) {
            LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.allReviewList);
            linearLayout.removeAllViews();
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            );

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("false")) {
                    doctorWriteReview.setVisibility(View.VISIBLE);
                    avgRating.setVisibility(View.GONE);
                } else {
                    //doctorWriteReview.setVisibility(View.GONE);
                    avgRating.setVisibility(View.VISIBLE);
                    doctorWriteReview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });
                    JSONArray reviewArray = jsonObject.getJSONArray("data");
                    Log.v("data", reviewArray.toString());
                    int totalDoctorReviewCount = reviewArray.length();
                    totalReviewCount = totalDoctorReviewCount;
                    totalReviews.setText(String.valueOf(totalDoctorReviewCount) + " " + getResources().getString(R.string.reviews));
                    for (int i = 0; i < reviewArray.length(); i++) {
                        final JSONObject filterData = reviewArray.getJSONObject(i);
                        View view;
                        LayoutInflater layoutInflater = (LayoutInflater) getContext()
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = layoutInflater.inflate(R.layout.doctor_review_list, null);
                        final ImageView profilePic = (ImageView) view.findViewById(R.id.user_profile_pic);

                        //user name
                        TextView textView1 = (TextView) view.findViewById(R.id.user_name);
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));
                        if (filterData.has("visible") && filterData.getString("visible").equals("1")) {
                            textView1.setText(filterData.getString("name"));
                        } else {
                            textView1.setText(getResources().getString(R.string.anonymous));
                        }

                        //date time
                        TextView commentTime = (TextView) view.findViewById(R.id.review_date_time);
                        commentTime.setText(filterData.getString("commentTime"));
                        commentTime.setTypeface(typeface);

                        //user rating
                        RatingBar userRating = (RatingBar) view.findViewById(R.id.user_review);
                        userRating.setRating(Float.valueOf(filterData.getString("avgRating")));

                        //user comment
                        TextView userComment = (TextView) view.findViewById(R.id.user_review_text);
                        userComment.setText(filterData.getString("comment"));
                        userComment.setTypeface(typeface);

                        //total photos and videos
                        TextView totalPhotoAndVideos = (TextView) view.findViewById(R.id.total_photo_video);
                        totalPhotoAndVideos.setTypeface(typeface);

                        //share review
                        TextView shareReview = (TextView) view.findViewById(R.id.share_review);
                        shareReview.setTypeface(typeface, typeface.BOLD);

                        shareReview.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    shareIt(filterData.getString("name"), "", "", filterData.getString("avgRating"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        //photo and video
                        ImageView photoAndVideo = (ImageView) view.findViewById(R.id.photoAndVideo);
                        photoAndVideo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast.makeText(getContext(),
                                        getResources().getString(R.string.no_photos_to_show), Toast.LENGTH_LONG).show();
                                //startActivity(new Intent(getContext(), PhotoOrVideoActivity.class));
                            }
                        });

                        linearLayout.addView(view);

                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                try {
                                    if (filterData.has("visible") && filterData.getString("visible").equals("1")) {
                                        String userID = filterData.getString("userId");
                                        userReviewDetails(filterData.getString("review_id"),
                                                filterData.getString("name"));
                                    } else {
                                        Toast.makeText(getContext(),
                                                getResources().getString(R.string.anonymous_profile_alert_message), Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        if (!filterData.getString("photo").equals("null") &&
                                filterData.getString("visible").equals("1")) {
                            Log.d("photourl","url"+filterData.getString("photo"));
                            Picasso.with(getContext())
                                    .load(filterData.getString("photo"))
                                    .resize(50, 50)
                                    .centerCrop()
                                    .into(profilePic);
//                                    .into(new Target() {
//                                        @Override
//                                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                                            profilePic.setImageBitmap(bitmap);
//                                        }
//
//                                        @Override
//                                        public void onBitmapFailed(Drawable errorDrawable) {
//                                            profilePic.setImageDrawable(errorDrawable);
//                                        }
//
//                                        @Override
//                                        public void onPrepareLoad(Drawable placeHolderDrawable) {
//                                            profilePic.setImageDrawable(placeHolderDrawable);
//                                        }
//                                    });
//                            new DownloadImageTask(profilePic).execute(filterData.getString("photo"));
                        } else {
                            profilePic.setImageResource(R.mipmap.first_name);
                        }

                        //review icon
                        ImageView reviewIconImage = (ImageView) view.findViewById(R.id.reviewIcon);
                        if (filterData.has("reviewIcon")) {
                            final String reviewIcon = filterData.getString("reviewIcon");
                            if (!reviewIcon.equals("0")) {
                                int iconId = getResources().getIdentifier("doctor_" + reviewIcon,
                                        "drawable", getContext().getPackageName());
                                reviewIconImage.setImageResource(iconId);
                                iconList.add("doctor_" + reviewIcon);
                            }
                        }
                    }
                }
                if (iconList.size() > 0) {
                    set.addAll(iconList);
                    TableRow iconListLayout = (TableRow) getView().findViewById(R.id.iconsTableRow);
                    iconListLayout.setVisibility(View.VISIBLE);
                    int dp = -20;
                    for (String temp : set) {
                        int id = getContext().getResources().getIdentifier(temp, "drawable",
                                getContext().getPackageName());
                        int imageid = getContext().getResources().getIdentifier(temp, "id",
                                getContext().getPackageName());
                        if (id !=0) {
                            ImageView imageView = (ImageView) getView().findViewById(imageid);
                            imageView.setImageResource(id);
                            imageView.setVisibility(View.VISIBLE);
                            float d = getContext().getResources().getDisplayMetrics().density;
                            int margin = (int) (dp * d);
                            imageView.setPadding(margin, 0, margin, 0);
                            dp += 30;
                        }

                    }
                    TextView textView = (TextView) getView().findViewById(R.id.totalIconCount);
                    textView.setText(String.valueOf(totalReviewCount));
                    textView.setTypeface(typeface);
                    textView.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void userReviewDetails(final String reviewId, final String name) {
        Bundle args = new Bundle();
        args.putString("id", reviewId);
        args.putString("name", name);
        args.putString("type", "doctor");
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        ReviewDetailsFragment reviewDetailsFragment = new ReviewDetailsFragment();
        reviewDetailsFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("reviewDetails");
        mFragmentTransaction.add(R.id.search_layout, reviewDetailsFragment).commit();
    }

    public void userProfile(final String userId) {
        Bundle args = new Bundle();
        args.putString("id", userId);
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        UserProfileFragment profileFragment = new UserProfileFragment();
        profileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("doctorProfile");
        mFragmentTransaction.add(R.id.search_layout, profileFragment).commit();
    }

    @Override
    public void onResume(){
        super.onResume();
        BaseActivity.showMenu();
        BaseActivity.buttonFragment = "DoctorProfile";
        doctorAvgRatingAsynck = new DoctorAvgRating();
        doctorAvgRatingAsynck.execute(doctorId);

        doctorAllReviewsAsynck = new DoctorAllReviews();
        doctorAllReviewsAsynck.execute(doctorId);

        /*BaseActivity.mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                BaseActivity.mNavigationView.getMenu().findItem(item.getItemId()).setChecked(true);

                if(item.getItemId() == R.id.write_a_review) {
                    writeReviewButton.performClick();
                }

                if(item.getItemId() == R.id.claim_profile) {
                    claimProfile.performClick();
                }

                if(item.getItemId() == R.id.share) {
                    socialShare.performClick();
                }
                BaseActivity.mDrawerLayout.closeDrawers();
                return false;
            }
        });*/
    }

    public void doctorBookMarks(final String doctorId) {

        class DoctorBookmarksAddUpdate extends AsyncTask<String, Void, String> {

            String URL = WEB_SERVICE_URL + "bookmark/add";

            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            String quickDoctorId = doctorId;
            String quickUserId = Preference.getValue(getContext(), "LOGIN_ID", "");

            private String resultStatus = "";

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("doctorId", quickDoctorId));
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("lang", appLang));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    return mClient.execute(httpPost, responseHandler);
                } catch(IOException e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.v("Bookmarks result", result);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String responseResult = jsonObject.getString("result");
                    String doctorName = jsonObject.getString("name");

                    if(responseResult.equals("Bookmarked") || responseResult.equals("Bookmark is added")) {
                        //addToBookmarks.setImageResource(R.mipmap.add_a_lab);
                        //Preference.setValue(getContext(), "BOOKMARK_STATUS", "true");
                        Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmarkd_add), Toast.LENGTH_SHORT).show();
                    } else {
                        //addToBookmarks.setImageResource(R.mipmap.bookmarks);
                        //Preference.setValue(getContext(), "BOOKMARK_STATUS", "false");
                        Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmark_remove), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
            }
        }

        DoctorBookmarksAddUpdate doctorBookmarksAddUpdate = new DoctorBookmarksAddUpdate();
        doctorBookmarksAddUpdate.execute(doctorId);
    }

    @Override
    public void onStop() {
        super.onStop();
        doctorProfileAsynck.cancel(true);
        if (doctorAvgRatingAsynck != null)
            doctorAvgRatingAsynck.cancel(true);
        if (doctorAllReviewsAsynck != null)
            doctorAllReviewsAsynck.cancel(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        BaseActivity.hideMenu();
    }
}
