package searchnative.com.topdoctors;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.media.Rating;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.text.Line;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

import static android.widget.Toast.*;
import static com.google.android.gms.analytics.internal.zzy.e;

/**
 * Created by root on 9/9/16.
 */
public class SearchFragment extends Fragment implements ScrollViewListener{

    public static EditText searchKeyword;
   // String searchData;
    Typeface typeface;
    public static LinearLayout linearLayout;
    String webServiceUrl = AppConfig.getWebServiceUrl();
    private ProgressDialog mProgressDialog;
    RelativeLayout relativeLayout;
    private MarkerOptions options = new MarkerOptions();
    private ArrayList<LatLng> latLngs = new ArrayList<>();
    //private double latitude, longitude;
    private SupportMapFragment supportMapFragment;
    private ImageView mapLocation;
    private LinearLayout searchResultListing,searchResultLocation;
    private boolean isListingLayout = true;
    private List<Double> latitudeList = new ArrayList<>();
    private List<Double> longitudeList = new ArrayList<>();
    private List<String> titles = new ArrayList<>();
    private List<String> mapTypes = new ArrayList<>();
    private boolean isMapLoaded = false;
    private TextView topTenDoctorText;
    private List specialityIcon;
    private String appLang = LocalInformation.getLocaleLang();
    private String loginUserId;
    private ProgressBar progressBar;
    private LinearLayout searchResultList;
    private String bookmarkMipmap;
    private String labSpeciality, clinicSpeciality, isNearBy = "", isTopRatedFirst = "", lat, lon;
    GuestLogin guestLogin;
    private boolean isGuest;
    private int totalSearchResult = 0, totalResultRendered = 0, totalRequestNo = 0;
    private ImageView user_profile_pic_bookmark;

    //async variable
    private String searchAsync, locationAsync, specialityAsync, genderAsync, nameAsync, searchTypeAsync,
            doctoridAsync, doctornameAsync, callDialerAsync, doctorAddressAsync, doctorRatingAsync, workidAsync;
    //Async classes
    GlobalSearch globalSearch;
    SearchFilter searchFilterAsync;
    DoctorCustomSearch doctorCustomSearch;
    HospitalSearch hospitalSearch;
    ClinicSearch clinicSearch;
    LabSearch labSearch;
    UserBookmarkList userBookmarkList;
    GetAllWorkMaster getAllWorkMaster;
    DoctorBookmarksAddUpdate doctorBookmarksAddUpdate;
    WorkTypeBookmark workTypeBookmark;
    private ScrollViewExt scrollView;


    String[] resarray;
    LinearLayout parent;
    LinearLayout linearMainCell;
    LinearLayout specialityLayout;
    View dialoglayout;

    //int j=1;


    private static boolean isAsynchOn = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Fabric.with(getContext(),new Crashlytics());
        View view = inflater.inflate(R.layout.search_layout, container, false);

        return view;
    }

    @Override
    public void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy) {
        // We take the last son in the scrollview
        View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
        int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

        // if diff is zero, then the bottom has been reached
        if (diff == 0) {
            //check for Internet
            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                return;
            }
            if (Preference.getValue(getContext(), "DOCTOR_SEARCH_PAGINATE", "").equals("true")) {
                if (totalSearchResult > totalResultRendered) {
                    if(isAsynchOn) {
                        isAsynchOn = false;
                            doctorCustomSearch = new DoctorCustomSearch();
                            doctorCustomSearch.execute();
                            if (doctorCustomSearch.getStatus() == AsyncTask.Status.FINISHED) {
                                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                    mProgressDialog.dismiss();
                                    mProgressDialog = null;
                                }
                            }
                        }
                }

            }

            /*hospital pagination while scrolling*/
            if (CustomSearchHandle.getHospitalSearchPaginate(getContext())) {
                if (totalSearchResult > totalResultRendered) {
                    if (isAsynchOn) {
                        isAsynchOn = false;
                        hospitalSearch = new HospitalSearch();
                        hospitalSearch.execute();
                        if (hospitalSearch.getStatus() == AsyncTask.Status.FINISHED) {
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                                mProgressDialog = null;
                            }
                        }
                    }
                }
            }

            /*clinic pagination while scrolling*/
            if (CustomSearchHandle.getClinicPaginate(getContext())) {
                if (totalSearchResult > totalResultRendered) {
                    if (isAsynchOn) {
                        isAsynchOn = false;
                        clinicSearch = new ClinicSearch();
                        clinicSearch.execute();
                        if (clinicSearch.getStatus() == AsyncTask.Status.FINISHED) {
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                                mProgressDialog = null;
                            }
                        }
                    }
                }
            }

            /*lab pagination while scrolling*/
            if(CustomSearchHandle.getLabPaginate(getContext())) {
                Log.v("Status", "Bottom reached!");
                if (totalSearchResult > totalResultRendered) {
                    if (isAsynchOn) {
                        isAsynchOn = false;
                        labSearch = new LabSearch();
                        labSearch.execute();
                        if (labSearch.getStatus() == AsyncTask.Status.FINISHED) {
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                                mProgressDialog = null;
                            }
                        }
                    }
                }
            }

            /*Work maseter pagination while scrolling*/
            if (CustomSearchHandle.getAllWorkMasterPaginate(getContext())) {
                if (totalSearchResult > totalResultRendered) {
                    if (isAsynchOn) {
                        isAsynchOn = false;
                        getAllWorkMaster = new GetAllWorkMaster();
                        getAllWorkMaster.execute();
                        if (getAllWorkMaster.getStatus() == AsyncTask.Status.FINISHED) {
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                                mProgressDialog = null;
                            }
                        }
                    }
                }
            }

            /*global search pagination when scrolling*/
            if (CustomSearchHandle.getGlobalSearchPaginate(getContext())) {
                if (totalSearchResult > totalResultRendered) {
                    if (isAsynchOn) {
                        isAsynchOn = false;
                        globalSearch = new GlobalSearch();
                        globalSearch.execute();
                        if (globalSearch.getStatus() == AsyncTask.Status.FINISHED) {
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                                mProgressDialog = null;
                            }
                        }
                    }
                }
            }

            /*filter search pagination when scrolling*/
            if (CustomSearchHandle.getFilterSearchPaginate(getContext())) {
                if (totalSearchResult > totalResultRendered) {
                    if (isAsynchOn) {
                        isAsynchOn = false;
                        searchFilterAsync = new SearchFilter();
                        searchFilterAsync.execute();
                        if (searchFilterAsync.getStatus() == AsyncTask.Status.FINISHED) {
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                                mProgressDialog = null;
                            }
                        }
                    }
                }
            }

            /*if (CustomSearchHandle.getBookmatkPaginate(getContext())) {
                if (isAsynchOn) {
                    isAsynchOn = false;
                    userBookmarkList = new UserBookmarkList();
                    userBookmarkList.execute();
                    if (userBookmarkList.getStatus() == AsyncTask.Status.FINISHED) {
                        if (mProgressDialog != null && mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                            mProgressDialog = null;
                        }
                    }
                }
            }*/
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //progress bar
        guestLogin = new GuestLogin();
        isGuest = guestLogin.getGuestLogin(getContext());
        searchResultList = (LinearLayout) getView().findViewById(R.id.search_result_layout);
        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        searchResultList.setVisibility(View.INVISIBLE);
        loginUserId = Preference.getValue(getContext(), "LOGIN_ID", "");
        scrollView = (ScrollViewExt) getView().findViewById(R.id.home_page_form_search_scroll);
        scrollView.setScrollViewListener(this);

        /*scrollView.setOnBottomReachedListener(new InteractiveScrollView.OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                if (Preference.getValue(getContext(), "DOCTOR_SEARCH_CUSTOM", "").equals("true")) {
                    doctorCustomSearch = new DoctorCustomSearch();
                    doctorCustomSearch.execute();

                }
                Log.v("Total request", String.valueOf(totalRequestNo));
            }
        });*/

        clearPreferences();
        //linearLayout.removeAllViews();
        //CustomSearchHandle.setAllWorkMasterPaginate(getContext());
        //getAllWorkMaster = new GetAllWorkMaster();
        //getAllWorkMaster.execute();

        globalSearch = new GlobalSearch();
        globalSearch.execute();
        CustomSearchHandle.setGlobalSearchPaginate(getContext());
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        //Preference.setValue(getContext(), "PRIMARY_SEARCH", "true");
        //Preference.setValue(getContext(), "PROMARY_SEARCH_KEY", "");
        setMenuVisibility(true);


        relativeLayout = (RelativeLayout) getView().findViewById(R.id.search_layout);
        relativeLayout.setBackgroundColor(Color.WHITE);
        searchResultListing = (LinearLayout) getView().findViewById(R.id.search_result_listing);
        searchResultLocation = (LinearLayout) getView().findViewById(R.id.search_result_with_location);

        specialityIcon = new ArrayList();
        specialityIcon.addAll(SpecialityData.getmInstance().specialityListIcon);

        typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/ExoMedium.otf");

        ImageView v = (ImageView) getActivity().findViewById(R.id.search_result_menu);
        searchKeyword = (EditText) getView().findViewById(R.id.hope_page_main_search_custom);

        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                DrawerLayout mDrawer = (DrawerLayout) getActivity().findViewById(R.id.drawerLayout);
                mDrawer.openDrawer(Gravity.LEFT);
                return true;
            }
        });

        linearLayout = (LinearLayout) getView().findViewById(R.id.search_result_layout);

        //new GetAllWorkMaster().execute();
        final EditText editText = (EditText) getView().findViewById(R.id.hope_page_main_search_custom);
        if(appLang.equals("ar")) {
            editText.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        if(event.getX() <= (editText.getCompoundDrawables()[DRAWABLE_LEFT].getBounds‌​().width())) {
                            // your action here
                            //view.clearFocus();
                            Preference.setValue(getContext(), "SEARCH_KEYWORD", editText.getText().toString());

                            Intent i = new Intent(getContext(),SearchFilterPop.class);
                            i.putExtra("isCustom",true);
                            startActivity(i);

                            //startActivity(new Intent(getContext(), SearchFilterPop.class));

                            //InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            //imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            relativeLayout = (RelativeLayout) getView().findViewById(R.id.search_layout);
                            relativeLayout.setBackgroundColor(Color.GRAY);
                            return true;
                        }

                    }

                    return false;
                }
            });

        } else {
            editText.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        if(event.getRawX() >= (editText.getRight() - editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            // your action here
                            //view.clearFocus();
                            Preference.setValue(getContext(), "SEARCH_KEYWORD", editText.getText().toString());

                            Intent i = new Intent(getContext(),SearchFilterPop.class);
                            i.putExtra("isCustom",true);
                            startActivity(i);
                           // startActivity(new Intent(getContext(), SearchFilterPop.class));


                            //InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            //imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            relativeLayout = (RelativeLayout) getView().findViewById(R.id.search_layout);
                            relativeLayout.setBackgroundColor(Color.GRAY);
                            return true;
                        }

                    }

                    return false;
                }
            });
        }

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if(i == EditorInfo.IME_ACTION_SEARCH) {
                    totalRequestNo = 0;
                    totalResultRendered = 0;
                    totalSearchResult = 0;
                    searchAsync = editText.getText().toString();
                    globalSearch = new GlobalSearch();
                    globalSearch.execute();
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    Preference.setValue(getContext(), "PRIMARY_SEARCH", "true");
                    Preference.setValue(getContext(), "PROMARY_SEARCH_KEY", editText.getText().toString());
                    setMenuVisibility(true);
                    //Preference.setValue(getContext(), "SEARCH_KEYWORD", searchAsync);
                    return true;
                }

                return false;
            }
        });

        mapLocation = (ImageView) getView().findViewById(R.id.search_map_location);
        topTenDoctorText = (TextView) getView().findViewById(R.id.top_ten_doctors_text);
        topTenDoctorText.setVisibility(View.GONE);
        topTenDoctorText.setTypeface(typeface);
        mapLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isListingLayout) {
                    searchResultListing.setVisibility(View.VISIBLE);
                    searchResultLocation.setVisibility(View.GONE);
                    isListingLayout = false;
                    mapLocation.setImageResource(R.mipmap.maplocation);
                } else {
                    FragmentManager fragmentManager;
                    fragmentManager = getActivity().getSupportFragmentManager();
                    supportMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.search_result_relative_layout);
                    if(!isMapLoaded) {
                        supportMapFragment = SupportMapFragment.newInstance();
                        isMapLoaded = true;
                    }
                    supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(final GoogleMap map) {
                            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                            //map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlg, 16));
//                            try {
                                for(int i = 0; i < latitudeList.size(); i++) {
                                    if (mapTypes.get(i).equals("doctor")) {
                                        map.addMarker(new MarkerOptions().position(new
                                                LatLng(Double.valueOf(latitudeList.get(i)), Double.valueOf(longitudeList.get(i))))
                                                .title(titles.get(i).toString())
                                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.doctor_location)));
                                    } else if (mapTypes.get(i).equals("hospital")) {
                                        map.addMarker(new MarkerOptions().position(new
                                                LatLng(Double.valueOf(latitudeList.get(i)), Double.valueOf(longitudeList.get(i))))
                                                .title(titles.get(i).toString())
                                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.hospital_dark)));
                                    } else if (mapTypes.get(i).equals("clinic")) {
                                        map.addMarker(new MarkerOptions().position(new
                                                LatLng(Double.valueOf(latitudeList.get(i)), Double.valueOf(longitudeList.get(i))))
                                                .title(titles.get(i).toString())
                                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.clinic_dark)));
                                    } else if (mapTypes.get(i).equals("lab") || mapTypes.get(i).equals("Radiology Lab") || mapTypes.get(i).equals("Medical Lab")) {
                                        map.addMarker(new MarkerOptions().position(new
                                                LatLng(Double.valueOf(latitudeList.get(i)), Double.valueOf(longitudeList.get(i))))
                                                .title(titles.get(i).toString())
                                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.lab_dark)));
                                    }
                                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.valueOf(latitudeList.get(i)),
                                            Double.valueOf(longitudeList.get(i))), 10));
                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }

                            map.setTrafficEnabled(true);
                            map.setIndoorEnabled(true);
                            map.setBuildingsEnabled(true);
                            map.getUiSettings().setZoomControlsEnabled(true);
                        }
                    });
                    fragmentManager.beginTransaction().replace(R.id.search_result_relative_layout, supportMapFragment).commit();

                    searchResultListing.setVisibility(View.GONE);
                    searchResultLocation.setVisibility(View.VISIBLE);
                    isListingLayout = true;
                    mapLocation.setImageResource(R.mipmap.menu_1);
                }
            }
        });
    }

    public void listingOrMapLayout() {
        isListingLayout = true;
        isMapLoaded = false;
        if (! (latitudeList.size() > 0 || longitudeList.size() > 0) ) {
            latitudeList = new ArrayList<>();
            longitudeList = new ArrayList<>();
        }
        if(isListingLayout) {
            searchResultListing.setVisibility(View.VISIBLE);
            searchResultLocation.setVisibility(View.GONE);
            isListingLayout = false;
            mapLocation.setImageResource(R.mipmap.maplocation);
        } else {
            searchResultListing.setVisibility(View.GONE);
            searchResultLocation.setVisibility(View.VISIBLE);
            isListingLayout = true;
            mapLocation.setImageResource(R.mipmap.maplocation);
        }
    }

    /**
     * Global search async
     */
    class GlobalSearch extends AsyncTask<String, Void, String> {
        String URL = webServiceUrl + "search/globalSearch";
        //String URL = "http://192.168.1.28/topdoctor/api/search/globalSearch";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("search", searchAsync));
            nameValuePairs.add(new BasicNameValuePair("lang", LocalInformation.getLocaleLang()));
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("page", String.valueOf(totalRequestNo)));

            try {
                HttpPost httpPost = new HttpPost(URL);
                Log.v("URL", URL);
                Log.v("Param", nameValuePairs.toString());
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if(result != null){
                new RenderSearchResult(result);
            }else {
                if(getContext() != null){
                    Toast.makeText(getContext(), getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            isAsynchOn = true;
            /*if (totalRequestNo <= 1)
                hideProgrssBar();
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }*/
            hideProgrssBar();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            totalRequestNo++;
            progressBar.setVisibility(View.VISIBLE);
            if (totalRequestNo <= 1)
                showProgressBar();
            else {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
            }
            Preference.setValue(getContext(), "PRIMARY_SEARCH", "");
            Preference.setValue(getContext(), "PROMARY_SEARCH_KEY", "");

        }
    }

    /**
     * Search filter async
     */
    class SearchFilter extends AsyncTask<String, Void, String> {
        String URL = webServiceUrl + "search/filter";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");
        String quickUserId = Preference.getValue(getContext(), "LOGIN_ID", "");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("location", locationAsync));
            nameValuePairs.add(new BasicNameValuePair("speciality", specialityAsync));
            nameValuePairs.add(new BasicNameValuePair("gender", genderAsync));
            nameValuePairs.add(new BasicNameValuePair("name", nameAsync));
            // put searachdata value  "" for getting proper result.
            nameValuePairs.add(new BasicNameValuePair("searchdata", ""));
            nameValuePairs.add(new BasicNameValuePair("searchType", searchTypeAsync));
            nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));
            nameValuePairs.add(new BasicNameValuePair("page", String.valueOf(totalRequestNo)));
            nameValuePairs.add(new BasicNameValuePair("lat", lat));
            nameValuePairs.add(new BasicNameValuePair("long", lon));
            nameValuePairs.add(new BasicNameValuePair("isnearby", isNearBy));
            nameValuePairs.add(new BasicNameValuePair("topratestfirst", isTopRatedFirst));

            try {
                HttpPost httpPost = new HttpPost(URL);
                Log.v("GLobal", URL);
                Log.v("namevalue", nameValuePairs.toString());
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            /*if(result != null) {
                if (Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", "").equals("doctor")) {
                    new RenderDoctorSearchResult(result);
                } else if (Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", "").equals("hospital")) {
                    new RenderHospitelSearchResult(result);
                } else if (Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", "").equals("clinic")) {
                    new RenderClinicSearchResult(result);
                } else if (Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", "").equals("Radiology Lab") ||
                        Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", "").equals("Medical Lab") ||
                        Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", "").equals("Lab") ||
                        Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", "").equals("lab")) {
                    new RenderLabSearchResult(result);
                } else {
                    Log.v("Log.v", Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", ""));
                    new RenderSearchResult(result);
                }
            }else {
                if(getContext() != null){
                    Toast.makeText(getContext(), getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }*/



            // 21 Dec 5:13 mayur commented this
            if(result != null){
                if(Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", "").equals("doctor")) {
                    new RenderDoctorSearchResult(result);
                } else {
                    Log.v("Log.v", Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", ""));
                    new RenderSearchResult(result);
                }
            }else{
                if(getContext() != null){
                    Toast.makeText(getContext(), getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }




            /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }*/
            isAsynchOn = true;
            hideProgrssBar();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            totalRequestNo++;
            if (totalRequestNo <= 1) {
                showProgressBar();
            } else {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
            }

        }
    }

    /**
     * get all work master details async
     */
    public class GetAllWorkMaster extends AsyncTask<Void, Void, String> {
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(Void... params) {
            String URL = webServiceUrl + "work/allWorkMaster?lang=" + appLang +
                    "&userId=" + loginUserId + "&page=" + totalRequestNo;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            //nameValuePairs.add(new BasicNameValuePair("lang", appLang));
            try {
                HttpGet httpGet = new HttpGet(URL);
                Log.v("url", URL);
                //httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();

                return mClient.execute(httpGet, responseHandler);
            } catch (ClientProtocolException exception) {
                exception.printStackTrace();
            } catch (IOException exception) {
                exception.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if(result != null){
                Log.v("Render Work All result", result);
                new RenderSearchResult(result);
            }else{
                if(getContext() != null){
                    Toast.makeText(getContext(), getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            isAsynchOn = true;

            /*if (totalRequestNo <= 1) {
                progressBar.setVisibility(View.GONE);
                searchResultList.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
                searchResultList.setVisibility(View.VISIBLE);
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }*/
            hideProgrssBar();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            totalRequestNo++;
            CustomSearchHandle.resetAllWorkMasterSearch(getContext());
            CustomSearchHandle.resetPaginateForWorkMaster(getContext());
            if (totalRequestNo <= 1) {
                showProgressBar();
            } else {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
            }
        }
    }

    /**
     * doctor custom search async
     */
    class DoctorCustomSearch extends AsyncTask<String, Void, String> {
        String URL = webServiceUrl + "search/doctorSearch";
        //String URL = "http://192.168.1.28/topdoctor/api/search/doctorSearch";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("location", locationAsync));
            nameValuePairs.add(new BasicNameValuePair("speciality", specialityAsync));
            nameValuePairs.add(new BasicNameValuePair("gender", genderAsync));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("page", String.valueOf(totalRequestNo)));
            nameValuePairs.add(new BasicNameValuePair("name", nameAsync));
            Log.v("name value pair", nameValuePairs.toString());
            //Log.v("Application Lang:", appLang);
            try {
                HttpPost httpPost = new HttpPost(URL);
                Log.v("URL", URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                //Log.v("URL", URL);
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            //Log.v("Doctor Result: ", result);
            if(result != null){
                new RenderDoctorSearchResult(result);
            }else{
                if(getContext() != null){
                    Toast.makeText(getContext(),getResources().getString(R.string.error_message),Toast.LENGTH_SHORT).show();
                }
            }

            isAsynchOn = true;
            Log.v("request no: ", String.valueOf(totalRequestNo));
            /*if (totalRequestNo <= 1) {
                //hideProgrssBar();
            } else {
                progressBar.setVisibility(View.GONE);
            }*/
            hideProgrssBar();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            totalRequestNo++;
            Preference.setValue(getContext(), "DOCTOR_SEARCH_CUSTOM", "");
            if (totalRequestNo <= 1)
                showProgressBar();
            else {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
            }
        }
    }

    /**
     * hospital search async
     */
    class HospitalSearch extends AsyncTask<Void, Void, String> {
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(Void... params) {
            String URL = webServiceUrl + "search/hospitalSearch?lang=" + appLang
                    +"&userId=" + loginUserId + "&page=" + totalRequestNo;
            //List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            //nameValuePairs.add(new BasicNameValuePair("lang", appLang));
            try {
                HttpGet httpGet = new HttpGet(URL);
                Log.v("url", URL);
                //httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();

                return mClient.execute(httpGet, responseHandler);
            } catch (ClientProtocolException exception) {
                exception.printStackTrace();
            } catch (IOException exception) {
                exception.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            new RenderHospitelSearchResult(result);
            isAsynchOn = true;
            /*if (totalRequestNo <= 1)
                hideProgrssBar();
            else {
                hideProgrssBar();
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }*/
            hideProgrssBar();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            totalRequestNo++;
            Log.v("request no", String.valueOf(totalRequestNo));
            Preference.setValue(getContext(), "HOSPITAL_SEARCH", "");
            if (totalRequestNo <= 1)
                showProgressBar();
            else {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
            }
        }
    }

    /**
     * clinic search async
     */
    class ClinicSearch extends AsyncTask<Void, Void, String> {
        String URL = webServiceUrl + "search/clinicSearch";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(Void... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));
            nameValuePairs.add(new BasicNameValuePair("page", String.valueOf(totalRequestNo)));

            try {
                HttpPost httpPost = new HttpPost(URL);
                Log.v("URL", URL);
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                return mClient.execute(httpPost, responseHandler);
            } catch (ClientProtocolException exception) {
                exception.printStackTrace();
            } catch (IOException exception) {
                exception.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if(result != null){
                new RenderClinicSearchResult(result);
            }else{
                if(getContext() != null){
                    Toast.makeText(getContext(),getResources().getString(R.string.error_message),Toast.LENGTH_SHORT).show();
                }
            }

            isAsynchOn = true;
            Log.v("request no: ", String.valueOf(totalRequestNo));
            /*if (totalRequestNo <= 1) {
                hideProgrssBar();
            } else {
                hideProgrssBar();
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }*/
            hideProgrssBar();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            totalRequestNo++;
            Preference.setValue(getContext(), "CLINIC_SEARCH", "");
            if (totalRequestNo <= 1)
                showProgressBar();
            else {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
            }
        }
    }

    /**
     * lab search async
     */
    class LabSearch extends AsyncTask<String, Void, String> {
        //String URL = "http://192.168.1.28/topdoctor/api/search/labSearch";
        String URL = webServiceUrl + "search/labSearch";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("speciality", specialityAsync));
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            //nameValuePairs.add(new BasicNameValuePair("gender", quickGender));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));
            nameValuePairs.add(new BasicNameValuePair("page", String.valueOf(totalRequestNo)));
            Log.v("Lab url", URL);
            Log.v("Lab data", nameValuePairs.toString());

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                new RenderLabSearchResult(result);
                isAsynchOn = true;
                /*if (totalRequestNo <=1)
                    hideProgrssBar();
                else {
                    hideProgrssBar();
                    if (mProgressDialog != null && mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                        mProgressDialog = null;
                    }
                }*/
            } else {
                new RenderLabSearchResult("");
                Log.v("Response", "Service unavailable");
            }
            hideProgrssBar();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            totalRequestNo++;
            Preference.setValue(getContext(), "LAB_SEARCH_CUSTOM", "");
            if (totalRequestNo <= 1)
                showProgressBar();
            else {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
            }
        }
    }

    /**
     * doctor bookmarks add update async
     */
    class DoctorBookmarksAddUpdate extends AsyncTask<String, Void, String> {
        String URL = AppConfig.getWebServiceUrl() + "bookmark/add";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");
        String quickUserId = Preference.getValue(getContext(), "LOGIN_ID", "");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("doctorId", workidAsync));
            nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));

            try {
                Log.v("Bookmark URL", URL);
                Log.v("namevalue", nameValuePairs.toString());
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String responseResult = jsonObject.getString("result");
                Log.v("Result", responseResult);
                String doctorName = jsonObject.getString("name");

                if(responseResult.equals("Bookmarked") || responseResult.equals("Bookmark is added")) {
                    //addToBookmarks.setImageResource(R.mipmap.lab_dark);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "true");
                    Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmarkd_add), Toast.LENGTH_SHORT).show();
                } else {
                    //addToBookmarks.setImageResource(R.mipmap.bookmarks);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "false");
                    Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmark_remove), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            //hideProgrssBar();
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
            //showProgressBar();
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
        }
    }

    /**
     * worktype bookmark async
     */
    class WorkTypeBookmark extends AsyncTask<String, Void, String> {
        final String URL = AppConfig.getWebServiceUrl() + "Worktype_bookmark/Add";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            Log.v("bookmark", "worktype");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("workId", workidAsync));
            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.v("result", result.toString());
            try {
                JSONObject jsonObject = new JSONObject(result);
                String responseResult = jsonObject.getString("result");
                String doctorName = jsonObject.getString("name");

                if(responseResult.equals("Bookmarked")) {
                    //addToBookmarks.setImageResource(R.mipmap.lab_dark);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "true");
                    Toast.makeText(getContext(), doctorName + " is successfully added in your bookmark.", Toast.LENGTH_SHORT).show();
                } else if(responseResult.equals("Bookmark is added")) {
                    Toast.makeText(getContext(), doctorName + " is successfully added in your bookmark.", Toast.LENGTH_SHORT).show();
                } else {
                    //addToBookmarks.setImageResource(R.mipmap.bookmarks);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "false");
                    Toast.makeText(getContext(), doctorName + " is successfully removed from your bookmark.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }

    /**
     * user bookmatk list
     */
    class UserBookmarkList extends AsyncTask<String, Void, String> {
        String URL = webServiceUrl + "bookmark/bookmarkList";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("page", String.valueOf(totalRequestNo)));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.v("Bookmark Doctor: ", result);
            new RenderUserBookmarksResult(result);
            isAsynchOn = true;
            if (totalRequestNo <= 1) {
                hideProgrssBar();
            } else {
                hideProgrssBar();
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            totalRequestNo++;
            Preference.setValue(getContext(), "USER_BOOKMARKS", "");
            if (totalRequestNo <= 1)
                showProgressBar();
            else {
                mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }
            //showProgressBar();
        }
    }


    @Override
    public void onResume(){
        super.onResume();
        Log.v("OnResume: ", "resume");
        searchTypeAsync = Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", "");
        //setMenuVisibility(true);
        relativeLayout.setBackgroundColor(Color.WHITE);
        String search = Preference.getValue(getContext(), "IS_SEARCH", "");
        if(search.equals("1")) {
            //Log.v("top ratef", Preference.getValue(getContext(), "TOP_RATED_FIRST", ""));
            CustomSearchHandle.resetFilterSearchPaginate(getContext());
            locationAsync = Preference.getValue(getContext(), "FILTER_LOCATION", "");
            specialityAsync = Preference.getValue(getContext(), "FILTER_SPECIALITY", "");
            genderAsync = Preference.getValue(getContext(), "FILTER_GENDER", "");
            nameAsync = Preference.getValue(getContext(), "FILTER_NAME", "");
            searchAsync = Preference.getValue(getContext(), "SEARCH_KEYWORD", "");
            lat = Preference.getValue(getContext(), "Latitude", "");
            lon = Preference.getValue(getContext(), "Longitude", "");
            Log.d("istoprest","istop"+Preference.getValue(getContext(),"TOP_RATED_FIRST","0"));
            isTopRatedFirst = Preference.getValue(getContext(), "FILTER_RATED", "0");
           //idy Preference.sharedPreferences.getString(key, value)
            isNearBy = Preference.getValue(getContext(), "FILTER_NEARBY", "0");
            if (!Preference.getValue(getContext(), "FILTER_NAME", "").equals("")) {
                searchKeyword.setText(Preference.getValue(getContext(), "FILTER_NAME", ""));
                searchAsync = Preference.getValue(getContext(), "FILTER_NAME", "");
            } else {
                searchAsync = "";
                searchKeyword.setText("");
            }

            Log.v("is top rated", "data: " + Preference.getValue(getContext(), "TOP_RATED_FIRST", ""));
            totalRequestNo = 0;
            totalResultRendered = 0;
            totalSearchResult = 0;
            //CustomSearchHandle.setGlobalSearchPaginate(getContext());

            CustomSearchHandle.setFilterSearchPaginate(getContext());
            searchFilterAsync = new SearchFilter();
            searchFilterAsync.execute();
        }
    }

    /**
     * Clear preferences
     */
    public void clearPreferences() {
        Preference.setValue(getContext(), "IS_SEARCH", "");
        Preference.setValue(getContext(), "FILTER_LOCATION", "");
        Preference.setValue(getContext(), "FILTER_SPECIALITY", "");
        Preference.setValue(getContext(), "FILTER_GENDER", "");
        Preference.setValue(getContext(), "FILTER_NAME", "");
        Preference.setValue(getContext(), "SEARCH_KEYWORD", "");
    }

    /**
     * render search result
     */
    public class RenderSearchResult {
        RenderSearchResult(String result) {
            try {

                if (!result.equals(null)) {

                    Log.v("result Render", result);
                    if (totalRequestNo <= 1)
                        linearLayout.removeAllViews();
                    linearLayout.setGravity(Gravity.START);
                    LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                    );
                    try {
                        listingOrMapLayout();
                        JSONObject jsonObject = new JSONObject(result);
                        if(jsonObject.getString("status").equals("false")) {
                            TextView textView = new TextView(getContext());
                            textView.setText(getResources().getString(R.string.no_data_found));
                            textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                            textView.setTextColor(Color.parseColor("#010101"));
                            textView.setTextSize(20);
                            textView.setLayoutParams(textLayoutParam);

                            //set counter
                            totalSearchResult = 0;
                            totalResultRendered = 0;

                            linearLayout.addView(textView);
                        } else {
                            JSONArray searchResult = jsonObject.getJSONArray("data");
                            if (totalRequestNo <= 1)
                                totalSearchResult = jsonObject.getInt("total_rows");
                            Log.v("total rows", String.valueOf(totalSearchResult));
                            for(int i = 0; i < searchResult.length(); i++) {
                                final JSONObject filterData = searchResult.getJSONObject(i);


                                if (filterData.getString("Name").equals("")) continue;

                                // 22 Dec 2:43 PM
                                if(filterData.getString("WorkType").equals("")
                                        || filterData.getString("WorkType").equals("null")){
                                        continue;
                                }

                                View view;
                                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                view = inflater.inflate(R.layout.search_result, null);

                                //speciality icons
                                LinearLayout specialityLayout = (LinearLayout) view.findViewById(R.id.speciality_mipmap);

                                String allSpecialityMipmap = filterData.getString("Mipmap");
                                String[] mipmapArray = allSpecialityMipmap.split(",");
                                int j = 1;
                                List<String> list = Arrays.asList(mipmapArray);
                                Set<String> set = new HashSet<String>(list);
                                String[] resarray= new String[set.size()];
                                set.toArray(resarray);
                                //String[] newMipmap = Arrays.stream(mipmapArray).distinct().toArray();
                                for (String specialityMipmapName : resarray) {
                                    if (j > 3)
                                        break;
                                    View specialityIcon;
                                    LayoutInflater specialityIconInflator = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    specialityIcon = specialityIconInflator.inflate(R.layout.mipmap_icon, null);
                                    ImageView mipmapSpec = (ImageView) specialityIcon.findViewById(R.id.mipmapIcon);
                                    int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName
                                            , "mipmap", getContext().getPackageName());

                                    if(mipmapId == 0){
                                        mipmapSpec.setVisibility(View.GONE);
                                    }
                                    else{
                                        mipmapSpec.setImageResource(mipmapId);
                                    }


                                    specialityLayout.addView(specialityIcon);

                                    j++;
                                }

                                //clinic name
                                TextView textView1 = (TextView) view.findViewById(R.id.search_title_work_place);
                                textView1.setText(filterData.getString("Name"));
                                textView1.setTypeface(typeface, typeface.BOLD);
                                textView1.setTextColor(Color.parseColor("#010101"));

                                //clinic address
                                TextView textView2 = (TextView) view.findViewById(R.id.search_work_place_address);
                                textView2.setText(filterData.getString("Address").toString().trim());
                                textView2.setTypeface(typeface);
                                textView2.setTextColor(Color.parseColor("#010101"));

                                //mobile
                                TextView textView3 = (TextView) view.findViewById(R.id.search_work_place_phone);
                                textView3.setText(filterData.getString("Phone"));
                                textView3.setTypeface(typeface);
                                textView3.setTextColor(Color.parseColor("#010101"));

                                //total reviews
                                TextView textView4 = (TextView) view.findViewById(R.id.search_total_reviews);
                                textView4.setText(filterData.getString("TotalReview") + " " + getResources().getString(R.string.total_reviews));
                                textView4.setTypeface(typeface);
                                textView4.setTextColor(Color.parseColor("#010101"));

                                //rating
                                RatingBar ratingBar = (RatingBar) view.findViewById(R.id.doctor_user_rating);
                                if ( ! (filterData.getString("RatingAvg").equals("null") ||
                                        filterData.getString("RatingAvg").isEmpty())) {
                                    ratingBar.setRating(Float.valueOf(filterData.getString("RatingAvg")));
                                } else {
                                    ratingBar.setRating(0.0f);
                                }

                                linearLayout.addView(view);

                                final String callDialer = filterData.getString("Phone");

                                //phone dialer
                                ImageView imageView = (ImageView) view.findViewById(R.id.search_phone_dialer);
                                //imageView.setId(imageView.getId() + i);
                                imageView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        openPhoneDialer(callDialer);
                                    }
                                });

                                //doctor details
                                //final String detailsId =  filterData.getString("WorkId");
                                final String hospitalName = filterData.getString("Name");
                                final String workType = filterData.getString("WorkType");

                                //bookmark
                                final LinearLayout withBookmark = (LinearLayout) view.findViewById(R.id.withBookmark);
                                final LinearLayout withoutBookmark = (LinearLayout) view.findViewById(R.id.withoutBookmark);
                                user_profile_pic_bookmark = (ImageView) view.findViewById(R.id.user_profile_pic_bookmark);
                                final ImageView addToBookmark = (ImageView) view.findViewById(R.id.add_to_bookmarks);
                                addToBookmark.setTag(1);
                                if (filterData.has("BookmarkStatus")) {
                                    String bookmarkStatus = filterData.getString("BookmarkStatus");
                                    if ( ! (bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                                        String bookmarkId = filterData.getString("BookmarkUserId");
                                        if (bookmarkId.equals(loginUserId) && bookmarkStatus.equals("1")) {
                                            addToBookmark.setTag(2);
                                            addToBookmark.setImageResource(R.mipmap.bookmark_2);
                                            withBookmark.setVisibility(View.VISIBLE);
                                            withoutBookmark.setVisibility(View.GONE);
                                        }
                                    }
                                    addToBookmark.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //check for Internet
                                            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                                return;
                                            }

                                            if (isGuest) {
                                                guestLogin.confirmAlert(getContext());
                                                return;
                                            }
                                            try {
                                                //Log.v("In add bookmark", "true");
                                                //Log.v("workid", filterData.getString("WorkId"));
                                                if (workType.equals("Doctor") || workType.equals("doctor")) {
                                                    if (filterData.has("WorkId"))
                                                        workidAsync =  filterData.getString("WorkId");
                                                    if (filterData.has("Id"))
                                                        workidAsync =  filterData.getString("Id");
                                                    doctorBookmarksAddUpdate = new DoctorBookmarksAddUpdate();
                                                    doctorBookmarksAddUpdate.execute();
                                                } else {
                                                    if (filterData.has("WorkId"))
                                                        workidAsync =  filterData.getString("WorkId");
                                                    if (filterData.has("Id"))
                                                        workidAsync =  filterData.getString("Id");
                                                    workTypeBookmark = new WorkTypeBookmark();
                                                    workTypeBookmark.execute();
                                                }

                                                if (Integer.parseInt(addToBookmark.getTag().toString()) == 1) {
                                                    addToBookmark.setImageResource(R.mipmap.bookmark_2);
                                                    addToBookmark.setTag(2);
                                                    withBookmark.setVisibility(View.VISIBLE);
                                                    withoutBookmark.setVisibility(View.GONE);
                                                } else {
                                                    addToBookmark.setImageResource(R.mipmap.bookmarks);
                                                    addToBookmark.setTag(1);
                                                    withBookmark.setVisibility(View.GONE);
                                                    withoutBookmark.setVisibility(View.VISIBLE);
                                                }

                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }

                                        }
                                    });
                                }


                                //lat and long for map location
                                if(!filterData.getString("Latitude").equals("null") &&
                                        !filterData.getString("Latitude").isEmpty() &&
                                        !filterData.getString("Longitude").equals("null") &&
                                        !filterData.getString("Longitude").isEmpty()) {
                                    latitudeList.add(Double.valueOf(filterData.getString("Latitude")));
                                    longitudeList.add(Double.valueOf(filterData.getString("Longitude")));
                                    titles.add(filterData.getString("Name"));
                                    if (workType.equals("Doctor") ||
                                            workType.equals("doctor")) {
                                        mapTypes.add("doctor");
                                    } else if (workType.equals("Lab") || workType.equals("Radiology Lab") || workType.equals("Medical Lab")) {
                                        mapTypes.add("lab");
                                    } else if (workType.equals("Clinic")) {
                                        mapTypes.add("clinic");
                                    } else if (workType.equals("Hospital")) {
                                        mapTypes.add("hospital");
                                    }
                                }

                                //profile pic
                                ImageView profilePic = (ImageView) view.findViewById(R.id.user_profile_pic);
                                ImageView user_profile_pic_bookmark = (ImageView) view.findViewById(R.id.user_profile_pic_bookmark);
                                if(workType.equals("Lab") || workType.equals("Radiology Lab") || workType.equals("Medical Lab")) {
                                    LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                                    ImageView viewProfile = (ImageView) view.findViewById(R.id.share_details);
                                    final String LABMIPMAP = filterData.getString("Mipmap");
                                    if (filterData.has("Speciality")) {
                                        labSpeciality = filterData.getString("Speciality");
                                    } else {
                                        labSpeciality = "";
                                    }
                                    //showDetails.setId(getId() + i);
                                    showDetails.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //check for Internet
                                            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                                return;
                                            }
                                            try {
                                                if (filterData.has("WorkId"))
                                                    workidAsync =  filterData.getString("WorkId");
                                                if (filterData.has("Id"))
                                                    workidAsync =  filterData.getString("Id");
                                                labProfile(workidAsync, LABMIPMAP, labSpeciality);
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }

                                        }
                                    });
                                    viewProfile.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //check for Internet
                                            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                                return;
                                            }
                                            try {
                                                if (filterData.has("WorkId"))
                                                    workidAsync =  filterData.getString("WorkId");
                                                if (filterData.has("Id"))
                                                    workidAsync =  filterData.getString("Id");
                                                labProfile(workidAsync, LABMIPMAP, labSpeciality);
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                    });
                                    profilePic.setImageResource(R.mipmap.lab_dark);
                                    user_profile_pic_bookmark.setImageResource(R.mipmap.lab_dark);
                                } else if(workType.equals("Clinic")) {
                                    LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                                    ImageView viewProfile = (ImageView) view.findViewById(R.id.share_details);
                                    final String CLINICMIPMAP = filterData.getString("Mipmap");
                                    if (filterData.has("Speciality")) {
                                        clinicSpeciality = filterData.getString("Speciality");
                                    } else {
                                        clinicSpeciality = "";
                                    }
                                    //showDetails.setId(getId() + i);
                                    showDetails.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //check for Internet
                                            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                                return;
                                            }
                                            Log.v("Type after click", workType);
                                            try {
                                                if (filterData.has("WorkId"))
                                                    workidAsync =  filterData.getString("WorkId");
                                                if (filterData.has("Id"))
                                                    workidAsync =  filterData.getString("Id");
                                                clinicProfile(workidAsync, CLINICMIPMAP, clinicSpeciality);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    viewProfile.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //check for Internet
                                            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                                return;
                                            }
                                            try {
                                                if (filterData.has("WorkId"))
                                                    workidAsync =  filterData.getString("WorkId");
                                                if (filterData.has("Id"))
                                                    workidAsync =  filterData.getString("Id");
                                                clinicProfile(workidAsync, CLINICMIPMAP, clinicSpeciality);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    profilePic.setImageResource(R.mipmap.clinic_dark);
                                    user_profile_pic_bookmark.setImageResource(R.mipmap.clinic_dark);
                                } else if(workType.equals("Hospital")) {
                                    LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                                    ImageView viewProfile = (ImageView) view.findViewById(R.id.share_details);
                                    //showDetails.setId(getId() + i);
                                    showDetails.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //check for Internet
                                            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                                return;
                                            }
                                            try {
                                                if (filterData.has("WorkId"))
                                                    workidAsync =  filterData.getString("WorkId");
                                                if (filterData.has("Id"))
                                                    workidAsync =  filterData.getString("Id");
                                                hospitalProfile(workidAsync, hospitalName);
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                    });
                                    viewProfile.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //check for Internet
                                            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                                return;
                                            }
                                            try {
                                                if (filterData.has("WorkId"))
                                                    workidAsync =  filterData.getString("WorkId");
                                                if (filterData.has("Id"))
                                                    workidAsync =  filterData.getString("Id");
                                                hospitalProfile(workidAsync, hospitalName);
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                    });
                                    profilePic.setImageResource(R.mipmap.hospital_dark);
                                    user_profile_pic_bookmark.setImageResource(R.mipmap.hospital_dark);
                                } else  if (workType.equals("Doctor") || workType.equals("doctor")) {
                                    LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                                    ImageView viewProfile = (ImageView) view.findViewById(R.id.share_details);
                                    //showDetails.setId(getId() + i);
                                    showDetails.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //check for Internet
                                            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                                return;
                                            }
                                            try {
                                                if (filterData.has("WorkId"))
                                                    workidAsync =  filterData.getString("WorkId");
                                                if (filterData.has("Id"))
                                                    workidAsync =  filterData.getString("Id");
                                                doctorProfile(workidAsync, hospitalName);
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                    });
                                    viewProfile.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //check for Internet
                                            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                                return;
                                            }
                                            try {
                                                if (filterData.has("WorkId"))
                                                    workidAsync =  filterData.getString("WorkId");
                                                if (filterData.has("Id"))
                                                    workidAsync =  filterData.getString("Id");
                                                doctorProfile(workidAsync, hospitalName);
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                    });
                                    profilePic.setImageResource(R.mipmap.most_visited_doctor);
                                    user_profile_pic_bookmark.setImageResource(R.mipmap.most_visited_doctor);
                                } else {
                                    profilePic.setImageResource(R.mipmap.most_visited_doctor);
                                }
                            }
                            Log.v("LatandLong", latitudeList.toString() + ", " + longitudeList.toString());
                            totalResultRendered += searchResult.length();
                            Log.v("total rendered", String.valueOf(totalResultRendered));
                            //Log.v("Lat & Long: ", latitudeList.toString() + longitudeList.toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    searchKeyword.setVisibility(View.VISIBLE);
                    mapLocation.setVisibility(View.VISIBLE);
                    topTenDoctorText.setVisibility(View.GONE);
                }

            }
            catch (NullPointerException e){
                Log.d("Search Result",""+e.getMessage());
            }
            catch (Exception e){
                Log.d("Search Result",""+e.getMessage());
            }

        }

    }

    /**
     * render hospital search result
     */
    public class RenderHospitelSearchResult {
        RenderHospitelSearchResult(String result) {
            //hideFilterIcon();
            if (totalRequestNo <= 1)
                linearLayout.removeAllViews();
            linearLayout.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            );
            try {
                listingOrMapLayout();
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.no_hospital_found));
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    //set counter
                    totalSearchResult = 0;
                    totalResultRendered = 0;

                    linearLayout.addView(textView);
                } else {
                    JSONArray searchResult = jsonObject.getJSONArray("data");
                    if (totalRequestNo <= 1)
                        totalSearchResult = jsonObject.getInt("total_rows");
                    Log.v("total rows", String.valueOf(totalSearchResult));
                    for(int i = 0; i < searchResult.length(); i++) {
                        final JSONObject filterData = searchResult.getJSONObject(i);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.search_result, null);

                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.search_title_work_place);
                        textView1.setText(filterData.getString("Name"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.search_work_place_address);
                        textView2.setText(filterData.getString("Address").toString().trim());
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.search_work_place_phone);
                        textView3.setText(filterData.getString("Phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        //total reviews
                        TextView textView4 = (TextView) view.findViewById(R.id.search_total_reviews);
                        textView4.setText(filterData.getString("TotalReview") + " " + getResources().getString(R.string.reviews));
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));

                        //rating
                        RatingBar doctorUserRating = (RatingBar) view.findViewById(R.id.doctor_user_rating);
                        if ( ! (filterData.getString("RatingAvg").equals("null") ||
                                filterData.getString("RatingAvg").isEmpty())) {
                            doctorUserRating.setRating(Float.valueOf(filterData.getString("RatingAvg")));
                        } else {
                            doctorUserRating.setRating(0.0f);
                        }

                        //profile image
                        ImageView profileImage = (ImageView) view.findViewById(R.id.user_profile_pic);
                        profileImage.setImageResource(R.mipmap.hospital_dark);

                        linearLayout.addView(view);

                        final String callDialer = filterData.getString("Phone");
                        final String hospitalName = filterData.getString("Name");
                        final String hospitalAddress = filterData.getString("Address");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.search_phone_dialer);
                        //imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });

                        //doctor details
                        final String detailsId =  filterData.getString("HospitalId");

                        LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                        //showDetails.setId(getId() + i);
                        showDetails.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                Bundle args = new Bundle();
                                args.putString("id", detailsId);

                                FragmentManager mFragmentManager;
                                mFragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction mFragmentTransaction;
                                mFragmentTransaction = mFragmentManager.beginTransaction();

                                HospitalProfileFragment hospitalProfileFragment = new HospitalProfileFragment();
                                hospitalProfileFragment.setArguments(args);

                                mFragmentTransaction.addToBackStack("hospitalProfile");
                                mFragmentTransaction.replace(R.id.search_layout, hospitalProfileFragment).commit();
                            }
                        });

                        if (filterData.has("Latitude") && filterData.has("Longitude")) {
                            if(!filterData.getString("Latitude").equals("null") &&
                                    !filterData.getString("Latitude").isEmpty() &&
                                    !filterData.getString("Longitude").equals("null") &&
                                    !filterData.getString("Longitude").isEmpty()) {
                                latitudeList.add(Double.valueOf(filterData.getString("Latitude")));
                                longitudeList.add(Double.valueOf(filterData.getString("Longitude")));
                                titles.add(filterData.getString("Name"));
                                mapTypes.add("hospital");
                            }
                        }

                        //bookmarks
                        final LinearLayout withBookmark = (LinearLayout) view.findViewById(R.id.withBookmark);
                        final LinearLayout withoutBookmark = (LinearLayout) view.findViewById(R.id.withoutBookmark);
                        user_profile_pic_bookmark = (ImageView) view.findViewById(R.id.user_profile_pic_bookmark);
                        user_profile_pic_bookmark.setImageResource(R.mipmap.hospital_dark);
                        final ImageView addToBookmarks = (ImageView) view.findViewById(R.id.add_to_bookmarks);
                        addToBookmarks.setTag(1);
                        String bookmarkStatus = filterData.getString("BookmarkStatus");
                        if ( ! (bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                            String bookmarkUserId = filterData.getString("BookmarkUserId");
                            if (bookmarkUserId.equals(loginUserId) && bookmarkStatus.equals("1")) {
                                addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                addToBookmarks.setTag(2);
                                withBookmark.setVisibility(View.VISIBLE);
                                withoutBookmark.setVisibility(View.GONE);
                            }
                        }

                        addToBookmarks.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                if (isGuest) {
                                    guestLogin.confirmAlert(getContext());
                                    return;
                                }
                                workidAsync = detailsId;
                                workTypeBookmark = new WorkTypeBookmark();
                                workTypeBookmark.execute();
                                if (Integer.parseInt(addToBookmarks.getTag().toString()) == 1) {
                                    addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                    addToBookmarks.setTag(2);
                                    withBookmark.setVisibility(View.VISIBLE);
                                    withoutBookmark.setVisibility(View.GONE);
                                } else {
                                    addToBookmarks.setImageResource(R.mipmap.bookmarks);
                                    addToBookmarks.setTag(1);
                                    withBookmark.setVisibility(View.GONE);
                                    withoutBookmark.setVisibility(View.VISIBLE);
                                }
                            }
                        });

                        //share on social
                        ImageView socialShare = (ImageView) view.findViewById(R.id.share_details);
                        socialShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                hospitalProfile(detailsId, hospitalName);
                                //shareIt(hospitalName, callDialer, hospitalAddress, "");
                            }
                        });

                        //specialites
                       // LinearLayout specialityLayout = (LinearLayout) view.findViewById(R.id.speciality_mipmap);
                       // LinearLayout linearMainCell = (LinearLayout) view.findViewById(R.id.show_details);

                         specialityLayout = (LinearLayout) view.findViewById(R.id.speciality_mipmap);
                         linearMainCell = (LinearLayout) view.findViewById(R.id.show_details);

                        String allSpecialityMipmap = filterData.getString("Mipmap");
                        String[] mipmapArray = allSpecialityMipmap.split(",");
                        List<String> list = Arrays.asList(mipmapArray);
                        Set<String> set = new HashSet<String>(list);
                       // String[] resarray= new String[set.size()];
                        resarray= new String[set.size()];
                        set.toArray(resarray);
                       // int j=1;

                        parent = (LinearLayout) view.findViewById(R.id.speciality_mipmap);

//                        Toast.makeText(getContext(),""+getResources().getDisplayMetrics().densityDpi,Toast.LENGTH_SHORT).show();
                        switch (getResources().getDisplayMetrics().densityDpi) {

                            case DisplayMetrics.DENSITY_280:
                                lowDensityLayout();
                                break;

                            case DisplayMetrics.DENSITY_360:
                                lowDensityLayout();
                                break;

                            case DisplayMetrics.DENSITY_400:
                                lowDensityLayout();
                                break;

                            case DisplayMetrics.DENSITY_420:
                                lowDensityLayout();
                                break;


                            case DisplayMetrics.DENSITY_560:
                                lowDensityLayout();
                                break;

                            case DisplayMetrics.DENSITY_LOW:
                                lowDensityLayout();
                                break;
                            case DisplayMetrics.DENSITY_MEDIUM:
                                lowDensityLayout();
                                break;
                            case DisplayMetrics.DENSITY_HIGH:
                                lowDensityLayout();
                                break;
                            case DisplayMetrics.DENSITY_XHIGH:{
                                    lowDensityLayout();
                                    //highDensityLayout();
                                }

                                break;
                            case DisplayMetrics.DENSITY_XXHIGH:{
                                    //highDensityLayout();
                                    lowDensityLayout();
                                 }

                                break;
                            case DisplayMetrics.DENSITY_XXXHIGH:{
                                    //highDensityLayout();
                                    lowDensityLayout();
                                }
                                break;

                        }


                        /*for (String specialityMipmapName : resarray) {

                            if(j > 3)
                            {
                                LinearLayout parent = (LinearLayout) view.findViewById(R.id.speciality_mipmap);


                                RelativeLayout relativeLayout = new RelativeLayout(getContext());
                                RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                relativeParams.addRule(RelativeLayout.BELOW, parent.getId());


                                LinearLayout child = new LinearLayout(getContext());
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                                child.setOrientation(LinearLayout.HORIZONTAL);
                                child.setLayoutParams(layoutParams);


                                View mipmapIcon;
                                LayoutInflater mipmapIconInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                mipmapIcon = mipmapIconInflater.inflate(R.layout.mipmap_icon, null);
                                ImageView mipmapSpec = (ImageView) mipmapIcon.findViewById(R.id.mipmapIcon);

                                int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName
                                        , "mipmap", getContext().getPackageName());
                                mipmapSpec.setImageResource(mipmapId);


                                child.addView(mipmapIcon);


                                relativeLayout.addView(child,relativeParams);


                                linearMainCell.addView(relativeLayout);


                               break;
                            }

                            View mipmapIcon;
                            LayoutInflater mipmapIconInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            mipmapIcon = mipmapIconInflater.inflate(R.layout.mipmap_icon, null);
                            ImageView mipmapSpec = (ImageView) mipmapIcon.findViewById(R.id.mipmapIcon);

                            int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName
                                    , "mipmap", getContext().getPackageName());
                            //ImageView spec = new ImageView(getContext());
                            mipmapSpec.setImageResource(mipmapId);
                            //int dp = (int) (getContext().getResources().getDimension(R.dimen.doctor_speciality_icon_width_height) / getContext().getResources().getDisplayMetrics().density);

                            specialityLayout.addView(mipmapIcon);


                            j++;
                        }*/


                       // Only 6 icons are shows.

                        /*
                        if(resarray.length > 3)
                        {
                            LinearLayout parent = (LinearLayout) view.findViewById(R.id.speciality_mipmap);

                            RelativeLayout relativeLayout = new RelativeLayout(getContext());
                            RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            relativeParams.addRule(RelativeLayout.BELOW, parent.getId());


                            LinearLayout child = new LinearLayout(getContext());
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                            child.setOrientation(LinearLayout.HORIZONTAL);
                            child.setLayoutParams(layoutParams);



                            int val = 1;


                            for (String specialityMipmapName : resarray) {


                                if (j > 3) {
                                    if (val <= 3) {

                                        View mipmapIcon;
                                        LayoutInflater mipmapIconInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        mipmapIcon = mipmapIconInflater.inflate(R.layout.mipmap_icon, null);
                                        ImageView mipmapSpec = (ImageView) mipmapIcon.findViewById(R.id.mipmapIcon);

                                        int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName
                                                , "mipmap", getContext().getPackageName());
                                        //ImageView spec = new ImageView(getContext());
                                        mipmapSpec.setImageResource(mipmapId);
                                        //int dp = (int) (getContext().getResources().getDimension(R.dimen.doctor_speciality_icon_width_height) / getContext().getResources().getDisplayMetrics().density);


                                        //if(val == 3)
                                        //{
                                        specialityLayout.addView(mipmapIcon);
                                        //}


                                        val++;
                                    }

                                } else {


                                    View mipmapIcon;
                                    LayoutInflater mipmapIconInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    mipmapIcon = mipmapIconInflater.inflate(R.layout.mipmap_icon, null);
                                    ImageView mipmapSpec = (ImageView) mipmapIcon.findViewById(R.id.mipmapIcon);

                                    int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName
                                            , "mipmap", getContext().getPackageName());
                                    mipmapSpec.setImageResource(mipmapId);


                                    child.addView(mipmapIcon);


                                   //if (j % 3 == 0) {

                                        relativeLayout.removeView(child);
                                        linearMainCell.removeView(relativeLayout);

                                        relativeLayout.addView(child, relativeParams);
                                        linearMainCell.addView(relativeLayout);
//                                   }


                                }

                                j++;
                            }
                        }
                        else
                        {
                            for (String specialityMipmapName : resarray) {

                                View mipmapIcon;
                                LayoutInflater mipmapIconInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                mipmapIcon = mipmapIconInflater.inflate(R.layout.mipmap_icon, null);
                                ImageView mipmapSpec = (ImageView) mipmapIcon.findViewById(R.id.mipmapIcon);

                                int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName
                                        , "mipmap", getContext().getPackageName());
                                //ImageView spec = new ImageView(getContext());
                                mipmapSpec.setImageResource(mipmapId);
                                //int dp = (int) (getContext().getResources().getDimension(R.dimen.doctor_speciality_icon_width_height) / getContext().getResources().getDisplayMetrics().density);

                                specialityLayout.addView(mipmapIcon);
                            }

                        }
                        */

                    }
                    totalResultRendered += searchResult.length();
                    Log.v("total rendered", String.valueOf(totalResultRendered));
                    Log.v("Lat & Long: ", latitudeList.toString() + longitudeList.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            searchKeyword.setVisibility(View.VISIBLE);
            mapLocation.setVisibility(View.VISIBLE);
            topTenDoctorText.setVisibility(View.GONE);
        }
    }


    public void highDensityLayout()
    {
        int j=1;

        if(resarray.length > 3)
        {

            RelativeLayout relativeLayout = new RelativeLayout(getContext());
            RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            relativeParams.addRule(RelativeLayout.BELOW, parent.getId());


            LinearLayout child = new LinearLayout(getContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            child.setOrientation(LinearLayout.HORIZONTAL);
            child.setLayoutParams(layoutParams);


            int val = 1;


            for (String specialityMipmapName : resarray) {


                if (j > 3) {

                    // in second row it shows only one image --> Total images are 4
                    if (val <= 1) {

                        View mipmapIcon;
                        LayoutInflater mipmapIconInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        mipmapIcon = mipmapIconInflater.inflate(R.layout.mipmap_icon, null);
                        ImageView mipmapSpec = (ImageView) mipmapIcon.findViewById(R.id.mipmapIcon);

                        int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName
                                , "mipmap", getContext().getPackageName());
                        //ImageView spec = new ImageView(getContext());
                        mipmapSpec.setImageResource(mipmapId);
                        //int dp = (int) (getContext().getResources().getDimension(R.dimen.doctor_speciality_icon_width_height) / getContext().getResources().getDisplayMetrics().density);


                        //if(val == 3)
                        //{
                        specialityLayout.addView(mipmapIcon);
                        //}


                        val++;
                    }

                } else {


                    View mipmapIcon;
                    LayoutInflater mipmapIconInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    mipmapIcon = mipmapIconInflater.inflate(R.layout.mipmap_icon, null);
                    ImageView mipmapSpec = (ImageView) mipmapIcon.findViewById(R.id.mipmapIcon);

                    int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName
                            , "mipmap", getContext().getPackageName());
                    mipmapSpec.setImageResource(mipmapId);


                    child.addView(mipmapIcon);


                    //if (j % 3 == 0) {

                    relativeLayout.removeView(child);
                    linearMainCell.removeView(relativeLayout);

                    relativeLayout.addView(child, relativeParams);
                    linearMainCell.addView(relativeLayout);
//                                   }

                }

                j++;
            }
        }
        else
        {
            lowDensityLayout();
        }


    }

    /*public void lowDensityLayout()
    {

     int j=1;
     for (String specialityMipmapName : resarray) {

         if(j > 4)
            break;

            View mipmapIcon;
            LayoutInflater mipmapIconInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mipmapIcon = mipmapIconInflater.inflate(R.layout.mipmap_icon, null);
            ImageView mipmapSpec = (ImageView) mipmapIcon.findViewById(R.id.mipmapIcon);

            int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName
                    , "mipmap", getContext().getPackageName());
            //ImageView spec = new ImageView(getContext());

            if(mipmapId == 0)
                mipmapSpec.setVisibility(View.GONE);
            else
                mipmapSpec.setImageResource(mipmapId);
            //int dp = (int) (getContext().getResources().getDimension(R.dimen.doctor_speciality_icon_width_height) / getContext().getResources().getDisplayMetrics().density);

            specialityLayout.addView(mipmapIcon);
         j++;
        }

    }*/
    public void lowDensityLayout()
    {

        int j=1;
        for (String specialityMipmapName : resarray) {

            if(j > 3)
                break;

            View mipmapIcon;
            LayoutInflater mipmapIconInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            mipmapIcon = mipmapIconInflater.inflate(R.layout.mipmap_icon, null);

            ImageView mipmapSpec = (ImageView) mipmapIcon.findViewById(R.id.mipmapIcon);

//         Toast.makeText(getContext(),"Dpi: "+getResources().getDisplayMetrics().densityDpi,Toast.LENGTH_SHORT).show();

            switch (getResources().getDisplayMetrics().densityDpi) {


                /**
                 * https://en.wikipedia.org/wiki/Pixel_density
                 * Named pixel densities
                 * DENSITY_280 - xhdpi
                 * DENSITY_360 - xxhdpi
                 * DENSITY_400 - xxhdpi
                 * DENSITY_420 - xxhdpi
                 * DENSITY_560 - xxxhdpi
                 */

                case DisplayMetrics.DENSITY_280:{
                    mipmapSpec.getLayoutParams().height=55;
                    mipmapSpec.getLayoutParams().width=60;
                }
                break;


                case DisplayMetrics.DENSITY_360:{
                    mipmapSpec.getLayoutParams().height=90;
                    mipmapSpec.getLayoutParams().width=90;
                }
                break;

                case DisplayMetrics.DENSITY_400:{
                    mipmapSpec.getLayoutParams().height=90;
                    mipmapSpec.getLayoutParams().width=90;
                }
                break;

                case DisplayMetrics.DENSITY_420:{
                    mipmapSpec.getLayoutParams().height=90;
                    mipmapSpec.getLayoutParams().width=90;
                }
                break;


                case DisplayMetrics.DENSITY_560:{
                    mipmapSpec.getLayoutParams().height=120;
                    mipmapSpec.getLayoutParams().width=120;
                }
                break;

                case DisplayMetrics.DENSITY_LOW:{
                    mipmapSpec.getLayoutParams().height=20;
                    mipmapSpec.getLayoutParams().width=20;
                }
                break;

                case DisplayMetrics.DENSITY_MEDIUM:{
                    mipmapSpec.getLayoutParams().height=30;
                    mipmapSpec.getLayoutParams().width=30;
                }
                break;

                case DisplayMetrics.DENSITY_HIGH:{
                    mipmapSpec.getLayoutParams().height=30;
                    mipmapSpec.getLayoutParams().width=30;
                }

                break;

                case DisplayMetrics.DENSITY_XHIGH:{
                    mipmapSpec.getLayoutParams().height=55;
                    mipmapSpec.getLayoutParams().width=55;
                }

                break;

                case DisplayMetrics.DENSITY_XXHIGH:{
                    mipmapSpec.getLayoutParams().height=70;
                    mipmapSpec.getLayoutParams().width=70;
                }

                break;

                case DisplayMetrics.DENSITY_XXXHIGH:{
                    mipmapSpec.getLayoutParams().height=120;
                    mipmapSpec.getLayoutParams().width=120;
                }
                break;

            }

            mipmapSpec.setScaleType(ImageView.ScaleType.FIT_XY);

            int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName
                    , "mipmap", getContext().getPackageName());
            //ImageView spec = new ImageView(getContext());

//            mipmapIcon = resizeView(mipmapIcon);

            if(mipmapId == 0)
                mipmapSpec.setVisibility(View.GONE);
            else
                mipmapSpec.setImageResource(mipmapId);
//         mipmapSpec.setImageResource(R.mipmap.face24);

            //int dp = (int) (getContext().getResources().getDimension(R.dimen.doctor_speciality_icon_width_height) / getContext().getResources().getDisplayMetrics().density);

            specialityLayout.addView(mipmapIcon);
            j++;
        }

    }

    /**
     * Render clinic search results
     */
    public class RenderClinicSearchResult {
        RenderClinicSearchResult(String result) {
            if (totalRequestNo <= 1)
                linearLayout.removeAllViews();
            linearLayout.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            );
            try {
                Log.v("Clinic", result);
                listingOrMapLayout();
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.no_clinic_found));
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    //set counter
                    totalSearchResult = 0;
                    totalResultRendered = 0;

                    linearLayout.addView(textView);
                } else {
                    JSONArray searchResult = jsonObject.getJSONArray("data");
                    if (totalRequestNo <= 1)
                        totalSearchResult = jsonObject.getInt("total_rows");
                    Log.v("total rows", String.valueOf(totalSearchResult));
                    for(int i = 0; i < searchResult.length(); i++) {
                        final JSONObject filterData = searchResult.getJSONObject(i);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.search_result, null);

                        //profile pic
                        ImageView profilePic = (ImageView) view.findViewById(R.id.user_profile_pic);
                        profilePic.setImageResource(R.mipmap.clinic_dark);

                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.search_title_work_place);
                        textView1.setText(filterData.getString("Name"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(getResources().getColor(R.color.black));

                        //total review
                        TextView totalReview = (TextView) view.findViewById(R.id.search_total_reviews);
                        totalReview.setText(filterData.getString("TotalReview") + " " + getResources().getString(R.string.reviews));
                        totalReview.setTypeface(typeface);
                        totalReview.setTextColor(getResources().getColor(R.color.black));

                        //avg rating
                        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.doctor_user_rating);
                        if ( ! (filterData.getString("RatingAvg").equals("null") ||
                                filterData.getString("RatingAvg").isEmpty()))
                            ratingBar.setRating(Float.valueOf(filterData.getString("RatingAvg")));
                        else
                            ratingBar.setRating(0.0f);

                        //speciality


                        /*String allSpecialityMipmap = filterData.getString("Mipmap");
                        String[] mipmapArray = allSpecialityMipmap.split(",");
                        List<String> list = Arrays.asList(mipmapArray);
                        Set<String> set = new HashSet<String>(list);
                        // String[] resarray= new String[set.size()];
                        resarray= new String[set.size()];
                        set.toArray(resarray);

                        lowDensityLayout();*/

                        /*int specialityId = getContext().getResources().getIdentifier(filterData.getString("Mipmap"),
                                "mipmap",
                                getContext().getPackageName());
                        ImageView specialityImage = (ImageView) view.findViewById(R.id.speciality);
                        specialityImage.setImageResource(specialityId);*/

                        specialityLayout = (LinearLayout) view.findViewById(R.id.speciality_mipmap);
                        String allSpecialityMipmap = filterData.getString("Mipmap");
                        String[] mipmapArray = allSpecialityMipmap.split(",");
                        List<String> list = Arrays.asList(mipmapArray);
                        Set<String> set = new HashSet<String>(list);
                        resarray= new String[set.size()];
                        set.toArray(resarray);
                        parent = (LinearLayout) view.findViewById(R.id.speciality_mipmap);

                        lowDensityLayout();




                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.search_work_place_address);
                        textView2.setText(filterData.getString("Address").toString().trim());
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(getResources().getColor(R.color.black));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.search_work_place_phone);
                        textView3.setText(filterData.getString("Phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(getResources().getColor(R.color.black));

                        linearLayout.addView(view);

                        final String callDialer = filterData.getString("Phone");
                        final String clinicAddress = filterData.getString("Address");
                        final String clinicName = filterData.getString("Name");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.search_phone_dialer);
                        //imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });

                        //doctor details
                        final String clinicId =  filterData.getString("ClinicId");
                        final String mipmap = filterData.getString("Mipmap");
                        final String speciality = filterData.getString("Speciality");

                        LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                        //showDetails.setId(getId() + i);
                        showDetails.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                clinicProfile(clinicId, mipmap, speciality);
                            }
                        });

                        if(!filterData.getString("Latitude").equals("null") &&
                                !filterData.getString("Latitude").isEmpty() &&
                                !filterData.getString("Longitude").equals("null") &&
                                !filterData.getString("Longitude").isEmpty()) {
                            latitudeList.add(Double.valueOf(filterData.getString("Latitude")));
                            longitudeList.add(Double.valueOf(filterData.getString("Longitude")));
                            titles.add(filterData.getString("Name"));
                            mapTypes.add("clinic");
                        }

                        //share on social
                        ImageView socialShare = (ImageView) view.findViewById(R.id.share_details);
                        socialShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                Log.v("name", clinicName);
                                //shareIt(clinicName, callDialer, clinicAddress, "");
                                clinicProfile(clinicId, mipmap, speciality);
                            }
                        });

                        //bookmarks
                        final LinearLayout withBookmark = (LinearLayout) view.findViewById(R.id.withBookmark);
                        final LinearLayout withoutBookmark = (LinearLayout) view.findViewById(R.id.withoutBookmark);
                        user_profile_pic_bookmark = (ImageView) view.findViewById(R.id.user_profile_pic_bookmark);
                        user_profile_pic_bookmark.setImageResource(R.mipmap.clinic_dark);
                        final ImageView addToBookmarks = (ImageView) view.findViewById(R.id.add_to_bookmarks);
                        addToBookmarks.setTag(1);
                        String bookmarkStatus = filterData.getString("BookmarkStatus");
                        if ( ! (bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                            String bookmarkUserId = filterData.getString("BookmarkUserId");
                            String bookmarkUserStatus = filterData.getString("BookmarkStatus");
                            if (bookmarkUserId.equals(loginUserId) && bookmarkStatus.equals("1")) {
                                addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                addToBookmarks.setTag(2);
                                withBookmark.setVisibility(View.VISIBLE);
                                withoutBookmark.setVisibility(View.GONE);
                            }
                        }

                        addToBookmarks.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                if (isGuest) {
                                    guestLogin.confirmAlert(getContext());
                                    return;
                                }
                                workidAsync = clinicId;
                                workTypeBookmark = new WorkTypeBookmark();
                                workTypeBookmark.execute();
                                //workTypeBookmark(loginUserId, clinicId);
                                if (Integer.parseInt(addToBookmarks.getTag().toString()) == 1) {
                                    addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                    addToBookmarks.setTag(2);
                                    withBookmark.setVisibility(View.VISIBLE);
                                    withoutBookmark.setVisibility(View.GONE);
                                } else {
                                    addToBookmarks.setImageResource(R.mipmap.bookmarks);
                                    addToBookmarks.setTag(1);
                                    withBookmark.setVisibility(View.GONE);
                                    withoutBookmark.setVisibility(View.VISIBLE);
                                }
                            }
                        });

                    }
                    totalResultRendered += searchResult.length();
                    Log.v("total rendered", String.valueOf(totalResultRendered));
                    //Log.v("Lat & Long: ", latitudeList.toString() + longitudeList.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
            searchKeyword.setVisibility(View.VISIBLE);
            mapLocation.setVisibility(View.VISIBLE);
            topTenDoctorText.setVisibility(View.GONE);
        }
    }

    /**
     * Render doctor search result
     */
    public class RenderDoctorSearchResult {
        RenderDoctorSearchResult(String result) {
            //Log.v("Inside render", result);
            hideFilterIcon();
            //remove all result at first request
            if (totalRequestNo <= 1)
                linearLayout.removeAllViews();
            //Log.v("request", String.valueOf(totalRequestNo));
            //totalRequestNo++;
            linearLayout.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            );
            try {
                listingOrMapLayout();
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.no_doctor_found));
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTypeface(typeface);
                    textView.setTextColor(getResources().getColor(R.color.black));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    //set counter
                    totalSearchResult = 0;
                    totalResultRendered = 0;

                    linearLayout.addView(textView);
                } else {
                    JSONArray searchResult = jsonObject.getJSONArray("data");
                    if (totalRequestNo <= 1)
                        totalSearchResult = jsonObject.getInt("total_rows");
                    Log.v("total rows", String.valueOf(totalSearchResult));
                    for(int i = 0; i < searchResult.length(); i++) {
                        final JSONObject filterData = searchResult.getJSONObject(i);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.search_result, null);

                        //speciality icon
                        ImageView doctorSpecialityIcon = (ImageView) view.findViewById(R.id.speciality);
                        if (!filterData.getString("Mipmap").equals("null")) {
                            int id = getContext().getResources().getIdentifier(
                                    specialityIcon.get(getCategoryPos(filterData.getString("Mipmap"))).toString(),
                                    "mipmap", getContext().getPackageName());
                            doctorSpecialityIcon.setImageResource(id);
                        }


                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.search_title_work_place);
                        textView1.setText(filterData.getString("Name"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.search_work_place_address);
                        textView2.setText(filterData.getString("Address").toString().trim());
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.search_work_place_phone);
                        textView3.setText(filterData.getString("Phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        //total reviews
                        TextView textView4 = (TextView) view.findViewById(R.id.search_total_reviews);
                        if(!filterData.getString("TotalReview").equals("null") &&
                                !filterData.getString("TotalReview").isEmpty())
                            textView4.setText(filterData.getString("TotalReview") + " " + getResources().getString(R.string.total_reviews));
                        else
                            textView4.setText(0 + " " + getResources().getString(R.string.total_reviews));
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));

                        //rating
                        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.doctor_user_rating);
                        if(!filterData.getString("RatingAvg").equals("null") &&
                                !filterData.getString("RatingAvg").isEmpty())
                            ratingBar.setRating(Float.valueOf(filterData.getString("RatingAvg")));

                        linearLayout.addView(view);

                        final String callDialer = filterData.getString("Phone");
                        callDialerAsync = filterData.getString("Phone");
                        doctorAddressAsync = filterData.getString("Address");
                        doctorRatingAsync = String.valueOf(filterData.getString("RatingAvg"));

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.search_phone_dialer);
                        //imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });

                        //doctor details
                        String doctorFilterId = "";
                        if (filterData.has("DoctorId")) {
                            doctorFilterId = filterData.getString("DoctorId");
                        }
                        if (filterData.has("Id")) {
                            doctorFilterId = filterData.getString("Id");
                        }
                        final String doctorId =  doctorFilterId;
                        final String doctorName =  filterData.getString("Name");
                        LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                        //showDetails.setId(getId() + i);
                        showDetails.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                doctorProfile(doctorId, doctorName);
                            }
                        });

                        if(!filterData.getString("Latitude").equals("null") &&
                                !filterData.getString("Latitude").toString().isEmpty() &&
                                !filterData.getString("Longitude").equals("null") &&
                                !filterData.getString("Longitude").toString().isEmpty()) {

                            latitudeList.add(Double.valueOf(filterData.getString("Latitude").trim()));
                            longitudeList.add(Double.valueOf(filterData.getString("Longitude").trim()));


                            //latitudeList.add(Double.valueOf(filterData.getString("Latitude")).doubleValue());
                            //longitudeList.add(Double.valueOf(filterData.getString("Longitude")).doubleValue());

                            titles.add(filterData.getString("Name"));
                            mapTypes.add("doctor");
                        }

                        //social share
                        ImageView socialShare = (ImageView) view.findViewById(R.id.share_details);
                        socialShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                //shareIt(doctorName, callDialer, doctorAddress, doctorRating);
                                doctorProfile(doctorId, doctorName);
                            }
                        });

                        //bookmark
                        final LinearLayout withBookmark = (LinearLayout) view.findViewById(R.id.withBookmark);
                        final LinearLayout withoutBookmark = (LinearLayout) view.findViewById(R.id.withoutBookmark);
                        user_profile_pic_bookmark = (ImageView) view.findViewById(R.id.user_profile_pic_bookmark);
                        user_profile_pic_bookmark.setImageResource(R.mipmap.most_visited_doctor);
                        final ImageView addToBookmarks = (ImageView) view.findViewById(R.id.add_to_bookmarks);
                        addToBookmarks.setTag(1);
                        String bookmarkStatus = filterData.getString("BookmarkStatus");
                        if ( ! (bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                            String bookmarkUserId = filterData.getString("BookmarkUserId");
                            if (bookmarkUserId.equals(loginUserId) && bookmarkStatus.equals("1")) {
                                addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                addToBookmarks.setTag(2);
                                withBookmark.setVisibility(View.VISIBLE);
                                withoutBookmark.setVisibility(View.GONE);
                            }
                        }

                        addToBookmarks.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                if (isGuest) {
                                    guestLogin.confirmAlert(getContext());
                                    return;
                                }
                                workidAsync = doctorId;
                                doctorBookmarksAddUpdate = new DoctorBookmarksAddUpdate();
                                doctorBookmarksAddUpdate.execute();
                                if (Integer.parseInt(addToBookmarks.getTag().toString()) == 1) {
                                    addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                    addToBookmarks.setTag(2);
                                    withBookmark.setVisibility(View.VISIBLE);
                                    withoutBookmark.setVisibility(View.GONE);
                                } else {
                                    addToBookmarks.setImageResource(R.mipmap.bookmarks);
                                    addToBookmarks.setTag(1);
                                    withBookmark.setVisibility(View.GONE);
                                    withoutBookmark.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }
                    totalResultRendered += searchResult.length();
                    Log.v("total rendered", String.valueOf(totalResultRendered));
                    Log.v("Lat & Long: ", latitudeList.toString() + longitudeList.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
                Toast.makeText(getContext(), getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
            }
            searchKeyword.setVisibility(View.VISIBLE);
            mapLocation.setVisibility(View.VISIBLE);
            topTenDoctorText.setVisibility(View.GONE);
        }
    }

    /**
     * doctor profile
     * @param doctorId string
     * @param doctorName string
     */
    public void doctorProfile(final String doctorId, final String doctorName) {
        Bundle args = new Bundle();
        args.putString("id", doctorId);
        Log.v("id", doctorId);
        Log.v("name", doctorName);
        args.putString("name", doctorName);
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        DoctorProfileFragment clinicProfileFragment = new DoctorProfileFragment();
        clinicProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("doctorProfile");
        mFragmentTransaction.add(R.id.search_layout, clinicProfileFragment).commit();
    }

    /**
     * clinic profile
     * @param id string
     * @param mipmap string
     * @param speciality string
     */
    public void clinicProfile(String id, String mipmap, String speciality) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("mipmap", mipmap);
        args.putString("speciality", speciality);

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        ClinicProfileFragment clinicProfileFragment = new ClinicProfileFragment();
        clinicProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("clinicProfile");
        mFragmentTransaction.replace(R.id.search_layout, clinicProfileFragment).commit();
    }

    /**
     * hospital profile
     * @param id string
     * @param name string
     */
    public void hospitalProfile(final String id, final String name) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("hospitalName", name);

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        HospitalProfileFragment doctorprofile = new HospitalProfileFragment();
        doctorprofile.setArguments(args);

        mFragmentTransaction.addToBackStack("hospitalProfile");
        mFragmentTransaction.replace(R.id.search_layout, doctorprofile).commit();
    }

    /**
     * lab profile
     * @param id string
     * @param mipmap string
     * @param speciality string
     */
    public void labProfile(String id, String mipmap, String speciality) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("mipmap", mipmap);
        args.putString("speciality", speciality);
        Log.v("profile param", args.toString());

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        LabProfileFragment labProfileFragment = new LabProfileFragment();
        labProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("labProfile");
        mFragmentTransaction.replace(R.id.search_layout, labProfileFragment).commit();
    }

    /**
     * Render lab search result
     */
    public class RenderLabSearchResult {
        RenderLabSearchResult(String result) {
            hideFilterIcon();
            if (totalRequestNo <= 1)
                linearLayout.removeAllViews();
            linearLayout.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            );
            try {
                listingOrMapLayout();
                JSONObject jsonObject = new JSONObject(result);
                Log.v("Lab Result", jsonObject.toString());
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.no_lab_found));
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    //set counter
                    totalSearchResult = 0;
                    totalResultRendered = 0;

                    linearLayout.addView(textView);
                } else {
                    JSONArray searchResult = jsonObject.getJSONArray("data");
                    if (totalRequestNo <= 1)
                        totalSearchResult = jsonObject.getInt("total_rows");
                    Log.v("total rows", String.valueOf(totalSearchResult));
                    for(int i = 0; i < searchResult.length(); i++) {
                        final JSONObject filterData = searchResult.getJSONObject(i);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.search_result, null);

                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.search_title_work_place);
                        textView1.setText(filterData.getString("Name"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //profile pic
                        ImageView profilePic = (ImageView) view.findViewById(R.id.user_profile_pic);
                        profilePic.setImageResource(R.mipmap.lab_dark);

                        //rating
                        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.doctor_user_rating);
                        if ( ! (filterData.getString("RatingAvg").equals("null") ||
                                filterData.getString("RatingAvg").isEmpty())) {
                            ratingBar.setRating(Float.valueOf(filterData.getString("RatingAvg")));
                        } else {
                            ratingBar.setRating(0.0f);
                        }

                        //review
                        TextView totalReviews = (TextView) view.findViewById(R.id.search_total_reviews);
                        totalReviews.setText(filterData.getString("TotalReview") + " " +
                                getResources().getString(R.string.reviews));
                        totalReviews.setTypeface(typeface);
                        totalReviews.setTextColor(Color.parseColor("#010101"));

                        //speciality
                        /*int specialityId = getContext().getResources().getIdentifier(filterData.getString("Mipmap"),
                                "mipmap",
                                getContext().getPackageName());
                        ImageView specialityImage = (ImageView) view.findViewById(R.id.speciality);
                        specialityImage.setImageResource(specialityId);*/



                        specialityLayout = (LinearLayout) view.findViewById(R.id.speciality_mipmap);
                        String allSpecialityMipmap = filterData.getString("Mipmap");
                        String[] mipmapArray = allSpecialityMipmap.split(",");
                        List<String> list = Arrays.asList(mipmapArray);
                        Set<String> set = new HashSet<String>(list);
                        resarray= new String[set.size()];
                        set.toArray(resarray);
                        parent = (LinearLayout) view.findViewById(R.id.speciality_mipmap);

                        lowDensityLayout();




                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.search_work_place_address);
                        textView2.setText(filterData.getString("Address").toString().trim());
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.search_work_place_phone);
                        textView3.setText(filterData.getString("Phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        linearLayout.addView(view);

                        final String callDialer = filterData.getString("Phone");
                        final String labName = filterData.getString("Name");
                        final String labAddress = filterData.getString("Address");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.search_phone_dialer);
                        //imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });

                        //lab details
                        final String detailsId =  filterData.getString("LabId");
                        final String mipmap = filterData.getString("Mipmap");
                        final String speciality = filterData.getString("Speciality");

                        LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                        //showDetails.setId(getId() + i);
                        showDetails.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                labProfile(detailsId, mipmap, speciality);
                            }
                        });

                        //social share
                        ImageView socialShare = (ImageView) view.findViewById(R.id.share_details);
                        socialShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                labProfile(detailsId, mipmap, speciality);
                            }
                        });

                        //bookmark
                        final LinearLayout withBookmark = (LinearLayout) view.findViewById(R.id.withBookmark);
                        final LinearLayout withoutBookmark = (LinearLayout) view.findViewById(R.id.withoutBookmark);
                        user_profile_pic_bookmark = (ImageView) view.findViewById(R.id.user_profile_pic_bookmark);
                        user_profile_pic_bookmark.setImageResource(R.mipmap.lab_dark);
                        final ImageView addToBookmarks = (ImageView) view.findViewById(R.id.add_to_bookmarks);
                        addToBookmarks.setTag(1);
                        String bookmarkStatus = filterData.getString("BookmarkStatus");
                        if ( ! (bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                            String bookmarkUserId = filterData.getString("BookmarkUserId");
                            String bookmarkUserStatus = filterData.getString("BookmarkStatus");
                            if (bookmarkUserId.equals(loginUserId) && bookmarkStatus.equals("1")) {
                                addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                addToBookmarks.setTag(2);
                                withBookmark.setVisibility(View.VISIBLE);
                                withoutBookmark.setVisibility(View.GONE);
                            }
                        }

                        addToBookmarks.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                if (isGuest) {
                                    guestLogin.confirmAlert(getContext());
                                    return;
                                }
                                workidAsync = detailsId;
                                workTypeBookmark = new WorkTypeBookmark();
                                workTypeBookmark.execute();
                                //workTypeBookmark(loginUserId, detailsId);
                                if (Integer.parseInt(addToBookmarks.getTag().toString()) == 1) {
                                    addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                    addToBookmarks.setTag(2);
                                    withBookmark.setVisibility(View.VISIBLE);
                                    withoutBookmark.setVisibility(View.GONE);
                                } else {
                                    addToBookmarks.setImageResource(R.mipmap.bookmarks);
                                    addToBookmarks.setTag(1);
                                    withBookmark.setVisibility(View.GONE);
                                    withoutBookmark.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }
                    totalResultRendered += searchResult.length();
                    Log.v("total rendered", String.valueOf(totalResultRendered));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            searchKeyword.setVisibility(View.VISIBLE);
            mapLocation.setVisibility(View.VISIBLE);
            topTenDoctorText.setVisibility(View.GONE);
        }
    }

    /**
     * Open phone dialer
     * @param callDialer string
     */
    public void openPhoneDialer(String callDialer) {
        android.support.v7.app.AlertDialog.Builder builderSingle = new android.support.v7.app.AlertDialog.Builder(getContext());
        builderSingle.setIcon(R.mipmap.call);
        builderSingle.setTitle(R.string.select_one_number);

        String search = "--";
        String search1 = "-";
        String search2 = ";";
        String[] array = new String[10];
        if (callDialer.indexOf(search) != -1) {
            array = callDialer.replaceAll(" ", "").split("\\--", -1);
        } else if (callDialer.indexOf(search1) != -1) {
            array = callDialer.replaceAll(" ", "").split("\\-", -1);
        } else if (callDialer.indexOf(search2) != -1) {
            array = callDialer.replaceAll(" ", "").split("\\;", -1);
        }

        if (array.length == 10) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + callDialer.trim()));
            startActivity(intent);
        } else {
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
            arrayAdapter.addAll(array);

            builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + strName));
                    startActivity(intent);

                }
            });
            builderSingle.show();
        }
    }

    /**
     * download image from internet
     */
    private class DownloadImageFromInternet extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public DownloadImageFromInternet(ImageView imageView) {
            this.imageView = imageView;
        }

        protected Bitmap doInBackground(String... urls) {
            String imageURL = urls[0];
            Bitmap bimage = null;
            try {
                InputStream in = new java.net.URL(imageURL).openStream();
                bimage = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                Log.e("Error Message", e.getMessage());
                e.printStackTrace();
            }
            return bimage;
        }

        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }
    }

    @Override
    public void onDestroyView() {
        //mContainer.removeAllViews();
        ViewGroup mContainer = (ViewGroup) getActivity().findViewById(R.id.search_layout);
        mContainer.removeAllViews();
        super.onDestroyView();
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);

        Log.v("Inside", "Menu visiblility" + String.valueOf(visible));
        if (visible) {
            //check for Internet
            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                return;
            }
            //clear map
            clearMapData();
            //check for menu visibility profile
            if (Preference.getValue(getContext(), "resetMenuVisibility","").equals("true")) {
                BaseActivity.showMenu();
            } else {
                BaseActivity.hideMenu();
            }
            Log.v("Menu Visibility", Preference.getValue(getContext(), "resetMenuVisibility", ""));
            if(Preference.getValue(getContext(), "HOSPITAL_SEARCH", "").equals("true")) {
                if (globalSearch != null)
                    globalSearch.cancel(true);
                totalRequestNo = 0;
                totalSearchResult = 0;
                totalResultRendered = 0;
                linearLayout.removeAllViews();
                CustomSearchHandle.resetPaginateCounterForHospital(getContext());
                hospitalSearch = new HospitalSearch();
                hospitalSearch.execute();
                CustomSearchHandle.setHospitalSearchpaginate(getContext());
                //Preference.setValue(getContext(), "HOSPITAL_SEARCH", "");
            } else if(Preference.getValue(getContext(), "CLINIC_SEARCH", "").equals("true")) {
                if (globalSearch != null)
                    globalSearch.cancel(true);
                totalRequestNo = 0;
                totalSearchResult = 0;
                totalResultRendered = 0;
                linearLayout.removeAllViews();
                CustomSearchHandle.resetPaginateCounterForClinic(getContext());
                clinicSearch = new ClinicSearch();
                clinicSearch.execute();
                CustomSearchHandle.setClinicPaginate(getContext());
                //Preference.setValue(getContext(), "CLINIC_SEARCH", "");
            } else if(Preference.getValue(getContext(), "DOCTOR_SEARCH_CUSTOM", "").equals("true")) {
                if (globalSearch != null)
                    globalSearch.cancel(true);
                Preference.setValue(getContext(), "GLOBAL_FILTER_TYPE", "doctor");
                locationAsync = Preference.getValue(getContext(), "DOCTOR_LOCATION_SEARCH", "");
                Preference.getValue(getContext(), "FILTER_LOCATION", locationAsync);
                specialityAsync = Preference.getValue(getContext(), "DOCTOR_SPECIALITY_SEARCH", "");
                genderAsync = Preference.getValue(getContext(), "DOCTOR_GENDER_SEARCH", "");
                //doctor search
                totalRequestNo = 0;
                totalSearchResult = 0;
                totalResultRendered = 0;
                linearLayout.removeAllViews();
                CustomSearchHandle.resetPaginateCounterForDoctor(getContext());
                doctorCustomSearch = new DoctorCustomSearch();
                doctorCustomSearch.execute();
                searchKeyword.setVisibility(View.VISIBLE);
                mapLocation.setVisibility(View.VISIBLE);
                topTenDoctorText.setVisibility(View.GONE);

                //set doctor search paginate
                CustomSearchHandle.setDoctorSearchPaginate(getContext());
                //Preference.setValue(getContext(), "DOCTOR_SEARCH_CUSTOM", "");
            } else if(Preference.getValue(getContext(), "LAB_SEARCH_CUSTOM", "").equals("true")) {
                if (globalSearch != null)
                    globalSearch.cancel(true);
                totalRequestNo = 0;
                totalSearchResult = 0;
                totalResultRendered = 0;
                linearLayout.removeAllViews();
                CustomSearchHandle.resetPaginateCounterForLab(getContext());
                specialityAsync = Preference.getValue(getContext(), "LAB_SEARCH_SPECIALITY", "");
                labSearch = new LabSearch();
                labSearch.execute();
                CustomSearchHandle.setLabPaginate(getContext());
                //Preference.setValue(getContext(), "LAB_SEARCH_GENDER", "");
                //Preference.setValue(getContext(), "LAB_SEARCH_SPECIALITY", "");
                //Preference.setValue(getContext(), "LAB_SEARCH_CUSTOM", "");
            } else if(Preference.getValue(getContext(), "USER_BOOKMARKS", "").equals("true")) {
                if (globalSearch != null)
                    globalSearch.cancel(true);
                totalRequestNo = 0;
                totalSearchResult = 0;
                totalResultRendered = 0;
                userBookmarkList = new UserBookmarkList();
                userBookmarkList.execute();
                linearLayout.removeAllViews();
                CustomSearchHandle.resetBookmarkPaginate(getContext());
                CustomSearchHandle.setBookmarkPaginate(getContext());
                Preference.setValue(getContext(), "USER_BOOKMARKS", "");
            } else if(Preference.getValue(getContext(), "PRIMARY_SEARCH", "").equals("true")) {
                if (globalSearch != null)
                    globalSearch.cancel(true);
                Log.v("inside primary", "primary search");
                totalRequestNo = 0;
                totalSearchResult = 0;
                totalResultRendered = 0;
                if (!Preference.getValue(getContext(), "PROMARY_SEARCH_KEY", "").equals("")) {
                    searchAsync = Preference.getValue(getContext(), "PROMARY_SEARCH_KEY", "");
                    searchKeyword.setText(searchAsync);
                }
                else {
                    searchAsync = "";
                    searchKeyword.setText("");
                }

                CustomSearchHandle.resetPaginateForGlobalSearch(getContext());
                //global search
                globalSearch = new GlobalSearch();
                globalSearch.execute();
                if (searchKeyword != null)
                    searchKeyword.setText(searchAsync);
                CustomSearchHandle.setGlobalSearchPaginate(getContext());
                //Preference.setValue(getContext(), "PRIMARY_SEARCH", "");
                //Preference.setValue(getContext(), "PROMARY_SEARCH_KEY", "");
            } else if (Preference.getValue(getContext(), "SEARCH_REDIRECT", "").equals("true")){
                getAllWorkMaster = new GetAllWorkMaster();
                getAllWorkMaster.execute();
                Preference.setValue(getContext(), "SEARCH_REDIRECT", "");
                //globalSearch(searchKeyword.getText().toString());
            }
        } else {
            BaseActivity.hideMenu();
        }
    }

    /**
     * render user bookmatk result
     */
    class RenderUserBookmarksResult {
        RenderUserBookmarksResult(String result) {
            if (totalRequestNo <= 1)
                linearLayout.removeAllViews();
            //linearLayout.removeAllViews();
            linearLayout.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            );
            try {
                listingOrMapLayout();
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.no_bookmarks_found));
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    totalSearchResult = 0;
                    totalResultRendered = 0;

                    linearLayout.addView(textView);
                } else {
                    JSONArray searchResult = jsonObject.getJSONArray("result");
                    Log.v("ResultData: ", String.valueOf(searchResult.length()));
                    for(int i = 0; i < searchResult.length(); i++) {
                        final JSONObject filterData = searchResult.getJSONObject(i);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.search_result, null);

                        //rating and review
                        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.doctor_user_rating);
                        TextView reviews = (TextView) view.findViewById(R.id.search_total_reviews);
                        ratingBar.setRating(0.0f);
                        if (!(filterData.getString("ratingAvg").equals("null") ||
                                filterData.getString("ratingAvg").isEmpty())) {
                            ratingBar.setRating(Float.valueOf(filterData.getString("ratingAvg")));
                        }
                        reviews.setText(filterData.getString("TotalReview") + " " + getResources().getString(R.string.reviews));

                        //bookmark name
                        TextView textView1 = (TextView) view.findViewById(R.id.search_title_work_place);
                        if (filterData.getString("Name").equals("null")) break;
                        textView1.setText(filterData.getString("Name"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.search_work_place_address);
                        textView2.setText(filterData.getString("address").toString().trim());
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.search_work_place_phone);
                        textView3.setText(filterData.getString("phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        //total reviews
                        /*TextView textView4 = (TextView) view.findViewById(R.id.search_total_reviews);
                        textView4.setText(getResources().getString(R.string.total_reviews));
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));*/

                        linearLayout.addView(view);

                        final String callDialer = filterData.getString("phone");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.search_phone_dialer);
                        //imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });

                        //doctor details
                        //workidAsync =  filterData.getString("Id");
                        doctornameAsync =  filterData.getString("Name");
                        LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                        //mipmap
                        if (filterData.has("MipMap")) {
                            ImageView mipmapImage = (ImageView) view.findViewById(R.id.speciality);
                            int mipmapId = getContext().getResources().getIdentifier(filterData.getString("MipMap"),
                                    "mipmap", getContext().getPackageName());
                            mipmapImage.setImageResource(mipmapId);
                            bookmarkMipmap = filterData.getString("MipMap");
                        }

                        //showDetails.setId(getId() + i);
                        showDetails.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
//                                Log.d("Type","Type: "+ filterData.getString("worktype"));
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                try {
                                    workidAsync =  filterData.getString("Id");
                                    Log.d("Type","Type: "+ filterData.getString("worktype"));
                                    if (filterData.has("worktype") && filterData.getString("worktype").equals("Doctor")) {
                                        //doctor profile

                                        doctorProfile(workidAsync, doctornameAsync);
                                    } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Hospital")) {
                                        //hospital profile
                                        hospitalProfile(workidAsync, doctornameAsync);
                                    } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Lab")) {
                                        //lab profile
                                        labProfile(workidAsync, bookmarkMipmap, "");
                                    } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Clinic")) {
                                        clinicProfile(workidAsync, bookmarkMipmap, "");
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });

                        //bookmark
                        final ImageView bookmarkImage = (ImageView) view.findViewById(R.id.add_to_bookmarks);
                        bookmarkImage.setImageResource(R.mipmap.bookmark_2);
                        bookmarkImage.setTag(2);
                        bookmarkImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                try {
                                    workidAsync =  filterData.getString("Id");
                                    Log.v("Type", workidAsync);
                                    if (filterData.has("worktype") && filterData.getString("worktype").equals("Doctor")) {
                                        //doctor bookmark
                                        doctorBookmarksAddUpdate = new DoctorBookmarksAddUpdate();
                                        doctorBookmarksAddUpdate.execute();
                                    } else if ((filterData.has("worktype")) &&
                                            filterData.getString("worktype").equals("Hospital") ||
                                            filterData.getString("worktype").equals("Lab") ||
                                            filterData.getString("worktype").equals("Medical Lab") ||
                                            filterData.getString("worktype").equals("Radiology Lab") ||
                                            filterData.getString("worktype").equals("Clinic")) {
                                        //hospital bookmark
                                        workTypeBookmark = new WorkTypeBookmark();
                                        workTypeBookmark.execute();
                                        //workTypeBookmark(loginUserId, clinicId);
                                    }
                                    if (Integer.parseInt(bookmarkImage.getTag().toString()) == 1) {
                                        bookmarkImage.setImageResource(R.mipmap.bookmark_2);
                                        bookmarkImage.setTag(2);
                                    } else {
                                        bookmarkImage.setImageResource(R.mipmap.bookmarks);
                                        bookmarkImage.setTag(1);
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });

                        //profile
                        ImageView profileImage = (ImageView) view.findViewById(R.id.share_details);
                        profileImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    workidAsync =  filterData.getString("Id");
                                    Log.v("Type", filterData.getString("worktype"));
                                    if (filterData.has("worktype") && filterData.getString("worktype").equals("Doctor")) {
                                        //doctor profile
                                        doctorProfile(workidAsync, doctornameAsync);
                                    } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Hospital")) {
                                        //hospital profile
                                        hospitalProfile(workidAsync, doctornameAsync);
                                    } else if (filterData.has("worktype") &&
                                            ((filterData.getString("worktype").equals("Lab")) || (filterData.getString("worktype").equals("Radiology Lab")) ||
                                            (filterData.getString("worktype").equals("Medical Lab")))) {
                                        //lab profile
                                        labProfile(workidAsync, bookmarkMipmap, "");
                                    } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Clinic")) {
                                        clinicProfile(workidAsync, bookmarkMipmap, "");
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });

                        //change profile pic
                        ImageView profilePic = (ImageView) view.findViewById(R.id.user_profile_pic);
                        try {
                            Log.v("Type", filterData.getString("worktype"));
                            if (filterData.has("worktype") && filterData.getString("worktype").equals("Doctor")) {
                                //doctor profile pic

                            } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Hospital")) {
                                //hospital profile
                                profilePic.setImageResource(R.mipmap.hospital_dark);
                            } else if (filterData.has("worktype") &&
                                    ((filterData.getString("worktype").equals("Lab")) || (filterData.getString("worktype").equals("Radiology Lab")) ||
                                            (filterData.getString("worktype").equals("Medical Lab"))))  {
                                //lab profile
                                profilePic.setImageResource(R.mipmap.lab_dark);
                            } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Clinic")) {
                                //clinicProfile(clinicId, bookmarkMipmap, "");
                                profilePic.setImageResource(R.mipmap.clinic_dark);
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            searchKeyword.setVisibility(View.GONE);
            mapLocation.setVisibility(View.GONE);
            topTenDoctorText.setVisibility(View.VISIBLE);
            //Preference.setValue(getContext(), "TOP_TEN_DOCTORS", "");
        }
    }

    /**
     * hide filter icon
     */
    public void hideFilterIcon() {
        //searchKeyword.setCompoundDrawables(null, null, null, null);
        //searchKeyword.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        //searchKeyword.setFocusable(false);
    }

    /**
     * get category position
     * @param category string
     * @return
     */
    private int getCategoryPos(String category) {
        return specialityIcon.indexOf(category);
    }

    public void shareIt(final String name, final String phone, final String address, final String rating) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Name: " + name + "\n" + "Phone: " + phone + "\n" + "Address: "
                + address + "\n" + "Rating: " + rating);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.social_text)));
    }

    public void hideProgrssBar() {
        progressBar.setVisibility(View.GONE);
        searchResultList.setVisibility(View.VISIBLE);
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        searchResultList.setVisibility(View.GONE);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (globalSearch != null)
            globalSearch.cancel(true);
        if (searchFilterAsync != null)
            searchFilterAsync.cancel(true);
        if (doctorCustomSearch != null)
            doctorCustomSearch.cancel(true);
        if (hospitalSearch != null)
            hospitalSearch.cancel(true);
        if (clinicSearch != null)
            clinicSearch.cancel(true);
        if (labSearch != null)
            labSearch.cancel(true);
        if (userBookmarkList != null)
            userBookmarkList.cancel(true);
        if (getAllWorkMaster != null)
            getAllWorkMaster.cancel(true);
    }

    public void clearMapData() {
        latitudeList = new ArrayList<>();
        longitudeList = new ArrayList<>();
        mapTypes = new ArrayList<>();
        titles = new ArrayList<>();
    }
}
