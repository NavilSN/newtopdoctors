package searchnative.com.topdoctors;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

/**
 * Created by root on 25/11/16.
 */

public class RecentlyViewedFragment extends Fragment {

    private String userLoginId;
    private String recentlyViewedIds;
    private LinearLayout recentlyViewedListLayout;
    private Typeface typeface;
    private ProgressBar progressBar;
    private String workidAsync;
    private TextView recentlyViewedProfile;
    String[] resarray;
    LinearLayout specialityLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getContext(), new Crashlytics());
        View view = inflater.inflate(R.layout.recently_viewed, container, false);
        //supportMapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.google_map_fragment);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        userLoginId = Preference.getValue(getContext(), "LOGIN_ID", "");
        recentlyViewedListLayout = (LinearLayout) getView().findViewById(R.id.recently_viewed_list);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ExoMedium.otf");
        recentlyViewedProfile = (TextView) getView().findViewById(R.id.recentlyViewedProfile);
        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        recentlyViewedProfile.setTypeface(typeface);

        ImageView imageView = (ImageView) getView().findViewById(R.id.reviews_swipe_menu);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawerLayout);
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });
        recentlyViewedIds = android.text.TextUtils.join(",", RecentlyViewed.getRecentlyViewedFromPreference(getContext(),
                RecentlyViewed.name));
        Log.v("ids", recentlyViewedIds);
        RecentltyViewedAsynck recentltyViewedAsynck = new RecentltyViewedAsynck();
        recentltyViewedAsynck.execute();
    }

    class RecentltyViewedAsynck extends AsyncTask<String, Void, String> {
        String URL = AppConfig.getWebServiceUrl() + "user/recentViewData";

        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("userId", userLoginId));
            nameValuePairs.add(new BasicNameValuePair("recentlyViewed", recentlyViewedIds));
            nameValuePairs.add(new BasicNameValuePair("lang", LocalInformation.getLocaleLang()));

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();

                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            new RenderRecentlyViewed(result);
            Log.v("recent", result);
            progressBar.setVisibility(View.GONE);
            recentlyViewedListLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
            recentlyViewedListLayout.setVisibility(View.GONE);
        }
    }

    class RenderRecentlyViewed {
        RenderRecentlyViewed(String result) {
            recentlyViewedListLayout.removeAllViews();
            recentlyViewedListLayout.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            );

            try {
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.no_recent_entry));
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    recentlyViewedListLayout.addView(textView);
                } else {
                    JSONArray searchResult = jsonObject.getJSONArray("result");
                    for(int i = 0; i < searchResult.length(); i++) {
                        final JSONObject filterData = searchResult.getJSONObject(i);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.search_result, null);

                        if (filterData.has("worktype")) {
                            if (filterData.getString("worktype").equals("Doctor")) {
                                ImageView doctorMipmap = (ImageView) view.findViewById(R.id.speciality);

                                int id = getContext().getResources().getIdentifier(filterData.getString("Mipmap"),
                                        "mipmap", getContext().getPackageName());
                                doctorMipmap.setImageResource(id);
                            } else {

                                specialityLayout = (LinearLayout) view.findViewById(R.id.speciality_mipmap);
                                String allSpecialityMipmap = filterData.getString("Mipmap");
                                String[] mipmapArray = allSpecialityMipmap.split(",");
                                List<String> list = Arrays.asList(mipmapArray);
                                Set<String> set = new HashSet<String>(list);
                                resarray= new String[set.size()];
                                set.toArray(resarray);
                                lowDensityLayout();

                            }
                        }

                        if (filterData.getString("Name").equals("") ||
                                filterData.getString("Name").isEmpty() ||
                                filterData.getString("Name").equals("null"))
                            continue;

                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.search_title_work_place);
                        textView1.setText(filterData.getString("Name"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));
                        final String doctorName = filterData.getString("Name");

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.search_work_place_address);
                        textView2.setText(filterData.getString("Address"));
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));
                        final String doctorAddress = filterData.getString("Address");

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.search_work_place_phone);
                        textView3.setText(filterData.getString("phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));
                        final String doctorPhone = filterData.getString("phone");

                        //rating
                        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.doctor_user_rating);
                        if(!filterData.getString("ratingAvg").equals("null") &&
                                !filterData.getString("ratingAvg").isEmpty()) {
                            ratingBar.setRating(Float.valueOf(filterData.getString("ratingAvg")));

                        }
                        final String doctorRating = String.valueOf(filterData.getString("ratingAvg"));

                        //total reviews
                        TextView textView4 = (TextView) view.findViewById(R.id.search_total_reviews);
                        if(!filterData.getString("totalreview").equals("null") &&
                                !filterData.getString("totalreview").isEmpty()) {
                            textView4.setText(filterData.getString("totalreview") + " " + getResources().getString(R.string.reviews));
                        } else {
                            textView4.setText(0 + getResources().getString(R.string.reviews));
                        }
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));



                        recentlyViewedListLayout.addView(view);

                        final String callDialer = filterData.getString("phone");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.search_phone_dialer);
                        imageView.setId(imageView.getId() + 100000 + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
                                builderSingle.setIcon(R.mipmap.call);
                                builderSingle.setTitle(R.string.select_one_number);

                                String search = "--";
                                String search1 = "-";
                                String search2 = ";";
                                String[] array = new String[10];
                                if (callDialer.indexOf(search) != -1) {
                                    array = callDialer.replaceAll(" ", "").split("\\--", -1);
                                } else if (callDialer.indexOf(search1) != -1) {
                                    array = callDialer.replaceAll(" ", "").split("\\-", -1);
                                } else if (callDialer.indexOf(search2) != -1) {
                                    array = callDialer.replaceAll(" ", "").split("\\;", -1);
                                }


                                if (array.length == 10) {
                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                    intent.setData(Uri.parse("tel:" + callDialer.trim()));
                                    startActivity(intent);
                                } else {
                                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
                                    arrayAdapter.addAll(array);

                                    builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                                    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            String strName = arrayAdapter.getItem(which);
                                            Intent intent = new Intent(Intent.ACTION_DIAL);
                                            intent.setData(Uri.parse("tel:" + strName));
                                            startActivity(intent);

                                        }
                                    });
                                    builderSingle.show();
                                }
                            }
                        });

                        //set profile image
                        ImageView profilePic = (ImageView) view.findViewById(R.id.user_profile_pic);
                        ImageView user_profile_pic_bookmark = (ImageView) view.findViewById(R.id.user_profile_pic_bookmark);
                        final String worktype = filterData.getString("worktype");
                        final String id = filterData.getString("ID");
                        final String name = filterData.getString("Name");
                        if (worktype.equals("Hospital")) {
                            profilePic.setImageResource(R.mipmap.hospital_dark);
                            user_profile_pic_bookmark.setImageResource(R.mipmap.hospital_dark);
                        } else if (worktype.equals("Clinic")) {
                            profilePic.setImageResource(R.mipmap.clinic_dark);
                            user_profile_pic_bookmark.setImageResource(R.mipmap.clinic_dark);
                        } else if (worktype.equals("Lab") || worktype.equals("Radiology Lab") || worktype.equals("Medical Lab")) {
                            profilePic.setImageResource(R.mipmap.lab_dark);
                            user_profile_pic_bookmark.setImageResource(R.mipmap.lab_dark);
                        }

                        //add to bookmark
                        final LinearLayout withBookmark = (LinearLayout) view.findViewById(R.id.withBookmark);
                        final LinearLayout withoutBookmark = (LinearLayout) view.findViewById(R.id.withoutBookmark);
                        final ImageView addToBookmarks = (ImageView) view.findViewById(R.id.add_to_bookmarks);
                        addToBookmarks.setTag(1);
                        String bookmarkStatus = filterData.getString("BookmarkStatus");
                        if ( ! (bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                            String bookmarkUserId = filterData.getString("BookmarkUserId");
                            //String bookmarkUserStatus = filterData.getString("BookmarkStatus");
                            if (bookmarkUserId.equals(userLoginId) && bookmarkStatus.equals("1")) {
                                addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                addToBookmarks.setTag(2);
                                withBookmark.setVisibility(View.VISIBLE);
                                withoutBookmark.setVisibility(View.GONE);
                            }
                        }

                        //addToBookmarks.setId(addToBookmarks.getId() + i + 10000 + i);
                        addToBookmarks.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (worktype.equals("Hospital") ||
                                        worktype.equals("Clinic") ||
                                        worktype.equals("Lab") || worktype.equals("Radiology Lab") || worktype.equals("Medical Lab")) {
                                    workidAsync = id;
                                    new WorkTypeBookmark().execute();
                                } else {
                                    doctorBookMarks(id);
                                }
                                if (Integer.parseInt(addToBookmarks.getTag().toString()) == 1) {
                                    addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                    addToBookmarks.setTag(2);
                                    withBookmark.setVisibility(View.VISIBLE);
                                    withoutBookmark.setVisibility(View.GONE);
                                } else {
                                    addToBookmarks.setImageResource(R.mipmap.bookmarks);
                                    addToBookmarks.setTag(1);
                                    withBookmark.setVisibility(View.GONE);
                                    withoutBookmark.setVisibility(View.VISIBLE);
                                }
                            }
                        });

                        LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                        showDetails.setId(getId() + i);
                        showDetails.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (worktype.equals("Hospital")) {
                                    hospitalProfile(id, name);
                                } else if (worktype.equals("Clinic")) {
                                    clinicProfile(id, "", "");
                                } else if (worktype.equals("Lab") || worktype.equals("Radiology Lab") || worktype.equals("Medical Lab")) {
                                    labProfile(id, "", "");
                                } else {
                                    doctorProfile(id, name);
                                }
                            }
                        });

                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (worktype.equals("Hospital")) {
                                    hospitalProfile(id, name);
                                } else if (worktype.equals("Clinic")) {
                                    clinicProfile(id, "", "");
                                } else if (worktype.equals("Lab") || worktype.equals("Radiology Lab") || worktype.equals("Medical Lab")) {
                                    labProfile(id, "", "");
                                } else {
                                    doctorProfile(id, name);
                                }
                            }
                        });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void doctorBookMarks(final String doctorId) {

        class DoctorBookmarksAddUpdate extends AsyncTask<String, Void, String> {

            String URL = AppConfig.getWebServiceUrl() + "bookmark/add";

            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            String quickDoctorId = doctorId;
            String quickUserId = Preference.getValue(getContext(), "LOGIN_ID", "");

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("doctorId", quickDoctorId));
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("lang", LocalInformation.getLocaleLang()));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    return mClient.execute(httpPost, responseHandler);
                } catch(IOException e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    Log.v("result", jsonObject.toString());
                    String responseResult = jsonObject.getString("result");
                    String doctorName = jsonObject.getString("name");
                    if(responseResult.equals("Bookmarked")) {
                        //addToBookmarks.setImageResource(R.mipmap.add_a_lab);
                        Toast.makeText(getContext(), doctorName + " is successfully added in your bookmark.", Toast.LENGTH_SHORT).show();
                    } else {
                        //addToBookmarks.setImageResource(R.mipmap.bookmarks);
                        Toast.makeText(getContext(), doctorName + " is successfully removed from your bookmark.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                /*mProgressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
            }
        }

        DoctorBookmarksAddUpdate doctorBookmarksAddUpdate = new DoctorBookmarksAddUpdate();
        doctorBookmarksAddUpdate.execute(doctorId);
    }

    class WorkTypeBookmark extends AsyncTask<String, Void, String> {
        final String URL = AppConfig.getWebServiceUrl() + "Worktype_bookmark/Add";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            Log.v("bookmark", "worktype");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("userId", userLoginId));
            nameValuePairs.add(new BasicNameValuePair("workId", workidAsync));
            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.v("result", result.toString());
            try {
                JSONObject jsonObject = new JSONObject(result);
                String responseResult = jsonObject.getString("result");
                String doctorName = jsonObject.getString("name");

                if(responseResult.equals("Bookmarked")) {
                    //addToBookmarks.setImageResource(R.mipmap.lab_dark);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "true");
                    Toast.makeText(getContext(), doctorName + " is successfully added in your bookmark.", Toast.LENGTH_SHORT).show();
                } else if(responseResult.equals("Bookmark is added")) {
                    Toast.makeText(getContext(), doctorName + " is successfully added in your bookmark.", Toast.LENGTH_SHORT).show();
                } else {
                    //addToBookmarks.setImageResource(R.mipmap.bookmarks);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "false");
                    Toast.makeText(getContext(), doctorName + " is successfully removed from your bookmark.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }

    /**
     * doctor profile
     * @param doctorId string
     * @param doctorName string
     */
    public void doctorProfile(final String doctorId, final String doctorName) {
        Bundle args = new Bundle();
        args.putString("id", doctorId);
        Log.v("id", doctorId);
        Log.v("name", doctorName);
        args.putString("name", doctorName);
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        DoctorProfileFragment clinicProfileFragment = new DoctorProfileFragment();
        clinicProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("doctorProfile");
        mFragmentTransaction.add(R.id.search_layout, clinicProfileFragment).commit();
    }

    /**
     * clinic profile
     * @param id string
     * @param mipmap string
     * @param speciality string
     */
    public void clinicProfile(String id, String mipmap, String speciality) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("mipmap", mipmap);
        args.putString("speciality", speciality);

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        ClinicProfileFragment clinicProfileFragment = new ClinicProfileFragment();
        clinicProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("clinicProfile");
        mFragmentTransaction.replace(R.id.search_layout, clinicProfileFragment).commit();
    }

    /**
     * hospital profile
     * @param id string
     * @param name string
     */
    public void hospitalProfile(final String id, final String name) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("hospitalName", name);

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        HospitalProfileFragment doctorprofile = new HospitalProfileFragment();
        doctorprofile.setArguments(args);

        mFragmentTransaction.addToBackStack("hospitalProfile");
        mFragmentTransaction.replace(R.id.search_layout, doctorprofile).commit();
    }

    /**
     * lab profile
     * @param id string
     * @param mipmap string
     * @param speciality string
     */
    public void labProfile(String id, String mipmap, String speciality) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("mipmap", mipmap);
        args.putString("speciality", speciality);

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        LabProfileFragment labProfileFragment = new LabProfileFragment();
        labProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("labProfile");
        mFragmentTransaction.replace(R.id.search_layout, labProfileFragment).commit();
    }


    public void lowDensityLayout()
    {

        int j=1;
        for (String specialityMipmapName : resarray) {

            if(j > 3)
                break;

            View mipmapIcon;
            LayoutInflater mipmapIconInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            mipmapIcon = mipmapIconInflater.inflate(R.layout.mipmap_icon, null);

            ImageView mipmapSpec = (ImageView) mipmapIcon.findViewById(R.id.mipmapIcon);

//         Toast.makeText(getContext(),"Dpi: "+getResources().getDisplayMetrics().densityDpi,Toast.LENGTH_SHORT).show();

            switch (getResources().getDisplayMetrics().densityDpi) {


                /**
                 * https://en.wikipedia.org/wiki/Pixel_density
                 * Named pixel densities
                 * DENSITY_280 - xhdpi
                 * DENSITY_360 - xxhdpi
                 * DENSITY_400 - xxhdpi
                 * DENSITY_420 - xxhdpi
                 * DENSITY_560 - xxxhdpi
                 */

                case DisplayMetrics.DENSITY_280:{
                    mipmapSpec.getLayoutParams().height=55;
                    mipmapSpec.getLayoutParams().width=60;
                }
                break;


                case DisplayMetrics.DENSITY_360:{
                    mipmapSpec.getLayoutParams().height=90;
                    mipmapSpec.getLayoutParams().width=90;
                }
                break;

                case DisplayMetrics.DENSITY_400:{
                    mipmapSpec.getLayoutParams().height=90;
                    mipmapSpec.getLayoutParams().width=90;
                }
                break;

                case DisplayMetrics.DENSITY_420:{
                    mipmapSpec.getLayoutParams().height=90;
                    mipmapSpec.getLayoutParams().width=90;
                }
                break;


                case DisplayMetrics.DENSITY_560:{
                    mipmapSpec.getLayoutParams().height=120;
                    mipmapSpec.getLayoutParams().width=120;
                }
                break;

                case DisplayMetrics.DENSITY_LOW:{
                    mipmapSpec.getLayoutParams().height=20;
                    mipmapSpec.getLayoutParams().width=20;
                }
                break;

                case DisplayMetrics.DENSITY_MEDIUM:{
                    mipmapSpec.getLayoutParams().height=30;
                    mipmapSpec.getLayoutParams().width=30;
                }
                break;

                case DisplayMetrics.DENSITY_HIGH:{
                    mipmapSpec.getLayoutParams().height=30;
                    mipmapSpec.getLayoutParams().width=30;
                }

                break;

                case DisplayMetrics.DENSITY_XHIGH:{
                    mipmapSpec.getLayoutParams().height=55;
                    mipmapSpec.getLayoutParams().width=55;
                }

                break;

                case DisplayMetrics.DENSITY_XXHIGH:{
                    mipmapSpec.getLayoutParams().height=70;
                    mipmapSpec.getLayoutParams().width=70;
                }

                break;

                case DisplayMetrics.DENSITY_XXXHIGH:{
                    mipmapSpec.getLayoutParams().height=120;
                    mipmapSpec.getLayoutParams().width=120;
                }
                break;

            }

            mipmapSpec.setScaleType(ImageView.ScaleType.FIT_XY);

            int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName.toString().trim()
                    , "mipmap", getContext().getPackageName());
            //ImageView spec = new ImageView(getContext());

//            mipmapIcon = resizeView(mipmapIcon);

            if(mipmapId == 0)
                mipmapSpec.setVisibility(View.GONE);
            else
                mipmapSpec.setImageResource(mipmapId);
//         mipmapSpec.setImageResource(R.mipmap.face24);

            //int dp = (int) (getContext().getResources().getDimension(R.dimen.doctor_speciality_icon_width_height) / getContext().getResources().getDisplayMetrics().density);

            specialityLayout.addView(mipmapIcon);
            j++;
        }

    }


}
