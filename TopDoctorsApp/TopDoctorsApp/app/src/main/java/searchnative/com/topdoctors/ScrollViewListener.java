package searchnative.com.topdoctors;

/**
 * Created by root on 23/11/16.
 */

public interface ScrollViewListener {
    void onScrollChanged(ScrollViewExt scrollView,
                         int x, int y, int oldx, int oldy);
}
