package searchnative.com.topdoctors;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

/**
 * Created by root on 1/12/16.
 */

public class BookmarkFragment extends Fragment
        implements ScrollViewListener {

    private String loginUserId;
    int totalRequestNo = 0, totalSearchResult = 0, totalResultRendered = 0;
    private String appLang = LocalInformation.getLocaleLang();
    private static boolean isAsynchOn = true;
    private LinearLayout linearLayout;
    private Typeface typeface;
    private String workidAsync, doctornameAsync, bookmarkMipmap;
    DoctorBookmarksAddUpdate doctorBookmarksAddUpdate;
    WorkTypeBookmark workTypeBookmark;
    UserBookmarkList userBookmarkList;
    private ProgressBar progressBar;
    private TextView bookmarkTitle;
    private ScrollViewExt scrollView;

    String[] resarray;
    LinearLayout specialityLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getContext(),new Crashlytics());
        View view = inflater.inflate(R.layout.bookmark_layout, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/ExoMedium.otf");
        linearLayout = (LinearLayout) getView().findViewById(R.id.bookmarkResult);
        linearLayout.setVisibility(View.INVISIBLE);
        scrollView = (ScrollViewExt) getView().findViewById(R.id.bookmarkScroll);
        scrollView.setVisibility(View.INVISIBLE);
        loginUserId = Preference.getValue(getContext(), "LOGIN_ID", "");
        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        bookmarkTitle = (TextView) getView().findViewById(R.id.bookmarkTitle);
        bookmarkTitle.setTypeface(typeface);

        ImageView v = (ImageView) getActivity().findViewById(R.id.bookmarkSwipeMenu);

        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                DrawerLayout mDrawer = (DrawerLayout) getActivity().findViewById(R.id.drawerLayout);
                mDrawer.openDrawer(Gravity.LEFT);
                return true;
            }
        });

        userBookmarkList = new UserBookmarkList();
        userBookmarkList.execute();
    }

    @Override
    public void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy) {
        // We take the last son in the scrollview
        View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
        int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
        if (diff == 0) {

        }
    }

    class UserBookmarkList extends AsyncTask<String, Void, String> {
        String URL = AppConfig.getWebServiceUrl() + "bookmark/bookmarkList";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("page", String.valueOf(totalRequestNo)));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.v("Bookmark Doctor: ", result);
            new RenderUserBookmarksResult(result);
            progressBar.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.INVISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        }
    }

    class RenderUserBookmarksResult {
        RenderUserBookmarksResult(String result) {
            if (totalRequestNo <= 1)
                linearLayout.removeAllViews();
            //linearLayout.removeAllViews();
            linearLayout.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            );
            try {
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.no_bookmarks_found));
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    totalSearchResult = 0;
                    totalResultRendered = 0;

                    linearLayout.addView(textView);
                } else {
                    JSONArray searchResult = jsonObject.getJSONArray("result");
                    Log.v("ResultData: ", String.valueOf(searchResult.length()));
                    for(int i = 0; i < searchResult.length(); i++) {
                        final JSONObject filterData = searchResult.getJSONObject(i);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.search_result, null);

                        final LinearLayout withBookmark = (LinearLayout) view.findViewById(R.id.withBookmark);
                        final LinearLayout withoutBookmark = (LinearLayout) view.findViewById(R.id.withoutBookmark);

                        withBookmark.setVisibility(View.VISIBLE);
                        withoutBookmark.setVisibility(View.GONE);


                        if (filterData.has("worktype")) {
                            if (filterData.getString("worktype").equals("Doctor")) {

                               ImageView doctorMipmap = (ImageView) view.findViewById(R.id.speciality);
                                int id = getContext().getResources().getIdentifier(filterData.getString("MipMap"),
                                        "mipmap", getContext().getPackageName());
                                doctorMipmap.setImageResource(id);


                               /* specialityLayout = (LinearLayout) view.findViewById(R.id.speciality_mipmap);
                                String allSpecialityMipmap = filterData.getString("MipMap");
                                String[] mipmapArray = allSpecialityMipmap.split(",");
                                List<String> list = Arrays.asList(mipmapArray);
                                Set<String> set = new HashSet<String>(list);
                                resarray= new String[set.size()];
                                set.toArray(resarray);
                                lowDensityLayout();*/



                            } else {

                                specialityLayout = (LinearLayout) view.findViewById(R.id.speciality_mipmap);
                                String allSpecialityMipmap = filterData.getString("Mipmap");
                                String[] mipmapArray = allSpecialityMipmap.split(",");
                                List<String> list = Arrays.asList(mipmapArray);
                                Set<String> set = new HashSet<String>(list);
                                resarray= new String[set.size()];
                                set.toArray(resarray);
                                lowDensityLayout();

                            }

                        }




                        //rating and review
                        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.doctor_user_rating);
                        TextView reviews = (TextView) view.findViewById(R.id.search_total_reviews);
                        ratingBar.setRating(0.0f);
                        if (!(filterData.getString("ratingAvg").equals("null") ||
                                filterData.getString("ratingAvg").isEmpty())) {
                            ratingBar.setRating(Float.valueOf(filterData.getString("ratingAvg")));
                        }
                        reviews.setText(filterData.getString("TotalReview") + " " + getResources().getString(R.string.reviews));
                        reviews.setTypeface(typeface);

                        //bookmark name
                        TextView textView1 = (TextView) view.findViewById(R.id.search_title_work_place);
                        if (filterData.getString("Name").equals("null")) break;
                        textView1.setText(filterData.getString("Name"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.search_work_place_address);
                        textView2.setText(filterData.getString("address"));
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.search_work_place_phone);
                        textView3.setText(filterData.getString("phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        //total reviews
                        /*TextView textView4 = (TextView) view.findViewById(R.id.search_total_reviews);
                        textView4.setText(getResources().getString(R.string.total_reviews));
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));*/

                        linearLayout.addView(view);

                        final String callDialer = filterData.getString("phone");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.search_phone_dialer);
                        //imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });

                        //doctor details
                        //workidAsync =  filterData.getString("Id");
                        doctornameAsync =  filterData.getString("Name");
                        LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                        //mipmap
                        if (filterData.has("MipMap")) {
                            ImageView mipmapImage = (ImageView) view.findViewById(R.id.speciality);
                            int mipmapId = getContext().getResources().getIdentifier(filterData.getString("MipMap"),
                                    "mipmap", getContext().getPackageName());
                            mipmapImage.setImageResource(mipmapId);
                            bookmarkMipmap = filterData.getString("MipMap");
                        }

                        //showDetails.setId(getId() + i);
                        showDetails.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    workidAsync =  filterData.getString("Id");
                                    Log.v("Type", filterData.getString("worktype"));
                                    if (filterData.has("worktype") && filterData.getString("worktype").equals("Doctor")) {
                                        //doctor profile
                                        doctorProfile(workidAsync, doctornameAsync);
                                    } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Hospital")) {
                                        //hospital profile
                                        hospitalProfile(workidAsync, doctornameAsync);
                                    } else if (filterData.has("worktype") && (filterData.getString("worktype").equals("Radiology Lab"))
                                            || (filterData.getString("worktype").equals("Madical Lab"))){
                                        //lab profile
                                        labProfile(workidAsync, bookmarkMipmap, "");
                                    } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Clinic")) {
                                        clinicProfile(workidAsync, bookmarkMipmap, "");
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });

                        //bookmark
                        final ImageView user_profile_pic_bookmark = (ImageView) view.findViewById(R.id.user_profile_pic_bookmark);
                        final ImageView bookmarkImage = (ImageView) view.findViewById(R.id.add_to_bookmarks);
                        bookmarkImage.setImageResource(R.mipmap.bookmark_2);
                        bookmarkImage.setTag(2);
                        bookmarkImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    workidAsync =  filterData.getString("Id");
                                    Log.v("Type", workidAsync);
                                    if (filterData.has("worktype") && filterData.getString("worktype").equals("Doctor")) {
                                        //doctor bookmark
                                        doctorBookmarksAddUpdate = new DoctorBookmarksAddUpdate();
                                        doctorBookmarksAddUpdate.execute();
                                    } else if ((filterData.has("worktype")) &&
                                            filterData.getString("worktype").equals("Hospital") ||
                                            filterData.getString("worktype").equals("Lab") ||
                                            filterData.getString("worktype").equals("Radiology Lab") ||
                                            filterData.getString("worktype").equals("Medical Lab") ||
                                            filterData.getString("worktype").equals("Clinic")) {
                                        //hospital bookmark
                                        workTypeBookmark = new WorkTypeBookmark();
                                        workTypeBookmark.execute();
                                        //workTypeBookmark(loginUserId, clinicId);
                                    }
                                    if (Integer.parseInt(bookmarkImage.getTag().toString()) == 1) {
                                        bookmarkImage.setImageResource(R.mipmap.bookmark_2);
                                        bookmarkImage.setTag(2);
                                        withBookmark.setVisibility(View.VISIBLE);
                                        withoutBookmark.setVisibility(View.GONE);
                                    } else {
                                        bookmarkImage.setImageResource(R.mipmap.bookmarks);
                                        bookmarkImage.setTag(1);
                                        withBookmark.setVisibility(View.GONE);
                                        withoutBookmark.setVisibility(View.VISIBLE);
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });

                        //profile
                        ImageView profileImage = (ImageView) view.findViewById(R.id.share_details);
                        profileImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    workidAsync =  filterData.getString("Id");
                                    Log.v("Type", filterData.getString("worktype"));
                                    if (filterData.has("worktype") && filterData.getString("worktype").equals("Doctor")) {
                                        //doctor profile
                                        doctorProfile(workidAsync, doctornameAsync);
                                    } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Hospital")) {
                                        //hospital profile
                                        hospitalProfile(workidAsync, doctornameAsync);
                                    } else if (filterData.has("worktype") && (filterData.getString("worktype").equals("Radiology Lab") ||
                                            filterData.getString("worktype").equals("Medical Lab"))) {
                                        //lab profile
                                        labProfile(workidAsync, bookmarkMipmap, "");
                                    } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Clinic")) {
                                        clinicProfile(workidAsync, bookmarkMipmap, "");
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });

                        //change profile pic
                        ImageView profilePic = (ImageView) view.findViewById(R.id.user_profile_pic);
                        try {
                            Log.v("Type", filterData.getString("worktype"));
                            if (filterData.has("worktype") && filterData.getString("worktype").equals("Doctor")) {
                                //doctor profile pic

                            } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Hospital")) {
                                //hospital profile
                                profilePic.setImageResource(R.mipmap.hospital_dark);
                                user_profile_pic_bookmark.setImageResource(R.mipmap.hospital_dark);
                            } else if (filterData.has("worktype") && (filterData.getString("worktype").equals("Radiology Lab") ||
                                    filterData.getString("worktype").equals("Medical Lab"))) {
                                //lab profile
                                profilePic.setImageResource(R.mipmap.lab_dark);
                                user_profile_pic_bookmark.setImageResource(R.mipmap.lab_dark);
                            } else if (filterData.has("worktype") && filterData.getString("worktype").equals("Clinic")) {
                                //clinicProfile(clinicId, bookmarkMipmap, "");
                                profilePic.setImageResource(R.mipmap.clinic_dark);
                                user_profile_pic_bookmark.setImageResource(R.mipmap.clinic_dark);
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //searchKeyword.setVisibility(View.GONE);
            //mapLocation.setVisibility(View.GONE);
            //topTenDoctorText.setVisibility(View.VISIBLE);
            //Preference.setValue(getContext(), "TOP_TEN_DOCTORS", "");
        }
    }

    /**
     * Open phone dialer
     * @param callDialer string
     */
    public void openPhoneDialer(String callDialer) {
        android.support.v7.app.AlertDialog.Builder builderSingle = new android.support.v7.app.AlertDialog.Builder(getContext());
        builderSingle.setIcon(R.mipmap.call);
        builderSingle.setTitle(R.string.select_one_number);

        String search = "--";
        String search1 = "-";
        String[] array = new String[10];
        if (callDialer.indexOf(search) != -1) {
            array = callDialer.replaceAll(" ", "").split("\\--", -1);
        } else if (callDialer.indexOf(search1) != -1) {
            array = callDialer.replaceAll(" ", "").split("\\-", -1);
        }

        if (array.length == 10) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + callDialer.trim()));
            startActivity(intent);
        } else {
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
            arrayAdapter.addAll(array);

            builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + strName));
                    startActivity(intent);

                }
            });
            builderSingle.show();
        }
    }

    /**
     * doctor profile
     * @param doctorId string
     * @param doctorName string
     */
    public void doctorProfile(final String doctorId, final String doctorName) {
        Bundle args = new Bundle();
        args.putString("id", doctorId);
        Log.v("id", doctorId);
        Log.v("name", doctorName);
        args.putString("name", doctorName);
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        DoctorProfileFragment clinicProfileFragment = new DoctorProfileFragment();
        clinicProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("doctorProfile");
        mFragmentTransaction.add(R.id.search_layout, clinicProfileFragment).commit();
    }

    /**
     * clinic profile
     * @param id string
     * @param mipmap string
     * @param speciality string
     */
    public void clinicProfile(String id, String mipmap, String speciality) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("mipmap", mipmap);
        args.putString("speciality", speciality);

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        ClinicProfileFragment clinicProfileFragment = new ClinicProfileFragment();
        clinicProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("clinicProfile");
        mFragmentTransaction.replace(R.id.search_layout, clinicProfileFragment).commit();
    }

    /**
     * hospital profile
     * @param id string
     * @param name string
     */
    public void hospitalProfile(final String id, final String name) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("hospitalName", name);

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        HospitalProfileFragment doctorprofile = new HospitalProfileFragment();
        doctorprofile.setArguments(args);

        mFragmentTransaction.addToBackStack("hospitalProfile");
        mFragmentTransaction.replace(R.id.search_layout, doctorprofile).commit();
    }

    /**
     * lab profile
     * @param id string
     * @param mipmap string
     * @param speciality string
     */
    public void labProfile(String id, String mipmap, String speciality) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("mipmap", mipmap);
        args.putString("speciality", speciality);

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        LabProfileFragment labProfileFragment = new LabProfileFragment();
        labProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("labProfile");
        mFragmentTransaction.replace(R.id.search_layout, labProfileFragment).commit();
    }

    class DoctorBookmarksAddUpdate extends AsyncTask<String, Void, String> {
        String URL = AppConfig.getWebServiceUrl() + "bookmark/add";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");
        String quickUserId = Preference.getValue(getContext(), "LOGIN_ID", "");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("doctorId", workidAsync));
            nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));

            try {
                Log.v("Bookmark URL", URL);
                Log.v("namevalue", nameValuePairs.toString());
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String responseResult = jsonObject.getString("result");
                Log.v("Result", responseResult);
                String doctorName = jsonObject.getString("name");

                if(responseResult.equals("Bookmarked") || responseResult.equals("Bookmark is added")) {
                    //addToBookmarks.setImageResource(R.mipmap.lab_dark);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "true");
                    Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmarkd_add), Toast.LENGTH_SHORT).show();
                } else {
                    //addToBookmarks.setImageResource(R.mipmap.bookmarks);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "false");
                    Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmark_remove), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            //hideProgrssBar();
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
            //showProgressBar();
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
        }
    }

    class WorkTypeBookmark extends AsyncTask<String, Void, String> {
        final String URL = AppConfig.getWebServiceUrl() + "Worktype_bookmark/Add";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            Log.v("bookmark", "worktype");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("workId", workidAsync));
            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.v("result", result.toString());
            try {
                JSONObject jsonObject = new JSONObject(result);
                String responseResult = jsonObject.getString("result");
                String doctorName = jsonObject.getString("name");

                if(responseResult.equals("Bookmarked")) {
                    //addToBookmarks.setImageResource(R.mipmap.lab_dark);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "true");
                    Toast.makeText(getContext(), doctorName + " is successfully added in your bookmark.", Toast.LENGTH_SHORT).show();
                } else if(responseResult.equals("Bookmark is added")) {
                    Toast.makeText(getContext(), doctorName + " is successfully added in your bookmark.", Toast.LENGTH_SHORT).show();
                } else {
                    //addToBookmarks.setImageResource(R.mipmap.bookmarks);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "false");
                    Toast.makeText(getContext(), doctorName + " is successfully removed from your bookmark.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        userBookmarkList.cancel(true);
    }

    public void lowDensityLayout()
    {

        int j=1;
        for (String specialityMipmapName : resarray) {

            if(j > 3)
                break;

            View mipmapIcon;
            LayoutInflater mipmapIconInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            mipmapIcon = mipmapIconInflater.inflate(R.layout.mipmap_icon, null);

            ImageView mipmapSpec = (ImageView) mipmapIcon.findViewById(R.id.mipmapIcon);

//         Toast.makeText(getContext(),"Dpi: "+getResources().getDisplayMetrics().densityDpi,Toast.LENGTH_SHORT).show();

            switch (getResources().getDisplayMetrics().densityDpi) {


                /**
                 * https://en.wikipedia.org/wiki/Pixel_density
                 * Named pixel densities
                 * DENSITY_280 - xhdpi
                 * DENSITY_360 - xxhdpi
                 * DENSITY_400 - xxhdpi
                 * DENSITY_420 - xxhdpi
                 * DENSITY_560 - xxxhdpi
                 */

                case DisplayMetrics.DENSITY_280:{
                    mipmapSpec.getLayoutParams().height=55;
                    mipmapSpec.getLayoutParams().width=60;
                }
                break;


                case DisplayMetrics.DENSITY_360:{
                    mipmapSpec.getLayoutParams().height=90;
                    mipmapSpec.getLayoutParams().width=90;
                }
                break;

                case DisplayMetrics.DENSITY_400:{
                    mipmapSpec.getLayoutParams().height=90;
                    mipmapSpec.getLayoutParams().width=90;
                }
                break;

                case DisplayMetrics.DENSITY_420:{
                    mipmapSpec.getLayoutParams().height=90;
                    mipmapSpec.getLayoutParams().width=90;
                }
                break;


                case DisplayMetrics.DENSITY_560:{
                    mipmapSpec.getLayoutParams().height=120;
                    mipmapSpec.getLayoutParams().width=120;
                }
                break;

                case DisplayMetrics.DENSITY_LOW:{
                    mipmapSpec.getLayoutParams().height=20;
                    mipmapSpec.getLayoutParams().width=20;
                }
                break;

                case DisplayMetrics.DENSITY_MEDIUM:{
                    mipmapSpec.getLayoutParams().height=30;
                    mipmapSpec.getLayoutParams().width=30;
                }
                break;

                case DisplayMetrics.DENSITY_HIGH:{
                    mipmapSpec.getLayoutParams().height=30;
                    mipmapSpec.getLayoutParams().width=30;
                }

                break;

                case DisplayMetrics.DENSITY_XHIGH:{
                    mipmapSpec.getLayoutParams().height=55;
                    mipmapSpec.getLayoutParams().width=55;
                }

                break;

                case DisplayMetrics.DENSITY_XXHIGH:{
                    mipmapSpec.getLayoutParams().height=70;
                    mipmapSpec.getLayoutParams().width=70;
                }

                break;

                case DisplayMetrics.DENSITY_XXXHIGH:{
                    mipmapSpec.getLayoutParams().height=120;
                    mipmapSpec.getLayoutParams().width=120;
                }
                break;

            }

            mipmapSpec.setScaleType(ImageView.ScaleType.FIT_XY);

            int mipmapId = getContext().getResources().getIdentifier(specialityMipmapName.toString().trim()
                    , "mipmap", getContext().getPackageName());
            //ImageView spec = new ImageView(getContext());

//            mipmapIcon = resizeView(mipmapIcon);

            if(mipmapId == 0)
                mipmapSpec.setVisibility(View.GONE);
            else
                mipmapSpec.setImageResource(mipmapId);
//         mipmapSpec.setImageResource(R.mipmap.face24);

            //int dp = (int) (getContext().getResources().getDimension(R.dimen.doctor_speciality_icon_width_height) / getContext().getResources().getDisplayMetrics().density);

            specialityLayout.addView(mipmapIcon);
            j++;
        }

    }


}
