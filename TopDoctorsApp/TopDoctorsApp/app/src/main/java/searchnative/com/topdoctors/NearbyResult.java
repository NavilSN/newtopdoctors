package searchnative.com.topdoctors;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by root on 25/11/16.
 */

public class NearbyResult extends Fragment implements ScrollViewListener {

    //Location mLastLocation;
    //private GoogleApiClient mGoogleApiClient;
    //private LocationRequest mLocationRequest;
    private ScrollViewExt scrollView;
    double lat = 0.0,lon = 0.0;
    private int totalSearchResult = 0, totalResultRendered = 0, totalRequestNo = 0;
    private boolean isAsynchOn = true;
    NearbyAsynck nearbyAsynck;
    private ProgressBar progressBar;
    private LinearLayout linearLayout;
    private Typeface typeface;
    GuestLogin guestLogin;
    private boolean isGuest;
    private String loginUserId;
    private String callDialerAsync, doctorAddressAsync, doctorRatingAsync, workidAsync;
    DoctorBookmarksAddUpdate doctorBookmarksAddUpdate;
    private TextView profile_detail_name;
    int height = 320;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getContext(), new Crashlytics());
        View view = inflater.inflate(R.layout.nearby_result, container, false);

        return view;
    }

    @Override
    public void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy) {
        // We take the last son in the scrollview
        View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
        int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

        // if diff is zero, then the bottom has been reached
        if (diff == 0) {
            if (totalSearchResult > totalResultRendered) {
                if (isAsynchOn) {
                    isAsynchOn = false;
                    nearbyAsynck = new NearbyAsynck();
                    nearbyAsynck.execute();
                    //Toast.makeText(getContext(), ""+ lat +","+ lon, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        scrollView = (ScrollViewExt) getView().findViewById(R.id.nearbyResultScroll);
        scrollView.setScrollViewListener(this);
        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        linearLayout = (LinearLayout) getView().findViewById(R.id.nearbyResult);
        linearLayout.setVisibility(View.INVISIBLE);
        typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/ExoMedium.otf");
        loginUserId = Preference.getValue(getContext(), "LOGIN_ID", "");
        profile_detail_name = (TextView) getView().findViewById(R.id.profile_detail_name);
        profile_detail_name.setTypeface(typeface);
        guestLogin = new GuestLogin();
        isGuest = guestLogin.getGuestLogin(getContext());

        ImageView imageView = (ImageView) getView().findViewById(R.id.nearBySwipeMenu);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawerLayout);
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        totalRequestNo = 0;
        totalResultRendered = 0;
        totalSearchResult = 0;
        CustomSearchHandle.resetNearbyPaginate(getContext());
        nearbyAsynck = new NearbyAsynck();
        nearbyAsynck.execute();
    }

    class NearbyAsynck extends AsyncTask<String, Void, String> {
        String URL = AppConfig.getWebServiceUrl() + "doctor/nearByDoctors";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<>();

            nameValuePairs.add(new BasicNameValuePair("lat",
                    Preference.getValue(getContext(), "Latitude", "")));
            nameValuePairs.add(new BasicNameValuePair("long",
                    Preference.getValue(getContext(), "Longitude", "")));

            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("lang", LocalInformation.getLocaleLang()));
            nameValuePairs.add(new BasicNameValuePair("page", String.valueOf(totalRequestNo)));

            try {
                HttpPost httpPost = new HttpPost(URL);
                Log.v("URL", URL);
                Log.v("values:",nameValuePairs+"");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if(result != null){
                new NearbyAsynckRender(result);
            }else{
                if(getContext() != null){
                    Toast.makeText(getContext(), getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            isAsynchOn = true;
            progressBar.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            totalRequestNo++;
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    class NearbyAsynckRender {
        NearbyAsynckRender(String result) {
            Log.v("Inside render", result);
            //remove all result at first request
            if (totalRequestNo <= 1)
                linearLayout.removeAllViews();
            //Log.v("request", String.valueOf(totalRequestNo));
            //totalRequestNo++;
            linearLayout.setGravity(Gravity.START);

            if(getActivity() != null){
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                 height = display.getHeight();
                 Log.d("height,","height"+height);
            }


            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, height
            );
           /* LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            );*/
            try {
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.no_doctor_found));
//                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL);
                    textView.setTypeface(typeface);
                    textView.setTextColor(getResources().getColor(R.color.black));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    //set counter
                    totalSearchResult = 0;
                    totalResultRendered = 0;

                    linearLayout.addView(textView);
                } else {
                    JSONArray searchResult = jsonObject.getJSONArray("result");
                    if (totalRequestNo <= 1)
                        totalSearchResult = jsonObject.getInt("total_rows");
                    Log.v("total rows", String.valueOf(totalSearchResult));
                    for(int i = 0; i < searchResult.length(); i++) {
                        final JSONObject filterData = searchResult.getJSONObject(i);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.search_result, null);

                        //speciality icon
                       ImageView doctorSpecialityIcon = (ImageView) view.findViewById(R.id.speciality);
                        int id = getContext().getResources().getIdentifier(filterData.getString("Mipmap"),
                                "mipmap", getContext().getPackageName());
                        doctorSpecialityIcon.setImageResource(id);

                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.search_title_work_place);
                        textView1.setText(filterData.getString("Name"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.search_work_place_address);
                        textView2.setText(filterData.getString("Address"));
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.search_work_place_phone);
                        textView3.setText(filterData.getString("Phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        //total reviews
                        TextView textView4 = (TextView) view.findViewById(R.id.search_total_reviews);
                        if(!filterData.getString("TotalReview").equals("null") &&
                                !filterData.getString("TotalReview").isEmpty())
                            textView4.setText(filterData.getString("TotalReview") + " " + getResources().getString(R.string.total_reviews));
                        else
                            textView4.setText(0 + " " + getResources().getString(R.string.total_reviews));
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));

                        //rating
                        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.doctor_user_rating);
                        if(!filterData.getString("RatingAvg").equals("null") &&
                                !filterData.getString("RatingAvg").isEmpty())
                            ratingBar.setRating(Float.valueOf(filterData.getString("RatingAvg")));

                        linearLayout.addView(view);

                        callDialerAsync = filterData.getString("Phone");
                        doctorAddressAsync = filterData.getString("Address");
                        doctorRatingAsync = String.valueOf(filterData.getString("RatingAvg"));

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.search_phone_dialer);
                        //imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialerAsync);
                            }
                        });

                        //doctor details
                        final String doctorId =  filterData.getString("DoctorId");
                        final String doctorName =  filterData.getString("Name");
                        LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                        //showDetails.setId(getId() + i);
                        showDetails.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                doctorProfile(doctorId, doctorName);
                            }
                        });

                        //social share
                        ImageView socialShare = (ImageView) view.findViewById(R.id.share_details);
                        socialShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //shareIt(doctorName, callDialer, doctorAddress, doctorRating);
                                doctorProfile(doctorId, doctorName);
                            }
                        });

                        //bookmark
                        final LinearLayout withBookmark = (LinearLayout) view.findViewById(R.id.withBookmark);
                        final LinearLayout withoutBookmark = (LinearLayout) view.findViewById(R.id.withoutBookmark);
                        final ImageView addToBookmarks = (ImageView) view.findViewById(R.id.add_to_bookmarks);
                        addToBookmarks.setTag(1);
                        String bookmarkStatus = filterData.getString("BookmarkStatus");
                        if ( ! (bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                            String bookmarkUserId = filterData.getString("BookmarkUserId");
                            if (bookmarkUserId.equals(loginUserId) && bookmarkStatus.equals("1")) {
                                addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                addToBookmarks.setTag(2);
                                withBookmark.setVisibility(View.VISIBLE);
                                withoutBookmark.setVisibility(View.GONE);
                            }
                        }

                        addToBookmarks.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (isGuest) {
                                    guestLogin.confirmAlert(getContext());
                                    return;
                                }
                                workidAsync = doctorId;
                                doctorBookmarksAddUpdate = new DoctorBookmarksAddUpdate();
                                doctorBookmarksAddUpdate.execute();
                                if (Integer.parseInt(addToBookmarks.getTag().toString()) == 1) {
                                    addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                    addToBookmarks.setTag(2);
                                    withBookmark.setVisibility(View.VISIBLE);
                                    withoutBookmark.setVisibility(View.GONE);
                                } else {
                                    addToBookmarks.setImageResource(R.mipmap.bookmarks);
                                    addToBookmarks.setTag(1);
                                    withBookmark.setVisibility(View.GONE);
                                    withoutBookmark.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }
                    totalResultRendered += searchResult.length();
                    Log.v("total rendered", String.valueOf(totalResultRendered));
                    //Log.v("Lat & Long: ", latitudeList.toString() + longitudeList.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
            //searchKeyword.setVisibility(View.VISIBLE);
            //mapLocation.setVisibility(View.VISIBLE);
            //topTenDoctorText.setVisibility(View.GONE);
        }
    }

    /**
     * Open phone dialer
     * @param callDialer string
     */
    public void openPhoneDialer(String callDialer) {
        android.support.v7.app.AlertDialog.Builder builderSingle = new android.support.v7.app.AlertDialog.Builder(getContext());
        builderSingle.setIcon(R.mipmap.call);
        builderSingle.setTitle(R.string.select_one_number);

        String search = "--";
        String search1 = "-";
        String[] array = new String[10];
        if (callDialer.indexOf(search) != -1) {
            array = callDialer.replaceAll(" ", "").split("\\--", -1);
        } else if (callDialer.indexOf(search1) != -1) {
            array = callDialer.replaceAll(" ", "").split("\\-", -1);
        }

        if (array.length == 10) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + callDialer.trim()));
            startActivity(intent);
        } else {
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
            arrayAdapter.addAll(array);

            builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + strName));
                    startActivity(intent);

                }
            });
            builderSingle.show();
        }
    }

    /**
     * doctor profile
     * @param doctorId string
     * @param doctorName string
     */
    public void doctorProfile(final String doctorId, final String doctorName) {
        Bundle args = new Bundle();
        args.putString("id", doctorId);
        Log.v("id", doctorId);
        Log.v("name", doctorName);
        args.putString("name", doctorName);
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        DoctorProfileFragment clinicProfileFragment = new DoctorProfileFragment();
        clinicProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("doctorProfile");
        mFragmentTransaction.add(R.id.search_layout, clinicProfileFragment).commit();
    }

    class DoctorBookmarksAddUpdate extends AsyncTask<String, Void, String> {
        String URL = AppConfig.getWebServiceUrl() + "bookmark/add";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");
        String quickUserId = Preference.getValue(getContext(), "LOGIN_ID", "");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("doctorId", workidAsync));
            nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
            nameValuePairs.add(new BasicNameValuePair("lang", LocalInformation.getLocaleLang()));

            try {
                Log.v("Bookmark URL", URL);
                Log.v("namevalue", nameValuePairs.toString());
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String responseResult = jsonObject.getString("result");
                Log.v("Result", responseResult);
                String doctorName = jsonObject.getString("name");

                if(responseResult.equals("Bookmarked") || responseResult.equals("Bookmark is added")) {
                    //addToBookmarks.setImageResource(R.mipmap.lab_dark);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "true");
                    Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmarkd_add), Toast.LENGTH_SHORT).show();
                } else {
                    //addToBookmarks.setImageResource(R.mipmap.bookmarks);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "false");
                    Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmark_remove), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            //hideProgrssBar();
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
            //showProgressBar();
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
        }
    }
}
