package searchnative.com.topdoctors;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.media.Rating;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.vision.text.Line;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

/**
 * Created by root on 10/10/16.
 */

public class Reviews extends Fragment {

    final String WEB_SERVICE_URL = AppConfig.getWebServiceUrl();
    private ProgressDialog mProgressDialog;
    private Typeface typeface;
    private TextView profileTitleName, reviewText, writeReviewReputation, totalReputationReview, writeReviewClinicAccessibilityText,
            totalReviewClinicAccessibility, availabilityInEmergenciesText, totalAvailibilityInEmergencies, writeReviewApproachabilityText,
            totalReviewApprochability, writeReviewTechnologyAndEquipmentText, totalReviewTechnolotyAndEquipment,
            writeYourReviewText;
    private LinearLayout writeYourReview, userReviewList;
    private RatingBar reputationRatingBar, clinicRatingBar, availabilityRatingBar, approachabilityRatingBar,
            technologyRatingBar;
    private String id, profileName;
    private String appLang = LocalInformation.getLocaleLang();
    private ProgressBar progressBar;
    private LinearLayout doctorAllReviews;
    private Set<String> set = new HashSet<String>();
    private List iconList = new ArrayList();
    GuestLogin guestLogin;
    private boolean isGuest;
    private int totalReviewCount = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getContext(), new Crashlytics());
        View view = inflater.inflate(R.layout.reviews, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //final String hospitalName = getArguments().getString("hospitalName");

        //progressbar
        guestLogin = new GuestLogin();
        isGuest = guestLogin.getGuestLogin(getContext());

        doctorAllReviews = (LinearLayout) getView().findViewById(R.id.doctorAllReviews);
        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        doctorAllReviews.setVisibility(View.INVISIBLE);

        id = getArguments().getString("id");
        profileName = getArguments().getString("name");

        //fetch reviews
        fetchDoctorReview(id);

        typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/ExoMedium.otf");

        ImageView imageView = (ImageView) getView().findViewById(R.id.reviews_swipe_menu);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawerLayout);
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        profileTitleName = (TextView) getView().findViewById(R.id.reviews_profile_title);
        reviewText = (TextView) getView().findViewById(R.id.reviews_text);
        writeReviewReputation = (TextView) getView().findViewById(R.id.write_review_reputation);
        totalReputationReview = (TextView) getView().findViewById(R.id.total_reputation_review);
        writeReviewClinicAccessibilityText = (TextView) getView().findViewById(R.id.write_review_clinic_accessibility);
        totalReviewClinicAccessibility = (TextView) getView().findViewById(R.id.total_review_clinic_accessibility);
        availabilityInEmergenciesText = (TextView) getView().findViewById(R.id.write_review_availability_in_emergencies);
        totalAvailibilityInEmergencies = (TextView) getView().findViewById(R.id.total_availibility_in_emergencies);
        writeReviewApproachabilityText = (TextView) getView().findViewById(R.id.write_review_approachability);
        totalReviewApprochability = (TextView) getView().findViewById(R.id.total_review_approachability);
        writeReviewTechnologyAndEquipmentText = (TextView) getView().findViewById(R.id.write_review_technology_and_equipment);
        totalReviewTechnolotyAndEquipment = (TextView) getView().findViewById(R.id.total_review_technology_and_equipment);
        //totalReviews = (TextView) getView().findViewById(R.id.total_reviews);
        writeYourReviewText = (TextView) getView().findViewById(R.id.write_your_review_text);
        reputationRatingBar = (RatingBar) getView().findViewById(R.id.reputation_rating);
        clinicRatingBar = (RatingBar) getView().findViewById(R.id.clinic_ratingbar);
        availabilityRatingBar = (RatingBar) getView().findViewById(R.id.availability_ratingbar);
        approachabilityRatingBar = (RatingBar) getView().findViewById(R.id.approachability_ratingbar);
        technologyRatingBar = (RatingBar) getView().findViewById(R.id.technology_ratingbar);

        profileTitleName.setText(profileName);

        /*Toast.makeText(getContext(), hospitalName, Toast.LENGTH_LONG).show();*/
        profileTitleName.setTypeface(typeface);
        reviewText.setTypeface(typeface);
        writeReviewReputation.setTypeface(typeface);
        totalReputationReview.setTypeface(typeface);
        writeReviewClinicAccessibilityText.setTypeface(typeface);
        totalReviewClinicAccessibility.setTypeface(typeface);
        availabilityInEmergenciesText.setTypeface(typeface);
        totalAvailibilityInEmergencies.setTypeface(typeface);
        writeReviewApproachabilityText.setTypeface(typeface);
        totalReviewApprochability.setTypeface(typeface);
        writeReviewTechnologyAndEquipmentText.setTypeface(typeface);
        totalReviewTechnolotyAndEquipment.setTypeface(typeface);
        //totalReviews.setTypeface(typeface);
        writeYourReviewText.setTypeface(typeface);

        //write your review
        writeYourReview = (LinearLayout) getView().findViewById(R.id.write_your_review);
        writeYourReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isGuest) {
                    guestLogin.confirmAlert(getContext());
                    return;
                }
                Intent intent = new Intent(getContext(), WriteReviewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        //review list
        userReviewList = (LinearLayout) getView().findViewById(R.id.doctor_review_list_layout);
    }

    /**
     * Fetch doctor reviews
     * @param doctorId string
     */
    public void fetchDoctorReview(final String doctorId) {

        class DoctorReview extends AsyncTask<String, Void, String> {

            String URL = WEB_SERVICE_URL + "review/reviewRating";

            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            String quickDoctorId = doctorId;

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("doctorId", quickDoctorId));
                nameValuePairs.add(new BasicNameValuePair("lang", appLang));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    return mClient.execute(httpPost, responseHandler);
                } catch(IOException e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/

                setRatings(result);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();*/
            }

        }

        DoctorReview doctorReview = new DoctorReview();
        doctorReview.execute(doctorId);

    }

    public void setRatings(String result) {
        try {
            //main rating object
            JSONObject mainRatingObject = new JSONObject(result);
            JSONObject ratingOnject = mainRatingObject.getJSONObject("data");

            //Reputation
            JSONObject reputationObject = ratingOnject.getJSONObject("reputation");
            if(reputationObject.has("avg") && (! reputationObject.getString("avg").equals("null")))
            {
                reputationRatingBar.setRating(Float.valueOf(reputationObject.getString("avg")));
            }
            else {
                Log.v("Reputation Avg is Null","Avg is null.");
                reputationRatingBar.setRating(0.0f);
            }


           // reputationRatingBar.setRating(Float.valueOf(reputationObject.getString("avg")));
            totalReputationReview.setText(reputationObject.getString("count"));

            //clinic
            JSONObject clinicObject = ratingOnject.getJSONObject("clinic");

            if(clinicObject.has("avg") && (! clinicObject.getString("avg").equals("null")))
            {
                clinicRatingBar.setRating(Float.valueOf(clinicObject.getString("avg")));
            }
            else {
                Log.v("Clinic Avg is Null","Avg is null.");
                clinicRatingBar.setRating(0.0f);

            }

            //clinicRatingBar.setRating(Float.valueOf(clinicObject.getString("avg")));
            totalReviewClinicAccessibility.setText(clinicObject.getString("count"));

            //availability
            JSONObject availabilityObject = ratingOnject.getJSONObject("availability");
            availabilityRatingBar.setRating(Float.valueOf(availabilityObject.getString("avg")));
            totalAvailibilityInEmergencies.setText(availabilityObject.getString("count"));

            //approachability
            JSONObject approachabilityObject = ratingOnject.getJSONObject("approachability");

            if(approachabilityObject.has("avg") && (! approachabilityObject.getString("avg").equals("null")))
            {
                approachabilityRatingBar.setRating(Float.valueOf(approachabilityObject.getString("avg")));
            }
            else {
                Log.v("approachabilityRating","Avg is null");
                approachabilityRatingBar.setRating(0.0f);

            }


            //approachabilityRatingBar.setRating(Float.valueOf(approachabilityObject.getString("avg")));
            totalReviewApprochability.setText(approachabilityObject.getString("count"));

            //technology
            JSONObject technologyObject = ratingOnject.getJSONObject("technology");


            if(technologyObject.has("avg") && (! technologyObject.getString("avg").equals("null")))
            {
                technologyRatingBar.setRating(Float.valueOf(technologyObject.getString("avg")));
            }
            else {
                Log.v("technologyRatingbar","Avg is null.");
                technologyRatingBar.setRating(0.0f);

            }

            //technologyRatingBar.setRating(Float.valueOf(technologyObject.getString("avg")));
            totalReviewTechnolotyAndEquipment.setText(technologyObject.getString("count"));

            //grab all reviews
            getAllUserReview(id);
            Log.v("doctor id", String.valueOf(id));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAllUserReview(final String userId) {

        class UserDoctorReviews extends AsyncTask<String, Void, String> {

            final String URL = AppConfig.getWebServiceUrl() + "review/ratingUserDetail";
            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");
            String quickUserId = userId;

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("doctorId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("lang", appLang));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    return mClient.execute(httpPost, responseHandler);
                } catch(IOException e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.v("Result review", result);
                new RenderAllReviews(result);
                //new SearchFragment.RenderLabSearchResult(result);
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
                progressBar.setVisibility(View.GONE);
                doctorAllReviews.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                /*mProgressDialog = new ProgressDialog(getContext());
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();*/
            }

        }

        UserDoctorReviews userDoctorReviews = new UserDoctorReviews();
        userDoctorReviews.execute(userId);
    }

    class RenderAllReviews {
        RenderAllReviews(String result) {
            userReviewList.removeAllViews();
            userReviewList.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            );
            try {
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.no_data_found));
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    userReviewList.addView(textView);
                } else {
                    JSONArray searchResult = jsonObject.getJSONArray("data");
                    totalReviewCount = searchResult.length();
                    for(int i = 0; i < searchResult.length(); i++) {
                        final JSONObject filterData = searchResult.getJSONObject(i);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.doctor_review_list, null);

                        //user name
                        TextView textView1 = (TextView) view.findViewById(R.id.user_name);
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));
                        if (filterData.has("visible") && filterData.getString("visible").equals("1")) {
                            textView1.setText(filterData.getString("name"));
                        } else {
                            textView1.setText(getResources().getString(R.string.anonymous));
                        }
                        /*TextView textView1 = (TextView) view.findViewById(R.id.user_name);
                        textView1.setText(filterData.getString("name"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));*/

                        //date time
                        TextView commentTime = (TextView) view.findViewById(R.id.review_date_time);
                        commentTime.setText(filterData.getString("commentTime"));

                        //user rating
                        RatingBar userRating = (RatingBar) view.findViewById(R.id.user_review);
                        userRating.setRating(Float.valueOf(filterData.getString("avgRating")));

                        //user comment
                        TextView userComment = (TextView) view.findViewById(R.id.user_review_text);
                        userComment.setText(filterData.getString("comment"));

                        //share review
                        TextView shareReview = (TextView) view.findViewById(R.id.share_review);
                        shareReview.setTypeface(typeface, typeface.BOLD);

                        shareReview.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    shareIt(filterData.getString("name"), "", "", filterData.getString("avgRating"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        //photo and video
                        ImageView photoAndVideo = (ImageView) view.findViewById(R.id.photoAndVideo);
                        photoAndVideo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(getContext(), PhotoOrVideoActivity.class));
                            }
                        });

                        userReviewList.addView(view);

                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    if (filterData.getString("visible").equals("1")) {
                                        String userID = filterData.getString("userId");
                                        userReviewDetails(filterData.getString("review_id"),
                                                filterData.getString("name"));
                                    } else {
                                        Toast.makeText(getContext(),
                                                getResources().getString(R.string.anonymous_profile_alert_message), Toast.LENGTH_LONG).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });

                        ImageView profilePic = (ImageView) view.findViewById(R.id.user_profile_pic);
                        if (filterData.has("photo")) {
                            Picasso.with(getContext())
                                    .load(filterData.getString("photo"))
                                    .resize(50, 50)
                                    .centerCrop()
                                    .into(profilePic);
                            //new DownloadImageTask(profilePic).execute(filterData.getString("photo"));
                        } else {
                            profilePic.setImageResource(R.mipmap.first_name);
                        }

                        //review icon
                        ImageView reviewIconImage = (ImageView) view.findViewById(R.id.reviewIcon);
                        if (filterData.has("reviewIcon")) {
                            final String reviewIcon = filterData.getString("reviewIcon");
                            if (!reviewIcon.equals("0")) {
                                int iconId = getResources().getIdentifier("doctor_" + reviewIcon,
                                        "drawable", getContext().getPackageName());
                                reviewIconImage.setImageResource(iconId);
                                iconList.add("doctor_" + reviewIcon);
                            }

                        }
                    }
                }
                set.addAll(iconList);
                Log.v("all set", set.toString());
                if (iconList.size() > 0) {
                    //show table row
                    TableRow tableRow = (TableRow) getView().findViewById(R.id.iconsTableRow);
                    tableRow.setVisibility(View.VISIBLE);
                    int dp = -20;
                    for (String temp : set) {
                        int id = getContext().getResources().getIdentifier(temp, "drawable",
                                getContext().getPackageName());
                        int imageid = getContext().getResources().getIdentifier(temp, "id",
                                getContext().getPackageName());
                        ImageView imageView = (ImageView) getView().findViewById(imageid);
                        imageView.setImageResource(id);
                        imageView.setVisibility(View.VISIBLE);
                        float d = getContext().getResources().getDisplayMetrics().density;
                        int margin = (int) (dp * d);
                        imageView.setPadding(margin, 0, margin, 0);
                        dp += 30;
                    }
                    TextView textView = (TextView) getView().findViewById(R.id.totalIconCount);
                    textView.setText(String.valueOf(totalReviewCount));
                    textView.setTypeface(typeface);
                    textView.setVisibility(View.VISIBLE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.v("Review", "resume");
    }

    public void userProfile(final String userId) {
        Bundle args = new Bundle();
        args.putString("id", userId);
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        UserProfileFragment profileFragment = new UserProfileFragment();
        profileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("doctorProfile");
        mFragmentTransaction.add(R.id.search_layout, profileFragment).commit();
    }

    public void shareIt(final String name, final String phone, final String address, final String rating) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Name: " + name + "\n" + "Phone: " + phone + "\n" + "Address: "
                + address + "\n" + "Rating: " + rating);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share via"));

        /*ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentTitle("Game Result Highscore")
                .setContentDescription("My new highscore is " + sum.getText() + "!!")
                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=de.ginkoboy.flashcards"))

                //.setImageUrl(Uri.parse("android.resource://de.ginkoboy.flashcards/" + R.drawable.logo_flashcards_pro))
                .setImageUrl(Uri.parse("http://bagpiper-andy.de/bilder/dudelsack%20app.png"))
                .build();

        shareDialog.show(linkContent);*/

    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            //navProfilePic.setImageBitmap(result);
            //progressBar.setVisibility(View.GONE);
            //profileDetails.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        progressBar.setVisibility(View.GONE);
    }

    public void userReviewDetails(final String reviewId, final String name) {
        Bundle args = new Bundle();
        args.putString("id", reviewId);
        args.putString("name", name);
        args.putString("type", "doctor");
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        ReviewDetailsFragment reviewDetailsFragment = new ReviewDetailsFragment();
        reviewDetailsFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("reviewDetails");
        mFragmentTransaction.add(R.id.search_layout, reviewDetailsFragment).commit();
    }


}
