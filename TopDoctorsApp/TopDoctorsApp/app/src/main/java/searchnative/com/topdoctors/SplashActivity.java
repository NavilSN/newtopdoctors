package searchnative.com.topdoctors;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,LocationListener {

    private Intent intent;
    private String userLang;

    Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    double lat = 0.0,lon = 0.0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        try{

            if (!isGooglePlayServicesAvailable()) {
                Toast.makeText(SplashActivity.this, "Google Play Service is unavailable", Toast.LENGTH_SHORT).show();
            }

            buildGoogleApiClient();

        }catch (Exception e)
        {
            Log.v("Location Exception",""+e.getMessage());
        }


        setContentView(R.layout.activity_splash);
        Preference.setValue(SplashActivity.this, "resetMenuVisibility","");

        //set lang
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
        //String locale = ims.getLocale();
        //Log.v("Lang", locale);
        //LocalInformation.setLocalLang(locale);

        //set user lang
        userLang = Preference.getValue(getBaseContext(), "APP_LANG", "");
        if(userLang.isEmpty()) {
            setUserLang("en_IN");
        } else {
            setUserLang(userLang);
        }
        Log.v("user lang", userLang);

        //check for Internet connection
        if(!isOnline()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.device_not_connected_to_internet))
                    .setTitle(getResources().getString(R.string.no_internet_connection))
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SplashActivity.this.finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

        } else {
            if(!Preference.getValue(SplashActivity.this, "PREF_FNAME", "").equals("")) {
                Toast.makeText(this, "Welcome back, " + Preference.getValue(SplashActivity.this, "PREF_FNAME", ""),
                        Toast.LENGTH_SHORT).show();
            }


            SpecialityData.getmInstance();
            //CountryData.getmInstance();
            LocationData.getmInstance();

            startThreadDelay();
            /*
            Thread thread = new Thread() {

                @Override
                public void run() {
                    try {
                        sleep(3000);

                        if(SpecialityData.getmInstance().specialityList.size() == 0
                                && CountryData.getmInstance().countryList.size() == 0
                                && LocationData.getmInstance().locationList.size() == 0)
                        {
                            Toast.makeText(SplashActivity.this,"Recursive call",Toast.LENGTH_SHORT).show();
                            run();
                        }
                        Toast.makeText(SplashActivity.this,"Not!!! Recursive",Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    handler.sendEmptyMessage(0);
                }
            };
            thread.start();
            */
        }
    }

    private Handler handler = new Handler(){

        @Override
        public void handleMessage(Message message) {
            super.handleMessage(message);


            if(SpecialityData.getmInstance().specialityList.size() == 0
                    || LocationData.getmInstance().locationList.size() == 0)
            {
//                Toast.makeText(SplashActivity.this,"Recursive call",Toast.LENGTH_SHORT).show();
                startThreadDelay();
                return;
            }
//            Toast.makeText(SplashActivity.this,"Ready",Toast.LENGTH_SHORT).show();



            clearPreference();
            //remove search filters
            CustomSearchHandle.removeAllSearchFilters(SplashActivity.this);
            String username = Preference.getValue(SplashActivity.this, "PREF_FNAME", "");
            setLatLongToPreference();
            if(username.equals("")) {
                intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
            } else {
                intent = new Intent(SplashActivity.this, BaseActivity.class);
                startActivity(intent);
            }
            finish();
        }
    };

    public void clearPreference() {
        Preference.setValue(SplashActivity.this, "GLOBAL_FILTER_TYPE", "");
        Preference.setValue(SplashActivity.this, "DOCTOR_LOCATION_SEARCH", "");
        Preference.setValue(SplashActivity.this, "DOCTOR_SPECIALITY_SEARCH", "");
        Preference.setValue(SplashActivity.this, "DOCTOR_GENDER_SEARCH", "");
        Preference.setValue(SplashActivity.this, "TOP_TEN_DOCTORS", "");
        Preference.setValue(SplashActivity.this, "SEARCH_REDIRECT", "");
        Preference.setValue(SplashActivity.this, "FILTER_NAME", "");
        Preference.setValue(SplashActivity.this, "GLOBAL_FILTER_TYPE", "");
        Preference.setValue(SplashActivity.this, "CLINIC_SEARCH", "");
        Preference.setValue(SplashActivity.this, "HOSPITAL_SEARCH", "");
        Preference.setValue(SplashActivity.this, "DOCTOR_SEARCH_CUSTOM", "");
        Preference.setValue(SplashActivity.this, "DOCTOR_SPECIALITY_SEARCH", "");
        Preference.setValue(SplashActivity.this, "DOCTOR_LOCATION_SEARCH", "");
        Preference.setValue(SplashActivity.this, "DOCTOR_GENDER_SEARCH", "");
        Preference.setValue(SplashActivity.this, "FILTER_LOCATION", "");
        Preference.setValue(SplashActivity.this, "LAB_SEARCH_CUSTOM", "");
        Preference.setValue(SplashActivity.this, "USER_BOOKMARKS", "");
        Preference.setValue(SplashActivity.this, "PRIMARY_SEARCH", "");
        Preference.setValue(SplashActivity.this, "PROMARY_SEARCH_KEY", "");
        Preference.setValue(SplashActivity.this, "SEARCH_REDIRECT", "");
        CustomSearchHandle.resetAllPaginateCounter(SplashActivity.this);
    }

    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        }

        return false;
    }

    private void startThreadDelay()
    {
        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler.sendEmptyMessage(0);
            }
        };
        thread.start();

    }

    public void setUserLang(final String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        //LocaleHelper.onCreate(BaseActivity.this, "ar");
        LocalInformation.setLocalLang(lang);
    }


    /**
     * For Latitude - Longitude
     * @return
     */


    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(100); // Update location every second

        try{

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);


            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            if (mLastLocation != null) {
                lat = mLastLocation.getLatitude();
                lon = mLastLocation.getLongitude();
            }

        }catch (SecurityException e){
            Log.v("SecurityException",""+e.getMessage());
        }
        catch (Exception e){
            Log.v("Exception",""+e.getMessage());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        buildGoogleApiClient();
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lon = location.getLongitude();
    }


    public void setLatLongToPreference()
    {
//        lat = 23.045591; lon = 72.528163;
        if(lat != 0.0 && lon != 0.0) {
            Preference.setValue(SplashActivity.this, "Latitude", String.valueOf(lat));
            Preference.setValue(SplashActivity.this, "Longitude", String.valueOf(lon));
        }
        else{
            Preference.setValue(SplashActivity.this, "Latitude", "");
            Preference.setValue(SplashActivity.this, "Longitude", "");
            Toast.makeText(SplashActivity.this,getResources().getString(R.string.no_gps),Toast.LENGTH_SHORT).show();
        }
    }



}
