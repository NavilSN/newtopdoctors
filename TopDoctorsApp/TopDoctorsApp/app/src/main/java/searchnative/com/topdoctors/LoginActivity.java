package searchnative.com.topdoctors;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity {

    private static String loginUserName;
    private static String loginAddress;

    TextView tvTitle, tvForgotPassword;
    EditText etEmail, etPassword;
    String getEmail, getPassword;
    Button btnLogin, btnRegister, btnGuest, btnFacebook;
    Boolean isValidate = true;
    String resMessage = "";
    String resStatus;
    String webServiceUrl = AppConfig.getWebServiceUrl();
    String DataParseUrl = webServiceUrl + "user/login";
    private String loginId;
    private static final int REQUEST_CAMERA = 0;

    private TextView info;
    private LoginButton loginButton;
    private CallbackManager callbackManager;

    private ProgressDialog mProgressDialog;

    private ProgressBar progressBar;
    GuestLogin guestLogin;

    //fb login
    private String userId, token, email, name, location, profilePic;

    ProgressDialog pd;
    boolean isfromguest=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        FacebookSdk.sdkInitialize(getApplicationContext());

        if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
//          Camera permission has not been granted.
            requestCameraPermission();
        } else {

        }

            getSupportActionBar().hide();
        setContentView(R.layout.activity_login);
        loginUserName = "";
        loginAddress = "";

        tvTitle = (TextView) findViewById(R.id.textViewLogin);
        tvForgotPassword = (TextView) this.findViewById(R.id.textViewForgotPassword);
        etEmail = (EditText) findViewById(R.id.email);
        etPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnRegister = (Button) findViewById(R.id.btn_register);
        btnGuest = (Button) findViewById(R.id.enter_as_a_guest);
        btnFacebook = (Button) findViewById(R.id.login_with_facebook);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        callbackManager = CallbackManager.Factory.create();
        info = (TextView)findViewById(R.id.info);
        loginButton = (LoginButton)findViewById(R.id.login_button);

        //set read permission
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends", "user_location"));

        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {


                // insert in db with isSocial flag and token




                token = loginResult.getAccessToken().getToken();
                userId = loginResult.getAccessToken().getUserId();

                /*info.setText(
                        "User ID: "
                                + loginResult.getAccessToken().getUserId()
                                + "\n" +
                                "Auth Token: "
                                + loginResult.getAccessToken().getToken()
                );*/

                //App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    email = object.getString("email");
                                    name = object.getString("name");
                                   // location = object.getJSONObject("location").toString();
                                    loginButton.setText(getResources().getString(R.string.logout));

                                    progressBar.setVisibility(View.VISIBLE);



                                    if(SpecialityData.mInstance == null){
                                        Showprogress();
                                        SpecialityData.getmInstance();
                                    }


                                    if(LocationData.mInstance == null){
                                        if(pd==null){
                                            Showprogress();
                                        }
                                        LocationData.getmInstance();
                                    }
                                    startThreadDelay();

                                    //insert db
                                    facebookLogin(userId, name, token, email, location);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                );
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,location");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                info.setText("Login attempt canceled.");
            }

            @Override
            public void onError(FacebookException error) {
                info.setText("Login attempt failed.");
            }
        });

        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.performClick();
            }
        });


        //set the font style
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ExoBlack.otf");
        tvTitle.setTypeface(face);
        btnLogin.setTypeface(face);

        //set font style ExoMedium
        Typeface face1 = Typeface.createFromAsset(getAssets(), "fonts/ExoMedium.otf");
        tvForgotPassword.setTypeface(face1);
        etEmail.setTypeface(face1);
        etPassword.setTypeface(face1);
        btnRegister.setTypeface(face1);
        btnGuest.setTypeface(face1);
        btnFacebook.setTypeface(face1);
        loginButton.setTypeface(face1);

        //text transform method
        btnRegister.setTransformationMethod(null);
        btnGuest.setTransformationMethod(null);
        btnFacebook.setTransformationMethod(null);

        btnGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                progressBar.setVisibility(View.VISIBLE);



                if(SpecialityData.mInstance == null || LocationData.mInstance == null){
                    isfromguest=true;
                    Showprogress();
                    SpecialityData.getmInstance();
                    //CountryData.getmInstance();
                    LocationData.getmInstance();
                    startThreadDelay();
                }else{
                    //guest login
                    guestLogin = new GuestLogin();
                    Thread thread = new Thread() {

                        @Override
                        public void run() {
                            try {
                                sleep(2000);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        /*Toast.makeText(LoginActivity.this, "Welcome," + getResources().getString(R.string.guest),
                                Toast.LENGTH_SHORT).show();*/


                                guestLogin.setGuestLogin(LoginActivity.this, "1");
                                startActivity(new Intent(LoginActivity.this, BaseActivity.class));
                            finish();

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.welcome)+ getResources().getString(R.string.guest),
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });


                            //progressBar.setVisibility(View.GONE);
                        }
                    };

                    progressBar.setVisibility(View.VISIBLE);
                    thread.start();

                }

//                if(CountryData.mInstance == null  ){
//                    if(pd==null){
//                        Showprogress();
//                    }
//
//                    CountryData.getmInstance();
//                }
//
//                if(LocationData.mInstance == null){
//                    if(pd==null){
//                        Showprogress();
//                    }
//                    LocationData.getmInstance();
//                }




                                   /*AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);

                    builder.setTitle(getResources().getString(R.string.guest_login))
                            .setMessage(getResources().getString(R.string.coming_soon))
                            .setCancelable(false)
                            .setNegativeButton(getResources().getString(R.string.close),new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();*/
            }
        });

        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {
                    //Toast.makeText(getApplicationContext(), "unfocus", Toast.LENGTH_LONG).show();
                    etEmail.setGravity(Gravity.LEFT);
                    if(etEmail.length() != 0) {
                        etEmail.setSelection(0);
                    }
                }
            }
        });

        //register button click listener
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyborad
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

//                pd=new ProgressDialog(LoginActivity.this);
//                pd.setMessage(getResources().getString(R.string.fetch_data));
//                pd.setCancelable(false);
//                pd.show();



                //check for validation
                checkValidation();
                if(isValidate) {



                    progressBar.setVisibility(View.VISIBLE);



                    if(SpecialityData.mInstance == null){
                        Showprogress();
                        SpecialityData.getmInstance();
                    }



                    if(LocationData.mInstance == null){
                        if(pd==null){
                            Showprogress();
                        }
                        LocationData.getmInstance();
                    }
                    startThreadDelay();






                    getData();
                   // progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable()
                            .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                    btnLogin.setEnabled(false);
                    sendDataToServer(getEmail, getPassword);
                }

            }
        });
    }

    private Bundle getFacebookData(JSONObject object) {
        Bundle bundle = new Bundle();
        try {
            String id = object.getString("id");

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                Log.i("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return bundle;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public static String getLoginUserName() {
        return loginUserName;
    }

    public static void setLoginUserName(String loginUserName1){
        LoginActivity.loginUserName = loginUserName1;
    }

    public static String getLoginAddress() {
        return loginAddress;
    }

    public static void setLoginAddress(String loginAddress1) {
        LoginActivity.loginAddress = loginAddress1;
    }

    public void getData() {
        getEmail = etEmail.getText().toString().trim();
        getPassword = etPassword.getText().toString();
    }

    public void sendDataToServer(final String email, final String password) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                String quickEmail = email;
                String quickPassword = password;

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("email", quickEmail));
                nameValuePairs.add(new BasicNameValuePair("password", quickPassword));

                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(DataParseUrl);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    String responseBody = httpClient.execute(httpPost, responseHandler);
                    JSONObject json = new JSONObject(responseBody);
                    Log.v("Login result", json.toString());
                    resMessage = json.getString("message");
                    resStatus = json.getString("status");
                    profilePic = json.getString("photo");
                    LoginActivity.setLoginUserName(json.getString("first_name") + " " + json.getString("last_name").substring(0,1));
                    if ((json.getString("country").trim().length() > 0) &&
                        (!json.getString("country").equalsIgnoreCase("null")))
                        LoginActivity.setLoginAddress(json.getString("country"));
                    else
                        LoginActivity.setLoginAddress("");
                    loginId = json.getString("id");
                    Log.v("Login Id", loginId);
                } catch (ClientProtocolException e) {

                } catch (IOException e) {

                } catch(JSONException e) {

                } finally {
                }
                return "Login request successfully serve!";
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if(resMessage == "") {
                    resMessage = "Service is unavailable";
                }
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
                progressBar.setVisibility(View.GONE);
                if(pd!=null ){
                    if(pd.isShowing()){
                        pd.cancel();
                    }

                }


                btnLogin.setEnabled(true);
                Toast.makeText(LoginActivity.this, resMessage, Toast.LENGTH_LONG).show();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(resStatus == "true") {
                            Intent intent = new Intent(getBaseContext(), BaseActivity.class);
                            Bundle extras = new Bundle();
                            extras.putString("userName", LoginActivity.getLoginUserName());
                            extras.putString("userAddress", LoginActivity.getLoginAddress());
                            //extras.putString("profilePic", profilePic);

                            //store in sharedpreference
                            Preference.setValue(LoginActivity.this, "PREF_FNAME", LoginActivity.getLoginUserName());
                            Preference.setValue(LoginActivity.this, "PREF_ADDRESS", LoginActivity.getLoginAddress());
                            Preference.setValue(LoginActivity.this, "LOGIN_ID", loginId);
                            Preference.setValue(LoginActivity.this, "PREF_ISLOGIN", "true");
                            Preference.setValue(LoginActivity.this, "USER_TYPE", "LOCAL");
                            Preference.setValue(LoginActivity.this, "USER_ID", "");
                            Preference.setValue(LoginActivity.this, "PROFILE_PIC", profilePic);

                            /*if (!Preference.getValue(LoginActivity.this, "GUEST_LOGIN", "").equals("")) {
                                finish();
                            }*/

                            intent.putExtras(extras);
                            finish();
                            startActivity(intent);
                        }
                    }
                }, 2000);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                /*mProgressDialog = new ProgressDialog(LoginActivity.this, R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();*/
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(email, password);
    }

    public void checkValidation() {
        isValidate = true;
        if(etEmail.getText().toString().trim().length() == 0) {
            etEmail.setError(getResources().getString(R.string.email_error));
            isValidate = false;
        }
        if(etPassword.getText().toString().trim().length() == 0) {
            etPassword.setError(getResources().getString(R.string.password_error));
            isValidate = false;
        }
        //check for valid email
        Pattern pattern1 = Pattern.compile( "^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");
        Matcher matcher1 = pattern1.matcher(etEmail.getText().toString().trim());
        if (!matcher1.matches()) {
            etEmail.setError(getResources().getString(R.string.valid_email_error));
            isValidate = false;
        }
    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {

            /**
             * Nikunj + Jaymin
             * removing Data of Location,Country and Speciality
             */
        try {

            if (TabFragment.timer != null){
                TabFragment.timer.cancel();
            }

            SpecialityData.mInstance = null;
            //CountryData.mInstance = null;
            LocationData.mInstance = null;

            finish(); // finish activity


        }catch (Exception e){
            Log.v("Exception back pressed ",e.getMessage()+"");
        }

        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    public void facebookLogin(final String userId, final String name, final String token,
                              final String email, final String location) {

        class FacebookLogin extends AsyncTask<String, Void, String> {
            final String URL = AppConfig.getWebServiceUrl() + "user/facebookLogin";
            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            String quickUserId = userId;
            String quickName = name;
            String quickToken = token;
            String quickEmail = email;
            String quickLocation = location;

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("name", quickName));
                nameValuePairs.add(new BasicNameValuePair("token", quickToken));
                nameValuePairs.add(new BasicNameValuePair("email", quickEmail));
                nameValuePairs.add(new BasicNameValuePair("location", quickLocation));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();

                    return mClient.execute(httpPost, responseHandler);
                } catch(IOException e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }


                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
                progressBar.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.success_login), Toast.LENGTH_LONG).show();

                try {
                    Log.v("Fb Login response", result);
                    JSONObject jsonObject = new JSONObject(result);

                    //store in sharedpreference
                    Preference.setValue(LoginActivity.this, "PREF_FNAME", jsonObject.getString("name"));
                    Preference.setValue(LoginActivity.this, "PREF_ADDRESS", "India");
                    Preference.setValue(LoginActivity.this, "LOGIN_ID", jsonObject.getString("loginId"));
                    Preference.setValue(LoginActivity.this, "PREF_ISLOGIN", "true");
                    Preference.setValue(LoginActivity.this, "USER_ID", quickUserId);
                    Preference.setValue(LoginActivity.this, "USER_TYPE", "FB");

                    //start new activity
                    if(pd != null){
                        pd.dismiss();
                    }

                    Intent intent = new Intent(LoginActivity.this, BaseActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("userName", jsonObject.getString("name"));
                    extras.putString("userAddress", "India");
                    intent.putExtras(extras);
                    finish();
                    startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(LoginActivity.this, R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();*/
            }
        }

        FacebookLogin facebookLogin = new FacebookLogin();
        facebookLogin.execute(userId, name, token, email, location);
    }






    private Handler handler = new Handler(){

        @Override
        public void handleMessage(Message message) {
            super.handleMessage(message);


            if(SpecialityData.getmInstance().specialityList.size() == 0
                    || LocationData.getmInstance().locationList.size() == 0)
            {
//                Toast.makeText(SplashActivity.this,"Recursive call",Toast.LENGTH_SHORT).show();
                startThreadDelay();
                return;
            }
//            Toast.makeText(LoginActivity.this,"Ready",Toast.LENGTH_SHORT).show();
            if(isfromguest){
                guestLogin = new GuestLogin();
                guestLogin.setGuestLogin(LoginActivity.this, "1");

                if(pd != null){
                    pd.dismiss();
                }
                startActivity(new Intent(LoginActivity.this, BaseActivity.class));
                finish();
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.welcome) + getResources().getString(R.string.guest),
                        Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void startThreadDelay()
    {
        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler.sendEmptyMessage(0);
            }
        };
        thread.start();

    }


    public void Showprogress(){
        if(pd==null){
            pd=new ProgressDialog(LoginActivity.this, R.style.AppCompatAlertDialogStyle);
            pd.setMessage(getResources().getString(R.string.fetch_data));
            pd.setCancelable(false);
            pd.show();
        }
    }

    private void requestCameraPermission() {
        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION))
        {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_CAMERA);
        } else {
            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CAMERA);
        }
        // END_INCLUDE(camera_permission_request)
    }


}
