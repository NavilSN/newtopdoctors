package searchnative.com.topdoctors;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static searchnative.com.topdoctors.R.id.reputation_rating;

public class WriteReviewActivity extends AppCompatActivity
        implements OnClickListener{

    private ImageView closeReview;
    private Typeface typeface;
    private TextView reviewTitle, reputationText, clinicAccessibilityText, availabilityInEmergenciedText,
            approachabilityText, technologyAndEquipmentText, chooseIconForHospitalText, hardToReach,
            attachPhoto, writeReviewTitle;
    private EditText writeReviewEditText, summary;
    private Button submitButton;
    private RatingBar reputationRatingBar, clinicRatingBar, availabilityRatingbar, approachabilityRatingBar,
            technologyRatingBar;
    private String doctorId, userReview, ipAddress, userId, reviewSummary,
            reputationRating, clinicRating, availabilityRating, approachabilityRating, technologyRating;
    final String WEB_SERVICE_URL = AppConfig.getWebServiceUrl();
    private ProgressDialog mProgressDialog;
    private RelativeLayout search_layout, relativeLayout;
    private CheckBox confirmReview, customdrawablecheckboxReview;
    private ProgressBar progressBar;
    private String visible = "1", reviewIcon = "0";

    SimpleTooltip.Builder simpleTooltip;

    private ImageView attachImage;
    private Bitmap bitmapImage;
    private int PICK_IMAGE_REQUEST = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        getSupportActionBar().hide();
        setContentView(R.layout.activity_write_review);

        Intent sender = getIntent();
        Bundle extras = sender.getExtras();

        search_layout = (RelativeLayout) findViewById(R.id.write_a_review_layout);
        //search_layout.setBackgroundColor(Color.WHITE);

        attachImage = (ImageView) findViewById(R.id.attachImage);

        reviewTitle = (TextView) findViewById(R.id.reviews_title);
        reputationText = (TextView) findViewById(R.id.reputation_text);
        clinicAccessibilityText = (TextView) findViewById(R.id.clinic_accessibility_text);
        availabilityInEmergenciedText = (TextView) findViewById(R.id.availability_in_emergencies_text);
        approachabilityText = (TextView) findViewById(R.id.approachability_text);
        technologyAndEquipmentText = (TextView) findViewById(R.id.technology_and_equipment_text);
        chooseIconForHospitalText = (TextView) findViewById(R.id.choose_icon_for_hospital_text);
        writeReviewEditText = (EditText) findViewById(R.id.write_review_edit_text);
        submitButton = (Button) findViewById(R.id.submit_button);
        reputationRatingBar = (RatingBar) findViewById(reputation_rating);
        clinicRatingBar = (RatingBar) findViewById(R.id.clinic_rating);
        availabilityRatingbar = (RatingBar) findViewById(R.id.availability_rating);
        approachabilityRatingBar = (RatingBar) findViewById(R.id.approachability_rating);
        technologyRatingBar = (RatingBar) findViewById(R.id.technology_rating);
        hardToReach = (TextView) findViewById(R.id.hard_to_reach);
        relativeLayout = (RelativeLayout) findViewById(R.id.write_a_review_layout);
        confirmReview = (CheckBox) findViewById(R.id.confirmReview);
        attachPhoto = (TextView) findViewById(R.id.attachPhoto);
        customdrawablecheckboxReview = (CheckBox) findViewById(R.id.customdrawablecheckbox);
        writeReviewTitle = (TextView) findViewById(R.id.write_review_title);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        summary = (EditText) findViewById(R.id.summary);

        typeface = Typeface.createFromAsset(getAssets(), "fonts/ExoMedium.otf");

        reviewTitle.setTypeface(typeface);
        reputationText.setTypeface(typeface);
        clinicAccessibilityText.setTypeface(typeface);
        availabilityInEmergenciedText.setTypeface(typeface);
        approachabilityText.setTypeface(typeface);
        technologyAndEquipmentText.setTypeface(typeface);
        chooseIconForHospitalText.setTypeface(typeface);
        writeReviewEditText.setTypeface(typeface);
        submitButton.setTypeface(typeface);
        hardToReach.setTypeface(typeface);
        attachPhoto.setTypeface(typeface);
        customdrawablecheckboxReview.setTypeface(typeface);
        writeReviewTitle.setTypeface(typeface);
        summary.setTypeface(typeface);

        //close activity
        closeReview = (ImageView) findViewById(R.id.close_write_review);
        closeReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //assign data to variable
        userId = Preference.getValue(WriteReviewActivity.this, "LOGIN_ID", "");
        Log.v("User ID", userId);
        ipAddress = Utils.getIPAddress(true);
        doctorId = extras.getString("id");
        userReview = writeReviewEditText.getText().toString();


        //attach image in write review
        attachImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        //submit review
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //check for Internet
                if ( ! NetworkInfoHelper.isOnline(WriteReviewActivity.this)) {
                    Toast.makeText(WriteReviewActivity.this, getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                    return;
                }
                reputationRating = String.valueOf(reputationRatingBar.getRating());
                clinicRating = String.valueOf(clinicRatingBar.getRating());
                availabilityRating = String.valueOf(availabilityRatingbar.getRating());
                approachabilityRating = String.valueOf(approachabilityRatingBar.getRating());
                technologyRating = String.valueOf(technologyRatingBar.getRating());
                userReview = writeReviewEditText.getText().toString().trim();
                reviewSummary = summary.getText().toString().trim();
                /*Log.v("Reputation", reputationRating);
                Log.v("Clinic", clinicRating);
                Log.v("Availability", availabilityRating);
                Log.v("Approachability", approachabilityRating);
                Log.v("Technology", technologyRating);
                Log.v("Hospital", doctorId);*/
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary),
                        PorterDuff.Mode.MULTIPLY);
                submitButton.setEnabled(false);
                postRating(reputationRating, clinicRating, availabilityRating, approachabilityRating, technologyRating,
                        userReview, userId, ipAddress, doctorId);
                //post rating
                /*LayoutInflater layoutInflater = LayoutInflater.from(WriteReviewActivity.this);
                View promptView = layoutInflater.inflate(R.layout.review_confirm_prompt, null);

                TextView submitText = (TextView) promptView.findViewById(R.id.textView2);
                CheckBox checkBox = (CheckBox) promptView.findViewById(R.id.confirmReview);
                Button button = (Button) promptView.findViewById(R.id.popupSubmitReview);

                submitText.setTypeface(typeface);
                checkBox.setTypeface(typeface);
                button.setTypeface(typeface);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(WriteReviewActivity.this);
                alertDialog.setView(promptView);

                final AlertDialog dialog = alertDialog.create();
                dialog.show();

                final Button submitReview = (Button) promptView.findViewById(R.id.popupSubmitReview);

                submitReview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        postRating(reputationRating, clinicRating, availabilityRating, approachabilityRating, technologyRating,
                                userReview, userId, ipAddress, doctorId);
                        dialog.dismiss();
                    }
                });*/


                /*postRating(reputationRating, clinicRating, availabilityRating, approachabilityRating, technologyRating,
                        userReview, userId, ipAddress, doctorId);*/
                //search_layout.setBackgroundColor(Color.GRAY);
                //startActivity(new Intent(WriteReviewActivity.this, SubmitReviewActivity.class));
                //RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.write_a_review_layout);
                //relativeLayout.setBackgroundColor(Color.GRAY);
            }
        });
    }

    public String getLocalIpAddress(){
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception ex) {
            Toast.makeText(this, "IP Address" + ex.toString(), Toast.LENGTH_LONG).show();
            Log.v("IP Address", ex.toString());
        }
        return null;
    }

    public void postRating(final String reputation, final String clinic, final String availability, final String approachability,
                           final String technology, final String customerReview, final String userId, final String ipAddress,
                           final String doctorId) {

        class PostDoctorRating extends AsyncTask<String, Integer, String> {
            String URL = WEB_SERVICE_URL + "review/add";
            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            String quickReputation = reputation;
            String quickClinic = clinic;
            String quickAvailability = availability;
            String quickApproachability = approachability;
            String quickTechnology = technology;
            String quickCustomerReview = customerReview;
            String quickUserId = userId;
            String quickIpaddress = ipAddress;
            String quickDoctorId = doctorId;

            @Override
            protected String doInBackground(String... params) {
                try {
                    String commentImage = "";
                    if (bitmapImage != null)
                        commentImage = getStringImage(bitmapImage);
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    nameValuePairs.add(new BasicNameValuePair("doctorId", quickDoctorId));
                    nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                    nameValuePairs.add(new BasicNameValuePair("userIp", quickIpaddress));
                    nameValuePairs.add(new BasicNameValuePair("reputation", quickReputation));
                    nameValuePairs.add(new BasicNameValuePair("clinic", quickClinic));
                    nameValuePairs.add(new BasicNameValuePair("availability", quickAvailability));
                    nameValuePairs.add(new BasicNameValuePair("approachability", quickApproachability));
                    nameValuePairs.add(new BasicNameValuePair("technology", quickTechnology));
                    nameValuePairs.add(new BasicNameValuePair("comment", quickCustomerReview));
                    nameValuePairs.add(new BasicNameValuePair("visible", visible));
                    nameValuePairs.add(new BasicNameValuePair("reviewIcon", reviewIcon));
                    //nameValuePairs.add(new BasicNameValuePair("image", commentImage));
                    nameValuePairs.add(new BasicNameValuePair("summary", reviewSummary));

                    Log.v("Name Value pair: ", nameValuePairs.toString());

                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();

                    return mClient.execute(httpPost, responseHandler);
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                // TODO Auto-generated method stub
                progressBar.setProgress(values[0]);
            }

            @Override
            protected void onPostExecute(String result) {
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    Log.v("JSON Object", jsonObject.toString());

                    if(jsonObject.getString("status").equals("true")){
                        setDefaultView();
                    } else if(jsonObject.getString("status").equals("false")
                            && jsonObject.getString("message").equals("You have already rated this Doctor")){
                        setDefaultView();
                    }
                    Toast.makeText(WriteReviewActivity.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
                submitButton.setEnabled(true);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                /*mProgressDialog = new ProgressDialog(WriteReviewActivity.this, R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();*/
            }
        }

        PostDoctorRating postDoctorRating = new PostDoctorRating();
        postDoctorRating.execute(reputation, clinic, availability, approachability, technology, customerReview, userId, ipAddress, doctorId);
    }


   /* @Override
    public void onResume() {
        super.onResume();
        relativeLayout.setBackgroundColor(Color.parseColor("#E6EBEF"));
    }*/

    /*@Override
    public void onClick(View v) {
        *//*String[] imageId = v.getResources().getResourceName(v.getId()).split("/");
        final ImageView imageView = (ImageView) findViewById(v.getId());
        String elementId = imageId[1];
        for (int i = 1; i <= 8; i++) {
            //imageView.setBackgroundResource(0);
            if (elementId.equals("doctor_" + i)) {
                imageView.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle));
            } else {
                int resourceId = this.getResources().getIdentifier("doctor_" + i, "id", this.getPackageName());
                ImageView otherImage = (ImageView) findViewById(resourceId);
                otherImage.setBackgroundResource(0);
            }
        }*//*

        if(!tipWindow.isTooltipShown())
            tipWindow.showToolTip(v);
        }
    }*/

    @Override
    public void onClick(View anchor) {
        String[] imageId = anchor.getResources().getResourceName(anchor.getId()).split("/");
        String[] messages = {"", getResources().getString(R.string.hard_to_reach),
                getResources().getString(R.string.amazing), getResources().getString(R.string.make_home_visits),
                getResources().getString(R.string.great_help), getResources().getString(R.string.angry_doctor),
                getResources().getString(R.string.reachable), getResources().getString(R.string.old_doctor),
                getResources().getString(R.string.like)};
        final ImageView imageView = (ImageView) findViewById(anchor.getId());
        String elementId = imageId[1];
        String iconMessage = "";
        for (int i = 1; i <= 8; i++) {
            //imageView.setBackgroundResource(0);
            if (elementId.equals("doctor_" + i)) {
                imageView.setVisibility(View.GONE);
                int selectedIcon = this.getResources().getIdentifier("doctor_selected_" + i,
                        "id", this.getPackageName());
                ImageView selectedImage = (ImageView) findViewById(selectedIcon);
                selectedImage.setVisibility(View.VISIBLE);
                iconMessage = messages[i];
                reviewIcon = String.valueOf(i);
            } else {
                int resourceId = this.getResources().getIdentifier("doctor_" + i, "id", this.getPackageName());
                int selectedIcon = this.getResources().getIdentifier("doctor_selected_" + i,
                        "id", this.getPackageName());
                ImageView otherImage = (ImageView) findViewById(resourceId);
                ImageView otherSelectedImage = (ImageView) findViewById(selectedIcon);
                otherImage.setVisibility(View.VISIBLE);
                otherSelectedImage.setVisibility(View.GONE);
            }
        }

        //simpaletooltip obj
        simpleTooltip = new SimpleTooltip.Builder(this);
        simpleTooltip.backgroundColor(getResources().getColor(R.color.colorPrimary));
        simpleTooltip.textColor(Color.WHITE);
        simpleTooltip.arrowColor(getResources().getColor(R.color.colorPrimary));
        simpleTooltip.dismissOnInsideTouch(true);
        simpleTooltip.modal(true);
        simpleTooltip.anchorView(imageView);
        simpleTooltip.text(iconMessage);
        simpleTooltip.gravity(Gravity.TOP);
        simpleTooltip.animated(false);
        simpleTooltip.transparentOverlay(true);
        simpleTooltip.build().show();

        /*new SimpleTooltip.Builder(this)
                .backgroundColor(getResources().getColor(R.color.colorPrimary))
                .textColor(Color.WHITE)
                .arrowColor(getResources().getColor(R.color.colorPrimary))
                .dismissOnInsideTouch(true)
                .onDismissListener(new SimpleTooltip.OnDismissListener() {
                    @Override
                    public void onDismiss(SimpleTooltip tooltip) {

                    }
                })
                .dismissOnOutsideTouch(true)
                .modal(true)
                .anchorView(imageView)
                .text(iconMessage)
                .gravity(Gravity.BOTTOM)
                .animated(false)
                .transparentOverlay(true)
                .build()
                .show();*/
    }

    public void onSelect(View anchor) {
        String[] imageId = anchor.getResources().getResourceName(anchor.getId()).split("/");
        String[] messages = {"", getResources().getString(R.string.hard_to_reach),
                getResources().getString(R.string.amazing), getResources().getString(R.string.make_home_visits),
                getResources().getString(R.string.great_help), getResources().getString(R.string.angry_doctor),
                getResources().getString(R.string.reachable), getResources().getString(R.string.old_doctor),
                getResources().getString(R.string.like)};
        final ImageView imageView = (ImageView) findViewById(anchor.getId());
        String elementId = imageId[1];
        String iconMessage = "";
        for (int i = 1; i <= 8; i++) {
            //imageView.setBackgroundResource(0);
            if (elementId.equals("doctor_selected_" + i)) {
                iconMessage = messages[i];
                reviewIcon = String.valueOf(i);
            } else {
                int selectedIcon = this.getResources().getIdentifier("doctor_selected_" + i,
                        "id", this.getPackageName());
                ImageView otherImage = (ImageView) findViewById(selectedIcon);
                otherImage.setVisibility(View.GONE);
            }
        }

        //simpaletooltip obj
        simpleTooltip = new SimpleTooltip.Builder(this);
        simpleTooltip.backgroundColor(getResources().getColor(R.color.colorPrimary));
        simpleTooltip.textColor(Color.WHITE);
        simpleTooltip.arrowColor(getResources().getColor(R.color.colorPrimary));
        simpleTooltip.dismissOnInsideTouch(true);
        simpleTooltip.dismissOnOutsideTouch(true);
        simpleTooltip.modal(true);
        simpleTooltip.anchorView(imageView);
        simpleTooltip.text(iconMessage);
        simpleTooltip.gravity(Gravity.TOP);
        simpleTooltip.animated(false);
        simpleTooltip.transparentOverlay(true);
        simpleTooltip.build().show();
        /*new SimpleTooltip.Builder(this)
                .backgroundColor(getResources().getColor(R.color.colorPrimary))
                .textColor(Color.WHITE)
                .arrowColor(getResources().getColor(R.color.colorPrimary))
                .dismissOnInsideTouch(true)
                .onDismissListener(new SimpleTooltip.OnDismissListener() {
                    @Override
                    public void onDismiss(SimpleTooltip tooltip) {

                    }
                })
                .dismissOnOutsideTouch(true)
                .modal(true)
                .anchorView(imageView)
                .text(iconMessage)
                .gravity(Gravity.BOTTOM)
                .animated(false)
                .transparentOverlay(true)
                .build()
                .show();*/
    }

    public void itemClicked(View v) {
        //code to check if this checkbox is checked!
        CheckBox checkBox = (CheckBox)v;
        if(checkBox.isChecked()){
            visible = "0";
        } else {
            visible = "1";
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                String path = getRealPathFromURI(filePath);

                if(!MaxSizeImage(path)){

                    Toast.makeText(WriteReviewActivity.this, getResources().getString(R.string.image_size_error), Toast.LENGTH_LONG).show();
                    return;
                }
                //Getting the Bitmap from Gallery
                bitmapImage = MediaStore.Images.Media.getBitmap(WriteReviewActivity.this.getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                attachImage.setImageBitmap(bitmapImage);
                attachImage.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
//            attachImage.setVisibility(View.GONE);
            Log.v("Write Review Image","No image selected!");
        }
    }

    /**
     * Image to string
     * @param bmp
     * @return string
     */
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 70, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


    private void setDefaultView(){
        reputationRatingBar.setRating(0.0f);
        clinicRatingBar.setRating(0.0f);
        availabilityRatingbar.setRating(0.0f);
        approachabilityRatingBar.setRating(0.0f);
        technologyRatingBar.setRating(0.0f);
        summary.setText("");
        writeReviewEditText.setText("");
        customdrawablecheckboxReview.setChecked(false);
        attachImage.setImageDrawable(getResources().getDrawable(R.mipmap.camera_2));


        for (int i = 1; i <= 8; i++) {

            int visibleIcon = WriteReviewActivity.this.getResources().getIdentifier("doctor_" + i,
                    "id", WriteReviewActivity.this.getPackageName());
            ImageView visibleImage = (ImageView) findViewById(visibleIcon);
            visibleImage.setVisibility(View.VISIBLE);

            int notVisibleIcon = WriteReviewActivity.this.getResources().getIdentifier("doctor_selected_" + i,
                    "id", WriteReviewActivity.this.getPackageName());
            ImageView notVisibleImage = (ImageView) findViewById(notVisibleIcon);
            notVisibleImage.setVisibility(View.GONE);
        }
    }



    @Override
    protected void onStop() {
        super.onStop();

//        Toast.makeText(Add.this, CheckingBackGround.isAppIsInBackground(AddClinicActivity.this)+"", Toast.LENGTH_SHORT).show();

        if(CheckingBackGround.isAppIsInBackground(WriteReviewActivity.this)){
            if(TabFragment.timer != null){
                TabFragment.timer.cancel();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        relativeLayout.setBackgroundColor(Color.parseColor("#E6EBEF"));
        if(TabFragment.timer !=null){
            TabFragment.timer.cancel();
            TabFragment.timer.start();
        }
    }

    public boolean MaxSizeImage(String imagePath) {
        boolean temp = false;
        File file = new File(imagePath);
        long length = file.length();

        if (length < 1500000) // 1.5 mb
            temp = true;

        return temp;
    }


    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
}
