package searchnative.com.topdoctors;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by root on 25/11/16.
 */

public class RecentlyViewed {

    public static List recentlyViewed = new ArrayList();
    public static int maxLimit = 10;
    public static int pointer = 0;
    public static String name = "recentlyViewed";
    public static boolean isCreated = false;

    public static List initFromPreference(Context context, String name) {
        isPrefCreated(context);
        List<String> list = new ArrayList<>();
        list.addAll(Preference.getRecentlyViewed(context, name));
        recentlyViewed.addAll(list);

        return list;
    }

    public static void isPrefCreated(Context context) {
        if (Preference.getValue(context, "RecentViewed", "").toString().isEmpty() ||
                Preference.getValue(context, "RecentViewed", "").equals("null")) {
            setRecentlyViewedInPreference(context, name);
            Preference.setValue(context, "RecentViewed", "created");
            isCreated = true;
        }
    }

    public static void add(String data) {
        Log.v("pointer", String.valueOf(pointer));
        recentlyViewed.add(pointer, data);
        pointer++;
        setPointerPosition();
    }

    public static List getAllRecentViewed() {
        return recentlyViewed;
    }

    public static int getPointerPosition() {
        return pointer;
    }

    public static void setPointerPosition() {
        if (pointer == (maxLimit - 1)) {
            pointer = 0;
        }
    }

    public static void setRecentlyViewedInPreference(Context context, String key) {
        Preference.setRecentlyViewed(context, key, recentlyViewed);
    }

    public static List getRecentlyViewedFromPreference(Context context, String name) {
        List list = new ArrayList();
        list.addAll(Preference.getRecentlyViewed(context, name));

        return list;
    }

    public static void removeItem() {}

    public static void checkItemInList() {}

    public static void resetPreference(Context context, String name) {
        //List list = new ArrayList();
        Preference.resetPreference(context, name);
    }

}
