package searchnative.com.topdoctors;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.vision.text.Text;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.fabric.sdk.android.Fabric;

public class UserProfileFragment extends Fragment {

    private LinearLayout badgesLayout;
    private Typeface typeface;
    private TextView titleEditProfile, profileName, profileEmail, reviewCount, photosCount, badgesCount, titleReview,
            titlePhotos, titleBadges, location, userLocation, mobileNumber, commentUserName, userNameComment, commentTime,
            usersCount, starCount, userPhotosCount, userMobileNumber;
    private ProgressDialog mProgressDialog;
    private ImageView editProfileName, editEmail, editUserLocation, editMobileNumber, userProfileImage;
    private String getFirstName, getLastName, getEmail, getLocation, getMobile;
    private Spinner locationSpinner;
    private List countryList;
    private ArrayAdapter countryArrayAdapter;
    private Bitmap bitmap;
    private String userProfileId;
    private ProgressBar progressBar;
    private LinearLayout userProfileDetail, reviewCountSection, photoVideoCountSection;
    private ScrollView userProfileScrollView;

    //async class
    UserAllReviews userAllReviews;
    UserProfileDetails userProfileDetails;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getContext(), new Crashlytics());
        View view = inflater.inflate(R.layout.user_profile, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //progressbar
        userProfileDetail = (LinearLayout) getView().findViewById(R.id.userProfileDetail);
        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        userProfileDetail.setVisibility(View.INVISIBLE);

        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ExoMedium.otf");

        userProfileId = getArguments().getString("id");;

        titleEditProfile = (TextView) getView().findViewById(R.id.title_edit_profile);
        profileName = (TextView) getView().findViewById(R.id.profile_name);
        profileEmail = (TextView) getView().findViewById(R.id.profile_email);
        reviewCount = (TextView) getView().findViewById(R.id.review_count);
        photosCount = (TextView) getView().findViewById(R.id.photos_count);
        badgesCount = (TextView) getView().findViewById(R.id.badges_count);
        titleReview = (TextView) getView().findViewById(R.id.title_review);
        titlePhotos = (TextView) getView().findViewById(R.id.title_photos);
        titleBadges = (TextView) getView().findViewById(R.id.title_badges);
        location = (TextView) getView().findViewById(R.id.location);
        userLocation = (TextView) getView().findViewById(R.id.user_location);
        mobileNumber = (TextView) getView().findViewById(R.id.mobile_number);
        userProfileImage = (ImageView) getView().findViewById(R.id.user_profile_default_image);
        reviewCountSection = (LinearLayout) getView().findViewById(R.id.reviewCountSection);
        photoVideoCountSection = (LinearLayout) getView().findViewById(R.id.photoVideoCountSection);
        userProfileScrollView = (ScrollView) getView().findViewById(R.id.user_profile);
        //commentUserName = (TextView) getView().findViewById(R.id.comment_user_name);
        //userNameComment = (TextView) getView().findViewById(R.id.user_name_comment);
        //commentTime = (TextView) getView().findViewById(R.id.comment_time);
        //usersCount = (TextView) getView().findViewById(R.id.users_count);
        //starCount = (TextView) getView().findViewById(R.id.star_count);
        //userPhotosCount = (TextView) getView().findViewById(R.id.user_photos_count);
        userMobileNumber = (TextView) getView().findViewById(R.id.user_mobile_number);
        editProfileName = (ImageView) getView().findViewById(R.id.profile_name_edit_image);
        editEmail = (ImageView) getView().findViewById(R.id.update_email_user);
        editUserLocation = (ImageView) getView().findViewById(R.id.editUserLocation);
        editMobileNumber = (ImageView) getView().findViewById(R.id.editMobileNumber);

        titleEditProfile.setTypeface(typeface, typeface.BOLD);
        profileName.setTypeface(typeface, typeface.BOLD);
        profileEmail.setTypeface(typeface);
        reviewCount.setTypeface(typeface);
        photosCount.setTypeface(typeface);
        badgesCount.setTypeface(typeface);
        titleReview.setTypeface(typeface);
        titlePhotos.setTypeface(typeface);
        titleBadges.setTypeface(typeface);
        location.setTypeface(typeface, typeface.BOLD);
        userLocation.setTypeface(typeface);
        mobileNumber.setTypeface(typeface, typeface.BOLD);
        //commentUserName.setTypeface(typeface);
        //userNameComment.setTypeface(typeface);
        //commentTime.setTypeface(typeface);
        //usersCount.setTypeface(typeface);
        //starCount.setTypeface(typeface);
        //userPhotosCount.setTypeface(typeface);
        userMobileNumber.setTypeface(typeface);

        badgesLayout = (LinearLayout) getView().findViewById(R.id.badges_layout);
        badgesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), BadgesActivity.class));
            }
        });

        //scroll to review
        reviewCountSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userProfileScrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout locationMobileLayout = (LinearLayout) getView().findViewById(R.id.locationMobileLayout);
                        userProfileScrollView.scrollTo(0, locationMobileLayout.getBottom());
                    }
                });
            }
        });

        photoVideoCountSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),
                        getResources().getString(R.string.no_photos_to_show), Toast.LENGTH_LONG).show();
            }
        });


        //edit profile name pop up
        editProfileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View promptView = layoutInflater.inflate(R.layout.prompts, null);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                alertDialog.setView(promptView);

                final TextView titleText = (TextView) promptView.findViewById(R.id.textView1);
                titleText.setTypeface(typeface);
                titleText.setTextColor(getResources().getColor(R.color.black));

                final EditText userInput = (EditText) promptView.findViewById(R.id.editTextDialogUserInput);
                userInput.setText(getFirstName);
                userInput.setTextColor(getResources().getColor(R.color.black));
                userInput.setTypeface(typeface);

                final EditText lastName = (EditText) promptView.findViewById(R.id.editTextLastName);
                lastName.setText(getLastName);
                lastName.setTypeface(typeface);

                alertDialog
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getFirstName = userInput.getText().toString();
                                        getLastName = lastName.getText().toString();
                                        final String userId = userProfileId;
                                        Log.v("User ID", userId);
                                        updateProfileName(userId, getFirstName, getLastName);
                                    }
                                })
                        .setNegativeButton(getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });
                final AlertDialog dialog = alertDialog.create();
                dialog.show();

                userInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if(userInput.getText().toString().trim().length() == 0 ||
                                lastName.getText().toString().trim().length() == 0) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        } else {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                lastName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if(lastName.getText().toString().trim().length() == 0 ||
                                userInput.getText().toString().trim().length() == 0 ) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        } else {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }
        });

        //edit email
        editEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View promptView = layoutInflater.inflate(R.layout.update_email_prompts, null);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(promptView);

                final TextView titleText = (TextView) promptView.findViewById(R.id.textView1);
                titleText.setTypeface(typeface);
                titleText.setTextColor(getResources().getColor(R.color.black));

                final EditText emailEditText = (EditText) promptView.findViewById(R.id.editTextEmail);
                emailEditText.setText(getEmail);
                emailEditText.setTextColor(getResources().getColor(R.color.black));
                emailEditText.setTypeface(typeface);

                builder
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getEmail = emailEditText.getText().toString();
                                        final String userId = userProfileId;
                                        updateEmail(userId, getEmail);
                                    }
                                })
                        .setNegativeButton(getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });

                final AlertDialog dialog = builder.create();
                dialog.show();

                emailEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if(emailEditText.getText().toString().trim().length() == 0) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        } else {
                            //check for valid email
                            Pattern pattern1 = Pattern.compile( "^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");
                            Matcher matcher1 = pattern1.matcher(emailEditText.getText().toString());
                            if (!matcher1.matches()) {
                                emailEditText.setError(getResources().getString(R.string.valid_email_error));
                                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                            } else {
                                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                            }
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }
        });

        //edit location
        editUserLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View promptView = layoutInflater.inflate(R.layout.update_location_prompt, null);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(promptView);

                final TextView titleText = (TextView) promptView.findViewById(R.id.textView1);
                titleText.setTypeface(typeface);
                titleText.setTextColor(getResources().getColor(R.color.black));

                locationSpinner = (Spinner) promptView.findViewById(R.id.userLocation);
                countryList = new ArrayList();
                countryList.add(getResources().getString(R.string.country));
                countryList.addAll(CountryData.getmInstance().countryList);
                countryArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item,
                        countryList) {
                    @Override
                    public boolean isEnabled(int position) {
                        if(position == 0) {
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @Override
                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
                        View v = null;
                        //View view  = super.getDropDownView(position, convertView, parent);
                        if(position == 0) {
                            TextView textView = new TextView(getContext());
                            textView.setHeight(0);
                            textView.setVisibility(View.GONE);
                            textView.setTextColor(getResources().getColor(R.color.textHighlightColor));
                            v = textView;
                        } else {
                            v = super.getDropDownView(position, null, parent);
                        }

                        return v;
                    }

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        ((TextView) view).setTypeface(typeface);
                        if(position == 0) {
                            ((TextView) view).setTextColor(getResources().getColor(R.color.textHighlightColor));
                            ((TextView) view).setTextSize(16);
                        } else {
                            ((TextView) view).setTextColor(getResources().getColor(R.color.black));
                            ((TextView) view).setTextSize(16);
                        }

                        return view;
                    }
                };
                countryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                countryArrayAdapter.setNotifyOnChange(true);
                locationSpinner.setAdapter(countryArrayAdapter);

                for(int i = 0; i < countryArrayAdapter.getCount(); i++) {
                    if(getLocation.trim().equals(countryArrayAdapter.getItem(i).toString())) {
                        locationSpinner.setSelection(i);
                    }
                }

                builder
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        final String userId = userProfileId;
                                        getLocation = locationSpinner.getSelectedItem().toString();
                                        updateCountry(userId, getLocation);
                                    }
                                })
                        .setNegativeButton(getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });

                locationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if(i > 0) {
                            locationSpinner.setSelection(i);
                            //userLocation.setText(getLocation);
                            //Log.v("data", locationSpinner.getSelectedItem().toString());
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        //update mobile
        editMobileNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View promptView = layoutInflater.inflate(R.layout.update_mobile_prompt, null);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(promptView);

                final TextView titleText = (TextView) promptView.findViewById(R.id.textView1);
                titleText.setTypeface(typeface);
                titleText.setTextColor(getResources().getColor(R.color.black));

                final EditText editUserMobile = (EditText) promptView.findViewById(R.id.editTextMobile);
                editUserMobile.setTypeface(typeface);
                editUserMobile.setText(getMobile);
                editUserMobile.setTextColor(getResources().getColor(R.color.black));

                builder
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getMobile = editUserMobile.getText().toString();
                                        final String userId = userProfileId;
                                        updateMobile(userId, getMobile);
                                    }
                                })
                        .setNegativeButton(getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });

                final AlertDialog dialog = builder.create();
                dialog.show();

                editUserMobile.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if(editUserMobile.getText().toString().trim().length() < 10) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        } else {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

            }
        });
    }

    public void updateEmail(final String userId, final String email) {

        class UpdateEmail extends AsyncTask<String, Void, String> {
            final String URL = AppConfig.getWebServiceUrl() + "profile/userProfileUpdateEmail";
            String quickUserId = userId;
            String quickEmail = email;
            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<>();
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("email", quickEmail));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();

                    return mClient.execute(httpPost, responseHandler);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    profileEmail.setText(getEmail);
                    Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }

        UpdateEmail updateEmail = new UpdateEmail();
        updateEmail.execute(userId, email);

    }

    public void updateMobile(final String userId, final String mobile) {

        class UpdateMobile extends AsyncTask<String, Void, String> {

            final String URL = AppConfig.getWebServiceUrl() + "profile/userProfileUpdateMobile";
            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            String quickUserId = userProfileId;
            String quickMobile = mobile;

            @Override
            protected String doInBackground(String... params) {
                Log.v("Login Id", quickUserId);
                List<NameValuePair> nameValuePairs = new ArrayList<>();
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("mobile", quickMobile));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();

                    return mClient.execute(httpPost, responseHandler);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.v("User Profile", result);
                //new RenderUserProfileDetails(result);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    userMobileNumber.setText(getMobile);
                    Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }

        UpdateMobile updateMobile = new UpdateMobile();
        updateMobile.execute(userId, mobile);
    }

    public void updateCountry(final String userId, final String country) {

        class UpdateCountry extends AsyncTask<String, Void, String> {
            final String URL = AppConfig.getWebServiceUrl() + "profile/userProfileUpdateLocation";
            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            String quickUserId = userId;
            String quickCountry = country;

            @Override
            protected String doInBackground(String... params) {
                Log.v("Login Id", quickUserId);
                List<NameValuePair> nameValuePairs = new ArrayList<>();
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("location", quickCountry));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();

                    return mClient.execute(httpPost, responseHandler);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.v("User Profile", result);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    userLocation.setText(getLocation);
                    Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }

        UpdateCountry updateCountry = new UpdateCountry();
        updateCountry.execute(userId, country);
    }

    class UserProfileDetails extends AsyncTask<String, Void, String> {

        final String URL = AppConfig.getWebServiceUrl() + "profile/userProfile";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        String quickUserId = userProfileId;

        @Override
        protected String doInBackground(String... params) {
            Log.v("Login Id", quickUserId);
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();

                //check for user type
                /*String userType =  Preference.getValue(getContext(), "USER_TYPE", "");
                Log.v("Login type", userType);
                if(userType.equals("FB")) {
                    String userID = Preference.getValue(getContext(), "USER_ID", "");
                    Log.v("User ID", userID);
                    //ImageView userProfileImage = (ImageView) getView().findViewById(R.id.user_profile_default_image);
                    //userProfileImage.setImageDrawable(getResources().getDrawable(R.drawable.doctor_1));
                    try {
                        URL imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=small");
                        bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
                        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
                        //userProfileImage.setImageBitmap(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Drawable drawable = getResources().getDrawable(R.drawable.user_profile_default_image);
                    bitmap = ((BitmapDrawable) drawable).getBitmap();
                }*/

                Drawable drawable = getResources().getDrawable(R.drawable.user_profile_default_image);
                bitmap = ((BitmapDrawable) drawable).getBitmap();
                return mClient.execute(httpPost, responseHandler);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.v("User Profile", result);
            new RenderUserProfileDetails(result);
            userProfileImage.setImageBitmap(bitmap);
            /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
            /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
            mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();*/
        }
    }

    class RenderUserProfileDetails {
        RenderUserProfileDetails(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("data");

                //user name
                String userName = jsonArray.getJSONObject(0).getString("Name");
                String email = jsonArray.getJSONObject(0).getString("email");
                String location = jsonArray.getJSONObject(0).getString("location");
                String mobile = jsonArray.getJSONObject(0).getString("phone");
                String doctorReview = jsonArray.getJSONObject(0).getString("DoctorReview");
                String workTypeReview = jsonArray.getJSONObject(0).getString("WorkReview");
                String imageUrl = jsonArray.getJSONObject(0).getString("Photo");
                Log.v("Doctor review", doctorReview);
                Log.v("Work type review", workTypeReview);

                new DownloadImageTask((ImageView) getView().findViewById(R.id.user_profile_default_image))
                        .execute(imageUrl);

                int totalUserReview = 0;
                if (!(jsonArray.getJSONObject(0).getString("DoctorReview").equals("null") ||
                        jsonArray.getJSONObject(0).getString("DoctorReview").isEmpty())) {
                    totalUserReview = Integer.parseInt(jsonArray.getJSONObject(0).getString("DoctorReview"));
                }
                if (!(jsonArray.getJSONObject(0).getString("WorkReview").equals("null") ||
                        jsonArray.getJSONObject(0).getString("WorkReview").isEmpty())) {
                    totalUserReview += Integer.parseInt(jsonArray.getJSONObject(0).getString("WorkReview"));
                }
                Log.v("Total review", String.valueOf(totalUserReview));

                getFirstName = jsonArray.getJSONObject(0).getString("firstName");
                getLastName = jsonArray.getJSONObject(0).getString("lastName");
                getEmail = email;
                getLocation = location;
                getMobile = mobile;

                //set values
                titleEditProfile.setText(userName);
                profileName.setText(userName);
                profileEmail.setText(email);
                userMobileNumber.setText(mobile);
                userLocation.setText(location);
                reviewCount.setText(String.valueOf(totalUserReview));
                //userNameComment.setText(userName);

                //set badges
                if (totalUserReview > 0 && totalUserReview <= 10) {
                    badgesCount.setText("1");
                } else if (totalUserReview > 10 && totalUserReview <= 100) {
                    badgesCount.setText("2");
                } else if (totalUserReview > 100) {
                    badgesCount.setText("3");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updateProfileName(final String userId, final String firstName, final String lastName) {

        class UpdateProfileName extends AsyncTask<String, Void, String> {
            final String URL = AppConfig.getWebServiceUrl() + "profile/userProfileUpdateFullName";
            String quickFirstName = firstName;
            String quickLastName = lastName;
            String quickUserId = userId;

            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<>();
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("firstName", quickFirstName));
                nameValuePairs.add(new BasicNameValuePair("lastName", quickLastName));
                Log.v("Nam value pair", nameValuePairs.toString());
                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();

                    return mClient.execute(httpPost, responseHandler);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    titleEditProfile.setText(getFirstName + " " + getLastName);
                    profileName.setText(getFirstName + " " + getLastName);
                    Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }

        UpdateProfileName updateProfileName = new UpdateProfileName();
        updateProfileName.execute(userId, firstName, lastName);
    }

    /**
     * user all reviews async
     */
    class UserAllReviews extends AsyncTask<String, Void, String> {
        final String URL = AppConfig.getWebServiceUrl() + "review/UserByDoctorReviews?userId=" + userProfileId;

        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            //Log.v("Inside background", userId);
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("userId", userProfileId));
            Log.v("Nam value pair", nameValuePairs.toString());
            try {
                Log.v("URL", URL);
                HttpGet httpPost = new HttpGet(URL);
                //httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();

                return mClient.execute(httpPost, responseHandler);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            new RenderAllUserReview(result);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        userProfileDetails = new UserProfileDetails();
        userProfileDetails.execute();
        userAllReviews = new UserAllReviews();
        userAllReviews.execute();
    }

    class RenderAllUserReview {
        RenderAllUserReview(String result) {

            LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.userAllReviewList);
            linearLayout.removeAllViews();

            linearLayout.setGravity(Gravity.START);

            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            );

            try {
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.you_have_not_given_any_review));
                    textView.setText("");
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    linearLayout.addView(textView);
                } else {
                    JSONArray reviewResult = jsonObject.getJSONArray("result");
                    Log.v("Review array", reviewResult.toString());
                    for (int i = 0; i < reviewResult.length(); i++) {
                        final JSONObject reviewData = reviewResult.getJSONObject(i);

                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.user_all_reviews, null);

                        TextView textView = (TextView) view.findViewById(R.id.comment_user_name);
                        textView.setText(reviewData.getString("doctorName"));
                        textView.setTypeface(typeface);

                        TextView textView1 = (TextView) view.findViewById(R.id.user_name_comment);
                        textView1.setText(getFirstName + " " + getLastName);
                        textView1.setTypeface(typeface);

                        TextView textView2 = (TextView) view.findViewById(R.id.comment_time);
                        textView2.setTypeface(typeface);
                        textView2.setText(reviewData.getString("commentTime"));

                        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.userRating);
                        ratingBar.setRating(Float.valueOf(reviewData.getString("avgReview")));

                        TextView userReview = (TextView) view.findViewById(R.id.userReviewComment);
                        userReview.setText(reviewData.getString("comment"));
                        userReview.setTypeface(typeface);

                        ImageView editUserReview = (ImageView) view.findViewById(R.id.userReview);
                        editUserReview.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    editUserReview(reviewData.getString("doctorId"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        //doctor or work type profile
                        final String profileId = reviewData.getString("doctorId");
                        final String profileName = reviewData.getString("doctorName");
                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                doctorProfile(profileId, profileName);
                            }
                        });

                        linearLayout.addView(view);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.v("Review result", result);
        }
    }

    public void editUserReview(final String doctorId) {
        final String userId = userProfileId;
        Log.v("User Id", userId);
        Log.v("Doctor Id", doctorId);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            progressBar.setVisibility(View.GONE);
            userProfileDetail.setVisibility(View.VISIBLE);
        }
    }

    public void doctorProfile(final String doctorId, final String doctorName) {
        Bundle args = new Bundle();
        args.putString("id", doctorId);
        args.putString("name", doctorName);
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        DoctorProfileFragment clinicProfileFragment = new DoctorProfileFragment();
        clinicProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("doctorProfile");
        mFragmentTransaction.add(R.id.search_layout, clinicProfileFragment).commit();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (userProfileDetails != null)
            userProfileDetails.cancel(true);
        if (userAllReviews != null)
            userAllReviews.cancel(true);
    }
}