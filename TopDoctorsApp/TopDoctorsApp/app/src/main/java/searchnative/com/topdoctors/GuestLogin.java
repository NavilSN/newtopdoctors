package searchnative.com.topdoctors;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

/**
 * Created by root on 23/11/16.
 */

public class GuestLogin {

    public void setGuestLogin(Context context, String status) {
        Preference.setValue(context, "GUEST_LOGIN", status);
        Preference.setValue(context, "LOGIN_ID", "0");
        Preference.setValue(context, "PREF_FNAME", "Guest");
        Preference.setValue(context, "PREF_ADDRESS", "");
        Preference.setValue(context, "PREF_ISLOGIN", status);
        Preference.setValue(context, "USER_TYPE", "LOCAL");
        Preference.setValue(context, "USER_ID", "");
    }

    public Boolean getGuestLogin(Context context) {
        boolean isGuest = false;
        String guest = Preference.getValue(context, "GUEST_LOGIN", "");
        if (guest.equals("1")) {
            Log.v("guest status", guest);
            isGuest = true;
        }

        return isGuest;
    }

    public void removeGuestLogin(Context context) {
        Preference.setValue(context, "GUEST_LOGIN", "");
        Preference.setValue(context, "LOGIN_ID", "");
        Preference.setValue(context, "PREF_FNAME", "");
        Preference.setValue(context, "PREF_ADDRESS", "");
        Preference.setValue(context, "PREF_ISLOGIN", "");
        Preference.setValue(context, "USER_TYPE", "");
        Preference.setValue(context, "USER_ID","");
    }

    public void confirmAlert(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true)
                .setTitle(context.getResources().getString(R.string.user_login))
                .setMessage(context.getResources().getString(R.string.guest_confirm_message))
                .setPositiveButton(context.getResources().getString(R.string.login),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {


                                if (TabFragment.timer != null){
                                    TabFragment.timer.cancel();
                                }

                                LocationData.mInstance = null;
                                SpecialityData.mInstance = null;
                                CountryData.mInstance = null;

                                removeGuestLogin(context);
                                context.startActivity(new Intent(context, LoginActivity.class));
                            }
                        })
                .setNegativeButton(context.getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources()
                        .getColor(R.color.colorPrimary));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(context.getResources()
                        .getColor(R.color.colorPrimary));
            }
        });
        dialog.show();
    }
}
