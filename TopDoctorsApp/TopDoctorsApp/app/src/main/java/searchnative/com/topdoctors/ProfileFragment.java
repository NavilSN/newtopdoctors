package searchnative.com.topdoctors;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.fabric.sdk.android.Fabric;

public class ProfileFragment extends Fragment {

    private LinearLayout badgesLayout;
    private Typeface typeface;
    private TextView titleEditProfile, profileName, profileEmail, reviewCount, photosCount, badgesCount, titleReview,
            titlePhotos, titleBadges, location, userLocation, mobileNumber, commentUserName, userNameComment, commentTime,
            usersCount, starCount, userPhotosCount, userMobileNumber;
    private ProgressDialog mProgressDialog;
    private ImageView editProfileName, editEmail, editUserLocation, editMobileNumber, userProfileImage;
    private String getFirstName, getLastName, getEmail, getLocation, getMobile;
    private Spinner locationSpinner;
    private List countryList;
    private ArrayAdapter countryArrayAdapter;
    private Bitmap bitmap;
    private ProgressBar progressBar;
    private LinearLayout profileDetails;
    private ImageView user_profile_default_image;

    private int PICK_IMAGE_REQUEST = 1;
    private String UPLOAD_URL = AppConfig.getWebServiceUrl() + "profile/userProfileUpdatePic";
    private String KEY_IMAGE = "image";
    private ImageView imageView;
    private ProgressDialog loading;
    private String loginUserId;
    private String userId;
    TabFragment tabFragment = new TabFragment();
    private String countryName = "", profileUsername = "";
    private LinearLayout reviewCountSection, photoVideoCountSection;
    private ScrollView userProfileScrollview;

    //navigation
    private NavigationView defaultNavigationView;
    private View headerView;
    private TextView newuser, navAddress;
    private ImageView navProfilePic;
    private File mFileTemp;
    //private boolean isOn;

    //Asynck
    UserProfileDetails userProfileDetailsAsynck;
    UserAllReviews userAllReviews;

    public static final String TEMP_PHOTO_FILE_NAME = "add_user.jpg";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getContext(), new Crashlytics());
        View view = inflater.inflate(R.layout.profile_layout, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //progressbar
        profileDetails = (LinearLayout) getView().findViewById(R.id.userProfileDetail);
        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        profileDetails.setVisibility(View.INVISIBLE);
        loginUserId = Preference.getValue(getContext(), "LOGIN_ID", "");
        defaultNavigationView = (NavigationView) getActivity()
                .findViewById(R.id.navigation_view);
        headerView = defaultNavigationView.getHeaderView(0);
        newuser = (TextView) headerView.findViewById(R.id.login_user_name);
        navAddress = (TextView) headerView.findViewById(R.id.login_user_address);
        navProfilePic = (ImageView) headerView.findViewById(R.id.imageView);
        //newuser.setText(Preference.getValue(getContext(), "PREF_FNAME", ""));
        //navAddress.setText(Preference.getValue(getContext(), "PREF_ADDRESS", ""));

        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ExoMedium.otf");

        //networkStatus();
        profileDetails.setVisibility(View.INVISIBLE);
        userId = Preference.getValue(getContext(), "LOGIN_ID", "");
        //profile
        userProfileDetailsAsynck = new UserProfileDetails();
        userProfileDetailsAsynck.execute();

        //load all review
        userAllReviews = new UserAllReviews();
        userAllReviews.execute();
        /*if (isOn) {

        }*/
        titleEditProfile = (TextView) getView().findViewById(R.id.title_edit_profile);
        profileName = (TextView) getView().findViewById(R.id.profile_name);
        profileEmail = (TextView) getView().findViewById(R.id.profile_email);
        reviewCount = (TextView) getView().findViewById(R.id.review_count);
        photosCount = (TextView) getView().findViewById(R.id.photos_count);
        badgesCount = (TextView) getView().findViewById(R.id.badges_count);
        titleReview = (TextView) getView().findViewById(R.id.title_review);
        titlePhotos = (TextView) getView().findViewById(R.id.title_photos);
        titleBadges = (TextView) getView().findViewById(R.id.title_badges);
        location = (TextView) getView().findViewById(R.id.location);
        userLocation = (TextView) getView().findViewById(R.id.user_location);
        mobileNumber = (TextView) getView().findViewById(R.id.mobile_number);
        userProfileImage = (ImageView) getView().findViewById(R.id.user_profile_default_image);
        user_profile_default_image = (ImageView) getView().findViewById(R.id.user_profile_default_image);
        reviewCountSection = (LinearLayout) getView().findViewById(R.id.reviewCountSection);
        photoVideoCountSection = (LinearLayout) getView().findViewById(R.id.photoVideoCountSection);
        userProfileScrollview = (ScrollView) getView().findViewById(R.id.user_profile);
        //commentUserName = (TextView) getView().findViewById(R.id.comment_user_name);
        //userNameComment = (TextView) getView().findViewById(R.id.user_name_comment);
        //commentTime = (TextView) getView().findViewById(R.id.comment_time);
        //usersCount = (TextView) getView().findViewById(R.id.users_count);
        //starCount = (TextView) getView().findViewById(R.id.star_count);
        //userPhotosCount = (TextView) getView().findViewById(R.id.user_photos_count);
        userMobileNumber = (TextView) getView().findViewById(R.id.user_mobile_number);
        editProfileName = (ImageView) getView().findViewById(R.id.profile_name_edit_image);
        editEmail = (ImageView) getView().findViewById(R.id.update_email_user);
        editUserLocation = (ImageView) getView().findViewById(R.id.editUserLocation);
        editMobileNumber = (ImageView) getView().findViewById(R.id.editMobileNumber);

        titleEditProfile.setTypeface(typeface, typeface.BOLD);
        profileName.setTypeface(typeface, typeface.BOLD);
        profileEmail.setTypeface(typeface);
        reviewCount.setTypeface(typeface);
        photosCount.setTypeface(typeface);
        badgesCount.setTypeface(typeface);
        titleReview.setTypeface(typeface);
        titlePhotos.setTypeface(typeface);
        titleBadges.setTypeface(typeface);
        location.setTypeface(typeface, typeface.BOLD);
        userLocation.setTypeface(typeface);
        mobileNumber.setTypeface(typeface, typeface.BOLD);
        //commentUserName.setTypeface(typeface);
        //userNameComment.setTypeface(typeface);
        //commentTime.setTypeface(typeface);
        //usersCount.setTypeface(typeface);
        //starCount.setTypeface(typeface);
        //userPhotosCount.setTypeface(typeface);
        userMobileNumber.setTypeface(typeface);

        badgesLayout = (LinearLayout) getView().findViewById(R.id.badges_layout);
        badgesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), BadgesActivity.class));
            }
        });

        //scroll to review
        reviewCountSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userProfileScrollview.post(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout locationMobileLayout = (LinearLayout) getView().findViewById(R.id.locationMobileLayout);
                        userProfileScrollview.scrollTo(0, locationMobileLayout.getBottom());
                    }
                });
            }
        });

        //photo and video count
        photoVideoCountSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),
                        getResources().getString(R.string.no_photos_to_show), Toast.LENGTH_LONG).show();
            }
        });

        //edit profile pic
        user_profile_default_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                final View promptView = layoutInflater.inflate(R.layout.update_image_profile, null);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                alertDialog.setView(promptView);

                imageView = (ImageView) promptView.findViewById(R.id.profilePicImage);
                final LinearLayout upload_photo_layout = (LinearLayout) promptView.findViewById(R.id.upload_photo_layout);
                upload_photo_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showFileChooser();
                    }
                });

                alertDialog
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        if(! NetworkInfoHelper.isOnline(getContext())){
                                            Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();
                                            return;
                                        }

                                        uploadImage();
                                    }
                                })
                        .setNegativeButton(getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });
                final AlertDialog dialog = alertDialog.create();

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources()
                                .getColor(R.color.colorPrimary));
                        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources()
                                .getColor(R.color.colorPrimary));
                    }
                });
                dialog.show();
            }
        });

        //edit profile name pop up
        editProfileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View promptView = layoutInflater.inflate(R.layout.prompts, null);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                alertDialog.setView(promptView);

                final TextView titleText = (TextView) promptView.findViewById(R.id.textView1);
                titleText.setTypeface(typeface);
                titleText.setTextColor(getResources().getColor(R.color.black));

                final EditText userInput = (EditText) promptView.findViewById(R.id.editTextDialogUserInput);
                userInput.setText(getFirstName);
                userInput.setTextColor(getResources().getColor(R.color.black));
                userInput.setTypeface(typeface);

                final EditText lastName = (EditText) promptView.findViewById(R.id.editTextLastName);
                lastName.setText(getLastName);
                lastName.setTypeface(typeface);

                alertDialog
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getFirstName = userInput.getText().toString();
                                        getLastName = lastName.getText().toString();
                                        final String userId = Preference.getValue(getContext(), "LOGIN_ID", "");
                                        Log.v("User ID", userId);
                                        updateProfileName(userId, getFirstName, getLastName);
                                    }
                                })
                        .setNegativeButton(getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });
                final AlertDialog dialog = alertDialog.create();

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources()
                        .getColor(R.color.colorPrimary));
                        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources()
                                .getColor(R.color.colorPrimary));
                    }
                });
                dialog.show();

                userInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if(userInput.getText().toString().trim().length() == 0 ||
                                lastName.getText().toString().trim().length() == 0) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        } else {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                lastName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if(lastName.getText().toString().trim().length() == 0 ||
                                userInput.getText().toString().trim().length() == 0 ) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        } else {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }
        });

        //edit email
        editEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View promptView = layoutInflater.inflate(R.layout.update_email_prompts, null);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(promptView);

                final TextView titleText = (TextView) promptView.findViewById(R.id.textView1);
                titleText.setTypeface(typeface);
                titleText.setTextColor(getResources().getColor(R.color.black));

                final EditText emailEditText = (EditText) promptView.findViewById(R.id.editTextEmail);
                emailEditText.setText(getEmail);
                emailEditText.setTextColor(getResources().getColor(R.color.black));
                emailEditText.setTypeface(typeface);

                builder
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getEmail = emailEditText.getText().toString();
                                        final String userId = Preference.getValue(getContext(), "LOGIN_ID", "");
                                        updateEmail(userId, getEmail);
                                    }
                                })
                        .setNegativeButton(getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });

                final AlertDialog dialog = builder.create();
                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources()
                                .getColor(R.color.colorPrimary));
                        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources()
                                .getColor(R.color.colorPrimary));
                    }
                });
                dialog.show();

                emailEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if(emailEditText.getText().toString().trim().length() == 0) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        } else {
                            //check for valid email
                            Pattern pattern1 = Pattern.compile( "^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");
                            Matcher matcher1 = pattern1.matcher(emailEditText.getText().toString());
                            if (!matcher1.matches()) {
                                emailEditText.setError(getResources().getString(R.string.valid_email_error));
                                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                            } else {
                                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                            }
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }
        });

        //edit location
        editUserLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View promptView = layoutInflater.inflate(R.layout.update_location_prompt, null);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(promptView);

                final TextView titleText = (TextView) promptView.findViewById(R.id.textView1);
                titleText.setTypeface(typeface);
                titleText.setTextColor(getResources().getColor(R.color.black));

                locationSpinner = (Spinner) promptView.findViewById(R.id.userLocation);
                countryList = new ArrayList();
                countryList.add(getResources().getString(R.string.country));
                countryList.addAll(CountryData.getmInstance().countryList);
                countryArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item,
                        countryList) {
                    @Override
                    public boolean isEnabled(int position) {
                        if(position == 0) {
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @Override
                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
                        View v = null;
                        //View view  = super.getDropDownView(position, convertView, parent);
                        if(position == 0) {
                            TextView textView = new TextView(getContext());
                            textView.setHeight(0);
                            textView.setVisibility(View.GONE);
                            textView.setTextColor(getResources().getColor(R.color.textHighlightColor));
                            v = textView;
                        } else {
                            v = super.getDropDownView(position, null, parent);
                        }

                        return v;
                    }

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        ((TextView) view).setTypeface(typeface);
                        if(position == 0) {
                            ((TextView) view).setTextColor(getResources().getColor(R.color.textHighlightColor));
                            ((TextView) view).setTextSize(16);
                        } else {
                            ((TextView) view).setTextColor(getResources().getColor(R.color.black));
                            ((TextView) view).setTextSize(16);
                        }

                        return view;
                    }
                };
                countryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                countryArrayAdapter.setNotifyOnChange(true);
                locationSpinner.setAdapter(countryArrayAdapter);

                for(int i = 0; i < countryArrayAdapter.getCount(); i++) {
                    if(getLocation.trim().equals(countryArrayAdapter.getItem(i).toString())) {
                        locationSpinner.setSelection(i);
                    }
                }

                builder
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        final String userId = Preference.getValue(getContext(), "LOGIN_ID", "");
                                        getLocation = locationSpinner.getSelectedItem().toString();
                                        updateCountry(userId, getLocation);
                                    }
                                })
                        .setNegativeButton(getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });

                locationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if(i > 0) {
                            locationSpinner.setSelection(i);
                            //userLocation.setText(getLocation);
                            //Log.v("data", locationSpinner.getSelectedItem().toString());
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                final AlertDialog dialog = builder.create();
                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources()
                                .getColor(R.color.colorPrimary));
                        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources()
                                .getColor(R.color.colorPrimary));
                    }
                });
                dialog.show();
            }
        });

        //update mobile
        editMobileNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View promptView = layoutInflater.inflate(R.layout.update_mobile_prompt, null);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(promptView);

                final TextView titleText = (TextView) promptView.findViewById(R.id.textView1);
                titleText.setTypeface(typeface);
                titleText.setTextColor(getResources().getColor(R.color.black));

                final EditText editUserMobile = (EditText) promptView.findViewById(R.id.editTextMobile);
                editUserMobile.setTypeface(typeface);
                editUserMobile.setText(getMobile);
                editUserMobile.setTextColor(getResources().getColor(R.color.black));

                builder
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getMobile = editUserMobile.getText().toString();
                                        final String userId = Preference.getValue(getContext(), "LOGIN_ID", "");
                                        updateMobile(userId, getMobile);
                                    }
                                })
                        .setNegativeButton(getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });

                final AlertDialog dialog = builder.create();
                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources()
                                .getColor(R.color.colorPrimary));
                        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources()
                                .getColor(R.color.colorPrimary));
                    }
                });
                dialog.show();

                editUserMobile.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if(editUserMobile.getText().toString().trim().length() < 10) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        } else {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

            }
        });
    }

    public void updateEmail(final String userId, final String email) {

        class UpdateEmail extends AsyncTask<String, Void, String> {
            final String URL = AppConfig.getWebServiceUrl() + "profile/userProfileUpdateEmail";
            String quickUserId = userId;
            String quickEmail = email;
            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<>();
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("email", quickEmail));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();

                    return mClient.execute(httpPost, responseHandler);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    profileEmail.setText(getEmail);
                    Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();                
                mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }

        UpdateEmail updateEmail = new UpdateEmail();
        updateEmail.execute(userId, email);

    }

    public void updateMobile(final String userId, final String mobile) {

        class UpdateMobile extends AsyncTask<String, Void, String> {

            final String URL = AppConfig.getWebServiceUrl() + "profile/userProfileUpdateMobile";
            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            String quickUserId = Preference.getValue(getContext(), "LOGIN_ID", "");
            String quickMobile = mobile;

            @Override
            protected String doInBackground(String... params) {
                Log.v("Login Id", quickUserId);
                List<NameValuePair> nameValuePairs = new ArrayList<>();
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("mobile", quickMobile));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();

                    return mClient.execute(httpPost, responseHandler);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.v("User Profile", result);
                //new RenderUserProfileDetails(result);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    userMobileNumber.setText(getMobile);
                    Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }

        UpdateMobile updateMobile = new UpdateMobile();
        updateMobile.execute(userId, mobile);
    }

    public void updateCountry(final String userId, final String country) {

        class UpdateCountry extends AsyncTask<String, Void, String> {
            final String URL = AppConfig.getWebServiceUrl() + "profile/userProfileUpdateLocation";
            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            String quickUserId = userId;
            String quickCountry = country;

            @Override
            protected String doInBackground(String... params) {
                Log.v("Login Id", quickUserId);
                List<NameValuePair> nameValuePairs = new ArrayList<>();
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("location", quickCountry));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();

                    return mClient.execute(httpPost, responseHandler);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.v("User Profile", result);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    userLocation.setText(getLocation);
                    Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    countryName = getLocation;
                    Preference.setValue(getContext(), "PREF_ADDRESS", countryName);
                    navAddress.setText(getLocation);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }

        UpdateCountry updateCountry = new UpdateCountry();
        updateCountry.execute(userId, country);
    }

    /**
     * user profile async
     */
    class UserProfileDetails extends AsyncTask<String, Void, String> {

        final String URL = AppConfig.getWebServiceUrl() + "profile/userProfile";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        String quickUserId = Preference.getValue(getContext(), "LOGIN_ID", "");

        @Override
        protected String doInBackground(String... params) {
            Log.v("Login Id", quickUserId);
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();

                //check for user type
                String userType =  Preference.getValue(getContext(), "USER_TYPE", "");
                Log.v("Login type", userType);
                if(userType.equals("FB")) {
                    /*String userID = Preference.getValue(getContext(), "USER_ID", "");
                    Log.v("User ID", userID);
                    //ImageView userProfileImage = (ImageView) getView().findViewById(R.id.user_profile_default_image);
                    //userProfileImage.setImageDrawable(getResources().getDrawable(R.drawable.doctor_1));
                    try {
                        URL imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=small");
                        bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
                        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
                        //userProfileImage.setImageBitmap(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                } else {

                }
                Drawable drawable = getResources().getDrawable(R.drawable.user_profile_default_image);
                bitmap = ((BitmapDrawable) drawable).getBitmap();

                return mClient.execute(httpPost, responseHandler);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.v("User Profile", result);
            new RenderUserProfileDetails(result);
            userProfileImage.setImageBitmap(bitmap);
            /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

            /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
            mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();*/
        }
    }

    /**
     * render user profile
     */
    class RenderUserProfileDetails {
        RenderUserProfileDetails(String result) {
            try {
                Log.v("Profile data", result);
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("data");

                //user name
                String userName = jsonArray.getJSONObject(0).getString("Name");
                String email = jsonArray.getJSONObject(0).getString("email");
                String location = jsonArray.getJSONObject(0).getString("location");
                String mobile = jsonArray.getJSONObject(0).getString("phone");
                String doctorReview = jsonArray.getJSONObject(0).getString("DoctorReview");
                String workTypeReview = jsonArray.getJSONObject(0).getString("WorkReview");
                String imageUrl = jsonArray.getJSONObject(0).getString("Photo");
                Log.v("Doctor review", doctorReview);
                Log.v("Work type review", workTypeReview);

//                Preference.setValue(getContext(), "PROFILE_PIC", imageUrl);

                new DownloadImageTask((ImageView) getView().findViewById(R.id.user_profile_default_image))
                        .execute(imageUrl);

                int totalUserReview = 0;
                if (!(jsonArray.getJSONObject(0).getString("DoctorReview").equals("null") ||
                        jsonArray.getJSONObject(0).getString("DoctorReview").isEmpty())) {
                    totalUserReview = Integer.parseInt(jsonArray.getJSONObject(0).getString("DoctorReview"));
                }
                if (!(jsonArray.getJSONObject(0).getString("WorkReview").equals("null") ||
                        jsonArray.getJSONObject(0).getString("WorkReview").isEmpty())) {
                    totalUserReview += Integer.parseInt(jsonArray.getJSONObject(0).getString("WorkReview"));
                }
                Log.v("Total review", String.valueOf(totalUserReview));
                getFirstName = jsonArray.getJSONObject(0).getString("firstName");
                getLastName = jsonArray.getJSONObject(0).getString("lastName");
                getEmail = email;
                getLocation = location;
                getMobile = mobile;

                //set values
                titleEditProfile.setText(userName);
                profileName.setText(userName);
                profileEmail.setText(email);
                userMobileNumber.setText(mobile);
                userLocation.setText(location);
                reviewCount.setText(String.valueOf(totalUserReview));
                //userNameComment.setText(userName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * user all reviews
     */
    class UserAllReviews extends AsyncTask<String, Void, String> {

        final String URL = AppConfig.getWebServiceUrl() + "review/UserByDoctorReviews";

        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("userId", userId));
            nameValuePairs.add(new BasicNameValuePair("lang", LocalInformation.getLocaleLang()));
            try {
                HttpPost httpPost = new HttpPost(URL);
                Log.v("review url", URL);
                Log.v("param", nameValuePairs.toString());
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();

                return mClient.execute(httpPost, responseHandler);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            new RenderAllUserReview(result);
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
            //mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
            //mProgressDialog.setCancelable(false);
            //mProgressDialog.show();
        }
    }

    public void updateProfileName(final String userId, final String firstName, final String lastName) {

        class UpdateProfileName extends AsyncTask<String, Void, String> {
            final String URL = AppConfig.getWebServiceUrl() + "profile/userProfileUpdateFullName";
            String quickFirstName = firstName;
            String quickLastName = lastName;
            String quickUserId = userId;

            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<>();
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("firstName", quickFirstName));
                nameValuePairs.add(new BasicNameValuePair("lastName", quickLastName));
                Log.v("Nam value pair", nameValuePairs.toString());
                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();

                    return mClient.execute(httpPost, responseHandler);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    titleEditProfile.setText(getFirstName + " " + getLastName);
                    profileName.setText(getFirstName + " " + getLastName);
                    Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    profileUsername = getFirstName + " " + getLastName.charAt(0);
                    Preference.setValue(getContext(), "PREF_FNAME", profileUsername);
                    newuser.setText(profileUsername);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }

        UpdateProfileName updateProfileName = new UpdateProfileName();
        updateProfileName.execute(userId, firstName, lastName);
    }


    @Override
    public void onResume(){
        super.onResume();
        String state =Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(getContext().getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
    }

    class RenderAllUserReview {
        RenderAllUserReview(String result) {
            Log.v("review ", result);

            LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.userAllReviewList);
            linearLayout.removeAllViews();

            linearLayout.setGravity(Gravity.START);

            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            );

            try {
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.you_have_not_given_any_review));
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    linearLayout.addView(textView);
                } else {
                    JSONArray reviewResult = jsonObject.getJSONArray("result");

                    //badges
                    int totalReviewCount = reviewResult.length();
                    if (totalReviewCount > 0 && totalReviewCount <= 10) {
                        badgesCount.setText("1");
                    } else if (totalReviewCount > 10 && totalReviewCount < 100) {
                        badgesCount.setText("2");
                    } else if (totalReviewCount > 100) {
                        badgesCount.setText("3");
                    }
                    for (int i = 0; i < reviewResult.length(); i++) {
                        final JSONObject reviewData = reviewResult.getJSONObject(i);

                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.user_all_reviews, null);

                        TextView textView = (TextView) view.findViewById(R.id.comment_user_name);
                        if (reviewData.getString("doctorName").equals("null")) break;
                        textView.setText(reviewData.getString("doctorName"));
                        textView.setTypeface(typeface);

                        TextView textView1 = (TextView) view.findViewById(R.id.user_name_comment);
                        textView1.setText(reviewData.getString("doctorName"));
                        textView1.setTypeface(typeface, typeface.BOLD);

                        TextView textView2 = (TextView) view.findViewById(R.id.comment_time);
                        textView2.setTypeface(typeface);
                        textView2.setText(reviewData.getString("commentTime"));

                        TextView userReview = (TextView) view.findViewById(R.id.userReviewComment);
                        userReview.setText(reviewData.getString("comment"));
                        userReview.setTypeface(typeface);

                        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.userRating);
                        ratingBar.setRating(Float.valueOf(reviewData.getString("avgReview")));

                        ImageView editUserReview = (ImageView) view.findViewById(R.id.userReview);
                        editUserReview.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    editUserReview(reviewData.getString("doctorId"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        //doctor or work type profile
                        final String profileId = reviewData.getString("doctorId");
                        final String profileName = reviewData.getString("doctorName");
                        String iconId = "";
                        if (reviewData.has("reviewIcon"))
                            iconId = reviewData.getString("reviewIcon");
                        Log.v("icon", iconId);

                        ImageView profilePic = (ImageView) view.findViewById(R.id.profilePic);
                        if (reviewData.getString("workType").equals("Doctor")) {
                            profilePic.setImageResource(R.mipmap.most_visited_doctor);
                            view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    if(! NetworkInfoHelper.isOnline(getContext())){
                                        Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    doctorProfile(profileId, profileName);
                                }
                            });

                            /*if (! (iconId.equals("0") || iconId.equals("")) ) {
                                int drawableId = getContext().getResources().getIdentifier("doctor_" + iconId,
                                        "drawable", getContext().getPackageName());
                                ImageView drawableIcon = (ImageView) view.findViewById(R.id.drawableIcon);
                                drawableIcon.setImageResource(drawableId);
                            }*/
                        } else if (reviewData.getString("workType").equals("Hospital")) {
                            profilePic.setImageResource(R.mipmap.hospital_dark);
                            view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if(! NetworkInfoHelper.isOnline(getContext())){
                                        Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    hospitalProfile(profileId, profileName);
                                }
                            });
                            /*if (! (iconId.equals("0") || iconId.equals("")) ) {
                                int drawableId = getContext().getResources().getIdentifier("hospital_review_" + iconId,
                                        "drawable", getContext().getPackageName());
                                ImageView drawableIcon = (ImageView) view.findViewById(R.id.drawableIcon);
                                drawableIcon.setImageResource(drawableId);
                            }*/
                        } else if (reviewData.getString("workType").equals("Lab")
                                || reviewData.getString("workType").equals("Radiology Lab")
                                || reviewData.getString("workType").equals("Medical Lab")) {
                            profilePic.setImageResource(R.mipmap.lab_dark);
                            view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if(! NetworkInfoHelper.isOnline(getContext())){
                                        Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    labProfile(profileId, "", "");
                                }
                            });
                            /*if (! (iconId.equals("0") || iconId.equals("")) ) {
                                int drawableId = getContext().getResources().getIdentifier("hospital_review_" + iconId,
                                        "drawable", getContext().getPackageName());
                                ImageView drawableIcon = (ImageView) view.findViewById(R.id.drawableIcon);
                                drawableIcon.setImageResource(drawableId);
                            }*/

                        } else if (reviewData.getString("workType").equals("Clinic")) {
                            profilePic.setImageResource(R.mipmap.clinic_dark);
                            view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if(! NetworkInfoHelper.isOnline(getContext())){
                                        Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    clinicProfile(profileId, "", "");
                                }
                            });
                            /*if (! (iconId.equals("0") || iconId.equals("")) ) {
                                int drawableId = getContext().getResources().getIdentifier("hospital_review_" + iconId,
                                        "drawable", getContext().getPackageName());
                                ImageView drawableIcon = (ImageView) view.findViewById(R.id.drawableIcon);
                                drawableIcon.setImageResource(drawableId);
                            }*/
                        }


                        linearLayout.addView(view);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.v("Review result", result);
        }
    }

    /**
     * clinic profile
     * @param id string
     * @param mipmap string
     * @param speciality string
     */
    public void clinicProfile(String id, String mipmap, String speciality) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("mipmap", mipmap);
        args.putString("speciality", speciality);

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        ClinicProfileFragment clinicProfileFragment = new ClinicProfileFragment();
        clinicProfileFragment.setArguments(args);

        mFragmentManager.popBackStack("clinicProfile",FragmentManager.POP_BACK_STACK_INCLUSIVE);

        mFragmentTransaction.addToBackStack("clinicProfile");
        //mFragmentTransaction.replace(R.id.primary_layout, clinicProfileFragment).commit();
        mFragmentTransaction.replace(R.id.search_layout, clinicProfileFragment).commit();

        TabFragment.tabLayout.getTabAt(1).select();
    }

    /**
     * hospital profile
     * @param id string
     * @param name string
     */
    public void hospitalProfile(final String id, final String name) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("hospitalName", name);

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        HospitalProfileFragment doctorprofile = new HospitalProfileFragment();
        doctorprofile.setArguments(args);

        mFragmentManager.popBackStack("hospitalProfile",FragmentManager.POP_BACK_STACK_INCLUSIVE);

        mFragmentTransaction.addToBackStack("hospitalProfile");
       // mFragmentTransaction.replace(R.id.primary_layout, doctorprofile).commit();

        mFragmentTransaction.replace(R.id.search_layout, doctorprofile).commit();
        TabFragment.tabLayout.getTabAt(1).select();
    }

    /**
     * lab profile
     * @param id string
     * @param mipmap string
     * @param speciality string
     */
    public void labProfile(String id, String mipmap, String speciality) {
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("mipmap", mipmap);
        args.putString("speciality", speciality);

        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        LabProfileFragment labProfileFragment = new LabProfileFragment();
        labProfileFragment.setArguments(args);

        mFragmentManager.popBackStack("labProfile",FragmentManager.POP_BACK_STACK_INCLUSIVE);

        mFragmentTransaction.addToBackStack("labProfile");
//        mFragmentTransaction.replace(R.id.primary_layout, labProfileFragment).commit();

        mFragmentTransaction.replace(R.id.search_layout, labProfileFragment).commit();
        TabFragment.tabLayout.getTabAt(1).select();
    }

    public void doctorProfile(final String doctorId, final String doctorName) {
        Bundle args = new Bundle();
        args.putString("id", doctorId);
        args.putString("name", doctorName);
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        DoctorProfileFragment clinicProfileFragment = new DoctorProfileFragment();
        clinicProfileFragment.setArguments(args);

        mFragmentManager.popBackStack("doctorProfile",FragmentManager.POP_BACK_STACK_INCLUSIVE);

        mFragmentTransaction.addToBackStack("doctorProfile");
//        mFragmentTransaction.add(R.id.primary_layout, clinicProfileFragment).commit();

        mFragmentTransaction.add(R.id.search_layout, clinicProfileFragment).commit();
        TabFragment.tabLayout.getTabAt(1).select();


    }

    public void editUserReview(final String doctorId) {
        final String userId = Preference.getValue(getContext(), "LOGIN_ID", "");
        Log.v("User Id", userId);
        Log.v("Doctor Id", doctorId);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        Bitmap bmp;
        if(resultCode != getActivity().RESULT_OK){
            imageView.setVisibility(View.GONE);
            return;
        }else{
            try {
                InputStream inputStream = getContext().getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
//                startCropImage();

                bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());

                Log.d("Bitmap","Bitmap"+bitmap);
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageBitmap(bitmap);


            } catch (Exception ex) {  ex.printStackTrace();                  }

          }

//        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
//            Uri filePath = data.getData();
//            String fileDataPath = filePath.toString();
//
//            try {
//                String path = getRealPathFromURI(filePath);
//                if(!MaxSizeImage(path)){
//
//                    Toast.makeText(getContext(), getResources().getString(R.string.image_size_error), Toast.LENGTH_LONG).show();
//                    return;
//                }
//                //Getting the Bitmap from Gallery
//                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), filePath);
//                //Bitmap resized = Bitmap.createScaledBitmap(bitmap, 200, 200, true);
//
//                /*ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
//                byte[] imageInByte = stream.toByteArray();
//                long lengthbmp = imageInByte.length;
//                Log.v("Image size", String.valueOf(lengthbmp));*/
//
//                //Setting the Bitmap to ImageView
//                imageView.setImageBitmap(bitmap);
//                imageView.setVisibility(View.VISIBLE);
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (Exception e){
//                e.printStackTrace();
//            }


//        } else {
//            imageView.setVisibility(View.GONE);
//        }
    }

    /*protected int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }*/

    /*private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(
                getContext().getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(
                getContext().getContentResolver().openInputStream(selectedImage), null, o2);
    }*/




    /**
     * Image to string
     * @param bmp
     * @return string
     */
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 70, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    /**
     * Show file chooser
     */
    private void showFileChooser() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

        //New Changes(4-8-2017)

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, PICK_IMAGE_REQUEST);
    }


    /**
     * Upload Image
     */
    private void uploadImage(){
        //Showing the progress dialog
        if (imageView.getDrawable() == null) {
            //Log.v("Image data: ", "Image not selected");
            Toast.makeText(getContext(), "Please upload profile pic", Toast.LENGTH_LONG).show();
        } else {
            loading = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
            loading.setMessage(getResources().getString(R.string.please_wait));
            loading.setCancelable(false);
            loading.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            loading.dismiss();
                            //Showing toast message of the response
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                Log.v("Response", jsonObject.toString());
                                Toast.makeText(getContext(), jsonObject.getString("message") , Toast.LENGTH_LONG).show();
                                //new DownloadImageTask((ImageView) getView().findViewById(R.id.user_profile_default_image))
                                //        .execute("https://3.bp.blogspot.com/-yEE6FqiglKw/VeBH_MmGGzI/AAAAAAAAbHE/HUYcYvkIwl0/s1600/AndroidImageViewSetImageFromURL2.png");
                                //Picasso.with(getContext()).invalidate(jsonObject.getString("data"));
                                /*Picasso.with(getContext())
                                        .load(jsonObject.getString("data"))
                                        .resize(50, 50)
                                        .centerCrop()
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into((ImageView) getView().findViewById(R.id.user_profile_default_image));*/
                                if (jsonObject.getString("status").equals("true")) {
                                    //user_profile_default_image.setImageDrawable(imageView.getDrawable());
                                    //set navigation drawer profile image
                                    //ImageView newUploadedImage = (ImageView) getView().findViewById(R.id.user_profile_default_image);
                                   new DownloadImageTask(user_profile_default_image)
                                            .execute(jsonObject.getString("data"));
                                    //navProfilePic.setImageDrawable(user_profile_default_image.getDrawable());
                                    //Bitmap bitmap = ((BitmapDrawable) newUploadedImage.getDrawable()).getBitmap();
                                    Preference.setValue(getContext(), "PROFILE_PIC", jsonObject.getString("data"));
                                }
                                /*new DownloadImageTask(user_profile_default_image)
                                        .execute(jsonObject.getString("data"));*/


                                //navProfilePic.setImageDrawable(newUploadedImage.getDrawable());
                                /*new DownloadImageTask(navProfilePic)
                                        .execute(jsonObject.getString("data"));*/
                                //navProfilePic.setImageDrawable(newUploadedImage.getDrawable());
                                //ImageView newProfileImage = (ImageView) hView.findViewById(R.id.imageView);
                                //TextView newuser = (TextView) hView.findViewById(R.id.login_user_name);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            try {
                                loading.dismiss();

                                //Showing toast
                                Log.v("Error", volleyError.getMessage().toString());
                                Toast.makeText(getContext(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                            } catch (NullPointerException ne) {
                                ne.printStackTrace();
                            }
                            catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Converting Bitmap to String
                    String image = null;
                    Map<String,String> params = new Hashtable<String, String>();
                    try {
                        image = getStringImage(bitmap);
                        Log.v("Image Data", image);
                        //Getting Image Name
                        //String name = editTextName.getText().toString().trim();

                        //Creating parameters

                        //Adding parameters
                        //params.put("name", getName);
                        //params.put("clinic_name", getClinicName);
                        params.put("userId", loginUserId);
                        //params.put("mobile", getMobile);

                        //params.put("address", getCLinicAddress);
                        //params.put("speciality", getSpeciality);

                        params.put("lang", LocalInformation.getLocaleLang());
                        params.put(KEY_IMAGE, image);

                        //returning parameters

                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Please select image", Toast.LENGTH_LONG).show();
                    }

                    return params;
                }
            };

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());

            //Adding request to the queue
            requestQueue.add(stringRequest);
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            if (result != null)
                user_profile_default_image.setImageBitmap(result);
            navProfilePic.setImageBitmap(result);
            progressBar.setVisibility(View.GONE);
            profileDetails.setVisibility(View.VISIBLE);
            CountryData.getmInstance();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (userProfileDetailsAsynck != null)
            userProfileDetailsAsynck.cancel(true);
        if (userAllReviews != null)
            userAllReviews.cancel(true);
    }

    public boolean MaxSizeImage(String imagePath) {
        boolean temp = false;
        File file = new File(imagePath);
        long length = file.length();

        if (length < 1500000) // 1.5 mb
            temp = true;

        return temp;
    }


    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    public String getImagePath(Uri uri){
       Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor.close();

        cursor = getContext().getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;

    }

    /*private void networkStatus()
    {
        AlertDialogHelper alert = new AlertDialogHelper();
        if(!NetworkInfoHelper.isOnline(getContext()))
        {
            if(alert.retryAlert(getContext())) {
                // retrying network status
                networkStatus();
            }
            else {
                // do nothing..
                isOn = false;
            }
        }
        else{
            isOn = true;
        }
    }*/

    public static void copyStream(InputStream input, OutputStream output)    throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
    }
}