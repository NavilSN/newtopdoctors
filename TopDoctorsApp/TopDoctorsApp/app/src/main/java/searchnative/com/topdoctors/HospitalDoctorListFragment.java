package searchnative.com.topdoctors;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by mayur on 28/9/16.
 */

public class HospitalDoctorListFragment extends Fragment
    implements ScrollViewListener {

    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    TabFragment tabFragment = new TabFragment();
    final String WEB_SERVICE_URL = AppConfig.getWebServiceUrl();
    private ProgressDialog mProgressDialog;
    private Typeface typeface;
    private String hospitalId, hospitalName, hospitalAddress, hospitalPhone;
    private LinearLayout linearLayout;
    private List specialityIcon;
    private ImageView hospitalSocialShare;
    private String appLang = LocalInformation.getLocaleLang();
    private String loginUserId;
    private LinearLayout hospitalDoctorLists;
    private ProgressBar progressBar;
    private ScrollViewExt doctor_list_scroll;

    //asynck objects
    HospitalDoctorList hospitalDoctorList;
    //DoctorBookmarksAddUpdate doctorBookmarksAddUpdate;

    private static boolean isAsynchOn = true;
    private int totalSearchResult = 0, totalResultRendered = 0, totalRequestNo = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getContext(), new Crashlytics());
        View view = inflater.inflate(R.layout.hospital_doctor_list, container, false);

        return view;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.v("Inside", "doctor list");
        hospitalId = getArguments().getString("id");
        hospitalName = getArguments().getString("hospitalName");
        hospitalAddress = getArguments().getString("hospitalAddress");
        hospitalPhone = getArguments().getString("hospitalPhone");
        doctor_list_scroll = (ScrollViewExt) getView().findViewById(R.id.doctor_list_scroll);
        doctor_list_scroll.setScrollViewListener(this);

        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        hospitalDoctorLists = (LinearLayout) getView().findViewById(R.id.hospital_doctor_lists);
        hospitalDoctorLists.setVisibility(View.INVISIBLE);

        //linearLayout = (LinearLayout) getView().findViewById(R.id.hospital_doctor_lists);
        //linearLayout.removeAllViews();
        //Toast.makeText(getContext(), hospitalName, Toast.LENGTH_SHORT).show();
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ExoMedium.otf");
        TextView profile_detail_name = (TextView) getView().findViewById(R.id.hospital_doctor_list_profile_detail_name);
        profile_detail_name.setTypeface(typeface, typeface.BOLD);
        profile_detail_name.setText(hospitalName);

        loginUserId = Preference.getValue(getContext(), "LOGIN_ID", "");



        hospitalDoctorList = new HospitalDoctorList();
        hospitalDoctorList.execute(hospitalId);

        specialityIcon = new ArrayList();
        specialityIcon.addAll(SpecialityData.getmInstance().specialityListIcon);

        ImageView imageView = (ImageView) getView().findViewById(R.id.hospital_doctor_lists_swipe_menu);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawerLayout);
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        hospitalSocialShare = (ImageView) getView().findViewById(R.id.hospital_social_share);
        hospitalSocialShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareIt(hospitalName, hospitalPhone, hospitalAddress, "");
            }
        });
    }

    @Override
    public void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy) {
        View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
        int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

        if (diff == 0) {
            //check for Internet
            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                return;
            } else {
                if (totalSearchResult > totalResultRendered) {
                    if (isAsynchOn) {
                        isAsynchOn = false;
                        hospitalDoctorList = new HospitalDoctorList();
                        hospitalDoctorList.execute();
                    }
                }
            }
        }
    }

    class HospitalDoctorList extends AsyncTask<String, Void, String> {
        String URL = WEB_SERVICE_URL + "profile/workTypeTotalDoctorsList";
        //String URL = "http://192.168.1.28/topdoctor/api/profile/workTypeTotalDoctorsList";

        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        String quickHospitalId = hospitalId;

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("workId", quickHospitalId));
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));
            nameValuePairs.add(new BasicNameValuePair("page", String.valueOf(totalRequestNo)));
            Log.v("URL", URL);
            Log.v("params", nameValuePairs.toString());
            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            //Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
            if(result != null){
                new RenderSearchResult(result);
            }else{
                if(getContext() != null){
                    Toast.makeText(getContext(), getResources().getString(R.string.error_message),
                            Toast.LENGTH_SHORT).show();
                }
            }

            progressBar.setVisibility(View.GONE);
            hospitalDoctorLists.setVisibility(View.VISIBLE);
            isAsynchOn = true;
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            totalRequestNo++;
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
        }
    }

    public class RenderSearchResult {
        RenderSearchResult(String result) {
            linearLayout = (LinearLayout) getView().findViewById(R.id.hospital_doctor_lists);
            if (totalRequestNo <= 1)
                linearLayout.removeAllViews();
            linearLayout.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            );
            try {
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.no_data_found));
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    //set counter
                    totalSearchResult = 0;
                    totalResultRendered = 0;

                    linearLayout.addView(textView);
                }
                else {
                    JSONArray searchResult = jsonObject.getJSONArray("data");
                    Log.v("Result: ", searchResult.toString());
                    if (totalRequestNo <= 1)
                        totalSearchResult = jsonObject.getInt("total_rows");
                    for(int i = 0; i < searchResult.length(); i++) {
                        final JSONObject filterData = searchResult.getJSONObject(i);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.search_result, null);

                        //speciality icon

                        ImageView doctorSpecialityIcon = (ImageView) view.findViewById(R.id.speciality);
                        try{

                        int id = getContext().getResources().getIdentifier(
                                specialityIcon.get(getCategoryPos(filterData.getString("Mipmap"))).toString(),
                                "mipmap", getContext().getPackageName());
                        doctorSpecialityIcon.setImageResource(id);

                        }catch (Exception e)
                        {
                            Log.v("Exception:",""+e.getMessage());
                        }
                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.search_title_work_place);
                        textView1.setText(filterData.getString("Name"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));
                        final String doctorName = filterData.getString("Name");

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.search_work_place_address);
                        textView2.setText(filterData.getString("Address"));
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));
                        final String doctorAddress = filterData.getString("Address");

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.search_work_place_phone);
                        textView3.setText(filterData.getString("Phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));
                        final String doctorPhone = filterData.getString("Phone");

                        //rating
                        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.doctor_user_rating);
                        if(!filterData.getString("RatingAvg").equals("null") &&
                                !filterData.getString("RatingAvg").isEmpty()) {
                            ratingBar.setRating(Float.valueOf(filterData.getString("RatingAvg")));

                        }
                        final String doctorRating = String.valueOf(filterData.getString("RatingAvg"));

                        //total reviews
                        TextView textView4 = (TextView) view.findViewById(R.id.search_total_reviews);
                        if(!filterData.getString("TotalReview").equals("null") &&
                                !filterData.getString("TotalReview").isEmpty()) {
                            textView4.setText(filterData.getString("TotalReview") + " " + getResources().getString(R.string.reviews));
                        } else {
                            textView4.setText(0 + getResources().getString(R.string.reviews));
                        }
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));

                        linearLayout.addView(view);

                        final String callDialer = filterData.getString("Phone");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.search_phone_dialer);
                        imageView.setId(imageView.getId() + 100000 + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
                                builderSingle.setIcon(R.mipmap.call);
                                builderSingle.setTitle(R.string.select_one_number);

                                String search = "--";
                                String search1 = "-";
                                String[] array = new String[10];
                                if (callDialer.indexOf(search) != -1) {
                                    array = callDialer.replaceAll(" ", "").split("\\--", -1);
                                } else if (callDialer.indexOf(search1) != -1) {
                                    array = callDialer.replaceAll(" ", "").split("\\-", -1);
                                }

                                if (array.length == 10) {
                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                    intent.setData(Uri.parse("tel:" + callDialer.trim()));
                                    startActivity(intent);
                                } else {
                                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
                                    arrayAdapter.addAll(array);

                                    builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                                    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            String strName = arrayAdapter.getItem(which);
                                            Intent intent = new Intent(Intent.ACTION_DIAL);
                                            intent.setData(Uri.parse("tel:" + strName));
                                            startActivity(intent);

                                        }
                                    });
                                    builderSingle.show();
                                }
                            }
                        });

                        //add to bookmarks
                        //doctor details
                        final LinearLayout withBookmark = (LinearLayout) view.findViewById(R.id.withBookmark);
                        final LinearLayout withoutBookmark = (LinearLayout) view.findViewById(R.id.withoutBookmark);
                        final String detailsId =  filterData.getString("DoctorId");
                        final String name = filterData.getString("Name");

                        final ImageView addToBookmarks = (ImageView) view.findViewById(R.id.add_to_bookmarks);
                        addToBookmarks.setTag(1);
                        String bookmarkStatus = filterData.getString("BookmarkStatus");
                        if ( ! (bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                            String bookmarkUserId = filterData.getString("BookmarkUserId");
                            //String bookmarkUserStatus = filterData.getString("BookmarkStatus");
                            if (bookmarkUserId.equals(loginUserId) && bookmarkStatus.equals("1")) {
                                addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                addToBookmarks.setTag(2);
                                withBookmark.setVisibility(View.VISIBLE);
                                withoutBookmark.setVisibility(View.GONE);
                            }
                        }
                        //addToBookmarks.setId(addToBookmarks.getId() + i + 10000 + i);
                        addToBookmarks.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                doctorBookMarks(detailsId);
                                if (Integer.parseInt(addToBookmarks.getTag().toString()) == 1) {
                                    addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                                    addToBookmarks.setTag(2);
                                    withBookmark.setVisibility(View.VISIBLE);
                                    withoutBookmark.setVisibility(View.GONE);
                                } else {
                                    addToBookmarks.setImageResource(R.mipmap.bookmarks);
                                    addToBookmarks.setTag(1);
                                    withBookmark.setVisibility(View.GONE);
                                    withoutBookmark.setVisibility(View.VISIBLE);
                                }
                            }
                        });

                        LinearLayout showDetails = (LinearLayout) view.findViewById(R.id.show_details);
                        showDetails.setId(getId() + i);
                        showDetails.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                doctorProfile(detailsId, name);
                            }
                        });

                        /*ImageView socialShare = (ImageView) view.findViewById(R.id.share_details);
                        socialShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                doctorProfile(detailsId, name);
                            }
                        });*/

                        /*final ImageView socialShare = (ImageView) view.findViewById(R.id.share_details);
                        socialShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //Log.v("Name: ", name);
                                //shareIt(doctorName, doctorPhone, doctorAddress, doctorRating);
                                doctorProfile(detailsId, name);
                            }
                        });*/
                    }
                    totalResultRendered += searchResult.length();
                    Log.v("total rendered", String.valueOf(totalResultRendered));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    public void doctorProfile(final String doctorId, final String doctorName) {
        Bundle args = new Bundle();
        args.putString("id", doctorId);
        args.putString("name", doctorName);
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        DoctorProfileFragment clinicProfileFragment = new DoctorProfileFragment();
        clinicProfileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("doctorProfile");
        mFragmentTransaction.add(R.id.search_layout, clinicProfileFragment).commit();
    }

    /**
     * Share it on social
     * @param name string
     * @param phone string
     * @param address string
     */
    public void shareIt(final String name, final String phone, final String address, final String rating) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Name: " + name + "\n" + "Phone: " + phone + "\n" + "Address: "
                + address + "\n" + "Rating: " + rating);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.social_text)));

    }

    public void doctorBookMarks(final String doctorId) {

        class DoctorBookmarksAddUpdate extends AsyncTask<String, Void, String> {

            String URL = WEB_SERVICE_URL + "bookmark/add";

            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            String quickDoctorId = doctorId;
            String quickUserId = Preference.getValue(getContext(), "LOGIN_ID", "");

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("doctorId", quickDoctorId));
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("lang", appLang));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    return mClient.execute(httpPost, responseHandler);
                } catch(IOException e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    Log.v("result", jsonObject.toString());
                    String responseResult = jsonObject.getString("result");
                    String doctorName = jsonObject.getString("name");
                    if(responseResult.equals("Bookmarked")) {
                        //addToBookmarks.setImageResource(R.mipmap.add_a_lab);
                        Toast.makeText(getContext(), doctorName + " is successfully added in your bookmark.", Toast.LENGTH_SHORT).show();
                    } else {
                        //addToBookmarks.setImageResource(R.mipmap.bookmarks);
                        Toast.makeText(getContext(), doctorName + " is successfully removed from your bookmark.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                /*mProgressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
            }
        }

        DoctorBookmarksAddUpdate doctorBookmarksAddUpdate = new DoctorBookmarksAddUpdate();
        doctorBookmarksAddUpdate.execute(doctorId);
    }

    private int getCategoryPos(String category) {
        return specialityIcon.indexOf(category);
    }

    @Override
    public void onStop() {
        super.onStop();
        hospitalDoctorList.cancel(true);
    }
}
