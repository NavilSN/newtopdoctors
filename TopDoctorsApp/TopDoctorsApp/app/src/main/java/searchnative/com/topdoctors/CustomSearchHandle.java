package searchnative.com.topdoctors;

import android.content.Context;

/**
 * Created by root on 24/11/16.
 */

public class CustomSearchHandle {

    public static void removeDoctorSearch(Context context) {
        Preference.setValue(context, "DOCTOR_SEARCH_CUSTOM", "");
        Preference.setValue(context, "DOCTOR_SEARCH_PAGINATE", "");
    }

    public static void removeLabSearch(Context context) {
        Preference.setValue(context, "LAB_SEARCH_GENDER", "");
        Preference.setValue(context, "LAB_SEARCH_SPECIALITY", "");
        Preference.setValue(context, "LAB_SEARCH_CUSTOM", "");
    }

    public static void removeHospitalSearch(Context context) {
        Preference.setValue(context, "HOSPITAL_SEARCH", "");
    }

    public static void removeClinicSearch(Context context) {
        Preference.setValue(context, "CLINIC_SEARCH", "");
    }

    public static void removeBookmarkSearch(Context context) {
        Preference.setValue(context, "USER_BOOKMARKS", "");
    }

    public static void removePrimarySearch(Context context) {
        Preference.setValue(context, "PRIMARY_SEARCH", "");
        Preference.setValue(context, "PROMARY_SEARCH_KEY", "");
    }

    public static void removeSearchRedirectSearch(Context context) {
        Preference.setValue(context, "SEARCH_REDIRECT", "");
    }

    public static void removeAllSearchFilters(Context context) {
        removeDoctorSearch(context);
        removeLabSearch(context);
        removeHospitalSearch(context);
        removeClinicSearch(context);
        removeBookmarkSearch(context);
        removePrimarySearch(context);
        removeSearchRedirectSearch(context);
    }

    public static void setDoctorSearchPaginate(Context context) {
        Preference.setValue(context, "DOCTOR_SEARCH_PAGINATE", "true");
    }

    public static void setHospitalSearchpaginate(Context context) {
        Preference.setValue(context, "HOSPITAL_SEARCH_PAGINATE", "true");
    }

    public static boolean getHospitalSearchPaginate(Context context) {
        boolean isPaginate = false;
        if (Preference.getValue(context, "HOSPITAL_SEARCH_PAGINATE", "").equals("true")) {
            isPaginate = true;
        }

        return isPaginate;
    }

    public static void setClinicPaginate(Context context) {
        Preference.setValue(context, "CLINIC_SEARCH_PAGINATE", "true");
    }

    public static boolean getClinicPaginate(Context context) {
        boolean isPaginate = false;
        if (Preference.getValue(context, "CLINIC_SEARCH_PAGINATE", "").equals("true")) {
            isPaginate = true;
        }

        return isPaginate;
    }

    public static void setLabPaginate(Context context) {
        Preference.setValue(context, "LAB_SEARCH_PAGINATE", "true");
    }

    public static void setAllWorkMasterPaginate(Context context) {
        Preference.setValue(context, "WORK_MASTER_PAGINATE", "true");
    }

    public static boolean getAllWorkMasterPaginate(Context context) {
        boolean isPaginated = false;
        if (Preference.getValue(context, "WORK_MASTER_PAGINATE", "").equals("true")) {
            isPaginated = true;
        }

        return isPaginated;
    }

    public static void setGlobalSearch(Context context) {
        Preference.setValue(context, "GLOBAL_SEARCH_OPTION", "true");
    }

    public static void resetGlobalSearch(Context context) {
        Preference.setValue(context, "GLOBAL_SEARCH_OPTION", "");
    }

    public static void setGlobalSearchPaginate(Context context) {
        Preference.setValue(context, "GLOBAL_SEARCH_PAGINATE", "true");
    }

    public static void setFilterSearchPaginate(Context context) {
        Preference.setValue(context, "FILTER_SEARCH_PAGINATE", "true");
    }

    public static void setGlobalSearchType(Context context) {
        Preference.setValue(context, "GLOBAL_FILTER_SEARCH", "true");
    }

    public static boolean getGlobalSearchType(Context context) {
        boolean isSearch = false;
        if (Preference.getValue(context, "GLOBAL_FILTER_SEARCH", "").equals("true")) {
            isSearch = true;
        }
        return isSearch;
    }

    public static void resetGlobalSearchType(Context context) {
        Preference.setValue(context, "GLOBAL_FILTER_SEARCH", "");
    }

    public static boolean getFilterSearchPaginate(Context context) {
        boolean isPaginate = false;
        if (Preference.getValue(context, "FILTER_SEARCH_PAGINATE", "").equals("true")) {
            isPaginate = true;
        }

        return isPaginate;
    }

    public static boolean getGlobalSearchPaginate(Context context) {
        boolean isPaginate = false;
        if (Preference.getValue(context, "GLOBAL_SEARCH_PAGINATE", "").equals("true")) {
            isPaginate = true;
        }

        return isPaginate;
    }

    public static void setAllWorkMasterSearch(Context context) {
        Preference.setValue(context, "ALL_WORK_MASTER", "true");
    }

    public static void resetAllWorkMasterSearch(Context context) {
        Preference.setValue(context, "ALL_WORK_MASTER", "");
    }

    public static boolean getLabPaginate(Context context) {
        boolean isPaginate = false;
        if (Preference.getValue(context, "LAB_SEARCH_PAGINATE", "").equals("true")) {
            isPaginate = true;
        }
        return isPaginate;
    }

    public static void resetBookmarkPaginate(Context context) {
        Preference.setValue(context, "HOSPITAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "CLINIC_SEARCH_PAGINATE", "");
        Preference.setValue(context, "LAB_SEARCH_PAGINATE", "");
        Preference.setValue(context, "DOCTOR_SEARCH_PAGINATE", "");
        Preference.setValue(context, "WORK_MASTER_PAGINATE", "");
        Preference.setValue(context, "GLOBAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "IS_NEARBY", "");
        Preference.setValue(context, "TOP_RATED_FIRST", "");
//        Preference.setValue(context, "Latitude", "");
//        Preference.setValue(context, "Longitude", "");
    }

    public static void resetNearbyPaginate(Context context) {
        Preference.setValue(context, "HOSPITAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "CLINIC_SEARCH_PAGINATE", "");
        Preference.setValue(context, "LAB_SEARCH_PAGINATE", "");
        Preference.setValue(context, "BOOKMARK_SEARCH_PAGINATE", "");
        Preference.setValue(context, "DOCTOR_SEARCH_PAGINATE", "");
        Preference.setValue(context, "WORK_MASTER_PAGINATE", "");
        Preference.setValue(context, "GLOBAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "IS_NEARBY", "");
        Preference.setValue(context, "TOP_RATED_FIRST", "");
//        Preference.setValue(context, "Latitude", "");
//        Preference.setValue(context, "Longitude", "");
    }

    public static void resetFilterSearchPaginate(Context context) {
        Preference.setValue(context, "HOSPITAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "CLINIC_SEARCH_PAGINATE", "");
        Preference.setValue(context, "LAB_SEARCH_PAGINATE", "");
        Preference.setValue(context, "BOOKMARK_SEARCH_PAGINATE", "");
        Preference.setValue(context, "DOCTOR_SEARCH_PAGINATE", "");
        Preference.setValue(context, "WORK_MASTER_PAGINATE", "");
        Preference.setValue(context, "GLOBAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "IS_NEARBY", "");
        Preference.setValue(context, "TOP_RATED_FIRST", "");
//        Preference.setValue(context, "Latitude", "");
//        Preference.setValue(context, "Longitude", "");
    }

    public static void resetPaginateForGlobalSearch(Context context) {
        Preference.setValue(context, "HOSPITAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "CLINIC_SEARCH_PAGINATE", "");
        Preference.setValue(context, "LAB_SEARCH_PAGINATE", "");
        Preference.setValue(context, "BOOKMARK_SEARCH_PAGINATE", "");
        Preference.setValue(context, "DOCTOR_SEARCH_PAGINATE", "");
        Preference.setValue(context, "WORK_MASTER_PAGINATE", "");
        Preference.setValue(context, "IS_NEARBY", "");
        Preference.setValue(context, "PRIMARY_SEARCH", "");
        Preference.setValue(context, "PROMARY_SEARCH_KEY", "");
    }

    public static void resetPaginateForWorkMaster(Context context) {
        Preference.setValue(context, "HOSPITAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "CLINIC_SEARCH_PAGINATE", "");
        Preference.setValue(context, "LAB_SEARCH_PAGINATE", "");
        Preference.setValue(context, "BOOKMARK_SEARCH_PAGINATE", "");
        Preference.setValue(context, "DOCTOR_SEARCH_PAGINATE", "");
        Preference.setValue(context, "GLOBAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "IS_NEARBY", "");
    }

    public static void setBookmarkPaginate(Context context) {
        Preference.setValue(context, "BOOKMARK_SEARCH_PAGINATE", "true");
    }

    public static boolean getBookmatkPaginate(Context context) {
        boolean isPaginate = false;
        if (Preference.getValue(context, "BOOKMARK_SEARCH_PAGINATE", "").equals("true")) {
            isPaginate = true;
        }
        return isPaginate;
    }

    public static void resetPaginateCounterForDoctor(Context context) {
        Preference.setValue(context, "HOSPITAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "CLINIC_SEARCH_PAGINATE", "");
        Preference.setValue(context, "LAB_SEARCH_PAGINATE", "");
        Preference.setValue(context, "BOOKMARK_SEARCH_PAGINATE", "");
        Preference.setValue(context, "WORK_MASTER_PAGINATE", "");
        Preference.setValue(context, "GLOBAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "IS_NEARBY", "");
    }

    public static void resetPaginateCounterForHospital(Context context) {
        Preference.setValue(context, "DOCTOR_SEARCH_PAGINATE", "");
        Preference.setValue(context, "CLINIC_SEARCH_PAGINATE", "");
        Preference.setValue(context, "LAB_SEARCH_PAGINATE", "");
        Preference.setValue(context, "BOOKMARK_SEARCH_PAGINATE", "");
        Preference.setValue(context, "WORK_MASTER_PAGINATE", "");
        Preference.setValue(context, "GLOBAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "IS_NEARBY", "");
    }

    public static void resetPaginateCounterForClinic(Context context) {
        Preference.setValue(context, "DOCTOR_SEARCH_PAGINATE", "");
        Preference.setValue(context, "HOSPITAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "LAB_SEARCH_PAGINATE", "");
        Preference.setValue(context, "BOOKMARK_SEARCH_PAGINATE", "");
        Preference.setValue(context, "WORK_MASTER_PAGINATE", "");
    }

    public static void resetPaginateCounterForLab(Context context) {
        Preference.setValue(context, "DOCTOR_SEARCH_PAGINATE", "");
        Preference.setValue(context, "HOSPITAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "CLINIC_SEARCH_PAGINATE", "");
        Preference.setValue(context, "BOOKMARK_SEARCH_PAGINATE", "");
        Preference.setValue(context, "WORK_MASTER_PAGINATE", "");
        Preference.setValue(context, "GLOBAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "IS_NEARBY", "");
    }

    public static void resetAllPaginateCounter(Context context) {
        Preference.setValue(context, "HOSPITAL_SEARCH_PAGINATE", "");
        Preference.setValue(context, "CLINIC_SEARCH_PAGINATE", "");
        Preference.setValue(context, "LAB_SEARCH_PAGINATE", "");
        Preference.setValue(context, "BOOKMARK_SEARCH_PAGINATE", "");
        Preference.setValue(context, "DOCTOR_SEARCH_PAGINATE", "");
        Preference.setValue(context, "WORK_MASTER_PAGINATE", "");
        Preference.setValue(context, "IS_NEARBY", "");
        Preference.setValue(context, "PRIMARY_SEARCH", "");
        Preference.setValue(context, "PROMARY_SEARCH_KEY", "");
    }

    public static void resetPreferencesForStartUp(Context context) {
        
    }

    public static void resetPaginateCounterForBookmarks(Context context) {
        Preference.setValue(context, "GLOBAL_FILTER_TYPE", "");
        Preference.setValue(context, "DOCTOR_LOCATION_SEARCH", "");
        Preference.setValue(context, "DOCTOR_SPECIALITY_SEARCH", "");
        Preference.setValue(context, "DOCTOR_GENDER_SEARCH", "");
        Preference.setValue(context, "TOP_TEN_DOCTORS", "");
        Preference.setValue(context, "SEARCH_REDIRECT", "");
        Preference.setValue(context, "FILTER_NAME", "");
        Preference.setValue(context, "GLOBAL_FILTER_TYPE", "");
        Preference.setValue(context, "CLINIC_SEARCH", "");
        Preference.setValue(context, "HOSPITAL_SEARCH", "");
        Preference.setValue(context, "DOCTOR_SEARCH_CUSTOM", "");
        Preference.setValue(context, "DOCTOR_SPECIALITY_SEARCH", "");
        Preference.setValue(context, "DOCTOR_LOCATION_SEARCH", "");
        Preference.setValue(context, "DOCTOR_GENDER_SEARCH", "");
        Preference.setValue(context, "FILTER_LOCATION", "");
        Preference.setValue(context, "LAB_SEARCH_CUSTOM", "");
        Preference.setValue(context, "USER_BOOKMARKS", "");
        Preference.setValue(context, "PRIMARY_SEARCH", "");
        Preference.setValue(context, "PROMARY_SEARCH_KEY", "");
        Preference.setValue(context, "SEARCH_REDIRECT", "");
        resetAllPaginateCounter(context);
    }
}
