package searchnative.com.topdoctors;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class TabFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 3 ;
    int tabPosition = 0;
//    private AdView mAdView;

    InterstitialAd mInterstitialAd;
    AdRequest adRequest;

    static CountDownTimer timer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View x =  inflater.inflate(R.layout.tab_layout,null);
        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);


//        mAdView = (AdView) x.findViewById(R.id.adView);
         adRequest = new AdRequest.Builder()
                .build();
//        mAdView.loadAd(adRequest);


        mInterstitialAd = new InterstitialAd(getContext());

        // Ad ID
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

        //mInterstitialAd.loadAd(adRequest);

        //startClock();

         timer =new CountDownTimer(69000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
//                Log.v("Timer",millisUntilFinished+"");
            }

            @Override
            public void onFinish() {

                handler.sendEmptyMessage(0);
            }
        }.start();



        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
//                Toast.makeText(getApplicationContext(), "Ad is loaded!", Toast.LENGTH_SHORT).show();

                showInterstitial();
            }

            @Override
            public void onAdClosed() {
//                Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
                timer.start();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
//                Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
                //startClock();
                timer.start();
            }

            @Override
            public void onAdLeftApplication() {
//                Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
//                Toast.makeText(getApplicationContext(), "Ad is opened!", Toast.LENGTH_SHORT).show();
            }
        });


        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        viewPager.setOffscreenPageLimit(3);

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */
        tabLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.v("Long tap", "success" + tabPosition);
                return true;
            }
        });

        /*Tab change event listener*/
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();

                switch (tabPosition) {
                    case 0 :
                        tabLayout.getTabAt(0).setIcon(R.mipmap.near_by);
                        tabLayout.getTabAt(1).setIcon(R.mipmap.search);
                        tabLayout.getTabAt(2).setIcon(R.mipmap.top_10_doctor_bottom_1);
                        break;
                    case 1 :
                        tabLayout.getTabAt(0).setIcon(R.mipmap.near_by_activated);
                        tabLayout.getTabAt(1).setIcon(R.mipmap.search_bottom);
                        tabLayout.getTabAt(2).setIcon(R.mipmap.top_10_doctor_bottom_1);
                        break;
                    case 2 :
                        tabLayout.getTabAt(0).setIcon(R.mipmap.near_by_activated);
                        tabLayout.getTabAt(1).setIcon(R.mipmap.search);
                        tabLayout.getTabAt(2).setIcon(R.mipmap.top_10_doctor_bottom);
                        break;
                    default:
                        tabLayout.getTabAt(0).setIcon(R.mipmap.near_by_activated);
                        tabLayout.getTabAt(1).setIcon(R.mipmap.search);
                        tabLayout.getTabAt(2).setIcon(R.mipmap.top_10_doctor_bottom_1);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
               /*final FragmentTransaction ft = getFragmentManager().beginTransaction();

                tabPosition = tab.getPosition();

                switch (tabPosition) {
                    case 0 :
                        ft.replace(R.id.primary_layout, new PrimaryFragment(), "PrimaryFragment");
                        ft.commit();
                        break;
                    case 1 :
                        ft.replace(R.id.search_layout, new SearchFragment(), "SearchFragment");
                        ft.commit();
                        break;
                    case 2 :

                        break;
                }*/
            }
        });

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
                for (int i = 0; i < tabLayout.getTabCount(); i++) {
                    switch (i){
                        case 0 :
                            tabLayout.getTabAt(0).setIcon(R.mipmap.near_by);
                        case 1 :
                            tabLayout.getTabAt(1).setIcon(R.mipmap.search);
                        case 2 :
                            tabLayout.getTabAt(2).setIcon(R.mipmap.top_10_doctor_bottom_1);
                    }
                }
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==1){
                    BaseActivity.showMenu();
//                    if(BaseActivity.isviewfrom.equals("hospital")){
//                        BaseActivity.showMenu();
//                    }else if(BaseActivity.isviewfrom.equals("")){
//
//                    }
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return x;

    }

    public void switchTab(int tab) {
        tabPosition = tab;
    }

    class MyAdapter extends FragmentPagerAdapter{

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position)
        {
            switch (position){
                case 0 : return new PrimaryFragment();
                case 1 : return new SearchFragment();
                case 2 : return new TopTenDcotorsFragment();
            }
            return null;
        }

        @Override
        public int getCount() {

            return int_items;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }


        private void showInterstitial() {

            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
//            startClock();
        }

    /*private void startThreadDelay()
    {
        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    sleep(10000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler.sendEmptyMessage(0);
            }
        };
        thread.start();

    }*/




    /*private void startClock(){
        new CountDownTimer(69000, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.v("Timer",millisUntilFinished+"");

            }

            public void onFinish() {

                handler.sendEmptyMessage(0);
            }

        }.start();
    }*/


    private Handler handler = new Handler(){

        @Override
        public void handleMessage(Message message) {
            super.handleMessage(message);

            // Load ads into Interstitial Ads
            switch (message.what){
                case 0:
                    timer.cancel();
                    mInterstitialAd.loadAd(adRequest);
                 break;

                default:
                    break;
            }
//            showInterstitial();
        }
    };
}