package searchnative.com.topdoctors;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

import static searchnative.com.topdoctors.SearchFragment.linearLayout;


public class PrimaryFragment extends Fragment implements ScrollViewListener {

    LinearLayout mLinearLayout, labLinearLayout;
    ValueAnimator mAnimator, labAnimator;

    Spinner spinner1, spinnerSpeciality, spinnerLocation, labSpecialitySpinner, labGenderSpinner;
    Typeface typeface;
    String[] genderArray;
    List<String> specialityListData, countryDataList, specialityListIcon;
    ArrayAdapter<String> spinnerArrayAdapter, specialityArrayAdapter;
    String webServiceUrl = AppConfig.getWebServiceUrl();
    private ProgressDialog mProgressDialog;
    TextView home_page_lab_search;
    private TextView radiologyLab, medicalLab;
    private ProgressBar progressBar;
    private LinearLayout primarySearch;
    private ScrollViewExt scrollView;
    private int currentPage = 1;
    private static boolean isAsynchOn = true;
    private int totalSearchResult = 0, totalResultRendered = 0, totalRequestNo = 0;
    private String loginUserId;
    GuestLogin guestLogin;
    private boolean isGuest;
    EditText etMainSearch;

    //doctor search param
    String getDoctorSearchGender = "", getDoctorSearchSpeciality = "", getDoctorSearchLocation = "";

    //lab search param
    String getLabSearchGender = "", getLabSearchSpeciality = "";

    GetNearBy getNearBy;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getContext(), new Crashlytics());
        View view = inflater.inflate(R.layout.primary_layout, container, false);



        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                collapse();
                labCollapse();
            }
        });
        guestLogin = new GuestLogin();
        isGuest = guestLogin.getGuestLogin(getContext());
    }

    @Override
    public void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy) {
        // We take the last son in the scrollview
        View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
        int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

        // if diff is zero, then the bottom has been reached
        if (diff == 0) {
            if (totalSearchResult > totalResultRendered) {
                if (isAsynchOn) {
                    isAsynchOn = false;
                    /*getNearBy = new GetNearBy();
                    getNearBy.execute();
                    if (getNearBy.getStatus() == AsyncTask.Status.FINISHED) {
                        if (mProgressDialog != null && mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                            mProgressDialog = null;
                        }
                    }*/
                }
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //progressbar
        loginUserId = Preference.getValue(getContext(), "LOGIN_ID", "");
        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        primarySearch = (LinearLayout) getView().findViewById(R.id.searchContainer);
        scrollView = (ScrollViewExt) getView().findViewById(R.id.home_page_form_scroll);
        scrollView.setScrollViewListener(this);
        Bundle bundle = getArguments();

        //expandable
        mLinearLayout = (LinearLayout) getView().findViewById(R.id.p_expandable);
        labLinearLayout = (LinearLayout) getView().findViewById(R.id.lab_expandable);
        //mLinearLayoutHeader = (LinearLayout) getView().findViewById(R.id.p_header);

        final TextView tv = (TextView) getView().findViewById(R.id.home_page_doctor_search_doctor);
        final TextView etLabsSearch = (TextView) getView().findViewById(R.id.home_page_search_lab_textview);

        mLinearLayout.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mLinearLayout.getViewTreeObserver().removeOnPreDrawListener(this);
                mLinearLayout.setVisibility(View.GONE);


                final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                mLinearLayout.measure(widthSpec, heightSpec);

                mAnimator = slideAnimator(0, mLinearLayout.getMeasuredHeight());
                return true;
            }
        });

        //lab expandable
        labLinearLayout.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                labLinearLayout.getViewTreeObserver().removeOnPreDrawListener(this);
                labLinearLayout.setVisibility(View.GONE);

                final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                labLinearLayout.measure(widthSpec, heightSpec);
                labAnimator = labSlideAnimator(0, labLinearLayout.getMeasuredHeight());

                return true;
            }
        });

        //final Drawable img = getContext().getResources().getDrawable(R.mipmap.labs);
        //final Drawable startImg = getContext().getResources().getDrawable(R.mipmap.doctors);
        final Drawable imgArrow = getContext().getResources().getDrawable(R.mipmap.dropdown_arrow);
        final Drawable imgArrawTop = getContext().getResources().getDrawable(R.mipmap.dropdown_arrow_top);
        //get the density of the device

        float density = getResources().getDisplayMetrics().density;
        //Toast.makeText(getContext(), "density: " + density, Toast.LENGTH_LONG).show();
        if (density >= 0.75 && density < 1.0) {
            imgArrow.setBounds(0, 0, 10, 10);
            imgArrawTop.setBounds(0, 0, 10, 10);
        } else if (density >= 1.0 && density <= 1.5) {
            imgArrow.setBounds(0, 0, 22, 22);
            imgArrawTop.setBounds(0, 0, 22, 22);
        } else if (density > 1.5 && density < 2.0) {
            imgArrow.setBounds(0, 0, 25, 25);
            imgArrawTop.setBounds(0, 0, 25, 25);
        } else if (density >= 2.0 && density <= 2.5) {
            imgArrow.setBounds(0, 0, 30, 30);
            imgArrawTop.setBounds(0, 0, 30, 30);
        } else if (density > 2.5 && density < 3.0) {
            imgArrow.setBounds(0, 0, 40, 40);
            imgArrawTop.setBounds(0, 0, 40, 40);
        } else if (density >= 3.0 && density < 3.5) {
            imgArrow.setBounds(0, 0, 50, 50);
            imgArrawTop.setBounds(0, 0, 40, 40);
        } else if (density >= 3.5 && density <= 4.0) {
            imgArrow.setBounds(0, 0, 60, 60);
            imgArrawTop.setBounds(0, 0, 60, 60);
        } else if (density > 4.0) {
            imgArrow.setBounds(0, 0, 70, 70);
            imgArrawTop.setBounds(0, 0, 70, 70);
        } else {
            imgArrow.setBounds(0, 0, 40, 40);
            imgArrawTop.setBounds(0, 0, 40, 40);
        }
        if (LocalInformation.getLocaleLang().equals("ar")) {
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLinearLayout.getVisibility() == View.GONE) {
                        tv.setCompoundDrawables(imgArrawTop, null, null, null);
                        expand();
                    } else {
                        tv.setCompoundDrawables(imgArrow, null, null, null);
                        collapse();
                    }
                }
            });

            etLabsSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (labLinearLayout.getVisibility() == View.GONE) {
                        etLabsSearch.setCompoundDrawables(imgArrawTop, null, null, null);
                        labExpand();
                    } else {
                        etLabsSearch.setCompoundDrawables(imgArrow, null, null, null);
                        labCollapse();
                    }
                }
            });
        } else {
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLinearLayout.getVisibility() == View.GONE) {
                        tv.setCompoundDrawables(null, null, imgArrawTop, null);
                        expand();
                    } else {
                        tv.setCompoundDrawables(null, null, imgArrow, null);
                        collapse();
                    }
                }
            });

            etLabsSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (labLinearLayout.getVisibility() == View.GONE) {
                        etLabsSearch.setCompoundDrawables(null, null, imgArrawTop, null);
                        labExpand();
                    } else {
                        etLabsSearch.setCompoundDrawables(null, null, imgArrow, null);
                        labCollapse();
                    }
                }
            });
        }

        ImageView v = (ImageView) getView().findViewById(R.id.swipe_menu);
        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                DrawerLayout mDrawer = (DrawerLayout) getActivity().findViewById(R.id.drawerLayout);
                mDrawer.openDrawer(Gravity.LEFT);
                return true;
            }
        });


        etMainSearch = (EditText) getView().findViewById(R.id.hope_page_main_search);
        //EditText etDoctorSearchLocation = (EditText) getView().findViewById(R.id.home_doctor_search_location);
        TextView etHospitalSearch = (TextView) getView().findViewById(R.id.home_page_search_hospital_textview);
        TextView etClinicSearch = (TextView) getView().findViewById(R.id.home_page_search_clinics_textview);
        Preference.setValue(getContext(), "PRIMARY_SEARCH", "");
        //global search
        etMainSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager inputMethodManager =
                            (InputMethodManager) getContext().getSystemService(
                                    getContext().INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(
                            getActivity().getCurrentFocus().getWindowToken(), 0);
                    String searchKey = etMainSearch.getText().toString();
                    Log.v("Search Key", searchKey);
                    Preference.setValue(getContext(), "PRIMARY_SEARCH", "true");
                    Preference.setValue(getContext(), "PROMARY_SEARCH_KEY", searchKey);
                    customSearch();

                }
                return false;
            }
        });

        //set font
        typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/ExoMedium.otf");
        etMainSearch.setTypeface(typeface);
        tv.setTypeface(typeface);
        etHospitalSearch.setTypeface(typeface);
        etClinicSearch.setTypeface(typeface);
        etLabsSearch.setTypeface(typeface);

        radiologyLab = (TextView) getView().findViewById(R.id.radiologyLab);
        medicalLab = (TextView) getView().findViewById(R.id.medicalLab);
        radiologyLab.setTypeface(typeface);
        medicalLab.setTypeface(typeface);

        radiologyLab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getLabSearchSpeciality = radiologyLab.getText().toString();
                getLabSearchSpeciality = "Radiology Lab";
                Log.v("Lab Search type", getLabSearchSpeciality);
                Preference.setValue(getContext(), "GLOBAL_FILTER_TYPE", "Radiology Lab");
                Preference.setValue(getContext(), "LAB_SEARCH_CUSTOM", "true");
                Preference.setValue(getContext(), "LAB_SEARCH_SPECIALITY", getLabSearchSpeciality);
                customSearch();
            }
        });

        medicalLab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getLabSearchSpeciality = medicalLab.getText().toString();
                getLabSearchSpeciality = "Medical Lab";
                Preference.setValue(getContext(), "GLOBAL_FILTER_TYPE", "Medical Lab");
                Preference.setValue(getContext(), "LAB_SEARCH_CUSTOM", "true");
                Preference.setValue(getContext(), "LAB_SEARCH_SPECIALITY", getLabSearchSpeciality);
                customSearch();
            }
        });

        //location spinner configuration
        spinnerLocation = (SearchableSpinner) getView().findViewById(R.id.spinner_location);
        countryDataList = new ArrayList<String>();
        countryDataList.add(getResources().getString(R.string.location));
        countryDataList.addAll(LocationData.getmInstance().locationList);
        spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item,
                countryDataList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;
                //View view  = super.getDropDownView(position, convertView, parent);
                if (position == 0) {
                    TextView textView = new TextView(getContext());
                    textView.setHeight(0);
                    textView.setVisibility(View.GONE);
                    textView.setTextColor(getResources().getColor(R.color.textHighlightColor));
                    v = textView;
                } else {
                    v = super.getDropDownView(position, null, parent);
                }

                return v;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                ((TextView) view).setTypeface(typeface);
                int dp = (int) (getContext().getResources().getDimension(R.dimen.add_doctor_input_font) / getContext().getResources().getDisplayMetrics().density);
                if (position == 0) {
                    ((TextView) view).setTextColor(Color.GRAY);
                    ((TextView) view).setTextSize(dp);
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.black));
                    ((TextView) view).setTextSize(dp);
                }

                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //new GetJSONCountry().execute();
        spinnerArrayAdapter.setNotifyOnChange(true);
        //final int listSize = countryDataList.size() - 1;
        spinnerLocation.setAdapter(spinnerArrayAdapter);

        Preference.setValue(getContext(), "DOCTOR_SEARCH_CUSTOM", "");

        //location/country wise doctor search
        spinnerLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    TextView textView = (TextView) adapterView.getChildAt(0);
                    textView.setText(spinnerLocation.getSelectedItem().toString().trim());
                    getDoctorSearchLocation = spinnerLocation.getSelectedItem().toString().trim();
                    //Toast.makeText(getContext(), "density: " + getDoctorSearchLocation, Toast.LENGTH_LONG).show();
                    Preference.setValue(getContext(), "DOCTOR_SEARCH_CUSTOM", "true");
                    Preference.setValue(getContext(), "DOCTOR_LOCATION_SEARCH", "");
                    Preference.setValue(getContext(), "DOCTOR_LOCATION", "");
                    Preference.setValue(getContext(), "DOCTOR_LOCATION_SEARCH", getDoctorSearchLocation);
                    Preference.setValue(getContext(), "DOCTOR_LOCATION", getDoctorSearchLocation);
                    Log.v("Location selected", getDoctorSearchLocation);
                    customSearch();
                    //search doctors based on location/country
                    //getDoctorSearchLocation = spinnerLocation.getSelectedItem().toString();
                    //searchDoctors(getDoctorSearchGender, getDoctorSearchSpeciality, getDoctorSearchLocation);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //speciality spinner configuration
        spinnerSpeciality = (Spinner) getView().findViewById(R.id.spinner_speciality);
        labSpecialitySpinner = (Spinner) getView().findViewById(R.id.lab_spinner_speciality);
        specialityListData = new ArrayList<String>();
        specialityListIcon = new ArrayList<>();
        specialityListData.add(getResources().getString(R.string.speciality));
        specialityListIcon.addAll(SpecialityData.getmInstance().specialityListIcon);
        specialityListData.addAll(SpecialityData.getmInstance().specialityList);
        CustomAdapter customAdapter = new CustomAdapter(getContext(), specialityListIcon, specialityListData);
        spinnerSpeciality.setAdapter(customAdapter);

        //speciality wise doctor search
        spinnerSpeciality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    //search doctors based on speciality
                    getDoctorSearchSpeciality = SpecialityData.getmInstance().specialityList.get(i - 1).toString();
                    Preference.setValue(getContext(), "DOCTOR_SEARCH_CUSTOM", "true");
                    Preference.setValue(getContext(), "DOCTOR_SPECIALITY_SEARCH", "");
                    Preference.setValue(getContext(), "DOCTOR_SPECIALITY_SEARCH", getDoctorSearchSpeciality);
                    customSearch();
                    //searchDoctors(getDoctorSearchGender, getDoctorSearchSpeciality, getDoctorSearchLocation);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Preference.setValue(getContext(), "LAB_SEARCH_CUSTOM", "");

        //speciality wise lab search
        labSpecialitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    //search lab based on speciality
                    getLabSearchSpeciality = labSpecialitySpinner.getSelectedItem().toString();
                    Preference.setValue(getContext(), "LAB_SEARCH_CUSTOM", "true");
                    Preference.setValue(getContext(), "LAB_SEARCH_SPECIALITY", getLabSearchSpeciality);
                    customSearch();
                    //searchLab(getLabSearchGender, getLabSearchSpeciality);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //gender spinner configuration
        spinner1 = (Spinner) getView().findViewById(R.id.spinner1_gender);
        labGenderSpinner = (Spinner) getView().findViewById(R.id.lab_spinner_gender);
        genderArray = getResources().getStringArray(R.array.gender);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, genderArray) {

            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;
                //View view  = super.getDropDownView(position, convertView, parent);
                if (position == 0) {
                    TextView textView = new TextView(getContext());
                    textView.setHeight(0);
                    textView.setVisibility(View.GONE);
                    textView.setTextColor(Color.GRAY);
                    v = textView;
                } else {
                    v = super.getDropDownView(position, null, parent);
                }

                return v;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTypeface(typeface);
                ((TextView) v).setTextColor(getResources().getColor(R.color.black));
                ((TextView) v).setTextColor(getResources().getColor(R.color.black));
                int dp = (int) (getContext().getResources().getDimension(R.dimen.add_doctor_input_font) / getContext().getResources().getDisplayMetrics().density);
                ((TextView) v).setTextSize(dp);
                if (position == 0) {
                    ((TextView) v).setTextColor(Color.GRAY);
                }

                return v;
            }
        };
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(arrayAdapter);
        labGenderSpinner.setAdapter(arrayAdapter);
        addListenerOnSpinnerItemSelection();

        //gender wise doctor search
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    //search doctors based on gender
                    getDoctorSearchGender = String.valueOf(i);
                    //getDoctorSearchGender = spinner1.getSelectedItem().toString();
                    Preference.setValue(getContext(), "DOCTOR_SEARCH_CUSTOM", "true");
                    Preference.setValue(getContext(), "DOCTOR_GENDER_SEARCH", String.valueOf(i));
                    customSearch();
                    //searchDoctors(getDoctorSearchGender, getDoctorSearchSpeciality, getDoctorSearchLocation);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        home_page_lab_search = (TextView) getView().findViewById(R.id.home_page_lab_search);
        home_page_lab_search.setTypeface(typeface);
        home_page_lab_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preference.setValue(getContext(), "LAB_SEARCH_CUSTOM", "true");
                Preference.setValue(getContext(), "GLOBAL_FILTER_TYPE", "lab");
                customSearch();
            }
        });



        //gender wise lab search
        labGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //search lab based on gender
                if (i > 0) {
                    getLabSearchGender = labGenderSpinner.getSelectedItem().toString();
                    Preference.setValue(getContext(), "LAB_SEARCH_CUSTOM", "true");
                    Preference.setValue(getContext(), "LAB_SEARCH_GENDER", getLabSearchGender);
                    customSearch();
                    //searchLab(getLabSearchGender, getLabSearchSpeciality);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //nearby linear layout configuration
        String URL = webServiceUrl + "doctor/nearByDoctors?page=" + totalRequestNo
                + "&lang=" + LocalInformation.getLocaleLang()
                + "&userId=" + loginUserId
                + "&lat=" + Preference.getValue(getContext(), "Latitude", "")
                + "&long=" + Preference.getValue(getContext(), "Longitude", "");
        getNearBy = new GetNearBy(URL);
        getNearBy.execute();
        //String nearby = new GetNearBy().execute();


        Preference.setValue(getContext(), "HOSPITAL_SEARCH", "");
        //hospitals
        etHospitalSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preference.setValue(getContext(), "HOSPITAL_SEARCH", "true");
                Preference.setValue(getContext(), "GLOBAL_FILTER_TYPE", "hospital");
                customSearch();
            }
        });

        //clinics
        Preference.setValue(getContext(), "CLINIC_SEARCH", "");
        etClinicSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preference.setValue(getContext(), "CLINIC_SEARCH", "true");
                Preference.setValue(getContext(), "GLOBAL_FILTER_TYPE", "clinic");
                customSearch();
                //new GetClinic().execute();
            }
        });

        //create doctor singlton class object
        //DoctorList.getmInstance();
    }

    public void addListenerOnSpinnerItemSelection() {
        spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    /**
     * GetJSONCountry
     * Get country json data
     */
    public class GetJSONCountry extends AsyncTask<Void, Void, String> {
        String URL = webServiceUrl + "country";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(Void... params) {
            HttpGet httpGet = new HttpGet(URL);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();

            try {
                return mClient.execute(httpGet, responseHandler);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        protected void onPostExecute(String result) {
            try {
                List<String> countryList = new ArrayList<String>();
                countryList.add(getResources().getString(R.string.location));
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject country = jsonArray.getJSONObject(i);
                    countryList.add(country.getString("country_name"));
                    //Log.v("Country: ", country.getString("country_name"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * GetJSONDATA
     * speciality data from the web service
     */
    private class GetJSONData extends AsyncTask<Void, Void, String> {
        String URL = webServiceUrl + "Speciality/index/?lang=" + LocalInformation.getLocaleLang();
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(Void... params) {
            HttpGet httpPost = new HttpGet(URL);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();

            try {
                return mClient.execute(httpPost, responseHandler);
            } catch (ClientProtocolException exception) {
                exception.printStackTrace();
            } catch (IOException exception) {
                exception.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                List<String> specialityListData = new ArrayList<String>();
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject spec = jsonArray.getJSONObject(i);
                    specialityListData.add(spec.getString("SPEC"));
                    //Log.v("Data: ", spec.getString("SPEC"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void expand() {
        //set Visible
        mLinearLayout.setVisibility(View.VISIBLE);
        mAnimator.start();
    }

    private void labExpand() {
        labLinearLayout.setVisibility(View.VISIBLE);
        labAnimator.start();
    }

    private void collapse() {
        int finalHeight = mLinearLayout.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, 0);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                mLinearLayout.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        mAnimator.start();
    }

    private void labCollapse() {
        int finalHeight = labLinearLayout.getHeight();

        ValueAnimator mAnimator = labSlideAnimator(finalHeight, 0);
        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                labLinearLayout.setVisibility(View.GONE);
                InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        mAnimator.start();
    }

    private ValueAnimator slideAnimator(int start, int end) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);


        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = mLinearLayout.getLayoutParams();
                layoutParams.height = value;
                mLinearLayout.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    private ValueAnimator labSlideAnimator(int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int value = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = labLinearLayout.getLayoutParams();
                layoutParams.height = value;
                labLinearLayout.setLayoutParams(layoutParams);
            }
        });

        return animator;
    }

    /**
     * Default near by hospitals
     */
    public class NearBy {
        NearBy(String result) {

            /**
             * Nikunj + Jaymin
             * if result is null then return
             */

            if(result == null){

                if(getActivity()!=null){
                    Toast.makeText(getActivity(), getResources().getString(R.string.error_message),
                            Toast.LENGTH_SHORT).show();
                }

                return;

            }else{

               /* if(getView() == null){

                    return;
                }*/

                //Log.v("Nearby result", result);
                try {

                    LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.nearby_linear_layout);
                    LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                    );
                    if (totalRequestNo <= 1) {
                        linearLayout.removeAllViews();
                        textLayoutParam.setMarginStart(GetSearchTitleMarginStart());
                        textLayoutParam.setMargins(15, 10, 15, 10);
                        TextView title = new TextView(getContext());
                        title.setLayoutParams(textLayoutParam);
                        title.setTextColor(Color.parseColor("#010101"));
                        title.setTypeface(typeface, typeface.BOLD);
                        title.setTextSize(17);
                        title.setText(getResources().getString(R.string.nearby_doctors));
                        linearLayout.addView(title);
                    }


                    JSONObject hospitalObject = new JSONObject(result);
                    if (hospitalObject.getString("status").equals("false")) {
                        TextView textView = new TextView(getContext());
                        textView.setText(getResources().getString(R.string.no_data_found));
                        textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                        textView.setTextColor(Color.parseColor("#010101"));
                        textView.setTextSize(20);
                        textView.setLayoutParams(textLayoutParam);
                        linearLayout.addView(textView);

                        //set counter
                        totalSearchResult = 0;
                        totalResultRendered = 0;
                    } else {
                        JSONArray hospitalArray = hospitalObject.getJSONArray("result");
                        if (totalRequestNo <= 1)
                            totalSearchResult = hospitalObject.getInt("total_rows");
                        for (int i = 0; i < hospitalArray.length(); i++) {
                            final JSONObject hospital = hospitalArray.getJSONObject(i);
                            //append control
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                            );
                            layoutParams.setMargins(0, 0, 0, 0);
                            linearLayout.setLayoutParams(layoutParams);
                            View view;
                            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            view = inflater.inflate(R.layout.nearby, null);

                            //mipmap
                            ImageView mipmap = (ImageView) view.findViewById(R.id.speciality);
                            if (hospital.has("Mipmap")) {
                                int id = getContext().getResources().getIdentifier(hospital.getString("Mipmap"), "mipmap",
                                        getContext().getPackageName());
                                mipmap.setImageResource(id);
                            }

                            ImageView profilePic = (ImageView) view.findViewById(R.id.most_visited_);
                            profilePic.setImageResource(R.mipmap.most_visited_doctor);
                            /*if (hospital.getString("WorkType").equals("Lab")) {
                                profilePic.setImageResource(R.mipmap.lab_dark);
                            } else if (hospital.getString("WorkType").equals("Clinic")) {
                                profilePic.setImageResource(R.mipmap.clinic_dark);
                            } else if (hospital.getString("WorkType").equals("Hospital")) {
                                profilePic.setImageResource(R.mipmap.hospital_dark);
                            }
    */
                            //clinic name
                            TextView textView1 = (TextView) view.findViewById(R.id.title_work_place);
                            if (hospital.getString("Name").trim().length() == 0) {
                                textView1.setText(hospital.getString("Name"));
                            } else {
                                textView1.setText(hospital.getString("Name"));
                            }
                            textView1.setTypeface(typeface, typeface.BOLD);
                            textView1.setTextColor(Color.parseColor("#010101"));

                            //clinic address
                            TextView textView2 = (TextView) view.findViewById(R.id.work_place_address);
                            if (hospital.getString("Address").trim().length() == 0) {
                                textView2.setText(hospital.getString("Address"));
                            } else {
                                textView2.setText(hospital.getString("Address"));
                            }
                            textView2.setTypeface(typeface);
                            textView2.setTextColor(Color.parseColor("#010101"));

                            //mobile
                            TextView textView3 = (TextView) view.findViewById(R.id.work_place_phone);
                            textView3.setText(hospital.getString("Phone"));
                            textView3.setTypeface(typeface);
                            textView3.setTextColor(Color.parseColor("#010101"));

                            linearLayout.addView(view);

                            //total reviews
                            TextView textView4 = (TextView) view.findViewById(R.id.total_reviews);
                            if (hospital.has("TotalReview"))
                                textView4.setText(hospital.getString("TotalReview") + " " + getResources().getString(R.string.total_reviews));
                            else
                                textView4.setText("0 " + getResources().getString(R.string.total_reviews));
                            textView4.setTypeface(typeface);
                            textView4.setTextColor(Color.parseColor("#010101"));

                            //rating
                            RatingBar ratingBar = (RatingBar) view.findViewById(R.id.doctor_user_rating);
                            ratingBar.setRating(0.0f);
                            if ((hospital.has("RatingAvg"))) {
                                if (!(hospital.getString("RatingAvg").isEmpty() ||
                                        hospital.getString("RatingAvg").equals("null"))) {
                                    ratingBar.setRating(Float.valueOf(hospital.getString("RatingAvg")));
                                }
                            }

                            final String callDialer = hospital.getString("Phone");

                            //phone dialer
                            ImageView imageView = (ImageView) view.findViewById(R.id.phone_dialer);
                            //imageview.setId(imageView.getId() + i);
                            imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    openPhoneDialer(callDialer);
                                }
                            });
                            view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //check for Internet
                                    if (!NetworkInfoHelper.isOnline(getContext())) {
                                        Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                        return;
                                    }
                                    try {
//                                        TabFragment.viewPager.setCurrentItem(1);
                                        doctorProfile(hospital.getString("DoctorId"), hospital.getString("Name"));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            final ImageView share_details = (ImageView) view.findViewById(R.id.share_details);
                            share_details.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //check for Internet
                                    if (!NetworkInfoHelper.isOnline(getContext())) {
                                        Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                        return;
                                    }
                                    try {
                                        doctorProfile(hospital.getString("DoctorId"), hospital.getString("Name"));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            final LinearLayout withBookmark = (LinearLayout) view.findViewById(R.id.withBookmark);
                            final LinearLayout withoutBookmark = (LinearLayout) view.findViewById(R.id.withoutBookmark);
                            //final ImageView user_profile_pic_bookmark = (ImageView) view.findViewById(R.id.user_profile_pic_bookmark);
                            final ImageView addToBookmark = (ImageView) view.findViewById(R.id.add_to_bookmarks);
                            addToBookmark.setTag(1);
                            if (hospital.has("BookmarkStatus")) {
                                String bookmarkStatus = hospital.getString("BookmarkStatus");
                                if (!(bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                                    String bookmarkId = hospital.getString("BookmarkUserId");
                                    if (bookmarkId.equals(loginUserId) && bookmarkStatus.equals("1")) {
                                        addToBookmark.setTag(2);
                                        addToBookmark.setImageResource(R.mipmap.bookmark_2);
                                        withBookmark.setVisibility(View.VISIBLE);
                                        withoutBookmark.setVisibility(View.GONE);
                                    }
                                }
                                addToBookmark.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //check for Internet
                                        if (!NetworkInfoHelper.isOnline(getContext())) {
                                            Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                            return;
                                        }
                                        if (isGuest) {
                                            guestLogin.confirmAlert(getContext());
                                            return;
                                        }
                                        try {
                                            progressBar.setVisibility(View.VISIBLE);
                                            doctorBookMarks(hospital.getString("DoctorId"));
                                            //workidAsync =  filterData.getString("WorkId");
                                            //workTypeBookmark = new SearchFragment.WorkTypeBookmark();
                                            //workTypeBookmark.execute();
                                            if (Integer.parseInt(addToBookmark.getTag().toString()) == 1) {
                                                addToBookmark.setImageResource(R.mipmap.bookmark_2);
                                                addToBookmark.setTag(2);
                                                withBookmark.setVisibility(View.VISIBLE);
                                                withoutBookmark.setVisibility(View.GONE);
                                            } else {
                                                addToBookmark.setImageResource(R.mipmap.bookmarks);
                                                addToBookmark.setTag(1);
                                                withBookmark.setVisibility(View.GONE);
                                                withoutBookmark.setVisibility(View.VISIBLE);
                                            }
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }

                                    }
                                });
                            }

                        }
                        totalResultRendered += hospitalArray.length();
                        Log.v("total rendered", String.valueOf(totalResultRendered));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
    }

    /**
     * Hospitals list
     */
    public class Hospitals {
        Hospitals(String result) {
            LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.nearby_linear_layout);
            linearLayout.removeAllViews();
            linearLayout.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            );
            textLayoutParam.setMarginStart(GetSearchTitleMarginStart());
            textLayoutParam.setMargins(15, 10, 15, 10);
            TextView title = new TextView(getContext());
            title.setLayoutParams(textLayoutParam);
            title.setTextColor(Color.parseColor("#010101"));
            title.setTypeface(typeface, typeface.BOLD);
            title.setTextSize(17);
            title.setText(getResources().getString(R.string.hospital));
            linearLayout.addView(title);
            try {
                JSONObject hospitalObject = new JSONObject(result);
                JSONArray hospitalArray = hospitalObject.getJSONArray("data");
                for (int i = 0; i < hospitalArray.length(); i++) {
                    JSONObject hospital = hospitalArray.getJSONObject(i);

                    //append control
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                    );
                    layoutParams.setMargins(0, 0, 0, 0);
                    linearLayout.setLayoutParams(layoutParams);
                    View view;
                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    view = inflater.inflate(R.layout.nearby, null);

                    if (LocalInformation.getLocaleLang().equals("ar")) {
                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.title_work_place);
                        if (hospital.getString("name_en").trim().length() == 0) {
                            textView1.setText(hospital.getString("name_en"));
                        } else {
                            textView1.setText(hospital.getString("name"));
                        }
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.work_place_address);
                        if (hospital.getString("address_en").trim().length() == 0) {
                            textView2.setText(hospital.getString("address_en"));
                        } else {
                            textView2.setText(hospital.getString("address"));
                        }
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.work_place_phone);
                        textView3.setText(hospital.getString("phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        //total reviews
                        TextView textView4 = (TextView) view.findViewById(R.id.total_reviews);
                        textView4.setText(getResources().getString(R.string.total_reviews));
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));
                        linearLayout.addView(view);

                        final String callDialer = hospital.getString("phone");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.phone_dialer);
                        imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });
                    } else {
                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.title_work_place);
                        textView1.setText(hospital.getString("name_en"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.work_place_address);
                        textView2.setText(hospital.getString("address_en"));
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.work_place_phone);
                        textView3.setText(hospital.getString("phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        //total reviews
                        TextView textView4 = (TextView) view.findViewById(R.id.total_reviews);
                        textView4.setText(getResources().getString(R.string.total_reviews));
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));
                        linearLayout.addView(view);

                        final String callDialer = hospital.getString("phone");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.phone_dialer);
                        imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public int GetSearchTitleMarginStart() {
        float density = getResources().getDisplayMetrics().density;
        //Toast.makeText(getContext(), "density: " + density, Toast.LENGTH_LONG).show();
        int margin = 0;
        //Toast.makeText(getContext(), "density: " + density, Toast.LENGTH_LONG).show();
        if (density >= 0.75 && density < 1.0) {

        } else if (density >= 1.0 && density < 1.5) {
            margin = 10;
        } else if (density >= 1.5 && density < 2.0) {
            margin = 16;
        } else if (density >= 2.0 && density <= 2.5) {
            margin = 26;
        } else if (density > 2.5 && density < 3.0) {
            margin = 30;
        } else if (density >= 3.0 && density < 3.5) {
            margin = 35;
        } else if (density >= 3.5 && density <= 4.0) {
            margin = 40;
        } else if (density > 4.0) {

        } else {

        }
        return margin;
    }

    /**
     * Clinic list
     */
    public class Clinics {
        Clinics(String result) {
            LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.nearby_linear_layout);
            linearLayout.removeAllViews();
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            );
            textLayoutParam.setMarginStart(GetSearchTitleMarginStart());
            textLayoutParam.setMargins(15, 10, 15, 10);
            TextView title = new TextView(getContext());
            title.setLayoutParams(textLayoutParam);
            title.setTextColor(Color.parseColor("#010101"));
            title.setTypeface(typeface, typeface.BOLD);
            title.setTextSize(17);
            title.setText(getResources().getString(R.string.clinics));
            linearLayout.addView(title);
            try {
                JSONObject hospitalObject = new JSONObject(result);
                JSONArray hospitalArray = hospitalObject.getJSONArray("data");
                for (int i = 0; i < hospitalArray.length(); i++) {
                    JSONObject clinic = hospitalArray.getJSONObject(i);

                    //append control
                    //LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.nearby_linear_layout);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                    );
                    layoutParams.setMargins(0, 0, 0, 0);
                    linearLayout.setLayoutParams(layoutParams);
                    View view;
                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    view = inflater.inflate(R.layout.nearby, null);

                    if (LocalInformation.getLocaleLang().equals("ar")) {
                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.title_work_place);
                        if (clinic.getString("name_en").trim().length() == 0) {
                            textView1.setText(clinic.getString("name_en"));
                        } else {
                            textView1.setText(clinic.getString("name"));
                        }
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.work_place_address);
                        if (clinic.getString("address_en").trim().length() == 0) {
                            textView2.setText(clinic.getString("address_en"));
                        } else {
                            textView2.setText(clinic.getString("address"));
                        }
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.work_place_phone);
                        textView3.setText(clinic.getString("phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        //total reviews
                        TextView textView4 = (TextView) view.findViewById(R.id.total_reviews);
                        textView4.setText(getResources().getString(R.string.total_reviews));
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));
                        linearLayout.addView(view);

                        final String callDialer = clinic.getString("phone");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.phone_dialer);
                        imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });
                    } else {
                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.title_work_place);
                        textView1.setText(clinic.getString("name_en"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.work_place_address);
                        textView2.setText(clinic.getString("address_en"));
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.work_place_phone);
                        textView3.setText(clinic.getString("phone"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        //total reviews
                        TextView textView4 = (TextView) view.findViewById(R.id.total_reviews);
                        textView4.setText(getResources().getString(R.string.total_reviews));
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));
                        linearLayout.addView(view);

                        final String callDialer = clinic.getString("phone");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.phone_dialer);
                        imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Get hospital list from web service
     */
    public class GetHospitalList extends AsyncTask<Void, Void, String> {
        String URL = webServiceUrl + "work/hospital";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(Void... params) {
            HttpGet httpGet = new HttpGet(URL);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            try {
                return mClient.execute(httpGet, responseHandler);
            } catch (ClientProtocolException exception) {
                exception.printStackTrace();
            } catch (IOException exception) {
                exception.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            new Hospitals(result);
            hideProgressBar();
            /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
            /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
            mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();*/
        }
    }

    /**
     * Get clinic list from web service
     */
    public class GetClinic extends AsyncTask<Void, Void, String> {
        String URL = webServiceUrl + "work/clinic";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(Void... params) {
            HttpGet httpGet = new HttpGet(URL);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            try {
                return mClient.execute(httpGet, responseHandler);
            } catch (ClientProtocolException exception) {
                exception.printStackTrace();
            } catch (IOException exception) {
                exception.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            new Clinics(result);
            hideProgressBar();
            /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
            /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
            mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();*/
        }
    }

    /**
     * Get nearby list
     */
    public class GetNearBy extends AsyncTask<Void, Void, String> {
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        String url;

        GetNearBy(String url) {
            this.url = url;
        }

        @Override
        protected String doInBackground(Void... params) {

            HttpGet httpGet = new HttpGet(url);
            Log.v("Url", url);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            try {
                return mClient.execute(httpGet, responseHandler);
            } catch (ClientProtocolException exception) {
                exception.printStackTrace();
            } catch (IOException exception) {
                exception.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            SpecialityData.getmInstance();
            //CountryData.getmInstance();
            LocationData.getmInstance();
            new NearBy(result);
            isAsynchOn = true;
            if (totalRequestNo <= 1)
                hideProgressBar();
            else {
                hideProgressBar();
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
            /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            totalRequestNo++;
            if (totalRequestNo <= 1)
                showProgressBar();
            else {
                mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
            /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
            mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();*/
        }
    }

    public void searchDoctors(final String gender, final String speciality, final String location) {

        class SearchDcotors extends AsyncTask<String, Void, String> {
            String URL = AppConfig.getWebServiceUrl() + "work/searchDoctors";
            HttpClient httpClient = new DefaultHttpClient();

            @Override
            protected String doInBackground(String... params) {
                String quickGender = gender;
                String quickSpeciality = speciality;
                String quickLocation = location;

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("gender", quickGender));
                nameValuePairs.add(new BasicNameValuePair("speciality", quickSpeciality));
                nameValuePairs.add(new BasicNameValuePair("location", quickLocation));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    //Log.v("url", URL);
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    return httpClient.execute(httpPost, responseHandler);
                    //JSONObject json = new JSONObject(responseBody);
                    //Log.v("data: ", json.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                new DoctorSearchResult(result);
                hideProgressBar();
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressBar();
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();*/
            }
        }

        SearchDcotors searchDcotors = new SearchDcotors();
        searchDcotors.execute(gender, speciality, location);
    }

    /**
     * Doctor search result
     */
    public class DoctorSearchResult {
        DoctorSearchResult(String result) {
            LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.nearby_linear_layout);
            linearLayout.removeAllViews();
            linearLayout.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            );
            textLayoutParam.setMarginStart(GetSearchTitleMarginStart());
            textLayoutParam.setMargins(15, 10, 15, 10);
            TextView title = new TextView(getContext());
            title.setLayoutParams(textLayoutParam);
            title.setTextColor(Color.parseColor("#010101"));
            title.setTypeface(typeface, typeface.BOLD);
            title.setTextSize(17);
            title.setText(getResources().getString(R.string.search_doctor_label));
            linearLayout.addView(title);

            try {
                JSONObject doctorSearchObject = new JSONObject(result);

                if (doctorSearchObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText("No data found");
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTypeface(typeface);
                    textView.setTextSize(15);
                    textView.setLayoutParams(textLayoutParam);

                    linearLayout.addView(textView);
                } else {
                    JSONArray searchResult = doctorSearchObject.getJSONArray("data");
                    for (int i = 0; i < searchResult.length(); i++) {
                        JSONObject doctor = searchResult.getJSONObject(i);

                        //append control
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                        );
                        layoutParams.setMargins(0, 0, 0, 0);
                        linearLayout.setLayoutParams(layoutParams);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.nearby, null);

                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.title_work_place);
                        textView1.setText(doctor.getString("first_name") + " " + doctor.getString("last_name"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.work_place_address);
                        textView2.setText(doctor.getString("address"));
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.work_place_phone);
                        textView3.setText(doctor.getString("phone_number"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        //total reviews
                        TextView textView4 = (TextView) view.findViewById(R.id.total_reviews);
                        textView4.setText(getResources().getString(R.string.total_reviews));
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));
                        linearLayout.addView(view);

                        final String callDialer = doctor.getString("phone_number");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.phone_dialer);
                        imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });


                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Open phone dialer
     *
     * @param callDialer string
     */
    public void openPhoneDialer(String callDialer) {
        android.support.v7.app.AlertDialog.Builder builderSingle = new android.support.v7.app.AlertDialog.Builder(getContext());
        builderSingle.setIcon(R.mipmap.call);
        builderSingle.setTitle(R.string.select_one_number);

        String search = "--";
        String search1 = "-";
        String[] array = new String[10];
        if (callDialer.indexOf(search) != -1) {
            array = callDialer.replaceAll(" ", "").split("\\--", -1);
        } else if (callDialer.indexOf(search1) != -1) {
            array = callDialer.replaceAll(" ", "").split("\\-", -1);
        }

        if (array.length == 10) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + callDialer.trim()));
            startActivity(intent);
        } else {
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
            arrayAdapter.addAll(array);

            builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + strName));
                    startActivity(intent);

                }
            });
            builderSingle.show();
        }
    }

    /**
     * Search lab based on gender and speciality
     *
     * @param gender
     * @param speciality
     */
    public void searchLab(final String gender, final String speciality) {

        class LabSearch extends AsyncTask<String, Void, String> {
            String URL = AppConfig.getWebServiceUrl() + "work/searchLab";
            HttpClient httpClient = new DefaultHttpClient();

            @Override
            protected String doInBackground(String... params) {
                String quickGender = gender;
                String quickSpeciality = speciality;

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("gender", quickGender));
                nameValuePairs.add(new BasicNameValuePair("speciality", quickSpeciality));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    return httpClient.execute(httpPost, responseHandler);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                new LabSearchResult(result);
                hideProgressBar();
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressBar();
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();*/
            }
        }

        LabSearch labSearch = new LabSearch();
        labSearch.execute(gender, speciality);
    }

    public class LabSearchResult {
        LabSearchResult(String result) {
            Log.v("result", result);
            LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.nearby_linear_layout);
            linearLayout.removeAllViews();
            linearLayout.setGravity(Gravity.START);
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            );
            textLayoutParam.setMarginStart(GetSearchTitleMarginStart());
            textLayoutParam.setMargins(15, 10, 15, 10);
            TextView title = new TextView(getContext());
            title.setLayoutParams(textLayoutParam);
            title.setTextColor(Color.parseColor("#010101"));
            title.setTypeface(typeface, typeface.BOLD);
            title.setTextSize(17);
            title.setText(getResources().getString(R.string.search_labs_label));
            linearLayout.addView(title);

            try {
                JSONObject doctorSearchObject = new JSONObject(result);

                if (doctorSearchObject.getString("status").equals("false")) {
                    TextView textView = new TextView(getContext());
                    textView.setText("No data found");
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(Color.parseColor("#010101"));
                    textView.setTypeface(typeface);
                    textView.setTextSize(15);
                    textView.setLayoutParams(textLayoutParam);

                    linearLayout.addView(textView);
                } else {
                    JSONArray searchResult = doctorSearchObject.getJSONArray("data");
                    for (int i = 0; i < searchResult.length(); i++) {
                        JSONObject doctor = searchResult.getJSONObject(i);

                        //append control
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                        );
                        layoutParams.setMargins(0, 0, 0, 0);
                        linearLayout.setLayoutParams(layoutParams);
                        View view;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.nearby, null);

                        //clinic name
                        TextView textView1 = (TextView) view.findViewById(R.id.title_work_place);
                        textView1.setText(doctor.getString("name_en"));
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));

                        //clinic address
                        TextView textView2 = (TextView) view.findViewById(R.id.work_place_address);
                        textView2.setText(doctor.getString("address_en"));
                        textView2.setTypeface(typeface);
                        textView2.setTextColor(Color.parseColor("#010101"));

                        //mobile
                        TextView textView3 = (TextView) view.findViewById(R.id.work_place_phone);
                        textView3.setText(doctor.getString("phone_number"));
                        textView3.setTypeface(typeface);
                        textView3.setTextColor(Color.parseColor("#010101"));

                        //total reviews
                        TextView textView4 = (TextView) view.findViewById(R.id.total_reviews);
                        textView4.setText(getResources().getString(R.string.total_reviews));
                        textView4.setTypeface(typeface);
                        textView4.setTextColor(Color.parseColor("#010101"));
                        linearLayout.addView(view);

                        final String callDialer = doctor.getString("phone_number");

                        //phone dialer
                        ImageView imageView = (ImageView) view.findViewById(R.id.phone_dialer);
                        imageView.setId(imageView.getId() + i);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openPhoneDialer(callDialer);
                            }
                        });
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void customSearch() {
        //check for Internet
        if (!NetworkInfoHelper.isOnline(getContext())) {
            Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

            return;
        }
        SearchFragment.searchKeyword.setText("");
        BaseActivity baseActivity = new BaseActivity();
        //SearchFragment searchFragment;
        //searchFragment = new SearchFragment();
        //searchFragment.isListingLayout = true;
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        baseActivity.tabFragment.tabLayout.getTabAt(1).select();
        etMainSearch.setText("");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v("Primary Start: ", "True");
        totalRequestNo = 0;
        totalSearchResult = 0;
        totalResultRendered = 0;
        //getNearBy = new GetNearBy();
        //getNearBy.execute();
        Log.v("Search type: ", Preference.getValue(getContext(), "GLOBAL_FILTER_TYPE", ""));
        Log.v("Near by :", "Search");
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        primarySearch.setVisibility(View.VISIBLE);
    }

    public void showProgressBar() {
        primarySearch.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (getNearBy != null)
            getNearBy.cancel(true);
    }

    /**
     * doctor profile
     *
     * @param doctorId   string
     * @param doctorName string
     */
    public void doctorProfile(final String doctorId, final String doctorName) {
        Bundle args = new Bundle();
        args.putString("isfrom","primary");
        args.putString("id", doctorId);
        Log.v("id", doctorId);
        Log.v("name", doctorName);
        args.putString("name", doctorName);
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        DoctorProfileFragment clinicProfileFragment = new DoctorProfileFragment();
        clinicProfileFragment.setArguments(args);
        mFragmentManager.popBackStack("doctorProfile",FragmentManager.POP_BACK_STACK_INCLUSIVE);
        mFragmentTransaction.addToBackStack("doctorProfile");
        mFragmentTransaction.add(R.id.search_layout, clinicProfileFragment).commit();
        TabFragment.tabLayout.getTabAt(1).select();
    }

    public void doctorBookMarks(final String doctorId) {

        class DoctorBookmarksAddUpdate extends AsyncTask<String, Void, String> {

            String URL = AppConfig.getWebServiceUrl() + "bookmark/add";

            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            String quickDoctorId = doctorId;
            String quickUserId = Preference.getValue(getContext(), "LOGIN_ID", "");

            private String resultStatus = "";

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("doctorId", quickDoctorId));
                nameValuePairs.add(new BasicNameValuePair("userId", quickUserId));
                nameValuePairs.add(new BasicNameValuePair("lang", LocalInformation.getLocaleLang()));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    return mClient.execute(httpPost, responseHandler);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                //Log.v("Bookmarks result", result);
                progressBar.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String responseResult = jsonObject.getString("result");
                    String doctorName = jsonObject.getString("name");

                    if (responseResult.equals("Bookmarked") || responseResult.equals("Bookmark is added")) {
                        //addToBookmarks.setImageResource(R.mipmap.add_a_lab);
                        //Preference.setValue(getContext(), "BOOKMARK_STATUS", "true");
                        Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmarkd_add), Toast.LENGTH_SHORT).show();
                    } else {
                        //addToBookmarks.setImageResource(R.mipmap.bookmarks);
                        //Preference.setValue(getContext(), "BOOKMARK_STATUS", "false");
                        Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmark_remove), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                /*mProgressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();*/
            }
        }

        DoctorBookmarksAddUpdate doctorBookmarksAddUpdate = new DoctorBookmarksAddUpdate();
        doctorBookmarksAddUpdate.execute(doctorId);
    }



}