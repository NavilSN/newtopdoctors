package searchnative.com.topdoctors;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MakeEnquiryActivity extends AppCompatActivity {

    private Typeface typeface;
    private TextView make_query_title, name_enquiry_message, profile_name_enquiry_message,
            fill_form_message;
    private EditText write_review_edit_text;
    private Button submit_button;
    private ImageView closeActivity;
    private String userId,workId,workType;
    private String webServiceUrl = AppConfig.getWebServiceUrl() + "enquiry/add";
    //private String webServiceUrl = "http://192.168.1.28/topdoctor/api/enquiry/add";

    private ProgressDialog progressDialog;
    private String  REQUEST_TAG = "MakeEnquiryActivity";
    private JSONObject jsonObject;
    private int MY_SOCKET_TIMEOUT_MS = 10000;

    private EditText etName,etPhone,etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_make_enquiry);

        Intent sender = getIntent();
        Bundle extras = sender.getExtras();
        userId = extras.getString("user_id","");
        workId = extras.getString("work_id","");
        workType = extras.getString("work_type","");


        typeface = Typeface.createFromAsset(getAssets(), "fonts/ExoMedium.otf");
        make_query_title = (TextView) findViewById(R.id.make_query_title);
        name_enquiry_message = (TextView) findViewById(R.id.name_enquiry_message);
        profile_name_enquiry_message = (TextView) findViewById(R.id.profile_name_enquiry_message);
        fill_form_message = (TextView) findViewById(R.id.fill_form_message);
        write_review_edit_text = (EditText) findViewById(R.id.write_review_edit_text);
        submit_button = (Button) findViewById(R.id.submit_button);
        closeActivity = (ImageView) findViewById(R.id.close_make_query);

        etName = (EditText) findViewById(R.id.make_name_enquiry);
        etPhone = (EditText) findViewById(R.id.make_phone_enquiry);
        etEmail = (EditText) findViewById(R.id.make_email_enquiry);

        etName.setTypeface(typeface);
        etPhone.setTypeface(typeface);
        etEmail.setTypeface(typeface);

        make_query_title.setTypeface(typeface);
        name_enquiry_message.setTypeface(typeface);
        profile_name_enquiry_message.setTypeface(typeface);
        fill_form_message.setTypeface(typeface);
        write_review_edit_text.setTypeface(typeface);
        submit_button.setTypeface(typeface);

        profile_name_enquiry_message.setText(extras.getString("name")
                + getResources().getString(R.string.question_mark));

        //for review scroll
        write_review_edit_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.write_review_edit_text) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        /*submit the query*/
        submit_button.setOnClickListener(   new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!(etName.getText().toString().trim().length() > 0)){
                    etName.setError(getResources().getString(R.string.name_is_required));
                    etName.requestFocus();
                    return;
                }
                /*else if(!(etPhone.getText().toString().trim().length() > 0)){
                    etPhone.setError("Phone number is required");
                    etPhone.requestFocus();
                    return;
                }*/
                else if(!(etEmail.getText().toString().trim().length() > 0)){
                    etEmail.setError(getResources().getString(R.string.email_error));
                    etEmail.requestFocus();
                    return;
                } else {

                    Pattern pattern1 = Pattern.compile( "^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");
                    Matcher matcher1 = pattern1.matcher(etEmail.getText().toString().trim());
                    if (!matcher1.matches()) {
                        etEmail.setError(getResources().getString(R.string.valid_email_error));
                        etEmail.requestFocus();
                        return;
                    }

                    if(write_review_edit_text.getText().toString().length() > 0){
                        progressDialog = new ProgressDialog(MakeEnquiryActivity.this);
                        progressDialog=new ProgressDialog(MakeEnquiryActivity.this,R.style.AppCompatAlertDialogStyle);
                        progressDialog.setMessage(getResources().getString(R.string.sending_enquiry));
                        progressDialog.setCancelable(false);
                        progressDialog.show();

                        volleyMakeEnquireRequest(webServiceUrl);

                    }else {
                        Toast.makeText(MakeEnquiryActivity.this,getResources().getString(R.string.enter_your_enquiry),
                                Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        closeActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void volleyMakeEnquireRequest(String url){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Toast.makeText(MakeEnquiryActivity.this,response,Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                        try {
                            Log.v("Response of Enquiry",response);
                            jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if(status.equals("true")){
                                Toast.makeText(MakeEnquiryActivity.this,message,Toast.LENGTH_LONG).show();
                                write_review_edit_text.setText("");
                                etName.setText("");
                                etEmail.setText("");
                                etPhone.setText("");
                            }else{
//                                if(status.equals("false") && message.equals("Error occured while sending an email"))
                                //Toast.makeText(MakeEnquiryActivity.this,getResources().getString(R.string.error_message),Toast.LENGTH_LONG).show();
                                Toast.makeText(MakeEnquiryActivity.this,message,Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(MakeEnquiryActivity.this,getResources().getString(R.string.error_message),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("user_id",userId);
                params.put("work_id",workId);
                params.put("work_type", workType);
                params.put("description",write_review_edit_text.getText().toString().trim());
                params.put("name",etName.getText().toString().trim());
                params.put("email",etEmail.getText().toString().trim());
                params.put("phone",etPhone.getText().toString().trim());
                Log.v("Params",params+"");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest,REQUEST_TAG);
    }



    @Override
    protected void onStop() {
        super.onStop();

//        Toast.makeText(Add.this, CheckingBackGround.isAppIsInBackground(AddClinicActivity.this)+"", Toast.LENGTH_SHORT).show();

        if(CheckingBackGround.isAppIsInBackground(MakeEnquiryActivity.this)){
            if(TabFragment.timer != null){
                TabFragment.timer.cancel();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(TabFragment.timer !=null){
            TabFragment.timer.cancel();
            TabFragment.timer.start();
        }
    }
}