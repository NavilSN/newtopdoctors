package searchnative.com.topdoctors;

/**
 * Created by root on 1/12/16.
 */

public class CheckForNullValues {

    public static String setValue(String data) {
        String returnData = data;

        try{
            if (returnData.length() == 0 || returnData.equals("null")) {
                returnData = "";
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return returnData;
    }
}
