package searchnative.com.topdoctors;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

import static android.content.Intent.ACTION_DIAL;
import static searchnative.com.topdoctors.R.id.textView7;

/**
 * Created by mayur on 28/9/16.
 */

public class ClinicProfileFragment extends Fragment {

    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    TabFragment tabFragment = new TabFragment();
    final String WEB_SERVICE_URL = AppConfig.getWebServiceUrl();
    private ProgressDialog mProgressDialog;
    private Typeface typeface;
    private double latitude, longitude;
    private LatLng clinicMap;
    private SupportMapFragment supportMapFragment;
    private String clinicMipmap;
    private String clinicName, clinicAddress, clinicPhone, clinicRating, clinicSpeciality, clinicReview = "";
    private ImageView socialShare, photosOrVideo;
    private String appLang = LocalInformation.getLocaleLang();
    public static ImageView clinicShare;
    private LinearLayout clinicProfileDetails, avgRating;
    public static LinearLayout clinicWriteReview;
    private ProgressBar progressBar;
    private String loginUserId, clinicId;
    GuestLogin guestLogin;
    private boolean isGuest;
    LinearLayout claimProfileLayout;
    public static TextView claimProfile;
    private TextView makeEnquiry, total_doctors;
    private Set<String> set = new HashSet<String>();
    private List iconList = new ArrayList();
    private int totalReviewCount = 0;
    private ScrollView clinic_scroll;
    private ImageView transparent_image;
    private RelativeLayout map_relative_layout;

    //async classes
    ClinicProfile clinicProfile;
    WorkTypeBookmark workTypeBookmark;
    HospitalAllReview hospitalAllReview;


    private LinearLayout clinic_speciality;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getContext(), new Crashlytics());
        View view = inflater.inflate(R.layout.clinic_profile, container, false);
        BaseActivity.isviewfrom="";
        return view;
    }

    /**
     * clinic profile async
     */
    class ClinicProfile extends AsyncTask<String, Void, String> {
        String URL = WEB_SERVICE_URL + "profile/clinicProfile";

        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("clinicId", clinicId));
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));

            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            new ClinicProfileFragment.DisplayClinicProfile(result);
            fetchClinicReview();
            hospitalAllReview = new HospitalAllReview();
            hospitalAllReview.execute();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }

    /**
     * work type bookmark async
     */
    class WorkTypeBookmark extends AsyncTask<String, Void, String> {
        final String URL = AppConfig.getWebServiceUrl() + "Worktype_bookmark/Add";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("userId", loginUserId));
            nameValuePairs.add(new BasicNameValuePair("workId", clinicId));
            try {
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.v("result", result.toString());
            try {
                JSONObject jsonObject = new JSONObject(result);
                String responseResult = jsonObject.getString("result");
                String doctorName = jsonObject.getString("name");

                if(responseResult.equals("Bookmarked") || responseResult.equals("Bookmark is added")) {
                    //addToBookmarks.setImageResource(R.mipmap.add_a_lab);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "true");
                    Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmarkd_add), Toast.LENGTH_SHORT).show();
                } else {
                    //addToBookmarks.setImageResource(R.mipmap.bookmarks);
                    //Preference.setValue(getContext(), "BOOKMARK_STATUS", "false");
                    Toast.makeText(getContext(), doctorName + " " + getResources().getString(R.string.bookmark_remove), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        BaseActivity.isviewfrom="";
        BaseActivity.showMenu();
        //progreebar
        guestLogin = new GuestLogin();
        isGuest = guestLogin.getGuestLogin(getContext());
        loginUserId = Preference.getValue(getContext(), "LOGIN_ID", "");
        clinicProfileDetails = (LinearLayout) getView().findViewById(R.id.clinicProfileDetails);
        progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        avgRating = (LinearLayout) getView().findViewById(R.id.avgRating);
        clinicWriteReview = (LinearLayout) getView().findViewById(R.id.clinicWriteReview);
        clinicProfileDetails.setVisibility(View.INVISIBLE);
        claimProfile = (TextView) getView().findViewById(R.id.claim_profile_button);
        total_doctors = (TextView) getView().findViewById(R.id.total_doctors);
        clinic_scroll = (ScrollView) getView().findViewById(R.id.clinic_scroll);
        transparent_image = (ImageView) getView().findViewById(R.id.transparent_image);
        map_relative_layout = (RelativeLayout) getView().findViewById(R.id.map_relative_layout);

        transparent_image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        clinic_scroll.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        clinic_scroll.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        clinic_scroll.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
        //makeEnquiry = (TextView) getView().findViewById(R.id.makeEnquiry);
        claimProfile.setTypeface(typeface);
        //makeEnquiry.setTypeface(typeface);

        clinic_speciality = (LinearLayout) getView().findViewById(R.id.clinic_speciality);

        BaseActivity.showMenu();
        Preference.setValue(getContext(), "resetMenuVisibility", "true");

        clinicId = getArguments().getString("id");
        //recent viewed
        RecentlyViewed.add("W" + clinicId);
        RecentlyViewed.setRecentlyViewedInPreference(getContext(), "recentlyViewed");

        clinicMipmap = getArguments().getString("mipmap");
        clinicSpeciality = getArguments().getString("speciality");

        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ExoMedium.otf");

        total_doctors.setTypeface(typeface);

        clinicProfile = new ClinicProfile();
        clinicProfile.execute();

        ImageView imageView = (ImageView) getView().findViewById(R.id.clinic_profile_swipe_menu);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawerLayout);
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        //social share
        socialShare = (ImageView) getView().findViewById(R.id.clinic_social_share);
        clinicShare = (ImageView) getView().findViewById(R.id.clinicShare);
        socialShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareIt(clinicName, clinicPhone, clinicAddress, CheckForNullValues.setValue(clinicSpeciality), clinicReview);
            }
        });
        clinicShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareIt(clinicName, clinicPhone, clinicAddress, CheckForNullValues.setValue(clinicSpeciality), clinicReview);
            }
        });

        //photo or videos
        photosOrVideo = (ImageView) getView().findViewById(R.id.photos_or_video);
        photosOrVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), PhotoOrVideoActivity.class));
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        BaseActivity.isviewfrom="";
    }

    @Override
    public void onDetach() {
        super.onDetach();
        BaseActivity.isviewfrom="NoView";

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BaseActivity.isviewfrom="NoView";
    }

    /**
     * Display clinic profile
     */
    public class DisplayClinicProfile {
        DisplayClinicProfile(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                //Log.v("JSON", jsonObject.toString());
                JSONArray profileDataArray = jsonObject.getJSONArray("data");
                final String totalWorkDoctors = jsonObject.getString("totalDcotors");
                Log.v("total doctors", totalWorkDoctors);
                total_doctors.setText(totalWorkDoctors + " " +
                    getResources().getString(R.string.doctors));

                total_doctors.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(getContext(), doctorCounter, Toast.LENGTH_LONG).show();
                        //Preference.setValue(getContext(), "GLOBAL_FILTER_TYPE", "");
                        if(totalWorkDoctors.equals("0")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                            builder.setTitle(getResources().getString(R.string.clinic_doctors))
                                    .setMessage(getResources().getString(R.string.no_doctors_are_in_this_clinic))
                                    .setCancelable(false)
                                    .setNegativeButton(getResources().getString(R.string.close),new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        } else {
                            //check for Internet
                            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                return;
                            }
                            Bundle args = new Bundle();
                            args.putString("id", clinicId);
                            args.putString("hospitalName", clinicName);

                            FragmentManager mFragmentManager;
                            mFragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction mFragmentTransaction;
                            mFragmentTransaction = mFragmentManager.beginTransaction();

                            HospitalDoctorListFragment hospitalDoctorListFragment = new HospitalDoctorListFragment();
                            hospitalDoctorListFragment.setArguments(args);

                            mFragmentTransaction.addToBackStack("clinicDoctorList");
                            mFragmentTransaction.replace(R.id.search_layout, hospitalDoctorListFragment).commit();
                        }
                    }
                });

                for(int i = 0; i < profileDataArray.length(); i++) {
                    final JSONObject profile = profileDataArray.getJSONObject(i);
                    Log.v("Name", profile.toString());
                    //name
                    TextView name = (TextView) getView().findViewById(R.id.profile_detail_name);
                    name.setText(profile.getString("Name"));
                    name.setTypeface(typeface, typeface.BOLD);
                    clinicName = profile.getString("Name");

                    //specilality


                    /*try{
                    ImageView specialityImage = (ImageView) getView().findViewById(R.id.speciality);
                    int specialityId = getContext().getResources().getIdentifier(profile.getString("Mipmap"),
                            "mipmap",
                            getContext().getPackageName());
                    specialityImage.setImageResource(specialityId);

                    }catch (Exception e)
                    {
                        Log.v("Exception",e.getMessage()+"");
                    }*/

                    //title
                    TextView title = (TextView) getView().findViewById(R.id.profile_detail_title);
                    title.setText(profile.getString("Name"));
                    title.setTypeface(typeface, typeface.BOLD);

                    //total review
                    TextView totalReview = (TextView) getView().findViewById(R.id.total_reviews);
                    totalReview.setText(profile.getString("TotalReview") + " " +getResources().getString(R.string.reviews));
                    totalReview.setTypeface(typeface);
                    totalReview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //check for Internet
                            if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                return;
                            }
                            if (isGuest) {
                                guestLogin.confirmAlert(getContext());
                                return;
                            }
                            Bundle bundle = new Bundle();
                            bundle.putString("id", clinicId);
                            bundle.putString("name", clinicName);

                            FragmentManager fragmentManager;
                            fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction;
                            fragmentTransaction = fragmentManager.beginTransaction();

                            HospitalReview reviews = new HospitalReview();
                            reviews.setArguments(bundle);

                            fragmentTransaction.addToBackStack("clinic_review");
                            fragmentTransaction.replace(R.id.search_layout, reviews).commit();
                        }
                    });

                    //avg review
                    RatingBar ratingBar = (RatingBar) getView().findViewById(R.id.doctor_user_rating);
                    if (! (profile.getString("RatingAvg").equals("null") ||
                            profile.getString("RatingAvg").isEmpty())) {
                        ratingBar.setRating(Float.valueOf(profile.getString("RatingAvg")));
                        clinicReview = profile.getString("RatingAvg");
                    } else {
                        ratingBar.setRating(0.0f);
                        clinicReview = "0";
                    }

                    //address
                    TextView address = (TextView) getView().findViewById(R.id.textView6);
                    address.setText(profile.getString("Address"));
                    address.setTypeface(typeface);
                    clinicAddress = profile.getString("Address");

                    //phone
                    /**
                     * Nikunj
                     * Date 14 Dec 2016 5:40 PM
                     *
                     * If Phone number is not available it's field is gone
                     * updating at 19 Dec 2016 12 noon for removing claim profile
                     */

                    RelativeLayout relativeLayoutPhone = (RelativeLayout) getView().findViewById(R.id.clinic_call);

                    View viewPhone = getView().findViewById(R.id.clinic_call_view);
                    if(profile.getString("Phone").equals("") || profile.getString("Phone").equals("null"))
                    {
                       //----
                        TextView textView7 = (TextView) getView().findViewById(R.id.textView7);
                        ImageView imageView6 = (ImageView) getView().findViewById(R.id.imageView6);
                        TextView clinicNo = (TextView) getView().findViewById(R.id.clinicNo);
                        clinicNo.setPaddingRelative(0,0,0,10);
                        textView7.setPaddingRelative(getResources().getDimensionPixelSize(R.dimen.main_container_padding_start_end),0,0,10);
                        imageView6.setPadding(0,0,0,10);

                        //----
                        relativeLayoutPhone.setVisibility(View.GONE);
                        viewPhone.setVisibility(View.GONE);

                    }else{
                        relativeLayoutPhone.setVisibility(View.VISIBLE);
                        viewPhone.setVisibility(View.VISIBLE);
                    }


                    TextView clinicNo = (TextView) getView().findViewById(R.id.clinicNo);
                    clinicNo.setTypeface(typeface);
                    clinicNo.setText(" ("+ profile.getString("Phone") +")");
                    TextView doctorPhone = (TextView) getView().findViewById(R.id.textView8);
                    //doctorPhone.setText(getResources().getString(R.string.call) + " (" + profile.getString("Phone") + ")");
                    doctorPhone.setTypeface(typeface);
                    clinicPhone = profile.getString("Phone");

                    //get direction
                    TextView getDirection = (TextView) getView().findViewById(textView7);
                    getDirection.setTypeface(typeface);

                    //write a review
                    TextView writeReview = (TextView) getView().findViewById(R.id.write_a_review_button);
                    writeReview.setTypeface(typeface);
                    writeReview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (isGuest) {
                                guestLogin.confirmAlert(getContext());
                                return;
                            }
                            Intent intent = new Intent(getContext(), WriteReviewWorkType.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("id", clinicId);
                            bundle.putString("hospitalName", clinicName);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });

                    clinicWriteReview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getContext(), WriteReviewWorkType.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("id", clinicId);
                            bundle.putString("hospitalName", clinicName);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });

                    //claim profile
                    final String profileName = profile.getString("Name");
                    claimProfile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (isGuest) {
                                guestLogin.confirmAlert(getContext());
                                return;
                            }
                            Intent intent = new Intent(getContext(), ClaimProfileActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("name", profileName);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });
                    /*makeEnquiry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getContext(), MakeEnquiryActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("user_id",loginUserId);
                            bundle.putString("work_id", clinicId);
                            bundle.putString("work_type", "Clinic");
                            bundle.putString("name", profileName);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });*/

                    final String callDialer = profile.getString("Phone");
                    RelativeLayout hospitalCall = (RelativeLayout) getView().findViewById(R.id.clinic_call);
                    hospitalCall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
                            builderSingle.setIcon(R.mipmap.call);
                            builderSingle.setTitle(R.string.select_one_number);

                            String search = "--";
                            String search1 = "-";
                            String[] array = new String[10];
                            if (callDialer.indexOf(search) != -1) {
                                array = callDialer.replaceAll(" ", "").split("\\--", -1);
                            } else if (callDialer.indexOf(search1) != -1) {
                                array = callDialer.replaceAll(" ", "").split("\\-", -1);
                            }

                            if (array.length == 10) {
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:" + callDialer.trim()));
                                startActivity(intent);
                            } else {
                                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
                                arrayAdapter.addAll(array);

                                builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String strName = arrayAdapter.getItem(which);
                                        Intent intent = new Intent(Intent.ACTION_DIAL);
                                        intent.setData(Uri.parse("tel:" + strName));
                                        startActivity(intent);

                                    }
                                });
                                builderSingle.show();
                            }
                        }
                    });

                    if(!profile.getString("Latitude").equals("null") &&
                            !profile.getString("Latitude").isEmpty() &&
                            !profile.getString("Longitude").equals("null") &&
                            !profile.getString("Longitude").isEmpty()) {
                        latitude = profile.getDouble("Latitude");
                        longitude = profile.getDouble("Longitude");
                    }

                    if(latitude == 0.0 || longitude == 0.0)
                    {
                        View border2 = getView().findViewById(R.id.viewWidth);
                        border2.setVisibility(View.GONE);
                        LinearLayout layout = (LinearLayout) getView().findViewById(R.id.clinic_profile_map_layout);
                        layout.setVisibility(View.GONE);
                        map_relative_layout.setVisibility(View.GONE);
                    }

                    //set latitude and longitude
                    clinicMap = new LatLng(latitude, longitude);
                    final String clinicAddress = profile.getString("Address");
                    //google map
                    try {
                        FragmentManager fragmentManager = getChildFragmentManager();
                        supportMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.clinic_profile_map_layout);
                        if(supportMapFragment == null) {
                            supportMapFragment = SupportMapFragment.newInstance();
                            supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                                @Override
                                public void onMapReady(final GoogleMap map) {
                                    try{

                                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                                    //map.setMyLocationEnabled(true);
                                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(clinicMap, 15));
                                    map.addMarker(new MarkerOptions().position(clinicMap).title(clinicAddress).icon(BitmapDescriptorFactory.fromResource(R.mipmap.clinic_dark)));
                                    map.setTrafficEnabled(true);
                                    map.setIndoorEnabled(true);
                                    map.setBuildingsEnabled(true);
                                        map.getUiSettings().setScrollGesturesEnabled(true);
                                    map.getUiSettings().setZoomControlsEnabled(true);

                                    }catch (SecurityException e)
                                    {
                                        Log.v("SecurityException",""+e.getMessage());
                                    }
                                    catch (Exception e)
                                    {
                                        Log.v("Exception",""+e.getMessage());
                                    }
                                }
                            });
                            fragmentManager.beginTransaction().replace(R.id.clinic_profile_map_layout, supportMapFragment).commit();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //get direction
                    RelativeLayout relativeLayout = (RelativeLayout) getView().findViewById(R.id.clinic_get_direction);
                    relativeLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(String.valueOf(latitude).isEmpty() || String.valueOf(longitude).isEmpty()) {
                                Toast.makeText(getContext(), getResources().getString(R.string.location_is_not_available),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                final Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://maps.google.com/maps?" + "saddr=my location" +"&daddr=" + latitude + "," + longitude));
                                intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
                                startActivity(intent);
                            }

                        }
                    });

                }

                JSONArray hospitalSpec = jsonObject.getJSONArray("mipmap");
                //hospital spec
                int j = 0;
                for (int i = 0; i < hospitalSpec.length(); i++) {
                    if (j > 6) break;
                    j++;
                    final String hospitalSpeciality = hospitalSpec.getString(i);
                    Log.v("Spec", hospitalSpeciality);
                    ImageView spec = new ImageView(getContext());

                    int mipmapSize = GetSearchTitleMarginStart();
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(mipmapSize, mipmapSize);
                    spec.setLayoutParams(layoutParams);

                    if(hospitalSpeciality.isEmpty()) {
                        spec.setImageResource(R.mipmap.teath);
                        spec.setVisibility(View.INVISIBLE);
                    } else {
                        int mipmapId = getContext().getResources().getIdentifier(hospitalSpeciality, "mipmap", getContext().getPackageName());
                        spec.setImageResource(mipmapId);
                    }
                    //int dp = (int) (get.getResources().getDimension(R.dimen.add_doctor_input_font) / getContext().getResources().getDisplayMetrics().density);

                    //spec.setMinimumHeight(100);
                    //spec.setMaxHeight(50);
                    spec.requestLayout();
                    //spec.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
                    clinic_speciality.addView(spec);
                }







                //bookmark
                final LinearLayout withoutBookmark = (LinearLayout) getView().findViewById(R.id.withoutBookmark);
                final LinearLayout withBookmark = (LinearLayout) getView().findViewById(R.id.withBookmark);
                String bookmarkStatus = jsonObject.getString("BookmarkStatus");
                String bookmarkUserId = jsonObject.getString("BookmarkUserId");
                final ImageView addToBookmarks = (ImageView) getView().findViewById(R.id.clinicBookmark);
                addToBookmarks.setTag(1);
                if ( ! (bookmarkStatus.equals("null")) || (bookmarkStatus.isEmpty())) {
                    if (bookmarkUserId.equals(loginUserId) && bookmarkStatus.equals("1")) {
                        addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                        addToBookmarks.setTag(2);
                        withBookmark.setVisibility(View.VISIBLE);
                        withoutBookmark.setVisibility(View.GONE);
                    }
                }
                addToBookmarks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //check for Internet
                        if ( ! NetworkInfoHelper.isOnline(getContext())) {
                            Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                            return;
                        }
                        if (isGuest) {
                            guestLogin.confirmAlert(getContext());
                            return;
                        }
                        workTypeBookmark = new WorkTypeBookmark();
                        workTypeBookmark.execute();
                        if (Integer.parseInt(addToBookmarks.getTag().toString()) == 1) {
                            addToBookmarks.setImageResource(R.mipmap.bookmark_2);
                            addToBookmarks.setTag(2);
                            withBookmark.setVisibility(View.VISIBLE);
                            withoutBookmark.setVisibility(View.GONE);
                        } else {
                            addToBookmarks.setImageResource(R.mipmap.bookmarks);
                            addToBookmarks.setTag(1);
                            withBookmark.setVisibility(View.GONE);
                            withoutBookmark.setVisibility(View.VISIBLE);
                        }
                    }
                });
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * share it on social
     * @param name
     * @param phone
     * @param address
     * @param speciality
     * @param rating
     */
   /* public void shareIt(final String name, final String phone, final String address, final String speciality, final String rating) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Name: " + name + "\n" + "Phone: " + phone + "\n" + "Address: "
                + address + "\n" + "Speciality: " + speciality + "\n" + "Rating: " + rating);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.social_text)));
    }*/


    public void shareIt(final String name, final String phone, final String address, final String speciality, final String rating) {
        if (isGuest) {
            guestLogin.confirmAlert(getContext());
            return;
        }
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Name: " + name + "\n" + "Phone: " + phone + "\n" + "Address: "
                + address + "\n" + "Speciality: " + speciality + "\n" + "Rating: " + rating + "\n" + "Profile: "
                + AppConfig.getWebSiteUrl() + "workProfile/index/" + clinicId);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.social_text)));
    }

    /**
     * all reviews async
     */
    class HospitalAllReview extends AsyncTask<String, Void, String> {

        String URL = WEB_SERVICE_URL + "work_review/ratingUserDetail";
        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("workId", clinicId));
            nameValuePairs.add(new BasicNameValuePair("lang", appLang));

            try {
                HttpPost httpPost = new HttpPost(URL);
                Log.v("Clinic URL", URL);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                return mClient.execute(httpPost, responseHandler);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                mClient.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            new RenderClinicReview(result);
            progressBar.setVisibility(View.GONE);
            clinicProfileDetails.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * render clinic review
     */
    class RenderClinicReview {
        RenderClinicReview(String result) {
            LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.allReviewList);
            linearLayout.removeAllViews();
            LinearLayout.LayoutParams textLayoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            );

            try {
                JSONObject jsonObject = new JSONObject(result);
                Log.v("review data", jsonObject.toString());
                if (jsonObject.getString("status").equals("false")) {
                    clinicWriteReview.setVisibility(View.VISIBLE);
                    avgRating.setVisibility(View.GONE);
                    /*TextView textView = new TextView(getContext());
                    textView.setText(getResources().getString(R.string.no_review_found));
                    textView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                    textView.setTextColor(getResources().getColor(R.color.black));
                    textView.setTypeface(typeface);
                    textView.setTextSize(20);
                    textView.setLayoutParams(textLayoutParam);

                    linearLayout.addView(textView);*/
                } else {
                    clinicWriteReview.setVisibility(View.GONE);
                    avgRating.setVisibility(View.VISIBLE);
                    JSONArray reviewArray = jsonObject.getJSONArray("data");
                    int totalClinicReview = reviewArray.length();
                    totalReviewCount = totalClinicReview;
                    TextView totalReview = (TextView) getView().findViewById(R.id.total_reviews);
                    totalReview.setText(String.valueOf(totalClinicReview) + " " + getResources().getString(R.string.reviews));
                    for (int i = 0; i < reviewArray.length(); i++) {
                        final JSONObject filterData = reviewArray.getJSONObject(i);
                        View view;
                        LayoutInflater layoutInflater = (LayoutInflater) getContext()
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = layoutInflater.inflate(R.layout.doctor_review_list, null);

                        //user name
                        TextView textView1 = (TextView) view.findViewById(R.id.user_name);
                        textView1.setTypeface(typeface, typeface.BOLD);
                        textView1.setTextColor(Color.parseColor("#010101"));
                        if (filterData.has("visible") && filterData.getString("visible").equals("1")) {
                            textView1.setText(filterData.getString("name"));
                        } else {
                            textView1.setText(getResources().getString(R.string.anonymous));
                        }

                        //date time
                        TextView commentTime = (TextView) view.findViewById(R.id.review_date_time);
                        commentTime.setText(filterData.getString("commentTime"));
                        commentTime.setTypeface(typeface);

                        //user rating
                        RatingBar userRating = (RatingBar) view.findViewById(R.id.user_review);
                        userRating.setRating(Float.valueOf(filterData.getString("avgRating")));

                        //user comment
                        TextView userComment = (TextView) view.findViewById(R.id.user_review_text);
                        userComment.setText(filterData.getString("comment"));
                        userComment.setTypeface(typeface);

                        //total photos and videos
                        TextView totalPhotoAndVideos = (TextView) view.findViewById(R.id.total_photo_video);
                        totalPhotoAndVideos.setTypeface(typeface);

                        //share review
                        TextView shareReview = (TextView) view.findViewById(R.id.share_review);
                        shareReview.setTypeface(typeface, typeface.BOLD);

                        shareReview.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                try {
                                    shareItUserReview(filterData.getString("name"), "", "", filterData.getString("avgRating"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        //photo and video
                        ImageView photoAndVideo = (ImageView) view.findViewById(R.id.photoAndVideo);
                        photoAndVideo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast.makeText(getContext(),
                                        getResources().getString(R.string.no_photos_to_show), Toast.LENGTH_LONG).show();
                                //startActivity(new Intent(getContext(), PhotoOrVideoActivity.class));
                            }
                        });

                        linearLayout.addView(view);
                        final String profileUserId = filterData.getString("userId");
                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //check for Internet
                                if ( ! NetworkInfoHelper.isOnline(getContext())) {
                                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connectivity), Toast.LENGTH_SHORT).show();

                                    return;
                                }
                                try {
                                    if (filterData.has("visible") && filterData.getString("visible").equals("1")) {
                                        userReviewDetails(profileUserId, filterData.getString("name"));
                                    } else {
                                        Toast.makeText(getContext(),
                                                getResources().getString(R.string.anonymous_profile_alert_message), Toast.LENGTH_LONG).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Log.v("Clicked user id", profileUserId);
                            }
                        });

                        ImageView profilePic = (ImageView) view.findViewById(R.id.user_profile_pic);


                        if (filterData.has("photo") &&
                                filterData.getString("visible").equals("1")) {
                            Picasso.with(getContext())
                                    .load(filterData.getString("photo"))
                                    .resize(50, 50)
                                    .centerCrop()
                                    .into(profilePic);
                            /*new DownloadImageTask(profilePic)
                                    .execute(filterData.getString("photo"));*/
                        } else {
                            profilePic.setImageResource(R.mipmap.first_name);
                        }

                        //review icon
                        ImageView reviewIconImage = (ImageView) view.findViewById(R.id.reviewIcon);
                        if (filterData.has("reviewIcon")) {
                            final String reviewIcon = filterData.getString("reviewIcon");
                            if (!reviewIcon.equals("0")) {
                                int iconId = getResources().getIdentifier("hospital_review_" + reviewIcon,
                                        "drawable", getContext().getPackageName());
                                reviewIconImage.setImageResource(iconId);
                                iconList.add("hospital_review_" + reviewIcon);
                            }

                        }
                    }
                }
                if (iconList.size() > 0) {
                    set.addAll(iconList);
                    TableRow tableRow = (TableRow) getView().findViewById(R.id.iconRow);
                    tableRow.setVisibility(View.VISIBLE);
                    int dp = -20;
                    for (String temp : set) {
                        int id = getContext().getResources().getIdentifier(temp, "drawable",
                                getContext().getPackageName());
                        int imageid = getContext().getResources().getIdentifier(temp, "id",
                                getContext().getPackageName());
                        if (id != 0) {
                            ImageView imageView = (ImageView) getView().findViewById(imageid);
                            imageView.setImageResource(id);
                            imageView.setVisibility(View.VISIBLE);
                            float d = getContext().getResources().getDisplayMetrics().density;
                            int margin = (int) (dp * d);
                            imageView.setPadding(margin, 0, margin, 0);
                            dp += 30;
                        }
                    }
                    Log.v("totalreview", String.valueOf(totalReviewCount));
                    TextView textViewTotal = (TextView) getView().findViewById(R.id.totalIconCount);
                    textViewTotal.setText(String.valueOf(totalReviewCount));
                    textViewTotal.setTypeface(typeface);
                    //textViewTotal.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void userReviewDetails(final String reviewId, final String name) {
        Bundle args = new Bundle();
        args.putString("id", reviewId);
        args.putString("name", name);
        args.putString("type", "work");
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        ReviewDetailsFragment reviewDetailsFragment = new ReviewDetailsFragment();
        reviewDetailsFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("reviewDetails");
        mFragmentTransaction.add(R.id.search_layout, reviewDetailsFragment).commit();
    }

    public void userProfile(final String userId) {
        Bundle args = new Bundle();
        args.putString("id", userId);
        FragmentManager mFragmentManager;
        mFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mFragmentManager.beginTransaction();

        UserProfileFragment profileFragment = new UserProfileFragment();
        profileFragment.setArguments(args);

        mFragmentTransaction.addToBackStack("user_profile");
        mFragmentTransaction.add(R.id.search_layout, profileFragment).commit();
    }

    /**
     * Share it on social
     * @param name string
     * @param phone string
     * @param address string
     */
    public void shareItUserReview(final String name, final String phone, final String address, final String rating) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Name: " + name + "\n" + "Phone: " + phone + "\n" + "Address: "
                + address + "\n" + "Rating: " + rating);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.social_text)));

    }

    @Override
    public void onStop() {
        super.onStop();
        if (clinicProfile != null) {
            clinicProfile.cancel(true);
        }
        if (workTypeBookmark != null) {
            workTypeBookmark.cancel(true);
        }
        if (hospitalAllReview != null) {
            hospitalAllReview.cancel(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BaseActivity.buttonFragment = "ClinicProfile";
        //hospitalAllReview = new HospitalAllReview();
        //hospitalAllReview.execute();
        /*BaseActivity.mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                BaseActivity.mNavigationView.getMenu().findItem(item.getItemId()).setChecked(true);

                if(item.getItemId() == R.id.write_a_review) {
                    clinicWriteReview.performClick();
                }

                if(item.getItemId() == R.id.claim_profile) {
                    claimProfile.performClick();
                }

                if(item.getItemId() == R.id.share) {
                    socialShare.performClick();
                }
                BaseActivity.mDrawerLayout.closeDrawers();
                return false;
            }
        });*/
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            //navProfilePic.setImageBitmap(result);
            //progressBar.setVisibility(View.GONE);
            //profileDetails.setVisibility(View.VISIBLE);
        }
    }

    public void fetchClinicReview() {

        class DoctorReview extends AsyncTask<String, Void, String> {

            String URL = WEB_SERVICE_URL + "work_review/reviewRating";

            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("workId", clinicId));
                nameValuePairs.add(new BasicNameValuePair("lang", appLang));

                try {
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    return mClient.execute(httpPost, responseHandler);
                } catch(IOException e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                /*if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }*/

                setRatings(result);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                /*mProgressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();*/
            }

        }

        DoctorReview doctorReview = new DoctorReview();
        doctorReview.execute();

    }

    public void setRatings(String result) {
        try {
            //main rating object
            JSONObject mainRatingObject = new JSONObject(result);
            JSONObject ratingOnject = mainRatingObject.getJSONObject("data");

            //Reputation
            JSONObject reputationObject = ratingOnject.getJSONObject("reputation");
            RatingBar reputationRatingBar = (RatingBar) getView().findViewById(R.id.reputation_rating);
            TextView totalReputationReview = (TextView) getView().findViewById(R.id.total_reputation_review);
            reputationRatingBar.setRating(Float.valueOf(reputationObject.getString("avg")));
            totalReputationReview.setText(reputationObject.getString("count"));

            //clinic
            JSONObject clinicObject = ratingOnject.getJSONObject("clinic");
            RatingBar clinicRatingBar = (RatingBar) getView().findViewById(R.id.clinic_ratingbar);
            TextView totalReviewClinicAccessibility = (TextView) getView().findViewById(R.id.total_review_clinic_accessibility);
            clinicRatingBar.setRating(Float.valueOf(clinicObject.getString("avg")));
            totalReviewClinicAccessibility.setText(clinicObject.getString("count"));

            //availability
            JSONObject availabilityObject = ratingOnject.getJSONObject("availability");
            RatingBar availabilityRatingBar = (RatingBar) getView().findViewById(R.id.availability_ratingbar);
            TextView totalAvailibilityInEmergencies = (TextView) getView().findViewById(R.id.total_availibility_in_emergencies);
            availabilityRatingBar.setRating(Float.valueOf(availabilityObject.getString("avg")));
            totalAvailibilityInEmergencies.setText(availabilityObject.getString("count"));

            //approachability
            JSONObject approachabilityObject = ratingOnject.getJSONObject("approachability");
            RatingBar approachabilityRatingBar = (RatingBar) getView().findViewById(R.id.approachability_ratingbar);
            TextView totalReviewApprochability = (TextView) getView().findViewById(R.id.total_review_approachability);
            approachabilityRatingBar.setRating(Float.valueOf(approachabilityObject.getString("avg")));
            totalReviewApprochability.setText(approachabilityObject.getString("count"));

            //technology
            JSONObject technologyObject = ratingOnject.getJSONObject("technology");
            RatingBar technologyRatingBar = (RatingBar) getView().findViewById(R.id.technology_ratingbar);
            TextView totalReviewTechnolotyAndEquipment = (TextView) getView().findViewById(R.id.total_review_technology_and_equipment);
            technologyRatingBar.setRating(Float.valueOf(technologyObject.getString("avg")));
            totalReviewTechnolotyAndEquipment.setText(technologyObject.getString("count"));

            //grab all reviews
            //getAllUserReview(id);
            //Log.v("doctor id", String.valueOf(id));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        BaseActivity.hideMenu();
    }

    public int GetSearchTitleMarginStart() {
        float density = getResources().getDisplayMetrics().density;
        //Toast.makeText(getContext(), "density: " + density, Toast.LENGTH_LONG).show();
        int margin = 0;
        //Toast.makeText(getContext(), "density: " + density, Toast.LENGTH_LONG).show();
        if(density >= 0.75 && density < 1.0) {

        } else if(density >= 1.0 && density < 1.5) {
            margin = 20;
        } else if(density >= 1.5 && density < 2.0) {
            margin = 30;
        } else if (density >= 2.0 && density <= 2.5) {
            margin = 45;
        } else if(density > 2.5 && density < 3.0) {
            margin = 70;
        } else if(density >= 3.0 && density < 3.5) {
            margin = 80;
        } else if(density >= 3.5 && density <= 4.0) {
            margin = 95;
        } else if(density > 4.0) {

        } else {

        }
        return margin;
    }



}
