package searchnative.com.topdoctors;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by root on 21/9/16.
 */
public class SearchFilterPop extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,LocationListener{

    Spinner locationSpinner, specialitySpinner, spinnerGender;
    String webServiceUrl = AppConfig.getWebServiceUrl();
    ArrayAdapter<String> locationArrayAdapter, specialityArrayAdapter;
    List<String> locationList, specialityList, specialityListIcon;
    Typeface typeface;
    String[] genderArray;
    EditText editText;
    Button searchButton, resetButton;
    TextView titleText;
    String getLocation, getSpeciality, getGender, getName;
    String prevLocation, prevSpeciality, prevGender;
    private String globalSearchType;
    Integer genderIndex;
    CheckBox checkNearBy,checkTopRated;
    private String isNearby = "0", isTopRated = "0";

    Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    double lat = 0.0,lon = 0.0;
    View dialoglayout;
    AlertDialog.Builder alert;
    AlertDialog dialog;
    RelativeLayout genderRelativeLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        Log.v("rated", Preference.getValue(SearchFilterPop.this, "TOP_RATED_FIRST", ""));
        LayoutInflater inflater = (LayoutInflater) LayoutInflater.from(this);
        dialoglayout = inflater.inflate(R.layout.search_filter, null);


        //Toast.makeText(SearchFilterPop.this, Preference.getValue(SearchFilterPop.this, "GLOBAL_FILTER_TYPE", ""), Toast.LENGTH_LONG).show();
        globalSearchType = Preference.getValue(SearchFilterPop.this, "GLOBAL_FILTER_TYPE", "");
        prevLocation = Preference.getValue(SearchFilterPop.this, "DOCTOR_LOCATION_SEARCH", "").trim();
        prevSpeciality = Preference.getValue(SearchFilterPop.this, "DOCTOR_SPECIALITY_SEARCH", "");
        prevGender = Preference.getValue(SearchFilterPop.this, "DOCTOR_GENDER_SEARCH", "");
        genderRelativeLayout = (RelativeLayout) dialoglayout.findViewById(R.id.gender_relative_layout);
        Log.v("hello",prevLocation);
        if(Preference.getValue(SearchFilterPop.this, "DOCTOR_GENDER_SEARCH", "").isEmpty()) {
            genderIndex = 0;
        } else {
            genderIndex = Integer.parseInt(Preference.getValue(SearchFilterPop.this, "DOCTOR_GENDER_SEARCH", ""));
        }
        //Log.v("Gender", String.valueOf(genderIndex));
        //Log.v("Pop up", prevSpeciality);
        //globalFilterType = Preference.getValue(SearchFilterPop.this, "GLOBAL_FILTER_TYPE", "");

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));



//        setContentView(R.layout.search_filter);


        boolean isCustom = getIntent().getBooleanExtra("isCustom",false);

        if(isCustom) {
            addCustomAlertView();
        }

        try{

            if (!isGooglePlayServicesAvailable()) {
                Toast.makeText(SearchFilterPop.this, "Google Play Service is unavailable", Toast.LENGTH_SHORT).show();
            }

            buildGoogleApiClient();

        }catch (Exception e)
        {
            Log.v("Location Exception",""+e.getMessage());
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        typeface = Typeface.createFromAsset(this.getAssets(), "fonts/ExoMedium.otf");



        locationSpinner = (SearchableSpinner) dialoglayout.findViewById(R.id.popup_spinner_location);
        specialitySpinner = (Spinner) dialoglayout.findViewById(R.id.popup_spinner_speciality);
        spinnerGender = (Spinner) dialoglayout.findViewById(R.id.popup_spinner_gender);
        searchButton = (Button) dialoglayout.findViewById(R.id.popup_search_button);
        titleText = (TextView) dialoglayout.findViewById(R.id.textView2);
        resetButton = (Button) dialoglayout.findViewById(R.id.reset_filter);

        checkNearBy = (CheckBox) dialoglayout.findViewById(R.id.popup_neary_by_first);
        checkTopRated = (CheckBox) dialoglayout.findViewById(R.id.top_rated_first);



        /*locationSpinner = (SearchableSpinner) findViewById(R.id.popup_spinner_location);
        specialitySpinner = (Spinner) findViewById(R.id.popup_spinner_speciality);
        spinnerGender = (Spinner) findViewById(R.id.popup_spinner_gender);
        searchButton = (Button) findViewById(R.id.popup_search_button);
        titleText = (TextView) findViewById(R.id.textView2);
        resetButton = (Button) findViewById(R.id.reset_filter);*/


        //check for global type
        if(globalSearchType.equals("clinic") || globalSearchType.equals("hospital") || globalSearchType.equals("lab")
                || globalSearchType.equals("Radiology Lab") || globalSearchType.equals("Medical Lab")) {
            prevGender = "";
            genderRelativeLayout.setVisibility(View.GONE);
            View view = dialoglayout.findViewById(R.id.gender_border);
            view.setVisibility(View.GONE);
        }

        locationList = new ArrayList<String>();
        locationList.add(getResources().getString(R.string.location));
        locationList.addAll(LocationData.getmInstance().locationList);
        locationArrayAdapter = new ArrayAdapter<String>(SearchFilterPop.this,
                android.R.layout.simple_spinner_dropdown_item, locationList) {
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;
                //View view  = super.getDropDownView(position, convertView, parent);
                if(position == 0) {
                    TextView textView = new TextView(getContext());
                    textView.setHeight(0);
                    textView.setVisibility(View.GONE);
                    textView.setTextColor(Color.BLACK);
                    v = textView;
                } else {
                    v = super.getDropDownView(position, null, parent);
                }

                return v;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTypeface(typeface);
                ((TextView) v).setTextColor(getResources().getColor(R.color.black));
                if(position == 0) {
                    ((TextView) v).setTextColor(getResources().getColor(R.color.textHighlightColor));
                }
                ((TextView) v).setTextSize(18);
                return v;
            }
        };
        //loadLocationSpinner();
        locationArrayAdapter.setNotifyOnChange(true);
        locationSpinner.setAdapter(locationArrayAdapter);

        for(int i = 0; i < locationArrayAdapter.getCount(); i++) {
            if ( ! prevLocation.equals(""))
                //Log.d("data","data"+locationArrayAdapter.getItem(i).toString());
                //String data = "" + locationArrayAdapter.getItem(i).toString();
                if(prevLocation.trim().equals(locationArrayAdapter.getItem(i).toString().trim())) {
                    locationSpinner.setSelection(i);
                }
        }

        specialityList = new ArrayList<String>();
        specialityListIcon = new ArrayList<>();
        specialityList.add(getResources().getString(R.string.speciality));
        specialityList.addAll(SpecialityData.getmInstance().specialityList);
        final CustomAdapter customAdapter=new CustomAdapter(getApplicationContext(), SpecialityData.getmInstance().specialityListIcon, specialityList);
        specialitySpinner.setAdapter(customAdapter);

        for(int i = 0; i < SpecialityData.getmInstance().specialityList.size(); i++) {
            if(prevSpeciality.trim().equals(SpecialityData.getmInstance().specialityList.get(i).toString())) {
                specialitySpinner.setSelection(i + 1);
                Log.v("Spec data: ", Preference.getValue(SearchFilterPop.this, "DOCTOR_SPECIALITY_SEARCH",""));
            }
        }

        specialitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i > 0) {
                    getSpeciality = SpecialityData.getmInstance().specialityList.get(i - 1).toString();
                    prevSpeciality = getSpeciality;
                    //Preference.setValue(SearchFilterPop.this, "FILTER_SPECIALITY", getSpeciality);
                    Preference.setValue(SearchFilterPop.this, "FILTER_SPECIALITY", getSpeciality);
                    Preference.setValue(SearchFilterPop.this, "DOCTOR_SPECIALITY_SEARCH", getSpeciality);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        loadGenderSpinner();

        editText = (EditText) dialoglayout.findViewById(R.id.popup_edittext_name);
//        editText = (EditText) findViewById(R.id.popup_edittext_name);
        editText.setTypeface(typeface);
        searchButton.setTypeface(typeface);
        resetButton.setTypeface(typeface);
        titleText.setTypeface(typeface);

        String fromSearch = Preference.getValue(SearchFilterPop.this, "SEARCH_KEYWORD", "");
        Log.v("From search", fromSearch);
        Preference.setValue(SearchFilterPop.this, "FILTER_NAME", fromSearch);

        //set filter values
        setPreviousFilterData();
        getLocation();
        getSpeciality();
        getGender();
        //getName();

        Preference.setValue(SearchFilterPop.this, "IS_SEARCH", "");

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getName = editText.getText().toString();
                Preference.setValue(SearchFilterPop.this, "FILTER_NAME", getName);
                Preference.setValue(SearchFilterPop.this, "FILTER_NEARBY", isNearby);
                Log.d("iftop","iftop"+isTopRated);
                Preference.setValue(SearchFilterPop.this, "FILTER_RATED", isTopRated);
                Preference.setValue(SearchFilterPop.this, "IS_SEARCH", "1");
                Preference.setValue(SearchFilterPop.this, "SEARCH_KEYWORD", getName);
                Log.v("Search from popup", "true");
                Log.v("rated", Preference.getValue(SearchFilterPop.this, "TOP_RATED_FIRST", ""));
                Log.v("nearby", Preference.getValue(SearchFilterPop.this, "IS_NEARBY", ""));

                // if NearBy is Checked then setting latitude and longitude at Preference
                if(checkNearBy.isChecked())
                {
                    setLatLongToPreference();
                }


                dialog.dismiss();

                finish();
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog =   new AlertDialog.Builder(SearchFilterPop.this);
                alertDialog.setTitle(getResources().getString(R.string.reset));
                alertDialog.setMessage(getResources().getString(R.string.reset_search_filter_message));
                alertDialog.setCancelable(true);

                alertDialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                        //reset location

                        Preference.setValue(SearchFilterPop.this, "GLOBAL_FILTER_TYPE", "");
                        Preference.setValue(SearchFilterPop.this, "PROMARY_SEARCH_KEY", "");
                        genderRelativeLayout.setVisibility(View.VISIBLE);
                        locationSpinner.setAdapter(null);
                        locationList = new ArrayList<String>();
                        locationList.add(getResources().getString(R.string.location));
                        locationSpinner.setAdapter(locationArrayAdapter);
                        prevLocation = getLocation = "";
                        Preference.setValue(SearchFilterPop.this, "FILTER_LOCATION", "");
                        Preference.setValue(SearchFilterPop.this, "DOCTOR_LOCATION_SEARCH", "");

                        //reset speciality
                        specialitySpinner.setAdapter(null);
                        specialityList = new ArrayList<String>();
                        specialityListIcon = new ArrayList<>();
                        specialityList.add(getResources().getString(R.string.speciality));
                        specialitySpinner.setAdapter(customAdapter);
                        prevSpeciality = getSpeciality = "";
                        Preference.setValue(SearchFilterPop.this, "FILTER_SPECIALITY", "");
                        Preference.setValue(SearchFilterPop.this, "DOCTOR_SPECIALITY_SEARCH", "");

                        //gender reset
                        spinnerGender.setAdapter(null);
                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(SearchFilterPop.this,
                                android.R.layout.simple_spinner_dropdown_item, genderArray) {

                            @Override
                            public boolean isEnabled(int position){
                                if(position == 0)
                                {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                }
                                else
                                {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                View v = null;
                                //View view  = super.getDropDownView(position, convertView, parent);
                                if(position == 0) {
                                    TextView textView = new TextView(getContext());
                                    textView.setHeight(0);
                                    textView.setVisibility(View.GONE);
                                    textView.setTextColor(Color.BLACK);
                                    v = textView;
                                } else {
                                    v = super.getDropDownView(position, null, parent);
                                }

                                return v;
                            }

                            @Override
                            public View getView(int position, View convertView, ViewGroup parent) {
                                View v = super.getView(position, convertView, parent);
                                ((TextView) v).setTypeface(typeface);
                                ((TextView) v).setTextColor(getResources().getColor(R.color.black));
                                if(position == 0) {
                                    ((TextView) v).setTextColor(getResources().getColor(R.color.textHighlightColor));
                                }
                                ((TextView) v).setTextSize(18);

                                return v;
                            }
                        };

                        // while resetting gender visibility gone for clinic , hospital and lab search
                        // nikunj + jaymin
                        // Date - 12 Dec 2016 7:00 PM
                        if(globalSearchType.equals("clinic") || globalSearchType.equals("hospital") || globalSearchType.equals("lab")
                                || globalSearchType.equals("Radiology Lab") || globalSearchType.equals("Medical Lab")) {
                            prevGender = "";
                            genderRelativeLayout.setVisibility(View.GONE);
                            View view = dialoglayout.findViewById(R.id.gender_border);
                            view.setVisibility(View.GONE);
                        }

                        spinnerGender.setAdapter(arrayAdapter);
                        prevGender = "";
                        getGender = "";
                        Preference.setValue(SearchFilterPop.this, "DOCTOR_GENDER_SEARCH", "");
                        Preference.setValue(SearchFilterPop.this, "FILTER_GENDER", "");

                        //name
                        Preference.setValue(SearchFilterPop.this, "FILTER_NAME", "");
                        editText.setText("");

                        //reset type
                        //Nikunj + Jaymin
                        // Date - 12 Dec 2016 7:00 PM
                        //globalSearchType = "";
                        //Preference.setValue(SearchFilterPop.this, "GLOBAL_FILTER_TYPE", "");


                    }
                });

                alertDialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                // Added for enable - disable gender
                // Nikunj + Jaymin
                // Date - 12 Dec 2016 7:00 PM
                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        //globalSearchType = "";
                        Preference.setValue(SearchFilterPop.this, "GLOBAL_FILTER_TYPE", globalSearchType);
                    }
                });
                alertDialog.show();


            }
        });
        //Preference.setValue(SearchFilterPop.this, "IS_SEARCH", "1");

        float density = getResources().getDisplayMetrics().density;
        double heightDensity = .41;
        //Toast.makeText(SearchFilterPop.this, "density: " + density, Toast.LENGTH_LONG).show();
        if(density >= 0.75 && density < 1.0) {
            heightDensity = .51;
        } else if(density >= 1.0 && density <= 1.5) {
            heightDensity = .49;
        } else if(density > 1.5 && density < 2.0) {
            heightDensity = .46;
        } else if (density >= 2.0 && density <= 2.5) {
            heightDensity = 0.45;
        } else if(density > 2.5 && density < 3.0) {
            heightDensity = 0.41;
        } else if(density >= 3.0 && density < 3.5) {
            heightDensity = .39;
        } else if(density >= 3.5 && density <= 4.0) {
            heightDensity = .37;
        } else if(density > 4.0) {
            heightDensity = .35;
        } else {
        }

        getWindow().setLayout((int) (width * .89), (int) (height * heightDensity));


    }

    public void setPreviousFilterData() {
        String name = Preference.getValue(SearchFilterPop.this, "FILTER_NAME", "");

        if(name.trim().length() > 0) {
            editText.setText(name);
        }
    }

    public void loadLocationSpinner() {

        class LocationData extends AsyncTask<Void, Void, String> {
            String URL = webServiceUrl + "country";
            AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

            @Override
            protected String doInBackground(Void... params) {
                HttpPost httpPost = new HttpPost(URL);
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                try {
                    return mClient.execute(httpPost, responseHandler);
                } catch(IOException e) {
                    e.printStackTrace();
                } finally {
                    mClient.close();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for(int i = 0; i < jsonArray.length(); i++) {
                        JSONObject country = jsonArray.getJSONObject(i);
                        locationArrayAdapter.add(country.getString("country_name"));
                    }

                    //prevLocation = Preference.getValue(SearchFilterPop.this, "FILTER_LOCATION", "");

                    for(int i = 0; i < locationArrayAdapter.getCount(); i++) {
                        if(prevLocation.trim().equals(locationArrayAdapter.getItem(i).toString())) {
                            locationArrayAdapter.getItem(i).toString().trim();
                            locationSpinner.setSelection(i);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        new LocationData().execute();
    }



    public void loadGenderSpinner() {
        genderArray = getResources().getStringArray(R.array.gender);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(SearchFilterPop.this,
                android.R.layout.simple_spinner_dropdown_item, genderArray) {

            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;
                //View view  = super.getDropDownView(position, convertView, parent);
                if(position == 0) {
                    TextView textView = new TextView(getContext());
                    textView.setHeight(0);
                    textView.setVisibility(View.GONE);
                    textView.setTextColor(Color.BLACK);
                    v = textView;
                } else {
                    v = super.getDropDownView(position, null, parent);
                }

                return v;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTypeface(typeface);
                ((TextView) v).setTextColor(getResources().getColor(R.color.black));
                if(position == 0) {
                    ((TextView) v).setTextColor(getResources().getColor(R.color.textHighlightColor));
                }
                ((TextView) v).setTextSize(18);

                return v;
            }
        };
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(arrayAdapter);

        //String prevGender = Preference.getValue(SearchFilterPop.this, "FILTER_GENDER", "");;
        spinnerGender.setSelection(genderIndex);
        /*for(int i = 0; i < arrayAdapter.getCount(); i++) {
            if(prevGender.trim().equals(arrayAdapter.getItem(i).toString())) {
                spinnerGender.setSelection(i);
            }
        }*/
    }

    public void getLocation() {
        locationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i > 0) {
//                    locationSpinner.getSelectedItem().toString().trim();
//                    String.valueOf(locationArrayAdapter.getItem(i));
//                    locationArrayAdapter.getItem(i).trim();
//                    locationArrayAdapter.notifyDataSetChanged();

                    TextView textView = (TextView) adapterView.getChildAt(0);
                    textView.setText(locationSpinner.getSelectedItem().toString().trim());
                    getLocation = locationSpinner.getSelectedItem().toString().trim();
                    prevLocation = getLocation;
                    Preference.setValue(SearchFilterPop.this, "FILTER_LOCATION", getLocation);
                    Preference.setValue(SearchFilterPop.this, "DOCTOR_LOCATION_SEARCH", getLocation);
                    //Log.v("Gender data", prevLocation);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void getSpeciality() {

    }

    public void getGender() {
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i > 0) {
                    genderIndex = i;
                    getGender = String.valueOf(genderIndex);
                    //getGender = spinnerGender.getSelectedItem().toString();
                    prevGender = getGender;
                    Log.v("Pr Gender", prevGender);
                    Preference.setValue(SearchFilterPop.this, "FILTER_GENDER", getGender);
                    Preference.setValue(SearchFilterPop.this, "DOCTOR_GENDER_SEARCH", prevGender);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(100); // Update location every second

        try{

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);


        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            lat = mLastLocation.getLatitude();
            lon = mLastLocation.getLongitude();
        }

        }catch (SecurityException e)
        {
            Log.v("SecurityException",""+e.getMessage());
        }
        catch (Exception e)
        {
            Log.v("Exception",""+e.getMessage());
        }
        //Toast.makeText(SearchFilterPop.this, ""+lat+","+lon, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lon = location.getLongitude();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        buildGoogleApiClient();
    }


    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }

    public void setLatLongToPreference()
    {
        if(lat != 0.0 && lon != 0.0)
        {
            Preference.setValue(SearchFilterPop.this, "Latitude", String.valueOf(lat));
            Preference.setValue(SearchFilterPop.this, "Longitude", String.valueOf(lon));

            /*Toast.makeText(SearchFilterPop.this, String.valueOf(lat) +", "+
                    String.valueOf(lon),Toast.LENGTH_SHORT).show();*/
        }
        else
        {
            Preference.setValue(SearchFilterPop.this, "Latitude", "");
            Preference.setValue(SearchFilterPop.this, "Longitude", "");
            Toast.makeText(SearchFilterPop.this,"Please enable Location services",Toast.LENGTH_SHORT).show();
        }
    }

    private void addCustomAlertView()
    {

        alert = new AlertDialog.Builder(SearchFilterPop.this);

        alert.setView(dialoglayout);

        dialog = alert.create();

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

//                Toast.makeText(SearchFilterPop.this, "back", Toast.LENGTH_SHORT).show();
                if (KeyEvent.KEYCODE_BACK == event.getKeyCode()) {
                    dialog.dismiss();
                    finish();
                }

                return true;
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
//                Toast.makeText(SearchFilterPop.this, "cancel", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                finish();
            }
        });

        dialog.show();
    }

    public void nearbyFirst(View v) {
        //code to check if this checkbox is checked!
        CheckBox checkBox = (CheckBox)v;
        if(checkBox.isChecked()){
            Log.v("Status", "checked");
            isNearby = "1";
        } else {
            Log.v("Status", "unchecked");
            isNearby = "0";
        }
    }

    public void topRatedFirst(View v) {
        //code to check if this checkbox is checked!
        CheckBox checkBox = (CheckBox)v;
        if(checkBox.isChecked()){
            Log.v("Status", "checked");
            isTopRated = "1";
        } else {
            Log.v("Status", "unchecked");
            isTopRated = "0";
        }
    }
}
